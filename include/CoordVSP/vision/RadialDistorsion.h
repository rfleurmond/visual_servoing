/*
 * RadialDistorsion.h
 *
 *  Created on: Jun 13, 2012
 *      @author Renliw Fleurmond
 */

#ifndef RADIALDISTORSION_H_
#define RADIALDISTORSION_H_

#include "Distorsion.h"

/**
   @brief Implements radial distorsion model 
 
   @author Renliw Fleurmond
*/

class RadialDistorsion: public Distorsion {

 private:

  double k1;

  static const double CONFIANCE;

 public:

  RadialDistorsion(double k);
  virtual ~RadialDistorsion();


  Eigen::VectorXd getParameters()const;

  void setParameters(Eigen::VectorXd);

  Eigen::VectorXd getDirtyImage(Eigen::VectorXd) const;

  Eigen::MatrixXd getJacobianDirtyImage(Eigen::VectorXd) const;

  Eigen::VectorXd getCleanImage(Eigen::VectorXd) const;

  Eigen::MatrixXd getJacobianCleanImage(Eigen::VectorXd) const;

  double getBeliefImage() const;

};

#endif /* RADIALDISTORSION_H_ */
