/**
 * Camera.h
 *
 *  Cette classe represente une camera et implemente en particulier
 *
 *  le modele stenope de la projection d'un point 3d sur une image 2D
 *
 *  Il y a des parametres extrinseques a la camera
 *
 *   le coordonnes du changement de repere  lies a la camera
 *
 *  le parametres intrinseques
 *
 *  	la focale f en metre
 *  	les dimensions des pixels ku et kv en m
 *  	la dimension de la retine   N * M
 *
 *	les parametres de distorsion sont decouples et sont places dans un objet distorsion
 *
 *	Nous avons les attributs d'objets
 *
 *	  alphaU = f/ku;
 *	  alphaV = f/kv;
 *
 *	  largeurVue = N/2
 *	  hauteurVue = M/2
 *
 *	La camera par defaut regarde dans la direction de l'axe X
 *
 *	cela est defini dans la definition des attributs:
 *
 * Pour regarder selon X
 *
 * 	int signeU    = -1;
 *	int signeV    = -1;
 *	int directionU = 1;
 *	int directionV = 2;
 *	int directionZ = 0;
 *
 *
 * Pour regarder selon Z
 *
 * 	int signeU    =  1;
 *	int signeV    =  1;
 *	int directionU = 0;
 *	int directionV = 1;
 *	int directionZ = 2;
 *
 *
 *  Created on: Jun 6, 2012
 *      @author Renliw Fleurmond
 */

#ifndef CAMERA_H_
#define CAMERA_H_

#include "Distorsion.h"
#include "graphiques-opencv.h"



/**
   @brief Implements a virtual camera

   @author Renliw Fleurmond
*/
  

class Camera {

 private:

  Repere frame;
  IntrinsicCam optique;
  int signeU;
  int signeV;
  int directionU;
  int directionV;
  int directionZ;

  
  
  Eigen::VectorXd parametres;
  Eigen::Vector2d getProjection( const Eigen::Vector3d & point) const;
  Eigen::Vector3d getDirection( const Eigen::Vector2d & ) const;
  Eigen::MatrixXd getJacobianProjection( const Eigen::Vector3d & ) const;

 public:

  static const int defaultSigneU    = -1;
  static const int defaultSigneV    = -1;
  static const int defaultDirectionU = 1;
  static const int defaultDirectionV = 2;
  static const int defaultDirectionZ = 0;
  static const int regardX = 1;
  static const int regardZ = 2;
	
  Camera();
  Camera(Repere);
  Camera(Repere , IntrinsicCam param);
  Camera(IntrinsicCam param);
  void setRegard(int);

  void setIntrinsicParameters( const IntrinsicCam & param);
  IntrinsicCam getIntrinsicParameters() const;

  int getSigneU() const;
  int getSigneV() const;
  int getDirectionU() const;
  int getDirectionV() const;
  int getDirectionZ() const;
  double getAlphaU() const;
  double getAlphaV() const;

  bool canSee( const Eigen::Vector3d & point) const;
  Eigen::Vector2d getDirtyImage(const Distorsion & modele, const Eigen::Vector3d & point) const;
  Eigen::MatrixXd getJacobianDirtyImage(const Distorsion & modele, const Eigen::Vector3d & point) const;
  Eigen::Vector2d getCleanImage( const Eigen::Vector3d & point) const;
  Eigen::Vector2d getCleanImage(const Distorsion &, const Eigen::Vector2d & point) const;
  Eigen::MatrixXd getJacobianCleanImage( const Eigen::Vector3d & point) const;
  Eigen::Vector2d getMetricProjection( const Eigen::Vector3d & point) const;
  Eigen::Vector2d getMetricProjection( const Eigen::Vector2d & point) const;
  Eigen::Vector3d getCleanDirection( const Eigen::Vector2d & image) const;
  Eigen::Vector3d getCleanDirection( const Eigen::Vector3d & point) const;
  Eigen::Vector3d getCleanDirection(const Distorsion &, const Eigen::Vector2d & point) const;
  Eigen::VectorXd getParameters(const Distorsion &) const;
  Eigen::VectorXd getParameters() const;
  void setParameters(Distorsion &, const Eigen::VectorXd & );
  void setParameters( const Eigen::VectorXd & );
  void setRepere(const Repere & );
  Repere getRepere() const;

  virtual CVEcran * getDisplay();

};

#endif /* CAMERA_H_ */
