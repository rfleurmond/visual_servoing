/*
 * StereoCamera.h
 * Cette classe implemente une paire stereo de camera
 *
 * Les repere de toutes les deux cameras sont exprimees par rapport au Repere centre
 *
 *
 *  Created on: Jun 12, 2012
 *      @author Renliw Fleurmond
 */

#ifndef STEREOCAMERA_H_
#define STEREOCAMERA_H_

#include "Camera.h"

/**
   @brief Implements stereo Camera
 
   @author Renliw Fleurmond
*/


class StereoCamera {


 private:

  Repere centre;

  Camera gauche;
  Camera droite;

  static const double seuilCroyance;


 public:
  StereoCamera();

  StereoCamera(Camera,Camera);

  void setRightCamera(Camera);
  void setLeftCamera(Camera);
  void setCentre(Repere rep);

  Camera getLeftCamera() const;
  Camera getRightCamera() const;
  Repere getCentre() const;


  Eigen::Vector2d getCleanRightImage(Eigen::Vector3d point) const;
  Eigen::Vector2d getCleanLeftImage(Eigen::Vector3d point) const;
  Eigen::MatrixXd getJacobianCleanRightImage(Eigen::Vector3d point) const;
  Eigen::MatrixXd getJacobianCleanLeftImage(Eigen::Vector3d point) const;

  Eigen::Vector2d getDirtyRightImage(const Distorsion &,Eigen::Vector3d point) const;
  Eigen::Vector2d getDirtyLeftImage(const Distorsion &,Eigen::Vector3d point) const;
  Eigen::MatrixXd getJacobianDirtyRightImage(const Distorsion &,Eigen::Vector3d point) const;
  Eigen::MatrixXd getJacobianDirtyLeftImage(const Distorsion &,Eigen::Vector3d point) const;

  bool areEpipolar(const Distorsion &, Eigen::Vector2d gauche,const Distorsion &,Eigen::Vector2d droite) const;

  double beliefEpipolar(const Distorsion &, Eigen::Vector2d gauche,const Distorsion &,Eigen::Vector2d droite) const;

  bool areEpipolar(Eigen::Vector2d gauche,Eigen::Vector2d droite) const;

  double beliefEpipolar(Eigen::Vector2d gauche,Eigen::Vector2d droite) const;

  Eigen::Vector3d getPoint(const Distorsion &, Eigen::Vector2d gauche,const Distorsion &,Eigen::Vector2d droite) const;

  Eigen::Vector3d getPoint(Eigen::Vector2d gauche,Eigen::Vector2d droite) const;

  //Eigen::MatrixXd getJacobianPoint(const Distorsion &, Eigen::Vector2d gauche,const Distorsion &,Eigen::Vector2d droite) const;

  //Eigen::MatrixXd getJacobianPoint(Eigen::Vector2d gauche,Eigen::Vector2d droite) const;


};

#endif /* STEREOCAMERA_H_ */
