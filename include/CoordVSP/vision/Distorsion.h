/*
 * Distorsion.h
 *
 * Cette classe represente la distorsion presente dans la plupart des cameras
 *
 * Elle est destinee a etre specialisee
 *
 */

#ifndef DISTORSION_H_
#define DISTORSION_H_

#include "../tools.h"

/**
   @brief Base class for distorsion model 

   *  Created on: Jun 6, 2012
 
   @author Renliw Fleurmond
*/

class Distorsion {

 protected:

  Eigen::VectorXd parametres;

 public:
  Distorsion();

  virtual ~Distorsion();

  virtual Eigen::VectorXd getParameters()const;

  virtual void setParameters(Eigen::VectorXd);

  virtual Eigen::VectorXd getDirtyImage(Eigen::VectorXd) const;

  virtual Eigen::MatrixXd getJacobianDirtyImage(Eigen::VectorXd) const;

  virtual Eigen::VectorXd getCleanImage(Eigen::VectorXd) const;

  virtual Eigen::MatrixXd getJacobianCleanImage(Eigen::VectorXd) const;

  virtual double getBeliefImage() const;


};

#endif /* DISTORSION_H_ */
