#ifndef GRAPHIQUES_OPENCV_H_
#define GRAPHIQUES_OPENCV_H_

#include <opencv/cv.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "intrinsic.h"

/**
   @brief Basic color manager
 
   @author Renliw Fleurmond
 */

class Palette{

 private:

  static int nbColors ;

 public :

  static cv::Scalar getLastColor();

  static cv::Scalar getLightColor();

};

/**
   @brief OpenCV display 
 
   @author Renliw Fleurmond
 */


class CVEcran{

 private:

  std::string windows;

  cv::Scalar couleurFond;

  cv::Scalar couleurGrille;

  cv::Scalar couleurRepere;

  int largeur;
  
  int hauteur;

  cv::Mat image;
  
  double ratioX;
  
  double ratioY;

  int maille;

  IntrinsicCam optique;

 public:

  CVEcran();

  CVEcran(CVEcran const & autre);

  CVEcran(std::string nom);

  CVEcran(std::string nom, int l, int h);

  ~CVEcran();

  void setNom(std::string nom);

  std::string getNom() const;

  void ouvrirFenetre();

  void fermerFenetre();

  void nettoyer();

  cv::Mat & getImage();

  void setImage(cv::Mat img);

  Eigen::Vector2d getPoint(Eigen::Vector2d A) const;

  Eigen::Vector2d getVector(Eigen::Vector2d V) const;

  void affichage();

  int getLargeur() const;

  int getHauteur() const;

  void setDimensions(int larg, int haut);

  void setOptique(IntrinsicCam opt);
  
};

/**
   @brief Record colors for visual features 
 
   @author Renliw Fleurmond

   @deprecated class Palette could cause disparition of this class
 */


struct visualisation{

  CVEcran windows;

  cv::Scalar couleur;

  cv::Scalar teinte;

visualisation():
  windows(),
    couleur(cv::Scalar(0,0,0)),
    teinte(cv::Scalar(64,64,64))
  {

  }

};


#endif
