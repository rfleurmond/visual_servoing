#ifndef INTRINSIC_H_
#define INTRINSIC_H_

#include "../tools.h"

/**
   @brief Implement intrinsic parameters of a camera

   @author Renliw Fleurmond
*/


struct IntrinsicCam {
  double u0;
  double v0;
  double alphaU;
  double alphaV;
  double largeur;
  double hauteur;

  IntrinsicCam();

  IntrinsicCam(double au, double av, double u00, double v00);

  Eigen::Vector2d getCenter() const;

};

std::ostream & operator<<(std::ostream & flux, const IntrinsicCam & opt);

#endif
