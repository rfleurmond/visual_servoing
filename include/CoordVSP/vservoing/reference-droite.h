#ifndef REFERENCE_DROITE_H_
#define REFERENCE_DROITE_H_

#include "visual-reference.h"
#include "line-point.h"
#include "feature-multipoint.h"

/**

   @brief Compute some reference based on alignement

   @deprecated Use instead daughter classes of AbstractLineFeature

   @author Renliw Fleurmond

 */


class AlignmentMiniLine: public MiniFeatureTask{

 private:

  MultiPointFeature * cible1;

  MultiPointFeature * cible2;

  Eigen::MatrixXi assoc; 

  int dimension;

 public:

  AlignmentMiniLine(MultiPointFeature *,MultiPointFeature *, const Eigen::MatrixXi &);

  int getDimension() const;

  Eigen::VectorXd getValue() const;

  Eigen::MatrixXd getJacobian() const;


};

#endif
