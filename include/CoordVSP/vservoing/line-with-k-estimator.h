#ifndef LINE_WITH_K_ESTIMATOR_H_
#define LINE_WITH_K_ESTIMATOR_H_

#include "line-estimator.h"
#include "line-with-k.h"

/**

   @brief Class made to estimate parameters of visual target LineWithK

   @author Renliw Fleurmond

 */


class LineKEstimator: public FeatureEstimator{

 protected:

  /// \f$ \phi\f$, a \f$3 \times 3 \f$ matrix which stores information about directions on the line
  Eigen::MatrixXd phi;

  /// \f$ \beta\f$, a \f$3 \times 1 \f$ vector which stores information about depth of the line
  Eigen::VectorXd beta;

  /// \f$ \phi_p\f$, a \f$3 \times 3 \f$ matrix which stores information about directions on a point
  Eigen::MatrixXd phiP;

  /// \f$ \beta_p\f$, a \f$3 \times 1 \f$ vector which stores information about depth of a point
  Eigen::VectorXd betaP;

  double length;

 public: 

  /**
     Constructor 
     @param model the pointer on visual target model
     @param tracker a tracker which gives image of the target
						
   */
  LineKEstimator(VisualFeature * model, VTracker * tracker);

  ~LineKEstimator(){
    closeLogFile();
  }

  void writeLog();
  
  void computeEstimation();

};

#endif
