#ifndef TRACK_SEGMENT_H_
#define TRACK_SEGMENT_H_

#include "graphiques-opencv.h"
#include "segment.h"

/**
   @brief Base class for real tracking on real images

   @author Renliw Fleurmond
   
 */

class RealTracker: public VTracker{

 protected:

  unsigned int nombreClicks;

  unsigned int limiteClicks;

  bool hasStarted;

  CVEcran * display;

 public: 

  static void mouseCallBack(int event, int x, int y, int flags, void * param);

  RealTracker();

  virtual void resetClicks();
  
  virtual void addVirtualClick();
 
  virtual void initTracking(CVEcran * display);

  virtual void track(const cv::Mat & image) = 0;

  virtual void answerOnClick(int x, int y);

  virtual bool isRunning();

};

/**
   @brief Tracker which uses sobel tranformations
*/

class SobelTracker: public RealTracker{

 protected:
  
  int largeur;

  int hauteur;

  int pas;

  int marge;

  double score;

  int seuil;

  cv::Mat histoire;

  double computeScore(const cv::Mat & img, cv::Point p1, cv::Point p2, bool divide = true);

  double distance(cv::Point p1, cv::Point p2);

  virtual void stepTracking(const cv::Mat & ) = 0;

 public:

  SobelTracker();

  virtual void track(const cv::Mat & img);

  virtual cv::Mat getStory() const;

  double getScore() const;

  cv::Mat sobel(const cv::Mat & input);

};

/**
   @brief Track a segment using sobel transformations

 */

class TrackSegment: public SobelTracker{

 private:

  cv::Point A;

  cv::Point B;

  cv::Point C;

  double rho;

  double theta;
  
  double k1;

  double k2;

  double length;
  
  double precision;

  Segment AB;

 protected:
  
  virtual void stepTracking(const cv::Mat & img);

 public:

  TrackSegment();

  virtual void attachToCamera(Camera * eye);

  void findFeatures(const Eigen::VectorXd & feat);

  void dessiner(cv::Mat & img) const;

  void answerOnClick(int x, int y);

  int getDimensionOutput() const;

  virtual bool itCanBeSeen() const;

  bool isLost() const;

  Eigen::VectorXd getFunction() const;

  void draw(CVEcran * const vue, cv::Scalar couleur) const;

  void drawFeat(const Eigen::VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const;

  double getRho() const;

  double getTheta() const;

  double getK1() const;

  double getK2() const;

};
#endif


