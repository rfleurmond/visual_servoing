#ifndef LINE_POINT_ESTIMATOR_H_
#define LINE_POINT_ESTIMATOR_H_

#include "line-estimator.h"
#include "line-point.h"

/**
   @brief Class made to estimate parameters of visual target LinePoint
   
   @author Renliw Fleurmond

 */


class LinePointEstimator: public FeatureEstimator{

 protected:

  /// \f$ \phi\f$, a \f$3 \times 3 \f$ matrice which stores information about directions
  Eigen::MatrixXd phi;

  /// \f$ \beta\f$, a \f$3 \times 1 \f$ vector which stores information about depth
  Eigen::VectorXd beta;

 public: 

  /**
     Constructor 
     @param model the pointer on visual target model
     @param tracker a tracker which gives image of the target
						
  */
  LinePointEstimator(VisualFeature * model, VTracker * tracker);

  ~LinePointEstimator(){
    closeLogFile();
  }


  void computeEstimation();
  
};

#endif
