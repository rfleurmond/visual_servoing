#ifndef FEATURE_ESTIMATOR_H_
#define FEATURE_ESTIMATOR_H_

#include "visualfeature.h"
#include <fstream>

/**
   @brief Base class for estimator of visual features

   an can be prefered to be direclty used by the visual task

   and indirecly use the basic  visual features class

   @author Renliw Fleurmond

*/

class FeatureEstimator: public VisualFeature{

 protected:

  bool hasLog; ///< hasLog file specified 

  bool access; /// file is opened

  std::string fichier; ///< file name

  std::ofstream printer; // output stream

  VTracker * traceur; ///< handler on Tracker 

  VisualFeature * modele; ///< Model of the visual target 

  bool hasInfluence; ///< If true, the estimated interaction matrix will be used

  Eigen::VectorXd estimation; ///< Vector of estimation

  int nUpdates; ///<number of estimation updates

  int seuil; ///< Threshold 

  double deadline; ///< Threshold 

  cv::Scalar blinkColor; ///< Special color

  double properTime; // Last time

  
 public:

  FeatureEstimator(VisualFeature * model, VTracker * tracker);

  /**
     Enable the use of estimation for projection and interaction matrix
   */
  void useEstimation(bool b);

  /**
     Set Threshold ; if number of updates is greater than this treshold, enable estimation
  */

  void setThreshold(int s);

  /**
     Set deadline in seconds ; if time is later than the deadline enable estimation
  */

  void setDeadline(double t);

  /**
     Get the dimension of the vector of image cues

  */


  int getDimensionOutput() const;

  /**

     Get the number of variables to be estimated

  */

  int getDimensionState() const;

  /**
     Return the current estimation
  */


  virtual Eigen::VectorXd getEstimation() const;

  /**
     Record the current value given by the tracker for further estimation
  */


  virtual void useTracker();

  /**
     Method to be implemented by any Daughter class

     Compute the estimation given the current and past values of the tracker
  */


  virtual void computeEstimation() = 0;

  /**
     Set a log file to record the estimation
     @param file string which content the complete path file
     Try to open a output stream to the log file
     This method is silent, if it cannot access the file
     it will not throw exception or send an error message
     
   */
  
  void setLogFile(std::string file);

  /**
     Record the current estimation to the log file
     Do nothing if no log file specified
     This method is silent, if it cannot access the file
     it will not throw exception or send an error message
     To be used in useTracker()

   */
  
  virtual void writeLog();
  
  /**
     Close the output stream to the log file
     Has non effect if no log file specified

     To be called in destructor
  */
  
  void closeLogFile();



  virtual void eraseMemory(){};  

  bool itCanBeSeen() const;

  Eigen::VectorXd getFunction(const Eigen::VectorXd & P) const;

  Eigen::VectorXd getFunction() const ;

  Eigen::MatrixXd getNaiveInteractionMatrix(const Eigen::VectorXd & F) const;
  
  Eigen::MatrixXd getInteractionMatrix(const Eigen::VectorXd & P) const;
   
  Eigen::MatrixXd getInteractionMatrix() const ;

  Eigen::VectorXd getErrorBetweenMeAnd(const Eigen::VectorXd & SS) const;

  Eigen::MatrixXd getJacobianError(const Eigen::VectorXd & SS) const;

  Eigen::MatrixXd getJacobianReferenceError(const Eigen::VectorXd & SS) const;

  Eigen::VectorXd getError(const Eigen::VectorXd & S, const Eigen::VectorXd & SS) const;

  Eigen::MatrixXd getJacobianError(const Eigen::VectorXd & S, const Eigen::VectorXd & SS) const;

  Eigen::MatrixXd getJacobianReferenceError(const Eigen::VectorXd & S, const Eigen::VectorXd & SS) const;

  virtual void setSituation(const Repere & r);

  virtual void attachToCamera(Camera * eye);

  virtual void draw(CVEcran * const vue, cv::Scalar couleur) const;

  void drawFeat(const Eigen::VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const;

};

#endif
