#ifndef MANY_ARM_HOLDER_TASK_H_
#define MANY_ARM_HOLDER_TASK_H_

#include "visualtask.h"

/**
   @brief Class used by ManyArmHolderTask to record a link to one robotic arm 

   and where the latter is fixed

   @author Renliw Fleurmond
 */

struct BaseArm{

  InverseKinematicModel * theArm;

  Repere theShoulder;

  BaseArm();

};

/**
   @brief Class used by ManyArmHolderTask to record a link to a camera 

   and where the latter is fixed

   @author Renliw Fleurmond

 */


struct MovingEye{

  Camera * eye;

  InverseKinematicModel * arm;

  CVEcran * ecran;

  int degres;

  Repere fixation;

  MovingEye();

};

/**
   @brief Class used by ManyArmHolderTask to record a link to one visual target 

   and where the latter is fixed

   @author Renliw Fleurmond

 */


struct MovingFeature{

  InverseKinematicModel * arm;

  int degres;

  cv::Scalar couleur;

  cv::Scalar teinte;

  Repere fixation;

  MovingFeature();
  
};


/**
   @brief Class used by ManyArmHolderTask to record the link between cameras and visual target

   @author Renliw Fleurmond
 
 
 */

struct CamFeat{

  Camera * eye;

  VisualFeature * target;

  VisualReference * ref;

  bool hasRef;

  Eigen::VectorXd K;

  Eigen::MatrixXd C;

  CamFeat();

};

/**
   @brief Class used by ManyArmHolderTask to record a RelativeTask

   @author Renliw Fleurmond

 */


struct CameraTask{

  Camera * eye;

  RelativeTask * task;

  CameraTask();

};


/**
   @brief Class used by ManyArmHolderTask to record last results of time consuming fonctions;

   @author Renliw Fleurmond

 */

struct SaveTime{

  bool hasStory;

  Eigen::VectorXd last_q;

  Eigen::VectorXd last_value;

  Eigen::MatrixXd last_jacobian;

SaveTime():
  hasStory(false),
    last_q(Eigen::VectorXd::Zero(1)),
    last_value(Eigen::VectorXd::Zero(1)),
    last_jacobian(Eigen::MatrixXd::Zero(1,1))
  {

  }
    
  bool hasAlreadyCompute(const Eigen::VectorXd & q){
    if(hasStory){
      Eigen::VectorXd erreur(last_q -q);
      if(erreur.norm()<1e-6)
	return true;
      else
	return false;
    }
    else
      return false;
  }
};



/**
   @brief An daughter of AbstracVisualTask which deals with plural robotic arms, cameras and visual targets

   Sister of VisualTask

   @author Renliw Fleurmond
 */

class ManyArmHolderTask: public AbstractVisualTask{

 private:

  const static int CVperiode;

  bool shouldErase;
 
  double periode;

  int numFeats;

  int numCams;

  int numArms;

  bool showDisplays;

  bool updateFonction;

  bool updateJacobien;

  SaveTime * story;

  std::list<BaseArm> allArms;

  std::list<MovingEye> allCameras;

  std::map<VisualFeature *, MovingFeature> allFeatures;

  std::list<CamFeat *> allPairs;

  std::list<CameraTask> allTasks;

  Eigen::VectorXd computeFonction(const Eigen::VectorXd & ) const;

  Eigen::MatrixXd computeJacobien(const Eigen::VectorXd & q) const;


 public:

  // Constructors and destructors

  ManyArmHolderTask(bool nonHide = true);

  ~ManyArmHolderTask();

  void enableFunctionEvaluation(bool v) {
    updateFonction = v;
  }
  
  void enableJacobianEvaluation(bool v) {
    updateJacobien = v;
  }

  // Specific methods

  void addArm(
	      InverseKinematicModel * arm, 
	      const Repere & shoulder);

  void addCamera(Camera * cam, 
		 InverseKinematicModel * arm, 
		 const Repere & f, 
		 int deg);

  void addCamera(
		 Camera * cam, 
		 InverseKinematicModel * arm, 
		 const Repere & f, 
		 int deg, 
		 std::string nom);

  void addVisualTarget(
		       VisualFeature * target, 
		       InverseKinematicModel * arm, 
		       const Repere & f, 
		       int deg);

  void makeTargetSeen(
		      VisualFeature * target, 
		      Camera * cam);

  void giveTargetReference(
			   Camera * cam, 
			   VisualFeature * target, 
			   VisualReference * vRef, 
			   bool ref);

  void giveKandCmatrix(
		       Camera * cam, 
		       VisualFeature * target, 
		       const Eigen::MatrixXd & C, 
		       const Eigen::VectorXd & K);

  void keepVisualTrace(bool val);

  void addOtherTask(Camera * cam, RelativeTask * t);

  // From Mother class Task

  void drawVisualFeatures() const;

  Eigen::VectorXd fonction(const Eigen::VectorXd & ) const;

  void setState(const Eigen::VectorXd &  q);

  Eigen::MatrixXd jacobien(const Eigen::VectorXd & q) const;

  double getPeriod() const;

  void setPeriod(double dt);

  bool hasChanged() const;

};

#endif
