#ifndef FLYING_SYSTEM_H_
#define FLYING_SYSTEM_H_

#include "../taches.h"
#include "../robots.h"

/**
   @brief  Controllable sytem wich implements a flying hand
 
   @author Renliw Fleurmond
 */



class FlyingSystem: public TaskSystem, public InverseKinematicModel{

 private:

  Repere frame;

  double periode;

  double tempo;

  Eigen::VectorXd vitesse;

 public:

  FlyingSystem();

  void setSituation(const Repere & r);

  int getDimState() const;

  Eigen::VectorXd getState() const;

  void setState(const Eigen::VectorXd & state);

  void takeCommand(const Eigen::VectorXd & order);

  void updateTime(double t);

  double getPeriod() const;

  int getTotalDegres() const;

  Repere getSituation(const Eigen::VectorXd & q) const;

  Repere getSituation(const Eigen::VectorXd & q, int i) const;

  Eigen::Vector3d getLinearVelocity(const Eigen::VectorXd & q, const Eigen::VectorXd & qpoint) const;

  Eigen::Vector3d getAngularVelocity(const Eigen::VectorXd & q, const Eigen::VectorXd & qpoint) const;

  Eigen::MatrixXd getKinematicJacobian(const Eigen::VectorXd & q) const;

  Eigen::MatrixXd getKinematicJacobian(const Eigen::VectorXd & q, int i) const;

  bool getCommandFromVelocity(
			      const Eigen::VectorXd & q, 
			      const Eigen::VectorXd & torseur, 
			      Eigen::VectorXd & commande) const;

  bool getCommandFromLinearVelocity(
				    const Eigen::VectorXd & q, 
				    const Eigen::Vector3d & linear, 
				    Eigen::VectorXd & commande) const;

  bool getCommandFromAngularVelocity(
				     const Eigen::VectorXd & q, 
				     const Eigen::Vector3d & angular, 
				     Eigen::VectorXd & commande) const;

};

#endif
