#ifndef ICYLINDER_ESTIMATOR_H_
#define ICYLINDER_ESTIMATOR_H_

#include "line-estimator.h"
#include "infinite-cylinder.h"

/**
   @brief Estimate parameters of a non bounded Cylinder
   
   @author Renliw Fleurmond

 */


class ICylinderEstimator: public FeatureEstimator{

 protected:

  /// \f$ n \f$, a scalar, the number of images used for estimation
  int n;

  /// \f$ \phi \f$, a \f$3 \times 3 \f$ matrix
  Eigen::MatrixXd phi;

  /// \f$ \beta \f$, a \f$3 \times 1 \f$ vector
  Eigen::VectorXd beta;

  /// \f$ \gamma \f$, a \f$3 \times 1 \f$ vector
  Eigen::VectorXd gamma;

  /// \f$ \mu \f$, a scalar 
  double mu;

  /// The known radius of the cylinder
  double radius;

  bool knowRadius;

  /**
     Compute the normal vector coordinates on the target frame

     corresponding to the visual cues \f$ \rho \f$, \f$ \theta\f$ in the image plane

     The coordinates of the normal vector on the camera frame has the following expression:

     \f[ N_i = \frac{1}{\sqrt{f^2 + \rho^2_i}} {\begin{bmatrix} f \cos{\theta} & f \sin{\theta} & -\rho \end{bmatrix}}^T \f]

     with \f$ f \f$ the focal length of the camera

   */

  Eigen::Vector3d getNormal(double rho, double theta) const;

 public: 

  /**
     Constructor 
     @param model the pointer on visual target model
     @param tracker a tracker which gives image of the target
						
   */
  ICylinderEstimator(VisualFeature * model, VTracker * tracker);

  ~ICylinderEstimator(){
    closeLogFile();
  }

  void setRadius(double r);

  void computeEstimation();

};

#endif
