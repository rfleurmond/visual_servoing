#ifndef FEATURE_POINT_H_
#define FEATURE_POINT_H_

#include "visualfeature.h"

/**
   @brief This class represent a visual target composed by only a point
   
   It's a basic class wich can be used by other visual features

   @author Renliw Fleurmond
**/


class PointFeature: public VisualFeature{

 private:

  /// The coordinates of the point

  Eigen::Vector3d point;
  
 public:


  /**
     A contructor which need the coordinates of the point
     @param p the coordinates of the point
     If no specified constructor which initialize the position of the point 
     to  \f${[0 \ 0 \ 0]}^T\f$ 
  **/
  PointFeature(Eigen::Vector3d p = Eigen::Vector3d::Zero());

  int getDimensionState() const;

  int getDimensionOutput() const;

  /**
     Return the value of the visual features
     expected for a point in the same pose with respect to the world reference frame
     and seen by the same camera
     @param P a vector of parameters which describe completely the target

     \f[
        P = {[x\ y\ z]}^T
     \f]

     \f[I = 
     \begin{bmatrix}
     fx/z \\ fy/z
     \end{bmatrix}
     \f]
     
     with f the focal length of the camera

     @see Camera
     @return I
  **/


  Eigen::VectorXd getFunction(const Eigen::VectorXd & P) const;

  Eigen::VectorXd getFunction() const; 

  /**
     Return a rough and naive interaction matrix corresponding to the
     given vector of the visual features
     with a depth setted to 1 meter
     @param F is the vector of visual cues
     
     \f[
     F = {[x\ y]}^T
     \f]

     \f[ L =
     \begin{bmatrix}
     -f & 0 & x & xy/f & -(f+{x^2}/f) & y\\
     0 & -f & y & f+{y^2}/f& -xy/f & -x \\ 
     \end{bmatrix}
     \f]

     with f the focal length of the camera

     @see Camera

     @return L
     
     
  **/


  Eigen::MatrixXd getNaiveInteractionMatrix(const Eigen::VectorXd & F) const;
  

  /**
     Return the interaction matrix of the visual features
     expected for a visual target of same kind
     in the same pose with respect to the wold reference frame
     and seen by the same camera
     @param P a vector of parameters which describe completely the target
       \f[
        P = {[x\ y\ z]}^T
     \f]

     \f[ L =
     \begin{bmatrix}
     -f/z & 0 & x/z & xy/f & -(f+{x^2}/f) & y\\
     0 & -f/z & y/z & f+{y^2}/f& -xy/f & -x \\ 
     \end{bmatrix}
     \f]

     with f the focal length of the camera

     @see Camera

     @return L
     
  **/

  Eigen::MatrixXd getInteractionMatrix(const Eigen::VectorXd & P) const;

  Eigen::MatrixXd getInteractionMatrix() const;

  Eigen::VectorXd getError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;

  bool itCanBeSeen() const;

  void drawFeat(const Eigen::VectorXd & value,CVEcran * const vue, cv::Scalar couleur) const;
  
};


#endif
