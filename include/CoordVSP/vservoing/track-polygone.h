#ifndef TRACK_POLYGONE_H_
#define TRACK_POLYGONE_H_

#include "track-segment.h"
#include "feature-multipoint.h"
#include <vector>

/**
   @brief Track a polygone using sobel transformations

   @author Renliw Fleurmond

*/

class TrackPoly: public SobelTracker{

 private:

  unsigned int N;

  double length;

  std::vector<cv::Point> liste;

  bool boucle;
  
  virtual void stepTracking(const cv::Mat & img);

  void displayLine(cv::Point a, cv::Point b, cv::Mat & img) const;
  
 public:

  TrackPoly(int n = 2, bool closed = false);

  ~TrackPoly();

  void answerOnClick(int x, int y);

  virtual void resetClicks();

  int getDimensionOutput() const;

  void findFeatures(const Eigen::VectorXd & feat);

  virtual bool itCanBeSeen() const;

  bool isLost() const;

  Eigen::VectorXd getFunction() const;

  void draw(CVEcran * const vue, cv::Scalar couleur) const;

  void drawFeat(const Eigen::VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const;

  void dessiner(cv::Mat & img) const;
  
};
#endif

