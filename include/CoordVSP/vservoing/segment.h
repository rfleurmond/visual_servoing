#ifndef SEGMENT_H_
#define SEGMENT_H_

#include "line-with-k.h"

/**

   @brief This class represent a 3D straight segment

   @see LineWithK

   @author Renliw Fleurmond

*/

class Segment: public LineFeature{

  public:

  Segment(const Eigen::VectorXd & pA, const Eigen::VectorXd & pB);

  int getDimensionOutput() const;

  virtual Eigen::VectorXd getFunction(const Eigen::VectorXd & P) const;  

  virtual Eigen::VectorXd getFunction() const;  

  virtual Eigen::MatrixXd getNaiveInteractionMatrix(const Eigen::VectorXd & F) const;
  
  virtual Eigen::MatrixXd getInteractionMatrix(const Eigen::VectorXd & P) const;

  virtual Eigen::MatrixXd getInteractionMatrix() const;

  virtual Eigen::VectorXd getError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;

  virtual Eigen::MatrixXd getJacobianError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;
  
  virtual Eigen::MatrixXd getJacobianReferenceError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;
  
  bool itCanBeSeen() const;

  virtual void draw(CVEcran * const vue, cv::Scalar couleur) const;

  virtual void drawFeat(const Eigen::VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const;

  static Eigen::VectorXd computeRhoTeta2K(Eigen::Vector2d A, Eigen::Vector2d B);

  static Eigen::VectorXd computeABfromRhoTeta2K(Eigen::VectorXd F);

  
};


#endif
