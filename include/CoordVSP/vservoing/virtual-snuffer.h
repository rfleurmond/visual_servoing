#ifndef VIRTUAL_SNUFFER_H_
#define VIRTUAL_SNUFFER_H_

#include "visualfeature.h"

/**
   @brief Class which can hide artificially a visual tracker

   @author Renliw Fleurmond
*/

class VSnuffer: public VTracker{

 protected: 

  VTracker * traceur;

  int  * nbUses;

  double firstCountDown;

  double secondCountDown;

  int x;

  int y;
 

 public:


  VSnuffer(VTracker * tracker, double first, double second);

  ~VSnuffer();

  virtual int getDimensionOutput() const;

  virtual bool itCanBeSeen() const;

  virtual bool isLost() const;

  virtual Eigen::VectorXd getFunction() const;  

  virtual void attachToCamera(Camera * eye);

  virtual void draw(CVEcran * const vue, cv::Scalar couleur) const;

  virtual void drawFeat(const Eigen::VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const;

};

#endif


