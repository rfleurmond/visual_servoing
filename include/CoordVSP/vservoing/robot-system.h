#ifndef ROBOT_SYSTEM_H_
#define ROBOT_SYSTEM_H_

#include "../taches.h"
#include "../robots.h"

/**
   @brief An link between a controllable system and a robot model 
 
   @author Renliw Fleurmond
 */


class RobotSystem: public TaskSystem {

 private:

  double periode;

  double tempo;

  Robot * body;

  int dimension;

  bool checkBounds;

  Eigen::VectorXd vitesse;

  Eigen::VectorXd minima;

  Eigen::VectorXd maxima;

 public:

  RobotSystem(Robot * r2d2);

  int getDimState() const;

  Eigen::VectorXd getState() const;

  void setState(const Eigen::VectorXd & state);

  void takeCommand(const Eigen::VectorXd & order);

  void updateTime(double t);

  double getPeriod() const;

  void setPeriod(double p);

  void enableCheckBounds(bool t);
  
};

#endif
