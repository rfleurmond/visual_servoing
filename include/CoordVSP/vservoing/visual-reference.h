#ifndef VISUAL_REFERENCE_H_
#define VISUAL_REFERENCE_H_

#include "visualfeature.h"

/**
   @brief Base class reference for visual features 
 
   @author Renliw Fleurmond
 */


class VisualReference{

 public:

  virtual ~VisualReference(){};

  virtual Eigen::VectorXd getReference() const = 0;

  virtual VisualFeature * getFeatures() {return NULL;}

};

/**
   @brief constant VisualReference for visual features 
 
   @author Renliw Fleurmond
 */


class ConstReference: public VisualReference{

 private:
  
  Eigen::VectorXd reference;

 public:

 ConstReference(const Eigen::VectorXd & ref):
  VisualReference(),
    reference(ref)
    {

    }

  Eigen::VectorXd getReference() const{
    return reference;
  }

};


/**
   @brief Make one visual feature be a VisualReference for oneanother visual feature 
 
   @author Renliw Fleurmond
 */
class OtherReference: public VisualReference{

 private:
  
  VisualFeature * reference;

 public:

 OtherReference(VisualFeature * ref):
  VisualReference(),
    reference(ref)
    {

    }

  virtual ~OtherReference(){};
  

  Eigen::VectorXd getReference() const{
    return reference->getFunction();
  }

  VisualFeature * getFeatures() {return reference;}

};


/**
   @brief Small simple visual task 
 
   @author Renliw Fleurmond
 */

class MiniFeatureTask{

 protected:

  bool isGradient;

 public: 

  virtual ~MiniFeatureTask(){};

  virtual int getDimension() const = 0;

  virtual Eigen::VectorXd getValue() const = 0;

  bool isGradientTask() const{ return isGradient;};

  virtual Eigen::MatrixXd getJacobian() const = 0;

};

/**
   @brief Small simple visual task based on visual features 
 
   @author Renliw Fleurmond
 */


class RelativeTask{

 public:

  virtual ~RelativeTask(){};

  virtual int getDimension() const = 0;

  virtual Eigen::VectorXd getValue() const = 0;

  virtual bool dependOf(VisualFeature * feat) const = 0;

  virtual Eigen::MatrixXd getJacobian(VisualFeature *) const = 0;

};


/**
   @brief Small simple visual task based on two visual features 
 
   @author Renliw Fleurmond
 */

class DifferenceFeature: public RelativeTask{

 private:

  VisualFeature * current;

  VisualFeature * wanteds;

  int dimension;

  Eigen::MatrixXd C;

  Eigen::VectorXd K;

 public:

  DifferenceFeature(VisualFeature * curr, VisualFeature * want);

  virtual ~DifferenceFeature(){};
  
  virtual int getDimension() const;

  virtual Eigen::VectorXd getValue() const;

  virtual bool dependOf(VisualFeature * feat) const;

  virtual Eigen::MatrixXd getJacobian(VisualFeature *) const;

  void setCMatrix(const Eigen::MatrixXd & A);

  void setKConstant(const Eigen::VectorXd & A);

};


/**
   @brief put two visual references in one singe VisualReference 
 
   @author Renliw Fleurmond
 */


class CoReference: public VisualReference{

 private:
  
  VisualReference * ref1;

  VisualReference * ref2;

 public:

 CoReference(VisualReference * r1,VisualReference * r2):
  VisualReference(),
    ref1(r1),
    ref2(r2)
    {
      assert(r1!=NULL);
      assert(r2!=NULL);
    }

  virtual ~CoReference(){};
  
  Eigen::VectorXd getReference() const{
    Eigen::VectorXd v1,v2,v12;
    v1 = ref1->getReference();
    v2 = ref2->getReference();
    v12 = Eigen::VectorXd::Zero(v1.rows()+v2.rows());
    v12.topRows(v1.rows()) = v1;
    v12.bottomRows(v2.rows()) = v2;
    return v12;
  }

};



/**
   @brief Small simple visual task based on two visual features 
 
   @author Renliw Fleurmond
 */

class CombinaisonFeature: public RelativeTask{

 private:

  VisualFeature * current;

  VisualFeature * wanteds;

  int dimension;

  Eigen::MatrixXd C1;

  Eigen::MatrixXd C2;

  Eigen::VectorXd K;

 public:

  CombinaisonFeature(VisualFeature * curr, VisualFeature * want);

  virtual ~CombinaisonFeature(){};

  virtual int getDimension() const;

  virtual Eigen::VectorXd getValue() const;

  virtual bool dependOf(VisualFeature * feat) const;

  virtual Eigen::MatrixXd getJacobian(VisualFeature *) const;

  void setMatrices(const Eigen::MatrixXd & A, const Eigen::MatrixXd & B, const Eigen::VectorXd & K);

};

#endif
