#ifndef SHORT_CYLINDER_TRACKER_H_
#define SHORT_CYLINDER_TRACKER_H_

#include "bounded-cylinder.h"
#include "track-polygone.h"


/**
   @brief Track a short cylinder

   @author Renliw Fleurmond

*/

class CylinderTracker: public RealTracker{

 private:

  Eigen::Vector2d center;

  TrackPoly traceurR;

  TrackPoly traceurL;

  BoundedCylinder designer;

 public:

  CylinderTracker();

  void track(const cv::Mat & img);

  void attachToCamera(Camera * eye);

  void findFeatures(const Eigen::VectorXd & feat);

  virtual void resetClicks();
  
  void answerOnClick(int x, int y);

  int getDimensionOutput() const;

  bool itCanBeSeen() const;

  bool isLost() const;

  Eigen::VectorXd getFunction() const;

  void draw(CVEcran * const vue, cv::Scalar couleur) const;

  void drawFeat(const Eigen::VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const;

};


#endif
