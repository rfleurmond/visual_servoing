#ifndef LINE_ESTIMATOR_H_
#define LINE_ESTIMATOR_H_

#include "feature-estimator.h"
#include "feature-droite.h"

/**
   @brief Class made to estimate parameters of visual target DroiteFeature

   @author Renliw Fleurmond

 */


class LineEstimator: public FeatureEstimator{

 protected:

  /// \f$ \phi\f$, a \f$3 \times 3 \f$ matrix which stores information about directions
  Eigen::MatrixXd phi;

  /// \f$ \beta\f$, a \f$3 \times 1 \f$ vector which stores information about depth
  Eigen::VectorXd beta;

 public: 

  /**
     Constructor 
     @param model the pointer on visual target model
     @param tracker a tracker which gives image of the target
						
  */
  LineEstimator(VisualFeature * model, VTracker * tracker);

  ~LineEstimator(){
    closeLogFile();
  }


  void computeEstimation();

  /**
     Special method used in computeEstimation()
     @param A[in] a Matrix \f$3 \times 3 \f$
     @param B[in] a Vector \f$3 \times 1 \f$
     @param P[out] a Vector \f$3 \times 1 \f$
     @param U[out] a Vector \f$3 \times 1 \f$

     \f$ P = A^{+} \times B \f$ , using a SVD decomposition

     \f$ U \f$ is the eigen vector corresponding to the lowest eigenvalue of matrix \f$ A \f$
     
  */
  static double resoudre(const Eigen::MatrixXd & A,
			 const Eigen::VectorXd & B,
			 Eigen::VectorXd & P,
			 Eigen::VectorXd & U);

  //Eigen::MatrixXd getEstimatedJacobian() const;

};

#endif
