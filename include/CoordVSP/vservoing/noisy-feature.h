#ifndef NOISY_FEATURE_H_
#define NOISY_FEATURE_H_

#include "visualfeature.h"

/**
   @brief Class used to add artificially pseudo-gaussian noise on visual features

   @author Renliw Fleurmond
   
 */

class NoisyFeature: public VisualFeature{

 private:

  VisualFeature * feat;

  int dimension;

  MultivariateGaussian * roulette;

  Eigen::VectorXd * bruit;

  double * heure;

 public:

  NoisyFeature(VisualFeature * f, Eigen::MatrixXd cov);

  ~NoisyFeature();

  int getDimensionState() const;

  int getDimensionOutput() const;

  void setCovariance(Eigen::MatrixXd cov);

  Eigen::VectorXd getClearFunction() const;  

  Eigen::VectorXd getFunction(const Eigen::VectorXd & P) const;

  Eigen::VectorXd getFunction() const;  

  Eigen::MatrixXd getNaiveInteractionMatrix(const Eigen::VectorXd & F) const;
  
  Eigen::MatrixXd getInteractionMatrix(const Eigen::VectorXd & P) const;

  Eigen::MatrixXd getInteractionMatrix() const;

  Eigen::VectorXd getError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;

  Eigen::MatrixXd getJacobianError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;
  
  Eigen::MatrixXd getJacobianReferenceError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;
  
  bool itCanBeSeen() const;

  virtual void setSituation(const Repere & r);

  virtual void attachToCamera(Camera * eye);

  void draw(CVEcran * const vue, cv::Scalar couleur) const;

  void drawFeat(const Eigen::VectorXd & value,CVEcran * const vue, cv::Scalar couleur) const;

};

#endif
