#ifndef FEATURE_MULTIPOINT_H_
#define FEATURE_MULTIPOINT_H_

#include "feature-point.h"

/**
   @brief This class represent a visual target composed by many PointFeature

   It is perfect for the classical well know case of 4 points
   
   @author Renliw Fleurmond
**/


class MultiPointFeature: public VisualFeature{

 private:

  Eigen::MatrixXd points;
  
 public:

  MultiPointFeature(Eigen::MatrixXd pts);

  int getDimensionState() const;

  int getDimensionOutput() const;

  /**
     Return the value of the visual features
     expected for plural points in the same pose with respect to the wold reference frame
     and seen by the same camera
     @param P a vector of parameters which describe completely the target

     \f[
        P = {[P^T_1\ P^T_2\ \hdots P^T_{n-1}\ P^T_n]}^T
     \f]

     \f[
        P_i = {[x_i\ y_i\ z_i]}^T
     \f]

     \f[S  = f
     \begin{bmatrix}
     x_1/z_1 \\ y_1/z_1 \\
     \vdots \\
     x_n/z_n \\ y_n/z_n
     \end{bmatrix}
     \f]
     
     with f the focal length of the camera

     @see Camera
     @return S
  **/


  Eigen::VectorXd getFunction(const Eigen::VectorXd & P) const;

  Eigen::VectorXd getFunction() const; 

  /**
     Return a rough and naive interaction matrix corresponding to the
     given vector of the visual features
     with a depth setted to 1 meter
     @param F is the vector of visual cues
     
     \f[
     F = 
     \begin{bmatrix}
     x_1 \\ y_1 \\
     \vdots \\
     x_n \\ y_n
     \end{bmatrix}
     \f]

     \f[ L_i =
     \begin{bmatrix}
     -f & 0 & x_i & x_i y_i/f & -(f+{x^2_i}/f) & y_i\\
     0 & -f & y_i & f+{y^2_i}/f& -x_iy_i/f & -x_i \\ 
     \end{bmatrix}
     \f]

     with f the focal length of the camera

     \f[L  = 
     \begin{bmatrix}
     L_1 \\
     \vdots \\
     L_n
     \end{bmatrix}
     \f]
     
     @see Camera

     @return L
     
     
  **/


  Eigen::MatrixXd getNaiveInteractionMatrix(const Eigen::VectorXd & F) const;
  

  /**
     Return the value of the visual features
     expected for plural points in the same pose with respect to the wold reference frame
     and seen by the same camera
     @param P a vector of parameters which describe completely the target

     \f[
        P = {[P^T_1\ P^T_2\ \hdots P^T_{n-1}\ P^T_n]}^T
     \f]

     \f[
        P_i = {[X_i\ Y_i\ Z_i]}^T
     \f]

     \f[ x_i = X_i/Z_i,\ y_i = Y_i/ Z_i \f]

     \f[ L_i =
     \begin{bmatrix}
     -f/Z_i & 0 & x_i/Z_i & x_i y_i/f & -(f+{x_i^2}/f) & y_i\\
     0 & -f/Z_i & y_i/Z_i & f+{{y_i}^2}/f& -x_i y_i /f & -x_i \\ 
     \end{bmatrix}
     \f]

     with f the focal length of the camera

     \f[L  = 
     \begin{bmatrix}
     L_1 \\
     \vdots \\
     L_n
     \end{bmatrix}
     \f]
     
 
     @see Camera
     @return L
  **/

  Eigen::MatrixXd getInteractionMatrix(const Eigen::VectorXd & P) const;

  Eigen::MatrixXd getInteractionMatrix() const;

  Eigen::VectorXd getError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;

  PointFeature getFeature(int i) const;

  bool itCanBeSeen() const;

  //  void draw(CVEcran * const vue, cv::Scalar couleur) const;

  void drawFeat(const Eigen::VectorXd & value,CVEcran * const vue, cv::Scalar couleur) const;
  
};


#endif
