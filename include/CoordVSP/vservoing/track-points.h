#ifndef TRACK_POLYPOINTS_H_
#define TRACK_POLYPOINTS_H_

#include "track-segment.h"
#include "feature-multipoint.h"
#include <vector>

/**
   @brief Track a polygone using sobel transformations

   @author Renliw Fleurmond

*/

class TrackPoints: public RealTracker{

 private:

  unsigned int N;

  std::vector<cv::Point2f> liste;

  virtual void stepTracking(const cv::Mat & img);

  cv::Mat memory;

  bool success;
  
 public:

  TrackPoints(int n = 1);

  ~TrackPoints();

  void answerOnClick(int x, int y);

  virtual void resetClicks();

  int getDimensionOutput() const;

  void findFeatures(const Eigen::VectorXd & feat);

  virtual void initTracking(CVEcran * display);

  virtual void track(const cv::Mat & image);

  virtual bool itCanBeSeen() const;

  bool isLost() const;

  Eigen::VectorXd getFunction() const;

  void draw(CVEcran * const vue, cv::Scalar couleur) const;

  void drawFeat(const Eigen::VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const;

};
#endif

