#ifndef POINT_ESTIMATOR_H_
#define POINT_ESTIMATOR_H_

#include "feature-estimator.h"
#include "feature-point.h"

/**
   @brief Class made to estimate parameters of visual target PointFeature

   @author Renliw Fleurmond

 */



class PointEstimator: public FeatureEstimator{

 protected:

  /// \f$ \phi\f$, a \f$3 \times 3 \f$ matrix which stores information about directions
  Eigen::MatrixXd phi; 

  /// \f$ \beta\f$, a \f$3 \times 1 \f$ vector which stores information about depth
  Eigen::VectorXd beta;

 public: 

  /**
     Constructor 
     @param model the pointer on visual target model
     @param tracker a tracker which gives image of the target
						
  */
  PointEstimator(VisualFeature * model, VTracker * tracker);

  ~PointEstimator(){
    closeLogFile();
  }


  void computeEstimation();

  /*
    Retourne une matrice de 3 * 15
    3 premieres colonnes l'orientation de la camera
    3 suivantes la position de la caméra.
    3 colonnes pour l'orientation de la cible visuelle ( si mobile)
    3 colonnes pour la position de la cible visuelle
    3 dernieres pour les coordonnes du point
  */
  //  Eigen::MatrixXd getEstimatedJacobian() const;

  

};

#endif
