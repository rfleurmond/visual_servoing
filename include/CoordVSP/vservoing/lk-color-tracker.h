#ifndef LK_COLOR_TRACKER_H_
#define LK_COLOR_TRACKER_H_

#include "track-segment.h"

/**
   @brief A special tracker wich uses in color tracker in intern
   
   @author Renliw Fleurmond

 */

class SpecialTracker: public RealTracker{

private:

  int h;

  int s;

  int v;

  int tolerance;

  int marge;

  double moyX;

  double moyY;

  double rho;

  double theta;

  double k;

  double u0;

  double v0;
  
  bool lost;

  Eigen::Matrix2d dispersion;

  LineWithK traceur;

public:

  cv::Mat photo;
    
  SpecialTracker(int tol = 2, int size = 15);

  void track(const cv::Mat & image);

  double calculTheta(const Eigen::Matrix2d & cov);

  void initiate(int hh, int ss, int vv);
 
  int getDimensionOutput() const;

  virtual bool itCanBeSeen() const;

  bool isLost() const;

  Eigen::VectorXd getFunction() const;

  void attachToCamera(Camera * eye);

  void draw(CVEcran * const vue, cv::Scalar couleur) const;

  void drawFeat(const Eigen::VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const;

  virtual void answerOnClick(int x, int y);

};


#endif
