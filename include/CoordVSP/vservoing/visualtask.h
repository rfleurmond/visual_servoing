#ifndef VISUAL_TASK_H_
#define VISUAL_TASK_H_

#include "../taches.h"
#include "visual-reference.h"
#include "../robots.h"
#include "graphiques-opencv.h"


/**
   @brief Base class for visual servoing Task

   @author Renliw Fleurmond
 */

class AbstractVisualTask: public Task{

 public:

  AbstractVisualTask();

};

/**
   @brief First visual task using one visual feature, one camera and one arm

   @author Renliw Fleurmond
 */


class VisualTask: public AbstractVisualTask{

 private:

  const static int CVperiode = 5;

  bool shouldErase;

  int modeFixation;

  double periode;

  visualisation * graphique;

  VisualFeature * features;

  InverseKinematicModel * modele;

  VisualReference * oracle;

  Eigen::VectorXd reference;

  Repere fixation;

  Repere trepied;

  Eigen::MatrixXd getKineticTransformation() const;
  
  Eigen::MatrixXd getInversKineticTransformation() const;

  void afficherFeatures(VisualFeature * feat) const;

  void afficherReferences(VisualFeature * feat) const;

  void attendreImage() const;

  void nettoyerImage() const;

  void creerFenetre();

 public :

  static const int DEPORTEE = 1;
  static const int EMBARQUE = 2;

  VisualTask(VisualFeature * visuel, InverseKinematicModel * mo);

  ~VisualTask();

  Eigen::VectorXd fonction(const Eigen::VectorXd & ) const;

  void setTransformation(const Repere & w);

  void setBaseCamera(const Repere & e);

  virtual void setState(const Eigen::VectorXd & q);

  void setModeFixation(int );

  bool isEyeInHand() const;
  
  bool isEyeToHand() const;

  Eigen::MatrixXd jacobien(const Eigen::VectorXd & q) const;

  Eigen::VectorXd calculCommande() const;

  Eigen::VectorXd calculCommande(const Eigen::VectorXd & erreur) const;

  void setReference(const Eigen::VectorXd & sStar);

  void setReference(VisualReference * prophete);

  Eigen::VectorXd getReference() const;

  double getPeriod() const;

  void setPeriod(double dt);

  bool hasChanged() const{ return false;}

  void keepVisualTrace(bool val);

};

#endif
