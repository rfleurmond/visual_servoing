#ifndef MULTI_POINT_ESTIMATOR_H_
#define MULTI_POINT_ESTIMATOR_H_

#include "feature-estimator.h"
#include "feature-multipoint.h"

/**
   @brief Class made to estimate parameters of visual target MultiPointFeature

   @author Renliw Fleurmond
 */


class MultiPointEstimator: public FeatureEstimator{

 protected:

  /// number of points
  int n; 

  /// \f$ \phi\f$, a \f$ 3n \times 3 \f$ matrix which stores information about directions
  Eigen::MatrixXd phi;

  /// \f$ \beta \f$, a \f$ 3n \times 1 \f$ vector which stores information about depths
  Eigen::VectorXd beta;

 public:

  MultiPointEstimator(VisualFeature * model, VTracker * tracker);

  ~MultiPointEstimator(){
    closeLogFile();
  }


  void computeEstimation();

  void computeSpecialEstimation(Eigen::VectorXd input);

  virtual void eraseMemory();  

  

};

#endif
