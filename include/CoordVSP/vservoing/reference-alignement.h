#ifndef REFERENCE_ALIGNEMENT_H_
#define  REFERENCE_ALIGNEMENT_H_

#include "feature-multipoint.h"
#include "alignement.h"
#include "visual-reference.h"

/**

   @brief Compute some reference based on alignement

   @deprecated Use instead daughter classes of AbstractLineFeature

   @author Renliw Fleurmond

*/

class AlignmentReference: public VisualReference{

 private:

  MultiPointFeature * cible1;

  MultiPointFeature * cible2;

  Eigen::MatrixXi assoc; 

 public:

  AlignmentReference(MultiPointFeature *,MultiPointFeature *, const Eigen::MatrixXi & );

  Eigen::VectorXd getReference() const;

};

/**

   @brief Compute some reference based on alignement

   @deprecated Use instead daughter classes of AbstractLineFeature

   @author Renliw Fleurmond

*/

class AlignmentGradient: public MiniFeatureTask{

 private:

  MultiPointFeature * cible1;

  MultiPointFeature * cible2;

  Eigen::MatrixXi assoc; 

  int dimension;

 public:

  AlignmentGradient(MultiPointFeature *,MultiPointFeature *, const Eigen::MatrixXi &);

  int getDimension() const;

  Eigen::VectorXd getValue() const;

  Eigen::MatrixXd getJacobian() const;


};


/**

   @brief Compute some reference based on alignement

   @deprecated Use instead daughter classes of AbstractLineFeature

   @author Renliw Fleurmond

*/

class AlignmentMiniTask: public MiniFeatureTask{

 private:

  MultiPointFeature * cible1;

  MultiPointFeature * cible2;

  Eigen::MatrixXi assoc; 

  int dimension;

 public:

  AlignmentMiniTask(MultiPointFeature *,MultiPointFeature *, const Eigen::MatrixXi & );

  int getDimension() const;

  Eigen::VectorXd getValue() const;

  Eigen::MatrixXd getJacobian() const;


};

#endif
