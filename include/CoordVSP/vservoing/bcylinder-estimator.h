#ifndef BCYLINDER_ESTIMATOR_H_
#define BCYLINDER_ESTIMATOR_H_

#include "icylinder-estimator.h"
#include "bounded-cylinder.h"

/**
   @brief Estimate parameters of a BoundedCylinder
   
   @author Renliw Fleurmond

 */


class BCylinderEstimator: public FeatureEstimator{

 private:

  int f_i;

  int g_i;

  int h_i;

  int j_i;

 protected:

  /// \f$ n \f$, a scalar, the number of images used for estimation
  int n;

  /// \f$ \omega_t \f$, a scalar, the number of features from the top cylinder used for estimation
  int omega_t;

  /// \f$ \omega_b \f$, a scalar, the number of features from the bottom cylinder used for estimation
  int omega_b;

  /// \f$ \phi \f$, a \f$3 \times 3 \f$ matrix
  Eigen::MatrixXd phi;

  /// \f$ \psi_b \f$, a \f$3 \times 3 \f$ matrix
  Eigen::MatrixXd psi_b;

  /// \f$ \psi_t \f$, a \f$3 \times 3 \f$ matrix
  Eigen::MatrixXd psi_t;

  /// \f$ \beta \f$, a \f$3 \times 1 \f$ vector
  Eigen::VectorXd beta;

  /// \f$ \lambda_b \f$, a \f$3 \times 1 \f$ vector
  Eigen::VectorXd lambda_b;

  /// \f$ \lambda_t \f$, a \f$3 \times 1 \f$ vector
  Eigen::VectorXd lambda_t;

  /// \f$ \gamma \f$, a \f$3 \times 1 \f$ vector
  Eigen::VectorXd gamma;

  /// \f$ \delta_b \f$, a \f$3 \times 1 \f$ vector
  Eigen::VectorXd delta_b;

  /// \f$ \delta_t \f$, a \f$3 \times 1 \f$ vector
  Eigen::VectorXd delta_t;

  /// \f$ \mu \f$, a scalar 
  double mu;

  /// \f$ \epsilon \f$, a scalar 
  double epsilon;

  /// The known radius of the cylinder
  double radius;

  /// The known length of the cylinder
  double length;

  bool knowRadius;

  bool knowLength;


  /**
     Compute the normal vector coordinates on the target frame

     corresponding to the visual cues \f$ \rho \f$, \f$ \theta\f$ in the image plane

     The coordinates of the normal vector on the camera frame has the following expression:

     \f[ N_i = \frac{1}{\sqrt{f^2 + \rho^2_i}} {\begin{bmatrix} f \cos{\theta} & f \sin{\theta} & -\rho \end{bmatrix}}^T \f]

     with \f$ f \f$ the focal length of the camera

   */

  Eigen::Vector3d getNormal(double rho, double theta) const;

 public: 

  /**
     Constructor 
     @param model the pointer on visual target model
     @param tracker a tracker which gives image of the target
						
   */
  BCylinderEstimator(VisualFeature * model, VTracker * tracker);

  ~BCylinderEstimator(){
    closeLogFile();
  }

  void setRadius(double r);

  void setLength(double l);

  /**
     Inform the estimator which information shoud be used for computeEstimation()

     The values shoud be 0 or 1 otherwise the program will stop with an assert exception
     
     if a = 1 then 
            use information about bottom right point ~ F(3) = \f$ k_{rb} \f$ 
     else then
            do not use it

     if b = 1 then 
            use information about bottom left point ~ F(7) = \f$ k_{lb} \f$ 
     else then
            do not use it
     
     if c = 1 then 
            use information about top right point ~ F(4) = \f$ k_{rt} \f$ 
     else then
            do not use it
     
     if d = 1 then 
            use information about top right point ~ F(8) = \f$ k_{lt} \f$ 
     else then
            do not use it

   */

  void selectInformation(int a, int b, int c, int d);

  void computeEstimation();

  virtual void draw(CVEcran * const vue, cv::Scalar couleur) const;

  virtual void writeLog();

};

#endif
