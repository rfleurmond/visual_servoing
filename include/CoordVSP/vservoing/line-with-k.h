#ifndef LINE_WITH_K_H_
#define LINE_WITH_K_H_

#include "line-point.h"

/**
   @brief Represent a straight line coupled with a point

   This class is a sister of class LineFeature

   @author Renliw Fleurmond

*/

class LineWithK: public AbstractLineFeature{

 private:

  LineFeature segment;
  PointFeature point;
  
 public:

  LineWithK(const Eigen::VectorXd & pA, const Eigen::VectorXd & pB, const Eigen::VectorXd & pC);
  
  int getDimensionState() const;

  int getDimensionOutput() const;

  Eigen::VectorXd getFunction(const Eigen::VectorXd & P) const;  

  Eigen::VectorXd getFunction() const;  

  Eigen::MatrixXd getNaiveInteractionMatrix(const Eigen::VectorXd & F) const;
  
  Eigen::MatrixXd getInteractionMatrix(const Eigen::VectorXd & P) const;

  Eigen::MatrixXd getInteractionMatrix() const;

  Eigen::VectorXd getError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;

  Eigen::MatrixXd getJacobianError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;
  
  Eigen::MatrixXd getJacobianReferenceError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;
  
  bool itCanBeSeen() const;

  virtual void setSituation(const Repere & r);

  virtual void attachToCamera(Camera * eye);

  void draw(CVEcran * const vue, cv::Scalar couleur) const;

  void drawFeat(const Eigen::VectorXd & value,CVEcran * const vue, cv::Scalar couleur) const;
  
};


#endif
