#ifndef VISUAL_TRACKER_H_
#define VISUAL_TRACKER_H_

#include "../vision.h"
#include "../tasks/my-clock.h"

/**

   @brief This class is a Abstract base class for tracker

   Object form this Class should provide geometrical information

   about features in images provided by the "attached" camera

   @author Renliw Fleurmond

*/

class VTracker{

 protected:

  Camera * oeil;

 public: 

  VTracker();

  virtual ~VTracker(){};
  
  /**
      Return the dimension of the output
   **/
  
  virtual int getDimensionOutput() const = 0;

  /**
     Return true if the visual features are visible from

     the "attached" camera, if the tracker is not lost

  **/

  virtual bool itCanBeSeen() const = 0;


  /**
     Return true if the tracker is not lost

  **/

  virtual bool isLost() const;

  /**

     Return the geometric features in an Eigen Vector

   **/

  virtual Eigen::VectorXd getFunction() const = 0;  


  /**

     Make the connection between the tracker

     and the camera


  **/

  virtual void attachToCamera(Camera * eye);


  /**
     Reset the tracker at the given value
     Depends of the attached camera

   **/

  virtual void findFeatures(const Eigen::VectorXd & feat);

  /**

     Return the "attached" camera

  **/

  virtual Camera * getEye() const;

    /**
    Draw the visual target seen by the "attached" camera
    int the current pose

    @see CVEcran
    @param vue a display 
    @param couleur a OpenCV color
  **/

  virtual void draw(CVEcran * const vue, cv::Scalar couleur) const = 0;

  /**
    Draw the visual features described by a vector 

    @see CVEcran
    @param value an Eigen vector which describes how a visual target is seen  
    @param vue a display 
    @param couleur a OpenCV color
   **/
  virtual void drawFeat(const Eigen::VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const = 0;


};


#endif

