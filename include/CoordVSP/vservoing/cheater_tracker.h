#ifndef ROS_CHEATER_TRACKER_H_
#define ROS_CHEATER_TRACKER_H_

#include "visualtracker.h"

class CheaterTracker: public VTracker{

 private:
 
  VTracker * slaveTracker;

  VTracker * showOffTracker;

 public: 

  CheaterTracker(VTracker * , VTracker *);

  virtual int getDimensionOutput() const;

  virtual void attachToCamera(Camera *);

  virtual bool itCanBeSeen() const;

  virtual bool isLost() const;

  virtual Eigen::VectorXd getFunction() const;  

  virtual void findFeatures(const Eigen::VectorXd & SS);  

  virtual void draw(CVEcran * const vue, cv::Scalar couleur) const;

  virtual void drawFeat(const Eigen::VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const;

};



class StupidTracker: public VTracker{

 private:

  VTracker * slaveTracker;

  int dim;

  bool perdu;

  Eigen::VectorXd F;

 
 public: 

 StupidTracker(int d, VTracker * track):
  VTracker(),
    slaveTracker(track),
    dim(d),
    perdu(false)
  {
    F = Eigen::VectorXd::Zero(dim);
  }

  virtual void attachToCamera(Camera * cm){
    oeil = cm;
    slaveTracker->attachToCamera(cm);
  }
  

  virtual int getDimensionOutput() const{
    return dim;
  }
  virtual bool itCanBeSeen() const{
    return true;
  }

  virtual bool isLost() const{
    return perdu;
  }

  void setLost(bool val){
    perdu = val;
  }

  virtual Eigen::VectorXd getFunction() const{
    return F;
  }
    
  virtual void findFeatures(const Eigen::VectorXd & SS){
    assert(SS.rows() == dim);
    F = SS;
  }

  virtual void draw(CVEcran * const vue, cv::Scalar couleur) const{
    if(isLost()){
      putText(vue->getImage(), "Tracker: Target Lost !", cv::Point(15,15), 
	      cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, couleur, 1, CV_AA);
    }
    else{
      slaveTracker->drawFeat(F,vue,couleur);
    }
    
  }

  virtual void drawFeat(const Eigen::VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const{
    slaveTracker->drawFeat(value,vue,couleur);
  }

};

#endif
