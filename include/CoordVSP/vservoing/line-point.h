#ifndef LINE_POINT_H_
#define LINE_POINT_H_

#include "feature-droite.h"

/**
   @brief This class represent a 3D straight line

   Two main differences with class DroiteFeature

   -  two 3D points are used to define the straight line

   -  This line is oriented

   @author Renliw Fleurmond

 */

class LineFeature: public AbstractLineFeature{

 protected:

  PointFeature pointA;
  PointFeature pointB;
  
 public:

  LineFeature(const Eigen::VectorXd & pA, const Eigen::VectorXd & pB);

  virtual int getDimensionState() const;

  virtual int getDimensionOutput() const;

  virtual Eigen::VectorXd getFunction(const Eigen::VectorXd & P) const;  

  virtual Eigen::VectorXd getFunction() const;  

  virtual Eigen::MatrixXd getNaiveInteractionMatrix(const Eigen::VectorXd & F) const;
  
  virtual Eigen::MatrixXd getInteractionMatrix(const Eigen::VectorXd & P) const;

  virtual Eigen::MatrixXd getInteractionMatrix() const;

  virtual Eigen::VectorXd getError(const Eigen::VectorXd & S,const Eigen::VectorXd & SS) const;

  virtual Eigen::MatrixXd getJacobianError(const Eigen::VectorXd & S,const Eigen::VectorXd & SS) const;
  
  virtual Eigen::MatrixXd getJacobianReferenceError(const Eigen::VectorXd & S,const Eigen::VectorXd & SS) const;
  
  virtual bool itCanBeSeen() const;

  virtual void setSituation(const Repere & r);

  virtual void attachToCamera(Camera * eye);

  virtual void draw(CVEcran * const vue, cv::Scalar couleur) const;

  virtual void drawFeat(const Eigen::VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const;

  PointFeature getFirstPoint() const;

  PointFeature getSecondPoint() const;

};


#endif
