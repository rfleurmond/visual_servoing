#ifndef VISUAL_FEATURES_H_
#define VISUAL_FEATURES_H_

#include "visualtracker.h"

/**
   This class inherits from the class VTracker

   @brief Base class for all visual features

   This class will be used for visual servoing

   It provides some services. In particular:

   S <- The value of the geometrical features extracted from the images

   E <- The error between two vector of visual features

   L <- The corresponding interaction matrix

   This class implement a visual target in a 3D world

   S and L are function of the pose of the target with repect to 

   the "attached" camera.

   @author Renliw Fleurmond

**/


class VisualFeature : public VTracker{

 protected:

  /// Pose of visual target

  Repere frame;

  /// What interaction matrix will be used ?

  int strategy;

  /// Interaction Matrix at the wanted pose 
  /// With respect to the camera

  Eigen::MatrixXd l_star;

  bool hasLStar;

 public:

  static const int NDEF = 0;
  static const int STAR = 1;
  static const int PINV = 2;
  static const int MIXE = 3;

  VisualFeature();

  /**
     Return the dimension of parameters used to describe
     the target in the 3d world, without pose parameters
   **/
  virtual int getDimensionState() const = 0;


  /**
     Return the value of the visual features
     expected for a visual target of same kind
     in the same pose with respect to the wold reference frame
     and seen by the same camera
     @param P a vector of parameters which describe completely the target
  **/

  virtual Eigen::VectorXd getFunction(const Eigen::VectorXd & P) const = 0;

  virtual Eigen::VectorXd getFunction() const = 0;


  /**
     Return a rough and naive interaction matrix corresponding to the
     given vector of the visual features
     with a depth setted to 1 meter
     @param F is the vector of visual cues
  **/

  virtual Eigen::MatrixXd getNaiveInteractionMatrix(const Eigen::VectorXd & F) const = 0;
   

  /**
     Return the interaction matrix of the visual features
     expected for a visual target of same kind
     in the same pose with respect to the wold reference frame
     and seen by the same camera
     @param P a vector of parameters which describe completely the target
  **/

  virtual Eigen::MatrixXd getInteractionMatrix(const Eigen::VectorXd & P) const = 0;
   
  /**
     Return the current interaction matrix
  **/

  virtual Eigen::MatrixXd getInteractionMatrix() const =0;

  /**
     Set the pose of the visual target in  the wold reference frame
   **/
  virtual void setSituation(const Repere & r);

  /**
     Set the wanted Interaction Matrix
   **/
  void setReferenceInteractionMatrix(const Eigen::MatrixXd & lstar);

  /**
     Return the pose of the visual target with respect to the world reference frame
   **/
  virtual Repere getSituation() const;

  /**
    Set a way to compute the moore penrose inverse of the interaction matrix

    PINV: 
     \f$\hat{L}^+ = {L}^+ \f$
     
     STAR: 
     \f$\hat{L}^+ = {L^*}^+ \f$
     
     MIXE:
     \f$\hat{L}^+ = {[0.5L + 0.5 L^*]}^+ \f$
   **/
  void setStrategy(int st);

  /**
     Return the moore penrose inverse of the interaction matrix

     The result depend of the chosen strategy
     and the availability of the different matrices

     PINV: 
     \f$\hat{L}^+ = {L}^+ \f$
     
     STAR: 
     \f$\hat{L}^+ = {L^*}^+ \f$
     
     MIXE:
     \f$\hat{L}^+ = {[0.5L + 0.5 L^*]}^+ \f$
     
     @return \f$\hat{L}^+\f$
   **/
  Eigen::MatrixXd getInverseInteractionMatrix() const;

  /**
     Compute the error betwenn the current S and a given Sstar
   **/
  virtual Eigen::VectorXd getErrorBetweenMeAnd(const Eigen::VectorXd & SS) const;


  /**
     Compute the error betwenn the given S and a given Sstar
   **/
  virtual Eigen::VectorXd getError(const Eigen::VectorXd & S, const Eigen::VectorXd & Sstar) const = 0;


  /**
     Compute the jacobian of the error betwenn the current S and a given SS
     wih respect to the current S
   **/
  virtual Eigen::MatrixXd getJacobianError(const Eigen::VectorXd & SS) const;

  /**
     Compute the jacobian of the error betwenn the current S and a given SS
     wih respect to the current S
  **/
  virtual Eigen::MatrixXd getJacobianReferenceError(const Eigen::VectorXd & SS) const;

  /**
     Compute the jacobian of the error betwenn the given S and a given SS
     wih respect to the current S
   **/
  virtual Eigen::MatrixXd getJacobianError(const Eigen::VectorXd & S, const Eigen::VectorXd & SS) const;

  /**
     Compute the jacobian of the error betwenn the given S and a given SS
     wih respect to the current S
  **/
  virtual Eigen::MatrixXd getJacobianReferenceError(const Eigen::VectorXd & S, const Eigen::VectorXd & SS) const;

  /**
    Compute the desired Kinematic torsor of the camera from
    @param lambda a proportional gain
    @param erreur an Eigen vector for the error
    
    @return \f$ T = - \lambda \times \hat{L}^+ \times E\f$
   **/
  Eigen::VectorXd getTorseurCinematique(double lambda, const Eigen::VectorXd & erreur) const;

  /**
     To inform the visual features that the value has been recorded
     Some visual feature class will make a specific compuation
     @see FeatureEstimator
  **/

  virtual void useTracker(){
    // Do nothing 
  }

  virtual void draw(CVEcran * const vue, cv::Scalar couleur) const;

  virtual void drawFeat(const Eigen::VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const = 0;

  
};


#endif
