#ifndef VISIBILITY_CONSTRAINT_TASK_H_
#define VISIBILITY_CONSTRAINT_TASK_H_

#include "visual-reference.h"

/**

   @brief Implements visual bounds for visual features
 
   @author Renliw Fleurmond
 */


struct bounds{

bounds():
  sMin(Eigen::VectorXd::Zero(1)),
    sMax(Eigen::VectorXd::Ones(1)),
    acti(Eigen::MatrixXd::Identity(1,1))
  {

  }
  
  Eigen::VectorXd sMin;

  Eigen::VectorXd sMax;

  Eigen::MatrixXd acti;

};

/**
   @brief RelativeTask which translate constraint on visual features 
 
   @author Renliw Fleurmond
 */

class ConstraintFeatures : public RelativeTask{

 private:

  double coeffMin;

  double coeffMax;

  double alpha;

  double max;

  int dimension;

  std::map<VisualFeature *, bounds> carte;
  
  std::list<VisualFeature *> liste;

  Eigen::VectorXd getValue(VisualFeature * f) const;


 public:

  ConstraintFeatures();

  ConstraintFeatures(VisualFeature * f, 
		     const Eigen::VectorXd & qmin, 
		     const Eigen::VectorXd & qmax);

  void addFeatures(VisualFeature * f, 
		   const Eigen::VectorXd & qmin, 
		   const Eigen::VectorXd & qmax);
  
  void addFeatures(VisualFeature * f, 
		   const Eigen::VectorXd & qmin, 
		   const Eigen::VectorXd & qmax, 
		   const Eigen::MatrixXd & a);
  
  virtual int getDimension() const;

  virtual Eigen::VectorXd getValue() const;

  virtual bool dependOf(VisualFeature * feat) const;

  virtual Eigen::MatrixXd getJacobian(VisualFeature *) const;

  void setCoeffMax(double);

  void setCoeffMin(double);
  
  void setAlpha(double);

  void setMax(double);


};

#endif
