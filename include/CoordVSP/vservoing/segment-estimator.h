#ifndef SEGMENT_ESTIMATOR_H_
#define SEGMENT_ESTIMATOR_H_

#include "feature-estimator.h"
#include "segment.h"

/**
   @brief Estimate the paramaters which define a Segment

   @author Renliw Fleurmond

*/

class SegmentEstimator: public FeatureEstimator{

 protected:

  /// \f$ \phi_1\f$, a \f$3 \times 3 \f$ matrice which stores information about directions
  Eigen::MatrixXd phi1;

  /// \f$ \beta_1\f$, a \f$3 \times 1 \f$ vector which stores information about depth
  Eigen::VectorXd beta1;

  /// \f$ \phi_2\f$, a \f$3 \times 3 \f$ matrice which stores information about directions
  Eigen::MatrixXd phi2;

  /// \f$ \beta_2\f$, a \f$3 \times 1 \f$ vector which stores information about depth
  Eigen::VectorXd beta2;

 public: 

  /**
     Constructor 
     @param model the pointer on visual target model
     @param tracker a tracker which gives image of the target

  */

  SegmentEstimator(VisualFeature * model, VTracker * tracker);

  ~SegmentEstimator(){
    closeLogFile();
  }

  void computeEstimation();

};

#endif
