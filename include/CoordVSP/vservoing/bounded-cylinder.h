#ifndef BOUNDED_CYLINDER_H_
#define BOUNDED_CYLINDER_H_

#include "segment.h"

/**

   @brief Represent a cylinder with a definite length

   This class is drawed in camera with two segments
   
   @author Renliw Fleurmond

*/

class BoundedCylinder: public VisualFeature{

 private:

  double rayon;
  
  double length;

  Eigen::Vector3d P;

  Eigen::Vector3d U;

  Eigen::Vector3d TOP;

  Eigen::Vector3d BOT;

  void init(double r, Eigen::Vector3d B, Eigen::Vector3d T);

  
 public:

  BoundedCylinder(double l, double r);

  BoundedCylinder(double l, double r, Eigen::Vector3d U);

  BoundedCylinder(double r, Eigen::Vector3d B, Eigen::Vector3d T);

  int getDimensionState() const;

  int getDimensionOutput() const;

  Eigen::VectorXd getFunction(const Eigen::VectorXd & P) const;  
  
  Eigen::VectorXd getFunction(double r, Eigen::Vector3d B, Eigen::Vector3d T) const;  
    
  Eigen::VectorXd getFunction() const;  

  Eigen::MatrixXd getNaiveInteractionMatrix(const Eigen::VectorXd & F) const;
  
  Eigen::MatrixXd getInteractionMatrix(const Eigen::VectorXd & param) const;

  Eigen::MatrixXd getInteractionMatrix(double r, Eigen::Vector3d B, Eigen::Vector3d T) const;

  Eigen::MatrixXd getInteractionMatrix() const;

  Eigen::VectorXd getError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;

  Eigen::MatrixXd getJacobianError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;
  
  Eigen::MatrixXd getJacobianReferenceError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;
  
  bool itCanBeSeen() const;

  virtual void setSituation(const Repere & r);

  virtual void attachToCamera(Camera * eye);

  void draw(CVEcran * const vue, cv::Scalar couleur) const;

  void drawFeat(const Eigen::VectorXd & value,CVEcran * const vue, cv::Scalar couleur) const;
  
};


#endif
