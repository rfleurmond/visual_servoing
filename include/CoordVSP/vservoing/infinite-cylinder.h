 #ifndef INFINITE_CYLINDER_H_
#define INFINITE_CYLINDER_H_

#include "line-point.h"

/**
   @brief Visual representation of a infinite-length cylinder

   This class is seen by the cameras as two straight lines

   @author Renliw Fleurmond

 */

class Cylinder: public VisualFeature{

 private:

  double rayon;

  Eigen::Vector3d P;

  Eigen::Vector3d U;

 public:

  Cylinder(double r);

  Cylinder(double r, Eigen::Vector3d U);

  Cylinder(double r, Eigen::Vector3d U, Eigen::Vector3d P);

  int getDimensionState() const;

  int getDimensionOutput() const;

  Eigen::VectorXd getFunction(const Eigen::VectorXd & P) const;  
  
  Eigen::VectorXd getFunction(double rr, Eigen::Vector3d UU, Eigen::Vector3d PP) const;  
    
  Eigen::VectorXd getFunction() const;  

  Eigen::MatrixXd getNaiveInteractionMatrix(const Eigen::VectorXd & F) const;
  
  Eigen::MatrixXd getInteractionMatrix(const Eigen::VectorXd & param) const;

  Eigen::MatrixXd getInteractionMatrix(double rr, Eigen::Vector3d UU, Eigen::Vector3d PP) const;

  Eigen::MatrixXd getInteractionMatrix() const;

  Eigen::VectorXd getError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;

  Eigen::MatrixXd getJacobianError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;
  
  Eigen::MatrixXd getJacobianReferenceError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;
  
  bool itCanBeSeen() const;

  virtual void setSituation(const Repere & r);

  virtual void attachToCamera(Camera * eye);

  void draw(CVEcran * const vue, cv::Scalar couleur) const;

  void drawFeat(const Eigen::VectorXd & value,CVEcran * const vue, cv::Scalar couleur) const;
  
};


#endif
