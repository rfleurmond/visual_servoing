#ifndef FEATURE_DROITE_H_
#define FEATURE_DROITE_H_

#include "../geometrie.h"
#include "feature-point.h"
#include "alignement.h"

/**
   @brief Abstract class which provides some methods useful to many visual features classes

   which use line features or derived features

   @author Renliw Fleurmond
   
 **/

class AbstractLineFeature : public VisualFeature{

 public :

  AbstractLineFeature();

  /**
     Draw a line on a OpenCv display
     @param oeil the pointor to the Camera
     @param value a Eigen vector which describe the line with two parameters \f$\rho\f$ and \f$\theta\f$
     @param vue the display
     @param couleur a OpenCv color
   **/

  static void dessinerDroite(Camera * oeil, 
			     const Eigen::VectorXd & value, 
			     CVEcran * const vue,
			     cv::Scalar couleur);

  /**
     Check if the sign of the parameters of the line are correct

     otherwise correct them
   **/

  static Eigen::Vector2d checkParameters(double & rho,double & teta);

  static Eigen::Vector2d computeError(Eigen::Vector2d real,Eigen::Vector2d wanted);

  static Eigen::MatrixXd computeJError(Eigen::Vector2d real,Eigen::Vector2d wanted);

  static Eigen::MatrixXd computeJRefError(Eigen::Vector2d real,Eigen::Vector2d wanted);

  static Eigen::Vector2d computeRhoTeta(Eigen::Vector2d A, Eigen::Vector2d B);

  static Eigen::MatrixXd computeJacobianRhoTeta(Eigen::Vector2d A, Eigen::Vector2d B);

  
  
};

/**

   @brief Visual target of a 3D straight line

   @author Renliw Fleurmnod

*/

class DroiteFeature: public AbstractLineFeature{

 private:

  Droite droite;

  Eigen::VectorXd getFunction(Droite d) const;  

  Eigen::MatrixXd getInteractionMatrix(Droite d) const;
  
  
 public:

  DroiteFeature(const Droite & d);

  int getDimensionState() const;

  int getDimensionOutput() const;

  Eigen::VectorXd getFunction(const Eigen::VectorXd & P) const;

  Eigen::VectorXd getFunction() const;

  Eigen::MatrixXd getNaiveInteractionMatrix(const Eigen::VectorXd & F) const;
  
  Eigen::MatrixXd getInteractionMatrix(const Eigen::VectorXd & P) const;

  Eigen::MatrixXd getInteractionMatrix() const;

  Eigen::VectorXd getError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;

  Eigen::MatrixXd getJacobianError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;
  
  Eigen::MatrixXd getJacobianReferenceError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;
  
  bool itCanBeSeen() const;

  void drawFeat(const Eigen::VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const;
  

};


#endif
