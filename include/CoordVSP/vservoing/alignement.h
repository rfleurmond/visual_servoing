#ifndef ALIGNEMENT_H_
#define ALIGNEMENT_H_

#include "../tools.h"

/**
   @brief Static class to compute some alignement stuff

   @author Renliw Fleurmond

 */

class Alignement{
 
 public:

  static Eigen::VectorXd getReference(Eigen::Vector2d A,Eigen::Vector2d B,Eigen::Vector2d C);

  static Eigen::VectorXd getReference(Eigen::Vector2d A,Eigen::Vector2d B,Eigen::Vector2d C, Eigen::Vector2d D);

  static Eigen::VectorXd getPerpendicularReference(Eigen::Vector2d A,Eigen::Vector2d B,Eigen::Vector2d C, Eigen::Vector2d D);


};

#endif
