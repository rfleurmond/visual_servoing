#ifndef MULTI_FEATURE_H_
#define MULTI_FEATURE_H_

#include "visualfeature.h"

/**
  @brief This class is designed to make a collection of visual features acts like a single VisualFeature

  @author Renliw Fleurmond
   
 */

class MultiFeature: public VisualFeature{

 private:

  std::list<VisualFeature *> liste;

 public:

  MultiFeature(VisualFeature * f);
  
  int getDimensionState() const;

  int getDimensionOutput() const;

  Eigen::VectorXd getFunction(const Eigen::VectorXd & P) const;

  Eigen::VectorXd getFunction() const;  

  Eigen::MatrixXd getNaiveInteractionMatrix(const Eigen::VectorXd & F) const;
  
  Eigen::MatrixXd getInteractionMatrix(const Eigen::VectorXd & P) const;

  Eigen::MatrixXd getInteractionMatrix() const;

  Eigen::VectorXd getError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;

  Eigen::MatrixXd getJacobianError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;
  
  Eigen::MatrixXd getJacobianReferenceError(const Eigen::VectorXd &, const Eigen::VectorXd &) const;
  
  bool itCanBeSeen() const;

  void addFeature(VisualFeature * f);

  void setSituation(const Repere & r);

  void attachToCamera(Camera * eye);

  void useTracker();

  void draw(CVEcran * const vue, cv::Scalar couleur) const;

  void drawFeat(const Eigen::VectorXd & value,CVEcran * const vue, cv::Scalar couleur) const;

};

#endif
