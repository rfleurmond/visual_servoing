#ifndef VSERVOING_H_
#define VSERVOING_H_

#include "vservoing/flying-system.h"
#include "vservoing/reference-alignement.h"
#include "vservoing/robot-system.h"
#include "vservoing/multi-feature.h"
#include "vservoing/reference-droite.h"
#include "vservoing/noisy-feature.h"
#include "vservoing/many-arm-holder-task.h"
#include "vservoing/point-estimator.h"
#include "vservoing/multi-point-estimator.h"
#include "vservoing/line-point-estimator.h"
#include "vservoing/line-with-k-estimator.h"
#include "vservoing/segment-estimator.h"
#include "vservoing/visibility-constraint.h"
#include "vservoing/virtual-snuffer.h"
#include "vservoing/bcylinder-estimator.h"
#include "vservoing/lk-color-tracker.h"
#include "vservoing/short-cylinder-tracker.h"
#include "vservoing/cheater_tracker.h"
#include "vservoing/track-points.h"

#endif
