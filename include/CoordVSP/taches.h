#ifndef TASKS_H_
#define TASKS_H_

#include "tasks/task-controller.h"
#include "tasks/q-system.h"
#include "tasks/noisy-system.h"
#include "tasks/dual-arm-system.h"
#include "tasks/qtask.h"
#include "tasks/qbounded-task.h"
#include "tasks/avoid-task.h"
#include "tasks/qpoint-task.h"
#include "tasks/part-task.h"
#include "tasks/co-task.h"
#include "tasks/egality-task.h"
#include "tasks/optima-priority-task.h"
#include "tasks/my-optima-priority-task.h"
#include "tasks/multi-task.h"
#include "tasks/sequency-task.h"
#include "tasks/my-clock.h"

#endif
