/*
 * Estimateur.h
 *  Cette classe virtuelle (donc destinee a etre specifiee)
 *
 *   liste les methodes minimales d'un estimateur
 *
 *  Created on: May 22, 2012
 *      @author Renliw Fleurmond
 */

#ifndef ESTIMATEUR_H_
#define ESTIMATEUR_H_

#include "linear-system.h"
#include "nonlinear-system.h"
/**

   @brief Base class of some terative estimators

   @author Renliw Fleurmond

   @deprecated This class could be deleted in future versions

 */



class Estimateur {

public:
	Estimateur();
	virtual ~Estimateur();
	virtual void computeEstimation(const LSystem &sys,Eigen::VectorXd Y) =0;
	virtual void computeEstimation(const NLSystem &sys,Eigen::VectorXd Y) =0;
	virtual Eigen::VectorXd getStateEstimation() const =0;
	virtual Eigen::VectorXd getObservEstimation() const =0;
	virtual void updateSystem(Systeme &) =0;

};

#endif /* ESTIMATEUR_H_ */
