/*
 * Systeme.h
 *
 *  Cette classe presente un modele de systeme que nous devrons estimer
 *
 *  avec  des variables d'etat , une fonction d'evolution et une fonction d' observation
 *
 *  elle est destinee a etre surchargee ou heritee par d'autres classes
 *
 *  Created on: May 22, 2012
 *      @author Renliw Fleurmond
 */

#ifndef SYSTEME_H_
#define SYSTEME_H_

#include "../tools.h"

/**

   @brief Base class of dynamic system for estimation

   @author Renliw Fleurmond

   @deprecated This class could be deleted in future versions

 */


class Systeme {

 protected:
  Eigen::VectorXd X;
  Eigen::VectorXd Y;
  double dt;
  double heure;
  int N;
  int M;

 public:
  Systeme();
  Systeme(Eigen::VectorXd X);
  virtual ~Systeme();

  double getDt() const;
  void setDt(double dt);

  virtual Eigen::VectorXd getX() const ;
  virtual Eigen::VectorXd getY() const ;
  virtual void setX(Eigen::VectorXd x);
  virtual void setY(Eigen::VectorXd y);

  virtual int getNStates() const;
  virtual int getMStates() const;
  virtual Eigen::VectorXd getEvolution() const = 0;
  virtual Eigen::VectorXd getObservation() const = 0;
  virtual void evoluate();

};

#endif /* SYSTEME_H_ */
