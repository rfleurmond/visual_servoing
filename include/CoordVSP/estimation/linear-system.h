/*
 * LSystem.h
 *
 * Cette classe implemente un systeme linaire simple
 *
 * Elle derive de la classe Systeme
 *
 * Le systeme est de la forme
 *
 * dX = A . X + B . U
 *
 * Y = C . X + D . U
 *
 * il peut etre discret ou non
 *
 *  Created on: May 22, 2012
 *      @author Renliw Fleurmond
 */

#ifndef LSYSTEM_H_
#define LSYSTEM_H_

#include "systeme.h"

/**

   @brief Base class of dynamic linear system for estimation

   @author Renliw Fleurmond

   @deprecated This class could be deleted in future versions

 */



class LSystem : public Systeme{

 protected:

  Eigen::VectorXd U;
  Eigen::MatrixXd A;
  Eigen::MatrixXd B;
  Eigen::MatrixXd C;
  Eigen::MatrixXd D;


 public:
  LSystem();
  LSystem(Eigen::VectorXd X);
  LSystem(Eigen::MatrixXd A,Eigen::MatrixXd C);
  LSystem(Eigen::MatrixXd A,Eigen::MatrixXd C,Eigen::VectorXd X);

  virtual ~LSystem();

  void setX(Eigen::VectorXd x);

  void setY(Eigen::VectorXd y);

  Eigen::MatrixXd getA() const;
  void setA(Eigen::MatrixXd a);

  Eigen::MatrixXd getB() const;
  void setB(Eigen::MatrixXd b);
  
  Eigen::MatrixXd getC() const;
  void setC(Eigen::MatrixXd c);

  Eigen::MatrixXd getD() const;
  void setD(Eigen::MatrixXd d);

  virtual Eigen::VectorXd getEvolution() const;
  virtual Eigen::VectorXd getObservation() const;
  
  void evoluate();

};

#endif /* LSYSTEM_H_ */
