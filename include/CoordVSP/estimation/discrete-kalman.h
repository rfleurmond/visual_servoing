/*
 * Kalman.h
 *
 * Cette classe impl'emente une estimateur de Kalman dans le cas discret
 *  et derive de la classe Estimateur voir Estimateur.h
 *
 * Trois methodes sont proposees
 *
 * - le filtre des kalman pour les systemes lineaires voir LSystem.h
 * - EKF (Extended Kalman filter) pour les systemes non lineaires NLSystem.h
 * - UKF (Unscented Kalman Filter) pour les systemes non lineaires NLSystem.h
 *
 * Pour tous on fait l' hypothese de bruits additifs de moyenne nulle (gaussiens, blancs)
 * dans les modele d'evolution et d' observation
 *
 *
 *  Created on: May 22, 2012
 *      @author Renliw Fleurmond
 */

#ifndef DISCRETEKALMAN_H_
#define DISCRETEKALMAN_H_

#include "estimateur.h"

/**

   @brief Tool kit of discrete kalman filters

   @author Renliw Fleurmond

   @deprecated This class could be deleted in future versions

 */

class DiscreteKalman : public Estimateur{

 private:

  int methode;

  int n;
  int m;

  Eigen::VectorXd X;
  Eigen::VectorXd U;
  Eigen::VectorXd YE;
  Eigen::VectorXd EY;
  Eigen::VectorXd XE;

  Eigen::MatrixXd Pxx;
  Eigen::MatrixXd Pxy;
  Eigen::MatrixXd Pyy;
  Eigen::MatrixXd K;
  Eigen::MatrixXd selection;

  bool discrimination;

  Eigen::MatrixXd BE;
  Eigen::MatrixXd BO;
  Eigen::MatrixXd BU;

  Eigen::MatrixXd A;
  Eigen::MatrixXd B;
  Eigen::MatrixXd C;
  Eigen::MatrixXd D;


  double alpha;
  double beta;
  double kappa;
  double gamma;
  double * WC;
  double * WM;

  void computeKalmanEstimation(const LSystem & sys,Eigen::VectorXd Y);
  void computeEKF_Estimation(const NLSystem & sys,Eigen::VectorXd Y);
  void computeUKFAdditiveEstimation(const NLSystem & sys,Eigen::VectorXd Y);
  void correction(bool acception, Eigen::VectorXd ey, Eigen::MatrixXd J);
  void correction(bool acception, Eigen::VectorXd ey);
  void calculUKFParametres();

  bool coefCalcul;

 public:

  static const int KALMAN  = 0; // Filtre de Kalman pour les systeme lineaires
  static const int EKF     = 1; // Extended Kalman Filter with additive noise
  static const int UKF_ADD = 2; // Unscented Kalman Filter with additive noise


  DiscreteKalman();
  virtual ~DiscreteKalman();
  void setMethode(int met);
  void setNumberStates(int n,int m);
  void setUKFparameters(double alpha,double beta,double kappa);
  void setSelection(Eigen::MatrixXd elus);
  void enableDiscrimination(bool choix);

  void setStateVariance(Eigen::MatrixXd P);
  void setNoiseEvolVariance(Eigen::MatrixXd BE);
  void setNoiseControlVariance(Eigen::MatrixXd BU);
  void setNoiseMeasureVariance(Eigen::MatrixXd BO);
  
  void computeEstimation(const LSystem & sys,Eigen::VectorXd Y);
  void computeEstimation(const NLSystem & sys,Eigen::VectorXd Y);
  Eigen::VectorXd getStateEstimation() const;
  Eigen::VectorXd getObservEstimation() const;
  Eigen::VectorXd getErrorMeasures() const;
  void updateSystem(Systeme &);
  Eigen::MatrixXd getPxx() const ;
  Eigen::MatrixXd getPxy() const ;
  Eigen::MatrixXd getPyy() const ;



};

#endif /* DISCRETEKALMAN_H_ */
