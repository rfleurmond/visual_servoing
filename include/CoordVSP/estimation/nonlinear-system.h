/*
 * NLSystem.h
 *
 *  Celle classe implemente un systeme non lineaire (automatique) destinee a etre estimeee
 *
 *  Ce systeme est de la forme
 *
 *  dX = f(X)
 *  Y  = g(X)
 *
 *  Cette classe est destinee a etre specialisee
 *
 *  Created on: May 22, 2012
 *      @author Renliw Fleurmond
 */

#ifndef NLSYSTEM_H_
#define NLSYSTEM_H_

#include "systeme.h"

/**

   @brief Base class of dynamic non linear system for estimation

   @author Renliw Fleurmond

   @deprecated This class could be deleted in future versions

 */


class NLSystem  : public Systeme{

 protected:

  int nO ;

  Eigen::VectorXd U;
  

 public:
  NLSystem();
  NLSystem(Eigen::VectorXd X);
  virtual ~NLSystem();

  virtual void setU(Eigen::VectorXd u);

  virtual Eigen::VectorXd getU() const;


  virtual Eigen::VectorXd getEvolution(Eigen::VectorXd X, Eigen::VectorXd U) const;
  virtual Eigen::MatrixXd getJacobianEvolutionX(Eigen::VectorXd X, Eigen::VectorXd U) const;
  virtual Eigen::MatrixXd getJacobianEvolutionU(Eigen::VectorXd X, Eigen::VectorXd U) const;
  
  virtual Eigen::VectorXd getObservation(Eigen::VectorXd X, Eigen::VectorXd U) const;
  virtual Eigen::MatrixXd getJacobianObservationX(Eigen::VectorXd X, Eigen::VectorXd U) const;
  virtual Eigen::MatrixXd getJacobianObservationU(Eigen::VectorXd X, Eigen::VectorXd U) const;

  virtual Eigen::VectorXd getEvolution() const;
  virtual Eigen::MatrixXd getJacobianEvolutionX() const;
  virtual Eigen::MatrixXd getJacobianEvolutionU() const;

  virtual Eigen::VectorXd getObservation() const;
  virtual Eigen::MatrixXd getJacobianObservationX() const;
  virtual Eigen::MatrixXd getJacobianObservationU() const;

  virtual bool hasNonTrivialError() const;
  virtual Eigen::VectorXd getError(Eigen::VectorXd X, Eigen::VectorXd U, Eigen::VectorXd Y) const;
  virtual Eigen::MatrixXd getJacobianError(Eigen::VectorXd X, Eigen::VectorXd U, Eigen::VectorXd Y) const;


  void evoluate();

};

#endif /* NLSYSTEM_H_ */
