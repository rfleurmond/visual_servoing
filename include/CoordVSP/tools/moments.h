#ifndef MOMENT_H_
#define MOMENT_H_

#include "../lib_eigen.h"
#include <list>

/**
   @brief A static class to compute geometric moments

   @author Renliw Fleurmond

   @deprecated This class can be deleted in future versions
 */


class Moment{

 public:

  static Eigen::VectorXd getMomentOrdre1(const std::list<Eigen::VectorXd> & liste, int dim);

  static Eigen::MatrixXd getMomentOrdre2(const std::list<Eigen::VectorXd> & liste, int dim);

};

#endif

