/**
 *  @brief Represent a 3d rotation 

 * VectRotation.h
 * Cette classe represente une rotation dans l'espace de dimension 3
 * On sait que l' espace des rotations dans cet espace est aussi de dimension 3
 * donc un groupe de trois valeurs suffissent a definir toutes les rotations possibles
 *
 * On peut definir une rotation (transformation corps rigide)
 * comme une rotation d'un angle donne autour d'un vecteur donnant le sens et la direction de la rotation
 *
 * Elle utilise en interne la classe AngleAxis de la libraire Eigen qui represente la meme rotation
 *
 * certaines methodes font appel directement a des methodes de la classe AngleAxis
 *
 * Les angles de rotations sont exprimes en radians
 *
 *  Created on: May 24, 2012
 *  @author Renliw Fleurmond
 */

#ifndef VECTROTATION_H_
#define VECTROTATION_H_

#include "outils.h"

class VectRotation {

 private:
  
  double angle;
  
  Eigen::Vector3d direction;

  Eigen::AngleAxisd rotation;

  Eigen::Matrix3d matrice;
  
  /** Methode appellee dans le constructeur
   * pour verifier tous les parametres
   * surtout l'angle de rotation qui doit etre positif ou nul
   *
   */
  void checkRotation();

 public:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  
  static const double EPSILON;

  static const double QUASIZERO;

  /**
   * Constructeur par defaut
   * l' angle et l'axe de rotation sont nuls
   *
   */
  VectRotation();
  /**
   * Constructeur qui prend en parametre
   * l'angle de rotation
   * et l'axe autour duquel s'effectue la rotation
   * cet axe doit etre unitaire,
   * c'est a dire la norme de ce vecteur doit etre egal a un
   *
   * Si cette derniere condition n'est pas respectee,
   * le constructeur normalise l'axe mais ne modifie pas l'angle
   *
   * l'angle de rotation est toujours pris positif dans le constructeur
   * si cette condition n'est pas respecte,
   * dans le constructeur on multiplie par -1 l'angle et l'axe de rotation
   *
   */
  VectRotation(double angle,Eigen::Vector3d axe);
  /**
   * Constructeur prenant en parametre
   * un AngleAxis de la librairie Eigen
   *
   */  

  VectRotation(Eigen::AngleAxisd brut);
  /**
   * Constructeur prenant en parametre une vecteur de norme quelquonque
   * la norme du vecteur sera l'angle de rotation toujours positif
   * et le vecteur normalise la direction de la rotation
   */  
  VectRotation(Eigen::Vector3d brut);
  
  /**
   * Constructeur prenant en parametre un Quaternion
   */
  VectRotation(Eigen::Quaterniond brut);

  /**
   * Constructeur prenant en parametre une matrice supposee etre
   * une matrice de rotation:
   * son determinant est egal a 1
   * elle est orthogonale, sa transposee est aussi son inverse
   *
   */

  VectRotation(Eigen::Matrix3d rodrigues);
  
  /**
   *Contructeur prenant en entree trois doubles
   */

  VectRotation(double x,double y,double z);

  /**
   * Cette methode retourne la matrice de rotation correspondant a la rotation
   *  et utilise la methode matrix() de la classe AngleAxis de la librairie Eigen
   */
  Eigen::Matrix3d getMatrix() const;

  /**
   * Retourne l' angle de rotation
   */
  double getAngle() const;

  /**
   * Retourne le vecteur passe en entree ayant subi une rotation
   *
   */
  Eigen::Vector3d rotate(const Eigen::Vector3d & v) const;

  /**
   * Retourne le vecteur v en entree ayant subi une rotation
   * dans le sens inverse
   */
  Eigen::Vector3d deRotate(const Eigen::Vector3d & v) const;

  /**
   * Retourne l'axe autour duquel s'effectue la rotation
   */
  Eigen::Vector3d getAxis() const;

  /**
   * Retourne le vecteur rotation,
   * dont la norme est egal a l'angle de rotation
   * dont la direction est celle de l'axe de rotation
   */  
  Eigen::Vector3d getVector() const;

  /**
   * Retourne un objet AngleAxis correspondant a la rotation
   *
   */  
  Eigen::AngleAxisd getAngleAxisd() const;

  /**
   * Retourne la jacobienne de la combinaison de cette rotation precedee de l'autre decrite par le Vecteur
   */  
  Eigen::MatrixXd getJacobianCombinaison(const Eigen::Vector3d & other) const;

  /**
   * Retourne la jacobienne de la combinaison de celle rotation precedee de l'autre decrite par l'objet AngleAxis
   *
   */ 
  Eigen::MatrixXd getJacobianCombinaison( const Eigen::AngleAxisd & other) const;
  /**
   * Retourne la jacobienne de la combinaison de la rotation de l'objet courant avec un autre objet du meme type
   *
   * Ici a ete fait le choix d'exprimer une rotation comme un "vecteur" (direction,sens,amplitude)
   * bien que par exemple l'addition ne soit pas definie
   * La combinaison de deux rotations est equivalent a une addition si seulement les deux "vecteurs" sont colineaires.
   *
   * Cette jacobienne est donc une matrice de 3 x 6
   * les trois premiere colonnes pour les parametres de l'objet courant
   * les trois dernieres colonnes pour ceux de l'objet passe en entree
   *
   * On rappelle que la combinaison de deux rotations n'est pas commutative
   */ 
  Eigen::MatrixXd getJacobianCombinaison( const VectRotation & other) const;

  /**
   * Retourne la jacobienne de la rotation du vecteur v en entree
   * en fonction des parametres de l' objet courant c'est donc une matrice 3 x 3
   *
   */ 
  Eigen::Matrix3d getJacobianRotation( const Eigen::Vector3d & other) const;
  /**
   * Retourne la matrice antisymetrique appartenant a l'espace tangeant
   *  aux matrices de rotation correspondant la rotation exprimee par le vecteur w
   *  passe en entree
   *
   *  	      |x|

   *  si  w = |y|

   *          |z|
   *
   *  Cette methode retourne:
   *
   *  | 0 -z  y|

   *  | z  0 -x|

   *  |-y  x  0|
   *
   *  C'est une convention arbitraire, (on aurait pu changer de signe),
   *  liee a la definition du produit vectoriel
   */

  static Eigen::Matrix3d getSkewMatrix(const Eigen::Vector3d & w);

  /**
   * Retourne la matrice antisymetrique correspondant a la rotation
   */  
  Eigen::Matrix3d getSkewMatrix() const;

  /**
   *
   *  Cette methode retourne une matrice de dimension 3x3
   *  qui donne la derivee de notre vecteur d'angle par rapport a la vitesse angulaire
   *  
   *    d(theta * u)/dt = dP/dt = Lw * w
   *    
   *    w est la vitesse angulaire
   *    Lw est la matrice retournee par cette methode
   *  
   *    Cette methode utilise les resultats de Ezio Malis dans sa these en 1998 a l' annexe A:
   *
   */

  Eigen::Matrix3d getTimeDerivativeJacobianOmega() const;

  /**
   *
   *  Cette methode retourne une matrice de dimension 3x3
   *  qui donne la derivee de notre vecteur d'angle par rapport a la vitesse angulaire
   *  
   *    d(theta * u)/dt = dP/dt = Lw * w
   *    
   *    w est la vitesse angulaire
   *    Cette methode donne l'inverse de la matrice Lw
   *  
   *    Cette methode utilise les resultats de Ezio Malis dans sa these en 1998 a l' annexe A:
   *
   */

   Eigen::Matrix3d getInverseTimeDerivativeJacobianOmega() const;


  /**
   * Retourne la resultante de la combinaison de la rotation de l'objet courant avec un autre objet du meme type
   *
   * Ici a ete fait le choix d'exprimer une rotation comme un "vecteur" (direction,sens,amplitude)
   * bien que par exemple l'addition ne soit pas definie
   * La combinaison de deux rotation est equivalent a une addition si seulement les deux "vecteurs" sont colineaires.
   *
   * On rappelle que la combinaison de deux rotations n'est pas commutative
   */
  VectRotation addTo(const VectRotation & other) const;

  /**
   * Effectue une multiplication par un scalaire
   */
  VectRotation getMultiple(const double & scalaire) const;

  /**
   * Donne la rotation inverse dans la base mobile
   */
  VectRotation inverse() const;


  static VectRotation getRotationOnZ(
				     const Eigen::Vector3d & axe, 
				     const Eigen::Vector3d & original, 
				     const Eigen::Vector3d & copie);


  std::ostream & afficher(std::ostream &flux) const;

 

};

/**
 * Surcharge de l'operateur *  pour exprimer la combinaison de deux rotations
 *
 * LA COMBINAISON DE DEUX ROTATIONS N'EST PAS COMMUTATIVE
 */
VectRotation  operator*(const VectRotation &a,const VectRotation &  b);


/**
 * Surcharge de l'operateur /  pour exprimer la combinaison de deux rotations
    (A/B) est solution de l'equation X * B = A
 *
 * LA COMBINAISON DE DEUX ROTATIONS N'EST PAS COMMUTATIVE
 */

VectRotation operator/(const VectRotation &a, const VectRotation &b);


/**
 * surcharge de l'operateur * pour definir la plutiplication d'un object VectRotation par un scalaire
 */
VectRotation  operator*(const VectRotation &b,const double &a); 	
VectRotation  operator*(const double &a,const VectRotation &b); 	

/**
 * Surcharge de l'operateur d'affichage pour les flux
 */
std::ostream & operator<<(std::ostream &flux,VectRotation const  & rot); 	// operateur affichage

#endif /* VECTROTATION_H_ */
