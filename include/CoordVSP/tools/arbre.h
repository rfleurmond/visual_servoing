#ifndef ARBRE_H_
#define ARBRE_H_

#include <list>

template <typename T>

/**
   @brief A node in a tree

   @author Renliw Fleurmond
 */


class Noeud{

 private:
  
  T fruit;

  std::list< Noeud<T> * > fils;

  Noeud<T> * pere;

  int profondeur;

  int hauteur;

  

 public:
  
  Noeud(const T & val){
    fruit = val;
    pere = NULL;
  }

  ~Noeud(){
    clear();
  }

  void clear(){
    pere = NULL;
    while(!fils.empty()){
      fils.front()->clear();
      delete fils.front();
      fils.pop_front();
    }
  }

  void addChild(Noeud<T> * bebe){
    bebe->pere = this;
    fils.push_back(bebe);
  }

  void addChild(T val){
    Noeud<T> * feuille = new Noeud<T>(val);
    addChild(feuille);
  }

  int getChildNumber(){
    return fils.size();
  }

  Noeud<T> * getFather(){
    return pere;
  }

  T getValue(){
    return fruit;
  }

  std::list< Noeud<T> * > getAllChilds(){
    return fils;
  }

  bool isRoot(){
    return pere==NULL;
  }

  bool isLeaf(){
    return fils.size()==0;
  }

  Noeud<T> * getRoot(){
    if(isRoot()){
      return this;
    }

    Noeud<T> * racine = pere;

    while(racine->pere!=NULL){
      racine = racine->pere;
    }

    return racine;
  }

  std::list< Noeud<T>* > getPathFromRoot(){
    std::list< Noeud<T> *> chemin;
    chemin.push_back(this);

    if(isRoot()){
      return chemin;
    }

    Noeud<T> * racine = pere;

    while(racine->pere!=NULL){
      chemin.push_back(racine);
      racine = racine->pere;
    }

    return chemin;

  }

};

#endif
