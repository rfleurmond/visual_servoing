#ifndef SIGMOID_H_
#define SIGMOID_H_
#include <assert.h>
#include <math.h>

/**
   @brief Class to compute a smooth transition

   @author Renliw Fleurmond
 */


class Sigmoid {

 private:

  double beta;

  public:
    
  Sigmoid(double b = 1);

  double operator()(double x);

  double value(double x) const;

  void setMax( double b);

};
#endif
