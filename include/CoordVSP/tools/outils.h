/**
 * outils.h
 *
 *  Created on: May 21, 2012
 *      @author Renliw Fleurmond
 */

#ifndef OUTILS_H_
#define OUTILS_H_

#include <vector>
#include "../lib_eigen.h"

std::vector<double *> loadDAT(std::string path,int cols);

bool writeDATA(std::string path, const std::vector<double *> & data, int cols);

bool writeVectors(std::string path, const std::vector<Eigen::VectorXd> & data);

bool writeVectors(std::string path, 
		  const std::vector<double> & temps, const std::vector<Eigen::VectorXd> & data);

void freeDATA(std::vector<double *> & data);

double hasard(double min,double max);

Eigen::VectorXd pinvSolve(const Eigen::MatrixXd & A, const Eigen::VectorXd & Y);

bool pinv(const Eigen::MatrixXd &a, Eigen::MatrixXd &a_pinv);

bool isEqual(double test, double val, double precision);

#endif /* OUTILS_H_ */









