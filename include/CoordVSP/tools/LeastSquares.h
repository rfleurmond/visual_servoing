/*
 * LeastSquares.h
 *
 *  Created on: Jul 17, 2012
 *      @author Renliw Fleurmond
 */

#ifndef LEASTSQUARES_H_
#define LEASTSQUARES_H_
#include "cout.h"
#include "outils.h"

typedef Eigen::VectorXd(*Fonction)(Eigen::VectorXd);
typedef Eigen::MatrixXd(*Jacobien)(Eigen::VectorXd);

/**
   @brief A class to solve least-square problem, gradient descent etc

   @author Renliw Fleurmond

   @deprecated This class can be deleted in future versions
 */


class LeastSquares {

 private:

  static const double defaultMinPas;
  static const double defaultMinError;
  static const int defaultMaxIterations;
  static const int maxDivergences;

  double activeMinPas;
  double activeMinError;
  int maxIterations;


 public:

  LeastSquares();

  void setMinPas(double m);
  void setMinError(double m);
  void setMaxIterations(int i);

  double getMinPas();
  double getMinError();
  int getMaxIterations();

  static double solve(const Eigen::VectorXd & Y,
		      const Eigen::MatrixXd & A,
		      Eigen::VectorXd & solution);

  static double solvePseudoInverse(const Eigen::VectorXd & Y, 
				   const Eigen::MatrixXd & A,
				   Eigen::VectorXd & solution);

  double solveNewton(const Eigen::VectorXd & X0, 
		     Fonction F, 
		     Jacobien J, 
		     Eigen::VectorXd & solution) const;

  double solveGaussNewton(const Eigen::VectorXd & X0, 
			  Fonction F, 
			  Jacobien J, 
			  Eigen::VectorXd & solution) const;

  double solveNewton(const Eigen::VectorXd & X0, 
		     Cout & cost, 
		     Eigen::VectorXd & solution) const;

  double solveGaussNewton(const Eigen::VectorXd &  X0,
			  Cout & cost, 
			  Eigen::VectorXd & solution) const;

};

#endif /* LEASTSQUARES_H_ */
