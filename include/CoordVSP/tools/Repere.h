/**
 * @brief Express a frame transformation
 *
 * Repere.h
 *  Cette classe a ete definie pour exprimer un repere dans un autre
 *  dans un espace vectoriel affine de dimension 3
 *
 * Il a ete convenu pour cette classe qu'un repere est souvent defini par la transformation
 * de corps rigide que existe entre lui et un autre repere
 *
 * Donc cette classe permet d'enregistrer les caracteristiques de la transformation de corps rigide
 * DU  le repere de REFERENCE  AU repere courant
 *
 * donc une rotation : VectRotation exprimee dans le repere de reference
 * 		la rotation effectuee pour  passer du repere de reference au repere courant
 *
 * et une translation: Vector3d     exprimee dans le repere de reference
 * 		le deplacement effectuee du repere de reference au repere courant
 *
 *
 * Cette classe exprime donc en fait un changement de repere SE3
 *
 *
 * On suppose ici et partout dans cette classe "Repere"
 *
 * que le repere "World" inertiel n'est pas adress'e
 *
 * et qu'on ne peut donc pas avoir son adresse.
 *
 * Un repere qui a son pointeur  Repere * reference == NULL
 *
 * est un repere qui s'exprime directement dans le repere "World"
 *
 * c' est a dire que la rotation et le deplacement sont exprimes
 *
 * dans le repere "World"
 *
 *  Created on: Jun 6, 2012
 *      @author Renliw Fleurmond
 */

#ifndef REPERE_H_
#define REPERE_H_

#include "VectRotation.h"

class Repere {

 private:
  
  VectRotation attitude;
  
  Eigen::Vector3d position;

 public:

  //EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  
  /**
   * Constructeur par defaut
   */
  
  Repere();

  /**
   * Constructeur prenant en parametre une matrice de changement de repere
   *
   * Elle peut etre une matrice de rotation 3 x3
   * auquel cas la translation est supposee nulle
   * ou une matrice homogene de dimension 4 x 4 de la forme
   *
   * | r11 r12 r13 t1 |
   *
   * | r21 r22 r23 t2 |
   *
   * | r31 r32 r33 t3 |
   *
   * | 0	 0   0	 1  |
   */

  Repere(Eigen::MatrixXd);
  /**
   * Constructeur prenant en parametre \a att une objet de type VectRotation
   *  qui exprime la rotation dans l'espace de dimension 3
   *
   *  la translation est supposee nulle
   *
   */

  Repere(VectRotation);

  /**
   * Constructeur prenant en parametre
   * la rotation par rapport au repere de reference
   * sous la forme d'un objet \a att de type VectRotation
   * et d'une translation \a pos sous la forme d'un vecteur
   *
   */

  Repere(VectRotation,Eigen::Vector3d);

  /**
   * Constructeur prenant en parametre un vecteur de six composantes
   */


  Repere(Eigen::VectorXd);

  
  /**
   * Modifie les parametres du changement de repere
   * En entree \a X un vecteur de dimensions six
   * pour les six parametres minimaux du changement de repere
   *
   * les trois premiers expriment la rotation
   * les trois derniers le deplacement
   */
  void setParameters(const Eigen::VectorXd & );
  
  /**
   * Retourne les parametre du changement de repere
   * sous la forme d'un vecteur de six parametres
   *
   * les trois premiers expriment la rotation
   * les trois derniers le deplacement
   *
   *
   */
  Eigen::VectorXd getParameters() const;
  /**
   * Retourne un repere resultant de la fusion de repere  decrit par l'objet courant
   * et le repere \a parent fourni en entree
   *
   * L'ordre ici est IMPORTANT
   *
   * on suppose que le repere courant est defini par rapport au repere parent
   *
   * qui lui meme est defini par rapport a un autre et non l'inverse
   *
   */

  Repere getFusionWithParent(const Repere & parent) const;

  /**
   * Retourne la matrice homogene du changement de repere
   * correspondant a l'objet courant
   *
   * Cette matrice homogene a la forme:
   *
   * | r11 r12 r13 t1 |

   * | r21 r22 r23 t2 |

   * | r31 r32 r33 t3 |

   * | 0   0   0   1  |
   *
   */
  Eigen::MatrixXd getHomogenMatrix() const;
  
  /**
   * Calcule les coordonnees d'un point exprime dans le repere de reference
   * dans le repere courant
   */
  Eigen::VectorXd getPointInME(const Eigen::Vector3d& ) const;
  
  /**
   * Calcule les coordonnees d'un vecteur exprime dans le repere de reference
   * dans le repere courant
   */
  Eigen::VectorXd getVectorInME(const Eigen::Vector3d & ) const;
  
  /**
   * Calcule les coordonnees d'un point exprime dans le repere  courant
   * dans le repere de reference
   */
  Eigen::VectorXd getPointInParent(const Eigen::Vector3d & ) const;
  
  /**
   * Calcule les coordonnees d'un vecteur exprime dans le repere  courant
   * dans le repere de reference
   */
  Eigen::VectorXd getVectorInParent(const Eigen::Vector3d & ) const;
  
  /**
   * Retourne la jacobienne de la fonction getPointInMe
   * Le resutlat est une matrice 3 x 9
   *
   * - les trois premieres colonnes sont les derivee en fonction de la rotation
   * - les trois suivantes en fonction du deplacement
   * - les trois derniers en fonction des coordonnes du parametre en entree
   */  

  Eigen::MatrixXd getJacobianPointInME(const Eigen::Vector3d & ) const;
  
  /**
   * Retourne la jacobienne de la fonction getVectorInMe
   * Le resultat est une matrice 3 x 9
   *
   * - les trois premieres colonnes sont les derivee en fonction de la rotation
   * - les trois suivantes en fonction du deplacement
   * - les trois derniers en fonction des coordonnes du parametre en entree
   */
  Eigen::MatrixXd getJacobianVectorInME(const Eigen::Vector3d & ) const;
  /**
   * Retourne la jacobienne de la fonction getPointInParent
   * Le resultat est une matrice 3 x 9
   *
   * - les trois premieres colonnes sont les derivee en fonction de la rotation
   * - les trois suivantes en fonction du deplacement
   * - les trois derniers en fonction des coordonnes du parametre en entree
   */  
  Eigen::MatrixXd getJacobianPointInParent(const Eigen::Vector3d & ) const;

  /**
   * Retourne la jacobienne de la fonction getVectorInParent
   * Le resultat est une matrice 3 x 9
   *
   * les trois premieres colonnes sont les derivee en fonction de la rotation
   * les trois suivantes en fonction du deplacement
   * les trois derniers en fonction des coordonnes du parametre en entree
   */  
  Eigen::MatrixXd getJacobianVectorInParent(const Eigen::Vector3d & ) const;

  /**
   * Retourne l'objet VectRotation encapsulant la rotation
   */
  VectRotation getRotation() const;
  
  /**
   * Modifie la rotation
   */  
  void setRotation(const VectRotation & );
  
  /**
   * Retourne le deplacement
   */  

  Eigen::Vector3d getDeplacement() const;
  
  /**
   * Modifie le deplacement
   */

  void setDeplacement(const Eigen::Vector3d & );

  Repere inverse() const;
  
  Eigen::Vector3d getX() const;

  Eigen::Vector3d getY() const;

  Eigen::Vector3d getZ() const;

  Eigen::MatrixXd getKineticMatrix() const;

  Eigen::MatrixXd getInversKineticMatrix() const;


};

/**
 * Surcharge de l'operateur *  pour exprimer la combinaison de deux SE3
 *
 * LA COMBINAISON DE DEUX SE3 N'EST PAS COMMUTATIVE
 */

Repere operator*(const Repere &a, const Repere &b);

/**
 * Surcharge de l'operateur /  pour exprimer la combinaison de deux SE3

    (A/B) est solution de l'equation X * B = A
 *
 * LA COMBINAISON DE DEUX SE3 N'EST PAS COMMUTATIVE
 */

Repere operator/(const Repere &a, const Repere &b);


/**
 * Surcharge de l'operateur d'affichage pour les flux
 */
std::ostream & operator<<(std::ostream &flux,Repere const  & se3);


#endif /* REPERE_H_ */
