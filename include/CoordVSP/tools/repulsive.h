#ifndef REPULSIVE_H_
#define REPULSIVE_H_
#include <assert.h>
#include <math.h>

/**
   @brief Class to compute a error when a value is in the vicinity of its limits

   @author Renliw Fleurmond
 */


class Repulsive {

 private:

  double coeffMin;

  double coeffMax;

  double min;

  double max;

  double alpha;

  double beta;

  double milieu;

  double psi;

  double omega;

  double pente;

  double plafond;

 public:
    
  Repulsive(double cMin = 0.1, double cMax = 0, double poids = 1);

  void setWeight(double poids);

  void setCeil(double c);

  void setBounds(double mint, double maxt);

  double operator()(double x);

  double coutHyp(double x) const;

  double jacobien( double x) const;

  /////

  double getAlpha() const;
  
  double getBeta() const;

  double getMiddle() const;

  double getPsi() const;

  double getOmega() const;

};
#endif
