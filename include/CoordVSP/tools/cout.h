
#ifndef COUT_H_
#define COUT_H_

#include "../lib_eigen.h"

/**
   @brief A base class for Cost function

   @author Renliw Fleurmond

   @deprecated This class can be deleted in future versions
 */


class Cout{

 public:

  virtual ~Cout(){};

  virtual Eigen::VectorXd fonctionObjectif(Eigen::VectorXd x) const =0;

  virtual Eigen::MatrixXd jacobienObjectif(Eigen::VectorXd x) const =0;


};

#endif
