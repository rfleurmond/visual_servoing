#ifndef MY_KINEMATIC_MODEL_H_
#define MY_KINEMATIC_MODEL_H_
#include "geo-robot.h"
#include <vector>

/**

   @brief Implementation of a forward and inverse kinematic model of a robot arm

   @author Renliw Fleurmond
 
 */

class MyKineModel: public InverseKinematicModel{

 private:

  GeoRobot * filament;

  static const double CRAYON;

  std::vector<int> frozen;

 public:

  MyKineModel(GeoRobot * robot);

  int getTotalDegres() const;

  void setCoordonneArticulaires(const Eigen::VectorXd & v);

  void addFrozenJoint(int i);

  Repere getSituation() const;

  Repere getSituation(const Eigen::VectorXd & q) const; 

  Repere getSituation(const Eigen::VectorXd & q, int i) const; 

  Eigen::Vector3d getLinearVelocity(const Eigen::VectorXd & q, const Eigen::VectorXd & qpoint) const;

  Eigen::Vector3d getAngularVelocity(const Eigen::VectorXd & q, const Eigen::VectorXd & qpoint) const;

  Eigen::MatrixXd getKinematicJacobian(const Eigen::VectorXd & q) const;

  Eigen::MatrixXd getKinematicJacobian(const Eigen::VectorXd & q, int i) const;

  bool getCommandFromVelocity(
			      const Eigen::VectorXd & q, 
			      const Eigen::VectorXd & torseur, 
			      Eigen::VectorXd & commande) const;

  bool getCommandFromLinearVelocity(
				    const Eigen::VectorXd & q, 
				    const Eigen::Vector3d & linear, 
				    Eigen::VectorXd & commande) const;

  bool getCommandFromAngularVelocity(
				     const Eigen::VectorXd & q, 
				     const Eigen::Vector3d & angular, 
				     Eigen::VectorXd & commande) const;


};

#endif
