#ifndef LIAISON_H_
#define LIAISON_H_

#include "../tools.h"


class Liaison;

/**

   @brief An implementation of a robot body

   @author Renliw Fleurmond
 
 */

class Corps{

 private:

  std::string nom;

  Repere meshToMe;

  Repere boutAbout;

  std::string mesh;

  Liaison * liaisonA;
  Liaison * liaisonB;

  
  
 public:

 Corps():
  nom(),
    meshToMe(),
    boutAbout(),
    mesh(),
    liaisonA(NULL),
    liaisonB(NULL)
      {

      }

 Corps(Repere r):
  nom(),
    meshToMe(),
    boutAbout(r),
    mesh(),
    liaisonA(NULL),
    liaisonB(NULL)
      {

      }



  ~Corps(){
    liaisonA = NULL;
    liaisonB = NULL;
  }


  Repere getMeshToMe() const;
  void setMeshToMe(const Repere & r);

  Repere getBoutAbout() const;
  void setBoutAbout(const Repere & r);
  
  Liaison * getLiaisonA() const;
  Liaison * getLiaisonB() const;
  void addLiaisonA(Liaison * l);
  void addLiaisonB(Liaison * l);
  
  std::string getNom() const;
  void setNom(std::string s);
    

};

/**

   @brief An implementation of a possible move of a joint

   @author Renliw Fleurmond
 
 */

typedef struct{

  Eigen::Vector3d origine;

  bool isRotation;

  Eigen::Vector3d direction;

} mouvement;


/**

   @brief An implementation of a robot joint

   @author Renliw Fleurmond
 
 */

class Liaison {

 protected:

  std::string nom;

  std::string mesh;

  int type;

  int degres;

  Repere meToBodyB;

  Repere meshToMe;

  Corps * corpsA;

  Corps * corpsB;

  Eigen::VectorXd etat;

  Eigen::VectorXd initial;

  Eigen::VectorXd minimum;

  Eigen::VectorXd maximum;

  static const double CRAYON;


 public:

  const static int ROTOIDE = 1;
  const static int PRISMAT = 2;
  const static int ROTULE  = 3;
  const static int VIRTUAL = 4;
  const static int STATIC  = 0;

  Liaison();

  virtual ~Liaison(){};

  int getDegres() const;

  void setCorpsA(Corps *c);

  void setCorpsB(Corps *c);

  Corps * getCorpsA() const;

  Corps * getCorpsB() const;

  Eigen::VectorXd getQ() const;
   
  int getType() const;

  void goToInitialState();

  Eigen::VectorXd getInitialState() const;

  void  setInitialState(const Eigen::VectorXd & begin);

  void setMinimum(const Eigen::VectorXd & m);

  void setMaximum(const Eigen::VectorXd & m);

  Eigen::VectorXd getMinimum();

  Eigen::VectorXd getMaximum();

  void setMesh(std::string file);

  std::string getMesh();

  Repere getFromMetoBodyB() const;

  virtual Repere getFromMetoBodyB(const Eigen::VectorXd & q) const = 0;

  virtual void setQ(const Eigen::VectorXd & q)=0;

  virtual mouvement getMouvement() const =0;

  virtual Eigen::MatrixXd getJacobian() const = 0;

  virtual bool getQValueFor(const Repere & transfo, Eigen::VectorXd & value) const = 0;

};


#endif
