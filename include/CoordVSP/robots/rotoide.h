#ifndef ROTOIDE_H_
#define ROTOIDE_H_
#include "liaison.h"
 

/**

   @brief Implementation of a revolution joint

   @author Renliw Fleurmond
 
 */

 class Rotoide: public Liaison {

 public:

  Rotoide(std::string n);

  Repere getFromMetoBodyB(const Eigen::VectorXd & q) const;

  void setQ(const Eigen::VectorXd & q);

  mouvement getMouvement() const;

  Eigen::MatrixXd getJacobian() const;

  bool getQValueFor(const Repere & transfo, Eigen::VectorXd & value) const;


};

#endif 
