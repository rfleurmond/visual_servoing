#ifndef MODELES_ROBOT_H_
#define MODELES_ROBOT_H_

#include "../tools.h"

/**

   @brief Virtual class implementation of a robot arm

   @author Renliw Fleurmond
 
 */

class Robot{

 public:

  virtual ~Robot(){};

  virtual int getTotalDegres() const = 0;

  virtual void setCoordonneArticulaires(const Eigen::VectorXd & q) = 0;

  virtual Eigen::VectorXd getCoordonneArticulaires() const = 0;

  virtual Eigen::VectorXd getMinimalQ() const = 0;

  virtual Eigen::VectorXd getMaximalQ() const = 0;

};

/**

   @brief Virtual class implementation of forward geometric model of a robot arm

   @author Renliw Fleurmond
 
 */
class GeometricModel{

 public:

  virtual ~GeometricModel(){};

  virtual int getTotalDegres() const = 0;

  virtual Repere getSituation(const Eigen::VectorXd & q) const = 0;

  virtual Repere getSituation(const Eigen::VectorXd & q, int i) const = 0;

};

/**

   @brief Virtual class implementation of a backward geometric model of a robot arm

   @author Renliw Fleurmond
 
 */


class InverseGeoModel: public GeometricModel{

 public:

  virtual ~InverseGeoModel(){};

  virtual std::list<Eigen::VectorXd> getCoordonnees(const Repere & situation) = 0;

};

/**

   @brief Virtual class implementation of a forward kinematic model of a robot arm

   @author Renliw Fleurmond
 
 */


class KinematicModel: public GeometricModel{

 public:

  virtual ~KinematicModel(){};

  virtual Eigen::Vector3d getLinearVelocity(
					    const Eigen::VectorXd & q, 
					    const Eigen::VectorXd & qpoint) const = 0;

  virtual Eigen::Vector3d getAngularVelocity(
					     const Eigen::VectorXd & q,
					     const Eigen::VectorXd & qpoint) const = 0;

  virtual Eigen::MatrixXd getKinematicJacobian(const Eigen::VectorXd & q) const = 0;

  virtual Eigen::MatrixXd getKinematicJacobian(const Eigen::VectorXd & q, int i) const = 0;
};


/**

   @brief Virtual class implementation of a Inverse kinematic model of a robot arm

   @author Renliw Fleurmond
 
 */

class InverseKinematicModel: public KinematicModel{

 public:

  virtual ~InverseKinematicModel(){};

  virtual bool getCommandFromVelocity(
				      const Eigen::VectorXd & q, 
				      const Eigen::VectorXd & torseur,
				      Eigen::VectorXd & commande) const = 0;

  virtual bool getCommandFromLinearVelocity(
					    const Eigen::VectorXd & q, 
					    const Eigen::Vector3d & linear, 
					    Eigen::VectorXd & commande) const  = 0;

  virtual bool getCommandFromAngularVelocity(
					     const Eigen::VectorXd & q, 
					     const Eigen::Vector3d & angular, 
					     Eigen::VectorXd & commande) const = 0;

};


/**

   @brief Virtual class implementation of robot wtih many models ?

   @author Renliw Fleurmond
 
 */

class WorkableRobotModel: public InverseKinematicModel{

 public:

  virtual ~WorkableRobotModel(){};

  virtual bool setTorseurCinematique(const Eigen::VectorXd & torseur) = 0;

};

/**

   @brief Virtual class implementation of robot wtih many models ?

   @author Renliw Fleurmond
 
 */


class FullyWorkableRobotModel: public WorkableRobotModel, public InverseGeoModel{
 

};

#endif
