#ifndef PRISMATIQUE_H_
#define PRISMATIQUE_H_
#include "liaison.h"

/**

   @brief Implementation of a prismatic joint

   @author Renliw Fleurmond
 
 */
 
class Prismatique: public Liaison {

 public:

  Prismatique(std::string n);

  Repere getFromMetoBodyB(const Eigen::VectorXd & q) const;

  void setQ(const Eigen::VectorXd & q);

  mouvement getMouvement() const;

  Eigen::MatrixXd getJacobian() const;

  bool getQValueFor(const Repere & transfo, Eigen::VectorXd & value) const;

};

#endif 
