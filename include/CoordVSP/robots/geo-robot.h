#ifndef GEO_ROBOT_H_
#define GEO_ROBOT_H_

#include "liaison.h"
#include "modeles-robot.h"

/**

   @brief An implementation of the forward geometric model of a robot

   @author Renliw Fleurmond
 
 */


class GeoRobot: public Robot, public GeometricModel {

 protected:

  std::string nom;

  std::string fichier;

  Corps * base;

  Corps * main;

  Liaison * link;
  
  Liaison * poignet;

  Corps * bone;

  int degres;

  int nombre;

  Eigen::VectorXd etat;

  Eigen::VectorXd qMin;

  Eigen::VectorXd qMax;

  void calculTotalDegres();

  void checkBounds();

  
  
 public:

  GeoRobot();

  ~GeoRobot();

  int getTotalDegres() const;

  Repere getSituation(const Eigen::VectorXd & q, int i) const;

  Repere getSituation(const Eigen::VectorXd & q) const;

  Repere getSituation() const;

  virtual void setCoordonneArticulaires(const Eigen::VectorXd & q);

  virtual Eigen::VectorXd getCoordonneArticulaires() const;

  virtual Eigen::VectorXd getMinimalQ() const;

  virtual Eigen::VectorXd getMaximalQ() const;

  int getNombreLiaisons() const;

  void setBase(Corps * c);

  void setHand(Corps * c);

  void addCorps(Corps * c);

  void addLiaison(Liaison * c);

  Liaison * next(Liaison * l) const;

  Liaison * previous(Liaison * l) const;

  Corps * next(Corps * l) const;

  Corps * previous(Corps * l) const;

  Liaison * getFirstLiaison();

  Liaison * getLastLiaison();

  Corps * getFirstCorps();

  Corps * getLastCorps();

};

bool saveDHTRobot(std::string path, GeoRobot & r2d2);

bool loadDHTRobot(std::string path, GeoRobot & r2d2);

bool saveSE3Robot(std::string path, GeoRobot & r2d2);

bool loadSE3Robot(std::string path, GeoRobot & r2d2);

#endif
