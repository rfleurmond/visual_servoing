#ifndef MY_INVERSE_GEO_MODEL_H_
#define MY_INVERSE_GEO_MODEL_H_

#include "geo-robot.h"
#include "../geometrie.h"

/**
   @brief To record what joint value was setted ?

   @author Renliw Fleurmond
 */


typedef struct {
  int index;
  double value;
} affectation;

/**
   @brief To represent the possible orientations of a joint or a link

   @author Renliw Fleurmond
 */


class PossibleOrientation{

 public:

  Eigen::Vector3d axe;
  Lieu * orient;
  bool everything;

 PossibleOrientation():
  axe(Eigen::Vector3d(0,0,1)),
      orient(NULL),
      everything(false)
      {
      }

  int getTypeLieu(Eigen::Vector3d vecteur){
    if(everything){
      return Lieu::SPHERE;
    }
    if(orient==NULL){
      return Lieu::NOT_DEFINED;
    }
    double scal = axe.transpose()*vecteur;
    if(isEqual(scal,1,CRAYON) || isEqual(scal,-1,CRAYON)){
      return orient->getType();
    }
    if(orient->getType()==Lieu::POINT){
      return Lieu::CIRCLE;
    }
    if(orient->getType()==Lieu::CIRCLE){
      Cercle * cc = dynamic_cast<Cercle *>(orient);
      if(isEqual(scal,0,CRAYON) && isEqual(cc->getRayon(),1,CRAYON))
	return Lieu::SPHERE;
      return Lieu::TORE;
    }
    return Lieu::NOT_DEFINED;
  }

  Cercle getCercleOfAxe(Eigen::Vector3d vecteur){
    Cercle cc;
    if(everything || orient ==NULL){
      return cc;
    }
    double scal = axe.transpose()*vecteur;
    if(orient->getType()==Lieu::CIRCLE){
      if(isEqual(scal,1,CRAYON) || isEqual(scal,-1,CRAYON)){
	cc = *(dynamic_cast<Cercle *>(orient));
      }
    }
    if(orient->getType()==Lieu::POINT){
      LPoint * lp = dynamic_cast<LPoint *>(orient);
      cc.setDirection(lp->getCenter());
      cc.setCenter(scal * lp->getCenter());
      double rayon = sqrt(1 -scal*scal);
      cc.setRayon(rayon);
    }
    return cc;
  }
  

};


/**
   @brief Private class used in trial of Inverse geometric model

   @author Renliw Fleurmond
 */

class Abstraction{

 public:

  Liaison * link;
  bool isSetted;
  double valeur;
  
  bool isPositionFixed;
  Eigen::Vector3d position;

  Lieu * centre;
  Lieu * lieuGauche;
  Lieu * lieuDroite;

  bool isOrientationFixed;
  VectRotation orientation;
  
  PossibleOrientation gaucheOrient;
  PossibleOrientation droiteOrient;


 Abstraction():
  link(NULL),
    isSetted(false),
    valeur(0),
    isPositionFixed(false),
    position(Eigen::Vector3d::Zero()),
    centre(NULL),
    lieuGauche(NULL),
    lieuDroite(NULL),
    isOrientationFixed(false),
    orientation(),
    gaucheOrient(),
    droiteOrient()
      {
  
      }
 
};
 

typedef Noeud<affectation> Node;

/**
   @brief Class which implements an expermimental way of inverse geometrical model of robots

   @author Renliw Fleurmond
 */

class MyInverseGeoModel: public InverseGeoModel{
 
 private:

  int n;

  int creations;

  int suppressions;

  bool complet;

  bool positionFound;

  bool situationFound;

  bool infinite;

  int genese;

  int apocal;
  
  GeoRobot * robot;

  Node * arbre;

  Abstraction * squelette;
  
  void reflexion();

  void analyseOrientation(Abstraction * symboles,int N, int i, int j, 
			  Repere debut, Repere fin, bool sens);

  void forwardAnalyse(Abstraction * symboles, int N,int i, Repere base);

  void backwardAnalyse(Abstraction * symboles, int N,int i, Repere base);

  bool universalAnalyse(Abstraction * symboles, int N, Lieu ** geo, int i, int j, 
			Repere debut, Repere terme, bool sens, int * stop);

  void exploitation(Abstraction * symboles, int N,int index, Repere frame, Repere situation, Node * noeud);

  std::list<double> findInDroite(Liaison * fossile, Droite * centre, Repere frame,Eigen::Vector3d base, Eigen::Vector3d lune, bool sens);

  std::list<double> findInCircle(Liaison * fossile, Cercle * centre, Repere frame, Eigen::Vector3d base, Eigen::Vector3d lune, bool sens);

  std::list<double> findInCylinder(Liaison * fossile,Cylindre * cy, Repere frame, Eigen::Vector3d base, Eigen::Vector3d lune, bool sens);

  std::list<double> findInTore(Liaison * fossile,Tore * tr, Repere frame,Eigen::Vector3d base,Eigen::Vector3d lune, bool sens);

  std::list<double> findInTurningPlan(Liaison * fossile, PlanTournant * pt, Repere frame,Eigen::Vector3d base, Eigen::Vector3d lune, bool sens);

  std::list<double> findInTurningDroite(Liaison * fossile, DroiteTournante * dt, Repere frame,Eigen::Vector3d base,Eigen::Vector3d lune, bool sens);

  std::list<double> findInSlidingPlan(Liaison * fossile, PlanGlissant * centre, Repere frame, Eigen::Vector3d base, Eigen::Vector3d lune, bool sens);

  std::list<double> findInSphere(Liaison * fossile, Sphere * centre, Repere frame, Eigen::Vector3d base, Eigen::Vector3d lune, bool sens);

  std::list<double> findSatelliteInOrbit(Liaison * joint, Lieu * centre, Repere frame, Eigen::Vector3d base, Eigen::Vector3d lune, bool sens);

  void ajout(Node * noeud,int index, std::list<double> liste);

  void ajout(Node * noeud,int index, double val);

  void clearMentalConstructs(Abstraction * symboles, int n);

  std::list<LPoint> searchPointsIntersectionGauche(Lieu * terre, Lieu * ciel);

  void printReflexion(Abstraction * copie, int N, int index);

  void bourgeonnement(Abstraction * symboles, int N, Node * noeud, int index, Repere frame, Repere situation);

 public:

  MyInverseGeoModel(GeoRobot * worker);

  ~MyInverseGeoModel();

  int getTotalDegres() const;

  Repere getSituation(const Eigen::VectorXd & q) const;

  Repere getSituation(const Eigen::VectorXd & q, int i) const;

  std::list<Eigen::VectorXd> getCoordonnees(Repere situation);

  bool isComplete();

  bool hasFoundSolutionsForPosition();

  bool hasFoundCompleteSolutions();

  bool hasInfinityOfSolutions();

};

#endif
