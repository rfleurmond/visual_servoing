/*
 * vision.h
 *
 *  Created on: Jun 14, 2012
 *      @author Renliw Fleurmond
 */

#ifndef VISION_H_
#define VISION_H_

#include "vision/RadialDistorsion.h"
#include "vision/StereoCamera.h"
#include <map>


#endif /* VISION_H_ */
