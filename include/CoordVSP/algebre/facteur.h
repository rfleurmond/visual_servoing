#ifndef FACTEUR_H_
#define FACTEUR_H_

#include "poly-solveur.h"


/**

   @brief Class used to implement polynome with its exposant

   @author Renliw Fleurmond

*/

typedef struct{
  int exposant;
  polynome terme;
} facteur;

/**

   @brief Class used to implement a products of polynomes

   @author Renliw Fleurmond

*/

class polyfacteur{

 private:

  int degre;
  double multiple;
  std::list<facteur> termes;
  static std::list<double> selectRacines(std::list<RFComplex> roots);
  

 public:

  polyfacteur();
  polyfacteur(facteur);
  polyfacteur(polynome p);
  polyfacteur(double a,std::list<double> racines);

  void addFacteur(int expo, polynome p);
  void addFacteur(facteur f);
  void multiplie(double d);
  void multiplie(facteur f);
  void multiplie(polynome p);
  void multiplie(polyfacteur pf);
  
  int getDegre();
  int getNombreFacteurs();
  std::list<facteur> getFacteurs();
  void setMultiple(double d);
  double getMultiple();
  void affiche(std::ostream&);
  void raz();
  polynome developpement();
  double getFunctionValue(double x);
  static polyfacteur factoriser(polynome p,double racine);
  static double extraire1racine(polynome p);
  static std::list<double> extraire2racines(polynome p);
  static std::list<double> extraire3racines(polynome p);
  //static list<double> extraireRacine4(polynome p);
  //static list<double> extraireRacineMultiple(polynome p);
  //static list<double> chercherRacines(polynome p);
};

std::ostream& operator << (std::ostream&, facteur&);
std::ostream& operator << (std::ostream&, polyfacteur&);
polyfacteur operator * (const polyfacteur &p,const polyfacteur &q);
polyfacteur operator * (const polyfacteur &q,double);
polyfacteur operator * (double, const polyfacteur &q);

#endif
