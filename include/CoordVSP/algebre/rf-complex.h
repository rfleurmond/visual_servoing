#include <cmath>
/**

   @brief implementation of a complex

   Copie du code de Gorgonzola sur le site cppfrance

   http://www.cppfrance.com/codes/RESOLUTION-EQUATIONS-JUSQU-3EME-DEGRE-CLASSE-MANIPULER-NOMBRES_30622.aspx
 */


class RFComplex
{
 private:

  double x;
  double y;

  friend RFComplex operator+(const RFComplex &c);
  friend RFComplex operator-(const RFComplex &c);
  friend RFComplex operator+(const RFComplex &c1,const RFComplex &c2);
  friend RFComplex operator-(const RFComplex &c1,const RFComplex &c2);
  friend RFComplex operator*(const RFComplex &c1,const RFComplex &c2);
  friend RFComplex operator/(const RFComplex &c1,const RFComplex &c2);

 public:

  RFComplex(const RFComplex &c);
  RFComplex(double a=0,double b=0);

  double Re();
  double Im();
  bool isReal();
  double Mod();
  double Arg();
  void Coord(double a,double b);
  void Polar(double mod,double arg);
  RFComplex Conj();
  RFComplex Root(unsigned n,unsigned k);

  bool operator==(const RFComplex &c);
  bool operator!=(const RFComplex &c);
  bool operator<=(const RFComplex &c);
  bool operator>=(const RFComplex &c);
  bool operator<(const RFComplex &c);
  bool operator>(const RFComplex &c);
  RFComplex &operator=(const RFComplex &c);
  RFComplex &operator+=(const RFComplex &c);
  RFComplex &operator-=(const RFComplex &c);
  RFComplex &operator*=(const RFComplex &c);
  RFComplex &operator/=(const RFComplex &c);

};
