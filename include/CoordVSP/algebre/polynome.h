#ifndef POLYNOME_H_
#define POLYNOME_H_

#include "../tools.h"

/**

   @brief Class to implement a polynome of one variable

   @author Renliw Fleurmond

 */

class polynome{

 private:
  
  int degre;

  Eigen::VectorXd coeff;

  void init(int n, double *p);

  void init(const Eigen::VectorXd & );

  
 public:

  polynome();
  polynome(int n,double *);
  polynome(const Eigen::VectorXd & coeff);
  
  int getDegre(void) {return degre;} 
  double getCoeff(int i) {return coeff(i);}
  Eigen::VectorXd getAll(){return coeff;}
  void raz(void); //degré 0, coef 0
  void ajusteDegre(void); //si les premiers coef sont nuls, on peut diminuer le degré
  void saisie(void); 
  void affiche(std::ostream&);
  void copie(polynome);
  bool egal(polynome);  //true si tous les coefficients sont égaux
  double valeur(double x);
  void additionner(double);
  void additionner(polynome);
  void soustraire(double);
  void soustraire(polynome);
  void multiplier(double);
  void multiplier(polynome);
  void deriver(void);
  void integrer(double); //on peut donner la constante (0 par déf)
  double premiere_racine(double binf,double bsup,double precision); //cherche la 1ère racine
  // en commençant à la borne inférieure, en essayant pas à pas. S'il ne trouve 
  // pas, rend une valeur supérieure à bsup. 
  // ATTENTION : essaye TOUTES les valeurs, choisissez une précision pas trop faible
  double racine(double binf,double bsup,double precision); //cherche la racine entre la borne
  // inférieure et supérieure,. Il DOIT n'y en avoir qu'UNE dans l'intervale.
  // Optimisé (dichotomie), vous pouvez demander une bonne précision.
}; 
 
/*déclaration de fonctions relatives aux polynomes ******************************************/
std::ostream& operator << (std::ostream&, polynome&);
std::istream& operator >> (std::istream&, polynome&);
int operator == (polynome&,polynome&);
int operator != (polynome&,polynome&);
polynome operator + (polynome&,polynome&);
polynome operator + (polynome&,double);
polynome operator + (double,polynome&);
polynome operator - (polynome&,polynome&);
polynome operator - (polynome&,double);
polynome operator - (double,polynome&);
polynome operator * (const polynome &p,const polynome &q);
polynome operator * (const polynome &q,double);
polynome operator * (double, const polynome &q);
polynome operator / (const polynome &q,double);

 

#endif
