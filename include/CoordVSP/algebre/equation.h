#ifndef EQUATION_H_
#define EQUATION_H_

#include "../tools.h"

/**

   @brief Class used to implement a linear equation

   @author Renliw Fleurmond

*/
   

class Equation{

 private:
  Eigen::MatrixXd mat;
  Eigen::VectorXd constante;
  Eigen::VectorXd solution;
  
 public:

 Equation():
  mat(Eigen::Matrix3d::Identity()),
    constante(Eigen::Vector3d::Zero()),
    solution(Eigen::Vector3d::Zero())
  {
  
  }

  void affiche(std::ostream& flux);
  void setValeurs(Eigen::MatrixXd m,Eigen::VectorXd v);
  void solve();
  Eigen::VectorXd getSolution();
  
};

std::ostream& operator << (std::ostream&, Equation&);

#endif
