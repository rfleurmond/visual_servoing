#include "rf-complex.h"
#include "polynome.h"

#define PRECISION 10000

/**

   @brief Static class to solve third, second and first degres polynomes

   @author Renliw Fleurmond

 */

class PolySolveur{

 public:

  static RFComplex resoudre1(polynome p);

  static std::list<RFComplex> resoudre2(polynome p);

  static std::list<RFComplex> resoudre3(polynome p);

};

