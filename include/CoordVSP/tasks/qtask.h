#ifndef Q_TASK_H_
#define Q_TASK_H_

#include "task.h"

/**
   @brief A Task which is null when a given joint coordinates are reached

   @author Renliw Fleurmond
 */


class QTask: public Task{

 private:

  double periode;
  
  Eigen::VectorXd desir;

  Eigen::MatrixXd activation;

 public:

  QTask(const Eigen::VectorXd & qWanted);
  
  QTask(const Eigen::VectorXd & qWanted, const Eigen::MatrixXd & acti);
  
  void setWantedState(const Eigen::VectorXd & q);

  void setActivationMatrix(const Eigen::MatrixXd &  a);

  void setState(const Eigen::VectorXd & q);
  
  Eigen::VectorXd getState() const;

  Eigen::VectorXd fonction(const Eigen::VectorXd & q) const;

  Eigen::MatrixXd jacobien(const Eigen::VectorXd & q) const;

  double getPeriod() const;
  
  void setPeriod(double p);

  bool hasChanged() const;
  
  
};


/**
   @brief A Task which is null when a given joint coordinates are reached

   @author Renliw Fleurmond
 */


class QTask1D: public Task{

 private:

  double periode;

  Eigen::VectorXd desir;
  
  Eigen::MatrixXd activation;
  

 public:

  QTask1D(const Eigen::VectorXd & qWanted);
  
  QTask1D(const Eigen::VectorXd & qWanted, const Eigen::MatrixXd &  acti);
  
  void setWantedState(const Eigen::VectorXd & q);

  void setActivationMatrix(const Eigen::MatrixXd &  a);

  void setState(const Eigen::VectorXd & q);
  
  Eigen::VectorXd getState() const;

  Eigen::VectorXd fonction(const Eigen::VectorXd & q) const;

  Eigen::MatrixXd jacobien(const Eigen::VectorXd & q) const;

  double getPeriod() const;
  
  void setPeriod(double p);

  bool hasChanged() const;
  
  
};

#endif
