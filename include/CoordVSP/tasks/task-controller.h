#ifndef TASK_CONTROLLER_H_
#define TASK_CONTROLLER_H_

#include "task.h"
#include "task-system.h"
#include "my-clock.h"

/**
   @brief A basic task controller to apply control law form a Task to a TaskSystem

   @author Renliw Fleurmond
 */


class TaskController{

 protected:

  TaskSystem * systeme;

  Task * tache;

  double lambda;

  double mu;

  double periodS;

  double periodT;

  double heure;

  double lastChange;

  double horizon;

  double normeMinimum;

  std::string fichierTache;

  std::string fichierSys;

  bool hasToLog;

  double multiple;

  bool smoothing;

  Eigen::VectorXd erreur;
  
  Eigen::VectorXd commande;
  
  Eigen::VectorXd old_order;

  Eigen::VectorXd last_order;
  
  Eigen::VectorXd integrale;
  
  Eigen::VectorXd derivee;
  
  Eigen::VectorXd etat_q;

 public:

  TaskController(TaskSystem * sys, Task * task);

  virtual ~TaskController(){};

  double getSmoothFactor();

  void setSmoothRatio(double m);

  void enableSmoothing(bool mode);

  void setTimeLimit(double limit);

  void setThreshold(double espilon);

  void setLogMode(bool mode);

  void setTaskLogFile(std::string path);

  void setSysLogFile(std::string path);

  virtual void run();

  virtual Eigen::VectorXd getTimeCommand(double tempo);

};

#endif
