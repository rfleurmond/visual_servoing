#ifndef DUALITY_TAKS_H_
#define DUALITY_TAKS_H_

#include "task.h"

/**
   @brief A couple of two tasks as one single Task

   @author Renliw Fleurmond
 */


class DualityTask: public Task{

 protected:

  Task * maitre;

  Task * esclave; 

  int hidden;

 public:

  DualityTask(Task * master, Task * slave);

  virtual void setState(const Eigen::VectorXd & q);

  virtual Eigen::VectorXd getState() const;

  virtual Eigen::VectorXd fonction(const Eigen::VectorXd & q) const;

  virtual Eigen::MatrixXd jacobien(const Eigen::VectorXd & q) const;

  virtual void recordCommand(const Eigen::VectorXd & dotQ);

  virtual Eigen::VectorXd calculCommandeFor(const Eigen::VectorXd & erreur) const = 0;

  virtual double getPeriod() const;

  virtual bool hasChanged() const;
  
  virtual bool isComplete() const;
  
  virtual void hideTaskOnControl(Task * t);
  
};

#endif
