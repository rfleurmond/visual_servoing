#ifndef PART_TAKS_H_
#define PART_TAKS_H_

#include "task.h"

/**
   @brief a Task which is a part of oneanother Task

   @author Renliw Fleurmond
 */

class PartTask: public Task{

 private:

  Task * task;

  Eigen::MatrixXd activation;

  Eigen::MatrixXd constante;

 public:

  PartTask(const Eigen::MatrixXd & activ, Task * tache);
  
  void setState(const Eigen::VectorXd & q);

  void setOffset(const Eigen::VectorXd & offset);

  Eigen::VectorXd getState() const;

  Eigen::VectorXd fonction(const Eigen::VectorXd & q) const;

  Eigen::MatrixXd jacobien(const Eigen::VectorXd & q) const;

  Eigen::VectorXd calculCommandeFor(const Eigen::VectorXd & erreur) const;

  void recordCommand(const Eigen::VectorXd & dotQ);

  double getPeriod() const;

  bool hasChanged() const;

  bool isComplete() const;
  
};

#endif
