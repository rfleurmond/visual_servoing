#ifndef EGALITY_TAKS_H_
#define EGALITY_TAKS_H_

#include "duality-task.h"

/**
   @brief DualityTask which simply tries to satisfy the control laws of the two Task

   @author Renliw Fleurmond
 */


class EgalityTask: public DualityTask{

 public:

  EgalityTask(Task * brother1, Task * brother2);
  
  Eigen::VectorXd calculCommandeFor(const Eigen::VectorXd & erreur) const;

  double getPeriod() const;

  bool hasChanged() const;
  
  
};

#endif
