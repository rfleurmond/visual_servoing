#ifndef MY_OPTIMA_PRIORITY_TAKS_H_
#define MY_OPTIMA_PRIORITY_TAKS_H_

#include "priority-task.h"

/**
   @brief PriorityTask which satisfy at the best the two tasks but the first has the priority

   @author Renliw Fleurmond
 */



class MyOptimaPriorityTask: public PriorityTask{

 public:

  MyOptimaPriorityTask(Task * master, Task * slave);
  
  Eigen::VectorXd calculCommandeFor(const Eigen::VectorXd & erreur) const;

};


#endif
