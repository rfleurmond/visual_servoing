#ifndef TASK_SYSTEM_H_
#define TASK_SYSTEM_H_

#include "../tools.h"

/**
   @brief A controllable system

   @author Renliw Fleurmond
 */

class TaskSystem{

 public:

  virtual ~TaskSystem(){};

  virtual int getDimState() const = 0;

  virtual Eigen::VectorXd getState() const = 0;

  virtual void setState(const Eigen::VectorXd & state) = 0;

  virtual void takeCommand(const Eigen::VectorXd & order) = 0;

  virtual void updateTime(double t) = 0;

  virtual double getPeriod() const = 0;
  
};

#endif
