#ifndef Q_SYSTEM_H_
#define Q_SYSTEM_H_

#include "task-system.h"

/**
   @brief A virtual controllable system TaskSystem

   @author Renliw Fleurmond
 */


class QSystem: public TaskSystem {

 private:

  Eigen::VectorXd etat;

  Eigen::VectorXd vitesse;

  int dimension;

  double periode;

  double heure;

 public:

  QSystem(const Eigen::VectorXd & state, double period);

  QSystem(int dim, double period);

  int getDimState() const;

  Eigen::VectorXd getState() const;

  void setState(const Eigen::VectorXd & state);

  void takeCommand(const Eigen::VectorXd & order);

  void updateTime(double t);

  double getPeriod() const;
  
};

#endif
