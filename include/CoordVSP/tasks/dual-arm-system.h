#ifndef DUAL_ARM_SYSTEM_H_
#define DUAL_ARM_SYSTEM_H_

#include "task-system.h"

/**
   @brief A couple of two controllable systems as one single system

   @author Renliw Fleurmond
 */


class DualArmSystem: public TaskSystem{

 private: 

  TaskSystem * brasGauche;

  TaskSystem * brasDroite;

  int dimension;

  int dimGauche;

  int dimDroite;

 public:

  DualArmSystem(TaskSystem * gauche, TaskSystem * droite);

  int getDimState() const;

  Eigen::VectorXd getState() const;

  void setState(const Eigen::VectorXd & state);

  void takeCommand(const Eigen::VectorXd & order);

  void updateTime(double t);

  double getPeriod() const;
  
};

#endif
