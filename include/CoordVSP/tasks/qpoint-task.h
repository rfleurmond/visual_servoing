#ifndef QPOINT_TASK_H_
#define QPOINT_TASK_H_

#include "task.h"

/**
   @brief A Task which gives a constant control law

   @author Renliw Fleurmond
 */

class QPointTask: public Task{

 private:

  double periode;

  Eigen::VectorXd desir;
  
  Eigen::MatrixXd activation;
  

 public:

  QPointTask(const Eigen::VectorXd & qWanted);
  
  QPointTask(const Eigen::VectorXd & qWanted, const Eigen::MatrixXd & acti);
  
  void setWantedState(const Eigen::VectorXd & q);

  void setActivationMatrix(const Eigen::MatrixXd & a);

  void setState(const Eigen::VectorXd & q);
  
  Eigen::VectorXd getState() const;

  Eigen::VectorXd fonction(const Eigen::VectorXd & q) const;

  Eigen::MatrixXd jacobien(const Eigen::VectorXd & q) const;

  double getPeriod() const;
  
  void setPeriod(double p);

  bool hasChanged() const;
  
  
};

#endif
