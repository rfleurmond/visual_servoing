#ifndef SEQUENCY_TAKS_H_
#define SEQUENCY_TAKS_H_

#include "task.h"
#include <queue>

/**
   @brief Class which is able to sequence temporally a queue of tasks but can be manipulated like a Task

   @author Renliw Fleurmond
 */


class SequencyTask: public Task{

 private:

  Task * activeTask;

  std::queue<Task * > file;

  void changeActiveTask();
  
 public:

  SequencyTask(Task * first, Task * second);
  
  void setState(const Eigen::VectorXd & q);

  Eigen::VectorXd getState() const;

  Eigen::VectorXd fonction(const Eigen::VectorXd & q) const;

  Eigen::MatrixXd jacobien(const Eigen::VectorXd & q) const;

  Eigen::VectorXd calculCommandeFor(const Eigen::VectorXd & erreur) const;
  
  void recordCommand(const Eigen::VectorXd & dotQ);

  void addTaskInStack(Task * nTask);

  int getTasksNumber() const;

  double getPeriod() const;

  bool hasChanged() const;

  bool isComplete() const;

  double getLambda() const;
  
 
};

#endif
