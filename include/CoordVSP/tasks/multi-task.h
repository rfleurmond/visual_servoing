#ifndef MULTI_TAKS_H_
#define MULTI_TAKS_H_

#include "task.h"

/**
   @brief A collection of tasks as one single Task

   @author Renliw Fleurmond
 */


class MultiTask: public Task{

 private:

  int n; // Nombre de taches

  Task ** tableau;

  bool defined;
  
 public:

  MultiTask(int number, Task * first);

  ~MultiTask();

  void checkAndComplete();

  void assingTaskToIndex(int i, Task * t);
  
  void setState(const Eigen::VectorXd & q);

  Eigen::VectorXd getState() const;

  Eigen::VectorXd fonction(const Eigen::VectorXd & q) const;

  Eigen::MatrixXd jacobien(const Eigen::VectorXd & q) const;

  Eigen::VectorXd calculCommandeFor(const Eigen::VectorXd & erreur) const;

  double getPeriod() const;

  bool hasChanged() const;
  
  
};

#endif
