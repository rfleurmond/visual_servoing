#ifndef MY_CLOCK_H_
#define MY_CLOCK_H_

/**
   @brief A virtual universal clock

   @author Renliw Fleurmond
 */

class MyClock{

 private:

  static double begin;

  static double heure;

  static bool hasStarted;

  static bool isExternal;

 public:

  static void start();
  
  static void start(double t);
  
  static bool setTime(double t);

  static void addToTime(double dt);

  static double getTime();

};

#endif
