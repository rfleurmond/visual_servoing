#ifndef TASK_H_
#define TASK_H_

#include "../tools.h"

/**
   @brief  Base class for tasks, no jacobian needed

   @author Renliw Fleurmond
 */


class TaskFunction{

 protected:

  int dimInput;

  int dimOutput;

 public:

  TaskFunction();

  virtual ~TaskFunction(){};

  int getDimInput() const;

  int getDimOutput() const;

  virtual Eigen::VectorXd fonction(const Eigen::VectorXd & ) const = 0;

};


/**
   @brief Class to deal with use of Adaptive Gain 

   Inspired by work (Mansard, Marchand ,etc ) in ViSP lagadic website

   @author Renliw Fleurmond
 */

class AdaptiveGain{

 private:

  double lZero;

  double lInfini;

  double ltemporel;

  double lastNorm;

  double lambda;

 public:

  AdaptiveGain(double l8 = 1, double l0 = 10);

  double getLambda() const;

  void updateNorm( double );

  void reset();

};
  



/**
   @brief Base class of task function with jacobian 

   according to the task function approach [Samson, Espiau and Le Borgne 1991]

   @author Renliw Fleurmond
 */

class Task: public TaskFunction{

 protected:

  bool isAdaptive;

  bool changement;

  double lambda;

  AdaptiveGain lambdaVariable;

  double borneInferieure;

  double tempo;

  Eigen::VectorXd etat_q;

  Eigen::VectorXd dot_q;

  
 public:

  Task();

  virtual ~Task(){};

  void setLambda(double l);

  void setLambda(AdaptiveGain ll);

  virtual double getLambda() const;

  virtual void setState(const Eigen::VectorXd & q);

  void updateGain();

  void start();

  Eigen::VectorXd getState() const;

  virtual void updateTime(double t);

  virtual void recordCommand(const Eigen::VectorXd & dotQ);

  virtual Eigen::VectorXd valeur() const;

  virtual Eigen::MatrixXd jacobien(const Eigen::VectorXd & q) const = 0;

  virtual Eigen::VectorXd calculCommande() const;

  virtual Eigen::VectorXd calculCommandeFor(const Eigen::VectorXd & erreur) const;

  virtual double getPeriod() const = 0;

  virtual bool hasChanged() const = 0;

  virtual double getMinimumErrorNorm() const;

  virtual void setMinimumErrorNorm( double m);

  virtual bool isComplete() const;

};

/**

   @brief Gradient Task 

   @author Renliw Fleurmond

*/

class GradientTask: public Task{

 private:

  Task * tache;

 public:

  GradientTask();

  virtual ~GradientTask(){};

  virtual void setBaseTask(Task * base);

  virtual double getHfunction(const Eigen::VectorXd & s) const = 0;

  virtual Eigen::VectorXd getGradient(const Eigen::VectorXd & s) const = 0;

  virtual Eigen::VectorXd calculCommande() const;

  virtual Eigen::MatrixXd jacobien(const Eigen::VectorXd & q) const;

  virtual Eigen::VectorXd calculCommandeFor(const Eigen::VectorXd & erreur) const;

  virtual double getPeriod() const;


};

/**

   @brief Class to convert class TaskFunction into class Task

   by computing (brute force) the corresponding jacobian

   @author Renliw Fleurmond

*/


class ApproximateTask: public TaskFunction{
  
 private:

  TaskFunction * tache;

  double pas;

 public:

  ApproximateTask(TaskFunction * t, double pas);

  virtual Eigen::VectorXd fonction(const Eigen::VectorXd &) const;

  virtual Eigen::MatrixXd jacobien(const Eigen::VectorXd &) const;
  
 
};

#endif
