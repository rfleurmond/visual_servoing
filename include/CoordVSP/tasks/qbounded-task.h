#ifndef QBOUNDED_TASK_H_
#define QBOUNDED_TASK_H_

#include "task.h"

/**
   @brief A Task which grow when joints are in the vicinity of their limits

   @author Renliw Fleurmond
 */


class QBoundedTask: public Task{

 private:

  double coeffMin;

  double coeffMax;

  double alpha;

  double MAX;
  
  double periode;

  Eigen::VectorXd min;
  
  Eigen::VectorXd max;

  Eigen::MatrixXd activation;


 public:

  QBoundedTask(const Eigen::VectorXd & qMin, const Eigen::VectorXd & qMax);
  
  QBoundedTask(const Eigen::VectorXd & qMin, const Eigen::VectorXd & qMax, const Eigen::MatrixXd & a);
  
  void setState(const Eigen::VectorXd & q);
  
  Eigen::VectorXd getState() const;

  Eigen::VectorXd fonction(const Eigen::VectorXd & q) const;

  Eigen::MatrixXd jacobien(const Eigen::VectorXd & q) const;

  double getPeriod() const;
  
  void setPeriod(double p);

  void setCoeffMax(double);

  void setCoeffMin(double);
  
  void setAlpha(double);

  void setMax(double);

  bool hasChanged() const;
  
  
};

#endif
