#ifndef NOISY_SYSTEM_H_
#define NOISY_SYSTEM_H_

#include "task-system.h"

/**
   @brief A class which add pseudo-gaussian noise to a controllable system TaskSystem

   @author Renliw Fleurmond
 */


class NoisySystem: public TaskSystem{

 private:

  Eigen::MatrixXd cov;
 
  TaskSystem * realSys;

  MultivariateGaussian * roulette;

 public:

  NoisySystem(TaskSystem * sys, const Eigen::MatrixXd & cov);

  NoisySystem(const NoisySystem & other);

  NoisySystem & operator=(const NoisySystem & other);

  ~NoisySystem();

  int getDimState() const;

  Eigen::VectorXd getState() const;

  void setState(const Eigen::VectorXd & state);

  void takeCommand(const Eigen::VectorXd & order);

  void updateTime(double t);

  double getPeriod() const;
  
};

#endif
