#ifndef OPTIMA_PRIORITY_TAKS_H_
#define OPTIMA_PRIORITY_TAKS_H_

#include "priority-task.h"

/**
   @brief PriorityTask which satisfy at the best the two tasks but the first has the priority

   @author Renliw Fleurmond
 */

class OptimaPriorityTask: public PriorityTask{

 public:

  OptimaPriorityTask(Task * master, Task * slave);
  
  Eigen::VectorXd calculCommandeFor(const Eigen::VectorXd & erreur) const;
 
};


#endif
