#ifndef CO_TAKS_H_
#define CO_TAKS_H_

#include "duality-task.h"

/**
   @brief DualityTask which simply add the control laws of the two Task

   @author Renliw Fleurmond
 */

class CoTask: public DualityTask{

 public:

  CoTask(Task * brother1, Task * brother2);
  
  Eigen::VectorXd calculCommandeFor(const Eigen::VectorXd & erreur) const;

  double getPeriod() const;

  bool hasChanged() const;
  
};

#endif
