#ifndef PRIORITY_TAKS_H_
#define PRIORITY_TAKS_H_

#include "duality-task.h"

/**
   @brief DualityTask which satisfy the first Task and tries to satisfy the second one

   @author Renliw Fleurmond
 */


class PriorityTask: public DualityTask{

 public:
  
  PriorityTask(Task * master, Task * slave);
  
  Eigen::VectorXd getState() const;

  virtual Eigen::VectorXd calculCommandeFor(const Eigen::VectorXd & erreur) const;

  static Eigen::MatrixXd projecteur(const Eigen::MatrixXd & J);

  virtual void hideTaskOnControl(Task * t);
 
};

#endif
