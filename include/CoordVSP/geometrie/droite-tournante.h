#ifndef DROITE_TOURNANTE_H_
#define DROITE_TOURNANTE_H_

#include "architect.h"

/**

   @brief An implementation of the geometric concept of a straight line Droite turning around an axis

   @author Renliw Fleurmond
 
 */


class DroiteTournante: public Lieu{

 protected:

  Eigen::Vector3d direction;
  Eigen::Vector3d rotation;
  Eigen::Vector3d moyeu;

  double a;
  double b;
  double c;

  void calculate();
  
 public :

  //EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  

  DroiteTournante();
  
  std::list<Eigen::Vector3d> getVectors() const;

  void setVectors(const std::list<Eigen::Vector3d> &);
   
  int getDimensions() const;
  
  int getDimEspace() const;

  bool isVolumic() const ;

  bool isSurface() const;

  bool hasPoint(Eigen::Vector3d p) const ;

  Eigen::Vector3d getNearestPoint(Eigen::Vector3d) const;

  void setDirection(Eigen::Vector3d d);
  
  Eigen::Vector3d getDirection() const;

  virtual void affiche(std::ostream&) const;

  Eigen::Vector3d getRotation() const;

  void setRotation(Eigen::Vector3d);

  void setDroite(Droite d);
  
  Cercle getCircleCenter() const;

  Droite getAxeRotation() const;

  std::list<Droite> getSuitableDroites(Eigen::Vector3d P) const;

  Eigen::Vector3d getMoyeu(Eigen::Vector3d direction) const;
  
  Eigen::Vector3d getDirection(Eigen::Vector3d moyeu) const;
  

};


#endif
