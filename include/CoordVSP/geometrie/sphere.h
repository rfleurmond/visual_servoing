#ifndef SPHERE_H_
#define SPHERE_H_

#include "lieu.h"

/**

   @brief An implementation of the geometric concept of sphere

   @author Renliw Fleurmond
 
 */


class Sphere: public Lieu{

 protected:

  double rayon;

 public :

  Sphere();

  Sphere(Eigen::Vector3d c, double r);

  virtual std::list<Eigen::Vector3d> getVectors() const;

  virtual void setVectors(const std::list<Eigen::Vector3d> &);
   
  virtual int getDimensions() const;
  
  virtual int getDimEspace() const;

  virtual bool isVolumic() const ;

  virtual bool isSurface() const;

  virtual bool hasPoint(Eigen::Vector3d p) const ;

  virtual Eigen::Vector3d getNearestPoint(Eigen::Vector3d) const;

  void setRayon(double r);

  double getRayon() const;

  virtual void affiche(std::ostream&) const;

  bool isSameAs(const Sphere & other) const;

};

#endif
