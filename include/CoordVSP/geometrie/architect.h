#ifndef ARCHITECT_H_
#define ARCHITECT_H_

#include "lpoint.h"
#include "disque.h"
#include "cylindre.h"
#include "tore.h"

/**

   @brief Compute the result of intersection or combination of many geometric objects

   @author Renliw Fleurmond

 */

class Architect{

 public:

  static std::list<Plan> combinaison(const LPoint & p, const Droite & d);

  static std::list<Plan> combinaison(const Droite & d1, const Droite & d2);

  static Tore combinaison(const Cercle & centre, const Cercle & motif);

  static PlanTournant combinaison(const Cercle & centre, const Plan & motif);

  static Cylindre combinaison(const Droite & translation, const Cercle & motif);

  static std::list<LPoint> intersection(const Droite & d1, const Droite & d2);

  static std::list<LPoint> intersection(const Droite & d1, const Plan & p2);

  static std::list<LPoint> intersection(const Droite & d1, const Cercle & c2);

  static std::list<LPoint> intersection(const Droite & d1, const Sphere & d2);

  static std::list<Droite> intersection(const Plan & p1, const Plan & p2);

  static std::list<Cercle> intersection(const Plan & p1, const Sphere & s2);

  static std::list<Disque> intersection(const Plan & p1, const Boule & b2);

  static std::list<LPoint> intersection(const Plan & p1, const Cercle & c2); 

  static std::list<LPoint> intersection(const Cercle & c1, const Cercle & b2);

  static std::list<LPoint> intersection(const Cercle & c1, const Sphere & s2);

  static std::list<Cercle> intersection(const Sphere & s1, const Sphere & b2);

  static std::list<Disque> intersection(const Boule & b1, const Boule & b2);

  static Eigen::Vector3d antiProjection(Eigen::Vector3d direction,Eigen::Vector3d sujet);

  static Eigen::Vector3d projection(Eigen::Vector3d direction,Eigen::Vector3d sujet);


};

#endif
