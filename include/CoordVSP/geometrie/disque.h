#ifndef DISQUE_H_
#define DISQUE_H_

#include "cercle.h"
#include "boule.h"

/**

   @brief An implementation of the geometric concept of disc surrounded by a circle Cercle

   @author Renliw Fleurmond
 
 */


class Disque: public Cercle{

 public :

  //EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  Disque();

  Disque(Cercle);

  std::list<Eigen::Vector3d> getVectors() const;

  void setVectors(const std::list<Eigen::Vector3d> &);
   
  int getDimensions() const;
  
  int getDimEspace() const;

  bool isVolumic() const ;

  bool isSurface() const;

  bool hasPoint(Eigen::Vector3d p) const ;

  Eigen::Vector3d getNearestPoint(Eigen::Vector3d) const;

  virtual void affiche(std::ostream&) const;

  Cercle getContour();

  bool isSameAs(Disque & other);

};


#endif
