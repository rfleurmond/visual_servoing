#ifndef CONE_H_
#define CONE_H_

#include "cercle.h"

/**

   @brief An implementation of the geometric concept of a Cone

   @author Renliw Fleurmond
 
 */


class Cone: public Lieu{

 protected:

  double angle;
  Eigen::Vector3d rotation;

 public :

  //EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  
 Cone():
  Lieu(),
    angle(1.0),
    rotation(Eigen::Vector3d(0,0,1))
      {
	etat = EXISTS;
	type = CONE;
      }
  
  std::list<Eigen::Vector3d> getVectors() const;

  void setVectors(const std::list<Eigen::Vector3d> &);
   
  int getDimensions() const;
  
  int getDimEspace() const;

  bool isVolumic() const ;

  bool isSurface() const;

  bool hasPoint(Eigen::Vector3d p) const ;

  Eigen::Vector3d getNearestPoint(Eigen::Vector3d) const;

  void setAngle(double r);

  double getAngle() const;

  void affiche(std::ostream&) const;

  Eigen::Vector3d getRotation() const;

  void setRotation(Eigen::Vector3d);

  Droite getAxeRotation();

};


#endif
