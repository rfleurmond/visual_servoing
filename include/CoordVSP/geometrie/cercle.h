#ifndef CERCLE_H_
#define CERCLE_H_

#include "plan.h"
#include "sphere.h"

/**

   @brief An implementation of the geometric concept of circle,

   which can be defined by intersection of a Plan and a Sphere

   @author Renliw Fleurmond
 
 */


class Cercle: public Sphere, public Planar{

 protected:

  Eigen::Vector3d direction;
 
 public :

  //EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  Cercle();

  std::list<Eigen::Vector3d> getVectors() const;

  void setVectors(const std::list<Eigen::Vector3d> &);
   
  virtual int getDimensions() const;
  
  virtual int getDimEspace() const;

  virtual bool isVolumic() const ;

  virtual bool isSurface() const;

  virtual bool hasPoint(Eigen::Vector3d p) const ;

  virtual Eigen::Vector3d getNearestPoint(Eigen::Vector3d) const;

  void setDirection(Eigen::Vector3d d);
  
  Eigen::Vector3d getDirection() const;

  virtual void affiche(std::ostream&) const;

  Plan getPlan() const;

  Sphere getSphere() const;

  bool isSameAs(Cercle & other);


};


#endif
