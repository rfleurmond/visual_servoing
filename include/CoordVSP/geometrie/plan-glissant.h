#ifndef PLAN_GLISSANT_H_
#define PLAN_GLISSANT_H_

#include "plan.h"

/**

   @brief An implementation of the geometric concept of sliding Plan e along a straight line

   @author Renliw Fleurmond
 
 */


class PlanGlissant: public Lieu{

 protected:

  Eigen::Vector3d direction;
 
 public :

  //EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  
  
 PlanGlissant():
  Lieu(),
    direction(Eigen::Vector3d(0,0,1))
      {
	etat = EXISTS;
	type = PLANGLISSANT;
      }

 PlanGlissant(Plan p):
  Lieu(p.getCenter()),
    direction(p.getDirection())
      {
	etat = EXISTS;
	type = PLANGLISSANT;
	direction = p.getDirection();
	centre = p.getCenter();
      }
  
  std::list<Eigen::Vector3d> getVectors() const;

  void setVectors(const std::list<Eigen::Vector3d> &);
   
  int getDimensions() const;
  
  int getDimEspace() const;

  bool isVolumic() const ;

  bool isSurface() const;

  bool hasPoint(Eigen::Vector3d p) const ;

  Eigen::Vector3d getNearestPoint(Eigen::Vector3d) const;

  void setDirection(Eigen::Vector3d d);
  
  Eigen::Vector3d getDirection() const;

  virtual void affiche(std::ostream&) const;

  void setPlan(Plan p);

  Plan getPlan(Eigen::Vector3d P);

  
};

#endif
