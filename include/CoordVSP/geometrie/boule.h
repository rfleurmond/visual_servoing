#ifndef BOULE_H_
#define BOULE_H_

#include "sphere.h"

/**

   @brief An implementation of the geometric concept of boule, volume delimited by a Sphere

   @author Renliw Fleurmond
 
 */


class Boule: public Sphere{

 public :

  Boule();

  Boule(Eigen::Vector3d c, double r);

  Boule(Sphere s);

  std::list<Eigen::Vector3d> getVectors() const;

  void setVectors(const std::list<Eigen::Vector3d> &);
   
  int getDimensions() const;
  
  int getDimEspace() const;

  bool isVolumic() const ;

  bool isSurface() const;

  bool hasPoint(Eigen::Vector3d p) const ;

  Eigen::Vector3d getNearestPoint(Eigen::Vector3d) const;

  void affiche(std::ostream&) const;

  Sphere getContour() const;

  bool isSameAs(const Boule & other) const;


};


#endif
