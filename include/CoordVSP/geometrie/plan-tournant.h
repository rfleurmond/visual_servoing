#ifndef PLAN_TOURNANT_H_
#define PLAN_TOURNANT_H_

#include "cercle.h"

/**

   @brief An implementation of the geometric concept of Plan e turning around an axis

   @author Renliw Fleurmond
 
 */


class PlanTournant: public Lieu{

 protected:

  Eigen::Vector3d direction;
  Eigen::Vector3d rotation;
  double distance;

 public :

  //EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  

 PlanTournant():
  Lieu(),
    direction(Eigen::Vector3d(1,0,0)),
    rotation(Eigen::Vector3d(0,0,1)),
    distance(1.0)
      {
	etat = EXISTS;
	type = PLANTOURNANT;
      }
  
  std::list<Eigen::Vector3d> getVectors() const;

  void setVectors(const std::list<Eigen::Vector3d> &);
   
  int getDimensions() const;
  
  int getDimEspace() const;

  bool isVolumic() const ;

  bool isSurface() const;

  bool hasPoint(Eigen::Vector3d p) const ;

  Eigen::Vector3d getNearestPoint(Eigen::Vector3d) const;

  void setDirection(Eigen::Vector3d d);
  
  Eigen::Vector3d getDirection() const;

  void setDistance(double r);

  double getDistance() const;

  virtual void affiche(std::ostream&) const;

  Eigen::Vector3d getRotation() const;

  void setRotation(Eigen::Vector3d);

  void setPlan(Plan p);

  Droite getAxeRotation();

  /**
     Retourne le(s) plans qui contiennent le point p
  */
  std::list<Plan> getSuitablePlans(Eigen::Vector3d p, int * n);

};

#endif
