#ifndef DROITE_H_
#define DROITE_H_

#include "lieu.h"

/**

   @brief An implementation of the geometric concept of a 3D straight line

   @author Renliw Fleurmond
 
 */


class Droite:  public Lieu{

 protected:

  Eigen::Vector3d direction;

 public :

  //EIGEN_MAKE_ALIGNED_OPERATOR_NEW

 Droite():
  Lieu(),
    direction(Eigen::Vector3d(0,0,1))
    {
      etat = EXISTS;
      type = DROITE;
      centre <<0,0,0;
    }

 Droite(Eigen::Vector3d c, Eigen::Vector3d direc):
  Lieu(c),
    direction(direc)
    {
      etat = EXISTS;
      type = DROITE;
      setDirection(direc);
    }

  std::list<Eigen::Vector3d> getVectors() const;

  void setVectors(const std::list<Eigen::Vector3d> &);
   
  int getDimensions() const;
  
  int getDimEspace() const;

  bool isVolumic() const ;

  bool isSurface() const;

  bool hasPoint(Eigen::Vector3d p) const ;

  Eigen::Vector3d getNearestPoint(Eigen::Vector3d) const;

  void setDirection(Eigen::Vector3d d);
  
  Eigen::Vector3d getDirection() const;

  virtual void affiche(std::ostream&) const;


};


#endif
