#ifndef LPOINT_H_
#define LPOINT_H_

#include "lieu.h"

/**

   @brief An implementation of the geometric concept of point

   @author Renliw Fleurmond
 
 */


class LPoint: public Lieu{

 public: 
  
 LPoint():
  Lieu()
    {
      centre <<0,0,0;
      etat = EXISTS;
      type = POINT;
    }

 LPoint(Eigen::Vector3d p):
  Lieu()
    {
      centre = p;
      etat = EXISTS;
      type = POINT;
    }


  std::list<Eigen::Vector3d> getVectors() const;

  void setVectors(const std::list<Eigen::Vector3d> &);
   
  int getDimensions() const;
  
  int getDimEspace() const;

  bool isVolumic() const ;

  bool isSurface() const;

  bool hasPoint(Eigen::Vector3d p) const ;

  Eigen::Vector3d getNearestPoint(Eigen::Vector3d) const;

  virtual void affiche(std::ostream&) const;


};



#endif
