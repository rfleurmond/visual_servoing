#ifndef TORE_H_
#define TORE_H_

#include "plan-tournant.h"

/**

   @brief An implementation of the geometric concept of tore

   @author Renliw Fleurmond
 
 */

class Tore: public Lieu{

 private:
  
  double a;
  double b;
  double c;
  void calculate();

 protected:

  Eigen::Vector3d moyeu;
  Eigen::Vector3d direction;
  Eigen::Vector3d rotation;
  double distance;
  double rayon;

 public :

  //EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  
  Tore();
  
  std::list<Eigen::Vector3d> getVectors() const;

  void setVectors(const std::list<Eigen::Vector3d> & );
   
  int getDimensions() const;
  
  int getDimEspace() const;

  bool isVolumic() const ;

  bool isSurface() const;

  bool hasPoint(Eigen::Vector3d p) const ;

  Eigen::Vector3d getNearestPoint(Eigen::Vector3d) const;

  Eigen::Vector3d getDirection() const;

  void setDistance(double r);

  double getDistance() const;

  virtual void affiche(std::ostream&) const;

  Eigen::Vector3d getRotation() const;

  void setRotation(Eigen::Vector3d);

  void setCercle(Cercle p);

  Cercle getCircleCenter() const;

  Droite getAxeRotation() const;

  PlanTournant getPlanTournant() const;

  Eigen::Vector3d getMoyeu(Eigen::Vector3d direction) const;
  
  Eigen::Vector3d getDirection(Eigen::Vector3d moyeu) const;

  std::list<Cercle> getSuitableCircles(Eigen::Vector3d p, int * n);
  
  
};

#endif
