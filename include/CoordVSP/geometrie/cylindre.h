#ifndef CYLINDRE_H_
#define CYLINDRE_H_

#include "cercle.h"

/**

   @brief An implementation of the geometric concept of cylindre

   @author Renliw Fleurmond
 
 */


class Cylindre: public Lieu{

 protected:

  Eigen::Vector3d direction;
  Eigen::Vector3d translation;
  double rayon;

 public :
  
  //EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  

 Cylindre():
  Lieu(),
    direction(Eigen::Vector3d(0,0,1)),
    translation(Eigen::Vector3d(0,0,1)),
    rayon(1.0)
      {
	etat = EXISTS;
	type = CYLINDRE;
	direction <<0,0,1;
	translation = direction;
	rayon = 1;
      }
  
 Cylindre(Cercle c, Eigen::Vector3d t):
  Lieu(c.getCenter()),
    direction(c.getDirection()),
    translation(t),
    rayon(c.getRayon())
      {
	etat = EXISTS;
	type = CYLINDRE;
	setDirection(c.getDirection());
	setTranslation(t);
	setCenter(c.getCenter());
      }

  std::list<Eigen::Vector3d> getVectors() const;

  void setVectors(const std::list<Eigen::Vector3d> & );
   
  int getDimensions() const;
  
  int getDimEspace() const;

  bool isVolumic() const ;

  bool isSurface() const;

  bool hasPoint(Eigen::Vector3d p) const ;

  Eigen::Vector3d getNearestPoint(Eigen::Vector3d) const;

  void setDirection(Eigen::Vector3d d);
  
  Eigen::Vector3d getDirection() const;

  void setRayon(double r);

  double getRayon() const;

  virtual void affiche(std::ostream&) const;

  Droite getDroite() const;

  Eigen::Vector3d getTranslation() const;

  void setTranslation(Eigen::Vector3d);

  Cercle getCercle(Eigen::Vector3d) const;

};


#endif
