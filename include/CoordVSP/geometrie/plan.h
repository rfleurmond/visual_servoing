#ifndef PLAN_H_
#define PLAN_H_

#include "droite.h"

/**

   @brief An implementation of the geometric concept of plane

   @author Renliw Fleurmond
 
 */


class Plan: public Lieu{

 protected:

  Eigen::Vector3d direction;

 public :

 Plan():
  Lieu(),
    direction(Eigen::Vector3d(0,0,1))
      {
	etat = EXISTS;
	type = PLAN;
      }

  //EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  std::list<Eigen::Vector3d> getVectors() const;

  void setVectors(const std::list<Eigen::Vector3d> & );
   
  int getDimensions() const;
  
  int getDimEspace() const;

  bool isVolumic() const ;

  bool isSurface() const;

  bool hasPoint(Eigen::Vector3d p) const ;

  Eigen::Vector3d getNearestPoint(Eigen::Vector3d) const;

  void setDirection(Eigen::Vector3d d);
  
  Eigen::Vector3d getDirection() const;

  virtual void affiche(std::ostream&) const;

  bool contains(Droite d) const;
  
  bool isSameAs(Plan & other);

};


/**

   @brief An interface which should be inherited by planar geometrical objects

   @author Renliw Fleurmond
 
 */


class Planar{

 public:

  virtual ~Planar(){};

  virtual Plan getPlan() const = 0;

};

#endif
