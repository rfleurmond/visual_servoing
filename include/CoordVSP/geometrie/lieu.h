#ifndef LIEU_H_
#define LIEU_H_

#define CRAYON 1e-10

#include "../algebre.h"

/**

   @brief Base class of geometric objects defining the concept of geometric locus

   @author Renliw Fleurmond
 
 */


class Lieu{

 protected:

  int type;
  int etat;
  
  Eigen::Vector3d centre;

  const static int EXISTS = 1000;
  const static int NO_EXISTS = -2;

  
 public:
 
  //EIGEN_MAKE_ALIGNED_OPERATOR_NEW


  const static int NOT_DEFINED = -1;
  const static int POINT = 0;
  const static int DROITE = 1;
  const static int PLAN  = 2;
  const static int CIRCLE = 3;
  const static int DISQUE  = 4;
  const static int CYLINDRE = 5;
  const static int SPHERE = 6;
  const static int BOULE = 7;
  const static int PLANTOURNANT = 8;
  const static int CONE = 9;
  const static int TORE = 10;
  const static int DROITETOURNANTE = 11;
  const static int PLANGLISSANT = 12;

 Lieu():
  type(NOT_DEFINED),
    etat(NOT_DEFINED),
    centre()
      {
      }

 Lieu(Eigen::Vector3d c):
  type(NOT_DEFINED),
    etat(NOT_DEFINED),
    centre(c)
      {
      }

  virtual ~Lieu(){};


  virtual std::list<Eigen::Vector3d> getVectors() const = 0;

  virtual void setVectors(const std::list<Eigen::Vector3d> &) = 0;
  
  virtual int getDimensions() const = 0;
  
  virtual int getDimEspace() const = 0;

  bool isLinear() const;

  bool isPlanar() const;

  bool isSpacial() const;

  virtual bool isVolumic() const  = 0;

  virtual bool isSurface() const = 0;

  virtual bool hasPoint(Eigen::Vector3d p) const  = 0;

  double getLowestDistance(Eigen::Vector3d p) const;

  virtual Eigen::Vector3d getNearestPoint(Eigen::Vector3d) const = 0;

  void  getTransformation(Repere se3);

  int getType();
      
  Eigen::Vector3d getCenter() const;

  void setCenter(Eigen::Vector3d c);

  bool exists();

  bool isDefined();

  virtual void affiche(std::ostream&) const = 0;

  std::string static getStringType(int type);

  std::string afficheType() ;

};

std::ostream& operator << (std::ostream& flux, Lieu & l);

#endif
