#include "graphiques-opencv.h"

using namespace Eigen;
using namespace std;

int Palette::nbColors = 0;


cv::Scalar Palette::getLastColor(){
  nbColors++;
  cout << "Number of colors = " <<nbColors<<endl;

  cv::Scalar couleur;

  int a = 0, b = 0 , c = 0;
  if((nbColors/4) % 2 == 1) c = 128;
  if((nbColors/2) % 2 == 1) b = 128;
  if( nbColors    % 2 == 1) a = 128;
  couleur = cv::Scalar(a,c,b);

  return couleur;
}

cv::Scalar Palette::getLightColor(){
  cv::Scalar couleur;

  int a = 128, b = 128 , c = 128;
  if((nbColors/4) % 2 == 1) c = 255;
  if((nbColors/2) % 2 == 1) b = 255;
  if( nbColors    % 2 == 1) a = 255;
  couleur = cv::Scalar(a,c,b);

  return couleur;
}


CVEcran::CVEcran():
  windows("Image Camera"),
  couleurFond(cv::Scalar(238,238,238)),
  couleurGrille(cv::Scalar(200,200,200)),
  couleurRepere(cv::Scalar(128,128,128)),
  largeur(600),
  hauteur(600),
  image(cv::Mat::zeros(600, 600,CV_8UC3)),
  ratioX(300),
  ratioY(300),
  maille(8),
  optique()
{
  std::cout<<"Creation ecran      :"<<windows<<std::endl;
}


CVEcran::CVEcran(CVEcran const & autre):
  windows(autre.windows),
  couleurFond(autre.couleurFond),
  couleurGrille(autre.couleurGrille),
  couleurRepere(autre.couleurRepere),
  largeur(autre.largeur),
  hauteur(autre.hauteur),
  image(autre.image),
  ratioX(autre.ratioX),
  ratioY(autre.ratioY),
  maille(autre.maille),
  optique(autre.optique)
{
  std::cout<<"Copie constructeur E:"<<windows<<std::endl;
}

CVEcran::CVEcran(string nom):
  windows(nom),
  couleurFond(cv::Scalar(238,238,238)),
  couleurGrille(cv::Scalar(200,200,200)),
  couleurRepere(cv::Scalar(128,128,128)),
  largeur(600),
  hauteur(600),
  image(cv::Mat::zeros(600, 600,CV_8UC3)),
  ratioX(300),
  ratioY(300),
  maille(8),
  optique()
{
  std::cout<<"Creation ecran      :"<<windows<<std::endl;
}


CVEcran::CVEcran(string nom, int l, int h):
  windows(nom),
  couleurFond(cv::Scalar(238,238,238)),
  couleurGrille(cv::Scalar(200,200,200)),
  couleurRepere(cv::Scalar(128,128,128)),
  largeur(l),
  hauteur(h),
  image(cv::Mat::zeros(600, 600,CV_8UC3)),
  ratioX(1.0*l),
  ratioY(1.0*h),
  maille(8),
  optique()
{
  std::cout<<"Creation ecran      :"<<windows<<std::endl;
}

CVEcran::~CVEcran(){
  std::cout<<"Destruction CVECran :"<<windows<<std::endl;
  std::cout<<"Destruction CVECran :";
  fermerFenetre();
}

void CVEcran::setNom(string nom){
  windows = nom;
}

string  CVEcran::getNom() const{
  return windows;
}

void CVEcran::setDimensions(int larg, int haut){
  largeur = larg;
  hauteur = haut;
  ratioX = (largeur * 1.0)/optique.largeur;
  ratioY = (hauteur * 1.0)/optique.hauteur;
  image = cv::Mat::zeros(hauteur, largeur,CV_8UC3);
}

void CVEcran::setOptique(IntrinsicCam opt){
  optique = opt;
  largeur = static_cast<int>(round(optique.largeur));
  hauteur = static_cast<int>(round(optique.hauteur));
  if(isEqual(optique.alphaU,1,1e-6) && 
     isEqual(optique.alphaV,1,1e-6)){
    setDimensions(static_cast<int>(round(300*optique.largeur)),
		  static_cast<int>(round(300*optique.hauteur)));
  }
  ratioX = (largeur * 1.0)/optique.largeur;
  ratioY = (hauteur * 1.0)/optique.hauteur;
  image = cv::Mat::zeros(hauteur, largeur,CV_8UC3);
  
  cout << "U0                = "<<opt.u0<<endl;
  cout << "V0                = "<<opt.v0<<endl;
  cout << "alphaU            = "<<opt.alphaU<<endl;
  cout << "alphaV            = "<<opt.alphaV<<endl;
  cout << "Largeur Retine    = "<<opt.largeur<<endl;
  cout << "Hauteur Retine    = "<<opt.hauteur<<endl;
  cout << "Largeur Affichage = "<<largeur<<endl;
  cout << "Hauteur Affichage = "<<hauteur<<endl;

}

void CVEcran::ouvrirFenetre(){
  cv::namedWindow(windows.c_str());
  std::cout<<"Ouverture fenetre   :"<<windows<<std::endl;
  nettoyer();
}

void CVEcran::fermerFenetre(){
  cv::destroyWindow(windows.c_str());
  std::cout<<"Fermeture fenetre   :"<<windows<<std::endl;
}

void CVEcran::nettoyer(){
  image = couleurFond;
  int pasX,pasY;
  pasX = largeur/maille;
  pasY = hauteur/maille;
  cv::Point debut,fin;

  for(int k = 1; k < maille;k++){
    debut.x = k*pasX;
    debut.y = 0;
    fin.x = k*pasX;
    fin.y = hauteur;
    line(image,debut,fin,couleurGrille,1,8);
    debut.y = k*pasY;
    debut.x = 0;
    fin.y = k*pasY;
    fin.x = largeur;
    line(image,debut,fin,couleurGrille,1,8);
  }

  debut.x = largeur/2;
  debut.y = 0;
  fin.x = largeur/2;
  fin.y = hauteur;
  line(image,debut,fin,couleurRepere,2,8);
  debut.y = hauteur/2;
  debut.x = 0;
  fin.y = hauteur/2;
  fin.x = largeur;
  line(image,debut,fin,couleurRepere,2,8);
}

cv::Mat & CVEcran::getImage(){
  return image;
}


void CVEcran::setImage(cv::Mat img){
  image = img;
  largeur = image.cols;
  hauteur = image.rows;
  ratioX = (largeur * 1.0)/optique.largeur;
  ratioY = (hauteur * 1.0)/optique.hauteur;
}


Vector2d CVEcran::getPoint(Vector2d A) const{
  Vector2d B = A;
  double x,y;
  if(isEqual(optique.alphaU,1,1e-6) && 
     isEqual(optique.alphaV,1,1e-6)){

    x = round(largeur/2 + ratioX * A(0) );
    y = round(hauteur/2 + ratioY * A(1) );
  }
  else{
    x = round( ratioX * A(0) );
    y = round( ratioY * A(1) );
  }
  B(0) = static_cast<int>(round(x));
  B(1) = static_cast<int>(round(y));
  return B;
}

Vector2d CVEcran::getVector(Vector2d V) const{
  Vector2d W = V;
  W(0) = static_cast<int>(round(ratioX * W(0)));
  W(1) = static_cast<int>(round(ratioY * W(1)));
  return W;
}

void CVEcran::affichage(){
  cv::imshow(windows,image);
}

int CVEcran::getLargeur() const{
  return largeur;
}

int CVEcran::getHauteur() const{
  return hauteur;
}

