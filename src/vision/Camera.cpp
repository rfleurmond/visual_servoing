/*
 * Camera.cpp
 *
 *  Created on: Jun 6, 2012
 *      @author Renliw Fleurmond
 */

#include "Camera.h"

using namespace Eigen;
using namespace std;

/**
 * Constructeur par defaut
 */
Camera::Camera():
  frame(),
  optique(),
  signeU(Camera::defaultSigneU),
  signeV(Camera::defaultSigneV),
  directionU(Camera::defaultDirectionU),
  directionV(Camera::defaultDirectionV),
  directionZ(Camera::defaultDirectionZ),
  parametres(VectorXd::Zero(10))
{
  parametres(6) = optique.alphaU;
  parametres(7) = optique.alphaV;
  parametres(8) = optique.u0;
  parametres(9) = optique.v0;

}


/**
 * Constructeur prenant en parametre
 *  le changement de repere
 *
 */
Camera::Camera(Repere rep):
  frame(rep),
  optique(),
  signeU(Camera::defaultSigneU),
  signeV(Camera::defaultSigneV),
  directionU(Camera::defaultDirectionU),
  directionV(Camera::defaultDirectionV),
  directionZ(Camera::defaultDirectionZ),
  parametres(VectorXd::Zero(10))
{
  parametres(6) = optique.alphaU;
  parametres(7) = optique.alphaV;
  parametres(8) = optique.u0;
  parametres(9) = optique.v0;

}
/**
 * Constructeur prenant en parametre le changement de repere
 * et les parametre istrinseques
 */
Camera::Camera(Repere rep,IntrinsicCam para):
  frame(rep),
  optique(para),
  signeU(Camera::defaultSigneU),
  signeV(Camera::defaultSigneV),
  directionU(Camera::defaultDirectionU),
  directionV(Camera::defaultDirectionV),
  directionZ(Camera::defaultDirectionZ),
  parametres(VectorXd::Zero(10))
{
  setIntrinsicParameters(para);
}

/**
 * Constructeur prenant en entree les parametres intrinseques de la camera
 *
 */
Camera::Camera(IntrinsicCam para):
  frame(),
  optique(para),
  signeU(Camera::defaultSigneU),
  signeV(Camera::defaultSigneV),
  directionU(Camera::defaultDirectionU),
  directionV(Camera::defaultDirectionV),
  directionZ(Camera::defaultDirectionZ),
  parametres(VectorXd::Zero(10))
{
  setIntrinsicParameters(para);
}


void Camera::setRegard(int typeRegard){
  if(typeRegard==Camera::regardX){
    signeU = defaultSigneU;
    signeV = defaultSigneV;
    directionU = defaultDirectionU;
    directionV = defaultDirectionV;
    directionZ = defaultDirectionZ;
  }

  if(typeRegard==Camera::regardZ){
    signeU = 1;
    signeV = 1;
    directionU = 1;
    directionV = 0;
    directionZ = 2;
  }
}

/**
 * Modifie les parametres de la camera
 */
void Camera::setIntrinsicParameters(const IntrinsicCam & param){
  optique = param;
  parametres(6) = param.alphaU;
  parametres(7) = param.alphaV;
  parametres(8) = param.u0;
  parametres(9) = param.v0;
}

IntrinsicCam Camera::getIntrinsicParameters() const{
  return optique;
}

int Camera::getSigneU() const{
  return signeU;
}

int Camera::getSigneV() const{
  return signeV;
}

int Camera::getDirectionU() const{
  return directionU;
}

int Camera::getDirectionV() const{
  return directionV;
}

int Camera::getDirectionZ() const{
  return directionZ;
}

double Camera::getAlphaU() const{
  return optique.alphaU;
}

double Camera::getAlphaV() const{
  return optique.alphaV;
}


void Camera::setRepere(const Repere & rep){

  frame = rep;
  parametres.block<3,1>(0,0) = frame.getRotation().getVector();
  parametres.block<3,1>(3,0) = frame.getDeplacement();

}

Repere Camera::getRepere() const{

  return frame;

}


/**
 * Retourne vrai si la projection d'un point se fera sur la retine
 *
 */

bool Camera::canSee(const Vector3d & point) const{

  Vector3d pointI;
  pointI = frame.getPointInME(point);

  double d = pointI(this->directionZ);


  if(d<=0){
    return false;
  }
  else{

    double u = this->signeU*this->optique.alphaU * pointI(this->directionU)/d;
    double v = this->signeV*this->optique.alphaV * pointI(this->directionV)/d;
      
    if(isEqual(optique.alphaU,1,1e-6) 
       && isEqual(optique.alphaV,1,1e-6)){

      if(fabs(u) > optique.largeur * 0.5){
	return false;
      }
    
      if(fabs(v) > optique.hauteur * 0.5){
	return false;
      }

    }
    else{

      if(optique.u0 + u > optique.largeur){
	return false;
      }
      if(optique.v0 + v > optique.largeur){
	return false;
      }

      if(optique.u0 + u < 0){
	return false;
      }
      if(optique.v0 + v < 0){
	return false;
      }

    }
  }

  return true;

}

/**
 * Projete l' image d' un point 3d sur un plan pour construire une image 2d
 *
 * c'est le modele du trou d'epingle, "Pinhole"
 */
Vector2d Camera::getProjection(const Vector3d & point) const{

  Vector3d pointI;
  pointI = frame.getPointInME(point);

  double u = this->signeU*this->optique.alphaU * pointI(this->directionU);
  double v = this->signeV*this->optique.alphaV * pointI(this->directionV);
  double d = pointI(this->directionZ);

  if(d>0){
    u/=d;
    v/=d;
  }

  Vector2d image;

  image << u,v;

  return image;

}

/**
 * Retourne la jacobienne de la fonction de projection
 *
 * en fonction:
 *
 * - trois premiere colonnes : l'attitude de la camera
 * - trois suivantes 			: la position de la camera
 * - deux suivantes   		: des parametres intrinseques dans l'ordre alphau et alpha V
 * - trois dernieres			: les coordonnees du point
 *
 */
MatrixXd Camera::getJacobianProjection(const Vector3d & point) const{

  Vector3d pointI;

  pointI = frame.getPointInME(point);

  MatrixXd JP = MatrixXd::Zero(2,5);

  double d = 0;

  d = pointI(this->directionZ);

  if(d<=0){
    d = 1;
  }

  JP.col(0)<< this->signeU * pointI(this->directionU) / d,                                           0;
  JP.col(1)<<                                           0, this->signeV * pointI(this->directionV) / d;
  JP.col(2+this->directionU) <<this->signeU * this->optique.alphaU / d,     0;
  JP.col(2+this->directionV) <<                              0,    this->signeV * this->optique.alphaV / d;
  JP.col(2+this->directionZ) <<
    -this->signeU * pointI(this->directionU) * this->optique.alphaU / pow(d,2),
    -this->signeV * pointI(this->directionV) * this->optique.alphaV / pow(d,2);

  MatrixXd JR = frame.getJacobianPointInME(point);

  MatrixXd JJ = MatrixXd::Zero(2,11);

  MatrixXd facteur = JP.block<2,3>(0,2);

  JJ.block<2,6>(0,0).noalias() = facteur * JR.block<3,6>(0,0);
  JJ.block<2,2>(0,6) = JP.block<2,2>(0,0);
  JJ.block<2,3>(0,8).noalias() = facteur * JR.block<3,3>(0,6);

  return JJ;


}
/**
 * Retourne la direction du point auquel correspond l' image
 */
Vector3d Camera::getDirection(const Vector2d & image) const{

  Vector2d img = image - optique.getCenter();

  Vector3d direction;

  direction(this->directionZ) = 1;

  direction(this->directionV) = this->signeV * img(1)/this->optique.alphaU;

  direction(this->directionU) = this->signeU * img(0)/this->optique.alphaV;

  direction = frame.getVectorInParent(direction);

  return direction.normalized();

}

/**
 * Retourne les coordonnees du point image apres la distorsion
 */
Vector2d Camera::getDirtyImage(const Distorsion & modele, const Vector3d & point) const{

  Vector2d image;

  image = this->getProjection(point);

  image = modele.getDirtyImage(image);

  Vector2d origine = optique.getCenter();

  image = image + origine;

  return image;

}

/**
 * Retourne la jacobienne de la fonction de projection avec distorsion
 *
 * en fonction:
 *
 * - trois premiere colonnes : l'attitude de la camera
 * - trois suivantes 			: la position de la camera
 * - deux suivantes   		: des parametres intrinseques dans l'ordre alphau et alpha V
 * - des parametre de distorsion
 * - trois dernieres			: les coordonnees du point
 *
 */
MatrixXd Camera::getJacobianDirtyImage(const Distorsion & modele, const Vector3d & point) const{

  Vector2d image = this->getProjection(point);

  //cout << "check 1 point =\n"<<point<<endl;

  //cout << "check 2 image =\n"<<image<<endl;

  MatrixXd JP = this->getJacobianProjection(point);

  //cout << "check 3 JP =\n"<<JP<<endl;

  MatrixXd JD = modele.getJacobianDirtyImage(image);

  long int diff = JD.cols()-2;

  //cout << "check 4 JD =\n"<<JD<<endl;

  MatrixXd facteur = JD.rightCols(2);

  //cout << "check 5 facteur =\n"<<facteur<<endl;

  MatrixXd JJ(2,13+diff);

  JJ.block(0,0,2,8).noalias() = facteur * JP.block(0,0,2,8);

  //cout << "check 6 "<<endl;

  JJ.block(0,8,2,2) = Matrix2d::Identity();

  //cout << "check 7 "<<endl;

  JJ.block(0,10,2,diff) = JD.leftCols(diff);

  //cout << "check 8 "<<endl;

  JJ.rightCols(3).noalias() = facteur * JP.rightCols(3);

  //cout << "check 9 "<<endl;

  return JJ;


}
/**
 * Retourne les coordonnees du point image sans distorsion
 */
Vector2d Camera::getCleanImage(const Vector3d & point) const{

  Vector2d image;

  image = this->getProjection(point);

  image += optique.getCenter();

  return image;

}

/**
 * Retourne la jacobienne de la fonction de projection
 *
 * en fonction:
 *
 * - trois premiere colonnes : l'attitude de la camera
 * - trois suivantes 			: la position de la camera
 * - quatres suivantes   		: des parametres intrinseques dans l'ordre alphaU,alphaV, u0 et v0
 * - trois dernieres			: les coordonnees du point
 *
 */
MatrixXd Camera::getJacobianCleanImage(const Vector3d & point) const{

  MatrixXd JP = this->getJacobianProjection(point);

  MatrixXd J(2,13);

  J.block<2,8>(0,0) = JP.block<2,8>(0,0);
  J.block<2,2>(0,8) = Matrix2d::Identity();
  J.block<2,3>(0,10) = JP.block<2,3>(0,8);

  return J;


}
/**
 * Retourne les coordonees du point image dans l'image redressee
 */

Vector2d Camera::getCleanImage(const Distorsion & modele,const Vector2d & point) const{

  Vector2d origine = optique.getCenter();

  Vector2d image;

  image = point - origine;

  image = modele.getCleanImage(image);

  return image+origine;

}


/**
 * Projete l' image d' un point 3d sur un plan a 1 metre de la camera
 *
 * c'est le modele du trou d'epingle, "Pinhole"
 */
Vector2d Camera::getMetricProjection(const Vector3d & point) const{

  Vector3d pointI;
  pointI = frame.getPointInME(point);

  double u = this->signeU * pointI(this->directionU);
  double v = this->signeV * pointI(this->directionV);
  double d = pointI(this->directionZ);

  if(d>0){
    u/=d;
    v/=d;
  }

  Vector2d image;

  image << u,v;

  return image;

}


/**
 * Projete l' image d' un point 3d sur un plan a 1 metre de la camera
 *
 * c'est le modele du trou d'epingle, "Pinhole"
 */
Vector2d Camera::getMetricProjection(const Vector2d & point) const{

  Vector2d origine = optique.getCenter();

  Vector2d image;

  image = point - origine;

  image(0) /= this->optique.alphaU;

  image(1) /= this->optique.alphaV;

  return image;

}


/**
 * Retourne la direction du point
 */

Vector3d Camera::getCleanDirection(const Vector3d & point) const{

  return getDirection(getProjection(point));

}

/**
 * Retourne la direction du point
 */

Vector3d Camera::getCleanDirection(const Vector2d & image) const{

  return this->getDirection(image);

}

/**
 * Retourne la direction du point
 */
Vector3d Camera::getCleanDirection(const Distorsion & modele,const Vector2d & point) const{

  Vector2d image = this->getCleanImage(modele,point);

  return this->getDirection(image);
}

/**
 * Retourne les parametres de la camera sous la forme  d'un vecteur
 */
VectorXd Camera::getParameters(const Distorsion & modele) const{

  VectorXd paraD = modele.getParameters();

  VectorXd settings;

  settings << parametres,paraD;

  return settings;

}

/**
 * Retourne les parametres de la camera sous la forme  d'un vecteur
 */
VectorXd Camera::getParameters() const{

  return parametres;
}

/**
 * Modifie les parametres de la camera
 */
void Camera::setParameters(Distorsion & modele, const VectorXd & para){

  this->setParameters(para.block<10,1>(0,0));

  modele.setParameters(para.block(10,0,para.rows()-10,1));
}

/**
 * Modifie les parametres de la camera
 */

void Camera::setParameters(const VectorXd & para){

  parametres = para.block<10,1>(0,0);

  Vector3d vecteur(parametres.block<3,1>(0,0));

  frame.setRotation(VectRotation(vecteur));

  vecteur = parametres.block<3,1>(3,0);

  frame.setDeplacement(vecteur);

  optique.alphaU = parametres(6);
  optique.alphaV = parametres(7);
  optique.u0     = parametres(8);
  optique.v0     = parametres(9);

}

CVEcran * Camera::getDisplay(){
  return NULL;
}
