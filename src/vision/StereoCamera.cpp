/*
 * StereoCamera.cpp
 *
 *  Created on: Jun 12, 2012
 *      @author Renliw Fleurmond
 */

#include "StereoCamera.h"

using namespace std;
using namespace Eigen;

const double StereoCamera::seuilCroyance = 0.99;


StereoCamera::StereoCamera():
  centre(),
  gauche(),
  droite()
{
  // TODO Auto-generated constructor stub

  Repere r;
  Vector3d dep;

  dep(Camera::defaultDirectionU) = -0.1;
  dep(Camera::defaultDirectionV) =    0;
  dep(Camera::defaultDirectionZ) =    0;

  r.setDeplacement(dep);

  droite.setRepere(r);

}


StereoCamera::StereoCamera(Camera g,Camera d):
  centre(),
  gauche(g),
  droite(d)
{

}

void StereoCamera::setRightCamera(Camera c){
  droite = c;
}
void StereoCamera::setLeftCamera(Camera c){
  gauche = c;
}

Camera StereoCamera::getLeftCamera() const{

  return gauche;

}

Camera StereoCamera::getRightCamera() const{

  return droite;

}


void StereoCamera::setCentre(Repere rep){

  centre = rep;

}

Repere StereoCamera::getCentre() const{

  return centre;

}
/**
 * Retourne l'image non deformee du point vu par la camera droite
 */

Vector2d StereoCamera::getCleanRightImage(Vector3d point) const{

  return droite.getCleanImage(point);

}
/**
 * Retourne l'image non deformee du point vu par la camera gauche
 */
Vector2d StereoCamera::getCleanLeftImage(Vector3d point) const{

  return gauche.getCleanImage(point);

}

MatrixXd StereoCamera::getJacobianCleanRightImage(Vector3d point) const{

  return droite.getJacobianCleanImage(point);

}

MatrixXd StereoCamera::getJacobianCleanLeftImage(Vector3d point) const{

  return gauche.getJacobianCleanImage(point);

}

/**
 * Retourne l'image rectifiee du point vu par la camera droite
 */
Vector2d StereoCamera::getDirtyRightImage(const Distorsion & modele,Vector3d point) const{

  return droite.getDirtyImage(modele,point);

}

/**
 * Retourne l'image rectifiee du point vu par la camera gauche
 */
Vector2d StereoCamera::getDirtyLeftImage(const Distorsion & modele,Vector3d point) const{

  return gauche.getDirtyImage(modele,point);

}

MatrixXd StereoCamera::getJacobianDirtyRightImage(const Distorsion & modele,Vector3d point) const{

  return droite.getJacobianDirtyImage(modele,point);

}

MatrixXd StereoCamera::getJacobianDirtyLeftImage(const Distorsion & modele,Vector3d point) const{

  return gauche.getJacobianDirtyImage(modele,point);

}
/**
 * Retourne true si les points des images rectifiees des deux cameras respectent a un degre defini la contrainte epipolaire
 *
 */
bool StereoCamera::areEpipolar(const Distorsion & modeleG, Vector2d imageG,const Distorsion & modeleD,Vector2d imageD) const{

  return (this->beliefEpipolar(modeleG,imageG,modeleD,imageD)>this->seuilCroyance);

}

/**
 * Retourne true si les points des  des deux cameras respectent a un degre defini la contrainte epipolaire
 *
 */
bool StereoCamera::areEpipolar(Vector2d imageG,Vector2d imageD) const{


  return (this->beliefEpipolar(imageG,imageD)>this->seuilCroyance);


}

/**
 * Retourne le degre de croyance que les deux points
 *  appartenant aux deux images rectifiees des cameras
 *  respectent la contrainte epipolaire
 */

double StereoCamera::beliefEpipolar(const Distorsion & modeleG, Vector2d imageG,const Distorsion & modeleD,Vector2d imageD) const{

  Vector2d gg = gauche.getCleanImage(modeleG,imageG);
  Vector2d dd = droite.getCleanImage(modeleD,imageD);

  return this->beliefEpipolar(gg,dd)*modeleG.getBeliefImage()*modeleD.getBeliefImage();

}

/**
 * Retourne le degre de croyance que les deux points
 *  appartenant aux deux images des cameras
 *  respectent la contrainte epipolaire
 */

double StereoCamera::beliefEpipolar(Vector2d imageG,Vector2d imageD) const{

  Vector3d U = gauche.getCleanDirection(imageG);
  Vector3d V = droite.getCleanDirection(imageD);
  Vector3d D = droite.getRepere().getDeplacement() - gauche.getRepere().getDeplacement();

  if(isEqual(D.norm(),0,1e-6)){
    return 0;
  }

  Vector3d UV = U.cross(V);

  if(isEqual(UV.norm(),0,1e-6)){
    return 0;
  }

  Matrix3d calcul;
  calcul.col(0) =   U;
  calcul.col(1) =  -V;
  calcul.col(2) = -UV;

  Vector3d facteurs = calcul.fullPivHouseholderQr().solve(D);

  double a = facteurs(0);
  double c = facteurs(2);

  Vector3d ecart = c*UV;

  double croyance = exp(-1*(ecart.norm()/U.norm()/a));

  return croyance;

}

/**
 * Retourne le point correspondant au plus pres aux coordonnees de
 * de deux points des images rectifiees des deux cameras
 *
 * Attention: la contrainte epipolaire peut ne pas etre respectee
 *
 * Si aucune solution n'est trouvee, cette methode retourne un vecteur nul
 */

Vector3d StereoCamera::getPoint(const Distorsion & modeleG, Vector2d imageG,const Distorsion & modeleD,Vector2d imageD) const{

  Vector2d gg = gauche.getCleanImage(modeleG,imageG);
  Vector2d dd = droite.getCleanImage(modeleD,imageD);
  //cout <<"image gauche rectifiee = \n"<<gg<<endl;
  //cout <<"image droite rectifiee = \n"<<dd<<endl;

  return this->getPoint(gg,dd);

}

/**
 * Retourne le point correspondant au plus pres aux coordonnees de
 * de deux points des images des deux cameras
 *
 * Attention: la contrainte epipolaire peut ne pas etre respectee
 *
 * Si aucune solution n'est trouvee, cette methode retourne un vecteur nul
 *
 */


Vector3d StereoCamera::getPoint(Vector2d imageG,Vector2d imageD) const{

  Vector3d U = gauche.getCleanDirection(imageG);
  Vector3d V = droite.getCleanDirection(imageD);
  Vector3d D = droite.getRepere().getDeplacement() - gauche.getRepere().getDeplacement();

  if(isEqual(D.norm(),0,1e-6)){
    return Vector3d::Zero();;
  }

  Vector3d UV = U.cross(V);

  if(isEqual(UV.norm(),0,1e-6)){
    return Vector3d::Zero();;
  }


  Matrix3d calcul;
  calcul.col(0) =   U;
  calcul.col(1) =  -V;
  calcul.col(2) = -UV;

  Vector3d facteurs = calcul.fullPivHouseholderQr().solve(D);

  double a = facteurs(0);

  double c = facteurs(2);

  Vector3d ecart = 0.5*c*UV;

  Vector3d point = a*U + ecart;

  return point;

}
/*
  Vector3d StereoCamera::getJacobianPoint(const Distorsion & modeleG, Vector2d gauche,const Distorsion & modeleD,Vector2d droite) const{

  }

  Vector3d StereoCamera::getJacobianPoint(Vector2d gauche,Vector2d droite) const{

  }
*/

