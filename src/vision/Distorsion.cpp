/*
 * Distorsion.cpp
 *
 *  Created on: Jun 6, 2012
 *      @author Renliw Fleurmond
 */

#include "Distorsion.h"

using namespace Eigen;

Distorsion::Distorsion() {
  // TODO Auto-generated constructor stub

}

Distorsion::~Distorsion() {
  // TODO Auto-generated destructor stub
}

/**
 * Retourne les parametres de la distorsion
 *
 * Cette methode est destinee a etre surchargee
 */
VectorXd Distorsion::getParameters()const{

  VectorXd p(1);

  p<<1;

  return p;
}

/**
 * Modifie les parametres de la distorsion
 *
 * Cette methode est destinee a etre surchargee
 */
void Distorsion::setParameters(VectorXd p){

}

/**
 * Retourne l'image distordue
 *
 * Cette methode est destinee a etre surchargee
 */
VectorXd Distorsion::getDirtyImage(VectorXd original) const{

  return original;

}

/**
 * Retourne la fonction de la fonction de distorsion
 *
 * Cette methode est destinee a etre surchargee
 */
MatrixXd Distorsion::getJacobianDirtyImage(VectorXd p) const{

  long int n = p.cols();

  MatrixXd jacob(n,n+1);

  jacob.col(0)= MatrixXd::Zero(n,1);

  jacob.block(0,1,n,n) = MatrixXd::Identity(n,n);

  return jacob;

}
/**
 * Retourne le point de l'image non distordue
 *
 * Cette methode est destinee a etre surchargee
 *
 */
VectorXd Distorsion::getCleanImage(VectorXd original) const{

  return original;

}

/**
 * Retourne la jacobiennne de la fonction de redressement de l' image distordue
 *
 * Cette methode est dstinee a etre surchargee
 *
 */
MatrixXd Distorsion::getJacobianCleanImage(VectorXd p) const{

  long int n = p.cols();

  MatrixXd jacob(n,n+1);

  jacob.col(0)= MatrixXd::Zero(n,1);

  jacob.block(0,1,n,n) = MatrixXd::Identity(n,n);

  return jacob;

}


double Distorsion::getBeliefImage() const{
  return 1;
}
