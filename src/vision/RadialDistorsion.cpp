/*
 * RadialDistorsion.cpp
 *
 *  Created on: Jun 13, 2012
 *      @author Renliw Fleurmond
 */

#include "RadialDistorsion.h"
#include <complex>
#include <iostream>

using namespace std;
using namespace Eigen;

const double RadialDistorsion::CONFIANCE = 0.999999;


RadialDistorsion::RadialDistorsion(double k):
  Distorsion(),
  k1(k)
{

  this->parametres = VectorXd(1);

  parametres << k;

}

RadialDistorsion::~RadialDistorsion() {

}

/**
 * Retourne les parametres de la distorsion
 *
 */
VectorXd RadialDistorsion::getParameters()const{

  return parametres;

}

/**
 * Modifie les parametres de la distorsion
 */
void RadialDistorsion::setParameters(VectorXd p){

  assert(p.rows()==1);

  parametres = p;

  k1 = p(0);

}

/**
 * Retourne l'image distordue
 *
 */
VectorXd RadialDistorsion::getDirtyImage(VectorXd original) const{

  assert(original.rows() == 2);

  if(isEqual(k1,0,1e-6)){
    return original;
  }
  double x = original(0);
  double y = original(1);

  double r2 = pow(x,2)+pow(y,2);

  x = x*(1+k1*r2);
  y = y*(1+k1*r2);

  VectorXd copie(2);

  copie << x, y;

  return copie;

}

/**
 * Retourne la fonction de la fonction de distorsion
 *
 */
MatrixXd RadialDistorsion::getJacobianDirtyImage(VectorXd p) const{

  assert(p.rows() == 2);

  MatrixXd jacob(2,3);

  if(isEqual(k1,0,1e-6)){
    jacob << 0, 1, 0,
      0, 0, 1;
  }
  else{

    double x = p(0);

    double y = p(1);

    double r2 = pow(x,2)+pow(y,2);

    jacob << x*r2, 1+3*k1*x*x + k1*y*y,            2*k1*x*y,
      y*r2,            2*k1*x*y, 1+3*k1*y*y + k1*x*x;
  }



  return jacob;

}
/**
 * Retourne le point de l'image non distordue
 *
 *
 */
VectorXd RadialDistorsion::getCleanImage(VectorXd d) const{

  assert(d.rows() == 2);

  if(isEqual(k1,0,1e-6)){
    return d;
  }

  double x = d(0);
  double y = d(1);

  double rd = sqrt(pow(x,2)+pow(y,2));

  if(isEqual(rd,0,1e-6)){
    return d;
  }

  double r,yr;

  yr = rd;

  int i =0;

  do{

    r= yr*(1-k1*yr*yr);
    yr = r*(1 + k1*r*r);
    r = r + 1*(rd - yr);
    yr = r*(1 + k1*r*r);
    i++;

    if(i==10){
      break;
    }

  }while(!isEqual(yr,rd,1e-6));


  x = x *r/rd;
  y = y *r/rd;

  VectorXd propre(2);

  propre <<  x,  y;

  return propre;

}

/**
 * Retourne la jacobiennne de la fonction de redressement de l' image distordue
 *
 *
 */
MatrixXd RadialDistorsion::getJacobianCleanImage(VectorXd d) const{

  MatrixXd brut = this->getJacobianDirtyImage(this->getCleanImage(d));

  MatrixXd mNet(2,3);

  mNet.col(0) = -brut.col(0);
  mNet.block<2,2>(0,1) = brut.block<2,2>(0,1).inverse();

  return mNet;

}

/**
 * Retourne la confiance en la justesse des coordonnees
 * d'un point dans l' image rectifieee,
 * calculees a partir des ses coordonnees dans l'image distordue
 */
double RadialDistorsion::getBeliefImage() const{

  return this->CONFIANCE;

}
