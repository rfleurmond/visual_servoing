#include "intrinsic.h"

using namespace Eigen;
using namespace std;

IntrinsicCam::IntrinsicCam():
  u0(0.0),
  v0(0.0),
  alphaU(1.0),
  alphaV(1.0),
  largeur(2.0),
  hauteur(2.0)
{

}

IntrinsicCam::IntrinsicCam(double au, double av, double u00, double v00):
  u0(u00),
  v0(v00),
  alphaU(au),
  alphaV(av),
  largeur(640),
  hauteur(480)
{

}

ostream & operator<<(ostream & flux, const IntrinsicCam & opt){

  flux <<"Intrinsic parameters:\n";
  flux << "|"<<opt.alphaU <<" \t"<< 0.0        <<" \t"<<opt.u0 <<"|"<<endl;
  flux << "|"<<       0.0 <<" \t"<< opt.alphaV <<" \t"<<opt.v0 <<"|"<<endl;
  flux <<endl;

  return flux;

}


Vector2d IntrinsicCam::getCenter() const{
  Vector2d center;
  center << u0, v0;
  return center;
}



