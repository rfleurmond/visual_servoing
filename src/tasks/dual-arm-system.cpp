#include "dual-arm-system.h"

using namespace Eigen;


DualArmSystem::DualArmSystem(TaskSystem * gauche, TaskSystem * droite){
  assert(gauche!=NULL);
  assert(droite!=NULL);
  brasGauche = gauche;
  brasDroite = droite;
  dimGauche = brasGauche->getDimState();
  dimDroite = brasDroite->getDimState();
  dimension = dimGauche + dimDroite;
}

int DualArmSystem::getDimState() const{
  return dimension;
}

VectorXd DualArmSystem::getState() const{
  VectorXd state = VectorXd::Zero(dimension);
  state.topRows(dimGauche)= brasGauche->getState();
  state.bottomRows(dimDroite)= brasDroite->getState();
  return state;
}

void DualArmSystem::setState(const VectorXd & state){
  brasGauche->setState(state.topRows(dimGauche));
  brasDroite->setState(state.bottomRows(dimDroite));
}

void DualArmSystem::takeCommand(const VectorXd & order){
  brasGauche->takeCommand(order.topRows(dimGauche));
  brasDroite->takeCommand(order.bottomRows(dimDroite));
}

void DualArmSystem::updateTime(double t){
  brasGauche->updateTime(t);
  brasDroite->updateTime(t);
}

double DualArmSystem::getPeriod() const{
  return brasGauche->getPeriod();
}

