#include "task-controller.h"

#include <fstream>
#include <stdlib.h>
#include <time.h>

using namespace std;
using namespace Eigen;


TaskController::TaskController(TaskSystem * sys, Task * ta):
  systeme(sys),
  tache(ta),
  lambda(1.0),
  mu(3.0),
  periodS(0.01),
  periodT(0.05),
  heure(0),
  lastChange(0.0),
  horizon(100.0),
  normeMinimum(1e-6),
  fichierTache("data_task.dat"),
  fichierSys("data_sys.dat"),
  hasToLog(true),
  multiple(3),
  smoothing(true),
  erreur(VectorXd::Zero(1)),
  commande(VectorXd::Zero(1)),
  old_order(VectorXd::Zero(1)),
  last_order(VectorXd::Zero(1)),
  integrale(VectorXd::Zero(1)),
  derivee(VectorXd::Zero(1)),
  etat_q(VectorXd::Zero(1))
{
  assert(sys!=NULL);
  assert(ta!=NULL);
  assert(systeme->getDimState() == tache->getDimInput());
  lambda = tache->getLambda()*multiple;
  mu = multiple * lambda;
  periodT = tache->getPeriod();
  periodS = systeme->getPeriod();
  erreur = tache->valeur();
  etat_q = systeme->getState();
  commande = VectorXd::Zero(tache->getDimInput());
  old_order = commande;
  last_order = commande;
  integrale = commande;
  derivee = commande;
  normeMinimum = tache->getMinimumErrorNorm();
  
}

double TaskController::getSmoothFactor(){
  return mu;
}


void TaskController::setSmoothRatio(double m){
  multiple = m;
  mu = multiple * lambda;
}

  
void TaskController::enableSmoothing(bool mode){
  smoothing = mode; 
}

void TaskController::setTimeLimit(double limit){
  horizon = limit;
}

void TaskController::setThreshold(double epsilon){
  normeMinimum = epsilon;
}

void TaskController::setLogMode(bool mode){
  hasToLog = mode; 
}

void TaskController::setTaskLogFile(string path){
  fichierTache = path;
}

void TaskController::setSysLogFile(string path){
  fichierSys = path;
}

VectorXd TaskController::getTimeCommand(double time){
  erreur = tache->valeur();

  //calcul correcteur proportionnel
  commande = tache->calculCommande();
  
  if(
     !isEqual(tache->getLambda(),lambda,1e-6) || 
     tache->hasChanged()
     )
    {
      lastChange = heure;
      lambda = tache->getLambda();
      mu = multiple*lambda;
      old_order = commande;
    } 

  if(tache->hasChanged()){
    cout<<"[TaskController]: changement de tache detecte"<<endl;
    cout<<"[TaskController]: Heure du changement "<<heure<<endl;
  } 

 if(smoothing){
  
    // Lissage de la commande avec un second ordre
    // Cette formule marche pour tous les cas
    commande += -1*  exp(-1*mu*periodT)*(commande - last_order);

    // Lissage de la commande, cette formule marche que si l'on connait la date du changement
    /*
      commande = commande -  exp(mu*(lastChange - time))*(commande - old_order);    
    //*/
  }


  last_order = commande;
  heure = time;
  return commande;

}


void TaskController::run(){

  MyClock::start();
  double temps = 0;

  ofstream printerT,printerS;
  bool accesT = false;
  bool accesS = false;
  
  if(hasToLog){
    printerT.open(fichierTache.c_str());
    if(printerT){
      accesT = true;
    }
    else{
      cout<<"[TaskController]: Warn: Log file for task not opened"<<endl;
    }
    printerS.open(fichierSys.c_str());
    if(printerS){
      accesS = true;
    }
    else{
      cout<<"[TaskController]: Warn: Log file for system  not opened"<<endl;
    }
    
  }
  
  cout<<"[TaskController]: demarrage du controleur"<<endl;
    
  while(heure<horizon && tache->isComplete()==false){
    tache->setState(etat_q);
    temps =  0;
    
    erreur = tache->valeur();

    //cout << "Task = " <<erreur.transpose() << endl;

    getTimeCommand(heure);
    tache->updateGain();
    
 
    tache->recordCommand(commande);
    
      
    while(temps<periodT){

      systeme->takeCommand(commande);

      if(accesT){
	printerT<<heure;
	printerT<<" "<<erreur.transpose()<<endl;
      }
      if(accesS){
	printerS<<heure;
	printerS<<" "<<commande.transpose();
	printerS<<" "<<etat_q.transpose()<<endl;
      }
      
      heure+=periodS;
      MyClock::setTime(heure);
      systeme->updateTime(heure);
      temps+=periodS;
      etat_q = systeme->getState();
    }

    tache->updateTime(heure);
  }
  if(heure>=horizon){
    cout<<"[TaskController]: Delay expired"<<endl;
  }
  else{
    cout<<"[TaskController]: Task is completed"<<endl;
  }

  if(accesT){
    printerT<<heure;
    printerT<<" "<<erreur.transpose()<<endl;
    printerT.close();
  }
  if(accesS){
    printerS<<heure;
    printerS<<" "<<commande.transpose();
    printerS<<" "<<etat_q.transpose()<<endl;
    printerS.close();
  }
      
  cout<<"[TaskController]: Arret du controleur"<<endl;
}

