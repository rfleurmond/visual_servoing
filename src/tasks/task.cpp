#include "task.h"

using namespace Eigen;

TaskFunction::TaskFunction():
  dimInput(1),
  dimOutput(1)
{
  
}

int TaskFunction::getDimInput() const{
  return dimInput;
}

int TaskFunction::getDimOutput() const{
  return dimOutput;
}


AdaptiveGain::AdaptiveGain(double l8, double l0):
  lZero(l0),
  lInfini(l8),
  ltemporel(1),
  lastNorm(-1),
  lambda(l8) 
{
  assert(lZero>0);
  assert(lInfini>0);
  assert(ltemporel>0);
  if (lInfini > lZero)
    lZero = 5* lInfini;
}

double AdaptiveGain::getLambda() const{
  return lambda;
}

void AdaptiveGain::updateNorm(double norme) {
  if(lastNorm<0){
    if(isEqual(norme,0,1e-7))
     norme = 1;
    ltemporel = - 2 * log(1-0.99)/norme;
    lastNorm = norme;
    lambda = lInfini;
  }
  else{
    lambda = ( lZero - lInfini) * exp ( - ltemporel * norme) + lInfini;
  }
}

void AdaptiveGain::reset(){
  lastNorm = -1;
}

Task::Task():
  TaskFunction(),
  isAdaptive(false),
  changement(false),
  lambda(0.5),
  lambdaVariable(AdaptiveGain()),
  borneInferieure(1e-3),
  tempo(0.0),
  etat_q(VectorXd::Zero(1)),
  dot_q(VectorXd::Zero(1))
{

}
  
void Task::setLambda(double l){
  lambda = l;
  isAdaptive = false;
}


void Task::setLambda(AdaptiveGain ll){
  lambda = ll.getLambda();
  lambdaVariable = ll;
  isAdaptive = true;
}

double Task::getLambda() const{
  return lambda;
}


void Task::updateGain(){
  if(isAdaptive){
    lambdaVariable.updateNorm(valeur().norm());
    if(lambda < lambdaVariable.getLambda()){
      lambda = lambdaVariable.getLambda();
      changement =true;
    }
  }
}

void Task::start(){
  lambdaVariable.reset();
}

void Task::setState(const VectorXd & q){
  updateGain();
  etat_q = q;
}

void Task::updateTime(double t){
  tempo = t;
}

void Task::recordCommand(const VectorXd & dotQ){
  assert(etat_q.rows() == dotQ.rows());
  dot_q = dotQ;
}


VectorXd Task::getState() const{
  return etat_q;
}


VectorXd Task::valeur() const{
  return fonction(etat_q);
}
  
 
VectorXd Task::calculCommande() const{
  return calculCommandeFor(valeur());
}

VectorXd Task::calculCommandeFor(const VectorXd & erreur) const{
  MatrixXd jDirect,jInvers;
  jDirect = jacobien(etat_q);
  pinv(jDirect,jInvers);
  VectorXd commande(erreur);
  commande.noalias() = - lambda * jInvers * erreur;
  return commande;
}

double Task::getMinimumErrorNorm() const{
  return borneInferieure;
}

bool Task::isComplete() const{
  double erreurN = valeur().norm();
  return erreurN <= borneInferieure;
}


void Task::setMinimumErrorNorm( double m){
  borneInferieure = m;
}

GradientTask::GradientTask():
  Task(),
  tache(NULL)
{

}


void GradientTask::setBaseTask(Task * base){
  tache = base;
}

MatrixXd GradientTask::jacobien(const VectorXd & q) const{
  assert(tache != NULL);
  return tache->jacobien(q);
}

VectorXd GradientTask::calculCommande() const{
  assert(tache != NULL);
  return -tache->calculCommandeFor(getGradient(tache->valeur())); 
}

VectorXd GradientTask::calculCommandeFor(const VectorXd & erreur) const{
  assert(tache != NULL);
  return -1 * tache->calculCommandeFor(getGradient(erreur));
}

double GradientTask::getPeriod() const{
  assert(tache != NULL);
  return tache->getPeriod();
}

ApproximateTask::ApproximateTask(TaskFunction * taskF, double step):
  TaskFunction(),
  tache(taskF),
  pas(step)
{
  assert(taskF!=NULL);
  assert(step>0);

  dimOutput = tache->getDimOutput();
  dimInput = tache->getDimInput();
}

VectorXd ApproximateTask::fonction(const VectorXd & q) const{
  return tache->fonction(q);
}

MatrixXd ApproximateTask::jacobien(const VectorXd & q) const{
  MatrixXd J = MatrixXd::Zero(dimOutput,dimInput);
  VectorXd dX = VectorXd::Zero(dimInput);
  VectorXd Y = VectorXd::Zero(dimOutput);
  VectorXd rY = VectorXd::Zero(dimOutput);
  
  rY = tache->fonction(q);
  
  for(int i = 0; i<dimInput; i++){
    dX(i) = pas;
    if(i>0) dX(i-1) = 0;
    Y = tache->fonction(q + dX);
    J.col(i) = (Y-rY)/pas;
  }

  return J;
}
  
