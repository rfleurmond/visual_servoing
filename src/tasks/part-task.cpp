#include "part-task.h"

using namespace Eigen;

PartTask::PartTask(const MatrixXd & activ, Task * tache):
  Task(),
  task(tache),
  activation(activ)
{
  assert(tache!=NULL);
  assert(tache->getDimOutput()==activ.cols());
 
  dimOutput = activ.rows();
  constante = VectorXd::Zero(dimOutput);
  dimInput = task->getDimInput();
  etat_q = task->getState();
  dot_q = 0 * etat_q;
  lambda = task->getLambda();

}

void PartTask::setState(const VectorXd & q){
  etat_q = q;
  task->setState(q);
  if(task->hasChanged()){
    changement = true;
    lambda = task->getLambda();
  }
  updateGain();
}

void PartTask::setOffset(const VectorXd & offset){
  constante  = offset;
}

VectorXd PartTask::getState() const{
  return etat_q;
}

VectorXd PartTask::fonction(const VectorXd & q) const{
  VectorXd output(task->fonction(q));
  output = activation * output - constante;
  return output;
}

MatrixXd PartTask::jacobien(const VectorXd & q) const{
  MatrixXd jacobienne(activation);
  jacobienne *= task->jacobien(q);
  return jacobienne;
}


VectorXd PartTask::calculCommandeFor(const VectorXd & erreur) const{
  VectorXd commande, derivee;
  MatrixXd jacobienne = jacobien(etat_q);
  MatrixXd Jplus;
  pinv(jacobienne,Jplus);
  commande.noalias() = -lambda * Jplus * erreur;
  return commande;
}

void PartTask::recordCommand(const VectorXd & dotQ){
  assert(etat_q.rows() == dotQ.rows());
  dot_q = dotQ;
  task->recordCommand(dotQ);
}


double PartTask::getPeriod() const{
  return task->getPeriod();
}


bool PartTask::hasChanged() const{
  return task->hasChanged();
}

bool PartTask::isComplete() const{
  double erreurN = valeur().norm();
  return ((erreurN <= borneInferieure));
}

