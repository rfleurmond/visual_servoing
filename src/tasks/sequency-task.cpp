#include "sequency-task.h"

using namespace Eigen;

SequencyTask::SequencyTask(Task * first, Task * second):
  Task(),
  activeTask(NULL),
  file(std::queue<Task * >())
{
  assert(first !=NULL);

  dimOutput = first->getDimOutput();
  dimInput = first->getDimInput();

  lambda = 1.0;
  isAdaptive = false;
  etat_q = first->getState();
  dot_q = 0 * etat_q;
  borneInferieure = 0.0;
  changement = false;
  tempo = 0.0;

  activeTask = first;

  dimInput = activeTask->getDimInput();
  dimOutput = activeTask->getDimOutput();
  addTaskInStack(first);
  addTaskInStack(second);
  changeActiveTask();
  lambda = first->getLambda();
}

void SequencyTask::setState(const VectorXd & q){
  activeTask->setState(q);
  etat_q = q;
  if(changement)
    std::cout<<"[SequencyTask  ]: changement effectue"<<std::endl;
  changement = false;
  if(activeTask->isComplete() && file.empty()==false){
    changeActiveTask();
    activeTask->setState(q);
  }
  if(activeTask->hasChanged()){
    lambda = activeTask->getLambda();
  }
  updateGain();

}

void SequencyTask::changeActiveTask(){
  if(file.empty()==false){
    activeTask= file.front();
    file.pop();
    dimOutput = activeTask->getDimOutput();
    changement = true;
    std::cout<<"[SequencyTask  ]: changement de tache"<<std::endl;
    std::cout<<"[SequencyTask  ]: Il y a maintenant "<<
      file.size()<<" tache(s) en attente"<<  std::endl;
    std::cout<<"[SequencyTask  ]:  Dimension tache = "<<dimOutput << std::endl;
      
  }
}

void SequencyTask::addTaskInStack(Task * nTask){
  assert(nTask!=NULL);
  assert(nTask->getDimInput()==dimInput);
  file.push(nTask);
  std::cout<<"[SequencyTask  ]: Ajout de tache"<<std::endl;
  std::cout<<"[SequencyTask  ]: Il y a maintenant "<<
    file.size()<<" tache(s) en attente"<<  std::endl;

}

int SequencyTask::getTasksNumber() const{
  return file.size();
}

VectorXd SequencyTask::getState() const{
  return activeTask->getState();
}

VectorXd SequencyTask::fonction(const VectorXd & q) const{
  return activeTask->fonction(q);
}

MatrixXd SequencyTask::jacobien(const VectorXd & q) const{
  return activeTask->jacobien(q);
}

VectorXd SequencyTask::calculCommandeFor(const VectorXd & erreur) const{
  return activeTask->calculCommandeFor(erreur);
}

void SequencyTask::recordCommand(const VectorXd & dotQ){
  assert(etat_q.rows() == dotQ.rows());
  dot_q = dotQ;
  activeTask->recordCommand(dotQ);
}


double SequencyTask:: getPeriod() const{
  return  activeTask->getPeriod();
}

double SequencyTask::getLambda() const{
  return  activeTask->getLambda();
}

bool SequencyTask::hasChanged() const{
  return (activeTask->hasChanged() | changement);
}

bool SequencyTask::isComplete() const{
  return (activeTask->isComplete() && file.empty());
}

