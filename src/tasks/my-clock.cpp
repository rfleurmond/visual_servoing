#include "my-clock.h"

double MyClock::heure = 0.0;

bool MyClock::hasStarted = false;

double MyClock::begin = 0.0;

bool MyClock::isExternal = false;

void MyClock::start(){
  heure = 0.0;
  hasStarted = true;
}

void MyClock::start(double t){
  begin = t;
  heure = 0.0;
  hasStarted = true;
  isExternal = true;
}

bool MyClock::setTime( double t){
  if(isExternal){
    if(t>=heure + begin){
      heure = t - begin;
      return true;
    }
    else if(hasStarted==false){
      heure = t- begin;
      return true;
    }
  }
  else{
    if(t>=heure){
      heure = t;
      return true;
    }
    else if(hasStarted==false){
      heure = t;
      return true;
    }
  }
  
  return false;
}

void MyClock::addToTime(double dt){
  heure+=dt;
}

double MyClock::getTime(){
  return heure;
}

