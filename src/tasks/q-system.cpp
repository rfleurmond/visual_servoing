#include "q-system.h"

using namespace Eigen;

QSystem::QSystem(const VectorXd & state, double p){
  periode = p;
  heure = 0;
  dimension = state.rows();
  etat = state;
}


QSystem::QSystem(int dim, double p){
  periode = p;
  heure = 0;
  dimension = dim;
  etat = VectorXd::Zero(dimension);
}

int QSystem::getDimState() const{
  return dimension;
}

VectorXd QSystem::getState() const{
  return etat;
}

void QSystem::setState(const VectorXd & state){
  assert(state.rows() == dimension);
  etat = state;
}

void QSystem::takeCommand(const VectorXd & order){
  assert(order.rows() == dimension);
  vitesse = order;
}

void QSystem::updateTime(double t){
  double dif_time = (t - heure);
  etat  = etat + dif_time*  vitesse;
  heure =t;
}

double QSystem::getPeriod() const{
  return periode;
}

