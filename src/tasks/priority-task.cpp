#include "priority-task.h"

using namespace Eigen;



PriorityTask::PriorityTask(Task * master, Task * slave):
  DualityTask(master,slave)
{

}


VectorXd PriorityTask::getState() const{
  return maitre->getState();
}

VectorXd PriorityTask::calculCommandeFor(const VectorXd & erreur) const{
  int dim1 = maitre->getDimOutput();
  int dim2 = esclave->getDimOutput();
  VectorXd e1, e2, cmd1, cmd2, cmd;
  MatrixXd J1;
  e1 = erreur.topRows(dim1);
  e2 = erreur.bottomRows(dim2);
  J1 = maitre->jacobien(etat_q);
  cmd1 = maitre->calculCommandeFor(e1);
  cmd2 = esclave->calculCommandeFor(e2);
  if(hidden == 0){
    cmd = cmd2;
  }
  else if(hidden == 1){
    cmd = cmd1;
  }
  else{
    cmd = cmd1;
    cmd.noalias() +=  projecteur(J1)* cmd2;
  }
  return cmd;
}

MatrixXd PriorityTask::projecteur(const MatrixXd & J){
  long int n = J.cols();
  MatrixXd II = MatrixXd::Identity(n,n);
  MatrixXd Jplus,P;
  pinv(J,Jplus);
  P = II;
  P.noalias() += -1 * Jplus * J;
  return P;
}

void PriorityTask::hideTaskOnControl(Task * t){
  if(t==NULL){
    hidden = -1;
    std::cout<<"[PriorityTask]:: All control laws are enabled"<<std::endl;
    return;
  }
  assert(t!=NULL);
  assert(t!=this);
  hidden = -1;
  if(maitre==t){
    hidden = 0;
    std::cout<<"[PriorityTask]:: The control law for the master will be ignored"<<std::endl;
  }
  if(esclave==t){
    hidden = 1;
    std::cout<<"[PriorityTask]:: The control law for the slave will be ignored"<<std::endl;
  }
  
}
