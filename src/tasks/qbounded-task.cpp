#include "qbounded-task.h"

using namespace Eigen;

QBoundedTask::QBoundedTask(const VectorXd & qMin, const VectorXd & qMax):
  Task(),
  coeffMin(0.0),
  coeffMax(0.0),
  alpha(1.0),
  MAX(10.0),
  periode(0.05)
{
  assert(qMin.rows()==qMax.rows());
  dimInput = qMin.rows();
  dimOutput = qMin.rows();
  activation = MatrixXd::Identity(dimOutput,dimInput);
  min = qMin;
  max = qMax;
}

QBoundedTask::QBoundedTask(const VectorXd & qMin, const VectorXd & qMax, const MatrixXd & a):
  Task(),
  coeffMin(0.0),
  coeffMax(0.0),
  alpha(1.0),
  MAX(10.0),
  periode(0.05)
{
  assert(qMin.rows()==a.rows());
  assert(qMin.rows()==qMax.rows());
  activation = a;
  dimInput = a.cols();
  dimOutput = a.rows();
  min = qMin;
  max = qMax;
}

void QBoundedTask::setCoeffMax(double d){
  coeffMax = d;
}


void QBoundedTask::setCoeffMin(double d){
  coeffMin = d;
}
  
void QBoundedTask::setAlpha(double a){
  alpha = a;
}

void QBoundedTask::setMax(double m){
  MAX = m;
}


void QBoundedTask::setState(const VectorXd & q){
  etat_q = q;
  changement = false;
}


VectorXd QBoundedTask::getState() const{
  return etat_q;
}

VectorXd QBoundedTask::fonction(const VectorXd & q) const{
  // Inspire du travail de Kermorgant 2011
  // Concernant les butees
  Repulsive cost(coeffMin,coeffMax,alpha);
  cost.setCeil(MAX);
  VectorXd output= VectorXd::Zero(dimOutput);
  VectorXd qq(q);
  qq = activation * q;
  double x = 0;
  for(int i = 0; i <dimOutput;i++){
    cost.setBounds(min(i),max(i));
    x = qq(i);
    output(i) = cost(x);
  }
  //std::cout<<std::endl;
  return output;
}

MatrixXd QBoundedTask::jacobien(const VectorXd & q) const{
  Repulsive cost(coeffMin,coeffMax,alpha);
  cost.setCeil(MAX);
  MatrixXd jacobienne(MatrixXd::Identity(dimOutput,dimOutput));
  VectorXd qq(q);
  qq = activation * q;
  double x = 0;
  for(int i = 0; i <dimOutput;i++){
    cost.setBounds(min(i),max(i));
    x = qq(i);
    jacobienne(i,i) = cost.jacobien(x);
  }
  jacobienne *= activation;
  return jacobienne;
}


double QBoundedTask::getPeriod() const{
  return  periode;

}
void QBoundedTask::setPeriod(double p){
  periode = p;
}

bool QBoundedTask::hasChanged() const{
  return changement;
}
