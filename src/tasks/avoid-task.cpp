#include "avoid-task.h"

using namespace Eigen;


AvoidTask::AvoidTask(const VectorXd & mean, 
		     const MatrixXd & acti,
		     double max,
		     double sigma):
  Task(),
  periode(0.05),
  sigma2(sigma*sigma),
  alpha(max),
  threshold(1e-2),
  repulse(mean),
  selection(acti)
{
  dimOutput = 1;
  dimInput = selection.cols();
}


void AvoidTask::setState(const VectorXd & q){
  etat_q = q;
  changement = false;
}


VectorXd AvoidTask::getState() const{
  return etat_q;
}

VectorXd AvoidTask::fonction(const VectorXd & q) const{
  VectorXd X = selection * q - repulse;
  double carre = X.dot(X);
  VectorXd output(dimOutput);
  double f = 1, j = 0;
  f  = exp(-1.0*carre/sigma2);
  if(f>=threshold){
    j = alpha*(f-threshold);
  }
  else{
    j = 0;
  }
  output(0) = j;
  return output;
}

MatrixXd AvoidTask::jacobien(const VectorXd & q) const{
  VectorXd X = selection*q-repulse;
  double carre = X.dot(X);
  MatrixXd jacobien = MatrixXd::Zero(dimOutput,dimInput);
  VectorXd output(dimOutput);
  double j =1,f =1;
  f = exp(-1.0*carre/sigma2);
  if(f>=threshold){
    //j = -2*alpha*sigma2*sqrt(carre)*f;
    j = -1/X.norm();
    jacobien =  j*X.transpose()*selection;
  }
  return jacobien;
}

double AvoidTask:: getPeriod() const{
  return  periode;
}

void AvoidTask:: setPeriod(double p){
  periode = p;
}

bool AvoidTask::hasChanged() const{
  return changement;
}
