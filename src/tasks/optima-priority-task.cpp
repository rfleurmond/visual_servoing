#include "optima-priority-task.h"

using namespace Eigen;

OptimaPriorityTask::OptimaPriorityTask(Task * master, Task * slave):
  PriorityTask(master,slave)
{

}

VectorXd OptimaPriorityTask::calculCommandeFor(const VectorXd & erreur) const{
  int dim1 = maitre->getDimOutput();
  int dim2 = esclave->getDimOutput();
  VectorXd e1, e2, cmd1, cmd2,cmd, e1Te2;
  MatrixXd J1,J2, J2P,J2Pinv, P1;
  e1 = erreur.topRows(dim1);
  e2 = erreur.bottomRows(dim2);
  J1 = maitre->jacobien(etat_q);
  J2 = esclave->jacobien(etat_q);
  cmd1 = maitre->calculCommandeFor(e1);
  cmd2 = esclave->calculCommandeFor(e2);
  
  P1 = projecteur(J1);
  e1Te2.noalias() = J2 * cmd1;
  if(hidden == 0){
    cmd = cmd2;
  }
  else if(hidden == 1){
    cmd = cmd1;
  }
  else{
    J2P.noalias() = J2 * P1;
    pinv(J2P,J2Pinv);
    cmd = cmd1;
    cmd.noalias() += J2Pinv * ( J2 * cmd2 - e1Te2);
  }
  
  return cmd;
}

