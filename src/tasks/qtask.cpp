#include "qtask.h"

using namespace Eigen;

QTask::QTask(const VectorXd & qWanted):
  Task(),
  periode(0.05),
  desir(qWanted)
{
  

  setWantedState(qWanted);
  etat_q = VectorXd::Zero(dimInput);
  dot_q = VectorXd::Zero(dimInput);
  setActivationMatrix(MatrixXd::Identity(dimOutput,dimInput));
}

QTask::QTask(const VectorXd & qWanted, const MatrixXd & a):
  periode(0.05),
  desir(qWanted),
  activation(a)
{

  setWantedState(qWanted);
  etat_q = VectorXd::Zero(dimInput);
  dot_q = VectorXd::Zero(dimInput);
  setActivationMatrix(a);
}

void QTask::setWantedState(const VectorXd & q){
  desir = q;
  dimInput = dimOutput = static_cast<int>(q.rows());
  changement = true;
}

void QTask::setActivationMatrix(const MatrixXd & a){
  activation = a;
  assert(a.rows() >  0);
  assert(a.rows() <= a.cols());
  assert(a.cols() == dimInput);
  dimOutput = static_cast<int>(a.rows());
}

void QTask::setState(const VectorXd & q){
  etat_q = q;
  changement = false;
}


VectorXd QTask::getState() const{
  return etat_q;
}

VectorXd QTask::fonction(const VectorXd & q) const{
  VectorXd output(q-desir);
  output = activation * output;
  return output;
}

MatrixXd QTask::jacobien(const VectorXd & q) const{
  MatrixXd jacobienne(MatrixXd::Identity(dimInput,dimInput));
  jacobienne = activation * jacobienne;
  return jacobienne;
}

double QTask:: getPeriod() const{
  return  periode;
}

void QTask:: setPeriod(double p){
  periode = p;
}

bool QTask::hasChanged() const{
  return changement;
}


QTask1D::QTask1D(const VectorXd & qWanted):
  Task(),
  periode(0.05),
  desir(qWanted)
{
  

  setWantedState(qWanted);
  etat_q = VectorXd::Zero(dimInput);
  dot_q = VectorXd::Zero(dimInput);
  setActivationMatrix(MatrixXd::Identity(dimInput,dimInput));
}


QTask1D::QTask1D(const VectorXd & qWanted, const MatrixXd & a):
  periode(0.05),
  desir(qWanted),
  activation(a)
{

  setWantedState(qWanted);
  etat_q = VectorXd::Zero(dimInput);
  dot_q = VectorXd::Zero(dimInput);
  setActivationMatrix(a);
}


void QTask1D::setWantedState(const VectorXd & q){
  desir = q;
  dimInput = static_cast<int>(q.rows());
  dimOutput = 1;
  changement = true;
}

void QTask1D::setActivationMatrix(const MatrixXd & a){
  activation = a;
  assert(a.rows() >  0);
  assert(a.rows() <= a.cols());
  assert(a.cols() == dimInput);
  dimOutput = 1;
}

void QTask1D::setState(const VectorXd & q){
  etat_q = q;
  changement = false;
}


VectorXd QTask1D::getState() const{
  return etat_q;
}

VectorXd QTask1D::fonction(const VectorXd & q) const{
  VectorXd I(q-desir);
  I = activation * I;
  double d2  = I.dot(I);
  double d = sqrt(d2);
  VectorXd output(VectorXd::Zero(1));
  output(0) = d;
  return output;
}

MatrixXd QTask1D::jacobien(const VectorXd & q) const{
  VectorXd I(q-desir);
  I = activation * I;
  double d2  = I.dot(I);
  double d = sqrt(d2);
  MatrixXd jacobienne(1/d*I.transpose()*activation);
  return jacobienne;
}

double QTask1D:: getPeriod() const{
  return  periode;
}

void QTask1D:: setPeriod(double p){
  periode = p;
}

bool QTask1D::hasChanged() const{
  return changement;
}
