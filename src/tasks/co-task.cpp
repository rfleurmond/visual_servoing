#include "co-task.h"

using namespace Eigen;

CoTask::CoTask(Task * brother1, Task * brother2):
  DualityTask(brother1,brother2)
{

}

VectorXd CoTask::calculCommandeFor(const VectorXd & erreur) const{
  int dim1 = maitre->getDimOutput();
  int dim2 = esclave->getDimOutput();
  VectorXd commande = 
    0.75 * maitre->calculCommandeFor(erreur.topRows(dim1)) + 
    0.25 * esclave->calculCommandeFor(erreur.bottomRows(dim2));
  return commande;
}

double CoTask:: getPeriod() const{
  return  maitre->getPeriod();
}

bool CoTask::hasChanged() const{
  return changement;
}

