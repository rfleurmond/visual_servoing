#include "egality-task.h"

using namespace Eigen;

EgalityTask::EgalityTask(Task * brother1, Task * brother2):
  DualityTask(brother1,brother2)
{

}

VectorXd EgalityTask::calculCommandeFor(const VectorXd & erreur) const{
  VectorXd commande, derivee;
  MatrixXd jacobienne = jacobien(etat_q);
  MatrixXd Jplus;
  pinv(jacobienne,Jplus);
  commande.noalias() = -lambda * Jplus * erreur;
  return commande;
}

double EgalityTask:: getPeriod() const{
  return  maitre->getPeriod();
}

bool EgalityTask::hasChanged() const{
  return changement;
}

