#include "noisy-system.h"

using namespace std;
using namespace Eigen;

NoisySystem::NoisySystem(TaskSystem * sys, const MatrixXd & covariance):
  TaskSystem(),
  cov(covariance),
  realSys(sys),
  roulette(new MultivariateGaussian(VectorXd::Zero(covariance.rows()),covariance))
{
  assert(sys!=NULL);
  assert(sys->getDimState()==cov.rows());
  assert(sys->getDimState()==cov.cols());
  realSys = sys;
}

NoisySystem::~NoisySystem(){
  delete roulette;
}

int NoisySystem::getDimState() const{
  return realSys->getDimState();
}

VectorXd NoisySystem::getState() const{
  VectorXd E = realSys->getState();
  VectorXd B = E;
  roulette->sample(B);
  return B+E;
}

void NoisySystem::setState(const VectorXd & state){
return realSys->setState(state);
}
void NoisySystem::takeCommand(const VectorXd & order){
  VectorXd B = order;
  roulette->sample(B);
  realSys->takeCommand(order+B);
}

void NoisySystem::updateTime(double t){
  realSys->updateTime(t);
}

double NoisySystem::getPeriod() const{
  return realSys->getPeriod();
}
 
