#include "priority-task.h"

using namespace Eigen;

DualityTask::DualityTask(Task * master, Task * slave):
  Task(),
  maitre(master),
  esclave(slave),
  hidden(-1)
{
  assert(master !=NULL);
  assert(slave != NULL);
  assert(master->getDimInput() == slave->getDimInput());
  dimInput = maitre->getDimInput();
  dimOutput = maitre->getDimOutput() + esclave->getDimOutput(); 
  lambda = maitre->getLambda();
  double l = esclave->getLambda();
  if(l>lambda){
    lambda = l;
  }
  borneInferieure = maitre->getMinimumErrorNorm();
  etat_q = master->getState();
}

void DualityTask::setState(const VectorXd & q){
  changement = false;
  maitre->setState(q);
  esclave->setState(q);
  etat_q = q;
  if(maitre->hasChanged()||esclave->hasChanged()){
    changement = true;
    dimOutput = maitre->getDimOutput() + esclave->getDimOutput(); 
    lambda = maitre->getLambda();
    double l = esclave->getLambda();
    if(l>lambda){
      lambda = l;
    }
    borneInferieure = maitre->getMinimumErrorNorm();
  }
}
VectorXd DualityTask::getState() const{
  return maitre->getState();
}


double DualityTask:: getPeriod() const{
  return  maitre->getPeriod();
}

bool DualityTask::hasChanged() const{
  return changement;
}

bool DualityTask::isComplete() const{
  return (maitre->isComplete() && esclave->isComplete());
}

void DualityTask::recordCommand(const VectorXd & dotQ){
  assert(etat_q.rows() == dotQ.rows());
  dot_q = dotQ;
  maitre->recordCommand(dotQ);
  esclave->recordCommand(dotQ);
}


VectorXd DualityTask::fonction(const VectorXd & q) const{
  int dim1 = maitre->getDimOutput();
  int dim2 = esclave->getDimOutput();
  VectorXd output = VectorXd::Zero(dimOutput);
  output.topRows(dim1) = maitre->fonction(q);
  output.bottomRows(dim2) = esclave->fonction(q);
  return output;
}

MatrixXd DualityTask::jacobien(const VectorXd & q) const{
  int dim1 = maitre->getDimOutput();
  int dim2 = esclave->getDimOutput();
  MatrixXd jacobienne = MatrixXd::Zero(dimOutput,dimInput);
  jacobienne.topRows(dim1) = lambda/maitre->getLambda()*maitre->jacobien(q);
  jacobienne.bottomRows(dim2) = lambda/esclave->getLambda()*esclave->jacobien(q);
  return jacobienne;
}

void DualityTask::hideTaskOnControl(Task * t){
  //assert(t!=NULL);
  //assert(t!=this);
  std::cout <<"[DualityTask]::Method <hideTaskOnControl> has not been overridden!"<<std::endl;
}
  
