#include "my-optima-priority-task.h"

using namespace Eigen;

MyOptimaPriorityTask::MyOptimaPriorityTask(Task * master, Task * slave):
  PriorityTask(master,slave)
{

}

VectorXd MyOptimaPriorityTask::calculCommandeFor(const VectorXd & erreur) const{
  int dim1 = maitre->getDimOutput();
  int dim2 = esclave->getDimOutput();
  VectorXd e1, e2, cmd1, cmd2,cmd,e1Te2,temp,addp,addo;
  MatrixXd J1,J2, J2P,J2Pinv, P1;
  double vpoint;
  e1 = erreur.topRows(dim1);
  e2 = erreur.bottomRows(dim2);
  J1 = maitre->jacobien(etat_q);
  J2 = esclave->jacobien(etat_q);
  cmd1 = maitre->calculCommandeFor(e1);
  cmd2 = esclave->calculCommandeFor(e2);
  if(hidden == 0){
    cmd = cmd2;
  }
  else if(hidden == 1){
    cmd = cmd1;
  }
  else{
    temp = cmd = cmd1;
    P1 = projecteur(J1);
    e1Te2.noalias() = J2*cmd1;
    vpoint = cmd1.dot(cmd2);
    if(vpoint < 0){
      cmd.noalias() += P1 * cmd2;
    }
    else
      {
	J2P.noalias() = J2*P1;
	pinv(J2P,J2Pinv);
	addp = P1 * cmd2;
	addo = J2Pinv * (J2 * cmd2 - e1Te2);
	cmd  += addp;
	temp += addo;

	if(temp.norm() < cmd.norm()){
	  cmd = temp;
	  //*
	  double deviation = addp.dot(addo);
	  if(deviation < 0){
	    cmd = cmd1 - addo;
	  }
	  //*/
	}

      }
  }
  return cmd;
}

