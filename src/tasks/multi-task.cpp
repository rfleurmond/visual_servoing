#include "multi-task.h"

using namespace Eigen;

MultiTask::MultiTask(int number, Task * first):
  Task(),
  n(number),
  tableau(NULL),
  defined(false)
{
  assert(number>0);
  assert(first !=NULL);

  etat_q = first->getState();
  dot_q = 0 * etat_q;

  defined = (n==1);
  dimInput = first->getDimInput();
  dimOutput = first->getDimOutput(); 
  borneInferieure = first->getMinimumErrorNorm();

  n = number;
  tableau = new Task * [n];
  tableau[0] = first;
  defined = false;
  checkAndComplete();
}

MultiTask::~MultiTask(){
  delete[] tableau;
}

void MultiTask::checkAndComplete(){
  int i = 0;
  defined =true;
  dimOutput = 0;
  while(i<n){
    if(tableau[i]==NULL){
      defined =false;
      break;
    }
    else{
      dimOutput+=tableau[i]->getDimOutput();
    }
    i++;
  }
  
}

void MultiTask::assingTaskToIndex(int i, Task * t){
  assert(i>0 && i<n);
  assert(t!=NULL);
  assert(t!=this);
  assert(t->getDimInput()== dimInput);
  tableau[i]=t;
  checkAndComplete();
}

void MultiTask::setState(const VectorXd & q){
  assert(defined==true);
  for(int i = 0; i<n; i++){
    tableau[i]->setState(q);
  }
  etat_q = q;
}

VectorXd MultiTask::getState() const{
  assert(defined==true);
  return etat_q;
}

VectorXd MultiTask::fonction(const VectorXd & q) const{
  assert(defined==true);
  VectorXd output = VectorXd::Zero(dimOutput);
  int dim = 0;
  int tot = 0;
  for(int i = 0; i<n; i++){
    dim = tableau[i]->getDimOutput();
    output.block(tot,0,dim,1) = tableau[i]->fonction(q);
    tot+=dim;
  }
  return output;
}

MatrixXd MultiTask::jacobien(const VectorXd & q) const{
  assert(defined==true);
  MatrixXd jacobienne = MatrixXd::Zero(dimOutput,dimInput);
  int dim = 0;
  int tot = 0;
  for(int i = 0; i<n; i++){
    dim = tableau[i]->getDimOutput();
    jacobienne.block(tot,0,dim,dimInput) = (1.0/tableau[i]->getLambda())*tableau[i]->jacobien(q);
    //jacobienne.block(tot,0,dim,dimInput) = tableau[i]->jacobien(q);
    tot+=dim;
  }
  return jacobienne;
}


VectorXd MultiTask::calculCommandeFor(const VectorXd & erreur) const{
  assert(defined==true);
  VectorXd commande, derivee;
  MatrixXd jacobienne = jacobien(etat_q);
  MatrixXd Jplus;
  pinv(jacobienne,Jplus);
  commande = -lambda * Jplus * erreur;
  return commande;
}

double MultiTask:: getPeriod() const{
  assert(defined==true);
  return  tableau[0]->getPeriod();
}

bool MultiTask::hasChanged() const{
  assert(defined==true);
  int i = 0;
  while(i<n){
    if(tableau[i]->hasChanged()){
      return true;
    }
  }
  return false;
}

