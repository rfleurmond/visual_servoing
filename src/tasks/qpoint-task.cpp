#include "qpoint-task.h"

using namespace Eigen;

QPointTask::QPointTask(const VectorXd & qWanted):
  Task(),
  periode(0.05),
  desir(qWanted)
{
  setWantedState(qWanted);
  etat_q = VectorXd::Zero(dimInput);
  dot_q = VectorXd::Zero(dimInput);
  setActivationMatrix(MatrixXd::Identity(dimOutput,dimInput));
}

QPointTask::QPointTask(const VectorXd & qWanted, const MatrixXd & a):
  Task(),
  periode(0.05),
  desir(qWanted),
  activation(a)
{
  setWantedState(qWanted);
  etat_q = VectorXd::Zero(dimInput);
  dot_q = VectorXd::Zero(dimInput);
  setActivationMatrix(a);
}

void QPointTask::setWantedState(const VectorXd & q){
  desir = q;
  dimInput = dimOutput = q.rows();
  changement = true;
}

void QPointTask::setActivationMatrix(const MatrixXd & a){
  activation = a;
  assert(a.rows() >  0);
  assert(a.rows() <= a.cols());
  assert(a.cols() == dimInput);
  dimOutput = a.rows();
}

void QPointTask::setState(const VectorXd & q){
  etat_q = q;
  changement = false;
}


VectorXd QPointTask::getState() const{
  return etat_q;
}

VectorXd QPointTask::fonction(const VectorXd & q) const{
  VectorXd FF(desir);
  FF.noalias() = -1.0 * activation * desir;
  return FF;
}

MatrixXd QPointTask::jacobien(const VectorXd & q) const{
  MatrixXd jacobienne(activation);
  jacobienne *= MatrixXd::Identity(dimInput,dimInput);
  return jacobienne;
}

double QPointTask:: getPeriod() const{
  return  periode;
}

void QPointTask:: setPeriod(double p){
  periode = p;
}

bool QPointTask::hasChanged() const{
  return changement;
}
