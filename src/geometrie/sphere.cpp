#include "sphere.h"

using namespace Eigen;
using namespace std;



Sphere::Sphere(){
  etat = EXISTS;
  type = SPHERE;
  rayon = 1;
}

Sphere::Sphere(Vector3d c, double r){
  etat = EXISTS;
  type = SPHERE;
  rayon = r;
  centre = c;
}


list<Vector3d> Sphere::getVectors() const{
  list<Vector3d> liste;
  return liste;
}

void Sphere::setVectors(const list<Vector3d> & l){

}
   
int Sphere::getDimensions() const{
  return 2;
}
  
int Sphere::getDimEspace() const{
  return 3;
}

bool Sphere::isSurface() const{
  return true;
}

bool Sphere::isVolumic() const{
  return false;
}

bool Sphere::hasPoint(Vector3d p) const{
  Vector3d CP = p - centre;
  if(isEqual(CP.norm(),rayon,CRAYON)){
    return true;
  }
  else
    return false;
}

Vector3d Sphere::getNearestPoint(Vector3d P) const{

  if(hasPoint(P)){
    return P;
  }

  Vector3d CP = P-centre;
  double n  = CP.norm();
  Vector3d M;
  if(isEqual(n,0,CRAYON)==false){
    M = rayon/n*CP + centre;
    return M;
  }

  M = centre;
  M(0)+=rayon;
  return M;
}

void Sphere::affiche(ostream &flux) const{
  double x,y,z;
  x = centre(0);
  y = centre(1);
  z = centre(2);
  flux<<"SPHERE:"<<endl;
  if(x<-CRAYON)
    flux<<"(x + "<<-x<<")^2 + ";
  else{
    if(x>CRAYON){
      flux<<"(x - "<<x<<")^2 + ";
    }
    else{
      flux<<"x^2 +";
    }
  }
  if(y<-CRAYON)
    flux<<"(y + "<<-y<<")^2 + ";
  else{
    if(y>CRAYON)
      flux<<"(y - "<<y<<")^2 + ";
    else
      flux<<"y^2 +";
  }
  if(z<-CRAYON)
    flux<<" (z + "<<-z<<")^2 ";
  else{
    if(z>CRAYON)
      flux<<" (z - "<<z<<")^2 ";
    else
      flux<<" z^2";
  }
  flux <<" = "<<rayon*rayon<<endl;
}

double Sphere::getRayon() const{
  return rayon;
}

void Sphere::setRayon(double r){
  rayon = r;
}

bool Sphere::isSameAs(const Sphere & other) const{
  Vector3d diff = other.getCenter()-centre;
  return (isEqual(diff.norm(),0,CRAYON) && isEqual(rayon,other.getRayon(),CRAYON));
}
 
