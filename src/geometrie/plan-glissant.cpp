#include "plan-glissant.h"

using namespace Eigen;
using namespace std;

list<Vector3d> PlanGlissant::getVectors() const{
  list<Vector3d> liste;
  liste.push_back(direction);
  return liste;
}

void PlanGlissant::setVectors(const list<Vector3d> & l){
  list<Vector3d>::const_iterator it;
  int i =0;
  for(it = l.begin(); it!=l.end(); it++){
    if(i==0)
      setDirection((*it));
    i++;
  }
}
   
int PlanGlissant::getDimensions() const{
  return 3;
}
  
int PlanGlissant::getDimEspace() const{
  return 3;
}

bool PlanGlissant::isSurface() const{
  return false;
}

bool PlanGlissant::isVolumic() const{
  return true;
}

bool PlanGlissant::hasPoint(Vector3d p) const{
  return true;
}

Vector3d PlanGlissant::getNearestPoint(Vector3d P) const{
  return P;
}


void PlanGlissant::setDirection(Vector3d d){
  direction = d.normalized();
}
  
Vector3d PlanGlissant::getDirection() const{
  return direction;
}

void PlanGlissant::affiche(ostream &flux) const{
  Plan PP;
  PP.setCenter(centre);
  PP.setDirection(direction);
  flux<<"PLAN GLISSANT"<<endl;
  flux<<"Le plan:"<<PP;
  flux<<"qui glisse sur la droite passant par le point\n"<<centre<<endl;
  flux<<"et de direction\n"<<direction<<endl; 
}

void PlanGlissant::setPlan(Plan p){
  direction = p.getDirection();
}


Plan PlanGlissant::getPlan(Vector3d P){
  Plan PP;
  PP.setCenter(P);
  PP.setDirection(direction);
  return PP;
}

