#include "cylindre.h"

using namespace Eigen;
using namespace std;

list<Vector3d> Cylindre::getVectors() const{
  list<Vector3d> liste;
  liste.push_back(direction);
  liste.push_back(translation);
  return liste;
}

void Cylindre::setVectors(const list<Vector3d> & l){
  list<Vector3d>::const_iterator it;
  int i =0;
  for(it = l.begin(); it!=l.end(); it++){
    if(i==0)
      setDirection((*it));
    if(i==1)
      setTranslation((*it));
    i++;
  }
}
   
int Cylindre::getDimensions() const{
  return 2;
}
  
int Cylindre::getDimEspace() const{
  return 3;
}

bool Cylindre::isSurface() const{
  return true;
}

bool Cylindre::isVolumic() const{
  return false;
}

bool Cylindre::hasPoint(Vector3d p) const{
  double num = direction.transpose()*(p-centre);
  double den = direction.transpose()*translation;
  if(isEqual(den,0,CRAYON)){
    double t = num/den;
    Vector3d rail = centre + translation *t;
    Cercle c;
    c.setCenter(rail);
    c.setRayon(rayon);
    c.setDirection(direction);
    return c.hasPoint(p);
  }
  else{
    double scal = translation.transpose()*(p-centre);
    Vector3d perp = p - centre - scal * translation;
    if(perp.norm()<=rayon){
      return true;
    }
    return false;
  }
}

Vector3d Cylindre::getNearestPoint(Vector3d P) const{
  if(hasPoint(P)){
    return P;
  }
  Droite d = getDroite();
  Vector3d rail = d.getNearestPoint(P);
  Cercle c;
  c.setCenter(rail);
  c.setRayon(rayon);
  c.setDirection(direction);
  Vector3d CP = c.getNearestPoint(P)-rail;
  Vector3d fleche = P-rail;
  fleche.normalize();
  Matrix3d PROJ = fleche * fleche.transpose();
  Vector3d M = rail + PROJ * CP;
  return M;
}

void Cylindre::setDirection(Vector3d d){
  direction = d.normalized();
  
}
  
Vector3d Cylindre::getDirection() const{
  return direction;
}

void Cylindre::affiche(ostream &flux) const{
  MatrixXd mat = MatrixXd::Identity(1,4);
  mat << direction.transpose(),-direction.transpose()*translation;
  Equation eq;
  VectorXd val = VectorXd::Zero(1);
  val(0) = centre.transpose()*direction;
  eq.setValeurs(mat,val);
  flux<<"Cylindre defini par l'intersection du plan glissant:\n"<<eq;
  flux<<"Et de la sphere glissante:"<<endl;
  double x,y,z,xx,yy,zz;
  x = centre(0);
  y = centre(1);
  z = centre(2);
  xx = translation(0);
  yy = translation(1);
  zz = translation(2);
  
  flux <<"(x - ("<<xx<<"t";
  if(x<=0)
    flux<<" - "<<-x<<"))^2 + ";
  else
    flux<<" + "<<x<<"))^2 + ";
  flux <<"(y - ("<<yy<<"t";
  if(y<=0)
    flux<<" - "<<-y<<"))^2 + ";
  else
    flux<<" + "<<y<<"))^2 + ";
  flux <<"(z - ("<<zz<<"t";
  if(z<=0)
    flux<<" - "<<-z<<"))^2 ";
  else
    flux<<" + "<<z<<"))^2 ";
  flux <<" = "<<rayon*rayon<<endl;
}


double Cylindre::getRayon() const{
  return rayon;
}

void Cylindre::setRayon(double r){
  rayon = r;
}

Vector3d Cylindre::getTranslation() const{
  return translation;
}

void Cylindre::setTranslation(Vector3d v){
  translation = v.normalized();
  //double val = translation.transpose() * direction;
  //if(isEqual(val,0,CRAYON)){
  //  translation = direction;
  //}
}

Droite Cylindre::getDroite() const{
  Droite d;
  d.setCenter(centre);
  d.setDirection(translation);
  return d;
}

Cercle Cylindre::getCercle(Vector3d P) const{
  Vector3d CP = P - centre;
  double num = direction.transpose()*CP;
  double den = direction.transpose()*translation;
  Cercle c;
  if(isEqual(den,0,CRAYON)){
    double t = num/den;
    Vector3d rail = centre + translation *t;
    c.setCenter(rail);
    c.setRayon(rayon);
    c.setDirection(direction);
    return c;
  }
  else{
    double scal = translation.transpose()*CP;
    Vector3d perp = CP - scal * translation;
    double cosa = 0;
    if(perp.norm()<=rayon){
      cosa = sqrt(rayon*rayon - perp.norm()*perp.norm());
    }
    c.setCenter(centre + (scal-cosa) * translation);
    c.setRayon(rayon);
    c.setDirection(direction);
    return c;
  }
} 
