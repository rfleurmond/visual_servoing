#include "lieu.h"

using namespace std;
using namespace Eigen;

bool Lieu::isLinear() const{
  return getDimEspace()==1;
}

bool Lieu::isPlanar() const{
  return getDimEspace()==2;
}

bool Lieu::isSpacial() const{
  return getDimEspace()==3;
}

double Lieu::getLowestDistance(Vector3d p) const{
  Vector3d dist = p-getNearestPoint(p);
  return dist.norm();
}

void Lieu::getTransformation(Repere se3){

  list<Vector3d>::const_iterator it;

  list<Vector3d> copie, original;

  Vector3d nombril, milieu,A,B;

  nombril =  getCenter();

  original = getVectors();

  for(it = original.begin(); it != original.end(); it++){

    A = (*it);
    B = se3.getVectorInParent(A);
    copie.push_back(B);

  }
    
  milieu = se3.getPointInParent(nombril);

  setCenter(milieu);

  setVectors(copie);

}

int Lieu::getType(){
  return type;
}
      
Vector3d Lieu::getCenter() const {
  return centre;
}

void Lieu::setCenter(Vector3d c){
  centre = c;
}

bool Lieu::exists(){
  return etat!=NO_EXISTS;
}

bool Lieu::isDefined(){
  return type!=NOT_DEFINED;
}

string Lieu::afficheType(){
  return Lieu::getStringType(getType());
}

string Lieu::getStringType(int type){

  string s("");
  switch(type){
  case Lieu::POINT: s = string("POINT");break;
  case Lieu::DROITE: s = string("DROITE");break;
  case Lieu::PLAN : s = string("PLAN");break;
  case Lieu::CIRCLE : s = string("CERCLE");break;
  case Lieu::DISQUE : s = string("DISQUE");break;
  case Lieu::CYLINDRE : s = string("CYLINDRE");break;
  case Lieu::SPHERE : s = string("SPHERE");break;
  case Lieu::BOULE : s = string("BOULE");break;
  case Lieu::PLANTOURNANT : s = string("PLAN TOURNANT ");break;
  case Lieu::CONE : s = string("CONE");break;
  case Lieu::TORE : s = string("TORE");break;
  case Lieu::DROITETOURNANTE : s = string("DROITE TOURNANTE");break;
  case Lieu::PLANGLISSANT : s = string("PLAN GLISSANT");break;
  case Lieu::NOT_DEFINED : s = string("NOT DEFINED");break;
  default : s = string("INCONNU");
  }
  return s;

}

ostream& operator << (ostream& flux, Lieu & l){
  l.affiche(flux);
  return flux;
}
