#include "architect.h"

using namespace Eigen;
using namespace std;

list<Plan> Architect::combinaison(const LPoint & p, const Droite & d){
  
  list<Plan> liste;
  if(d.hasPoint(p.getCenter())){
    return liste;
  }

  Vector3d pp = d.getNearestPoint(p.getCenter());
  Vector3d cp = pp-p.getCenter();
  Vector3d normale = cp.cross(d.getDirection());
  normale.normalize();
  Plan plan;
  plan.setCenter(p.getCenter());
  plan.setDirection(normale);

  liste.push_back(plan);

  return liste;
}

list<Plan> Architect::combinaison(const Droite & d1, const Droite & d2){
 
  list<Plan> liste;

  Vector3d normale = d1.getDirection().cross(d2.getDirection());
  Vector3d p1 = d1.getCenter();
  Vector3d p2 = p1 + d1.getDirection();
  bool b1 = d2.hasPoint(p1);
  bool b2 = d2.hasPoint(p2);
    
  if(isEqual(normale.norm(),0,CRAYON)){
    if(b1&&b2){
      return liste;
    }
    if(b1){
      liste = combinaison(p2,d2);
    }
    if(b2){
      liste = combinaison(p1,d2);
    }
    return liste;
  }

  Plan pp;
  pp.setCenter(p1);
  pp.setDirection(normale);
  b1 = pp.hasPoint(p2);
  b2 = pp.hasPoint(p1+d2.getDirection());
  if(b1&&b2){
    liste.push_back(pp);
  }
   
  return liste;
}  

//Cone Architect::combinaison(Cercle centre, Droite motif);

Tore Architect::combinaison(const Cercle & centre, const Cercle & motif){
  Tore tore;
  tore.setCenter(centre.getCenter());
  tore.setRotation(centre.getDirection());
  tore.setCercle(motif);
  return tore;
}


PlanTournant Architect::combinaison(const Cercle & centre, const Plan & motif){
  PlanTournant pt;
  pt.setCenter(centre.getCenter());
  pt.setRotation(centre.getDirection());
  pt.setPlan(motif);
  return pt;
}

Cylindre Architect::combinaison(const Droite & translation, const Cercle & motif){
  Cylindre cyl(motif,translation.getDirection());
  return cyl;
}

list<LPoint> Architect::intersection(const Droite & d1, const Droite & d2){
  list<LPoint> liste;
  Vector3d normale = d1.getDirection().cross(d2.getDirection());
  Vector3d p1 = d1.getCenter();
  Vector3d p3 = d2.getCenter();
  if(isEqual(normale.norm(),0,CRAYON)){
    Vector3d p2 = p1 + d1.getDirection();
    Vector3d p4 = p3 + d2.getDirection();
    if(d2.hasPoint(p1)){
      LPoint lp(p1);
      liste.push_back(lp);
    }
    if(d2.hasPoint(p2)){
      LPoint lp(p2);
      liste.push_back(lp);
    }
    if(d1.hasPoint(p3)){
      LPoint lp(p3);
      liste.push_back(lp);
    }
    if(d1.hasPoint(p4)){
      LPoint lp(p4);
      liste.push_back(lp);
    }
    return liste;
  }
  
  normale.normalize();
  Vector3d cc = p3-p1;
  MatrixXd AA = MatrixXd::Zero(3,3);
  AA.col(0) = d1.getDirection();
  AA.col(1) = d2.getDirection();
  AA.col(2) = normale;
  VectorXd para;
  LeastSquares::solve(cc,AA,para);

  if(isEqual(para(2),0,CRAYON)==false){
    return liste;
  }

  double a = para(0);
  
  Vector3d sol = p1+ a * d1.getDirection();
  LPoint lp(sol);
  liste.push_back(lp);
  
  return liste;

}
  
list<LPoint> Architect::intersection(const Droite & d1, const Plan & p2){

  list<LPoint> liste;
  Vector3d p1 = d1.getCenter();
  Vector3d pt2 = p1 + d1.getDirection();
  bool b1 = p2.hasPoint(p1);
  bool b2 = p2.hasPoint(pt2);
  if(b1){
    LPoint lp(p1);
    liste.push_back(lp);
  }
  if(b2){
    LPoint lp(pt2);
    liste.push_back(lp);
  }
  if(b1||b2){
    return liste;
  }
  
  Vector3d C,M,N,U;
  M = p1;
  C = p2.getCenter();
  U = d1.getDirection();
  N = p2.getDirection();
  double deno = U.transpose()*N;
  double nume = N.transpose()*(C-M);
  if(isEqual(deno,0,CRAYON)){
    return liste;
  }

  double t = nume/deno;
  pt2 = p1 + t * U;
  LPoint lp(pt2);
  liste.push_back(lp);

  return liste;

}


list<LPoint> Architect::intersection(const Droite & d1, const Sphere & s2){

  list<LPoint> liste;
  /**
     C centre de la sphere
     A point de la droite
     X le point recherche
     U le vecteur directeur de la droite
     r le rayon de la sphere
 
     |X - c| = r
     X = A + t *U
     donne
     t2 * (U.U) + 2*t*[ U.(A-C)] + (A-C).(A-C) -r2 = 0
  */

  Vector3d C = s2.getCenter();
  double r = s2.getRayon();
  double r2 = r * r;
  Vector3d A = d1.getCenter();
  Vector3d AC = A - C;
  Vector3d U = d1.getDirection();

  double para[3];
  para[0] = AC.transpose()*AC -r2;
  para[1] = 2*U.transpose()*AC;
  para[2] = U.transpose()*U;

  polynome poly(3,para);

  list<double> racines = polyfacteur::extraire2racines(poly);

  list<double>::const_iterator it;

  double t1,t2;
  int i=0;

  for(it =racines.begin();it!=racines.end();it++){
    if(i==0)
      t1 = (*it);
    if(i==1)
      t2 = (*it);
    i++;
  }

  if(i==0){
    return liste;
  }
  
  Vector3d sol;
  if(i>=1){
    sol = A+t1*U;
    LPoint lp(sol);
    liste.push_back(lp);
  }

  if(i==2){
    if(isEqual(t1,t2,CRAYON)==false){
      sol = A+t2*U;
      LPoint lp(sol);
      liste.push_back(lp);
    }
  }
  return liste;
}

list<LPoint> Architect::intersection(const Droite & d1, const Cercle & c2){

  list<LPoint> premier = intersection(d1,c2.getSphere());

  Plan pp = c2.getPlan();
  
  list<LPoint> deuxieme;

  list<LPoint>::const_iterator it;

  LPoint lp;
  Vector3d point;

  for(it = premier.begin(); it != premier.end(); it++){
    lp = (*it);
    point = lp.getCenter();
    if(pp.hasPoint(point)){
      deuxieme.push_back(lp);
    }
  }

  return deuxieme;
}
  

list<Droite> Architect::intersection(const Plan & p1, const Plan & p2){
  list<Droite> liste;
  Vector3d U = p1.getDirection();
  Vector3d V = p2.getDirection();
  Vector3d UV = U.cross(V);

  if(isEqual(UV.norm(),0,CRAYON)){
    if(p1.hasPoint(p2.getCenter())){
      Vector3d A;
      A << U(1),U(2),U(0);
      A = A.cross(U);
      A.normalize();
      Droite d1(p1.getCenter(),A);
      Vector3d B = A.cross(U);
      B.normalize();
      Droite d2(p1.getCenter(),B);
      liste.push_back(d1);
      liste.push_back(d2);
    }
    return liste;
  }

  UV.normalize();
	
  Vector3d pt1 = p1.getCenter();
  Vector3d pt2 = p2.getCenter();
  bool b12 = p1.hasPoint(pt2);
  bool b21 = p2.hasPoint(pt1);
  
  if(b12){
    Droite d(pt2,UV);
    liste.push_back(d);
    return liste;
  }
  if(b21){
    Droite d(pt1,UV);
    liste.push_back(d);
    return liste;
  }

  Vector3d W = UV.cross(U);
  W.normalize();
  
  /*
    si 
    pt1 un point du plan p1
    pt2 un point du plan p2
    W un vecteur perpendiculaire a U et a la droite d' intersection (UV)
    V le vecteur normal au plan p2
    U le vecteur normal au plan p1

    X = pt1 + t * W
    X.V = V.pt2

  */ 

  double deno = W.transpose()*V;
  double nume = V.transpose()*(pt2-pt1);
  double t = nume/deno;
  Vector3d centre = pt1 + t * W;

  Droite d(centre,UV);

  liste.push_back(d);
  
  return liste;
}

list<Cercle> Architect::intersection(const Plan & p1, const Sphere & s2){

  list<Cercle> liste;

  Vector3d P = s2.getCenter();
  Vector3d centre = p1.getNearestPoint(P);
  Vector3d CP = P - centre;
  double d = CP.norm();
  
  double rayon = s2.getRayon();

  if(d>rayon+CRAYON){
    return liste;
  }

  double r;
  if(isEqual(d,rayon,CRAYON)){
    r = 0;
  }
  else{
    r = sqrt(rayon*rayon - d * d);
  }
  Cercle c;
  c.setCenter(centre);
  c.setRayon(r);
  c.setDirection(p1.getDirection());
  liste.push_back(c);

  return liste;

}

list<Disque> Architect::intersection(const Plan & p1, const Boule & b2){
  list<Disque> selection;
  Sphere s2 = b2.getContour();
  list<Cercle> minerai = intersection(p1,s2);
  list<Cercle>::const_iterator it;
  for(it = minerai.begin();it!=minerai.end();it++){
    Cercle c = (*it);
    Disque d(c);
    selection.push_back(d);
  }

  return selection;
}
  
list<LPoint> Architect::intersection(const Plan & p1, const Cercle & c2){
  list<LPoint> selection;
  Plan p2 = c2.getPlan();
  list<Droite> minerai = intersection(p1,p2);
  list<Droite>::const_iterator it;
  for(it = minerai.begin();it!=minerai.end();it++){
    Droite d = (*it);
    list<LPoint> boisseau = intersection(d, c2);
    list<LPoint>::const_iterator it2;
    for(it2 = boisseau.begin();it2!=boisseau.end();it2++){
      LPoint lp = (*it2);
      selection.push_back(lp);
    }
  }

  return selection;
}

list<LPoint> Architect::intersection(const Cercle & c1, const Cercle & c2){
  list<LPoint> selection;
  Plan p1 = c1.getPlan();
  Plan p2 = c2.getPlan();
  list<LPoint> boisseau;
  list<LPoint>::const_iterator it2;
  LPoint lp;
  Vector3d point;
  if(p1.isSameAs(p2)){
    Sphere s1 = c1.getSphere();
    Sphere s2 = c2.getSphere();
    list<Cercle> minerai= intersection(s1,s2);
    list<Cercle>::const_iterator it;
    for(it = minerai.begin();it!=minerai.end();it++){
      Cercle cc = (*it);
      boisseau = intersection(p1, cc);
      for(it2 = boisseau.begin();it2!=boisseau.end();it2++){
	lp = (*it2);
	point = lp.getCenter();
	selection.push_back(lp);
      }
    }
    
  }
  else{
    list<Droite> minerai = intersection(p1,p2);
    list<Droite>::const_iterator it;
    for(it = minerai.begin();it!=minerai.end();it++){
      Droite d = (*it);
      boisseau = intersection(d, c2);
      for(it2 = boisseau.begin();it2!=boisseau.end();it2++){
	lp = (*it2);
	point = lp.getCenter();
	if(c1.hasPoint(point)){
	  selection.push_back(lp);
	}
      }
    }
  }
  
  return selection;
}
  


list<LPoint> Architect::intersection(const Cercle & c1, const Sphere & s2){
  list<LPoint> selection,minerai;
  list<LPoint>::const_iterator itp;
  Plan p1 = c1.getPlan();
  list<Cercle> anneaux = intersection(p1,s2);
  list<Cercle>::const_iterator it;
  for(it = anneaux.begin();it!=anneaux.end();it++){
    Cercle cc = (*it);
    minerai = Architect::intersection(c1, cc);
    for(itp = minerai.begin();itp!=minerai.end();itp++){
      LPoint lp = *itp;
      selection.push_back(lp);
    }
  }
   return minerai;
}


list<Cercle> Architect::intersection(const Sphere & s1, const Sphere & s2){
  list<Cercle> liste;
  Vector3d c1 = s1.getCenter();
  Vector3d c2 = s2.getCenter();
  double r1 = s1.getRayon();
  double r2 = s2.getRayon();
  Vector3d CC = c2-c1;
  double l = CC.norm();
  if(l>r1+r2+CRAYON){
    return liste;
  }
  
  Cercle cercle;
  
  if(isEqual(l,0,CRAYON)){
    if(isEqual(r1,r2,CRAYON)){
      Vector3d U;
      U<<1,0,0;
      cercle.setCenter(c1);
      cercle.setRayon(r1);
      cercle.setDirection(U);
      liste.push_back(cercle);
      U<<0,1,0;
      cercle.setCenter(c1);
      cercle.setRayon(r1);
      cercle.setDirection(U);
      liste.push_back(cercle);
      U<<0,0,1;
      cercle.setCenter(c1);
      cercle.setRayon(r1);
      cercle.setDirection(U);
      liste.push_back(cercle);
    }
    cout<<endl;
    return liste;
  }

  double val = 0.5*((r1*r1 -r2*r2)/(l*l) + 1);
  Vector3d centre = c1+ val*CC;
  val = val*l;
  double rayon = sqrt(r1*r1 -val*val);
  Vector3d U = CC.normalized();
  cercle.setCenter(centre);
  cercle.setRayon(rayon);
  cercle.setDirection(U);
  liste.push_back(cercle);
  return liste;
}
  
list<Disque> Architect::intersection(const Boule & b1, const Boule & b2){
  list<Disque> selection;
  Sphere s1 = b1.getContour();
  Sphere s2 = b2.getContour();
  list<Cercle> minerai = intersection(s1,s2);
  list<Cercle>::const_iterator it;
  for(it = minerai.begin();it!=minerai.end();it++){
    Cercle c = (*it);
    Disque d(c);
    selection.push_back(d);
  }

  return selection;
}

Vector3d Architect::antiProjection(Vector3d direction, Vector3d sujet){
  return sujet - projection(direction,sujet);
}


Vector3d Architect::projection(Vector3d direction, Vector3d sujet){
  if(isEqual(direction.norm(),0,CRAYON)){
    return direction;
  }
  double scal = direction.transpose()*sujet;
  Vector3d solution = scal * direction.normalized();
  return solution;
}

  

