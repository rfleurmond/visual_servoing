#include "disque.h"

using namespace Eigen;
using namespace std;

Disque::Disque(){
  etat = EXISTS;
  type = DISQUE;
  direction <<0,0,1;
  rayon = 1;
}

Disque::Disque(Cercle c){
  etat = EXISTS;
  type = DISQUE;
  centre = c.getCenter();
  direction = c.getDirection();
  rayon = c.getRayon();
}
  

list<Vector3d> Disque::getVectors() const{
  list<Vector3d> liste;
  liste.push_back(direction);
  return liste;
}

void Disque::setVectors(const list<Vector3d> &l){
  list<Vector3d>::const_iterator it;
  if(l.size()>0){
    it = l.begin();
    setDirection((*it));
  }
}
   
int Disque::getDimensions() const{
  return 2;
}
  
int Disque::getDimEspace() const{
  return 2;
}

bool Disque::isSurface() const{
  return true;
}

bool Disque::isVolumic() const{
  return false;
}

bool Disque::hasPoint(Vector3d p) const{
  Vector3d CP = p - centre;
  double val = CP.transpose()*direction; 
  if(isEqual(val,0,CRAYON)){
    if(CP.norm()<=rayon+CRAYON){
      return true;
    }
    else
      return false;
  } 
  else
    return false;
}

Vector3d Disque::getNearestPoint(Vector3d P) const{

  if(hasPoint(P)){
    return P;
  }

  Plan plan1 = this->getPlan();
  Vector3d PP = plan1.getNearestPoint(P);
  if(hasPoint(PP)){
    return PP;
  }
  Vector3d PV = PP-centre;
  double n  = PV.norm();
  Vector3d M;
  if(isEqual(n,0,CRAYON)==false){
    M = rayon/n*PV + centre;
    return M;
  }

  return centre;
}


void Disque::affiche(ostream &flux) const{
  MatrixXd mat = MatrixXd::Identity(1,3);
  mat = direction.transpose();
  Equation eq;
  VectorXd val = VectorXd::Zero(1);
  val(0) = centre.transpose()*direction;
  eq.setValeurs(mat,val);
  flux<<"Disque defini par l'intersection du plan:\n"<<eq;
  flux<<"Et de la boule:"<<endl;
  Boule bb;
  bb.setCenter(centre);
  bb.setRayon(rayon);
  flux<<bb;
  
}

Cercle Disque::getContour(){
  Cercle c;
  c.setCenter(getCenter());
  c.setDirection(getDirection());
  c.setRayon(getRayon());
  return c;
}

bool Disque::isSameAs(Disque & other){
  Cercle c1 = getContour();
  Cercle c2 = other.getContour();
  return c1.isSameAs(c2);
}
