#include "droite.h"

using namespace Eigen;
using namespace std;


list<Vector3d> Droite::getVectors() const{
  list<Vector3d> liste;
  liste.push_back(direction);
  return liste;
}

void Droite::setVectors(const list<Vector3d> & l){
  list<Vector3d>::const_iterator it;
  if(l.size()>0){
    it = l.begin();
    setDirection((*it));
  }
}
   
int Droite::getDimensions() const{
  return 1;
}
  
int Droite::getDimEspace() const{
  return 1;
}

bool Droite::isSurface() const{
  return false;
}

bool Droite::isVolumic() const{
  return false;
}

bool Droite::hasPoint(Vector3d p) const{
  Vector3d CP = p - centre;
  Vector3d U = CP.cross(direction);
  if(isEqual(U.norm(),0,CRAYON))
    return true;
  else
    return false;
}

Vector3d Droite::getNearestPoint(Vector3d P) const{

  Vector3d CP = P - centre;
  Vector3d U = CP.cross(direction);
  if(isEqual(U.norm(),0,CRAYON)){
    return P;
  }
  Vector3d V = U.cross(direction);
  MatrixXd mat = MatrixXd::Zero(3,2);
  mat.col(0) = direction;
  mat.col(1) = -V;
  VectorXd sol = VectorXd::Zero(2);
  LeastSquares::solvePseudoInverse(CP,mat,sol);
  Vector3d M = 0.5*( P + sol(1)*V + centre + sol(0)*direction);
  return M;
}

void Droite::setDirection(Vector3d d){
  direction = d.normalized();
  
}
  
Vector3d Droite::getDirection() const{
  return direction;
}

void Droite::affiche(ostream &flux) const{
  MatrixXd mat = MatrixXd::Identity(3,4);
  mat.col(3) = - direction;
  Equation eq;
  Vector3d val = centre;
  eq.setValeurs(mat,val);
  flux<<"Equations de la droite:\n"<<eq;
}

 
