#include "cercle.h"

using namespace Eigen;
using namespace std;


Cercle::Cercle(){
  etat = EXISTS;
  type = CIRCLE;
  direction <<0,0,1;
  centre <<0,0,0;
  rayon = 1;
}

list<Vector3d> Cercle::getVectors() const{
  list<Vector3d> liste;
  liste.push_back(direction);
  return liste;
}

void Cercle::setVectors(const list<Vector3d> & l){
  list<Vector3d>::const_iterator it;
  if(l.size()>0){
    it = l.begin();
    setDirection((*it));
  }
}
   
int Cercle::getDimensions() const{
  return 1;
}
  
int Cercle::getDimEspace() const{
  return 2;
}

bool Cercle::isSurface() const{
  return false;
}

bool Cercle::isVolumic() const{
  return false;
}

bool Cercle::hasPoint(Vector3d p) const{
  Vector3d CP = p - centre;
  double val = CP.transpose()*direction; 
  if(isEqual(val,0,CRAYON)){
    if(isEqual(CP.norm(),rayon,CRAYON)){
      return true;
    }
    else
      return false;
  } 
  else
    return false;
}

Vector3d Cercle::getNearestPoint(Vector3d P) const{

  if(hasPoint(P)){
    return P;
  }

  Plan plan1 = this->getPlan();
  Vector3d PP = plan1.getNearestPoint(P)-centre;
  double n  = PP.norm();
  Vector3d M;
  if(isEqual(n,0,CRAYON)==false){
    M = rayon/n*PP + centre;
    return M;
  }

  Vector3d U,V;
  U<<direction(2),direction(3),direction(1);
  U.normalize();
  V = direction.cross(U);
  V.normalize();
  M= rayon * V + centre;
  return M;
}

void Cercle::setDirection(Vector3d d){
  direction = d.normalized();
  
}
  
Vector3d Cercle::getDirection() const{
  return direction;
}

void Cercle::affiche(ostream &flux) const{
  MatrixXd mat = MatrixXd::Identity(1,3);
  mat = direction.transpose();
  Equation eq;
  VectorXd val = VectorXd::Zero(1);
  val(0) = centre.transpose()*direction;
  eq.setValeurs(mat,val);
  flux<<"Cercle defini par l'intersection du plan:\n"<<eq;
  flux<<"Et de la sphere:";
  Sphere ss;
  ss.setCenter(centre);
  ss.setRayon(rayon);
  flux<<ss;
}

Plan Cercle::getPlan() const{
  Plan p;
  p.setDirection(direction);
  p.setCenter(centre);
  Vector3d milieu = p.getNearestPoint(Vector3d::Zero());
  p.setCenter(milieu);
  return p;
}

Sphere Cercle::getSphere() const{
  Sphere sp(centre,rayon);
  return sp;
}
  
bool Cercle::isSameAs(Cercle & other){
  Plan p1 = getPlan();
  Plan p2 = other.getPlan();
  Sphere s1 = getSphere();
  Sphere s2 = other.getSphere();
  return (p1.isSameAs(p2) && s1.isSameAs(s2));
}

 
