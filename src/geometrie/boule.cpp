#include "boule.h"

using namespace Eigen;
using namespace std;

Boule::Boule(){
  etat = EXISTS;
  type = BOULE;
  rayon = 1;
  centre << 0,0,0;
}

Boule::Boule(Vector3d c, double r){
  etat = EXISTS;
  type = BOULE;
  rayon = r;
  centre = c;
}

Boule::Boule(Sphere s){
  etat = EXISTS;
  type = BOULE;
  centre = s.getCenter();
  rayon = s.getRayon();
}


list<Vector3d> Boule::getVectors() const{
  list<Vector3d> liste;
  return liste;
}

void Boule::setVectors(const list<Vector3d> & l){

}
   
int Boule::getDimensions() const{
  return 2;
}
  
int Boule::getDimEspace() const{
  return 3;
}

bool Boule::isSurface() const{
  return true;
}

bool Boule::isVolumic() const{
  return true;
}

bool Boule::hasPoint(Vector3d p) const{
  Vector3d CP = p - centre;
  if(CP.norm()<=rayon){
    return true;
  }
  else
    return false;
}

Vector3d Boule::getNearestPoint(Vector3d P) const{
  if(hasPoint(P)){
    return P;
  }
  Vector3d CP = P-centre;
  double n  = CP.norm();
  Vector3d M;
  M = rayon/n*CP + centre;
  return M;
 
}

void Boule::affiche(ostream &flux) const{
  double x,y,z;
  x = centre(0);
  y = centre(1);
  z = centre(2);
  
  if(x<-CRAYON)
    flux<<"(x + "<<-x<<")^2 + ";
  else{
    if(x>CRAYON){
      flux<<"(x - "<<x<<")^2 + ";
    }
    else{
      flux<<"x^2 +";
    }
  }
  if(y<-CRAYON)
    flux<<"(y + "<<-y<<")^2 + ";
  else{
    if(y>CRAYON)
      flux<<"(y - "<<y<<")^2 + ";
    else
      flux<<"y^2 +";
  }
  if(z<-CRAYON)
    flux<<" (z + "<<-z<<")^2 ";
  else{
    if(z>CRAYON)
      flux<<" (z - "<<z<<")^2 ";
    else
      flux<<" z^2";
  }
  flux <<" <= "<<rayon*rayon<<endl;
}


Sphere Boule::getContour() const{
  Sphere sp(centre,rayon);
  return sp;
}

bool Boule::isSameAs(const Boule & other) const{
  Vector3d diff = other.getCenter()-centre;
  return (isEqual(diff.norm(),0,CRAYON) && isEqual(rayon,other.getRayon(),CRAYON));
}


 
