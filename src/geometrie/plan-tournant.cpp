#include "plan-tournant.h"

using namespace Eigen;
using namespace std;

list<Vector3d> PlanTournant::getVectors() const{
  list<Vector3d> liste;
  liste.push_back(direction);
  liste.push_back(rotation);
  return liste;
}

void PlanTournant::setVectors(const list<Vector3d> & l){
  list<Vector3d>::const_iterator it;
  int i =0;
  for(it = l.begin(); it!=l.end(); it++){
    if(i==0)
      setDirection((*it));
    if(i==1)
      setRotation((*it));
    i++;
  }
}
   
int PlanTournant::getDimensions() const{
  return 3;
}
  
int PlanTournant::getDimEspace() const{
  return 3;
}

bool PlanTournant::isSurface() const{
  return false;
}

bool PlanTournant::isVolumic() const{
  return true;
}

bool PlanTournant::hasPoint(Vector3d p) const{
  double scal = 0;
  Vector3d U,V,CP,X;
  Vector2d Y;
  VectorXd SOL;
  CP = p - centre;
  scal = direction.transpose()*rotation;
  U = direction.cross(rotation);
  V = CP.cross(rotation);
  /* si le vecteur normal au plan est parrallele avec l' axe de rotation
     le lieu geometrique est un seul et meme plan
  */

  if(isEqual(U.norm(),0,CRAYON)){
    V = centre + distance * direction;
    Plan PP;
    PP.setCenter(V);
    PP.setDirection(direction);
    return PP.hasPoint(p);
  }

  /**
     recherche de la direction du plan qui contiendrait le point p
     avec pour contrainte si 
     X le vecteur unitaire normal au plan
     R le vecteur autour duquel se fait la rotation

     equations
     X . p = distance equation du plan
     X . R = constante le vecteur R tourne autour du plan
     X . X = 1 vecteur unitaire

     X est combinaison lineaire de P, de R et de P^R

     En premier lieu a partir des deux premieres equations
     X = a.P + b.R + t. (P^R) t est quelconque c'est l'equation d'une droite
     X = U + t . (P^R) avec U = a.P + b.R 
     En second lieu cette droite rencontre la sphere unite pour la troisieme equation
     on a :
     0 solution  |U| > 1 le point n'appartient pas au lieu
     1 solution  |U| = 1 le point est a la limite
     2 solutions |U| < 1 le point est dans le lieu

     Cas particulier P || R

     Solution seulement si |P| . constant = distance

  */

  if(isEqual(V.norm(),0,CRAYON)){
    if(isEqual(distance,CP.norm()*scal,CRAYON)){
      return true;
    }
    else{
      return false;
    }
  }

  MatrixXd A1 = MatrixXd::Zero(2,3);
  A1.row(0)= CP.transpose();
  A1.row(1) = rotation.transpose();
  MatrixXd A2 = MatrixXd::Zero(3,2);
  A2.col(0) = CP;
  A2.col(1) = rotation;
  MatrixXd A3(3,3);
  A3.noalias() = A1*A2;
  Y << distance, scal;
  LeastSquares::solve(Y, A3 ,SOL);

  U = A2*SOL;

  if(U.norm()>1+CRAYON){
    return false;
  }

  return true;
 
}

Vector3d PlanTournant::getNearestPoint(Vector3d P) const{

  if(hasPoint(P)){
    return P;
  }

  double scal = 0;
  Vector3d U,V,CP,X,M,MM,DD,Y;
  VectorXd SOL;
  CP = P - centre;
  scal = direction.transpose()*rotation;
  U = direction.cross(rotation);

  /* si le vecteur normal au plan est parrallele avec l' axe de rotation
     le lieu geometrique est un seul et meme plan
  */

  if(isEqual(U.norm(),0,CRAYON)){
    V = centre + distance * direction;
    Plan PP;
    PP.setCenter(V);
    PP.setDirection(direction);
    return PP.getNearestPoint(P);
  }

  X = CP.cross(rotation);

  if(isEqual(X.norm(),0,CRAYON)){
    V = centre + distance * direction;
    Plan PP;
    PP.setCenter(V);
    PP.setDirection(direction);
    return PP.getNearestPoint(P);
  }
  
  double scalP = P.normalized().transpose()*rotation;
  U = P.normalized() - scalP * rotation;
  U.normalize();
  V = scal * rotation;

  /*
     X  = t*U + scal * Rotation = t* U + V
    |X| = 1 = U.U .t2 + 2 U.V t + V.V

  */

  double para[3];
  para[0] = V.transpose()*V-1;
  para[1] = 2*V.transpose()*U;
  para[2] = U.transpose()*U;

  polynome poly(3,para);

  list<double> racines = polyfacteur::extraire2racines(poly);

  list<double>::const_iterator it;

  double ecartM = 100*distance;

  for(it = racines.begin();it!=racines.end();it++){
    double t = (*it);
    X = t*U +V;
    X.normalize();
    Y = centre + distance * X;
    Plan PP;
    PP.setCenter(Y);
    PP.setDirection(X);
    M = PP.getNearestPoint(P);
    DD = P-M;
    if(DD.norm()<ecartM){
      MM = M;
      ecartM = DD.norm();
    }
  }

  return MM;

}


void PlanTournant::setDirection(Vector3d d){
  direction = d.normalized();
  
}
  
Vector3d PlanTournant::getDirection() const{
  return direction;
}

void PlanTournant::affiche(ostream &flux) const{
  Plan PP;
  PP.setCenter(centre + direction* distance);
  PP.setDirection(direction);
  flux<<"PLAN TOURNANT"<<endl;
  flux<<"Le plan:"<<PP;
  flux<<"qui tourne de l' axe de rotation passant par le point\n"<<centre<<endl;
  flux<<"et de direction\n"<<rotation<<endl; 
}


double PlanTournant::getDistance() const{
  return distance;
}

void PlanTournant::setDistance(double r){
  distance = r;
}

Vector3d PlanTournant::getRotation() const{
  return rotation;
}

void PlanTournant::setRotation(Vector3d v){
  rotation = v.normalized();

}

void PlanTournant::setPlan(Plan p){
  Vector3d d = p.getNearestPoint(centre)-centre;
  direction = d.normalized();
  distance = d.norm();
}

Droite PlanTournant::getAxeRotation(){
  Droite d;
  d.setCenter(centre);
  d.setDirection(rotation);
  return d;
}

list<Plan> PlanTournant::getSuitablePlans(Vector3d p, int * n){

  list<Plan> coupes;

  *n =0;

  if(hasPoint(p)==false){
    *n = 0;
    //cout<<"Le point n'appartient pas au lieu!"<<endl;
    return coupes;
  }

  double scal = 0;
  Vector3d U,V,CP,X,YY;
  Vector2d Y;
  VectorXd SOL;
  CP = p - centre;
  scal = direction.transpose()*rotation;
  U = direction.cross(rotation);
  V = CP.cross(rotation);
  
  /* si le vecteur normal au plan est parrallele avec l' axe de rotation
     le lieu geometrique est un seul et meme plan
  */

  if(isEqual(U.norm(),0,CRAYON)){
    V = centre + distance * direction;
    Plan PP;
    PP.setCenter(V);
    PP.setDirection(direction);
    *n = 1;
    coupes.push_back(PP);
    //cout<<"Le plan tournant est equivalent a un seul plan!"<<endl;
    return coupes;
  }

  /* recherche de la direction X du plan qui contiendrait le point p
     avec pour contrainte si 
     X le vecteur unitaire normal au plan
     R le vecteur autour duquel se fait la rotation

     equations
     X . p = distance equation du plan
     X . R = constante le vecteur R tourne autour du plan
     X . X = 1 vecteur unitaire

     X est combinaison lineaire de P, de R et de P^R

     En premier lieu a partir des deux premieres equations
     X = a.P + b.R + t. (P^R) t est quelconque c'est l'equation d'une droite
     X = U + t . (P^R) avec U = a.P + b.R 
     En second lieu cette droite rencontre la sphere unite pour la troisieme equation
     on a :
     0 solution  |U| > 1 le point n'appartient pas au lieu
     1 solution  |U| = 1 le point est a la limite
     2 solutions |U| < 1 le point est dans le lieu

     Cas particulier P || R

     Solution seulement si |P| . constant = distance

  */

  if(isEqual(V.norm(),0,CRAYON)){
    if(isEqual(distance,CP.norm()*scal,CRAYON)){
      V = centre + distance * direction;
      Plan PP;
      PP.setCenter(V);
      PP.setDirection(direction);
      *n = 1000;
      coupes.push_back(PP);
      //cout<<"Le point est au centre d'un cone, infinite de solution "<<endl;  
      return coupes;
    }
    else{
      //cout<<"Normallement ce message ne devrait pas s'afficher"<<endl;
      *n = 0;
      return coupes;
    }
  }

  MatrixXd A1 = MatrixXd::Zero(2,3);
  A1.row(0)= CP.transpose();
  A1.row(1) = rotation.transpose();
  MatrixXd A2 = MatrixXd::Zero(3,2);
  A2.col(0) = CP;
  A2.col(1) = rotation;
  MatrixXd A3(3,3);
  A3.noalias() = A1*A2;
  Y << distance, scal;
  LeastSquares::solve(Y, A3 ,SOL);

  U = A2*SOL;
  V.normalize();

  double para[3];
  para[0] = U.transpose()*U-1;
  para[1] = 2*V.transpose()*U;
  para[2] = V.transpose()*V;

  polynome poly(3,para);

  list<double> racines = polyfacteur::extraire2racines(poly);

  list<double>::const_iterator it;

  *n = 0;

  for(it = racines.begin();it!=racines.end();it++){
    double t = (*it);
    X = t*V +U;
    X.normalize();
    YY = centre + distance * X;
    Plan PP;
    PP.setCenter(YY);
    PP.setDirection(X);
    coupes.push_back(PP);
    (*n)++;
  }

  return coupes;

}

