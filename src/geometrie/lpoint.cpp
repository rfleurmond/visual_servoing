#include "lpoint.h"

using namespace Eigen;
using namespace std;


list<Vector3d> LPoint::getVectors() const{
  list<Vector3d> liste;
  return liste;
}

void LPoint::setVectors(const list<Vector3d> & l){

}
   
int LPoint::getDimensions() const{
  return 0;
}
  
int LPoint::getDimEspace() const{
  return 0;
}

bool LPoint::isSurface() const{
  return false;
}

bool LPoint::isVolumic() const{
  return false;
}

bool LPoint::hasPoint(Vector3d p) const{
  Vector3d CP = p - centre;
  return isEqual(CP.norm(),0,CRAYON);
}

Vector3d LPoint::getNearestPoint(Vector3d P) const{
  return centre;
}

void LPoint::affiche(ostream &flux) const{
  flux<<"Point:\n"<<centre<<endl;
}

 
