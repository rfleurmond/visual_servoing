#include "tore.h"

using namespace Eigen;
using namespace std;

void Tore::calculate(){

  Matrix3d A = Matrix3d::Zero();
  A.col(0) = moyeu;
  A.col(1) = rotation;
  A.col(2) = moyeu.cross(rotation);
  VectorXd para = Vector3d::Zero();
  LeastSquares::solve(direction,A,para);
  a = para(0);
  b = para(1);
  c = para(2);
}

Tore::Tore(){
    etat = EXISTS;
    type = TORE;
    direction <<0,-1,0;
    rotation  <<0,0,1;
    moyeu << 1,0,0;
    distance = 1;
    centre << 0,0,0;
    rayon = 0.1;
    calculate();
}

Vector3d Tore::getMoyeu(Eigen::Vector3d d) const{
  d.normalize();
  d = d - b * rotation;
  Matrix3d AA = Matrix3d::Zero();
  AA = a*Matrix3d::Identity() -c*VectRotation::getSkewMatrix(rotation);
  VectorXd mm;
  LeastSquares::solvePseudoInverse(d,AA,mm);
  return mm.block<3,1>(0,0);
}

Vector3d Tore::getDirection(Eigen::Vector3d m) const{
  Vector3d mr = m.cross(rotation);
  Vector3d dd = a *m + b*rotation + c*mr;
  dd.normalize();
  return dd;
}
  

list<Vector3d> Tore::getVectors() const{
  list<Vector3d> liste;
  liste.push_back(direction);
  liste.push_back(rotation);
  liste.push_back(moyeu);
  return liste;
}

void Tore::setVectors(const list<Vector3d> & l){
  list<Vector3d>::const_iterator it;
  int i =0;
  for(it = l.begin(); it!=l.end(); it++){
    if(i==0){
      direction = (*it);
    }
    if(i==1){
      setRotation((*it));
    }
    if(i==2){
      moyeu = (*it);
    }
  
    i++;
  }
}

   
int Tore::getDimensions() const{
  return 2;
}
  
int Tore::getDimEspace() const{
  return 3;
}

bool Tore::isSurface() const{
  return true;
}

bool Tore::isVolumic() const{
  return false;
}

bool Tore::hasPoint(Vector3d p) const{
  PlanTournant pt = getPlanTournant();
  if(pt.hasPoint(p)==false){
    return false;
  }

  int n = -1;
  list<Plan> liste = pt.getSuitablePlans(p,&n);
  list<Plan>::const_iterator it;
  for(it=liste.begin();it!=liste.end();it++){
    Plan pp = (*it);
    Vector3d dd = pp.getDirection();
    Vector3d milieu = centre + distance * getMoyeu(dd);
    Cercle cc;
    cc.setRayon(rayon);
    cc.setCenter(milieu);
    cc.setDirection(dd);
    if(cc.hasPoint(p)){
      return true;
    }
  }
  return false;
}

/**
   Give a no-accuracy result (non-optimal)
*/ 
Vector3d Tore::getNearestPoint(Vector3d P) const{
  if(hasPoint(P)){
    return P;
  }
  Vector3d CP = P - centre;
  double scal = CP.transpose()*rotation;
  Vector3d U =  CP - scal *rotation;
  if(isEqual(U.norm(),0,CRAYON)){
    U = moyeu;
  }
  else{
    U.normalize();
  }

  Vector3d milieu = centre + distance * U;
  Cercle cc;
  cc.setRayon(rayon);
  cc.setCenter(milieu);
  cc.setDirection(getDirection(U));
  return cc.getNearestPoint(P);

}

Vector3d Tore::getDirection() const{
  return direction;
}

void Tore::affiche(ostream &flux) const{
  Cercle CC;
  CC.setCenter(centre + moyeu * distance);
  CC.setDirection(direction);
  CC.setRayon(rayon);
  flux<<"La tore est un cercle comme celui ci:\n"<<CC;
  flux<<"qui tourne de l'axe de rotation passant par le point\n"<<centre<<endl;
  flux<<"et de direction\n"<<rotation<<endl; 
}


double Tore::getDistance() const{
  return distance;
}

void Tore::setDistance(double r){
  distance = r;
}

Vector3d Tore::getRotation() const{
  return rotation;
}

void Tore::setRotation(Vector3d v){
  rotation = v.normalized();
  calculate();
}

void Tore::setCercle(Cercle cc){
  Vector3d d = cc.getCenter() - centre;
  direction = cc.getDirection();
  double scal = d.transpose()*rotation;
  centre = centre +scal*rotation;
  d = cc.getCenter() - centre;
  distance = d.norm();
  rayon = cc.getRayon();
  moyeu = d.normalized();
  calculate();
}

Droite Tore::getAxeRotation() const{
  Droite d;
  d.setCenter(centre);
  d.setDirection(rotation);
  return d;
}

Cercle Tore::getCircleCenter() const{
  Cercle cc;
  cc.setCenter(centre);
  cc.setDirection(rotation);
  cc.setRayon(distance);
  return cc;
}

PlanTournant Tore::getPlanTournant() const{
  Vector3d  point = centre + distance * moyeu;
  Plan pp;
  pp.setDirection(direction);
  pp.setCenter(point);
  PlanTournant pt;
  pt.setRotation(rotation);
  pt.setCenter(centre);
  pt.setPlan(pp);
  return pt;
}

 
list<Cercle> Tore::getSuitableCircles(Vector3d p, int * n){
  list<Cercle> sortie;
  if(hasPoint(p)==false){
    *n = 0;
    return sortie;
  }
  PlanTournant pt = getPlanTournant();
  list<Plan> liste = pt.getSuitablePlans(p,n);
  list<Plan>::const_iterator it;
  for(it=liste.begin();it!=liste.end();it++){
    Plan pp = (*it);
    Vector3d dd = pp.getDirection();
    Vector3d milieu = centre + distance * getMoyeu(dd);
    Cercle cc;
    cc.setRayon(rayon);
    cc.setCenter(milieu);
    cc.setDirection(dd);
    if(cc.hasPoint(p)){
      sortie.push_back(cc);
    }
  }

  return sortie;

}
