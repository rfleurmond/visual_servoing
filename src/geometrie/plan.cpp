#include "plan.h"

using namespace Eigen;
using namespace std;


list<Vector3d> Plan::getVectors() const{
  list<Vector3d> liste;
  liste.push_back(direction);
  return liste;
}

void Plan::setVectors(const list<Vector3d> & l){
  list<Vector3d>::const_iterator it;
  if(l.size()>0){
    it = l.begin();
    setDirection((*it));
  }
}
   
int Plan::getDimensions() const{
  return 2;
}
  
int Plan::getDimEspace() const{
  return 2;
}

bool Plan::isSurface() const{
  return true;
}

bool Plan::isVolumic() const{
  return false;
}

bool Plan::hasPoint(Vector3d p) const{
  Vector3d CP = p - centre;
  double val = CP.transpose()*direction; 
  if(isEqual(val,0,CRAYON))
    return true;
  else
    return false;
}

Vector3d Plan::getNearestPoint(Vector3d P) const{

  Vector3d CP = P - centre;
  double val = CP.transpose()*direction;
  if(isEqual(val,0,CRAYON)){
    return P;
  }
  Vector3d M = CP - val*direction + centre;
  return M;
}

void Plan::setDirection(Vector3d d){
  direction = d.normalized();
}
  
Vector3d Plan::getDirection() const{
  return direction;
}

void Plan::affiche(ostream &flux) const{
  MatrixXd mat = MatrixXd::Identity(1,3);
  mat = direction.transpose();
  Equation eq;
  VectorXd val = VectorXd::Zero(1);
  val(0) = centre.transpose()*direction;
  eq.setValeurs(mat,val);
  flux<<"Equations du plan:\n"<<eq;
}

bool Plan::contains(Droite d) const{
  double scalaire = direction.transpose()*d.getDirection();
  if(isEqual(scalaire,0,CRAYON)){
    return hasPoint(d.getCenter());
  }
  return false;
}

bool Plan::isSameAs(Plan & other){
  double scal = getDirection().transpose()*other.getDirection();
  return ((isEqual(scal,1,CRAYON) || isEqual(scal,-1,CRAYON))  && other.hasPoint(centre));
}
