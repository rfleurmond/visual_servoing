#include "cone.h"

using namespace Eigen;
using namespace std;

list<Vector3d> Cone::getVectors() const{
  list<Vector3d> liste;
  liste.push_back(rotation);
  return liste;
}

void Cone::setVectors(const list<Vector3d> & l){
  list<Vector3d>::const_iterator it;
  int i =0;
  for(it = l.begin(); it!=l.end(); it++){
    if(i==0)
      setRotation((*it));
    i++;
  }
}
   
int Cone::getDimensions() const{
  return 2;
}
  
int Cone::getDimEspace() const{
  return 3;
}

bool Cone::isSurface() const{
  return true;
}

bool Cone::isVolumic() const{
  return false;
}

bool Cone::hasPoint(Vector3d p) const{
  double sina = fabs(sin(angle));
  Vector3d V,CP;
  CP = p - centre;
  V = CP.cross(rotation);
  return isEqual(V.norm(),CP.norm()*sina,CRAYON);  
}

Vector3d Cone::getNearestPoint(Vector3d P) const{

  if(hasPoint(P)){
    return P;
  }

  Vector3d U,V,CP,D;
  
  CP = P - centre;

  double scal = CP.transpose()*rotation; 

  V = CP - scal*rotation;
  
  if(isEqual(V.norm(),0,CRAYON)){
    U << rotation(1),rotation(2),rotation(0);
    V = U.cross(rotation); 
  }

  V.normalize();

  double sina = fabs(sin(angle));
  double cosa = fabs(cos(angle));
  D = cosa * rotation + sina * V;

  Droite d1;
  d1.setCenter(centre);
  d1.setDirection(D);

  return d1.getNearestPoint(P);

}

void Cone::affiche(ostream &flux) const{
  MatrixXd mat = MatrixXd::Identity(1,4);
  mat << rotation.transpose(),-1;
  Equation eq;
  VectorXd val = VectorXd::Zero(1);
  val(0) = centre.transpose()*rotation;
  eq.setValeurs(mat,val);
  flux<<"Cone defini par l'intersection du plan glissant:\n"<<eq;
  flux<<"Et de la sphere variable:"<<endl;
  double x,y,z,xx,yy,zz;
  x = centre(0);
  y = centre(1);
  z = centre(2);
  xx = rotation(0);
  yy = rotation(1);
  zz = rotation(2);
  
  flux <<"(x - ("<<xx<<"t";
  if(x<=0)
    flux<<" - "<<-x<<"))^2 + ";
  else
    flux<<" + "<<x<<"))^2 + ";
  flux <<"(y - ("<<yy<<"t";
  if(y<=0)
    flux<<" - "<<-y<<"))^2 + ";
  else
    flux<<" + "<<y<<"))^2 + ";
  flux <<"(z - ("<<zz<<"t";
  if(z<=0)
    flux<<" - "<<-z<<"))^2 ";
  else
    flux<<" + "<<z<<"))^2 ";
  flux <<" = "<<tan(angle)<<"t2"<<endl;
}

double Cone::getAngle() const{
  return angle;
}

void Cone::setAngle(double r){
  angle = r;
}

Vector3d Cone::getRotation() const{
  return rotation;
}

void Cone::setRotation(Vector3d v){
  rotation = v.normalized();

}

Droite Cone::getAxeRotation(){
  Droite d;
  d.setCenter(centre);
  d.setDirection(rotation);
  return d;
}

 
