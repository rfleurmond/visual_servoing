#include "droite-tournante.h"

using namespace Eigen;
using namespace std;

void DroiteTournante::calculate(){

  Matrix3d A = Matrix3d::Zero();
  A.col(0) = moyeu;
  A.col(1) = rotation;
  A.col(2) = moyeu.cross(rotation);
  VectorXd para = Vector3d::Zero();
  LeastSquares::solve(direction,A,para);
  a = para(0);
  b = para(1);
  c = para(2);
}

DroiteTournante::DroiteTournante(){
  etat = EXISTS;
  type = DROITETOURNANTE;
  rotation  << 0,0,1;
  moyeu <<1,0,0;
  direction << 0, 0.8 ,0.6;
  centre << 0,0,0;
  calculate();
    
}


Vector3d DroiteTournante::getMoyeu(Eigen::Vector3d d) const{
  d.normalize();
  d = d - b * rotation;
  Matrix3d AA = Matrix3d::Zero();
  AA  =  a * Matrix3d::Identity() - c * VectRotation::getSkewMatrix(rotation);
  VectorXd mm;
  LeastSquares::solvePseudoInverse(d,AA,mm);
  return mm.block<3,1>(0,0);
}

Vector3d DroiteTournante::getDirection(Eigen::Vector3d m) const{
  Vector3d mr = m.cross(rotation);
  Vector3d dd = a *m + b*rotation + c*mr;
  dd.normalize();
  return dd;
}
  

list<Vector3d> DroiteTournante::getVectors() const{
  list<Vector3d> liste;
  liste.push_back(direction);
  liste.push_back(rotation);
  liste.push_back(moyeu);
  return liste;
}

void DroiteTournante::setVectors(const list<Vector3d> & l){
  list<Vector3d>::const_iterator it;
  int i =0;
  for(it = l.begin(); it!=l.end(); it++){
    if(i==0){
      direction = (*it);
    }
    if(i==1){
      setRotation((*it));
    }
    if(i==2){
      moyeu = (*it);
    }
  
    i++;
  }
}

   
int DroiteTournante::getDimensions() const{
  return 2;
}
  
int DroiteTournante::getDimEspace() const{
  return 3;
}

bool DroiteTournante::isSurface() const{
  return true;
}

bool DroiteTournante::isVolumic() const{
  return false;
}

bool DroiteTournante::hasPoint(Vector3d p) const{
  
  Vector3d CP = p - centre;
  double scal = CP.transpose()*rotation;

  double rayon1 = moyeu.norm();
  Cercle cc1;
  cc1.setCenter(centre);
  cc1.setRayon(rayon1);
  cc1.setDirection(rotation);

  if(isEqual(scal,0,CRAYON)){
    return cc1.hasPoint(p);
  }
    
  double cosa = rotation.transpose()*direction;
  double sina = sqrt(1 - cosa*cosa);
  Vector3d centre2 = centre + CP - rotation * scal;
  double rayon2 = scal * sina;
  Cercle cc2;
  cc2.setCenter(centre2);
  cc2.setRayon(rayon2);
  cc2.setDirection(rotation);
  
  list<LPoint> candidature = Architect::intersection(cc1,cc2);
  list<LPoint>::const_iterator it;

  for(it = candidature.begin();it!=candidature.end();it++){
    LPoint lp = (*it);
    Vector3d point = lp.getCenter();
    Vector3d m = point - centre;
    Vector3d d1 = getDirection(m);
    Vector3d d2 = p - point;
    
    d2.normalize();
    Vector3d test = d1.cross(d2);
    if(isEqual(test.norm(),0,CRAYON)){
      return true;
    }
  }
  
  return false;
  
}

list<Droite> DroiteTournante::getSuitableDroites(Eigen::Vector3d P) const{
  
  list<Droite> liste;

  Vector3d CP = P - centre;
  double scal = CP.transpose()*rotation;

  double rayon1 = moyeu.norm();
  Cercle cc1;
  cc1.setCenter(centre);
  cc1.setRayon(rayon1);
  cc1.setDirection(rotation);

  if(isEqual(scal,0,CRAYON)){
    if(cc1.hasPoint(P)){
      Vector3d d1 = getDirection(CP);
      Droite dd(P,d1);
      liste.push_back(dd);
    }
    return liste;
  }
  
  double cosa = rotation.transpose()*direction;
  double sina = sqrt(1 - cosa*cosa);
  Vector3d centre2 = centre + CP - rotation * scal;
  double rayon2 = scal * sina;
  Cercle cc2;
  cc2.setCenter(centre2);
  cc2.setRayon(rayon2);
  cc2.setDirection(rotation);
  
  list<LPoint> candidature = Architect::intersection(cc1,cc2);
  list<LPoint>::const_iterator it;

  for(it = candidature.begin();it!=candidature.end();it++){
    
    LPoint lp = (*it);
    Vector3d point = lp.getCenter();
    Vector3d m = point - centre;
    Vector3d d1 = getDirection(m);
    Vector3d d2 = P - point;
    d2.normalize();
    Vector3d tt = d1.cross(d2);
    if(isEqual(tt.norm(),0,CRAYON)){
      Droite dd(point,d1);
      liste.push_back(dd);
    }
  }
  
  return liste;
}


/**
   Give a no-accuracy result (non-optimal)
*/ 
Vector3d DroiteTournante::getNearestPoint(Vector3d P) const{
  if(hasPoint(P)){
    return P;
  }
  Vector3d CP = P - centre;
  double scal = CP.transpose()*rotation;
  Vector3d U =  CP - scal *rotation;
  
  if(isEqual(U.norm(),0,CRAYON)){
    Droite dd(centre + moyeu, direction);
    return dd.getNearestPoint(P);
  }

  double scal2 = direction.transpose()* rotation;

  Vector3d MM = moyeu.norm() * U.normalized();

  Vector3d dir = getDirection(MM);

  if(isEqual(scal2,0,1e+2*CRAYON)==false){
     
    Vector3d v = moyeu + scal/scal2 *direction;

    double cosa = v.transpose()*rotation;

    v = v - cosa * rotation;

    double sina = v.norm();

    MM = sina *U.normalized();
  }

  Droite dd(centre +MM,dir);

  return dd.getNearestPoint(P);


}

Vector3d DroiteTournante::getDirection() const{
  return direction;
}

void DroiteTournante::affiche(ostream &flux) const{
  Droite dd(centre + moyeu, direction);
  flux<<"La droite :"<<dd;
  flux<<"qui tourne de l'axe de rotation passant par le point\n"<<centre<<endl;
  flux<<"et de direction\n"<<rotation<<endl; 
}


Vector3d DroiteTournante::getRotation() const{
  return rotation;
}

void DroiteTournante::setRotation(Vector3d v){
  rotation = v.normalized();
  calculate();
}

void DroiteTournante::setDroite(Droite d){
  moyeu = d.getCenter()- centre;
  direction = d.getDirection();
  calculate();
}

Droite DroiteTournante::getAxeRotation() const{
  Droite d;
  d.setCenter(centre);
  d.setDirection(rotation);
  return d;
}

Cercle DroiteTournante::getCircleCenter() const{
  Cercle cc;
  cc.setCenter(centre);
  cc.setDirection(rotation);
  cc.setRayon(moyeu.norm());
  return cc;
}

 
