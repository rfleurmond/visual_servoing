#include "track-points.h"
#include <cmath>
#include <algorithm> 

using namespace Eigen;
using namespace std;

TrackPoints::TrackPoints(int n):
  RealTracker(),
  N(n),
  liste(vector<cv::Point2f>())
{
  if( N<=1 ) N = 1;
  limiteClicks = N;

  success = true;
}
 
TrackPoints::~TrackPoints(){
}

void TrackPoints::answerOnClick(int x, int y){
  if(nombreClicks <= N){
    cv::Point2f epingle(x,y);
    liste.push_back(epingle);
  }
}

void TrackPoints::resetClicks(){
  hasStarted = false;
  nombreClicks = 0;
  liste.clear();
}

void TrackPoints::initTracking(CVEcran * d){
  display = d;
  assert(display!=NULL);
  // Mouse event to select the tracked color on the original image
  cvSetMouseCallback(display->getNom().c_str(), RealTracker::mouseCallBack, this);
  cv::Mat gray;
  cvtColor( d->getImage(), gray, cv::COLOR_RGB2GRAY );
  memory = gray;
}


void TrackPoints::track(const cv::Mat & img){
  cv::Mat gray;
  cvtColor( img, gray, cv::COLOR_RGB2GRAY );
  stepTracking(gray);
  memory = gray;
}  

void TrackPoints::stepTracking(const cv::Mat & img){

  if(liste.size()!=N)
    return;

  assert(img.channels()==1);


  std::vector<cv::Point2f> anciens = liste;
  std::vector<uchar> status(4);
  std::vector<float> err(4);

  cv::calcOpticalFlowPyrLK(memory, img,
			   anciens,liste,
			   status,
			   err
			   );

  success = true;

  for(int i = 0; i < status.size();i++){
    success = success && (status[i]==1);
    if(status[i] != 1){
      break;
    }
  }

}

void TrackPoints::findFeatures(const Eigen::VectorXd & feat){
  assert(feat.rows() == 2 * N);
  liste.clear();
  for(unsigned int i = 0; i < N; i++){
    cv::Point2f p;
    p.x =  static_cast<float>(round(feat(2*i)));
    p.y =  static_cast<float>(round(feat(2*i+1)));
    liste.push_back(p);
  }
}

int TrackPoints::getDimensionOutput() const{
  return 2*N;
}

bool TrackPoints::itCanBeSeen() const{
  return true;
}

bool TrackPoints::isLost() const{
  return !success;
}

VectorXd TrackPoints::getFunction() const{
  VectorXd F = VectorXd::Zero(2*N);
  if(liste.size()>=N){
    for(unsigned int i = 0; i < N; i++){
      F.block<2,1>(2*i,0) << liste.at(i).x, liste.at(i).y;
    }
  }
  return F;
}  
 

void TrackPoints::draw(CVEcran * const vue, cv::Scalar couleur) const{
  drawFeat(getFunction(),vue,couleur);
}

void TrackPoints::drawFeat(const VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const{
  MultiPointFeature mpf(MatrixXd::Identity(3,3));
  mpf.drawFeat(value,vue,couleur);
}
