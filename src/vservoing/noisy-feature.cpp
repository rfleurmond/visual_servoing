#include  "noisy-feature.h"

using namespace Eigen;
using namespace std;

NoisyFeature::NoisyFeature(VisualFeature * f, MatrixXd cov):
  VisualFeature(),
  feat(f),
  dimension(0),
  roulette(NULL),
  bruit(NULL),
  heure(NULL)
{
  assert(f != NULL);
  dimension = f->getDimensionOutput();
  feat = f;
  assert(cov.rows() == cov.cols());
  assert(cov.rows() == dimension);
  VectorXd mean = VectorXd::Zero(dimension);
  roulette = new MultivariateGaussian(mean,cov); 
  strategy = VisualFeature::PINV;
  oeil = feat->getEye();
  frame = feat->getSituation();
  
  bruit = new VectorXd();
  *bruit = VectorXd::Zero(dimension);
  heure = new double();
  *heure = -0.05;
}

NoisyFeature::~NoisyFeature(){
  delete roulette;
  delete bruit;
  delete heure;
}

void NoisyFeature::setCovariance(MatrixXd cov){
  VectorXd mean = VectorXd::Zero(dimension);
  delete roulette;
  roulette = new MultivariateGaussian(mean,cov); 
}
  
int NoisyFeature::getDimensionState() const{
  return feat->getDimensionState();
}  

VectorXd NoisyFeature::getClearFunction() const{
  return feat->getFunction();
}  

VectorXd NoisyFeature::getFunction(const VectorXd & P) const{
  return feat->getFunction(P);
}  


VectorXd NoisyFeature::getFunction() const{
  VectorXd F = feat->getFunction();
  VectorXd B = *bruit;
  if(*heure<MyClock::getTime()){
    *heure = MyClock::getTime();
    roulette->sample(B);
    *bruit = B;
  }
  return F + B;
}  


bool NoisyFeature::itCanBeSeen() const{
  return feat->itCanBeSeen();
}

int NoisyFeature::getDimensionOutput() const{
  return dimension;
}


void NoisyFeature::setSituation(const Repere & r){
  frame = r;
  feat->setSituation(r);
}

void NoisyFeature::attachToCamera(Camera * eye){
  oeil = eye;
  feat->attachToCamera(eye);
}


MatrixXd NoisyFeature::getNaiveInteractionMatrix(const VectorXd & F) const{
  return feat->getNaiveInteractionMatrix(F);
}

MatrixXd NoisyFeature::getInteractionMatrix(const VectorXd & P) const{
  return feat->getInteractionMatrix(P);
}

MatrixXd NoisyFeature::getInteractionMatrix() const{
  return feat->getInteractionMatrix();
}

VectorXd NoisyFeature::getError(const VectorXd & S, const VectorXd & autre) const{
  VectorXd erreur = feat->getError(S,autre);
  return erreur;
}


MatrixXd NoisyFeature::getJacobianError(const VectorXd & S, const VectorXd & autre) const{
  return feat->getJacobianError(S,autre);
}


MatrixXd NoisyFeature::getJacobianReferenceError(const VectorXd & S, const VectorXd & autre) const{
  return feat->getJacobianReferenceError(S,autre);
}


void NoisyFeature::draw(CVEcran * const vue, cv::Scalar couleur) const{
  //feat->draw(vue,couleur);
  feat->drawFeat(getFunction(),vue,couleur);
}


void NoisyFeature::drawFeat(const VectorXd & value,CVEcran * const vue, cv::Scalar couleur) const{
  feat->drawFeat(value,vue,couleur);
}
