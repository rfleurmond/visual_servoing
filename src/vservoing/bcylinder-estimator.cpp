#include "bcylinder-estimator.h"

using namespace Eigen;
using namespace std;

BCylinderEstimator::BCylinderEstimator(VisualFeature * model, VTracker * tracker):
  FeatureEstimator(model,tracker),
  f_i(1),
  g_i(1),
  h_i(1),
  j_i(1),
  n(0),
  omega_t(0),
  omega_b(0),
  phi(MatrixXd::Zero(3,3)),
  psi_b(MatrixXd::Zero(3,3)),
  psi_t(MatrixXd::Zero(3,3)),
  beta(VectorXd::Zero(3)),
  lambda_b(VectorXd::Zero(3)),
  lambda_t(VectorXd::Zero(3)),
  gamma(VectorXd::Zero(3)),
  delta_b(VectorXd::Zero(3)),
  delta_t(VectorXd::Zero(3)),
  mu(0),
  epsilon(0),
  radius(0.25),
  length(1.0),
  knowRadius(false),
  knowLength(false)
{
  assert(model!=NULL);
  assert(tracker!=NULL);

  estimation = VectorXd::Zero(7);
  estimation(0) = radius;
  estimation(6) = length;
  frame = modele->getSituation();
  oeil = tracker->getEye();

}

void BCylinderEstimator::setRadius(double r){
  if(r>0){
    radius = r;
    knowRadius = true;
  }
}

void BCylinderEstimator::setLength(double l){
  if(l>0){
    length = l;
    knowLength = true;
  }
}


Vector3d BCylinderEstimator::getNormal(double rho, double theta) const{
  
  double cosa = cos(theta);
  double sina = sin(theta);

  double x,y;

  Vector2d centre = oeil->getIntrinsicParameters().getCenter();

  x = rho * cosa;
  y = rho * sina;
  
  Vector2d imgi(x,y);
  imgi += centre;

  Vector3d U1 = frame.getVectorInME(oeil->getCleanDirection(imgi));

  double k = 0.25 * oeil->getIntrinsicParameters().alphaU;

  x = rho * cosa - k * sina;
  y = rho * sina + k * cosa;
  
  imgi << x, y;
  imgi += centre;

  Vector3d U2 = frame.getVectorInME(oeil->getCleanDirection(imgi));

  Vector3d N = U1.cross(U2).normalized();

  return N;
  
}

void BCylinderEstimator::selectInformation(int a, int b, int c, int d){
  assert(a == 0 || a == 1);
  assert(b == 0 || b == 1);
  assert(c == 0 || c == 1);
  assert(d == 0 || d == 1);

  f_i = a;
  g_i = b;
  h_i = c;
  j_i = d;
}

void BCylinderEstimator::computeEstimation(){

  Vector3d CI = frame.getPointInME(oeil->getRepere().getDeplacement());

  VectorXd S = traceur->getFunction();

  if(S.rows()!=8)
    return ;

  /// @see BoundedCylinder::getFunction()\n

  /// @see ICylinderEstimator::computeEstimation()\n

  /** 
      F is the vector of visual features cues describing the cylinder
   
      \f[ F \rightarrow {\begin{bmatrix} 
      \rho_r & \theta_r  & k_{rt} & k_{rb} &
      \rho_l & \theta_l & k_{lt} & k_{lb}\end{bmatrix}}^T \f]
  */

  /// Tranform the last measurements and update matrices, vectors and scalars

  /**
     \f$C_i \f$ is the current position of the optical center of the camera in target frame
     
     we make the transformation
     
     \f$ [\rho_r, \ \theta_r]  \rightarrow \f$ getNormal() \f$ \rightarrow N \f$ \n
     \f$ [\rho_r, \ \theta_r,\ k_{rb}]  \rightarrow F_i \f$ \n
     \f$ [\rho_r, \ \theta_r,\ k_{rt}]  \rightarrow G_i \f$ \n
     \f$ [\rho_l, \ \theta_l]  \rightarrow \f$ getNormal() \f$ \rightarrow Q \f$ \n
     \f$ [\rho_l, \ \theta_l,\ k_{lb}]  \rightarrow H_i \f$ \n
     \f$ [\rho_l, \ \theta_l,\ k_{lt}]  \rightarrow I_i \f$ 
     
  */



  int a_i = -1;
  int b_i = 1;

  Vector3d FI,GI,HI,II;

  MatrixXd DELTA,THETA,CHI,OMEGA;
  
  double cosa,sina,x,y;
  Vector2d imgi, centre;
  IntrinsicCam optique = oeil->getIntrinsicParameters();
  centre = optique.getCenter();
  

  double rho = S(0);
  double theta = S(1);
  double k_b = S(2);
  double k_t = S(3);

  cosa = cos(theta);
  sina = sin(theta);
  
  x = rho * cosa - k_b * sina;
  y = rho * sina + k_b * cosa;

  imgi << x,y;
  imgi += centre;
  
  FI = frame.getVectorInME(oeil->getCleanDirection(imgi));

  x = rho * cosa - k_t * sina;
  y = rho * sina + k_t * cosa;

  imgi << x,y;
  imgi += centre;
  
  GI = frame.getVectorInME(oeil->getCleanDirection(imgi));


  Vector3d NI = getNormal(rho,theta);


  rho = S(4);
  theta = S(5);
  k_b = S(6);
  k_t = S(7);
  cosa = cos(theta);
  sina = sin(theta);

  x = rho * cosa - k_b * sina;
  y = rho * sina + k_b * cosa;

  imgi << x,y;
  imgi += centre;
  
  HI = frame.getVectorInME(oeil->getCleanDirection(imgi));

  x = rho * cosa - k_t * sina;
  y = rho * sina + k_t * cosa;

  imgi << x,y;
  imgi += centre;
  
  II = frame.getVectorInME(oeil->getCleanDirection(imgi));
  
  Vector3d QI = getNormal(rho,theta);

    /**    
     We set  \f$ a_i = -1 \f$ and \f$ b_i = 1 \f$ 
     

     \f{eqnarray*}
        \Pi_i      & = & N \cdot N^T + Q \cdot Q^T\\
	\Delta_i   & = & I_3 - F_i F^T_i\\  
	\Theta_i   & = & I_3 - G_i G^T_i\\  
	\chi_i     & = & I_3 - H_i H^T_i\\  
	\Omega_i   & = & I_3 - I_i I^T_i\\
	\gamma_i   & = & a_i N_i + b_i Q_i \\
     \f}

     \f{eqnarray*}
        \Pi_n      & = & \Pi_n + \Pi_i\\
	\Psi_b     & = & \Psi_b + f_i \Delta_i + g_i \chi_i \\
	\Psi_t     & = & \Psi_t + h_i \Theta_i + j_i \Omega_i\\
	\beta      & = & \beta + \phi_i \cdot C_i\\
	\lambda_b  & = & \lambda_b + [f_i \Delta_i + g_i \chi_i] C_i\\
	\lambda_t  & = & \lambda_t + {[ h_i \Theta_i + j_i \Omega_i]}C_i\\
	\gamma     & = & \gamma + \gamma_i\\
	\delta_b   & = & \delta_b + {[f_i a_i \Delta_i N_i + g_i b_i \chi_i Q_i ]} \\
	\delta_t   & = & \delta_t + {[h_i a_i \Theta_i N_i + j_i b_i \Omega_i Q_i ]}\\
	\mu        & = & \mu + \gamma^T_i \cdot C_i \\
	\omega_b   & = & \omega_b + {[f_i + g_i ]}\\
	\omega_t   & = & \omega_t + {[h_i + j_i]} \\
	n          & = & n + 1
     \f}

   */


  MatrixXd AB = NI * NI.transpose();

  AB.noalias() +=  QI * QI.transpose();

  MatrixXd I3 = MatrixXd::Identity(3,3);

  DELTA = I3;
  DELTA.noalias() -= FI * FI.transpose();
  THETA = I3;
  THETA.noalias() -= GI * GI.transpose();
  CHI   = I3;
  CHI.noalias() -= HI * HI.transpose();
  OMEGA = I3;
  OMEGA.noalias() -= II * II.transpose();
  
  Vector3d betaI = AB * CI;

  Vector3d gammaI = a_i * NI + b_i * QI;

  double muI = CI.dot(gammaI);

  double epsilonI = CI.dot(a_i * (f_i * DELTA + h_i * THETA) * NI + b_i * (g_i * CHI + j_i * OMEGA) * QI );

  phi += AB;

  psi_b.noalias() += f_i * DELTA + g_i * CHI;

  psi_t.noalias() += h_i * THETA + j_i * OMEGA;

  beta += betaI;

  lambda_b.noalias() += (f_i * DELTA + g_i * CHI) * CI;

  lambda_t.noalias() += (h_i * THETA + j_i * OMEGA) * CI;

  gamma += gammaI;

  delta_b.noalias() += f_i * a_i * DELTA * NI + g_i * b_i * CHI * QI;

  delta_t.noalias() += h_i * a_i * THETA * NI + j_i * b_i * OMEGA * QI;

  mu += muI;

  epsilon += epsilonI;

  n += 1;

  omega_b += f_i + g_i;

  omega_t += h_i + j_i;


  /** Compute the estimation
      
      \f$ U \f$ is the eigenvector corresponding to the smallest eigenvalue in \f$ \Pi_n \f$

      if the radius \f$r\f$ is known 
      
      \f$ P = \Pi_n^+ (\beta + r \cdot \gamma) \f$

      otherwise

      \f$ P = {(4 \Pi_n - \frac{2}{n} \gamma \gamma^T)}^+ (4 \beta + \frac{1}{n} \gamma \gamma^T \cdot U - \frac{2}{n} \mu \cdot \gamma) \f$

      \f$ r = \frac{1}{4n} (\gamma^T (2 P +U ) - 2 \mu )\f$

      \f{eqnarray*}
      	k     & = & {[\Psi_b U]}^{+}( r \delta_b +  \lambda_b - \Psi_b P)\\
	k+l   & = & {[\Psi_t U]}^{+}( r \delta_t +  \lambda_t - \Psi_t P)\\
	B     & = & P + kU\\
	T     & = & B + (k+l) U
      \f}

   */

 
  VectorXd P, U, B, T;

  double r = radius;

  LineEstimator::resoudre(phi, beta + r * gamma , P, U);

  MatrixXd PROJ = MatrixXd::Identity(3,3) - U * U.transpose(); 

  double gammaU = 1;

  if(knowRadius==false){
    
    double in = 1.0/n;

    MatrixXd TETA = 4 * phi - 2 * in * gamma * gamma.transpose();

    VectorXd Y = 4 * beta  + gammaU * in * gamma * gamma.transpose() * U  - 2 * in * mu * gamma ;

    P = pinvSolve(TETA,Y);

    P = PROJ * P;

    //LineEstimator::resoudre(TETA, Y, P, U);

    r = 0.25 * in * ( gamma.dot(2 * P + gammaU * U) - 2 * mu );

    if( r< 0) r = - r;

    if(isEqual(r,0,1e-3)){
      std::cout <<" Probleme !!! R == 0 !!!  " << r << std::endl;
      r = 1e-3;
    }

  }

  double k,l,kl;
  k  = 0;
  kl = 0;
  l = length;

  if(omega_b > 0 && omega_t == 0){

    VectorXd bigA = VectorXd::Zero(4);
    
    bigA.block<3,1>(0,0) = psi_b * U ;

    bigA(3) =  U.dot( delta_b + gamma);

    VectorXd bigB = VectorXd::Zero(4);
    
    bigB.block<3,1>(0,0) = r * delta_b + lambda_b  - psi_b * P ;

    bigB(3) =  epsilon + r * omega_b - delta_b.dot(P);

    VectorXd X = VectorXd::Zero(1);

    X = pinvSolve(bigA,bigB);
    
    k = X(0);
  }
  
  if(omega_t > 0 && omega_b == 0){

    VectorXd bigA = VectorXd::Zero(4);
    
    bigA.block<3,1>(0,0) = psi_t * U ;

    bigA(3) =  U.dot( delta_t + gamma);

    VectorXd bigB = VectorXd::Zero(4);
    
    bigB.block<3,1>(0,0) = r * delta_t + lambda_t  - psi_t * P ;

    bigB(3) =  epsilon + r * omega_t - delta_t.dot(P);

    VectorXd X = VectorXd::Zero(1);

    X = pinvSolve(bigA,bigB);
    
    kl = X(0);

    if(knowLength){
    
      k = kl - l;
    }

    else{

      l = kl - k;

    }
      
  }

  if( omega_b > 0 && omega_t > 0){

    MatrixXd bigA = MatrixXd::Zero(7,2);
    
    bigA.block<3,1>(0,0) = psi_b * U ;

    bigA(6,0) =  U.dot( delta_b + gamma);

    bigA.block<3,1>(3,1) = psi_t * U ;

    bigA(6,1) =  U.dot( delta_t + gamma);

    VectorXd bigB = VectorXd::Zero(7);
    
    bigB.block<3,1>(0,0) = r * delta_b + lambda_b  - psi_b * P ;

    bigB.block<3,1>(3,0) = r * delta_t + lambda_t  - psi_t * P ;

    bigB(6) =  epsilon + r * (omega_t + omega_b) - P.dot(delta_b + delta_t);

    VectorXd X = VectorXd::Zero(2);

    X = pinvSolve(bigA,bigB);
    
    k = X(0);

    kl = X(1);
 
    l = kl - k;

    if(!knowLength){
      length = abs(l);
    }

  }

  /*

      if(omega_b>0){

    Vector3d inv = psi_b * U;

    double norme = inv.norm();

    Vector3d rect = r * delta_b + lambda_b  - psi_b * P;

    k  = inv.dot(rect)/ (norme * norme);

  }
  
  if(omega_t>0){

    Vector3d inv = psi_t * U;

    double norme = inv.norm();

    Vector3d rect = r * delta_t + lambda_t  - psi_t * P;

    kl  = inv.dot(rect)/ (norme * norme);

    if(knowLength && omega_b == 0){
    
      k = kl - l;
    }

    else{

      l = kl - k;

    }
      
  }

  */

  B.noalias() = P + k * U;
  
  T.noalias() = P + (k+l) * U;
  

  estimation(0) = r;

  estimation.block<3,1>(1,0) = B;

  estimation.block<3,1>(4,0) = T;

  VectorXd SE = modele->getFunction(estimation);
 
  VectorXd ES = modele->getError(S,SE);
  
  double eT = ES(1);

  if(abs(eT)> 0.5* M_PI){

    estimation.block<3,1>(1,0).noalias() = P - k * U;

    estimation.block<3,1>(4,0).noalias() = P - (k+l) * U;

  }

}


void BCylinderEstimator::draw(CVEcran * const vue, cv::Scalar couleur) const{
  //if(hasInfluence || traceur->isLost())
    {
      double rr = estimation(0);
      if(rr <= 0.5e-2){
	rr = 1e-2;
      }
      Vector3d B = estimation.block<3,1>(1,0);
      Vector3d T = estimation.block<3,1>(4,0);
      Vector3d BT = B - T;
      double l  = BT.norm();
      if(l < 1e-2){
	T = 1e-2 * BT.normalized() + B;
      }
      BoundedCylinder tube(rr,B,T);
      tube.attachToCamera(oeil);
      tube.setSituation(frame);
      tube.draw(vue,blinkColor);
      //modele->drawFeat(modele->getFunction(estimation),vue,blinkColor);
    }
    //else
    //traceur->draw(vue,couleur);
    //if(traceur->isLost())
    //traceur->draw(vue,couleur);
}


void BCylinderEstimator::writeLog(){
  double heure = MyClock::getTime();
  if(hasLog && access)
    {
      if(heure>properTime)
	{
	  printer << heure << " " << estimation.transpose()<<" "<< length << std::endl;
	  properTime = heure;
	}
      //printer << nUpdates << " " << estimation.transpose()<<std::endl;
    }
}
