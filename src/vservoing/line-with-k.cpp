#include "line-with-k.h"

using namespace Eigen;
using namespace std;



LineWithK::LineWithK(const VectorXd & pA, const VectorXd & pB, const VectorXd & pC):
  AbstractLineFeature(),
  segment(pA,pB),
  point(pC)
{
}


int LineWithK::getDimensionState() const{
  return 9;
}

int LineWithK::getDimensionOutput() const{
  return 3;
}

bool LineWithK::itCanBeSeen() const{
  return segment.itCanBeSeen() && point.itCanBeSeen();
}


VectorXd LineWithK::getFunction(const VectorXd & P) const{
  assert(P.rows()==9);
  IntrinsicCam optics = oeil->getIntrinsicParameters();
  VectorXd F = VectorXd::Zero(3);
  F.block<2,1>(0,0) = segment.getFunction(P.block<6,1>(0,0));
  //double rho = F(0);
  double theta = F(1);
  VectorXd XY = point.getFunction(P.block<3,1>(6,0))-optics.getCenter();
  double x = XY(0);
  double y = XY(1);
  F(2) = -x * sin(theta) + y * cos(theta);
  /*
  if(rho<0){
    F(2) = - F(2);
  }
  //*/
  return F;
}  


MatrixXd LineWithK::getNaiveInteractionMatrix(const VectorXd & F) const{
  assert(F.rows()==3);
  IntrinsicCam optics = oeil->getIntrinsicParameters();
  double rho = F(0);
  double theta = F(1);
  double cosT = cos(theta);
  double sinT = sin(theta);
  double k = F(2);
  double x = rho * cosT - k * sinT;
  double y = rho * sinT + k * cosT;
  Vector2d A(x, y);
  
  MatrixXd JK = MatrixXd::Zero(1,4);
  JK <<          
        0,  -rho,
    -sinT,  cosT;
  MatrixXd LX = MatrixXd::Zero(4,6);
  LX.block<2,6>(0,0) = segment.getNaiveInteractionMatrix(F.block<2,1>(0,0));
  LX.block<2,6>(2,0) = point.getNaiveInteractionMatrix(A + optics.getCenter());
  MatrixXd L = MatrixXd::Zero(3,6);
  L.block<2,6>(0,0) = LX.block<2,6>(0,0);
  L.block<1,6>(2,0) = JK * LX;
  return L;
}


MatrixXd LineWithK::getInteractionMatrix(const VectorXd & P) const{
  assert(P.rows()==9);
  MatrixXd jacobien = MatrixXd::Zero(4,6);
  jacobien.block<2,6>(0,0) = segment.getInteractionMatrix(P.block<6,1>(0,0));
  jacobien.block<2,6>(2,0) = point.getInteractionMatrix(P.block<3,1>(6,0));
  VectorXd F = VectorXd::Zero(2);
  F = segment.getFunction(P.block<6,1>(0,0));
  double rho = F(0);
  double theta = F(1);
  /*
  if(rho<0){
    rho = - rho;
    theta -= M_PI;
  }
  //*/
  MatrixXd A = MatrixXd::Zero(1,4);
  A <<           0,        -rho,
       -sin(theta),  cos(theta);
  MatrixXd L = MatrixXd::Zero(3,6);
  L.block<2,6>(0,0) = jacobien.block<2,6>(0,0);
  L.block<1,6>(2,0) = A * jacobien;
  return L;
}

VectorXd LineWithK::getFunction() const{
  IntrinsicCam optics = oeil->getIntrinsicParameters();
  VectorXd F = VectorXd::Zero(3);
  F.block<2,1>(0,0) = segment.getFunction();
  //double rho = F(0);
  double theta = F(1);
  VectorXd XY = point.getFunction()-optics.getCenter();
  double x = XY(0);
  double y = XY(1);
  F(2) = -x*sin(theta) + y * cos(theta);
  /*
  if(rho<0){
    F(2) = - F(2);
  }
  //*/
  return F;
}  

MatrixXd LineWithK::getInteractionMatrix() const{
  MatrixXd jacobien = MatrixXd::Zero(4,6);
  jacobien.block<2,6>(0,0) = segment.getInteractionMatrix();
  jacobien.block<2,6>(2,0) = point.getInteractionMatrix();
  VectorXd F = VectorXd::Zero(2);
  F = segment.getFunction();
  double rho = F(0);
  double theta = F(1);
  /*
  if(rho<0){
    rho = - rho;
    theta -= M_PI;
  }
  //*/
  MatrixXd A = MatrixXd::Zero(1,4);
  A <<           0,        -rho,
       -sin(theta),  cos(theta);
  MatrixXd L = MatrixXd::Zero(3,6);
  L.block<2,6>(0,0) = jacobien.block<2,6>(0,0);
  L.block<1,6>(2,0) = A * jacobien;
  return L;
}

VectorXd LineWithK::getError(const VectorXd & S,const VectorXd & R) const{
  VectorXd erreur = S - R;
  erreur.block<2,1>(0,0)= segment.getError(S.block<2,1>(0,0),R.block<2,1>(0,0));
  return erreur;
}

MatrixXd LineWithK::getJacobianError(const VectorXd & S,const VectorXd & reference) const{
  MatrixXd jE = MatrixXd::Identity(3,3);
  jE.block<2,2>(0,0) = 
    segment.getJacobianError(S.block<2,1>(0,0),reference.block<2,1>(0,0));
  return jE;
}

MatrixXd LineWithK::getJacobianReferenceError(const VectorXd & S,const VectorXd & R) const{
  VectorXd E = S - R;
  MatrixXd jE = -MatrixXd::Identity(3,3);
  jE.block<2,2>(0,0) = segment.getJacobianReferenceError(S.block<2,1>(0,0), R.block<2,1>(0,0));
  return jE;
}

  
void LineWithK::attachToCamera(Camera * eye){
  assert(eye!=NULL);
  oeil = eye;
  segment.attachToCamera(eye);
  point.attachToCamera(eye);
}

void LineWithK::setSituation(const Repere & r){
  frame = r;
  segment.setSituation(r);
  point.setSituation(r);
}

void LineWithK::draw(CVEcran * const vue, cv::Scalar couleur) const{
  segment.draw(vue, couleur);
  Vector2d val = point.getFunction();
  val = vue->getPoint(val);
  int a,b;
  a = static_cast<int>(val(0));
  b = static_cast<int>(val(1));
  cv::circle(vue->getImage(),cv::Point(a,b),2,couleur,-1);
}
  

void LineWithK::drawFeat(const VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const{
  segment.drawFeat(value.block<2,1>(0,0), vue, couleur);
  double rho = value(0);
  double theta = value(1);
  double k = value(2);
  double x = rho * cos(theta) - k * sin(theta);
  double y = rho * sin(theta) + k * cos(theta);
  Vector2d val = VectorXd::Zero(2);
  val(0) = x;
  val(1) = y;
  IntrinsicCam optics = oeil->getIntrinsicParameters();
  val = val + optics.getCenter();
  Vector2d I = val;
  I = vue->getPoint(I);
  int a,b;
  a = static_cast<int>(I(0));
  b = static_cast<int>(I(1));
  cv::circle(vue->getImage(),cv::Point(a,b), 3 ,couleur,-1);
}
