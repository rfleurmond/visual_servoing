#include "point-estimator.h"

using namespace Eigen;

PointEstimator::PointEstimator(VisualFeature * model, VTracker * tracker):
  FeatureEstimator(model,tracker),
  phi(MatrixXd::Zero(3,3)),
  beta(VectorXd::Zero(3))
{
  assert(model!=NULL);
  assert(tracker!=NULL);

  traceur = tracker;
  modele = model;
  estimation = VectorXd::Zero(3);
  frame = modele->getSituation();
  oeil = tracker->getEye();

}

void PointEstimator::computeEstimation(){
  /**
     \n
     Detail of calculation and implementation
  */

  /// \f$ C_i \f$ is the current position of the optical center
  /// of the camera with respect to the target frame

  Vector3d CI = frame.getPointInME(oeil->getRepere().getDeplacement());
  
  /// \f$ I_i \f$ is the image of the point and \f$ U_i \f$ the corresponding direction vector
  
  Vector2d I = traceur->getFunction().block<2,1>(0,0);
  
  Vector3d UI = frame.getVectorInME(oeil->getCleanDirection(I));
  
  /// \f$ P_i  = I_3 - U_i \cdot U^T_i \f$ built projector for \f$ U_i \f$ 
  
  MatrixXd PI( MatrixXd::Identity(3,3));
  PI.noalias() -= UI * UI.transpose();

  /**
    
    integrates the last informations

    \f{eqnarray*}{
       \phi    & = & \phi  + P_i \\
       \beta   & = & \beta + P_i \cdot C_i
    \f}


    then make the computation of estimation

    
    \f{eqnarray*}{
       \widehat{P} & = & {\phi}^{+} \times \beta
    \f}

    Estimation = \f$ \widehat{P} \f$
 */

  phi += PI;
  
  beta.noalias() += PI * CI;
  
  estimation = phi.ldlt().solve(beta);
}

/*
MatrixXd  PointEstimator::getEstimatedJacobian() const{
  
  Vector3d pointM = frame.getPointInParent(point);
  Repere rC = oeil->getRepere();
  Vector3d P = rC.getPointInME(pointM);
  double fU = oeil->getSigneU()*oeil->getAlphaU(); 
  double fV = oeil->getSigneV()*oeil->getAlphaV();
  Vector2d trou = oeil->getCleanImage(pointM);
  double x = trou(0);
  double y = trou(1);
  double Z = P(oeil->getDirectionZ());
  MatrixXd J1 = MatrixXd::Zero(2,3);
  J1.col(oeil->getDirectionU()) << fU/Z,    0;
  J1.col(oeil->getDirectionV()) <<    0, fV/Z;
  J1.col(oeil->getDirectionZ()) << -x/Z, -y/Z;
  MatrixXd J2 = rC.getJacobianPointInME(pointM);
  MatrixXd J3 = frame.getJacobianPointInParent(point);
  MatrixXd J = MatrixXd::Zero(2,15);
  J.block<2,6>(0,0) = J1*J2.block<3,6>(0,0); 
  J.block<2,9>(0,6) = J1*J2.block<3,3>(0,6)*J3;
  return J;
}
*/
