#include "visualtracker.h"

VTracker::VTracker():
  oeil(NULL)
{

}

void VTracker::attachToCamera(Camera * eye){
  assert(eye!=NULL);
  oeil = eye;
}

void VTracker::findFeatures(const Eigen::VectorXd & feat){
}

Camera * VTracker::getEye() const{
  return oeil;
}

bool VTracker::isLost() const{
 return false;
}


