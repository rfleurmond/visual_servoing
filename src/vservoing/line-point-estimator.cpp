#include "line-point-estimator.h"

using namespace Eigen;


LinePointEstimator::LinePointEstimator(VisualFeature * model, VTracker * tracker):
  FeatureEstimator(model,tracker),
  phi(MatrixXd::Zero(3,3)),
  beta(VectorXd::Zero(3))
{
  assert(model!=NULL);
  assert(tracker!=NULL);

  estimation = VectorXd::Zero(6);
  frame = modele->getSituation();
  oeil = tracker->getEye();

}

void LinePointEstimator::computeEstimation(){
  
  /**
     \n
     Detail of calculation and implementation
  */

  /// \f$ C_i \f$ is the current position of the optical center
  /// of the camera with respect to the target frame

  Vector3d CI = frame.getPointInME(oeil->getRepere().getDeplacement());
  
  /// Get information form tracker

  /// \f$[\rho \ \theta ] <- F \f$

  VectorXd F = traceur->getFunction();
  
  Vector2d RhoTheta = F.block<2,1>(0,0);
  
  /// Compute two points on the line on image plane
  
  double rho = RhoTheta(0);
  double theta = RhoTheta(1);

  double x,y;
  Vector2d imgi, centre;
  IntrinsicCam optique = oeil->getIntrinsicParameters();
  centre = optique.getCenter();
  
  double k1,k2;
  k1 = 0;
  k2 = 0.25 * optique.alphaU;

  x = rho * cos(theta) - k1 * sin(theta);
  y = rho * sin(theta) + k1 * cos(theta);

  imgi << x,y ;
  imgi += centre;

  Vector3d U1 = frame.getVectorInME(oeil->getCleanDirection(imgi));

  x = rho * cos(theta) - k2 * sin(theta);
  y = rho * sin(theta) + k2 * cos(theta);

  imgi << x,y ;
  imgi += centre;

  /// Compute the 2 corresponding direction vector

  /// Compute a normal \f$ N_i \f$ to the line by the optical Center

  Vector3d U2 = frame.getVectorInME(oeil->getCleanDirection(imgi));

  Vector3d NI = U1.cross(U2).normalized();

  /// Compute a projector \f$ P_i = N_i \times N^T_i \f$ 
 
  /**
    
    integrates the last information

    then make the computation of estimation
    
    \f{eqnarray*}{
       \phi    & = & \phi  + P_i \\
       \beta   & = & \beta + P_i \cdot C_i
    \f}
    
    resoudre(phi,beta,P,U)

    \f$ [\widehat{P}, \ \widehat{U}] \f$ will be putted on member estimation
 */


  MatrixXd PI = NI * NI.transpose();
  
  phi  +=  PI;
  beta.noalias() +=  PI * CI;
  
  VectorXd PP, UU;

  LineEstimator::resoudre(phi,beta,PP,UU);

  estimation.block<3,1>(0,0) = PP;

  estimation.block<3,1>(3,0).noalias() = PP + UU;

  VectorXd FE = modele->getFunction(estimation);
 
  VectorXd EF = modele->getError(F,FE);
  
  double eT = EF(1);

  if( eT>0.5* M_PI || eT < - 0.5* M_PI){
    
    estimation.block<3,1>(3,0).noalias() = PP - UU;

  }
  
}


