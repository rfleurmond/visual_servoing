#include "segment-estimator.h"

using namespace Eigen;

SegmentEstimator::SegmentEstimator(VisualFeature * model, VTracker * tracker):
  FeatureEstimator(model,tracker),
  phi1(MatrixXd::Zero(3,3)),
  beta1(VectorXd::Zero(3)),
  phi2(MatrixXd::Zero(3,3)),
  beta2(VectorXd::Zero(3))
{
  assert(model!=NULL);
  assert(tracker!=NULL);

  estimation = VectorXd::Zero(6);
  frame = modele->getSituation();
  oeil = tracker->getEye();

}

void SegmentEstimator::computeEstimation(){

  /// @see PointEstimator::computeEstimation()
  
  

  Vector3d CI = frame.getPointInME(oeil->getRepere().getDeplacement());
  VectorXd F = traceur->getFunction();
  
  double rho = F(0);
  double theta = F(1);
  double k1 = F(2);
  double k2 = F(3);

  double x,y;
  Vector2d imgi, centre;
  IntrinsicCam optique = oeil->getIntrinsicParameters();
  centre = optique.getCenter();
  
  x = rho * cos(theta) - k1 * sin(theta);
  y = rho * sin(theta) + k1 * cos(theta);

  imgi << x,y ;
  imgi += centre;

  Vector3d U1 = frame.getVectorInME(oeil->getCleanDirection(imgi));

  x = rho * cos(theta) - k2 * sin(theta);
  y = rho * sin(theta) + k2 * cos(theta);

  imgi << x,y ;
  imgi += centre;

  Vector3d U2 = frame.getVectorInME(oeil->getCleanDirection(imgi));

  MatrixXd PI(MatrixXd::Identity(3,3));
  PI.noalias() -= U1 * U1.transpose();
  
  phi1 += PI;
  beta1.noalias() += PI * CI;
  
  PI = MatrixXd::Identity(3,3);
  PI.noalias() -= U2 * U2.transpose();
  
  phi2 += PI;
  beta2.noalias() += PI * CI;
  
  estimation.block<3,1>(0,0) = phi1.ldlt().solve(beta1);

  estimation.block<3,1>(3,0) = phi2.ldlt().solve(beta2);

}


