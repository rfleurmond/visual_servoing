#include "robot-system.h"

using namespace Eigen;

RobotSystem::RobotSystem(Robot * r2d2):
  periode(1.0/200),
  tempo(0),
  body(r2d2),
  dimension(1),
  checkBounds(true),
  vitesse(VectorXd::Zero(1)),
  minima(VectorXd::Zero(1)),
  maxima(VectorXd::Zero(1))
{
  assert(r2d2!=NULL);
  dimension = static_cast<int>(r2d2->getCoordonneArticulaires().rows());
  minima = r2d2->getMinimalQ();
  maxima = r2d2->getMaximalQ();
  vitesse = VectorXd::Zero(dimension);
  assert(dimension == minima.rows());
  assert(dimension == maxima.rows());
}

int RobotSystem::getDimState() const{
  return dimension;
}

VectorXd RobotSystem::getState() const{
  return body->getCoordonneArticulaires();
}

void RobotSystem::setState(const VectorXd & state){
  body->setCoordonneArticulaires(state);
}

void RobotSystem::takeCommand(const VectorXd & order){
  VectorXd etat(body->getCoordonneArticulaires());
  assert(order.rows()==dimension);
  vitesse = order;
  if(checkBounds){
    for(int i = 0; i<dimension; i++){
      assert(minima(i)<maxima(i));
      if(isEqual(etat(i),maxima(i),1e-6)){
	if(vitesse(i) > 0)
	  vitesse(i) = 0;
      }
      if(isEqual(etat(i),minima(i),1e-6)){
	if(vitesse(i) < 0)
	  vitesse(i) = 0;
      }
      if(etat(i)>maxima(i)){
	vitesse(i) = 0;
      }
      if(etat(i)<minima(i)){
	vitesse(i) = 0;
      }
    }
  }
}

void RobotSystem::updateTime(double t){
  VectorXd etat(body->getCoordonneArticulaires());
  if(checkBounds){
    for(int i = 0;i<dimension;i++){
      assert(minima(i)<maxima(i));
      if(etat(i)>maxima(i)){
	etat(i) = maxima(i);
      }
      if(etat(i)<minima(i)){
	etat(i) = minima(i);
      }
    }
  }
  etat += (t - tempo) * vitesse;
  setState(etat);
  tempo = t;
}

double RobotSystem::getPeriod() const{
  return periode;
}

void RobotSystem::setPeriod(double p){
  periode = p;
}

void RobotSystem::enableCheckBounds(bool t){
  checkBounds = t;
}
