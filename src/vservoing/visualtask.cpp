#include "visualtask.h"

using namespace Eigen;

AbstractVisualTask::AbstractVisualTask():
  Task()
{

}

VisualTask::VisualTask(VisualFeature * visuel, InverseKinematicModel * mo):
  AbstractVisualTask(),
  shouldErase(true),
  modeFixation(VisualTask::EMBARQUE),
  periode(0.05),
  graphique(NULL),
  features(visuel),
  modele(mo),
  oracle(NULL),
  reference(),
  fixation(),
  trepied()
{
  assert(visuel != NULL);
  assert(mo!=NULL);


  dimInput = modele->getTotalDegres();
  dimOutput = features->getDimensionOutput();
  
  etat_q = VectorXd::Zero(dimInput);
  dot_q = 0 * etat_q;
  reference = VectorXd::Zero(dimOutput);
  
  graphique = new visualisation();

  graphique->windows.setNom("Une seule camera");

  assert(features->getEye()!=NULL);

  graphique->windows.setDimensions(640,480);
  
  graphique->windows.setOptique(features->getEye()->getIntrinsicParameters());
  
  graphique->couleur = Palette::getLastColor();
  
  graphique->teinte = Palette::getLightColor();

  creerFenetre();

}

VisualTask::~VisualTask(){
  graphique->windows.fermerFenetre();
  delete graphique;
  cv::destroyAllWindows();
  
}

void VisualTask::setReference(const VectorXd & sStar){
  reference = sStar;

}
VectorXd VisualTask::getReference() const{
  if(oracle!=NULL){
    return oracle->getReference();
  }
  return reference;
}

VectorXd VisualTask::fonction(const VectorXd & q) const{
  assert(features->itCanBeSeen());
  features->useTracker();
  nettoyerImage();
  
  VectorXd erreur(features->getErrorBetweenMeAnd(getReference()));
  afficherReferences(features);
  afficherFeatures(features);
  graphique->windows.affichage();
  attendreImage();
  return erreur;
}

void VisualTask::setState(const VectorXd & q){
  etat_q = q;
  Repere rr = fixation;
  Repere a = modele->getSituation(etat_q);
  rr = rr * a;

  if(isEyeInHand()){
    assert(features->getEye()!=NULL);
    features->getEye()->setRepere(rr);
  }
  if(isEyeToHand()){
    features->setSituation(rr);
  }
}

void VisualTask::setTransformation(const Repere & w){
  fixation = w;
}

void VisualTask::setModeFixation(int m){
  if(m!=VisualTask::DEPORTEE && m!=VisualTask::EMBARQUE){
    modeFixation = VisualTask::EMBARQUE;
  }
  else{
    modeFixation = m;
  }
}

bool VisualTask::isEyeInHand() const{
  return modeFixation==VisualTask::EMBARQUE;
}
  
bool VisualTask::isEyeToHand() const{
  return modeFixation==VisualTask::DEPORTEE;
}

void VisualTask::setBaseCamera(const Repere & r){
  trepied = r;
  if(isEyeToHand()){
    features->getEye()->setRepere(trepied);
  }
}


MatrixXd VisualTask::getKineticTransformation() const{
  MatrixXd kine = MatrixXd::Identity(6,6);
  if(isEyeInHand()){
    kine = fixation.getInversKineticMatrix();
  }
  if(isEyeToHand()){
    Repere ephemere = trepied.inverse() * modele->getSituation(etat_q);
    kine = -ephemere.getInversKineticMatrix();  
  }
  return kine;
}

MatrixXd VisualTask::getInversKineticTransformation() const{
  MatrixXd kine= MatrixXd::Identity(6,6);
  if(isEyeInHand()){
    kine = fixation.getKineticMatrix();
  }
  if(isEyeToHand()){
    Repere ephemere = trepied.inverse() * modele->getSituation(etat_q);
    kine = -ephemere.getKineticMatrix();
  }
  return kine;
}


MatrixXd VisualTask::jacobien(const VectorXd & q) const{
  MatrixXd jacobienne(features->getJacobianError(getReference()));
  jacobienne = jacobienne * features->getInteractionMatrix() * getKineticTransformation();
  MatrixXd rota = modele->getSituation(q)
    .getRotation().getMatrix().transpose();
  jacobienne.leftCols(3) = jacobienne.leftCols(3) * rota;
  jacobienne.rightCols(3) = jacobienne.rightCols(3) * rota;
  jacobienne = jacobienne * modele->getKinematicJacobian(q);
  return jacobienne;
}

VectorXd VisualTask::calculCommande() const{
  return calculCommande(fonction(etat_q));
}

VectorXd VisualTask::calculCommande(const VectorXd & erreur) const{
  VectorXd torseur = features->getTorseurCinematique(lambda,erreur);
  torseur = getInversKineticTransformation()*torseur;
  MatrixXd rota = modele->getSituation(etat_q)
    .getRotation().getMatrix();
  torseur.topRows(3) = rota * torseur.topRows(3);
  torseur.bottomRows(3) = rota * torseur.bottomRows(3);
  VectorXd commande;
  modele->getCommandFromVelocity(etat_q,torseur,commande);
  return commande;
}

double VisualTask::getPeriod() const{
  return periode;
}

void VisualTask::setPeriod(double dt){
  periode = dt;
}

void VisualTask::afficherFeatures(VisualFeature * feat) const{
  feat->draw(&graphique->windows,graphique->couleur);
}

void VisualTask::afficherReferences(VisualFeature * feat) const{
  feat->drawFeat(getReference(),&graphique->windows,graphique->teinte);
}

void VisualTask::attendreImage() const{
  cv::waitKey(CVperiode);
}

void VisualTask::nettoyerImage() const{
  if(shouldErase){
    graphique->windows.nettoyer();
  }
}

void VisualTask::creerFenetre(){
  graphique->windows.ouvrirFenetre();
  graphique->windows.nettoyer();
  
}

void VisualTask::keepVisualTrace(bool val){
  shouldErase = (val==false);
}
