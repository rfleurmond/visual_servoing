#include  "multi-feature.h"

using namespace Eigen;

using namespace std;


MultiFeature::MultiFeature(VisualFeature * f):
  VisualFeature()
{
  addFeature(f);
}

VectorXd MultiFeature::getFunction(const VectorXd & P) const{
  VectorXd image = VectorXd::Zero(getDimensionOutput());
  int dim = 0;
  int tot = 0;
  int dimS = 0;
  int totS = 0;
  VisualFeature * f = NULL;
  list<VisualFeature *>::const_iterator it;
  for(it = liste.begin(); it!= liste.end();it++){
    f = (*it);
    dim = f->getDimensionOutput();
    dimS = f->getDimensionState();
    image.block(tot,0,dim,1) = f->getFunction(P.block(totS,0,dimS,1));
    tot += dim;
    totS += dimS;
  }
  return image;
}  

VectorXd MultiFeature::getFunction() const{
  VectorXd image = VectorXd::Zero(getDimensionOutput());
  int dim = 0;
  int tot = 0;
  VisualFeature * f = NULL;
  list<VisualFeature *>::const_iterator it;
  for(it = liste.begin(); it!= liste.end();it++){
    f = (*it);
    dim = f->getDimensionOutput();
    image.block(tot,0,dim,1) = f->getFunction();
    tot+=dim;
  }
  return image;
}  



bool MultiFeature::itCanBeSeen() const{
  VisualFeature * f = NULL;
  list<VisualFeature *>::const_iterator it;
  for(it = liste.begin(); it!= liste.end();it++){
    f = (*it);
    if(f->itCanBeSeen()==false){
      return false;
    }
  }
  return true;
}

int MultiFeature::getDimensionState() const{
  int dim = 0;
  int tot = 0;
  VisualFeature * f = NULL;
  list<VisualFeature *>::const_iterator it;
  for(it = liste.begin(); it!= liste.end();it++){
    f = (*it);
    dim = f->getDimensionState();
    tot+=dim;
  }
  return tot;
}  


int MultiFeature::getDimensionOutput() const{
  int dim = 0;
  int tot = 0;
  VisualFeature * f = NULL;
  list<VisualFeature *>::const_iterator it;
  for(it = liste.begin(); it!= liste.end();it++){
    f = (*it);
    dim = f->getDimensionOutput();
    tot+=dim;
  }
  return tot;
}


void MultiFeature::setSituation(const Repere & r){
  frame = r;
  VisualFeature * f = NULL;
  list<VisualFeature *>::const_iterator it;
  for(it = liste.begin(); it!= liste.end();it++){
    f = (*it);
    f->setSituation(r);
  }
}

void MultiFeature::useTracker(){
  VisualFeature * f = NULL;
  list<VisualFeature *>::const_iterator it;
  for(it = liste.begin(); it!= liste.end();it++){
    f = (*it);
    f->useTracker();
  }
}


void MultiFeature::attachToCamera(Camera * eye){
  assert(eye!=NULL);
  oeil = eye;
  VisualFeature * f = NULL;
  list<VisualFeature *>::const_iterator it;
  for(it = liste.begin(); it!= liste.end();it++){
    f = (*it);
    f->attachToCamera(eye);
  }
}

MatrixXd MultiFeature::getNaiveInteractionMatrix(const VectorXd & F) const{
  MatrixXd jacobienne = MatrixXd::Zero(getDimensionOutput(),6);
  int dim = 0;
  int tot = 0;
  VisualFeature * f = NULL;
  list<VisualFeature *>::const_iterator it;
  for(it = liste.begin(); it!= liste.end();it++){
    f = (*it);
    dim = f->getDimensionOutput();
    jacobienne.block(tot,0,dim,6) = f->getNaiveInteractionMatrix(F.block(tot,0,dim,1));
    tot += dim;
  }
  return jacobienne;
}


MatrixXd MultiFeature::getInteractionMatrix(const VectorXd & P) const{
  MatrixXd jacobienne = MatrixXd::Zero(getDimensionOutput(),6);
  int dim = 0;
  int tot = 0;
  int dimS = 0;
  int totS = 0;
  VisualFeature * f = NULL;
  list<VisualFeature *>::const_iterator it;
  for(it = liste.begin(); it!= liste.end();it++){
    f = (*it);
    dim = f->getDimensionOutput();
    dimS = f->getDimensionState();
    jacobienne.block(tot,0,dim,6) = f->getInteractionMatrix(P.block(totS,0,dimS,1));
    tot += dim;
    totS += dimS;
  }
  return jacobienne;
}



MatrixXd MultiFeature::getInteractionMatrix() const{
  MatrixXd jacobienne = MatrixXd::Zero(getDimensionOutput(),6);
  int dim = 0;
  int tot = 0;
  VisualFeature * f = NULL;
  list<VisualFeature *>::const_iterator it;
  for(it = liste.begin(); it!= liste.end();it++){
    f = (*it);
    dim = f->getDimensionOutput();
    jacobienne.block(tot,0,dim,6) = f->getInteractionMatrix();
    tot+=dim;
  }
  return jacobienne;
}

VectorXd MultiFeature::getError(const VectorXd & S, const VectorXd & autre) const{
  VectorXd erreur = VectorXd::Zero(getDimensionOutput());
  int dim = 0;
  int tot = 0;
  VisualFeature * f = NULL;
  list<VisualFeature *>::const_iterator it;
  for(it = liste.begin(); it!= liste.end();it++){
    f = (*it);
    dim = f->getDimensionOutput();
    erreur.block(tot,0,dim,1) = f->getError(S.block(tot,0,dim,1) , autre.block(tot,0,dim,1));
    tot+=dim;
  }
  return erreur;
}


MatrixXd MultiFeature::getJacobianError(const VectorXd & S, const VectorXd & autre) const{
  MatrixXd jacobien = MatrixXd::Zero(getDimensionOutput(),getDimensionOutput());
  int dim = 0;
  int tot = 0;
  VisualFeature * f = NULL;
  list<VisualFeature *>::const_iterator it;
  for(it = liste.begin(); it!= liste.end();it++){
    f = (*it);
    dim = f->getDimensionOutput();
    jacobien.block(tot,tot,dim,dim) = f->getJacobianError(S.block(tot,0,dim,1), autre.block(tot,0,dim,1));
    tot+=dim;
  }
  return jacobien;
}


MatrixXd MultiFeature::getJacobianReferenceError(const VectorXd & S, const VectorXd & autre) const{
  MatrixXd jacobien = MatrixXd::Zero(getDimensionOutput(),getDimensionOutput());
  int dim = 0;
  int tot = 0;
  VisualFeature * f = NULL;
  list<VisualFeature *>::const_iterator it;
  for(it = liste.begin(); it!= liste.end();it++){
    f = (*it);
    dim = f->getDimensionOutput();
    jacobien.block(tot,tot,dim,dim) = f->getJacobianReferenceError(S.block(tot,0,dim,1), autre.block(tot,0,dim,1));
    tot+=dim;
  }
  return jacobien;
}


void MultiFeature::addFeature(VisualFeature * f){
  assert(f!=NULL);
  assert(f!=this);
  liste.push_back(f);
}

void MultiFeature::draw(CVEcran * const vue, cv::Scalar couleur) const{
  VisualFeature * f = NULL;
  list<VisualFeature *>::const_iterator it;
  for(it = liste.begin(); it!= liste.end();it++){
    f = (*it);
    f->draw(vue,couleur);
  }
}


void MultiFeature::drawFeat(const VectorXd & value,CVEcran * const vue, cv::Scalar couleur) const{
  int dim = 0;
  int tot = 0;
  VisualFeature * f = NULL;
  list<VisualFeature *>::const_iterator it;
  for(it = liste.begin(); it!= liste.end();it++){
    f = (*it);
    dim = f->getDimensionOutput();
    f->drawFeat(value.block(tot,0,dim,1),vue,couleur);
    tot+=dim;
  }
}
