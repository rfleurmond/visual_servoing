 #include "feature-multipoint.h"

using namespace Eigen;
using namespace std;

MultiPointFeature::MultiPointFeature(MatrixXd pts):
  VisualFeature(),
  points(pts)
{

}

int MultiPointFeature::getDimensionState() const{
  return static_cast<int>(3*points.rows());
}

VectorXd MultiPointFeature::getFunction(const VectorXd & P) const{
  assert(P.rows() == getDimensionState());
  VectorXd image = VectorXd::Zero(2*points.rows());
  Vector3d point;
  PointFeature pf;
  pf.attachToCamera(oeil);
  pf.setSituation(frame);
  for(int i = 0;i<points.rows();i++){
    point = P.block<3,1>(3*i,0);
    image.block<2,1>(2*i,0) = pf.getFunction(point);
  }
  return image;
}  

VectorXd MultiPointFeature::getFunction() const{
  VectorXd image = VectorXd::Zero(2*points.rows());
  Vector3d point,pointM;
  PointFeature pf;
  pf.attachToCamera(oeil);
  pf.setSituation(frame);
  for(int i = 0;i<points.rows();i++){
    point = points.block<1,3>(i,0).transpose();
    image.block<2,1>(2*i,0) = pf.getFunction(point);
  }
  return image;
}  


bool MultiPointFeature::itCanBeSeen() const{
  Vector3d point,pointM;
  for(int i = 0;i<points.rows();i++){
    point = points.block<1,3>(i,0).transpose();
    pointM = frame.getPointInParent(point);
    if(oeil->canSee(pointM)==false){
      std::cout<<"PointM in world: "<<pointM.transpose()<<std::endl;
      std::cout<<"Repere de la camera:\n"<<oeil->getRepere()<<std::endl;
      std::cout<<"Repere du marqueur :\n"<<frame<<std::endl;
      std::cout<<"Matrice intiiale:\n"<<points<<std::endl;
      return false;
    }
  }
  return true;
}

int MultiPointFeature::getDimensionOutput() const{
  return static_cast<int>(2*points.rows());
}


MatrixXd MultiPointFeature::getNaiveInteractionMatrix(const VectorXd & F) const{
  assert(F.rows() == getDimensionOutput());
  Vector3d point;
  PointFeature pf;
  pf.attachToCamera(oeil);
  pf.setStrategy(strategy);
  pf.setSituation(frame);
  
  MatrixXd jacobienne = MatrixXd::Zero(2*points.rows(),6);
  
  for(int i = 0;i<points.rows();i++){
    jacobienne.block<2,6>(2*i,0) = pf.getNaiveInteractionMatrix(F.block<2,1>(2*i,0));
  }
  return jacobienne;
}


MatrixXd MultiPointFeature::getInteractionMatrix(const VectorXd & PTS) const{
  assert(PTS.rows() == getDimensionState());
  Vector3d point;
  PointFeature pf;
  pf.attachToCamera(oeil);
  pf.setStrategy(strategy);
  pf.setSituation(frame);
  
  MatrixXd jacobienne = MatrixXd::Zero(2*points.rows(),6);
  
  for(int i = 0;i<points.rows();i++){
    point = PTS.block<3,1>(3*i,0);
    jacobienne.block<2,6>(2*i,0) = pf.getInteractionMatrix(point);
  }
  return jacobienne;
}



MatrixXd MultiPointFeature::getInteractionMatrix() const{
  VectorXd PTS = VectorXd::Zero(3*points.rows());
  for(int i = 0;i<points.rows();i++){
    PTS.block<3,1>(3*i,0) = points.block<1,3>(i,0).transpose();
  }
  return getInteractionMatrix(PTS);
}

VectorXd MultiPointFeature::getError(const VectorXd & S, const VectorXd & autre) const{
  return S - autre;
}

PointFeature MultiPointFeature::getFeature(int i) const{
  assert(i<points.rows());
  PointFeature pf(points.row(i).transpose());
  pf.setSituation(getSituation());
  pf.setStrategy(strategy);
  pf.attachToCamera(getEye());
  return pf;
}
  
void MultiPointFeature::drawFeat(const VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const{
  Vector2d I, J;
  I = Vector2d::Zero();
  long int n = value.rows()/2;
  int x,y;
  for(long int k = 0; k < n; k++){
    I = value.block<2,1>(2*k,0);
    J = vue->getPoint(I); 
    x = static_cast<int>(J(0));
    y = static_cast<int>(J(1));
    cv::circle(vue->getImage(),cv::Point(x,y),3,couleur,-1);
  }
}

