#include "feature-droite.h"

using namespace Eigen;
using namespace std;

AbstractLineFeature::AbstractLineFeature():
  VisualFeature()
{

}


DroiteFeature::DroiteFeature(const Droite & d):
  droite(d)
{

}

int DroiteFeature::getDimensionState() const{
  return 6;
}

bool DroiteFeature::itCanBeSeen() const{
  Droite copie = droite;
  copie.getTransformation(frame);
  Repere rC = oeil->getRepere();
  copie.getTransformation(rC.inverse());
  Vector3d direction = Vector3d::Zero();
  direction(oeil->getDirectionZ()) = 1;
  Vector3d pointN = copie.getNearestPoint(Vector3d(0,0,0));
  double scal = pointN.transpose()*direction;
  if(scal >1e-6){
    //cout<<"Le point de la droite le plus proche de la camera est devant celle ci !"<<endl;
    list<Plan> liste = Architect::combinaison(LPoint(),copie);
    if(liste.size()>0){
      //cout<<"Le centre de la camera ne fait pas partie de la droite !"<<endl;
      list<Plan>::const_iterator it;
      for(it = liste.begin(); it!= liste.end(); it++){
	Plan planDroite = (*it);
	Plan planImage;
	planImage.setDirection(direction);
	planImage.setCenter(direction);
	list<Droite> listD = Architect::intersection(planDroite,planImage);
	if(listD.size()>0){
	  //cout<<"La projection de la droite rencontre effectivement le plan image !"<<endl;
    	  list<Droite>::const_iterator itD;
	  for(itD = listD.begin(); itD != listD.end(); itD++){
	    Droite droiteI = (*itD);
	    pointN =  droiteI.getNearestPoint(direction);
	    Vector3d pointNC = rC.getPointInParent(pointN);
	    if(oeil->canSee(pointNC)){
	      //cout<<"Le point de la droite dans l'image le plus proche du centre optique est dans le champ de vue !"<<endl;
    	      return true;
	    }
	  }
	}
      }
    }
  }
  return false;
}

VectorXd DroiteFeature::getFunction(Droite copie) const{
  double rho = 0;
  double teta = 0;
  VectorXd image = VectorXd::Zero(2);
  copie.getTransformation(frame);
  Repere rC = oeil->getRepere();
  copie.getTransformation(rC.inverse());
  Vector3d direction = Vector3d::Zero();
  direction(oeil->getDirectionZ()) = 1;
  Vector3d pointN = copie.getNearestPoint(Vector3d(0,0,0));
  double scal = pointN.transpose()*direction;
  if(scal >1e-6){
    list<Plan> liste = Architect::combinaison(LPoint(),copie);
    if(liste.size()>0){
      list<Plan>::const_iterator it;
      for(it = liste.begin(); it!= liste.end(); it++){
	Plan planDroite = (*it);
	Plan planImage;
	planImage.setDirection(direction);
	planImage.setCenter(direction);
	list<Droite> listD = Architect::intersection(planDroite,planImage);
	if(listD.size()>0){
	  list<Droite>::const_iterator itD;
	  for(itD = listD.begin(); itD != listD.end(); itD++){
	    Droite droiteI = (*itD);
	    pointN =  droiteI.getNearestPoint(direction);
	    Vector3d pointNC = rC.getPointInParent(pointN);
	    if(oeil->canSee(pointNC)){
	      MatrixXd projection = MatrixXd::Zero(2,3);
	      projection(0,oeil->getDirectionU()) = oeil->getSigneU() * oeil->getAlphaU();
	      projection(1,oeil->getDirectionV()) = oeil->getSigneV() * oeil->getAlphaV();
	      Vector2d pente = projection * droiteI.getDirection();
	      Vector2d point = projection * pointN;
	      teta = atan2(pente(1),pente(0)) - M_PI/2;
	      if(teta > M_PI){
		teta -= 2*M_PI;
	      }
	      if(teta < -M_PI){
		teta += 2*M_PI;
	      }
	      rho = point(0)*cos(teta) + point(1)*sin(teta);
	      image = checkParameters(rho,teta);
	    }
	  }
	}
      }
    }
  }
  return image;
}  

MatrixXd DroiteFeature::getNaiveInteractionMatrix(const VectorXd & F) const{
  assert(F.rows()==2);
  IntrinsicCam optics = oeil->getIntrinsicParameters();
  double rho = F(0);
  double theta = F(1);
  double cosT = cos(theta);
  double sinT = sin(theta);
  double k_1 = 0.25 * oeil->getIntrinsicParameters().alphaU;
  double k_2 = -0.25 * oeil->getIntrinsicParameters().alphaU;
  double x_1 = rho * cosT - k_1 * sinT;
  double y_1 = rho * sinT + k_1 * cosT;
  double x_2 = rho * cosT - k_2 * sinT;
  double y_2 = rho * sinT + k_2 * cosT;
  Vector2d A(x_1, y_1);
  Vector2d B(x_2, y_2);

  PointFeature pf;
  pf.attachToCamera(oeil);
  pf.setSituation(frame);

  Vector2d C = optics.getCenter();
  
  MatrixXd LX = MatrixXd::Zero(4,6);
  LX.block<2,6>(0,0) = pf.getNaiveInteractionMatrix(A + C);
  LX.block<2,6>(2,0) = pf.getNaiveInteractionMatrix(B + C);
  
  MatrixXd JJJ(computeJacobianRhoTeta(A,B));

  JJJ = JJJ* LX;
  
  return JJJ;
}



MatrixXd DroiteFeature::getInteractionMatrix(Droite copie) const{
  MatrixXd jacobien = MatrixXd::Zero(2,6);
  double rho = 0;
  double teta = 0;
  copie.getTransformation(frame);
  Repere rC = oeil->getRepere();
  copie.getTransformation(rC.inverse());
  Vector3d direction = Vector3d::Zero();
  direction(oeil->getDirectionZ()) = 1;
  Vector3d pointN = copie.getNearestPoint(Vector3d(0,0,0));
  double scal = pointN.transpose()*direction;
  IntrinsicCam optics = oeil->getIntrinsicParameters();
  if(scal >1e-6){
    list<Plan> liste = Architect::combinaison(LPoint(),copie);
    if(liste.size()>0){
      list<Plan>::const_iterator it;
      for(it = liste.begin(); it!= liste.end(); it++){
	Plan planDroite = (*it);
	Plan planImage;
	planImage.setDirection(direction);
	planImage.setCenter(direction);
	list<Droite> listD = Architect::intersection(planDroite,planImage);
	if(listD.size()>0){
	  list<Droite>::const_iterator itD;
	  for(itD = listD.begin(); itD != listD.end(); itD++){
	    Droite droiteI = (*itD);
	    pointN =  droiteI.getNearestPoint(oeil->getAlphaU()*direction);
	    Vector3d pointNC = rC.getPointInParent(pointN);
	    if(oeil->canSee(pointNC)){
	      MatrixXd projection = MatrixXd::Zero(2,3);
	      projection(0,oeil->getDirectionU()) = oeil->getSigneU() * oeil->getAlphaU();
	      projection(1,oeil->getDirectionV()) = oeil->getSigneV() * oeil->getAlphaV();
	      Vector2d pente = projection * droiteI.getDirection();
	      Vector2d point = projection * pointN;
	      teta = atan2(pente(1),pente(0)) - M_PI/2;
	      if(teta > M_PI){
		teta -= 2*M_PI;
	      }
	      if(teta < -M_PI){
		teta += 2*M_PI;
	      }
	      rho = point(0)*cos(teta) + point(1)*sin(teta);
	      checkParameters(rho,teta);
	      VectorXd eRho  = VectorXd::Zero(2);
	      VectorXd eTheta = VectorXd::Zero(2);
	      eRho   << cos(teta),  sin(teta);
	      eTheta  << -sin(teta), cos(teta);
	      MatrixXd A = MatrixXd::Zero(2,2);
	      A(0,0) = 1;
	      A(1,0) = 1;
	      MatrixXd B = MatrixXd::Zero(2,4);
	      B.block<1,2>(0,0) = eRho.transpose();
	      B.block<1,2>(1,2) = eRho.transpose();
	      MatrixXd points = MatrixXd::Zero(3,2);
	      points.block<3,1>(0,0) = pointN;
	      VectorXd point2 = pointN + droiteI.getDirection();
	      points.block<3,1>(0,1) = point2;
	      list<LPoint> inter;
	      list<LPoint>::const_iterator itP;
	      Droite dX;
	      MatrixXd LX = MatrixXd::Zero(4,6);
	      for(int i = 0; i<2; i++){
		dX = Droite(Vector3d(0,0,0),points.block<3,1>(0,i));
		inter = Architect::intersection(dX,copie);
		for(itP = inter.begin(); itP!= inter.end(); itP++){
		  LPoint pp = (*itP);
		  PointFeature pf(rC.getPointInParent(pp.getCenter()));
		  pf.attachToCamera(oeil);
		  pf.setSituation(frame);
		  A(i,1) = -1*eTheta.transpose()*(pf.getFunction()-optics.getCenter());
		  LX.block<2,6>(2*i,0) = pf.getInteractionMatrix();
		}
	      }
	      jacobien  = A.colPivHouseholderQr().solve(B * LX);
	    }
	  
	  }
	}
      }
    }
  }
  return jacobien;
}


VectorXd DroiteFeature::getFunction(const VectorXd & P) const{
  assert(P.rows() == 6);
  
  Droite delta(
	       static_cast<Vector3d>(P.block<3,1>(0,0)),
	       static_cast<Vector3d>(P.block<3,1>(3,0))
	       );
  
  return getFunction(delta);

}

VectorXd DroiteFeature::getFunction() const{
  return getFunction(droite);
}  


MatrixXd DroiteFeature::getInteractionMatrix(const VectorXd & P) const{
  assert(P.rows() == 6 );
  
  Droite delta(
	       static_cast<Vector3d>(P.block<3,1>(0,0)),
	       static_cast<Vector3d>(P.block<3,1>(3,0))
	       );
  
  return getInteractionMatrix(delta);

}


MatrixXd DroiteFeature::getInteractionMatrix() const{
  return getInteractionMatrix(droite);
}


VectorXd DroiteFeature::getError(const VectorXd & S, const VectorXd & ref) const{
  VectorXd E = S - ref;
  if(E(1)>M_PI/2){
    E(1) = M_PI - E(1);
    E(0) = - ref(0) - S(0);
  }
  if(E(1)<-M_PI/2){
    E(1) = M_PI + E(1); 
    E(0) = -ref(0) - S(0);
  }
  return E;
}


MatrixXd DroiteFeature::getJacobianError(const VectorXd & S,const VectorXd & ref) const{
  MatrixXd J = MatrixXd::Identity(2,2);
  VectorXd E = S - ref;
  /*
  if(E(1)>M_PI/2){
    E(1) = M_PI - E(1);
    E(0) = - ref(0) - S(0);
    J(0,0) = -1;
  }
  if(E(1)<-M_PI/2){
    E(1) = M_PI + E(1); 
    E(0) = -ref(0) - S(0);
    J(0,0) = -1;
  }
  //*/
  return J;
}
  

MatrixXd DroiteFeature::getJacobianReferenceError(const VectorXd & S, const VectorXd & R) const{
  MatrixXd J = -MatrixXd::Identity(2,2);
  return J;
}

int DroiteFeature::getDimensionOutput() const{
  return 2;
}


void DroiteFeature::drawFeat(const VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const{
  dessinerDroite(oeil,value,vue,couleur);
}

void AbstractLineFeature::dessinerDroite(Camera * eyes, 
					 const VectorXd & value, CVEcran * const vue, cv::Scalar couleur){
  double rho = value(0);
  double teta = value(1);
  Vector2d A = Vector2d::Zero();
  Vector2d B = Vector2d::Zero();
  // Yes all should be alphaU or alls should be alphaV
  A(0) = rho*cos(teta) - 2*eyes->getAlphaU()*sin(teta);
  A(1) = rho*sin(teta) + 2*eyes->getAlphaU()*cos(teta);
  B(0) = rho*cos(teta) + 2*eyes->getAlphaU()*sin(teta);
  B(1) = rho*sin(teta) - 2*eyes->getAlphaU()*cos(teta);
  IntrinsicCam optics = eyes->getIntrinsicParameters();
  A = optics.getCenter() + A;
  B = optics.getCenter() + B;
  A = vue->getPoint(A);
  B = vue->getPoint(B);
  cv::Point AA,BB;
  AA.x = static_cast<int>(A(0));
  AA.y = static_cast<int>(A(1));
  BB.x = static_cast<int>(B(0));
  BB.y = static_cast<int>(B(1));
  line(vue->getImage(),AA,BB,couleur,1,8);
}

Vector2d AbstractLineFeature::checkParameters(double & rho,double & teta){
  Vector2d s = Vector2d::Zero();
  if(teta > M_PI/2){
    teta -= M_PI;
    rho = -rho;
  }
  if(teta < -M_PI/2){
    teta += M_PI;
    rho = -rho;
  }
  s(0) = rho;
  s(1) = teta;
  return s;
}

Vector2d AbstractLineFeature::computeRhoTeta(Vector2d A, Vector2d B){
  Vector2d C = Vector2d::Zero();
  Vector2d point = Alignement::getReference(A,B,C);
  Vector2d pente = B - A;
  double teta = atan2(pente(1),pente(0)) - M_PI/2;
  if(teta>=M_PI){
    teta-=2*M_PI;
  }
  if(teta<-M_PI){
    teta+=2*M_PI;
  }
  double rho = point(0)*cos(teta) + point(1)*sin(teta);
  Vector2d image;
  image << rho,teta;
  return image;
}

MatrixXd AbstractLineFeature::computeJacobianRhoTeta(Vector2d A, Vector2d B){
  Vector2d RT = computeRhoTeta(A,B);
  double teta = RT(1);
  VectorXd eRho  = VectorXd::Zero(2);
  VectorXd eTheta = VectorXd::Zero(2);
  eRho   << cos(teta),  sin(teta);
  eTheta  << -sin(teta), cos(teta);
  MatrixXd AA = MatrixXd::Zero(2,2);
  AA(0,0) = 1;
  AA(1,0) = 1;
  MatrixXd BB = MatrixXd::Zero(2,4);
  BB.block<1,2>(0,0) = eRho.transpose();
  BB.block<1,2>(1,2) = eRho.transpose();
  AA(0,1) = -eTheta.transpose()*A;
  AA(1,1) = -eTheta.transpose()*B;
  MatrixXd jacobien = AA.colPivHouseholderQr().solve(BB);
  return jacobien;
}


Vector2d AbstractLineFeature::computeError(Vector2d S,Vector2d ref){
  VectorXd E = S - ref;
  if(E(1)>=M_PI){
    E(1)-=2*M_PI;
  }
  if(E(1)<-M_PI){
    E(1)+=2*M_PI;
  }
  return E;
}

MatrixXd AbstractLineFeature::computeJError(Vector2d S, Vector2d ref){
  MatrixXd J = MatrixXd::Identity(2,2);
  VectorXd E = S - ref;
  /*
  if(fabs(E(1))>=M_PI/2){
    J(1,1) = -1; 
  }
  //*/
  return J; 
}

MatrixXd AbstractLineFeature::computeJRefError(Vector2d S, Vector2d R){
  MatrixXd J = -MatrixXd::Identity(2,2);
  /*
  Vector2d E = S-R;
  if(fabs(E(1))>=M_PI){
    J(1,1) = 1;
  }
  //*/
  return J;
}

