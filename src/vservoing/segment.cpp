#include "segment.h"

using namespace Eigen;
using namespace std;


Segment::Segment(const VectorXd & pA, const VectorXd & pB):
  LineFeature(pA,pB)
{

}

int Segment::getDimensionOutput() const{
  return 4;
}

bool Segment::itCanBeSeen() const{
  bool val  = (pointA.itCanBeSeen() && pointA.itCanBeSeen()); 
  /*
  if(!val){
    std::cout<<"...................Segment .................."<<std::endl;
    std::cout<<"Repere de la camera:\n"<<oeil->getRepere()<<std::endl;
    std::cout<<"Repere du marqueur :\n"<<frame<<std::endl;
    std::cout<<"\n"<<std::endl;
    return false;
  }
  */
  return val;
}


VectorXd Segment::computeRhoTeta2K(Eigen::Vector2d A, Eigen::Vector2d B){
  VectorXd output = VectorXd::Zero(4);
  output.block<2,1>(0,0) = computeRhoTeta(A,B);
  double theta = output(1);
  double x = A(0);
  double y = A(1);
  output(2)  = - x * sin(theta) + y * cos(theta);
  x = B(0);
  y = B(1);
  output(3)  = - x * sin(theta) + y * cos(theta);
  /*
  if(rho<0){
    output.block<2,1>(2,0) = -output.block<2,1>(2,0);
  }
  //*/
  return output;
}


VectorXd Segment::computeABfromRhoTeta2K(Eigen::VectorXd F){
  assert(F.rows()>=4);
  VectorXd output = VectorXd::Zero(4);
  double rho = F(0);
  double theta = F(1);
  double cosT = cos(theta);
  double sinT = sin(theta);
  double k_1 = F(2);
  double k_2 = F(3);
  output(0) = rho * cosT - k_1 * sinT;
  output(1) = rho * sinT + k_1 * cosT;
  output(2) = rho * cosT - k_2 * sinT;
  output(3) = rho * sinT + k_2 * cosT;
  return output;
}

VectorXd Segment::getFunction(const VectorXd & P) const{
  assert(P.rows()==6);
  IntrinsicCam optics = oeil->getIntrinsicParameters();
  Vector2d A = pointA.getFunction(P.block<3,1>(0,0)) - optics.getCenter();
  Vector2d B = pointB.getFunction(P.block<3,1>(3,0)) - optics.getCenter();
  return computeRhoTeta2K(A,B);
}  

MatrixXd Segment::getNaiveInteractionMatrix(const VectorXd & F) const{
  assert(F.rows()==4);
  IntrinsicCam optics = oeil->getIntrinsicParameters();
  double rho = F(0);
  double theta = F(1);
  double cosT = cos(theta);
  double sinT = sin(theta);
  double k_1 = F(2);
  double k_2 = F(3);
  double x_1 = rho * cosT - k_1 * sinT;
  double y_1 = rho * sinT + k_1 * cosT;
  double x_2 = rho * cosT - k_2 * sinT;
  double y_2 = rho * sinT + k_2 * cosT;
  Vector2d A(x_1, y_1);
  Vector2d B(x_2, y_2);
  
  MatrixXd JK = MatrixXd::Zero(1,4);
  JK <<          
        0,  -rho,
    -sinT,  cosT;
  MatrixXd LX = MatrixXd::Zero(4,6);
  LX.block<2,6>(0,0) = pointA.getNaiveInteractionMatrix(A + optics.getCenter());
  LX.block<2,6>(2,0) = pointB.getNaiveInteractionMatrix(B + optics.getCenter());
  MatrixXd J(2,6);
  J.noalias() = computeJacobianRhoTeta(A,B) * LX;
  MatrixXd L = MatrixXd::Zero(4,6);
  L.block<2,6>(0,0) = J;
  L.block<1,6>(2,0).noalias() =  JK.block<1,2>(0,0) * J;
  L.block<1,6>(2,0).noalias() += JK.block<1,2>(0,2) * LX.block<2,6>(0,0);
  L.block<1,6>(3,0).noalias() =  JK.block<1,2>(0,0) * J;
  L.block<1,6>(3,0).noalias() += JK.block<1,2>(0,2) * LX.block<2,6>(2,0);
  
  return L;
}


MatrixXd Segment::getInteractionMatrix(const VectorXd & P) const{
  assert(P.rows()==6);
  IntrinsicCam optics = oeil->getIntrinsicParameters();
  Vector2d A = pointA.getFunction(P.block<3,1>(0,0)) - optics.getCenter();
  Vector2d B = pointB.getFunction(P.block<3,1>(3,0)) - optics.getCenter();
  Vector2d line = computeRhoTeta(A,B);
  double rho = line(0);
  double theta = line(1);
  /*
  if(rho<0){
    rho = - rho;
    theta -= M_PI;
  }
  //*/
  MatrixXd JK = MatrixXd::Zero(1,4);
  JK <<           0,        -rho,
       -sin(theta),  cos(theta);
  MatrixXd LX = MatrixXd::Zero(4,6);
  LX.block<2,6>(0,0) = pointA.getInteractionMatrix(P.block<3,1>(0,0));
  LX.block<2,6>(2,0) = pointB.getInteractionMatrix(P.block<3,1>(3,0));
  MatrixXd J(2,6);
  J.noalias() = computeJacobianRhoTeta(A,B) * LX;
  MatrixXd L = MatrixXd::Zero(4,6);
  L.block<2,6>(0,0) = J;
  L.block<1,6>(2,0).noalias() =  JK.block<1,2>(0,0) * J;
  L.block<1,6>(2,0).noalias() += JK.block<1,2>(0,2) * LX.block<2,6>(0,0);
  L.block<1,6>(3,0).noalias() =  JK.block<1,2>(0,0) * J;
  L.block<1,6>(3,0).noalias() += JK.block<1,2>(0,2) * LX.block<2,6>(2,0);
  
  return L;
}

VectorXd Segment::getFunction() const{
  IntrinsicCam optics = oeil->getIntrinsicParameters();
  Vector2d A = pointA.getFunction() - optics.getCenter();
  Vector2d B = pointB.getFunction() - optics.getCenter();
  return computeRhoTeta2K(A,B);
}

MatrixXd Segment::getInteractionMatrix() const{
  IntrinsicCam optics = oeil->getIntrinsicParameters();
  Vector2d A = pointA.getFunction() - optics.getCenter();
  Vector2d B = pointB.getFunction() - optics.getCenter();
  Vector2d line = computeRhoTeta(A,B);
  double rho = line(0);
  double theta = line(1);
  /*
  if(rho<0){
    rho = - rho;
    theta -= M_PI;
  }
  //*/
  MatrixXd JK = MatrixXd::Zero(1,4);
  JK <<           0,        -rho,
       -sin(theta),  cos(theta);
  MatrixXd LX = MatrixXd::Zero(4,6);
  LX.block<2,6>(0,0) = pointA.getInteractionMatrix();
  LX.block<2,6>(2,0) = pointB.getInteractionMatrix();
  MatrixXd J(2,6);
  J.noalias() = computeJacobianRhoTeta(A,B) * LX;
  MatrixXd L = MatrixXd::Zero(4,6);
  L.block<2,6>(0,0) = J;
  L.block<1,6>(2,0).noalias() =  JK.block<1,2>(0,0) * J;
  L.block<1,6>(2,0).noalias() += JK.block<1,2>(0,2) * LX.block<2,6>(0,0);
  L.block<1,6>(3,0).noalias() =  JK.block<1,2>(0,0) * J;
  L.block<1,6>(3,0).noalias() += JK.block<1,2>(0,2) * LX.block<2,6>(2,0);
  return L;
}

VectorXd Segment::getError(const VectorXd & S, const VectorXd & R) const{
  VectorXd E = S - R;
  E.block<2,1>(0,0) = computeError(S.block<2,1>(0,0),R.block<2,1>(0,0));
  return E;
}

MatrixXd Segment::getJacobianError(const VectorXd & S, const VectorXd & R) const{
  MatrixXd J = MatrixXd::Identity(4,4);
  J.block<2,2>(0,0) = computeJError(S.block<2,1>(0,0),R.block<2,1>(0,0));
  return J;
}

MatrixXd Segment::getJacobianReferenceError(const VectorXd & S, const VectorXd & R) const{
  MatrixXd J = -MatrixXd::Identity(4,4);
  J.block<2,2>(0,0) = computeJRefError(S.block<2,1>(0,0),R.block<2,1>(0,0));
  return J;
}

void Segment::draw(CVEcran * const vue, cv::Scalar couleur) const{
  drawFeat(getFunction(),vue,couleur);
}

void Segment::drawFeat(const VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const{
  dessinerDroite(oeil,value.block<2,1>(0,0),vue,couleur);
  double rho = value(0);
  double theta = value(1);
  double cosT = cos(theta);
  double sinT = sin(theta);
  double k = value(2);
  double x = rho * cosT - k * sinT;
  double y = rho * sinT + k * cosT;
  VectorXd val = VectorXd::Zero(2);
  val(0) = x;
  val(1) = y;
  IntrinsicCam optics = oeil->getIntrinsicParameters();
  val = val + optics.getCenter();
  Vector2d I = val.block<2,1>(0,0);
  I = vue->getPoint(I);
  int a,b;
  a = static_cast<int>(round(I(0)));
  b = static_cast<int>(round(I(1)));
  cv::circle(vue->getImage(),cv::Point(a,b),2,couleur,-1);

  k = value(3);
  x = rho * cosT - k * sinT;
  y = rho * sinT + k * cosT;
  val(0) = x;
  val(1) = y;
  val = val + optics.getCenter();
  I = val.block<2,1>(0,0);
  I = vue->getPoint(I);
  a = static_cast<int>(round(I(0)));
  b = static_cast<int>(round(I(1)));
  cv::circle(vue->getImage(),cv::Point(a,b),2,couleur,-1);

  // Dessin de la flèche

  int pa = a, pb = b;
  double lx = 0.04 * oeil->getAlphaU();
  double ly = 0.02 * oeil->getAlphaU();
  
  x = rho * cosT - k * sinT + ly * cosT + lx * sinT;
  y = rho * sinT + k * cosT + ly * sinT - lx * cosT;
  val(0) = x;
  val(1) = y;
  val = val + optics.getCenter();
  I = val.block<2,1>(0,0);
  I = vue->getPoint(I);
  a = static_cast<int>(round(I(0)));
  b = static_cast<int>(round(I(1)));
  cv::line(vue->getImage(),cv::Point(pa,pb),cv::Point(a,b),couleur,1,8);
  
  x = rho * cosT - k * sinT - ly * cosT + lx * sinT;
  y = rho * sinT + k * cosT - ly * sinT - lx * cosT;
  val(0) = x;
  val(1) = y;
  val = val + optics.getCenter();
  I = val.block<2,1>(0,0);
  I = vue->getPoint(I);
  a = static_cast<int>(round(I(0)));
  b = static_cast<int>(round(I(1)));
  cv::line(vue->getImage(),cv::Point(pa,pb),cv::Point(a,b),couleur,1,8);
  
}
  

