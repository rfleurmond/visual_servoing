#include "many-arm-holder-task.h"
#include <string>

using namespace std;
using namespace Eigen;

const int ManyArmHolderTask::CVperiode = 1;

BaseArm::BaseArm():
  theArm(NULL),
  theShoulder()
{

}

MovingEye::MovingEye():
  eye(NULL),
  arm(NULL),
  ecran(NULL),
  degres(6),
  fixation()
{

}

MovingFeature::MovingFeature():
  arm(NULL),
  degres(6),
  couleur(cv::Scalar(0,0,0)),
  teinte(cv::Scalar(128,128,128)),
  fixation()
{

}

CamFeat::CamFeat():
  eye(NULL),
  target(NULL),
  ref(NULL),
  hasRef(false),
  K(VectorXd::Zero(1)),
  C(MatrixXd::Zero(1,1))
{

}

CameraTask::CameraTask():
  eye(NULL),
  task(NULL)
{

}


ManyArmHolderTask::ManyArmHolderTask(bool show):
  AbstractVisualTask(),
  shouldErase(true),
  periode(0.05),
  numFeats(0),
  numCams(0),
  numArms(0),
  showDisplays(show),
  updateFonction(false),
  updateJacobien(false),
  story(new SaveTime()),
  allArms(std::list<BaseArm>()),
  allCameras(std::list<MovingEye>()),
  allFeatures(std::map<VisualFeature *, MovingFeature>()),
  allPairs(std::list<CamFeat *>()),
  allTasks(std::list<CameraTask>())
{
  dimOutput = 0;
  dimInput = 0;
  assert(story!=NULL);
}


ManyArmHolderTask::~ManyArmHolderTask(){
  delete story;
  list<CamFeat *>::const_iterator it;
  CamFeat * cm = NULL;
  for(it = allPairs.begin(); it!=allPairs.end();it++){
    cm = * it;
    delete cm;
  }
  list<MovingEye>::const_iterator itc;
  for(itc = allCameras.begin(); itc!=allCameras.end();itc++){
    if((*itc).ecran!=NULL){
      (*itc).ecran->fermerFenetre();
      if((*itc).eye->getDisplay()==NULL)
	delete (*itc).ecran;
    }
  }
  // cv::destroyAllWindows();
}


void ManyArmHolderTask::addArm(InverseKinematicModel * arm, const Repere & shoulder){
  assert(arm!=NULL);
  BaseArm nouveau;
  nouveau.theArm = arm;
  nouveau.theShoulder = shoulder;
  dimInput+=arm->getTotalDegres();
  etat_q = VectorXd::Zero(dimInput);
  allArms.push_back(nouveau);
  numArms++;
}

void ManyArmHolderTask::addCamera(Camera * cam, InverseKinematicModel * arm, 
				  const Repere & f, int deg){
  assert(cam!=NULL);
  MovingEye nouvelle;
  nouvelle.eye = cam;
  nouvelle.arm = arm;
  nouvelle.fixation = f;
  if(arm==NULL){
  cam->setRepere(f);
  }
  nouvelle.degres = deg;

  if(showDisplays){  
    // créer un flux de sortie
    std::ostringstream oss;
    // écrire un nombre dans le flux
    oss << "Camera # ";
    oss << numCams+1;
    // récupérer une chaîne de caractères
    std::string result = oss.str();
    //gestion de l'affichage
    if(cam->getDisplay()==NULL){
      nouvelle.ecran = new CVEcran(result);
      nouvelle.ecran->ouvrirFenetre();
      nouvelle.ecran->nettoyer();
      nouvelle.ecran->setDimensions(640,480);
      nouvelle.ecran->setOptique(cam->getIntrinsicParameters());
    }
    else{
      nouvelle.ecran = cam->getDisplay();
    }
   
  }

  allCameras.push_back(nouvelle);
  numCams++;
}


void ManyArmHolderTask::addCamera(Camera * cam, InverseKinematicModel * arm,
				  const Repere & f, int deg, string nom){
  assert(cam!=NULL);
  MovingEye nouvelle;
  nouvelle.eye = cam;
  nouvelle.arm = arm;
  nouvelle.fixation = f;
  if(arm==NULL){
  cam->setRepere(f);
  }
  nouvelle.degres = deg;
  
  if(showDisplays){  
    if(cam->getDisplay()==NULL){
      nouvelle.ecran = new CVEcran(nom);
      nouvelle.ecran->ouvrirFenetre();
      nouvelle.ecran->nettoyer();
      nouvelle.ecran->setDimensions(640,480);
      nouvelle.ecran->setOptique(cam->getIntrinsicParameters());
    }
    else{
      nouvelle.ecran = cam->getDisplay();
    }
    
  }
  allCameras.push_back(nouvelle);
  numCams++;
}

void ManyArmHolderTask::addVisualTarget(
					VisualFeature * target, InverseKinematicModel * arm, 
					const Repere & f, int deg){
  assert(target!=NULL);
  MovingFeature marqueur; 
  marqueur.arm = arm;
  marqueur.fixation = f;
  if(arm==NULL){
    target->setSituation(f);
  }
  marqueur.degres = deg;
  numFeats++;
  
  // Gestion de la couleur des features
  marqueur.couleur = Palette::getLastColor();

  marqueur.teinte = Palette::getLightColor();
  
  allFeatures[target]=marqueur;
  
}

void ManyArmHolderTask::makeTargetSeen(VisualFeature * target, Camera * cam){
  assert(target!=NULL);
  assert(cam!=NULL);
  CamFeat * regard = new CamFeat;
  regard->eye = cam;
  regard->target = target;
  regard->ref = NULL;
  regard->hasRef = false;
  allPairs.push_back(regard);
}

void ManyArmHolderTask::giveTargetReference(
					    Camera * cam, VisualFeature * target, 
					    VisualReference * vRef, bool ref){
  list<CamFeat *>::const_iterator it;
  CamFeat * mf = NULL;
  it = allPairs.begin();
  while(it!=allPairs.end()){
    mf = *it;
    if(mf->target==target && mf->eye == cam){
      if(ref){assert(vRef!=NULL);}
      mf->ref = vRef;
      mf->hasRef=ref;
      int n = target->getDimensionOutput();
      mf->K = VectorXd::Zero(n);
      mf->C = MatrixXd::Identity(n,n);
      if(ref){dimOutput+=n;}
      break;
    }
    it++;
  }
}

void ManyArmHolderTask::giveKandCmatrix(Camera * cam, VisualFeature * target, 
					const MatrixXd & C, const VectorXd & K){
  list<CamFeat *>::const_iterator it;
  CamFeat * mf = NULL;
  it = allPairs.begin();
  while(it!=allPairs.end()){
    mf = *it;
    if(mf->target==target && mf->eye == cam){
      int n = target->getDimensionOutput();
      assert(K.rows()==n);
      assert(C.cols()==n);
      mf->K = K;
      mf->C = C; 
      dimOutput+= static_cast<int>(C.rows())-n;
      break;
    }
    it++;
  }
}

void ManyArmHolderTask::addOtherTask(Camera * cam, RelativeTask * t){
  assert(t!=NULL);
  assert(cam!=NULL);
  CameraTask nouveau;
  nouveau.eye = cam;
  nouveau.task = t;
  dimOutput+=t->getDimension();
  allTasks.push_back(nouveau);
}

  

void ManyArmHolderTask::keepVisualTrace(bool val){
  shouldErase = val==false;
}


void ManyArmHolderTask::drawVisualFeatures() const{
  list<MovingEye>::const_iterator itEye;
  list<CamFeat *>::const_iterator itView;
  list<BaseArm>::const_iterator itArm;
  
  int l = 20;
  //Recuperer l' affichage
  for(itEye=allCameras.begin();itEye!=allCameras.end();itEye++){
    MovingEye me = *itEye;
    for(itView = allPairs.begin();itView!=allPairs.end();itView++){
      CamFeat * cf = *itView;
      if(cf->eye == me.eye){
	cf->target->attachToCamera(me.eye);
	const MovingFeature & mf = allFeatures.at(cf->target);
	if(allFeatures.find(cf->target)==allFeatures.end()){
	  cout<<"Probleme avec Moving Feature"<<endl;
	}
	    
	if(cf->target->itCanBeSeen()){
	  //Dessiner l' objet sur l'ecran
	  if(showDisplays){  
	    cf->target->draw(me.ecran, mf.couleur);
	  }
	  cf->target->useTracker();

	}
	else{
	  putText(me.ecran->getImage(), "target not visible ", cv::Point(50,l+=20), 
		  cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, mf.couleur, 1, CV_AA);
	}
	
      }
    }

    if(showDisplays){  
      me.ecran->affichage();
      if(shouldErase){
	if(me.eye->getDisplay()==NULL)
	  {
	    cv::waitKey(CVperiode);
      	    me.ecran->nettoyer();
	  }
      }
    }

  }

}

VectorXd ManyArmHolderTask::fonction(const VectorXd & q) const{
  assert(q.rows()==dimInput);
  if(story->hasAlreadyCompute(q)){
    if(updateFonction){
      story->hasStory = true;
      story->last_q = q;
      story->last_value = computeFonction(q);
      story->last_jacobian = computeJacobien(q);
    }
  }
  else{
    story->hasStory = true;
    story->last_q = q;
    story->last_value = computeFonction(q);
    story->last_jacobian = computeJacobien(q);
  }
  return story->last_value;
}


VectorXd ManyArmHolderTask::computeFonction(const VectorXd & q) const{
  assert(q.rows()==dimInput);
  int curDim = 0;
  int totDim = 0;
  VectorXd output = VectorXd::Zero(dimOutput);
  VectorXd value;
  VectorXd sStar;
  list<MovingEye>::const_iterator itEye;
  list<CamFeat *>::const_iterator itView;
  list<BaseArm>::const_iterator itArm;

  int ligneFeature = 10;
  
  //Recuperer l' affichage
  for(itEye=allCameras.begin();itEye!=allCameras.end();itEye++){
    MovingEye me = *itEye;
    for(itView = allPairs.begin();itView!=allPairs.end();itView++){
      CamFeat * cf = *itView;
      if(cf->eye == me.eye){
	cf->target->attachToCamera(me.eye);
	const MovingFeature & mf = allFeatures.at(cf->target);
	if(allFeatures.find(cf->target)==allFeatures.end()){
	  cout<<"Probleme avec Moving Feature"<<endl;
	}
	if(cf->target->itCanBeSeen()){
	  //Dessiner l' objet sur l'ecran
	  if(showDisplays){  
	    cf->target->draw(me.ecran, mf.couleur);
	  }
	  cf->target->useTracker();
	}
	else{
	  putText(me.ecran->getImage(), "target not visible ", cv::Point(50,ligneFeature+=20), 
		  cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, mf.couleur, 1, CV_AA);
	}
	if(cf->hasRef){
	  assert(cf->target->itCanBeSeen());
	  if(cf->ref->getFeatures()!=NULL){
	    cf->ref->getFeatures()->attachToCamera(me.eye);
	    assert(cf->ref->getFeatures()->itCanBeSeen());
	  }
	  sStar= cf->ref->getReference();
	  curDim = static_cast<int>(cf->C.rows());
	  if(cf->ref->getFeatures()==NULL){
	    //dessiner la reference
	    if(showDisplays){  
	      cf->target->drawFeat(sStar, me.ecran, mf.teinte);
	    }
	  }
	  value = cf->C * (cf->target->getErrorBetweenMeAnd(sStar)- cf->K);
	  output.block(totDim,0,curDim,1) = value;
	  totDim+=curDim;
	}
      }
    }

    if(showDisplays){  
      me.ecran->affichage();
      if(shouldErase){
	if(me.eye->getDisplay()==NULL){
	  cv::waitKey(CVperiode);
	  me.ecran->nettoyer();
	}
      }
    }

  }

  list<CameraTask>::const_iterator itTask;
  for(itTask = allTasks.begin();itTask!=allTasks.end();itTask++){
    CameraTask ct = *itTask;
    map<VisualFeature *, MovingFeature>::const_iterator it;
    // pour parcourir toutes les paires de la map
    for(it=allFeatures.begin(); it!=allFeatures.end();++it){
      it->first->attachToCamera(ct.eye);
    }
    curDim = ct.task->getDimension();
    output.block(totDim,0,curDim,1) = ct.task->getValue();
    totDim+=curDim;
  }
  return output;
}


void ManyArmHolderTask::setState(const VectorXd & q){
  assert(q.rows()==dimInput);
 
  /*
  Eigen::VectorXd erreur(etat_q -q);
  if(erreur.norm()<1e-6 && q.norm()> 1e-10)
    return;
  */
  
  etat_q = q;
  int curDim = 0;
  int totDim = 0;
  list<MovingEye>::const_iterator itEye;
  list<BaseArm>::const_iterator itArm;
  VectorXd qArm;
  Repere se3;

  //Calculer la nouvelle positions des bras
  for(itArm=allArms.begin();itArm!=allArms.end();itArm++){
    BaseArm ba = *itArm;
    curDim = ba.theArm->getTotalDegres();
    qArm = q.block(totDim,0,curDim,1);
    totDim += curDim;

    for(itEye=allCameras.begin();itEye!=allCameras.end();itEye++){
      MovingEye me = *itEye;
      if(me.arm == ba.theArm){
	assert(me.degres<=curDim);
	se3 = ba.theShoulder * ba.theArm->getSituation(qArm, me.degres);
	se3 = se3 * me.fixation;
	me.eye->setRepere(se3);
      }
      
    }
    
    // pour parcourir toutes les paires de la map
    for(map<VisualFeature *, MovingFeature>::const_iterator it=allFeatures.begin(); it!=allFeatures.end();++it){
      VisualFeature * feat = it->first;
      MovingFeature mf = it->second;
      if(mf.arm == ba.theArm){
	assert(mf.degres<=curDim);
	se3 = ba.theShoulder * ba.theArm->getSituation(qArm, mf.degres);
	se3 = se3 * mf.fixation;
	feat->setSituation(se3);
      }
      
    }
  }
}


MatrixXd ManyArmHolderTask::jacobien(const VectorXd & q) const{
  assert(q.rows()==dimInput);
  if(story->hasAlreadyCompute(q)){
    if(updateJacobien){
      story->hasStory = true;
      story->last_q = q;
      story->last_value = computeFonction(q);
      story->last_jacobian = computeJacobien(q);
    }
  }
  else{
    story->hasStory = true;
    story->last_q = q;
    story->last_value = computeFonction(q);
    story->last_jacobian = computeJacobien(q);
  }
  return story->last_jacobian;
}

MatrixXd ManyArmHolderTask::computeJacobien(const VectorXd & q) const{
  int curDim = 0;
  int totDim = 0;
  int curQ = 0;
  int totQ = 0;
  MatrixXd jacobienne = MatrixXd::Zero(dimOutput,dimInput);
  VectorXd sStar,qArm;
  MatrixXd JS,L,WC,WO,JC,JO,JR,ROT;
  Repere se3;
  list<MovingEye>::const_iterator itEye;
  list<CamFeat *>::const_iterator itView;
  list<BaseArm>::const_iterator itArm;

  for(itEye=allCameras.begin();itEye!=allCameras.end();itEye++){
    MovingEye me = *itEye;
    for(itView = allPairs.begin();itView!=allPairs.end();itView++){
      CamFeat * cf = *itView;
      if(cf->eye == me.eye){
	cf->target->attachToCamera(me.eye);
	const MovingFeature & mf = allFeatures.at(cf->target);
	if(cf->hasRef){
	  assert(cf->target->itCanBeSeen());
	  if(cf->ref->getFeatures()!=NULL){
	    cf->ref->getFeatures()->attachToCamera(me.eye);
	    assert(cf->ref->getFeatures()->itCanBeSeen());
	  }
	  sStar= cf->ref->getReference();
	  curDim = static_cast<int>(cf->C.rows());
	  curQ = totQ = 0;
	  JS = MatrixXd::Zero(curDim,dimInput);
	  JR = MatrixXd::Zero(curDim,dimInput);
	  L.noalias() = cf->C * cf->target->getJacobianError(sStar) * cf->target->getInteractionMatrix();
	  WC = WO = MatrixXd::Identity(6,6);
	  JC = JO = MatrixXd::Zero(6,dimInput);
	  for(itArm=allArms.begin();itArm!=allArms.end();itArm++){
	    BaseArm ba = *itArm;
	    curQ = ba.theArm->getTotalDegres();
	    qArm = q.block(totQ,0,curQ,1);
	    if(ba.theArm==me.arm){
	      assert(me.degres<=curQ);
	      se3 = ba.theArm->getSituation(qArm,me.degres);
	      ROT = se3.getRotation().getMatrix().transpose();
	      JC.block(0,totQ,6,curQ) = ba.theArm->getKinematicJacobian(qArm,me.degres);
	      JC.block(0,totQ,3,curQ) = ROT * JC.block(0,totQ,3,curQ);
	      JC.block(3,totQ,3,curQ) = ROT * JC.block(3,totQ,3,curQ);
	      se3 = ba.theShoulder * se3;
	      WC.noalias() = me.eye->getRepere().getInversKineticMatrix() * se3.getKineticMatrix();
	    }
	    if(ba.theArm==mf.arm){
	      assert(mf.degres<=curQ);
	      se3 = ba.theArm->getSituation(qArm,mf.degres);
	      ROT = se3.getRotation().getMatrix().transpose();
	      JO.block(0,totQ,6,curQ) = ba.theArm->getKinematicJacobian(qArm,mf.degres);
	      JO.block(0,totQ,3,curQ) = ROT * JO.block(0,totQ,3,curQ);
	      JO.block(3,totQ,3,curQ) = ROT * JO.block(3,totQ,3,curQ);
	      se3 = ba.theShoulder * se3;
	      WO.noalias() = me.eye->getRepere().getInversKineticMatrix() * se3.getKineticMatrix();
	    }
	    totQ += curQ;
	  }
	  JS.noalias() = L * (WC * JC - WO * JO);
	  if(cf->ref->getFeatures()!=NULL){
	    VisualFeature * feat = cf->ref->getFeatures();
	    const MovingFeature & mf2 = allFeatures.at(feat);
	    L = cf->C * cf->target->getJacobianReferenceError(sStar) * feat->getInteractionMatrix();
	    WC = WO = MatrixXd::Identity(6,6);
	    JC = JO = MatrixXd::Zero(6,dimInput);
	    curQ = totQ = 0;
	    for(itArm=allArms.begin();itArm!=allArms.end();itArm++){
	      BaseArm ba = *itArm;
	      curQ = ba.theArm->getTotalDegres();
	      qArm = q.block(totQ,0,curQ,1);
	      if(ba.theArm==me.arm){
		assert(me.degres<=curQ);
		se3 = ba.theArm->getSituation(qArm,me.degres);
		ROT = se3.getRotation().getMatrix().transpose();
		JC.block(0,totQ,6,curQ) = ba.theArm->getKinematicJacobian(qArm,me.degres);
		JC.block(0,totQ,3,curQ) = ROT * JC.block(0,totQ,3,curQ);
		JC.block(3,totQ,3,curQ) = ROT * JC.block(3,totQ,3,curQ);
		se3 = ba.theShoulder * se3;
		WC.noalias() = me.eye->getRepere().getInversKineticMatrix()*se3.getKineticMatrix();
	      }
	      if(ba.theArm==mf2.arm){
		assert(mf2.degres<=curQ);
		se3 = mf2.arm->getSituation(qArm,mf2.degres);
		ROT = se3.getRotation().getMatrix().transpose();
		JO.block(0,totQ,6,curQ) = ba.theArm->getKinematicJacobian(qArm,mf2.degres);
		JO.block(0,totQ,3,curQ) = ROT * JO.block(0,totQ,3,curQ);
		JO.block(3,totQ,3,curQ) = ROT * JO.block(3,totQ,3,curQ);
		se3 = ba.theShoulder * se3;
		WO.noalias() = me.eye->getRepere().getInversKineticMatrix()*se3.getKineticMatrix();
	      }
	      totQ += curQ;
	    }
	    JR.noalias() = L * (WC * JC - WO * JO);
	  }
	  jacobienne.block(totDim,0,curDim,dimInput).noalias() = JS + JR;
	  totDim+=curDim;
	}
      }
    }
  }

  list<CameraTask>::const_iterator itTask;
  map<VisualFeature *, MovingFeature>::const_iterator it;
  VisualFeature * feat = NULL;
  MovingFeature mf;
  MatrixXd JT,LJ,JI;
  MovingEye me;
  for(itTask = allTasks.begin();itTask!=allTasks.end();itTask++){
    CameraTask ct = *itTask;
    curDim = ct.task->getDimension();
    JT = MatrixXd::Zero(curDim,dimInput);
    // pour parcourir toutes les paires de la map
    for(it=allFeatures.begin(); it!=allFeatures.end();++it){
      feat = it->first;
      mf = it->second;
      if(ct.task->dependOf(feat)){
	feat->attachToCamera(ct.eye);
	for(itEye=allCameras.begin();itEye!=allCameras.end();itEye++){
	  me = *itEye;
	  if(me.eye==ct.eye){
	    break;
	  }
	}
	curQ = totQ = 0;
	JS = MatrixXd::Zero(curDim,dimInput);
	L = feat->getInteractionMatrix();
	WC = WO = MatrixXd::Identity(6,6);
	JC = JO = MatrixXd::Zero(6,dimInput);
	for(itArm=allArms.begin();itArm!=allArms.end();itArm++){
	  BaseArm ba = *itArm;
	  curQ = ba.theArm->getTotalDegres();
	  qArm = q.block(totQ,0,curQ,1);
	  if(ba.theArm==me.arm){
	    assert(me.degres<=curQ);
	    se3 = ba.theArm->getSituation(qArm,me.degres);
	    ROT = se3.getRotation().getMatrix().transpose();
	    JC.block(0,totQ,6,curQ) = ba.theArm->getKinematicJacobian(qArm,me.degres);
	    JC.block(0,totQ,3,curQ) = ROT * JC.block(0,totQ,3,curQ);
	    JC.block(3,totQ,3,curQ) = ROT * JC.block(3,totQ,3,curQ);
	    se3 = ba.theShoulder * se3;
	    WC.noalias() = me.eye->getRepere().getInversKineticMatrix() * se3.getKineticMatrix();
	  }
	  if(ba.theArm==mf.arm){
	    assert(mf.degres<=curQ);
	    se3 = ba.theArm->getSituation(qArm,mf.degres);
	    ROT = se3.getRotation().getMatrix().transpose();
	    JO.block(0,totQ,6,curQ) = ba.theArm->getKinematicJacobian(qArm,mf.degres);
	    JO.block(0,totQ,3,curQ) = ROT * JO.block(0,totQ,3,curQ);
	    JO.block(3,totQ,3,curQ) = ROT * JO.block(3,totQ,3,curQ);
	    se3 = ba.theShoulder * se3;
	    WO.noalias() = me.eye->getRepere().getInversKineticMatrix() * se3.getKineticMatrix();
	  }
	  totQ += curQ;
	}
	JS.noalias() = L * (WC * JC - WO * JO);
	JI.noalias() = ct.task->getJacobian(feat)*JS;
	JT+=JI;
      }
      
    }
    jacobienne.block(totDim,0,curDim,dimInput) = JT;
    totDim+=curDim;
  }

  return jacobienne;
}

double ManyArmHolderTask::getPeriod() const{
  return periode;
}

void ManyArmHolderTask::setPeriod(double dt){
  periode = dt;
}

bool ManyArmHolderTask::hasChanged() const{
  return false;
}
