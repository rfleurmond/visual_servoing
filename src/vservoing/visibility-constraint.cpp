#include "visibility-constraint.h"

using namespace Eigen;
using namespace std;

ConstraintFeatures::ConstraintFeatures():
  RelativeTask(),
  coeffMin(0.05),
  coeffMax(0),
  alpha(1.0),
  max(10),
  dimension(0),
  carte(map<VisualFeature *, bounds>()),
  liste(list<VisualFeature *>())
{

}

ConstraintFeatures::ConstraintFeatures(VisualFeature *f, const VectorXd & qmin, const VectorXd & qmax):
  RelativeTask(),
  coeffMin(0.05),
  coeffMax(0),
  alpha(1.0),
  max(10),
  dimension(0),
  carte(map<VisualFeature *, bounds>()),
  liste(list<VisualFeature *>())
{
  addFeatures(f,qmin,qmax);
}

void ConstraintFeatures::setCoeffMax(double d){
  coeffMax = d;
}


void ConstraintFeatures::setCoeffMin(double d){
  coeffMin = d;
}
  
void ConstraintFeatures::setAlpha(double a){
  alpha = a;
}

void ConstraintFeatures::setMax(double m){
  max = m;
}


void ConstraintFeatures::addFeatures(VisualFeature *f,const VectorXd & qmin, const VectorXd & qmax){
  assert(f!=NULL);

  long int dim = qmin.rows();
  assert(f->getDimensionOutput() == dim);
  assert(qmax.rows() == dim);
  
  bounds limites;
  limites.acti = MatrixXd::Identity(dim,dim);
  limites.sMin = qmin;
  limites.sMax = qmax;
  dimension += static_cast<int>(dim);

  carte[f] = limites;
  liste.push_back(f);

}

void ConstraintFeatures::addFeatures(
				     VisualFeature *f,
				     const VectorXd & qmin, const VectorXd & qmax, 
				     const MatrixXd & a){
  assert(f!=NULL);

  long int dim = f->getDimensionOutput();
  assert(a.cols() == dim);
  dim = a.rows();
  assert(qmax.rows() == dim);
  assert(qmin.rows() == dim);
  
  bounds limites;
  limites.acti = a;
  limites.sMin = qmin;
  limites.sMax = qmax;
  dimension += static_cast<int>(dim);

  carte[f] = limites;
  liste.push_back(f);
}
  
int ConstraintFeatures::getDimension() const{
  return dimension;
}


VectorXd ConstraintFeatures::getValue() const{
  VectorXd output = VectorXd::Zero(dimension);
  VectorXd part;
  long int tot = 0;
  long int dim = 0;
  VisualFeature * feat = NULL;
  std::list<VisualFeature *>::const_iterator 
    lit (liste.begin()), 
    lend(liste.end()); 
  for(;lit!=lend;++lit){  
    feat = *lit;
    part = getValue(feat);
    dim = part.rows();
    output.block(tot,0,dim,1) = part;
    tot += dim;
  }
  return output;
}

VectorXd ConstraintFeatures::getValue(VisualFeature * feat) const{
  assert(feat!=NULL);
  Repulsive cost(coeffMin,coeffMax,alpha);
  cost.setCeil(max);
  const bounds & limites = carte.at(feat);
  VectorXd q = feat->getFunction();
  long int dim = limites.acti.rows();
  VectorXd output= VectorXd::Zero(dim);
  VectorXd qq;
  qq =  limites.acti * q;
  double x = 0;
  for(int i = 0; i <dim;i++){
    cost.setBounds(limites.sMin(i),limites.sMax(i));
    x = qq(i);
    output(i) = cost(x);
  }
  return output;
}

bool ConstraintFeatures::dependOf(VisualFeature * feat) const{
  if(feat==NULL) return false;
  return carte.count(feat)>0;
}

MatrixXd ConstraintFeatures::getJacobian(VisualFeature * feat) const{
  assert(feat!=NULL);
  Repulsive cost(coeffMin,coeffMax,alpha);
  cost.setCeil(max);
  const bounds & limites = carte.at(feat);
  VectorXd q = feat->getFunction();
  long int dim = limites.acti.rows();
  MatrixXd jacobien = MatrixXd::Zero(dim,dim);
  VectorXd qq;
  qq = limites.acti * q;
  double x = 0;
  for(int i = 0; i <dim;i++){
    x = qq(i);
    cost.setBounds(limites.sMin(i),limites.sMax(i));
    jacobien(i,i) = cost.jacobien(x);
  }
  jacobien = jacobien * limites. acti;
  return jacobien;
}

