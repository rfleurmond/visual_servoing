#include "feature-estimator.h"

using namespace Eigen;


FeatureEstimator::FeatureEstimator(VisualFeature * model, VTracker * tracker):
  VisualFeature(),
  hasLog(false),
  access(false),
  fichier(),
  printer(),
  traceur(tracker),
  modele(model),
  hasInfluence(false),
  estimation(VectorXd::Zero(1)),
  nUpdates(0),
  seuil(-1),
  deadline(600.0),
  blinkColor(Palette::getLastColor()),
  properTime(0.0)
{
  frame = modele->getSituation();
  oeil = traceur->getEye();
}
  

void FeatureEstimator::setLogFile(std::string file){
  fichier = file;
  printer.open(fichier.c_str());
  hasLog = true;
  if(printer){
    access = true;
  }
}

void FeatureEstimator::writeLog(){
  double heure = MyClock::getTime();
  if(hasLog && access)
    {
      if(heure>properTime)
	{
	  printer << heure << " " << estimation.transpose()<<std::endl;
	  properTime = heure;
	}
      //printer << nUpdates << " " << estimation.transpose()<<std::endl;
    }
}
 
void FeatureEstimator::closeLogFile(){
  if(hasLog && access){
    printer.close();
    access = false;
  }
}

void FeatureEstimator::useEstimation(bool b){
  hasInfluence = b;
}

void FeatureEstimator::setThreshold(int s){
  seuil = s;
}

void FeatureEstimator::setDeadline(double t){
  deadline = t;
}

int FeatureEstimator::getDimensionOutput() const{
  return traceur->getDimensionOutput();
}

int FeatureEstimator::getDimensionState() const{
  return modele->getDimensionState();
}


VectorXd FeatureEstimator::getEstimation() const{
  return estimation;
}

void FeatureEstimator::useTracker(){
  nUpdates++;
  if(traceur->isLost()==false)
    {
      //if(nUpdates%3==1)
      {
	computeEstimation();
	
      }
    }  

  if(hasInfluence && traceur->isLost()){
    //if(nUpdates%2==0) 
    traceur->findFeatures(modele->getFunction(estimation));
	
  }
  this->writeLog();
  if
    (
     nUpdates==seuil ||
     (MyClock::getTime()>=deadline && hasInfluence ==false)
     )
    {
      std::cout << "Activation estimation "<<std::endl; 
      useEstimation(true);
    }
}

bool FeatureEstimator::itCanBeSeen() const{
  if(hasInfluence || traceur->isLost()){
    return modele->itCanBeSeen();
  }
  else
    return traceur->itCanBeSeen();
}
  
VectorXd FeatureEstimator::getFunction(const VectorXd & p) const{
  return modele->getFunction(p);
}

VectorXd FeatureEstimator::getFunction() const{
  VectorXd F = traceur->getFunction();
  if(hasInfluence || traceur->isLost()){
    return modele->getFunction(estimation);
  }
  else
    return F;
}


MatrixXd FeatureEstimator::getNaiveInteractionMatrix(const VectorXd & F) const{
  return modele->getNaiveInteractionMatrix(F);
}


MatrixXd FeatureEstimator::getInteractionMatrix(const VectorXd & P) const{
  return modele->getInteractionMatrix(P);
}
  
MatrixXd FeatureEstimator::getInteractionMatrix() const{
  if(hasInfluence  || traceur->isLost()){
    return modele->getInteractionMatrix(estimation);
  }
  else{
    //return modele->getNaiveInteractionMatrix(traceur->getFunction());
    return modele->getInteractionMatrix();
  }
}

VectorXd FeatureEstimator::getErrorBetweenMeAnd(const VectorXd & autre) const{
  return modele->getError(getFunction(),autre);
}
 
MatrixXd FeatureEstimator::getJacobianError(const VectorXd & autre) const{
  return modele->getJacobianError(getFunction(),autre);
}

MatrixXd FeatureEstimator::getJacobianReferenceError(const VectorXd & autre) const{
  return modele->getJacobianReferenceError(getFunction(),autre);
}

VectorXd FeatureEstimator::getError(const VectorXd & S, const VectorXd & autre) const{
  return modele->getError(S,autre);
}
 
MatrixXd FeatureEstimator::getJacobianError(const VectorXd & S, const VectorXd & autre) const{
  return modele->getJacobianError(S,autre);
}

MatrixXd FeatureEstimator::getJacobianReferenceError(const VectorXd & S, const VectorXd & autre) const{
  return modele->getJacobianReferenceError(S,autre);
}

void FeatureEstimator::draw(CVEcran * const vue, cv::Scalar couleur) const{
  //if(hasInfluence || traceur->isLost())
    {
      traceur->drawFeat(modele->getFunction(estimation),vue,blinkColor);
      //modele->drawFeat(modele->getFunction(estimation),vue,blinkColor);
    }
    //else
    //traceur->draw(vue,couleur);
    //if(traceur->isLost())
    //traceur->draw(vue,couleur);
}

void FeatureEstimator::drawFeat(const VectorXd & value,CVEcran * const vue, cv::Scalar couleur) const{
  traceur->drawFeat(value,vue,couleur);
}

void FeatureEstimator::setSituation(const Repere & r){
  frame = r;
  modele->setSituation(r);
}

void FeatureEstimator::attachToCamera(Camera * eye){
    assert(eye!=NULL);
    oeil = eye;
    traceur->attachToCamera(eye);
    modele->attachToCamera(eye);
}
