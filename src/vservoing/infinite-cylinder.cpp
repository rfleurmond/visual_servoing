#include "infinite-cylinder.h"

using namespace std;
using namespace Eigen;


Cylinder::Cylinder(double r):
  VisualFeature(),
  rayon(r),
  P(Vector3d::Zero()),
  U(Vector3d::UnitZ())
{
  assert(r>=0.5e-2);
}

Cylinder::Cylinder(double r, Vector3d UU):
  VisualFeature(),
  rayon(r),
  P(Vector3d::Zero()),
  U(UU.normalized())
{
  assert(r>=0.5e-2);
}

Cylinder::Cylinder(double r, Vector3d UU, Vector3d PP):
  VisualFeature(),
  rayon(r),
  P(PP),
  U(UU.normalized())
{
  assert(r>=0.5e-2);
}


int Cylinder::getDimensionState() const{
  return 7;
}

int Cylinder::getDimensionOutput() const{
  return 4;
}


bool Cylinder::itCanBeSeen() const{

  MatrixXd PRJ2 = MatrixXd::Identity(3,3) - U * U.transpose();
  Vector3d pos = oeil->getRepere().getDeplacement();
  
  pos = frame.getPointInME(pos);
  Vector3d X = PRJ2 * (pos - P);
  double d = X.norm();
  X.normalize();
  Vector3d Y = U.cross(X);
  
  if(d<=rayon){
    return false;
  }
  
  double cosA = rayon/d;
  double sinA = sqrt(1-cosA*cosA);
  Vector3d A,B,C,D;
  A = P + rayon * (cosA * X + sinA * Y);
  B = P + rayon * (cosA * X + sinA * Y) + U;
  C = P + rayon * (cosA * X - sinA * Y);
  D = P + rayon * (cosA * X - sinA * Y) + U;
  LineFeature l1(A,B);
  LineFeature l2(C,D);
  l1.setSituation(frame);
  l2.setSituation(frame);
  l1.attachToCamera(oeil);
  l2.attachToCamera(oeil);
  return l1.itCanBeSeen() && l2.itCanBeSeen();
}

VectorXd Cylinder::getFunction(const VectorXd & param) const{
  assert(param.rows()==7);
  double rr = param(0);
  if( !(rr>0)) cout << "R = "<< rr << " !!!! Really ??" <<endl;
  assert(rr > 0);
  Vector3d PP = param.block<3,1>(1,0);
  Vector3d UU = param.block<3,1>(4,0);
  return getFunction(rr,UU,PP);
}
VectorXd Cylinder::getFunction() const{
  return getFunction(rayon,U,P);
}

VectorXd Cylinder::getFunction(double rr, Vector3d UU, Vector3d PP) const{
  
  VectorXd output(VectorXd::Zero(4));
  
  MatrixXd PRJ2(MatrixXd::Identity(3,3));
  PRJ2.noalias() -= UU * UU.transpose();
  Vector3d pos = oeil->getRepere().getDeplacement();
  
  pos = frame.getPointInME(pos);
  Vector3d X = PRJ2 * (pos - PP);
  double d = X.norm();
  X.normalize();
  Vector3d Y = UU.cross(X);
  
  
  double cosA = rr/d;
  double sinA = sqrt(1-cosA*cosA);
  
  Vector3d A,B,C,D;
  A.noalias() = PP + rr * (cosA * X + sinA * Y);
  B.noalias() = PP + rr * (cosA * X + sinA * Y) + UU;
  C.noalias() = PP + rr * (cosA * X - sinA * Y);
  D.noalias() = PP + rr * (cosA * X - sinA * Y) + UU;
  
  LineFeature l1(A,B);
  LineFeature l2(C,D);
  l1.setSituation(frame);
  l2.setSituation(frame);
  l1.attachToCamera(oeil);
  l2.attachToCamera(oeil);
  output.block<2,1>(0,0) = l1.getFunction();
  output.block<2,1>(2,0) = l2.getFunction();
  return output;
}  

MatrixXd Cylinder::getNaiveInteractionMatrix(const VectorXd & F) const{
  assert(F.rows()==4);
  LineFeature l1(Vector3d::Zero(),Vector3d::UnitZ());
  l1.setSituation(frame);
  l1.attachToCamera(oeil);
  MatrixXd L = MatrixXd::Zero(4,6);
  L.block<2,6>(0,0) = l1.getNaiveInteractionMatrix(F.block<2,1>(0,0));
  L.block<2,6>(2,0) = l1.getNaiveInteractionMatrix(F.block<2,1>(2,0));
  return L;
}

MatrixXd Cylinder::getInteractionMatrix(const VectorXd & param) const{
  assert(param.rows()==7);
  double rr = param(0);
  assert(rr>0);
  Vector3d PP = param.block<3,1>(1,0);
  Vector3d UU = param.block<3,1>(4,0);
  return getInteractionMatrix(rr,UU,PP);
}

MatrixXd Cylinder::getInteractionMatrix() const{
  return getInteractionMatrix(rayon,U,P);
}

MatrixXd Cylinder::getInteractionMatrix(double rr, Vector3d UU, Vector3d PP) const{
  MatrixXd jacobien = MatrixXd::Zero(4,6);

  MatrixXd PRJ2 = MatrixXd::Identity(3,3) - UU * UU.transpose();
  Vector3d pos = oeil->getRepere().getDeplacement();
  
  pos = frame.getPointInME(pos);
  
  Vector3d X = PRJ2 * (pos - PP);
  double d = X.norm();
  X.normalize();
  
  Vector3d Y = UU.cross(X);
  
  double cosA = rr/d;
  double sinA = sqrt(1-cosA*cosA);
  
  Vector3d A,B,C,D;
  A = PP + rr * (cosA * X + sinA * Y);
  B = PP + rr * (cosA * X + sinA * Y) + UU;
  C = PP + rr * (cosA * X - sinA * Y);
  D = PP + rr * (cosA * X - sinA * Y) + UU;
  
  LineFeature l1(A,B);
  LineFeature l2(C,D);
  l1.setSituation(frame);
  l2.setSituation(frame);
  l1.attachToCamera(oeil);
  l2.attachToCamera(oeil);


  double derivX = - cosA * cosA;
  double derivY = cosA / sinA - cosA * sinA;
  
  Repere se3 = oeil->getRepere().inverse()*frame; // From cylinder to camera
  
  MatrixXd ROT = se3.getRotation().getMatrix(); // From cylinder to camera
  MatrixXd TOR = ROT.transpose(); // From camera to cylinder

  MatrixXd WI = se3.getKineticMatrix();
  MatrixXd IW = se3.getInversKineticMatrix();

  MatrixXd K6 = MatrixXd::Identity(6,6);
  K6(5,5) = 0;
  K6(2,2) = 0;
  
  Matrix3d POL;
  POL.col(0) = X;
  POL.col(1) = Y;
  POL.col(2) = UU;

  MatrixXd PRJ = MatrixXd::Zero(6,6);
  PRJ.block<3,3>(0,0) = POL.transpose();
  PRJ.block<3,3>(3,3) = POL.transpose();
  PRJ.block<3,3>(0,3).noalias() = - POL.transpose() * se3.getRotation().getSkewMatrix(PP);

  MatrixXd JRP = MatrixXd::Zero(6,6);
  JRP.block<3,3>(0,0) = POL;
  JRP.block<3,3>(3,3) = POL;
  JRP.block<3,3>(0,3).noalias() = se3.getRotation().getSkewMatrix(PP) * POL;

  MatrixXd SL1 = MatrixXd::Identity(6,6);
  SL1.block<3,3>(0,0) << 
    1+derivX, 0, 0,
      derivY, 1, 0,
           0, 0, 1;
  MatrixXd SL2 = MatrixXd::Identity(6,6);
  SL2.block<3,3>(0,0) << 
    1+derivX, 0, 0,
     -derivY, 1, 0,
     0, 0, 1;
  jacobien.block<2,6>(0,0).noalias() =   l1.getInteractionMatrix() * WI * JRP * SL1 * PRJ * K6 * IW; 
  jacobien.block<2,6>(2,0).noalias() =   l2.getInteractionMatrix() * WI * JRP * SL2 * PRJ * K6 * IW; 
  
  return jacobien;
}

VectorXd Cylinder::getError(const VectorXd & S, const VectorXd & ref) const{
  VectorXd output = VectorXd::Zero(4);
  Vector3d A,B,C,D;
  A << 0,0,0;
  B << 0,0,1;
  C << 0,0,0;
  D << 0,0,1;
  LineFeature l1(A,B);
  LineFeature l2(C,D);
  l1.setSituation(frame);
  l2.setSituation(frame);
  l1.attachToCamera(oeil);
  l2.attachToCamera(oeil);
  output.block<2,1>(0,0) = l1.getError(S.block<2,1>(0,0), ref.block<2,1>(0,0));
  output.block<2,1>(2,0) = l2.getError(S.block<2,1>(2,0), ref.block<2,1>(2,0));
  return output;
}

MatrixXd Cylinder::getJacobianError(const VectorXd & S, const VectorXd & R) const{
  MatrixXd jacobien = MatrixXd::Zero(4,4);
  Vector3d A,B,C,D;
  A << 0,0,0;
  B << 0,0,1;
  C << 0,0,0;
  D << 0,0,1;
  LineFeature l1(A,B);
  LineFeature l2(C,D);
  l1.setSituation(frame);
  l2.setSituation(frame);
  l1.attachToCamera(oeil);
  l2.attachToCamera(oeil);
  jacobien.block<2,2>(0,0) = l1.getJacobianError(S.block<2,1>(0,0), R.block<2,1>(0,0));
  jacobien.block<2,2>(2,2) = l2.getJacobianError(S.block<2,1>(2,0), R.block<2,1>(2,0));
  return jacobien;
}

MatrixXd Cylinder::getJacobianReferenceError(const VectorXd & S, const VectorXd & R) const{
  MatrixXd jacobien = MatrixXd::Zero(4,4);
  Vector3d A,B,C,D;
  A << 0,0,0;
  B << 0,0,1;
  C << 0,0,0;
  D << 0,0,1;
  LineFeature l1(A,B);
  LineFeature l2(C,D);
  l1.setSituation(frame);
  l2.setSituation(frame);
  l1.attachToCamera(oeil);
  l2.attachToCamera(oeil);
  jacobien.block<2,2>(0,0) = l1.getJacobianReferenceError(S.block<2,1>(0,0), R.block<2,1>(0,0));
  jacobien.block<2,2>(2,2) = l2.getJacobianReferenceError(S.block<2,1>(2,0), R.block<2,1>(2,0));
  return jacobien;
}

  
void Cylinder::attachToCamera(Camera * eye){
  assert(eye!=NULL);
  oeil = eye;
}

void Cylinder::setSituation(const Repere & r){
  frame = r;
}



void Cylinder::draw(CVEcran * const vue, cv::Scalar couleur) const{
  MatrixXd PRJ2 = MatrixXd::Identity(3,3) - U * U.transpose();
  Vector3d pos = oeil->getRepere().getDeplacement();
  
  pos = frame.getPointInME(pos);
  Vector3d X = PRJ2 * (pos - P);
  double d = X.norm();
  X.normalize();
  Vector3d Y = U.cross(X);
  
  
  double cosA = rayon/d;
  double sinA = sqrt(1-cosA*cosA);
  
  Vector3d A,B,C,D;
  A = P + rayon * (cosA * X + sinA * Y);
  B = P + rayon * (cosA * X + sinA * Y) + U;
  C = P + rayon * (cosA * X - sinA * Y);
  D = P + rayon * (cosA * X - sinA * Y) + U;
  
  LineFeature l1(A,B);
  LineFeature l2(C,D);
  l1.setSituation(frame);
  l2.setSituation(frame);
  l1.attachToCamera(oeil);
  l2.attachToCamera(oeil);
  l1.draw(vue, couleur);
  l2.draw(vue, couleur);
}
  

void Cylinder::drawFeat(const VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const{
  Vector3d A,Z;
  Z << 0, 0, 1;
  A = 0 * Z;
  LineFeature l(A,Z);
  l.attachToCamera(oeil);
  l.drawFeat(value.block<2,1>(0,0),vue, couleur);
  l.drawFeat(value.block<2,1>(2,0),vue, couleur);
}


