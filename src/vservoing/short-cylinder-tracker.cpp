#include "short-cylinder-tracker.h"

using namespace std;
using namespace Eigen;


CylinderTracker::CylinderTracker():
  RealTracker(),
  center(320,240),
  traceurR(2,false),
  traceurL(2,false),
  designer(0.1,0.02)
{
  limiteClicks = 4;
}

void CylinderTracker::resetClicks(){
  nombreClicks = 0;
  hasStarted = false;
  traceurR.resetClicks();
  traceurL.resetClicks();
}
 
void CylinderTracker::answerOnClick(int x, int y){
  if(nombreClicks<=2)
    traceurR.answerOnClick(x,y);
  else
    traceurL.answerOnClick(x,y);
}

void CylinderTracker::track(const cv::Mat & image){
  traceurR.track(image);
  traceurL.track(image);
}

int CylinderTracker::getDimensionOutput() const{
  return 8;
}

bool CylinderTracker::itCanBeSeen() const{
  return traceurR.itCanBeSeen() && traceurL.itCanBeSeen();
}

bool CylinderTracker::isLost() const{
  return false;
  //return traceurR.getScore() < 0.5 || traceurR.getScore() < 0.5;
}

void CylinderTracker::attachToCamera(Camera * eye){
  assert(eye!=NULL);
  oeil = eye;
  traceurR.attachToCamera(eye);
  traceurL.attachToCamera(eye);
  designer.attachToCamera(eye);
  center = oeil->getIntrinsicParameters().getCenter();
}

void CylinderTracker::findFeatures(const VectorXd & feat){
  assert(feat.rows()==8);
  double RHO = feat(0);
  double THETA  = feat(1);
  double K1 = feat(2);
  double K2 = feat(3);
  double cosa = cos(THETA);
  double sina = sin(THETA);
  double x1 = RHO * cosa - K1 * sina;
  double y1 = RHO * sina + K1 * cosa;
  double x2 = RHO * cosa - K2 * sina;
  double y2 = RHO * sina + K2 * sina;
  Vector2d A,B;
  A << x1,y1;
  B << x2,y2;
  A = A + center;
  B = B + center;
  VectorXd PP(4);
  PP << A, B;
  traceurR.findFeatures(PP);

  
  RHO = feat(4);
  THETA  = feat(5);
  K1 = feat(6);
  K2 = feat(7);
  cosa = cos(THETA);
  sina = sin(THETA);
  x1 = RHO * cosa - K1 * sina;
  y1 = RHO * sina + K1 * cosa;
  x2 = RHO * cosa - K2 * sina;
  y2 = RHO * sina + K2 * sina;
  A << x1,y1;
  B << x2,y2;
  A = A + center;
  B = B + center;
  PP << A, B;
  traceurL.findFeatures(PP);
}




VectorXd CylinderTracker::getFunction() const{
  VectorXd Input = VectorXd::Zero(8);
  Input.block<4,1>(0,0)= traceurR.getFunction();
  Input.block<4,1>(4,0)= traceurL.getFunction();
  VectorXd Output = Input;
  Vector2d A,B,C, RT;
  for(int i = 0; i < 2; i++){
    A = Input.block<2,1>(4*i,0);
    B = Input.block<2,1>(4*i+2,0);
    A -= center;
    B -= center;
    RT = AbstractLineFeature::computeRhoTeta(A,B);
    double rho = RT(0);
    double theta = RT(1);
    double cosa = cos(theta);
    double sina = sin(theta);
    double k1 = A(1) * cosa - A(0) * sina; 
    double k2 = B(1) * cosa - B(0) * sina;
    Output.block<4,1>(4*i,0) << rho, theta, k1, k2;
  }
  return Output;
}  
 

void CylinderTracker::draw(CVEcran * const vue, cv::Scalar couleur) const{
  traceurR.dessiner(vue->getImage());
  traceurL.dessiner(vue->getImage());
}

void CylinderTracker::drawFeat(const VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const{
  designer.drawFeat(value,vue,couleur);
}
