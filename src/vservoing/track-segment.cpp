#include "track-segment.h"
#include <cmath>
#include <algorithm> 

using namespace std;
using namespace Eigen;

RealTracker::RealTracker():
  VTracker(),
  nombreClicks(0),
  limiteClicks(1),
  hasStarted(false),
  display(NULL)
{
}

void RealTracker::initTracking(CVEcran * d){
  display = d;
  assert(display!=NULL);
  // Mouse event to select the tracked color on the original image
  cvSetMouseCallback(display->getNom().c_str(), RealTracker::mouseCallBack, this);

}

void RealTracker::mouseCallBack(int event, int x, int y, int flags, void * param){
  if(param!=NULL){
    if(event == CV_EVENT_LBUTTONUP){
      RealTracker * tracker = static_cast<RealTracker*>(param);
      if(tracker!=NULL)
	{
	  if((++tracker->nombreClicks) <= tracker->limiteClicks){
	    tracker->answerOnClick(x,y);
	  }
	  if((tracker->nombreClicks) == tracker->limiteClicks){
	    tracker->hasStarted = true;
	  }
	}
    }
    if(event == CV_EVENT_RBUTTONUP){
      RealTracker * tracker = static_cast<RealTracker*>(param);
      if(tracker!=NULL)	{
	if(tracker->isRunning()){
	  tracker->resetClicks();
	}
      }
    }
  }
}

void RealTracker::resetClicks(){
  hasStarted = false;
  nombreClicks = 0;
}


void RealTracker::addVirtualClick(){
  if(++nombreClicks>=limiteClicks)
    hasStarted = true;
}


void RealTracker::answerOnClick(int x, int y){
  cerr << "!!!! I do not understand what I am doing here !!!!\n This method should be overriden !!!!" <<endl;
  cv::putText(display->getImage(), " CLICK ?", cv::Point(x,y), 
	      cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, cv::Scalar(255,0,0), 1, CV_AA);
     display->affichage();
}
  

bool RealTracker::isRunning(){
  return hasStarted;
}
  

SobelTracker::SobelTracker():
  RealTracker(),
  largeur(640),
  hauteur(480),
  pas(1),
  marge(3),
  score(0),
  seuil(15),
  histoire(cv::Mat(640,480,CV_16S))
{

}

double SobelTracker::distance(cv::Point A, cv::Point B){
  double d = sqrt((A.x - B.x)*(A.x - B.x) + (A.y - B.y)*(A.y - B.y));
  return d;
}


double SobelTracker::computeScore(const cv::Mat & img, cv::Point p1, cv::Point p2, bool divide){

  assert(img.channels()==1);

  double u1 = p1.x;
  double v1 = p1.y;
  double u2 = p2.x;
  double v2 = p2.y;

  double d = distance(p1,p2);;
  
  double somme = 0, ranking = 0;
  int val = 0;

  int sensX = 1;
  int sensY = 1;
  if(u2<u1) sensX = -1;
  if(v2<v1) sensY = -1;

  int du = static_cast<int>(abs(u2-u1));
  int dv = static_cast<int>(abs(v2-v1));
  int traj = 0; // horizontale
  int dd = du; // nombre pixels
  double pente;
  if(dv>du){
    traj = 1; // vertical
    dd = dv;
    pente = (1.0*(u2-u1))/(1.0*(v2-v1));
  }
  else{
    pente = (1.0*(v2-v1))/(1.0*(u2-u1));
  }
  
  int a,b;
  for( int k = 0; k <=dd ; k++){

    if(traj==0){
      a = static_cast<int>(sensX*k + u1);
      b = static_cast<int>(round(k*sensX*pente) + v1);
    }
    else{
      a = static_cast<int>(round(k*sensY*pente) + u1);
      b = static_cast<int>(sensY*k + v1);
    }

    if(a<0) a = 0;
    if(b<0) b = 0;
    val = img.at<uchar>(b,a);
    somme += 1e-2*val;
  }

  if(divide)
    ranking = somme / sqrt(d);
  else
    ranking = somme;

  /*
  std::cout<<"D="<<d<<std::endl;
  std::cout<<"Sum = "<<somme<<std::endl;
  std::cout<<"Score = "<<ranking<<std::endl;
  */
  return ranking;
}


void SobelTracker::track(const cv::Mat & img){
  int n = 0;
  cv::Mat contours = sobel(img);
  //cv::imshow("Sobel translation", contours);

  double s,ss;
  do{
    ss = score;
    stepTracking(contours);
    s = score;
    n++;
    if(n>=4) break;
  }while(abs(s-ss)<1e-8);
  //cout<<" n = "<<n<<"  score = "<<s<<endl;
}  

double SobelTracker::getScore() const{
  return score;
}


cv::Mat SobelTracker::getStory() const{
  cv::Mat story = histoire;
  //display(story);
  return story;
}


cv::Mat SobelTracker::sobel(const cv::Mat & input){

  cv::Mat output = input;
  int scale = 1;
  int delta = 0;
  int ddepth = CV_16S;

  cv::Mat gray,grad;

  // Flouter et réduire le bruit

  GaussianBlur( input, input, cv::Size(3,3), 0, 0, cv::BORDER_DEFAULT );

  // Mettre en noir et blanc

  cvtColor( input, gray, cv::COLOR_RGB2GRAY );

  // Appliquer un filtre Sobel ou Scharr

  cv::Mat grad_x, grad_y;
  cv::Mat abs_grad_x, abs_grad_y;

  /// Gradient X
  //Scharr( gray, grad_x, ddepth, 1, 0, scale, delta, cv::BORDER_DEFAULT );
  Sobel( gray, grad_x, ddepth, 1, 0, 3, scale, delta, cv::BORDER_DEFAULT );
  convertScaleAbs( grad_x, abs_grad_x );

  /// Gradient Y
  //Scharr( gray, grad_y, ddepth, 0, 1, scale, delta, cv::BORDER_DEFAULT );
  Sobel( gray, grad_y, ddepth, 0, 1, 3, scale, delta, cv::BORDER_DEFAULT );
  convertScaleAbs( grad_y, abs_grad_y );

  /// Total Gradient (approximate)
  addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad );

  output = grad;

  // Binariser l'image
  
  threshold( grad, output, seuil, 255, cv::THRESH_TOZERO);

  //threshold( grad, output, seuil, 255, cv::THRESH_BINARY);

  //adaptiveThreshold( grad, output, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY_INV, 5, 6);

  //adaptiveThreshold( grad, output, 255, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY_INV, 3, 5);
  
  //Mat tempo = output;

  //*
  
  //double retro = 1.0/64;
  
  //output =  (1-retro) * output + retro * histoire;
  
  grad = output;

  //threshold( grad, output, seuil, 255, THRESH_TOZERO);

  //*/

  //histoire = output;

  return output;

}


TrackSegment::TrackSegment():
  SobelTracker(),
  A(160,240),
  B(480,240),
  C(320,240),
  rho(0.0),
  theta(M_PI),
  k1(160.0),
  k2(-160.0),
  precision(0.0),
  AB(Vector3d::Zero(),Vector3d::UnitZ())
{
  limiteClicks = 2;
}

void TrackSegment::answerOnClick(int x, int y){
  if(nombreClicks == 1){
    A.x = x;
    A.y = y;
  }
  else if(nombreClicks == 2){
    B.x = x;
    B.y = y;
    
    if(display!=NULL){

      Vector2d AA,BB;
      AA << A.x - C.x, A.y - C.y;
      BB << B.x - C.x, B.y - C.y;
      
      VectorXd output  = Segment::computeRhoTeta2K(AA,BB);
      rho  = output(0);
      theta = output(1);
      k1  = output(2);
      k2  = output(3);
  
      length = abs(k2-k1);

      precision = 1.5*pas/length;

      cv::Mat contours = sobel(display->getImage());
  
      score = computeScore(contours,A,B);

    }
  }
}

  
void TrackSegment::attachToCamera(Camera * eye){
  assert(eye!=NULL);
  oeil = eye;
  C.x = static_cast<int>(round(oeil->getIntrinsicParameters().u0));
  C.y = static_cast<int>(round(oeil->getIntrinsicParameters().v0));
  AB.attachToCamera(eye);
  cout << "Center ("<< C.x << " , "<< C.y << " ) "<< endl;
}



void TrackSegment::stepTracking(const cv::Mat & img){

  int u1M = A.x;
  int v1M = A.y;
  int u2M = B.x;
  int v2M = B.y;

  cv::Point p1,p2;

  p1 = A;
  p2 = B;
  
  //score = computeScore(contours,p1,p2);

  double scoreMax = 0;
  
  double sC = 0;

  for(int i1 = -marge; i1 <marge+1; i1++){

    for(int j1 = -marge; j1 <marge+1; j1++){

      for(int i2 = -marge; i2 <marge+1; i2++){

	for(int j2 = -marge; j2 <marge+1; j2++){

	  
	  p1.x = (A.x+pas*i1);
	  p1.y = (A.y+pas*j1);
	  p2.x = (B.x+pas*i2);
	  p2.y = (B.y+pas*j2);

	  if( (p1.x>=0 && p1.y>=0 && p2.x>=0 && p2.y>=0) 
	      &(p1.x<=largeur && p1.y<=hauteur && p2.x<=largeur && p2.y<=hauteur)){
	  
	    sC = computeScore(img,p1,p2);
	  
	    if(sC> scoreMax){

	      scoreMax = sC;
	    
	      u1M = p1.x;
	      v1M = p1.y;
	      u2M = p2.x;
	      v2M = p2.y;

	    }
	  }
	    
	}

      }

    }

  }

  A.x = u1M;
  A.y = v1M;
  B.x = u2M;
  B.y = v2M;

  Vector2d AA,BB;
  AA << A.x - C.x, A.y - C.y;
  BB << B.x - C.x, B.y - C.y;

      
  VectorXd output  = Segment::computeRhoTeta2K(AA,BB);
  rho  = output(0);
  theta = output(1);
  k1  = output(2);
  k2  = output(3);
  
  score = scoreMax;

  length = abs(k2-k1);

  precision = 1.5*pas/length;
  
}

void TrackSegment::findFeatures(const Eigen::VectorXd & feat){
  assert(feat.rows()==4);
  VectorXd P4 = Segment::computeABfromRhoTeta2K(feat);
  A.x = static_cast<int>(round(P4(0)));
  A.y = static_cast<int>(round(P4(1)));
  B.x = static_cast<int>(round(P4(2)));
  B.y = static_cast<int>(round(P4(3)));
  A = A + C;
  B = B + C;
}



int TrackSegment::getDimensionOutput() const{
  return 4;
}

bool TrackSegment::itCanBeSeen() const{
  return true;
}

bool TrackSegment::isLost() const{
  return score < 0.5;
}

VectorXd TrackSegment::getFunction() const{
  VectorXd F(4);
  F << rho,theta,k1,k2;
  return F;
}  

void TrackSegment::draw(CVEcran * const vue, cv::Scalar couleur) const{
  line(vue->getImage(),A,B,couleur,1,8);
  //  AB.drawFeat(getFunction(),vue,couleur);
}

void TrackSegment::drawFeat(const VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const{
  AB.drawFeat(value,vue,couleur);
}

void TrackSegment::dessiner(cv::Mat & img) const{
  line(img,A,B,cv::Scalar(255,255,255),1,8);
}

double TrackSegment::getRho() const{
  return rho;
}

double TrackSegment::getTheta() const{
  return theta;
}

double TrackSegment::getK1() const{
  return k1;
}

double TrackSegment::getK2() const{
  return k2;
}


  
