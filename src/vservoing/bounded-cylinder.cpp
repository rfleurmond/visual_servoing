#include "bounded-cylinder.h"

using namespace std;
using namespace Eigen;

void BoundedCylinder::init(double r, Vector3d bot, Vector3d top){
  assert(r>=0.5e-2);
  BOT = bot;
  TOP = top;
  Vector3d BT = top - bot; // Top - bottom
  double l = BT.norm();
  assert(l>=1e-2);
  U = BT.normalized(); 
  length = l;
  rayon = r;
  P = (MatrixXd::Identity(3,3) - U*U.transpose()) * bot;
  strategy = VisualFeature::PINV;
}

BoundedCylinder::BoundedCylinder(double l, double r):
  VisualFeature(),
  rayon(r),
  length(l),
  P(Vector3d::Zero()),
  U(Vector3d::UnitZ()),
  TOP(l*Vector3d::UnitZ()),
  BOT(Vector3d::Zero())
{
  //init(r,Vector3d::Zero(), l * Vector3d::UnitZ());
}

BoundedCylinder::BoundedCylinder(double l, double r, Vector3d uu):
  VisualFeature(),
  rayon(r),
  length(l),
  P(Vector3d::Zero()),
  U(uu),
  TOP(l*uu),
  BOT(Vector3d::Zero())
{
  //init(r,Vector3d::Zero(),l*uu);
}

BoundedCylinder::BoundedCylinder(double r, Vector3d bot, Vector3d top):
  VisualFeature(),
  rayon(r),
  length(1),
  P(Vector3d::Zero()),
  U(Vector3d::UnitZ()),
  TOP(top),
  BOT(bot)
{
  init(r,bot,top);
}


int BoundedCylinder::getDimensionState() const{
  return 7;
}

int BoundedCylinder::getDimensionOutput() const{
  return 8;
}


bool BoundedCylinder::itCanBeSeen() const{

  MatrixXd PRJ2 = MatrixXd::Identity(3,3) - U * U.transpose();
  Vector3d pos = oeil->getRepere().getDeplacement();
  
  pos = frame.getPointInME(pos);
  Vector3d X = PRJ2 * (pos - P);
  double d = X.norm();
  X.normalize();
  Vector3d Y = U.cross(X);
  
  if(d<=rayon){
    return false;
  }
  
  double cosA = rayon/d;
  double sinA = sqrt(1-cosA*cosA);
  Vector3d A,B,C,D;
  A = BOT + rayon * (cosA * X + sinA * Y);
  B = TOP + rayon * (cosA * X + sinA * Y);
  C = BOT + rayon * (cosA * X - sinA * Y);
  D = TOP + rayon * (cosA * X - sinA * Y);
  Segment l1(A,B);
  Segment l2(C,D);
  l1.setSituation(frame);
  l2.setSituation(frame);
  l1.attachToCamera(oeil);
  l2.attachToCamera(oeil);
  bool val =  l1.itCanBeSeen() && l2.itCanBeSeen();
  /*
  if(!val){
    std::cout<<"...................Bounded Cylinder .................."<<std::endl;
    std::cout<<"Repere de la camera:\n"<<oeil->getRepere()<<std::endl;
    std::cout<<"Repere du marqueur :\n"<<frame<<std::endl;
    std::cout<<"\n"<<std::endl;
    return false;
  }
  */
  return val;
}

VectorXd BoundedCylinder::getFunction(const VectorXd & param) const{
  assert(param.rows()==7);
  double rr = param(0);
  if( !(rr>0)) {cout << "R = "<< rr << " !!!! Really ??" <<endl;rr = 0.01;}
  assert(rr > 0);
  Vector3d BB(static_cast<Vector3d>(param.block<3,1>(1,0)));
  Vector3d TT(static_cast<Vector3d>(param.block<3,1>(4,0)));
  return getFunction(rr,BB,TT);
}

VectorXd BoundedCylinder::getFunction(double rr, Vector3d BB, Vector3d TT) const{
  
  Vector3d BT = TT - BB;

  Vector3d UU = BT.normalized();

  MatrixXd PRJ2 = MatrixXd::Identity(3,3) - UU * UU.transpose();
  
  Vector3d PP = PRJ2 * BB;
  
  VectorXd output = VectorXd::Zero(8);
  
  Vector3d pos = oeil->getRepere().getDeplacement();
  
  pos = frame.getPointInME(pos);
  Vector3d X = PRJ2 * (pos - PP);
  double d = X.norm();
  X.normalize();
  Vector3d Y = UU.cross(X);
  
  
  double cosA = rr/d;
  double sinA = sqrt(1-cosA*cosA);
  
  Vector3d A,B,C,D;
  A = BB + rr * (cosA * X + sinA * Y);
  B = TT + rr * (cosA * X + sinA * Y);
  C = BB + rr * (cosA * X - sinA * Y);
  D = TT + rr * (cosA * X - sinA * Y);
  
  Segment l1(A,B);
  Segment l2(C,D);
  l1.setSituation(frame);
  l2.setSituation(frame);
  l1.attachToCamera(oeil);
  l2.attachToCamera(oeil);
  output.block<4,1>(0,0) = l1.getFunction();
  output.block<4,1>(4,0) = l2.getFunction();
  return output;
}  


VectorXd BoundedCylinder::getFunction() const{
  return getFunction(rayon,BOT,TOP);
}

MatrixXd BoundedCylinder::getNaiveInteractionMatrix(const VectorXd & F) const{
  assert(F.rows()==8);
  Segment l1(Vector3d::Zero(),Vector3d::UnitZ());
  l1.setSituation(frame);
  l1.attachToCamera(oeil);
  MatrixXd L = MatrixXd::Zero(8,6);
  L.block<4,6>(0,0) = l1.getNaiveInteractionMatrix(F.block<4,1>(0,0));
  L.block<4,6>(4,0) = l1.getNaiveInteractionMatrix(F.block<4,1>(4,0));
  return L;
}
  

MatrixXd BoundedCylinder::getInteractionMatrix(const VectorXd & param) const{
  assert(param.rows()==7);
  double rr = param(0);
  assert(rr>0);
  Vector3d BB(static_cast<Vector3d>(param.block<3,1>(1,0)));
  Vector3d TT(static_cast<Vector3d>(param.block<3,1>(4,0)));
  return getInteractionMatrix(rr,BB,TT);
}

MatrixXd BoundedCylinder::getInteractionMatrix(double rr, Vector3d BB, Vector3d TT) const{


  Vector3d BT = TT - BB;

  Vector3d UU = BT.normalized();

  MatrixXd PRJ2 = MatrixXd::Identity(3,3) - UU * UU.transpose();
  
  Vector3d PP = PRJ2 * BB;

  MatrixXd jacobien = MatrixXd::Zero(8,6);

  Vector3d pos = oeil->getRepere().getDeplacement();
  
  pos = frame.getPointInME(pos);
  
  Vector3d X = PRJ2 * (pos - PP);
  double d = X.norm();
  X.normalize();
  
  Vector3d Y = UU.cross(X);
  
  double cosA = rr/d;
  double sinA = sqrt(1-cosA*cosA);
  
  Vector3d A,B,C,D;
  A = BB + rr * (cosA * X + sinA * Y);
  B = TT + rr * (cosA * X + sinA * Y);
  C = BB + rr * (cosA * X - sinA * Y);
  D = TT + rr * (cosA * X - sinA * Y);
  
  Segment l1(A,B);
  Segment l2(C,D);
  l1.setSituation(frame);
  l2.setSituation(frame);
  l1.attachToCamera(oeil);
  l2.attachToCamera(oeil);


  double derivX = - cosA * cosA;
  double derivY = cosA / sinA - cosA * sinA;
  
  Repere se3 = oeil->getRepere().inverse()*frame; // From cylinder to camera
  
  MatrixXd ROT = se3.getRotation().getMatrix(); // From cylinder to camera
  MatrixXd TOR = ROT.transpose(); // From camera to cylinder

  MatrixXd WI = se3.getKineticMatrix();
  MatrixXd IW = se3.getInversKineticMatrix();

  MatrixXd K6 = MatrixXd::Identity(6,6);
  K6(5,5) = 0;
  
  Matrix3d POL;
  POL.col(0) = X;
  POL.col(1) = Y;
  POL.col(2) = UU;

  MatrixXd PRJ = MatrixXd::Zero(6,6);
  PRJ.block<3,3>(0,0) = POL.transpose();
  PRJ.block<3,3>(3,3) = POL.transpose();
  PRJ.block<3,3>(0,3).noalias() = - POL.transpose() * se3.getRotation().getSkewMatrix(PP);

  MatrixXd JRP = MatrixXd::Zero(6,6);
  JRP.block<3,3>(0,0) = POL;
  JRP.block<3,3>(3,3) = POL;
  JRP.block<3,3>(0,3).noalias() = se3.getRotation().getSkewMatrix(PP) * POL;

  MatrixXd SL1 = MatrixXd::Identity(6,6);
  SL1.block<3,3>(0,0) << 
    1+derivX, 0, 0,
      derivY, 1, 0,
           0, 0, 1;
  MatrixXd SL2 = MatrixXd::Identity(6,6);
  SL2.block<3,3>(0,0) << 
    1+derivX, 0, 0,
     -derivY, 1, 0,
           0, 0, 1;
  jacobien.block<4,6>(0,0).noalias() =   l1.getInteractionMatrix() * WI * JRP * SL1 * PRJ * K6 * IW; 
  jacobien.block<4,6>(4,0).noalias() =   l2.getInteractionMatrix() * WI * JRP * SL2 * PRJ * K6 * IW; 
  
  return jacobien;
}


MatrixXd BoundedCylinder::getInteractionMatrix() const{
  return getInteractionMatrix(rayon,BOT,TOP);
}

VectorXd BoundedCylinder::getError(const VectorXd & S, const VectorXd & ref) const{
  VectorXd output = VectorXd::Zero(8);
  Vector3d A,B,C,D;
  A << 0,0,0;
  B << 0,0,1;
  C << 0,0,0;
  D << 0,0,1;
  Segment l1(A,B);
  Segment l2(C,D);
  l1.setSituation(frame);
  l2.setSituation(frame);
  l1.attachToCamera(oeil);
  l2.attachToCamera(oeil);
  output.block<4,1>(0,0) = l1.getError(S.block<4,1>(0,0), ref.block<4,1>(0,0));
  output.block<4,1>(4,0) = l2.getError(S.block<4,1>(4,0), ref.block<4,1>(4,0));
  return output;
}

MatrixXd BoundedCylinder::getJacobianError(const VectorXd & S, const VectorXd & R) const{
  MatrixXd jacobien = MatrixXd::Zero(8,8);
  Vector3d A,B,C,D;
  A << 0,0,0;
  B << 0,0,1;
  C << 0,0,0;
  D << 0,0,1;
  Segment l1(A,B);
  Segment l2(C,D);
  l1.setSituation(frame);
  l2.setSituation(frame);
  l1.attachToCamera(oeil);
  l2.attachToCamera(oeil);
  jacobien.block<4,4>(0,0) = l1.getJacobianError(S.block<4,1>(0,0), R.block<4,1>(0,0));
  jacobien.block<4,4>(4,4) = l2.getJacobianError(S.block<4,1>(4,0), R.block<4,1>(4,0));
  return jacobien;
}

MatrixXd BoundedCylinder::getJacobianReferenceError(const VectorXd & S, const VectorXd & R) const{
  MatrixXd jacobien = MatrixXd::Zero(8,8);
  Vector3d A,B,C,D;
  A << 0,0,0;
  B << 0,0,1;
  C << 0,0,0;
  D << 0,0,1;
  Segment l1(A,B);
  Segment l2(C,D);
  l1.setSituation(frame);
  l2.setSituation(frame);
  l1.attachToCamera(oeil);
  l2.attachToCamera(oeil);
  jacobien.block<4,4>(0,0) = l1.getJacobianReferenceError(S.block<4,1>(0,0), R.block<4,1>(0,0));
  jacobien.block<4,4>(4,4) = l2.getJacobianReferenceError(S.block<4,1>(4,0), R.block<4,1>(4,0));
  return jacobien;
}

  
void BoundedCylinder::attachToCamera(Camera * eye){
  assert(eye!=NULL);
  oeil = eye;
}

void BoundedCylinder::setSituation(const Repere & r){
  frame = r;
}



void BoundedCylinder::draw(CVEcran * const vue, cv::Scalar couleur) const{
  MatrixXd PRJ2 = MatrixXd::Identity(3,3) - U * U.transpose();
  Vector3d pos = oeil->getRepere().getDeplacement();
  
  pos = frame.getPointInME(pos);
  Vector3d X = PRJ2 * (pos - P);
  double d = X.norm();
  X.normalize();
  Vector3d Y = U.cross(X);
  
  
  double cosA = rayon/d;
  double sinA = sqrt(1-cosA*cosA);
  
  Vector3d A,B,C,D;
  A = BOT + rayon * (cosA * X + sinA * Y);
  B = TOP + rayon * (cosA * X + sinA * Y);
  C = BOT + rayon * (cosA * X - sinA * Y);
  D = TOP + rayon * (cosA * X - sinA * Y);
  
  Segment l1(A,B);
  Segment l2(C,D);
  l1.setSituation(frame);
  l2.setSituation(frame);
  l1.attachToCamera(oeil);
  l2.attachToCamera(oeil);
  l1.draw(vue, couleur);
  l2.draw(vue, couleur);


  cv::Point P1,P2,P3,P4;

  unsigned int parts = 12;

  double secteur = 2.0 *M_PI/parts;

  double angle = 0;

  PointFeature pfeat;
  pfeat.setSituation(frame);
  pfeat.attachToCamera(oeil);

  Vector2d I;

  for(unsigned int i = 0; i < parts; i ++){

    angle = i * secteur;
    cosA = cos(angle);
    sinA = sin(angle);
    A = BOT + rayon * (cosA * X + sinA * Y);
    C = TOP + rayon * (cosA * X + sinA * Y);
    cosA = cos(angle + secteur);
    sinA = sin(angle + secteur);
    B = BOT + rayon * (cosA * X + sinA * Y);
    D = TOP + rayon * (cosA * X + sinA * Y);
    
    I = pfeat.getFunction(A);
    I = vue->getPoint(I);
    P1.x = static_cast<int>(I(0));
    P1.y = static_cast<int>(I(1));
  
    I = pfeat.getFunction(B);
    I = vue->getPoint(I);
    P2.x = static_cast<int>(I(0));
    P2.y = static_cast<int>(I(1));
  

    I = pfeat.getFunction(C);
    I = vue->getPoint(I);
    P3.x = static_cast<int>(I(0));
    P3.y = static_cast<int>(I(1));
  
    I = pfeat.getFunction(D);
    I = vue->getPoint(I);
    P4.x = static_cast<int>(I(0));
    P4.y = static_cast<int>(I(1));
  
    line(vue->getImage(),P1,P2,couleur,1,8);

    line(vue->getImage(),P3,P4,couleur,1,8);
    
  }  
 
  
}
  

void BoundedCylinder::drawFeat(const VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const{
  assert(value.rows()==8);
  Vector3d A,Z;
  Z << 0, 0, length;
  A = 0 * Z;
  Segment l(A,Z);
  l.attachToCamera(oeil);
  l.drawFeat(value.block<4,1>(0,0),vue, couleur);
  l.drawFeat(value.block<4,1>(4,0),vue, couleur);
}


