#include "line-with-k-estimator.h"

using namespace Eigen;

LineKEstimator::LineKEstimator(VisualFeature * model, VTracker * tracker):
  FeatureEstimator(model,tracker),
  phi(MatrixXd::Zero(3,3)),
  beta(VectorXd::Zero(3)),
  phiP(MatrixXd::Zero(3,3)),
  betaP(VectorXd::Zero(3)),
  length(0.1)
{
  assert(model!=NULL);
  assert(tracker!=NULL);

  estimation = VectorXd::Zero(9);
  frame = modele->getSituation();
  oeil = tracker->getEye();

}

void LineKEstimator::computeEstimation(){

  Vector3d CI = frame.getPointInME(oeil->getRepere().getDeplacement());
  VectorXd F = traceur->getFunction();
  
  /// @see LineEstimator::computeEstimation()
  
  double rho = F(0);
  double theta = F(1);
  double k2 = F(2);

  double cosa,sina;

  cosa = cos(theta);
  sina = sin(theta);

  double x,y;
  Vector2d imgi, centre;
  IntrinsicCam optique = oeil->getIntrinsicParameters();
  centre = optique.getCenter();
  
  double k1;
  k1 = -0.25 * optique.alphaU;
  k2 =  0.25 * optique.alphaU;

  x = rho * cosa - k1 * sina;
  y = rho * sina + k1 * cosa;
  
  imgi << x,y ;
  imgi += centre;

  Vector3d U1 = frame.getVectorInME(oeil->getCleanDirection(imgi));

  x = rho * cosa - k2 * sina;
  y = rho * sina + k2 * cosa;
  
  imgi << x,y ;
  imgi += centre;

  Vector3d U2 = frame.getVectorInME(oeil->getCleanDirection(imgi));

  Vector3d NI = U1.cross(U2).normalized();

  MatrixXd PI = NI * NI.transpose();
  
  phi  +=  PI;
  beta.noalias() +=  PI * CI;

  VectorXd PP, UU;

  LineEstimator::resoudre(phi,beta,PP,UU);

  //Vector3d VV = se3.getVectorInME(frame.getVectorInParent(UU));
  
  //std::cout << "Vecteur U dans le repère de la caméra: " << VV.transpose() << std::endl;

  /// @see PointEstimator::computeEstimation()
  
  k2 = F(2);

  x = rho * cosa - k2 * sina;
  y = rho * sina + k2 * cosa;
  
  imgi << x,y ;
  imgi += centre;

  Vector3d U3 = frame.getVectorInME(oeil->getCleanDirection(imgi));

  PI = MatrixXd::Identity(3,3);
  PI.noalias() -= U3 * U3.transpose();
  
  phiP += PI;
  betaP.noalias() += PI * CI;

  VectorXd  PS;

  PS = phiP.ldlt().solve(betaP);

  estimation.block<3,1>(0,0) = PP;

  estimation.block<3,1>(3,0).noalias() = PP + length * UU;

  estimation.block<3,1>(6,0) = PS;

  VectorXd FE = modele->getFunction(estimation);
 
  VectorXd EF = modele->getError(F,FE);
  
  double eT = EF(1);

  if(eT > 0.5* M_PI || eT < - 0.5* M_PI){
    
    estimation.block<3,1>(3,0).noalias() = PP - length * UU;

  }

}

void LineKEstimator::writeLog(){
  double heure = MyClock::getTime();
  if(hasLog && access)
    {
      if(heure>properTime)
	{
	  VectorXd V = estimation;
	  V.block<3,1>(3,0) = (V.block<3,1>(3,0) - V.block<3,1>(0,0))/length;
	  printer << heure << " " << V.transpose()<<std::endl;
	  properTime = heure;
	}
      //printer << nUpdates << " " << estimation.transpose()<<std::endl;
    }
}

