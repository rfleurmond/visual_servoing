#include "lk-color-tracker.h"

using namespace Eigen;

SpecialTracker::SpecialTracker(int tol, int size):
  RealTracker(),
  h(0),
  s(0),
  v(0),
  tolerance(tol),
  marge(size),
  moyX(0),
  moyY(0),
  rho(0),
  theta(0),
  k(0),
  u0(320),
  v0(240),
  lost(false),
  dispersion(Matrix2d::Zero()),
  traceur(VectorXd::Zero(3),VectorXd::Zero(3),VectorXd::Zero(3)),
  photo(cv::Mat())
{

}

void SpecialTracker::track(const cv::Mat & image){

  lost = false;

  photo = image;

  int sommeX = 0, sommeY = 0;
    
  int nbPixels = 0;

  int taille = 2 * marge + 1;
 
  // Create the mask &initialize it to white (no color detected)
  cv::Mat mask(image.size(), image.depth(), 1);
 
  // Create the hsv image
  cv::Mat hsv = image.clone();
  
  cvtColor(image, hsv, CV_BGR2HSV);
 
  // We create the mask

    
  //inRange(hsv, cv::Scalar(h - tolerance -1, s - tolerance, 0), cv::Scalar(h + tolerance - 1, s + tolerance, 255), mask);
 
  inRange(hsv, cv::Scalar(h - tolerance - 1, s - tolerance, 0), cv::Scalar(h + tolerance - 1, 255, 255), mask);
 
  // Create kernels for the morphological operation
  cv::Mat kernel =  getStructuringElement(CV_SHAPE_ELLIPSE, cv::Size(taille,taille), cv::Point(marge,marge));
  
  // Morphological opening (inverse because we have white pixels on black background)
  dilate(mask, mask, kernel);
  erode(mask, mask, kernel);

  // Show the result of the mask image
  cv::imshow("GeckoGeek Mask", mask);


  int x = 0, y = 0;

  double xx = 0, xy = 0, yy = 0, px = 0, py = 0;
 
  // We go through the mask to look for the tracked object and get its gravity center
  for(x = 0; x < mask.cols; x++) {
    for(y = 0; y < mask.rows; y++) { 
      
      // If its a tracked pixel, count it to the center of gravity's calcul
      if(mask.at<uchar>(y,x) == 255) {
	sommeX += x;
	sommeY += y;
	nbPixels++;
      }
    }
  }

  if(nbPixels < 35){

    lost = true;
    // We release the memory of kernels
    kernel.release();
 
    // We release the memory of the mask
    mask.release();
    // We release the memory of the hsv image
    hsv.release();
    return;
  }

  moyX = sommeX * 1.0/nbPixels;
  moyY = sommeY * 1.0/nbPixels;

  for(x = 0; x < mask.cols; x++) {
    for(y = 0; y < mask.rows; y++) { 
      
      // If its a tracked pixel, count it to the center of gravity's calcul
      if(mask.at<uchar>(y,x) == 255) {
	px = x - moyX;
	py = y - moyY;
	xx += px*px;
	xy += px*py;
	yy += py*py;
      }
    }
  }

  dispersion << xx, xy, xy, yy;

  dispersion = dispersion / (nbPixels * nbPixels);

  theta = calculTheta(dispersion);

  if(lost){
    // We release the memory of kernels
    kernel.release();
 
    // We release the memory of the mask
    mask.release();
    // We release the memory of the hsv image
    hsv.release();
    return;
  }


  /// TROUVER LE K!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  double sina = sin(theta);

  double cosa = cos(theta);

  rho = (moyX - u0) * cosa + (moyY - v0) * sina;
    
  double kmin = 642;
  //double r;
	      
  for(x = 0; x < mask.cols; x++) {
    for(y = 0; y < mask.rows; y++) { 
      
      // If its a tracked pixel, count it to the center of gravity's calcul
      if(mask.at<uchar>(y,x) == 255) {
	px = x - u0;
	py = y - v0;
	k = -px * sina + py * cosa;
	//r =  px * cosa + py * sina;
	if( k < kmin /*&& isEqual (r,0,2)*/){
	  //std::cout << "K = "<<k << "   Kmin = "<<kmin <<endl;
	  kmin = k;
	    
	}
      }
    }
  }
    
  k = kmin;

  // We release the memory of kernels
  kernel.release();
 
  // We release the memory of the mask
  mask.release();
  // We release the memory of the hsv image
  hsv.release();

  /*
    if(lost)
    cout <<"Tracker:  LOST !  = " <<rho << " theta = "<< theta*180/M_PI <<" degres, k = " << k <<endl;
    else
    cout <<"Tracker: rho = " <<rho << " theta = "<< theta*180/M_PI <<" degres, k = " << k <<endl;
  */
 
}

double SpecialTracker::calculTheta(const Matrix2d & cov){
    
  double t = 0;

  //*
    
    JacobiSVD<MatrixXd> svdA(cov,ComputeThinU | ComputeThinV);

    VectorXd U = svdA.matrixV().col(0);

    U = U.normalized();
    
    t = atan2(U(1),U(0)) - M_PI/2;

    if(t > M_PI){
    t -= 2*M_PI;
    }

    if(t < -M_PI){
    t += 2*M_PI;
    }

 
  //*/

  /*

  double ixy,ixx,iyy;
    
  ixx = cov(0,0);

  ixy = cov(0,1);

  iyy = cov(1,1);

  t = 0.5 * atan(2*ixy/(ixx-iyy));
  //*/  

  return t;
}


void SpecialTracker::initiate(int hh, int ss, int vv){
  h = hh;
  s = ss;
  v = vv;
  hasStarted = true;
}
 
int SpecialTracker::getDimensionOutput() const{
  return 3;
}

bool SpecialTracker::itCanBeSeen() const{
  return true;
}


bool SpecialTracker::isLost() const{
  return lost;
}

Eigen::VectorXd SpecialTracker::getFunction() const{
  VectorXd F(3);
  F << rho, theta,k;
  return F;
}

void SpecialTracker::attachToCamera(Camera * eye){
  assert(eye!=NULL);
  oeil = eye;
  traceur.attachToCamera(eye);
  u0 = oeil->getIntrinsicParameters().u0;
  v0 = oeil->getIntrinsicParameters().v0;
}

void SpecialTracker::draw(CVEcran * const vue, cv::Scalar couleur) const{
  traceur.drawFeat(getFunction(),vue,couleur);

  int x = static_cast<int>(round(moyX));
  int y = static_cast<int>(round(moyY));

  if(lost)
    {
      cv::circle(vue->getImage(), cv::Point(x,y), 45, CV_RGB(0, 0, 255), 2);
	
      cv::circle(vue->getImage(), cv::Point(x,y),  5, CV_RGB(255, 0, 0), -1);
    }
}

void SpecialTracker::drawFeat(const VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const{
  traceur.drawFeat(value,vue,couleur);
}


/*
 * Get the color of the pixel where the mouse has clicked
 * We put this color as model color (the color we want to tracked)
 */
void SpecialTracker::answerOnClick(int x, int y) {
 
  // Vars
  cv::Mat hsv;
 
  // Get the hsv image
  hsv = SpecialTracker::photo.clone();

  cvtColor(SpecialTracker::photo, hsv, CV_BGR2HSV);
 
  // Get the selected pixel
  cv::Vec3b pixel = hsv.at<cv::Vec3b>(y,x);
 
  // Change the value of the tracked color with the color of the selected pixel
  h = static_cast<int>(pixel.val[0]);
  s = static_cast<int>(pixel.val[1]);
  v = static_cast<int>(pixel.val[2]);

  std::cout <<"H = " << h <<",  S = " << s <<",  V = " << v << std::endl;

  // Release the memory of the hsv image
  hsv.release();
 
}

