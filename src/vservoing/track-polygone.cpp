#include "track-polygone.h"
#include <cmath>
#include <algorithm> 

using namespace Eigen;
using namespace std;

TrackPoly::TrackPoly(int n, bool closed):
  SobelTracker(),
  N(n),
  length(0),
  liste(vector<cv::Point>()),
  boucle(closed)
{
  if( N<=2 ) N = 2;
  limiteClicks = N;
  seuil = 15;
  pas = 1;
  marge = 3;
}
 
TrackPoly::~TrackPoly(){
}

void TrackPoly::resetClicks(){
  hasStarted = false;
  nombreClicks = 0;
  liste.clear();
}


void TrackPoly::answerOnClick(int x, int y){
  if(nombreClicks <= N){
    cv::Point epingle(x,y);
    liste.push_back(epingle);
  }
  if(nombreClicks == N && liste.size() >= N){

    if(display!=NULL){
      
      //cv::Mat contours = sobel(display->getImage());
  
      //stepTracking(contours);

    }
  }
}

void TrackPoly::stepTracking(const cv::Mat & img){

  if(liste.size()!=N)
    return;


  double scoreMax = 0;

  double longueur = 0;
  
  double sC = 0;

  cv::Point pp(320,240);

  cv::Point avant(320,240);

  cv::Point apres(320,240);

  unsigned int before,after;

  for(unsigned int k = 0; k < N; k++){ 

    pp = liste.at(k);

    before = k-1;
    if(k == 0){
      if(boucle) 
	before = N - 1;
      else 
	before = 0;
      avant = liste.at(before);
    
    }
    else{
      before = k;
      avant = pp;
    }
      
    after = (k+1)%N;
    apres = liste.at(after);

    int x,y,u,v;

    x = pp.x;

    y = pp.y;

    u = x;
    v = y;

    scoreMax = 0;
   

    for(int i1 = -marge; i1 <marge+1; i1++){

      for(int j1 = -marge; j1 <marge+1; j1++){

	pp.x = (x+pas*i1);
	pp.y = (y+pas*j1);

	longueur = 1;
	
	if( pp.x>=0 
	    && pp.y>=0
	    && pp.x<=largeur
	    && pp.y<=hauteur){

	  sC = 0;
	  
	  if(before<k){
	    sC += computeScore(img,avant,pp,false);
	    longueur += distance(avant,pp);
	  } 
	  if(before == N-1){
	    sC += computeScore(img,avant,pp,false);
	    longueur += distance(avant,pp);
	  } 
	  if( k < after){
	    sC += computeScore(img,pp,apres,false);
	    longueur += distance(pp,apres);
	  }
	  
	  sC /= sqrt(longueur);

	  if(sC > scoreMax){
	    scoreMax = sC;
	    u = x + pas * i1;
	    v = y + pas * j1;
	  }
	}
	    
      }
    }
    //cout<<"Check point # "<<k<<endl;
    liste.at(k).x = u;
    liste.at(k).y = v;
  }

  score = 0;

  length = 0;

  for(unsigned int i = 1; i < N; i++){
    
    score  += computeScore(img,liste.at(i-1),liste.at(i),false);
    length += distance(liste.at(i-1),liste.at(i));
  }

  if(boucle){

    score  += computeScore(img,liste.at(0),liste.at(N-1),false);
    length += distance(liste.at(0),liste.at(N-1));
  
  }

  score /= sqrt(length);

}

void TrackPoly::findFeatures(const Eigen::VectorXd & feat){
  assert(feat.rows() == 2 * N);
  liste.clear();
  for(unsigned int i = 0; i < N; i++){
    cv::Point p;
    p.x =  static_cast<int>(round(feat(2*i)));
    p.y =  static_cast<int>(round(feat(2*i+1)));
    liste.push_back(p);
  }
}


void TrackPoly::displayLine(cv::Point A, cv::Point B, cv::Mat & img) const{
  line(img,A,B,cv::Scalar(255,255,255),1,8);
}

int TrackPoly::getDimensionOutput() const{
  return 2*N;
}

bool TrackPoly::itCanBeSeen() const{
  return true;
}

bool TrackPoly::isLost() const{
  return score < 0.5;
}

VectorXd TrackPoly::getFunction() const{
  VectorXd F = VectorXd::Zero(2*N);
  if(liste.size()>=N){
    for(unsigned int i = 0; i < N; i++){
      F.block<2,1>(2*i,0) << liste.at(i).x, liste.at(i).y;
    }
  }
  return F;
}  
 
void TrackPoly::dessiner(cv::Mat & img) const{

  for(unsigned int i = 1; i < liste.size(); i++){
    
    displayLine(liste.at(i-1),liste.at(i),img);

  }

  if(boucle){

    displayLine(liste.at(0),liste.at(N-1),img);

  }
  
}

void TrackPoly::draw(CVEcran * const vue, cv::Scalar couleur) const{
  dessiner(vue->getImage());
}

void TrackPoly::drawFeat(const VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const{
  MultiPointFeature mpf(MatrixXd::Identity(3,3));
  mpf.drawFeat(value,vue,couleur);
}
