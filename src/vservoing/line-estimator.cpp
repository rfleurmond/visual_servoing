#include "line-estimator.h"

using namespace Eigen;


LineEstimator::LineEstimator(VisualFeature * model, VTracker * tracker):
  FeatureEstimator(model,tracker),
  phi(MatrixXd::Zero(3,3)),
  beta(VectorXd::Zero(3))
{
  assert(model!=NULL);
  assert(tracker!=NULL);


  estimation = VectorXd::Zero(6);
  frame = modele->getSituation();
  oeil = tracker->getEye();

}

void LineEstimator::computeEstimation(){
  
  /**
     \n
     Detail of calculation and implementation
  */

  /// \f$ C_i \f$ is the current position of the optical center
  /// of the camera with respect to the target frame

  Vector3d CI = frame.getPointInME(oeil->getRepere().getDeplacement());
  
  /// Get information form tracker

  /// \f$[\rho, \ \theta ] \leftarrow F \f$

  VectorXd F = traceur->getFunction();
  
  Vector2d RhoTheta = F.block<2,1>(0,0);
  
  /// Compute two points on the line on image plane
  
  double rho = RhoTheta(0);
  double theta = RhoTheta(1);

  double x,y;
  Vector2d imgi, centre;
  IntrinsicCam optique = oeil->getIntrinsicParameters();
  centre = optique.getCenter();
  
  double k1,k2;

  double cosa,sina;

  cosa = cos(theta);
  sina = sin(theta);

  k1 = 0;
  k2 = 0.25 * optique.alphaU;

  x = rho * cosa - k1 * sina;
  y = rho * sina + k1 * cosa;

  imgi << x,y ;
  imgi += centre;

  Vector3d U1 = frame.getVectorInME(oeil->getCleanDirection(imgi));

  x = rho * cosa - k2 * sina;
  y = rho * sina + k2 * cosa;

  imgi << x,y ;
  imgi += centre;

  /// Compute the 2 corresponding direction vector

  /// Compute a normal \f$ N_i \f$ to the line by the optical Center

  /// \f[ N_i = \frac{1}{\sqrt{f^2 + \rho^2_i}} {\begin{bmatrix} f \cos{\theta_i} & f \sin{\theta_i} & -\rho_i \end{bmatrix}}^T \f]

  Vector3d U2 = frame.getVectorInME(oeil->getCleanDirection(imgi));

  Vector3d NI = U1.cross(U2).normalized();

  /// Compute a projector \f$ P_i = N_i \times N^T_i \f$ 
 
  /**
    
    integrates the last information

    then make the computation of estimation
    
    \f{eqnarray*}{
       \phi    & = & \phi  + P_i \\
       \beta   & = & \beta + P_i \cdot C_i
    \f}
    
    LineEstimator::resoudre (\f$ \phi \f$, \f$ \beta \f$, \f$ \widehat{P} \f$, \f$ \widehat{U} \f$)

    Estimation = \f$ \begin{bmatrix} \widehat{P} \\ \widehat{U} \end{bmatrix} \f$
 */


  MatrixXd PI = NI * NI.transpose();
  
  phi  +=  PI;
  beta.noalias() +=  PI * CI;
  
  VectorXd PP, UU;

  resoudre(phi,beta,PP,UU);

  estimation.block<3,1>(0,0) = PP;

  estimation.block<3,1>(3,0) = UU;
  
}


double LineEstimator::resoudre(const MatrixXd & A, 
			       const VectorXd & B,
			       VectorXd & P,
			       VectorXd & U){

  JacobiSVD<MatrixXd> svdA(A,ComputeThinU | ComputeThinV);

  double ratio = (svdA.singularValues()(1) - svdA.singularValues()(2))/svdA.singularValues()(0);

  //std::cout << "Ratio de qualité: " << 100 * ratio << std::endl;

  //std::cout << "Ratio de qualité: " << svdA.singularValues()(2)/svdA.singularValues()(0) << std::endl;

  U = svdA.matrixV().col(2);

  U = U.normalized();

  // Remove the lowest eigen valeur  in SVD decomposition

  MatrixXd vSingular = svdA.singularValues();

  VectorXd vPseudoInvertedSingular(svdA.matrixV().cols(),1);
  
  for (int iRow =0; iRow<vSingular.rows(); iRow++)     {
    if(iRow == 2){ 
      vPseudoInvertedSingular(iRow,0)= 0.;
    }
    else if ( fabs(vSingular(iRow))<=1e-6 ){ // Todo : Put epsilon in parameter
      vPseudoInvertedSingular(iRow,0)= 0.;
    }
    else{
      vPseudoInvertedSingular(iRow,0)=1./vSingular(iRow);
    }
  }

  MatrixXd mAdjointU = 
    svdA.matrixU().adjoint().block(0,0,vSingular.rows(),svdA.matrixU().adjoint().cols());

    // Pseudo-Inversion : V * S * U'
  P = (svdA.matrixV() *  vPseudoInvertedSingular.asDiagonal()) * mAdjointU * B;

  //*/

  //P = svdA.solve(B);
  
  MatrixXd proj(MatrixXd::Identity(3,3));
  proj.noalias() -= U * U.transpose();

  P = proj * P;

  return ratio;

}



/**
 6 premieres colonnes pour la pose de la caméra
 6 suivantes pour la pose de l'objet visuel
 toutes les autres pour les coordonnees des points;
*/
/*
MatrixXd  LineEstimator::getEstimatedJacobian() const{
  MatrixXd jacobienne = MatrixXd::Zero(3,18);
  
  EstimatedPoint ep1(point);
  EstimatedPoint ep2(point+direction);
  ep1.setSituation(frame);
  ep2.setSituation(frame);
  ep1.attachToCamera(oeil);
  ep2.attachToCamera(oeil);
  MatrixXd J1 = ep1.getEstimatedJacobian();
  MatrixXd J2 = ep2.getEstimatedJacobian();
  MatrixXd RT = LineFeature::computeJacobianRhoTeta(
						     (Vector2d)ep1.getFunction(),
						     (Vector2d)ep2.getFunction());
  VectorXd FT = LineFeature::computeRhoTeta(
					     (Vector2d)ep1.getFunction(),
					     (Vector2d)ep2.getFunction());
 
  double rho = FT(0);
  double theta = FT(1);
  MatrixXd A = MatrixXd::Zero(1,2);
  MatrixXd B = MatrixXd::Zero(1,2);
  A <<           0,        -rho;
  B << -sin(theta),  cos(theta);

  // pose Camera
  jacobienne.block<2,6>(0,0) = 
    RT.block<2,2>(0,0) * J1.block<2,6>(0,0) +
    RT.block<2,2>(0,2) * J2.block<2,6>(0,0);

  // pose cible
  jacobienne.block<2,6>(0,6) = 
    RT.block<2,2>(0,0) * J1.block<2,6>(0,6) +
    RT.block<2,2>(0,2) * J2.block<2,6>(0,6);

  // Premier point
  jacobienne.block<2,3>(0,12) = RT.block<2,2>(0,0) * J1.block<2,3>(0,12);

  // Deuxieme point
  jacobienne.block<2,3>(0,15) = RT.block<2,2>(0,2) * J2.block<2,3>(0,12);

  ////// Concernant k

  // Pose camera
  jacobienne.block<1,6>(2,0) = A * jacobienne.block<2,6>(0,0) + B * J1.block<2,6>(0,0);
 
  // Pose cible
  jacobienne.block<1,6>(2,6) = A * jacobienne.block<2,6>(0,6) + B * J1.block<2,6>(0,6);
 
  // premier point
  jacobienne.block<1,3>(2,12) = A * jacobienne.block<2,3>(0,12) + B * J1.block<2,3>(0,12);
 
  // deuxieme point
  jacobienne.block<1,3>(2,15) = A * jacobienne.block<2,3>(0,15);

  MatrixXd TR = MatrixXd::Zero(6,6);

  TR.block<3,3>(0,0) = MatrixXd::Identity(3,3);

  TR.block<3,3>(3,0) = MatrixXd::Identity(3,3);

  //double n = direction.norm();

  //Vector3d U = direction;

  //TR.block<3,3>(3,3) = (1.0/pow(n,3))*(n*n*MatrixXd::Identity(3,3) - U*U.transpose());

  TR.block<3,3>(3,3) = MatrixXd::Identity(3,3);

  jacobienne.block<2,6>(0,12) = jacobienne.block<2,6>(0,12) * TR;

  return jacobienne;
  
}

*/
