#include "flying-system.h"

using namespace Eigen;

FlyingSystem::FlyingSystem():
  TaskSystem(),
  InverseKinematicModel(),
  frame(),
  periode(1.0/200),
  tempo(0),
  vitesse(VectorXd::Zero(6))
{

}

void FlyingSystem::setSituation(const Repere & r){
  frame = r;
}

int FlyingSystem::getDimState() const{
  return 6;
}

VectorXd FlyingSystem::getState() const{
  return frame.getParameters();
}

void FlyingSystem::setState(const VectorXd & state){
  frame.setParameters(state);
}

void FlyingSystem::takeCommand(const VectorXd & order){
  assert(order.rows()==6);
  vitesse = order;
}

void FlyingSystem::updateTime(double t){
  VectorXd etat(6);
  etat.noalias() = getState() + (t - tempo) * vitesse;
  setState(etat);
  tempo = t;
}

double FlyingSystem::getPeriod() const{
  return periode;
}

int FlyingSystem::getTotalDegres() const{
  return 6;
}

Repere FlyingSystem::getSituation(const VectorXd & q) const{
  return getSituation(q,6);
}

Repere FlyingSystem::getSituation(const VectorXd & q, int i) const{
  assert(i==6);
  Repere r(q);
  return r;
}

Vector3d FlyingSystem::getLinearVelocity(const VectorXd & q, const VectorXd & qpoint) const{
  VectorXd torseur(getKinematicJacobian(q) * qpoint);
  Vector3d velocity(torseur.block<3,1>(0,0));
  return velocity;
}

Vector3d FlyingSystem::getAngularVelocity(const VectorXd & q, const VectorXd & qpoint) const{
  VectorXd torseur(getKinematicJacobian(q)*qpoint);
  Vector3d velocity(torseur.block<3,1>(3,0));
  return velocity;
}

MatrixXd FlyingSystem::getKinematicJacobian(const VectorXd & q) const{
  return getKinematicJacobian(q,6);
}

MatrixXd FlyingSystem::getKinematicJacobian(const VectorXd & q, int i) const{
  assert(i==6);
  Repere r(q);
  MatrixXd  jacobienne = MatrixXd::Zero(6,6);
  jacobienne.block<3,3>(3,0) = r.getRotation().getInverseTimeDerivativeJacobianOmega();
  jacobienne.block<3,3>(0,3) = MatrixXd::Identity(3,3);
  return jacobienne;
}

bool FlyingSystem::getCommandFromVelocity(const VectorXd & q, const VectorXd & torseur, VectorXd & commande) const{
  assert(getTotalDegres()!=0);
  MatrixXd jacobienne(getKinematicJacobian(q));
  double erreur = LeastSquares::solve(torseur,jacobienne,commande);
  return (erreur<CRAYON); 
}

bool FlyingSystem::getCommandFromLinearVelocity(const VectorXd & q, const Vector3d & linear, VectorXd & commande) const{
  assert(getTotalDegres()!=0);
  MatrixXd jacobienne(getKinematicJacobian(q));
  VectorXd ordre = VectorXd::Zero(getTotalDegres());
  MatrixXd JJ(jacobienne.topRows(3));
  double erreur = LeastSquares::solvePseudoInverse(linear,JJ,commande);
  return (erreur<CRAYON);
}

bool FlyingSystem::getCommandFromAngularVelocity(const VectorXd & q, const Vector3d & angular, VectorXd & commande) const{
  assert(getTotalDegres()!=0);
  MatrixXd jacobienne(getKinematicJacobian(q));
  MatrixXd JJ(jacobienne.bottomRows(3));
  double erreur = LeastSquares::solvePseudoInverse(angular,JJ,commande);
  return (erreur<CRAYON);

}
