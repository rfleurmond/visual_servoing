#include "reference-droite.h"

using namespace Eigen;
using namespace std;

AlignmentMiniLine::AlignmentMiniLine(MultiPointFeature * c1,MultiPointFeature * c2,const MatrixXi & paire){
  cible1 = c1;
  cible2 = c2;
  assoc = paire;
  isGradient = false;
  long int rows = assoc.rows();
  long int cols = assoc.cols();
  
  dimension = 0;
  for(long int i = 0; i< rows; i++){
    dimension+=1;
    if(cols>=4){
      if(assoc(i,3)>=0){
	dimension+=1;
      }
    }
  }
  
}

VectorXd AlignmentMiniLine::getValue() const{
  long int rows = assoc.rows();
  long int cols = assoc.cols();
  VectorXd image1,image2;
  image1 = cible1->getFunction();
  image2 = cible2->getFunction();
  
  int  dim = 0;
  VectorXd G = VectorXd::Zero(dimension);
  Vector2d A,B,C,D,AB;
  VectorXd V,K;
  int tot = 0;
  for(long int i = 0; i< rows; i++){
    dim = 1;
    if(cols>=4){
      if(assoc(i,3)>=0){
	dim = 2;
      }
    }
    A = image1.block<2,1>(2*assoc(i,0),0); 
    B = image1.block<2,1>(2*assoc(i,1),0); 
    C = image2.block<2,1>(2*assoc(i,2),0);
    AB = B - A;
    if(dim==1){
      V = C - Alignement::getReference(A,B,C);
      G(tot,0) = V.transpose()*V;
    }
    else if(dim==2){
      D = image2.block<2,1>(2*assoc(i,3),0);
      Vector2d S = LineFeature::computeRhoTeta(C,D);
      Vector2d Sstar = LineFeature::computeRhoTeta(A,B);
      Vector2d E = DroiteFeature::computeError(S,Sstar);
      G.block<2,1>(tot,0) = E;
    }
    tot += dim;
  }
  return G;
}

int AlignmentMiniLine::getDimension() const{
  return dimension;
}

MatrixXd AlignmentMiniLine::getJacobian() const{
  long int rows = assoc.rows();
  long int cols = assoc.cols();
  VectorXd image1,image2;
  image1 = cible1->getFunction();
  image2 = cible2->getFunction();
  
  MatrixXd J = MatrixXd::Zero(dimension,cible2->getDimensionOutput());
  
  Vector2d A,B,C,D,AB,V;
  MatrixXd AABB = MatrixXd::Zero(2,2);
  MatrixXd TR;
  double scal = 0;
  int dim = 0, tot = 0;
  for(long int i = 0; i< rows; i++){
    dim = 1;
    A = image1.block<2,1>(2*assoc(i,0),0); 
    B = image1.block<2,1>(2*assoc(i,1),0); 
    C = image2.block<2,1>(2*assoc(i,2),0);
    AB = B - A;
    scal = 1/(AB.transpose() * AB);
    AABB = AB * AB.transpose();
    TR = MatrixXd::Identity(2,2) - scal * AABB;
    
    V = C - Alignement::getReference(A,B,C);
    J.block<1,2>(tot,2*assoc(i,2)) = 2 * V.transpose() * TR;

    if(cols>=4){
      if(assoc(i,3)>=0){
	dim = 2;
	D = image2.block<2,1>(2*assoc(i,3),0);
	MatrixXd petitJ = LineFeature::computeJacobianRhoTeta(C,D);
	J.block<2,2>(tot,2*assoc(i,2)) = petitJ.block<2,2>(0,0);
	J.block<2,2>(tot,2*assoc(i,3)) = petitJ.block<2,2>(0,2);
      }
    }
    tot+=dim;
  }
  return J;
}



