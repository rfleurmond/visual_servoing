#include "virtual-snuffer.h"

using namespace Eigen;

VSnuffer::VSnuffer(VTracker * tracker, double first, double second):
  VTracker(),
  traceur(tracker),
  nbUses(NULL),
  firstCountDown(first),
  secondCountDown(second),
  x(10),
  y(10)
{
  assert(tracker!=NULL);
  assert(first>0);
  assert(second> first);
  nbUses = new int;
  *nbUses = 0;

  x = static_cast<int>(hasard(30,400));
  y = static_cast<int>(hasard(30,400));
}

VSnuffer::~VSnuffer(){
  delete nbUses;
}
  
int VSnuffer::getDimensionOutput() const{
  return traceur->getDimensionOutput();
}  

VectorXd VSnuffer::getFunction() const{
  (*nbUses)++;
  /*
  if(firstCountDown == (*nbUses))
    std::cout << "[Eteignoir]:  Cible occultee"<<std::endl;
  
  if(secondCountDown == (*nbUses))
    std::cout << "[Eteignoir]:  Cible retrouvee"<<std::endl;
  */
  return traceur->getFunction();
}  

bool VSnuffer::itCanBeSeen() const{
  return traceur->itCanBeSeen();
}

bool VSnuffer::isLost() const{
  /*
  if(firstCountDown < (*nbUses) && (*nbUses) < secondCountDown)
    return true;
  else 
    return false;
  */
  if(firstCountDown < MyClock::getTime() && MyClock::getTime() < secondCountDown)
    return true;
  else 
    return false;
  
}

void VSnuffer::attachToCamera(Camera * eye){
  oeil = eye;
  traceur->attachToCamera(eye);
}


void VSnuffer::draw(CVEcran * const vue, cv::Scalar couleur) const{
  if(isLost()){
    putText(vue->getImage(), "Tracker: Target Lost !", cv::Point(x,y), 
	    cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, couleur, 1, CV_AA);
  }
  else{
    traceur->draw(vue,couleur);
  }
}

void VSnuffer::drawFeat(const VectorXd & value,CVEcran * const vue, cv::Scalar couleur) const{
  traceur->drawFeat(value,vue,couleur);
}
