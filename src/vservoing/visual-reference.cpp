#include "visual-reference.h"

using namespace Eigen;
using namespace std;

DifferenceFeature::DifferenceFeature(VisualFeature * curr, VisualFeature * want):
  RelativeTask(),
  current(curr),
  wanteds(want),
  dimension(curr->getDimensionOutput()),
  C(MatrixXd::Identity(curr->getDimensionOutput(), curr->getDimensionOutput())),  
  K(VectorXd::Zero(curr->getDimensionOutput()))
{
  assert(curr!=NULL);
  assert(want!=NULL);
  assert(curr!=want);
  assert(curr->getDimensionOutput() == want->getDimensionOutput()); 
}

int DifferenceFeature::getDimension() const{
  return dimension;
}

VectorXd DifferenceFeature::getValue() const{
  VectorXd erreur(current->getErrorBetweenMeAnd(wanteds->getFunction()));
  erreur = C * ( erreur - K);
  return erreur;
}

bool DifferenceFeature::dependOf(VisualFeature * feat) const{
  if(current==feat){
    return true;
  }
  if(wanteds==feat){
    return true;
  }
  return false;
}

MatrixXd DifferenceFeature::getJacobian(VisualFeature * feat) const{
  MatrixXd J = MatrixXd::Zero(getDimension(),feat->getDimensionOutput());
  if(current==feat){
    J.noalias() = C * current->getJacobianError(wanteds->getFunction()); 
  }
  if(wanteds==feat){
    J.noalias() = C * current->getJacobianReferenceError(wanteds->getFunction());
  }
  return J;
}


void DifferenceFeature::setCMatrix(const MatrixXd & A){
  assert(A.cols() == current->getDimensionOutput());
  C = A;
  dimension = static_cast<int>(A.rows());
  K = VectorXd::Zero(dimension);
}

void DifferenceFeature::setKConstant(const VectorXd & A){
  assert(A.rows() == dimension);
  K = A;
}

CombinaisonFeature::CombinaisonFeature(VisualFeature * curr, VisualFeature * want):
  RelativeTask(),
  current(curr),
  wanteds(want),
  dimension(1),
  C1(MatrixXd::Identity(1,1)),
  C2(MatrixXd::Identity(1,1)),
  K(VectorXd::Ones(1))
{
  assert(curr!=NULL);
  assert(want!=NULL);
  assert(curr!=want);
  assert(curr->getDimensionOutput() == want->getDimensionOutput()); 
  dimension = curr->getDimensionOutput();
  C1 = C2  = MatrixXd::Identity(dimension,dimension);
  K = VectorXd::Zero(dimension);
}

int CombinaisonFeature::getDimension() const{
  return dimension;
}

VectorXd CombinaisonFeature::getValue() const{
  VectorXd erreur = 
    C1 * current->getFunction() +
    C2 * wanteds->getFunction() - K;
  return erreur;
}

bool CombinaisonFeature::dependOf(VisualFeature * feat) const{
  if(current==feat){
    return true;
  }
  if(wanteds==feat){
    return true;
  }
  return false;
}

MatrixXd CombinaisonFeature::getJacobian(VisualFeature * feat) const{
  MatrixXd J = MatrixXd::Zero(getDimension(),feat->getDimensionOutput());
  if(current==feat){
    J = C1; 
  }
  if(wanteds==feat){
    J = C2;
  }
  return J;
}


void CombinaisonFeature::setMatrices(const MatrixXd & A, const MatrixXd & B, const VectorXd & k){
  assert(A.cols() == current->getDimensionOutput());
  assert(B.cols() == wanteds->getDimensionOutput());
  assert(A.rows() == B.rows());
  assert(B.rows() == k.rows());
  dimension = static_cast<int>(A.rows());
  C1 = A;
  C2 = B;
  K = k;
}
