#include "line-point.h"

using namespace Eigen;
using namespace std;


LineFeature::LineFeature(const VectorXd & pA, const VectorXd & pB):
  AbstractLineFeature(),
  pointA(pA),
  pointB(pB)
{
}

int LineFeature::getDimensionState() const{
  return 6;
}
int LineFeature::getDimensionOutput() const{
  return 2;
}

bool LineFeature::itCanBeSeen() const{
  return pointA.itCanBeSeen() && pointA.itCanBeSeen(); 
}

VectorXd LineFeature::getFunction(const VectorXd & P) const{
  assert(P.rows()==6);
  IntrinsicCam optics = oeil->getIntrinsicParameters();
  Vector2d A = pointA.getFunction(P.block<3,1>(0,0)) - optics.getCenter();
  Vector2d B = pointB.getFunction(P.block<3,1>(3,0)) - optics.getCenter();
  return computeRhoTeta(A,B);
}  


VectorXd LineFeature::getFunction() const{
  IntrinsicCam optics = oeil->getIntrinsicParameters();
  Vector2d A = pointA.getFunction() - optics.getCenter();
  Vector2d B = pointB.getFunction() - optics.getCenter();
  return computeRhoTeta(A,B);
}

MatrixXd LineFeature::getNaiveInteractionMatrix(const VectorXd & F) const{
  assert(F.rows()==2);
  IntrinsicCam optics = oeil->getIntrinsicParameters();
  double rho = F(0);
  double theta = F(1);
  double cosT = cos(theta);
  double sinT = sin(theta);
  double k_1 = 0.25 * oeil->getIntrinsicParameters().alphaU;
  double k_2 = -0.25 * oeil->getIntrinsicParameters().alphaU;
  double x_1 = rho * cosT - k_1 * sinT;
  double y_1 = rho * sinT + k_1 * cosT;
  double x_2 = rho * cosT - k_2 * sinT;
  double y_2 = rho * sinT + k_2 * cosT;
  Vector2d A(x_1, y_1);
  Vector2d B(x_2, y_2);
  
  MatrixXd LX = MatrixXd::Zero(4,6);
  LX.block<2,6>(0,0) = pointA.getNaiveInteractionMatrix(A + optics.getCenter());
  LX.block<2,6>(2,0) = pointB.getNaiveInteractionMatrix(B + optics.getCenter());
  MatrixXd temp (computeJacobianRhoTeta(A,B));
  MatrixXd J(temp * LX);
  
  return J;
}
 

MatrixXd LineFeature::getInteractionMatrix(const VectorXd & P) const{
  assert(P.rows()==6);
  IntrinsicCam optics = oeil->getIntrinsicParameters();
  MatrixXd jacobien = MatrixXd::Zero(2,6);
  Vector2d A = pointA.getFunction(P.block<3,1>(0,0)) - optics.getCenter();
  Vector2d B = pointB.getFunction(P.block<3,1>(3,0)) - optics.getCenter();
  MatrixXd LX = MatrixXd::Zero(4,6);
  LX.block<2,6>(0,0) = pointA.getInteractionMatrix(P.block<3,1>(0,0));
  LX.block<2,6>(2,0) = pointB.getInteractionMatrix(P.block<3,1>(3,0));
  MatrixXd temp (computeJacobianRhoTeta(A,B));
  MatrixXd J(temp * LX);
  return J;
}
 

MatrixXd LineFeature::getInteractionMatrix() const{
  IntrinsicCam optics = oeil->getIntrinsicParameters();
  MatrixXd jacobien = MatrixXd::Zero(2,6);
  Vector2d A = pointA.getFunction() - optics.getCenter();
  Vector2d B = pointB.getFunction() - optics.getCenter();
  MatrixXd LX = MatrixXd::Zero(4,6);
  LX.block<2,6>(0,0) = pointA.getInteractionMatrix();
  LX.block<2,6>(2,0) = pointB.getInteractionMatrix();
  MatrixXd temp (computeJacobianRhoTeta(A,B));
  MatrixXd J(temp * LX);
  
  return J;
}

VectorXd LineFeature::getError(const VectorXd & S, const VectorXd & reference) const{
  return computeError(S,reference);
}

MatrixXd LineFeature::getJacobianError(const VectorXd & S,const VectorXd & R) const{
  return computeJError(S,R);
}

MatrixXd LineFeature::getJacobianReferenceError(const VectorXd & S, const VectorXd & R) const{
  return computeJRefError(S,R);;
}

  
void LineFeature::attachToCamera(Camera * eye){
  assert(eye!=NULL);
  oeil = eye;
  pointA.attachToCamera(eye);
  pointB.attachToCamera(eye);
}

void LineFeature::setSituation(const Repere & r){
  frame = r;
  pointA.setSituation(r);
  pointB.setSituation(r);
}


PointFeature LineFeature::getFirstPoint() const{
  return pointA;
}

PointFeature LineFeature::getSecondPoint() const{
  return pointB;
}


void LineFeature::draw(CVEcran * const vue, cv::Scalar couleur) const{
  pointA.drawFeat(pointA.getFunction(),vue,couleur);
  pointB.drawFeat(pointB.getFunction(),vue,couleur);
  drawFeat(getFunction(),vue,couleur);
}


void LineFeature::drawFeat(const VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const{
  dessinerDroite(oeil,value,vue,couleur);

  // Dessin d'une flèche

  double rho = value(0);
  double teta = value(1);
  double cosT = cos(teta);
  double sinT = sin(teta);
  Vector2d A = Vector2d::Zero();
  Vector2d B = Vector2d::Zero();
  Vector2d C = Vector2d::Zero();
  
  A(0) = rho * cosT - 0.25 * oeil->getAlphaU() * sinT;
  A(1) = rho * sinT + 0.25 * oeil->getAlphaU() * cosT;
  double lx = 0.04 * oeil->getAlphaU();
  double ly = 0.02 * oeil->getAlphaU();
  B(0) = A(0) + ly * cosT + lx * sinT;
  B(1) = A(1) + ly * sinT - lx * cosT;
  C(0) = A(0) - ly * cosT + lx * sinT;
  C(1) = A(1) - ly * sinT - lx * cosT;
  
  IntrinsicCam optics = oeil->getIntrinsicParameters();
  A = optics.getCenter() + A;
  B = optics.getCenter() + B;
  C = optics.getCenter() + C;
  A = vue->getPoint(A);
  B = vue->getPoint(B);
  C = vue->getPoint(C);
  
  cv::Point AA,BB,CC;
  AA.x = static_cast<int>(A(0));
  AA.y = static_cast<int>(A(1));
  BB.x = static_cast<int>(B(0));
  BB.y = static_cast<int>(B(1));
  CC.x = static_cast<int>(C(0));
  CC.y = static_cast<int>(C(1));
  line(vue->getImage(),AA,BB,couleur,1,8);
  line(vue->getImage(),AA,CC,couleur,1,8);
  
}
  

