#include "reference-alignement.h"

using namespace Eigen;

AlignmentReference::AlignmentReference(MultiPointFeature * c1,MultiPointFeature * c2,const MatrixXi & paire){
  cible1 = c1;
  cible2 = c2;
  assoc = paire;
  
}

Eigen::VectorXd AlignmentReference::getReference() const{
  long int cols, rows;
  cols = assoc.cols();
  rows = assoc.rows();
  VectorXd image1,image2;
  image1 = cible1->getFunction();
  image2 = cible2->getFunction();
  
  int  dim = 0;
  for(int i = 0; i< rows; i++){
    dim+=2;
    if(cols>=4){
      if(assoc(i,3)>=0){
	dim+=2;
      }
    }
  }
  VectorXd ref = VectorXd::Zero(dim);
  Vector2d A,B,C,D;
  int tot = 0;
  for(int i = 0; i< rows; i++){
    dim = 2;
    if(cols>=4){
      if(assoc(i,3)>=0){
	dim = 4;
      }
    }
    A = image1.block<2,1>(2*assoc(i,0),0); 
    B = image1.block<2,1>(2*assoc(i,1),0); 
    C = image2.block<2,1>(2*assoc(i,2),0); 
    if(dim==2){
      ref.block(tot,0,dim,1) = Alignement::getReference(A,B,C);
    }
    else{
      D = image2.block<2,1>(2*assoc(i,3),0); 
      ref.block(tot,0,dim,1) = Alignement::getPerpendicularReference(A,B,C,D);
    }
    tot += dim;
  }
  return ref;
}


AlignmentGradient::AlignmentGradient(MultiPointFeature * c1,MultiPointFeature * c2,const MatrixXi & paire){
  cible1 = c1;
  cible2 = c2;
  assoc = paire;
  isGradient = true;
  long int rows = assoc.rows();
  long int cols = assoc.cols();

  dimension = 0;
  for(long int i = 0; i< rows; i++){
    dimension+=2;
    if(cols>=4){
      if(assoc(i,3)>=0){
	dimension+=2;
      }
    }
  }
  
}

VectorXd AlignmentGradient::getValue() const{
  long int rows = assoc.rows();
  long int cols = assoc.cols();
  VectorXd image1,image2;
  image1 = cible1->getFunction();
  image2 = cible2->getFunction();
  
  int  dim = 0;
  VectorXd G = VectorXd::Zero(dimension);
  Vector2d A,B,C,D,AB;
  MatrixXd AABB = MatrixXd::Zero(2,2);
  MatrixXd TRANS = AABB;
  VectorXd V,K;
  double scal = 1;
  int tot = 0;
  for(long int i = 0; i< rows; i++){
    dim = 2;
    if(cols>=4){
      if(assoc(i,3)>=0){
	dim = 4;
      }
    }
    A = image1.block<2,1>(2*assoc(i,0),0); 
    B = image1.block<2,1>(2*assoc(i,1),0); 
    C = image2.block<2,1>(2*assoc(i,2),0);
    AB = B - A;
    scal = AB.transpose()*AB;
    scal = 1.0/scal;
    AABB = scal * AB * AB.transpose();
    TRANS = MatrixXd::Identity(2,2) - AABB;
    TRANS = 2 * TRANS;

    if(dim==2){
      V = C -Alignement::getReference(A,B,C);
      G.block<2,1>(tot,0) = TRANS * V;
    }
    else{
      K = VectorXd::Zero(4);
      K.block<2,1>(0,0) = C;
      D = image2.block<2,1>(2*assoc(i,3),0); 
      K.block<2,1>(2,0) = D;
      V = K -Alignement::getPerpendicularReference(A,B,C,D);
      G.block<2,1>(tot,0) = TRANS * V.block<2,1>(0,0);
      G.block<2,1>(tot+2,0) = TRANS * V.block<2,1>(2,0);
    }
    tot += dim;
  }
  return G;
}

int AlignmentGradient::getDimension() const{
  return dimension;
}

MatrixXd AlignmentGradient::getJacobian() const{
  long int rows = assoc.rows();
  long int cols = assoc.cols();
  MatrixXd J = MatrixXd::Zero(dimension,cible2->getDimensionOutput());
  int val = 0, dim = 0, tot = 0;
  for(long int i = 0; i< rows; i++){
    val = assoc(i,2);
    J.block<2,2>(tot,2*val) = Matrix2d::Identity();
    dim = 2;
    if(cols>=4){
      if(assoc(i,3)>=0){
	dim = 4;
	val = assoc(i,3);
	J.block<2,2>(tot+2,2*val) = Matrix2d::Identity();
      }
    }
    tot+=dim;
  }
  return J;
}



AlignmentMiniTask::AlignmentMiniTask(MultiPointFeature * c1,MultiPointFeature * c2,const MatrixXi & paire){
  cible1 = c1;
  cible2 = c2;
  assoc = paire;
  isGradient = false;
  long int rows = assoc.rows();
  long int cols = assoc.cols();
  
  dimension = 0;
  for(long int i = 0; i< rows; i++){
    dimension+=1;
    if(cols>=4){
      if(assoc(i,3)>=0){
	dimension+=1;
      }
    }
  }
  
}

VectorXd AlignmentMiniTask::getValue() const{
  long int rows = assoc.rows();
  long int cols = assoc.cols();
  VectorXd image1,image2;
  image1 = cible1->getFunction();
  image2 = cible2->getFunction();
  
  int  dim = 0;
  VectorXd G = VectorXd::Zero(dimension);
  Vector2d A,B,C,D,AB;
  VectorXd V,K;
  int tot = 0;
  for(long int i = 0; i< rows; i++){
    dim = 1;
    if(cols>=4){
      if(assoc(i,3)>=0){
	dim = 2;
      }
    }
    A = image1.block<2,1>(2*assoc(i,0),0); 
    B = image1.block<2,1>(2*assoc(i,1),0); 
    C = image2.block<2,1>(2*assoc(i,2),0);
    AB = B - A;
    V = C - Alignement::getReference(A,B,C);
    G(tot,0) = V.transpose()*V;
    if(dim==2){
      D = image2.block<2,1>(2*assoc(i,3),0); 
      V = D -Alignement::getReference(A,B,D);
      G(tot+1,0) = V.transpose()*V;
    }
    tot += dim;
  }
  return G;
}

int AlignmentMiniTask::getDimension() const{
  return dimension;
}

MatrixXd AlignmentMiniTask::getJacobian() const{
  long int rows = assoc.rows();
  long int cols = assoc.cols();
  VectorXd image1,image2;
  image1 = cible1->getFunction();
  image2 = cible2->getFunction();
  
  MatrixXd J = MatrixXd::Zero(dimension,cible2->getDimensionOutput());
  
  Vector2d A,B,C,D,AB,V;
  MatrixXd AABB = MatrixXd::Zero(2,2);
  MatrixXd TR;
  double scal = 0;
  int dim = 0, tot = 0;
  for(long int i = 0; i< rows; i++){
    dim = 1;
    A = image1.block<2,1>(2*assoc(i,0),0); 
    B = image1.block<2,1>(2*assoc(i,1),0); 
    C = image2.block<2,1>(2*assoc(i,2),0);
    AB = B - A;
    scal = 1/(AB.transpose() * AB);
    AABB = AB * AB.transpose();
    TR = MatrixXd::Identity(2,2) - scal * AABB;
    
    V = C - Alignement::getReference(A,B,C);
    J.block<1,2>(tot,2*assoc(i,2)) = 2 * V.transpose() * TR;

    if(cols>=4){
      if(assoc(i,3)>=0){
	dim = 2;
	D = image2.block<2,1>(2*assoc(i,3),0);
	V = D - Alignement::getReference(A,B,D);
	J.block<1,2>(tot+1,2*assoc(i,3)) = 2 * V.transpose() * TR;
      }
    }
    tot+=dim;
  }
  return J;
}

