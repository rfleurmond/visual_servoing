#include "icylinder-estimator.h"

using namespace Eigen;

ICylinderEstimator::ICylinderEstimator(VisualFeature * model, VTracker * tracker):
  FeatureEstimator(model,tracker),
  n(0),
  phi(MatrixXd::Zero(3,3)),
  beta(VectorXd::Zero(3)),
  gamma(VectorXd::Zero(3)),
  mu(0),
  radius(1),
  knowRadius(false)
{
  assert(model!=NULL);
  assert(tracker!=NULL);
  estimation = VectorXd::Zero(7);
  estimation(0) = radius;
  estimation(6) = 1;
  frame = modele->getSituation();
  oeil = tracker->getEye();

}

void ICylinderEstimator::setRadius(double r){
  if(r>0){
    radius = r;
    knowRadius = true;
  }
}


Vector3d ICylinderEstimator::getNormal(double rho, double theta) const{
  
  double cosa = cos(theta);
  double sina = sin(theta);

  double x,y;

  Vector2d centre = oeil->getIntrinsicParameters().getCenter();

  x = rho * cosa;
  y = rho * sina;
  
  Vector2d imgi(x,y);
  imgi += centre;

  Vector3d U1 = frame.getVectorInME(oeil->getCleanDirection(imgi));

  double k = 0.25 * oeil->getIntrinsicParameters().alphaU;

  x = rho * cosa - k * sina;
  y = rho * sina + k * cosa;
  
  imgi << x, y;
  imgi += centre;

  Vector3d U2 = frame.getVectorInME(oeil->getCleanDirection(imgi));

  Vector3d N = U1.cross(U2).normalized();

  return N;
  
}

void ICylinderEstimator::computeEstimation(){

  Vector3d CI = frame.getPointInME(oeil->getRepere().getDeplacement());

  VectorXd F = traceur->getFunction();

  /// @see Cylinder::getFunction()\n

  /// F is the vector of visual features cues describing the cylinder
  /// \f[ F \rightarrow \begin{bmatrix} \rho_1 \\ \theta_1 \\ \rho_2 \\ \theta_2 \end{bmatrix} \f]
  

  /// Tranform the last measurements and update matrices, vectors and scalars

  /**
     \f$C_i \f$ is the current position of the optical center of the camera in target frame
     
     we make the transformation
     
     \f$ [\rho_1, \ \theta_ 1]  \rightarrow \f$ getNormal() \f$ \rightarrow N \f$ \n
     \f$ [\rho_2, \ \theta_ 2]  \rightarrow \f$ getNormal() \f$ \rightarrow Q \f$
     
     \f{eqnarray*}
        \phi_i   & = & N \cdot N^T + Q \cdot Q^T\\
	\gamma_i & = &  N - Q \\
	\phi     & = & \phi + \phi_i\\
	\beta    & = & \beta + \phi_i \cdot C_i\\
	\gamma   & = & \gamma + \gamma_i\\
	\mu      & = & \mu + \gamma^T_i \cdot C_i\\
	n        & = & n + 1
     \f}

   */

  
  double rho = F(0);
  double theta = F(1);
  
  Vector3d NI = getNormal(rho,theta);

  rho = F(2);
  theta = F(3);

  Vector3d QI = getNormal(rho,theta);

  MatrixXd AB = NI * NI.transpose();

  AB.noalias() += QI * QI.transpose();
  
  Vector3d betaI = AB * CI;

  Vector3d gammaI = QI - NI;

  double muI = CI.dot(gammaI);


  phi += AB ;

  beta += betaI;

  gamma += gammaI;

  mu += muI;

  n += 1;

  /** Compute the estimation
      
      \f$ U \f$ is the eigenvector corresponding to the smallest eigenvalue in \f$ \phi \f$

      if the radius \f$r\f$ is known 
      
      \f$ P = \phi^+ (\beta + r \cdot \gamma) \f$

      otherwise

      \f$ P = {(4 \phi - \frac{2}{n} \gamma \gamma^T)}^+ (4 \beta + \frac{1}{n} \gamma \gamma^T \cdot U - \frac{2}{n} \mu \cdot \gamma) \f$

      \f$ r = \frac{1}{4n} (\gamma^T (2 P +U ) - 2 \mu )\f$
   */

  VectorXd P, U;

  double r = radius;

  LineEstimator::resoudre(phi, beta + r * gamma , P, U);

  MatrixXd PROJ(MatrixXd::Identity(3,3));
  PROJ.noalias() -= U * U.transpose(); 

  if(knowRadius==false){
    
    double in = 1.0/n;

    MatrixXd TETA = 4 * phi;

    TETA.noalias() -= 2 * in * gamma * gamma.transpose();

    VectorXd Y = 4 * beta ;
    Y.noalias() += in * gamma * gamma.transpose() * U  - 2 * in * mu * gamma ;

    P = pinvSolve(TETA,Y);

    P = PROJ * P;

    //LineEstimator::resoudre(TETA, Y, P, U);

    r = 0.25 * in * ( gamma.dot(2 * P + U) - 2 * mu );

    if( r< 0) r = - r;

    if(isEqual(r,0,1e-3)){
      std::cout <<" Probleme !!! R == 0 !!!  " << r << std::endl;
      r = 1e-3;
    }

  }


  estimation(0) = r;

  estimation.block<3,1>(1,0) = P;

  estimation.block<3,1>(4,0) = U;

  VectorXd FE = modele->getFunction(estimation);
 
  VectorXd EF = modele->getError(F,FE);
  
  double eT = EF(1);

  if( eT> 0.5* M_PI || eT < - 0.5* M_PI){
    
    estimation.block<3,1>(4,0) = -U;

  }

}


