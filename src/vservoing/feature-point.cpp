#include "feature-point.h"

using namespace Eigen;

PointFeature::PointFeature(Vector3d p):
  VisualFeature(),
  point(p)
{

}


int PointFeature::getDimensionState() const{
  return 3;
}

int PointFeature::getDimensionOutput() const{
  return 2;
}

VectorXd PointFeature::getFunction(const VectorXd & p) const{
  assert(p.rows()== getDimensionState());
  Vector3d pp =  p.block<3,1>(0,0);
  VectorXd image = VectorXd::Zero(2);
  Vector3d pointM = frame.getPointInParent(pp);
  image = oeil->getCleanImage(pointM);
  return image;
}

VectorXd PointFeature::getFunction() const{
  return getFunction(point);
}

bool PointFeature::itCanBeSeen() const{
  Vector3d pointM = frame.getPointInParent(point);
  if(oeil->canSee(pointM)==false){
    /*
    std::cout<<"Point in target frame: "<<point.transpose()<<std::endl;
    std::cout<<"Point in world  frame: "<<pointM.transpose()<<std::endl;
    std::cout<<"Point in camera frame: "<<oeil->getRepere().getPointInME(pointM).transpose()<<std::endl;
    std::cout<<"Repere de la camera:\n"<<oeil->getRepere()<<std::endl;
    std::cout<<"Repere du marqueur :\n"<<frame<<std::endl;
    std::cout<<"\n"<<std::endl;
    */
    return false;
  }
  return true;
}


MatrixXd PointFeature::getNaiveInteractionMatrix(const VectorXd & F) const{
  assert(F.rows() == getDimensionOutput());
  double fU = oeil->getSigneU()*oeil->getAlphaU(); 
  double fV = oeil->getSigneV()*oeil->getAlphaV();
  IntrinsicCam optics = oeil->getIntrinsicParameters();
  Vector2d trou = F.block<2,1>(0,0) - optics.getCenter();
  double x = trou(0);
  double y = trou(1);
  double Z = 1;
  Vector3d Pc(x,y,Z);;
  MatrixXd J1 = MatrixXd::Zero(2,3);
  MatrixXd J2 = MatrixXd::Zero(3,6);
  J2.block<3,3>(0,0) = -MatrixXd::Identity(3,3);
  J2.block<3,3>(0,3) = frame.getRotation().getSkewMatrix(Pc);
  J1.col(oeil->getDirectionU()) << fU/Z,    0;
  J1.col(oeil->getDirectionV()) <<    0, fV/Z;
  J1.col(oeil->getDirectionZ()) << -x/Z, -y/Z;
  MatrixXd J(2,6);
  J.noalias() = J1 * J2;
  return J;
}


MatrixXd PointFeature::getInteractionMatrix(const VectorXd & Param) const{
  assert(Param.rows() == getDimensionState());
  Vector3d P = Param.block<3,1>(0,0);
  Vector3d pointM = frame.getPointInParent(P);
  Repere rC = oeil->getRepere();
  IntrinsicCam optics = oeil->getIntrinsicParameters();
  Vector2d trou = oeil->getCleanImage(pointM) - optics.getCenter();
  
  Vector3d Pc = rC.getPointInME(pointM);
  double fU = oeil->getSigneU()*oeil->getAlphaU(); 
  double fV = oeil->getSigneV()*oeil->getAlphaV();
  double x = trou(0);
  double y = trou(1);
  double Z = Pc(oeil->getDirectionZ());
  MatrixXd J1 = MatrixXd::Zero(2,3);
  MatrixXd J2 = MatrixXd::Zero(3,6);
  J2.block<3,3>(0,0) = -MatrixXd::Identity(3,3);
  J2.block<3,3>(0,3) = frame.getRotation().getSkewMatrix(Pc);
  J1.col(oeil->getDirectionU()) << fU/Z,    0;
  J1.col(oeil->getDirectionV()) <<    0, fV/Z;
  J1.col(oeil->getDirectionZ()) << -x/Z, -y/Z;
  MatrixXd J(2,6);
  J.noalias() = J1 * J2;
  return J;
}
  

MatrixXd PointFeature::getInteractionMatrix() const{
  return getInteractionMatrix(point);
}

VectorXd PointFeature::getError(const VectorXd & S, const VectorXd & autre) const{
  return S-autre;
}
 
void PointFeature::drawFeat(const VectorXd & value,CVEcran * const vue, cv::Scalar couleur) const{
  Vector2d I = value.block<2,1>(0,0);
  I = vue->getPoint(I);
  int x,y;
  x = static_cast<int>(I(0));
  y = static_cast<int>(I(1));
  cv::circle(vue->getImage(),cv::Point(x,y),3,couleur,-1);
}
