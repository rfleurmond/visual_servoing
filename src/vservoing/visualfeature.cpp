#include "visualfeature.h"

using namespace Eigen;

VisualFeature::VisualFeature():
  VTracker(),
  strategy(VisualFeature::PINV),
  l_star(MatrixXd::Zero(1,6)),
  hasLStar(false)
{

}

void VisualFeature::setSituation(const Repere & r){
  frame =r;
} 

void VisualFeature::setReferenceInteractionMatrix(const MatrixXd & lstar){
  hasLStar = false;
  assert(lstar.cols()==6);
  assert(lstar.rows()==getDimensionOutput());
  l_star = lstar;
  hasLStar = true;
}

Repere VisualFeature::getSituation() const{
  return frame;
}


void VisualFeature::setStrategy(int sta){
  switch(sta){
  case VisualFeature::STAR: strategy = sta; break; 
  case VisualFeature::PINV: strategy = sta; break; 
  case VisualFeature::MIXE: strategy = sta; break; 
  default: strategy = VisualFeature::PINV;
  } 
  
}

MatrixXd VisualFeature::getInverseInteractionMatrix() const{

  MatrixXd l_curr = getInteractionMatrix();

  MatrixXd L_plus = l_curr;

  MatrixXd LEE;

  switch(VisualFeature::strategy){
  case VisualFeature::STAR: (hasLStar)? LEE = l_star : LEE = l_curr; break; 
  case VisualFeature::MIXE: (hasLStar)? LEE = 0.5*(l_curr+l_star) : LEE = l_curr;break; 
  case VisualFeature::PINV: LEE = l_curr; break; 
  default : LEE = l_curr;
  }

  pinv(LEE,L_plus);

  return L_plus;
  
}

VectorXd VisualFeature::getTorseurCinematique(double lambda, const VectorXd & erreur) const{

  MatrixXd LE_plus(getInverseInteractionMatrix());

  VectorXd commande(6);

  VectorXd tmp(pinvSolve(getJacobianError(erreur),erreur));

  commande.noalias()=  -1 * lambda * LE_plus * tmp ;

  return commande;

}

VectorXd VisualFeature::getErrorBetweenMeAnd(const VectorXd & SS) const{

  return getError(getFunction(),SS);

}

MatrixXd VisualFeature::getJacobianError(const VectorXd & S, const VectorXd & SS) const{

  int n = getDimensionOutput();

  return MatrixXd::Identity(n,n);

}


MatrixXd VisualFeature::getJacobianReferenceError(const VectorXd & S, const VectorXd & SS) const{

  int n = getDimensionOutput();

  return -MatrixXd::Identity(n,n);

}

MatrixXd VisualFeature::getJacobianError(const VectorXd & SS) const{

  return getJacobianError(getFunction(),SS);

}


MatrixXd VisualFeature::getJacobianReferenceError(const VectorXd & SS) const{

  return getJacobianReferenceError(getFunction(),SS);

}

void VisualFeature::draw(CVEcran * const vue, cv::Scalar couleur) const{

  drawFeat(getFunction(),vue,couleur);

}
