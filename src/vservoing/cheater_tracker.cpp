#include "cheater_tracker.h"

using namespace Eigen;
using namespace std;

CheaterTracker::CheaterTracker(VTracker * good,VTracker * bad):
  VTracker(),
  slaveTracker(good),
  showOffTracker(bad)
{
  assert(good!=NULL);
  assert(bad!=NULL);
  assert(good->getDimensionOutput() == bad->getDimensionOutput());
}

void CheaterTracker::attachToCamera(Camera * eye){
  assert(eye!=NULL);
  oeil = eye;
  showOffTracker->attachToCamera(eye);
  slaveTracker->attachToCamera(eye);
}


int CheaterTracker::getDimensionOutput() const{
  return slaveTracker->getDimensionOutput();
}

bool CheaterTracker::itCanBeSeen() const{
  return slaveTracker->itCanBeSeen();
}

bool CheaterTracker::isLost() const{
  return slaveTracker->isLost();
}

VectorXd CheaterTracker::getFunction() const{
  return slaveTracker->getFunction();
}


void CheaterTracker::findFeatures(const VectorXd & SS){
  showOffTracker->findFeatures(SS);
}  

void CheaterTracker::draw(CVEcran * const vue, cv::Scalar couleur) const{
  slaveTracker->draw(vue,couleur);
}

void CheaterTracker::drawFeat(const VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const{
  showOffTracker->drawFeat(value,vue,couleur);
}
