#include "alignement.h"

using namespace Eigen;
using namespace std;


VectorXd Alignement::getReference(Vector2d A,Vector2d B,Vector2d C){
  Vector2d AC,AB,ACstar,Cstar;
  double nume,deno;
  AB = B-A;
  AC = C-A;
  deno = AB.transpose()*AB;
  nume = AC.transpose()*AB;
  ACstar = AB * nume / deno;
  Cstar = ACstar + A;
  return static_cast<VectorXd> (Cstar);
}

VectorXd Alignement::getReference(Vector2d A,Vector2d B,Vector2d C, Vector2d D){
  double epsilon = 1e-6;
  Vector2d AB,AC,CD,AD,I,IC,ICstar,ID,Astar,Cstar,Dstar;
  Vector3d ABB,CDD,ACC,ABCD,CDAC;
  VectorXd CDstar = VectorXd::Zero(4);
  double nume,deno,lambda;
  AB = B-A;
  AC = C-A;
  CD = D-C;
  AD = D-A;
  ABB = Vector3d::Zero();
  ABB.block<2,1>(0,0) = AB;
  ACC = Vector3d::Zero();
  ACC.block<2,1>(0,0) = AC;
  CDD = Vector3d::Zero();
  CDD.block<2,1>(0,0) = CD;
  ABCD = ABB.cross(CDD);
  
  if(ABCD.norm()<=epsilon){
    deno = AB.transpose()*AB;
    nume = AC.transpose()*AB;
    Astar = AB * nume / deno;
    Cstar = Astar + A;
    nume = AD.transpose()*AB;
    Astar = AB * nume / deno;
    Dstar = Astar + A;
    //cout <<"Segments paralleles"<<endl;
  }
  else{
    CDAC = CDD.cross(ACC);
    lambda = -CDAC(2)/ABCD(2);
    I = AB*lambda +A;
    //cout <<"I = "<<I.transpose()<<endl;
    IC = C-I;
    ID = D-I;
    lambda = AB.transpose()*CD;
    Matrix2d rot;
    rot << 0, -1, 1, 0;
    if(isEqual(lambda,0,epsilon)){
      Cstar = rot * IC + I;
      Dstar = rot * ID + I;
      //cout <<"Segments perpendiculaires"<<endl;
    }
    
    else{
      nume  = IC.transpose()*AB;
      deno  = AB.norm()*sqrt(nume*nume)/IC.norm();
      Cstar = AB*nume/deno + I;
      
      nume  = ID.transpose()*AB;
      deno  = AB.norm()*sqrt(nume*nume)/ID.norm();
      Dstar = AB*nume/deno + I;
      //cout <<"Segments secants"<<endl;
    }
  }

  
  CDstar.block<2,1>(0,0) = Cstar;
  CDstar.block<2,1>(2,0) = Dstar;
    
  return CDstar;

}


VectorXd Alignement::getPerpendicularReference(Vector2d A,Vector2d B,Vector2d C, Vector2d D){
  Vector2d AB,AC,CD,AD,Astar,Cstar,Dstar;
  VectorXd CDstar = VectorXd::Zero(4);
  double nume,deno;
  AB = B-A;
  AC = C-A;
  CD = D-C;
  AD = D-A;
  
  deno = AB.transpose()*AB;
  nume = AC.transpose()*AB;
  Astar = AB * nume / deno;
  Cstar = Astar + A;
  nume = AD.transpose()*AB;
  Astar = AB * nume / deno;
  Dstar = Astar + A;
  
  CDstar.block<2,1>(0,0) = Cstar;
  CDstar.block<2,1>(2,0) = Dstar;
    
  return CDstar;

}

