#include "multi-point-estimator.h"

using namespace Eigen;

MultiPointEstimator::MultiPointEstimator(VisualFeature * model, VTracker * tracker):
  FeatureEstimator(model,tracker)
{
  assert(model!=NULL);
  assert(tracker!=NULL);

  n = modele->getDimensionState()/3;
  phi = MatrixXd::Zero(3 * n,3);
  beta = VectorXd::Zero(3 * n);
  estimation = VectorXd::Zero(3 * n);
}

void MultiPointEstimator::eraseMemory(){
  phi = MatrixXd::Zero(3 * n,3);
  beta = VectorXd::Zero(3 * n);
  estimation = VectorXd::Zero(3 * n);
}

void MultiPointEstimator::computeEstimation(){

  /**
     \n

     Detail of calculation and implementation

    @see PointEstimator::computeEstimation()


  \f$ C_i \f$ is the current position of the optical center
   of the camera with respect to the target frame
   
   \f$ I_i \f$ is the image of the point and \f$ U_i \f$ the corresponding direction vector
   
   \f$ P_i  = I_3 - U_i \cdot U^T_i \f$ built projector for \f$ U_i \f$

   integrates the last informations

       \f{eqnarray*}{
       \phi    & = & \phi  + P_i \\
       \beta   & = & \beta + P_i \cdot C_i
    \f}


    then make the computation of estimation

    
    \f{eqnarray*}{
       \widehat{P} & = & {\phi}^{+} \times \beta
    \f}

    Estimation = \f$ \widehat{P} \f$

  
  */
  Vector3d CI = frame.getPointInME(oeil->getRepere().getDeplacement());
  VectorXd IN = traceur->getFunction();
  Vector2d I;
  Vector3d UI;
  MatrixXd PI;
  VectorXd X,Y;
  MatrixXd A;
  for(int i = 0; i < n; i++){
    I = IN.block<2,1>(2*i,0);
    UI = frame.getVectorInME(oeil->getCleanDirection(I));
    PI = MatrixXd::Identity(3,3) - UI * UI.transpose();
    
    phi.block<3,3>(3*i,0) += PI;
    A = phi.block<3,3>(3*i,0);
    
    beta.block<3,1>(3*i,0) += PI * CI;
    Y = beta.block<3,1>(3*i,0);
    
    X = A.ldlt().solve(Y);
    estimation.block<3,1>(3*i,0) = X;
  }
}


void MultiPointEstimator::computeSpecialEstimation(VectorXd input){

  Vector3d CI = frame.getPointInME(oeil->getRepere().getDeplacement());
  VectorXd IN = input;
  Vector2d I;
  Vector3d UI;
  MatrixXd PI;
  VectorXd X,Y;
  MatrixXd A;
  for(int i = 0; i < n; i++){
    I = IN.block<2,1>(2*i,0);
    UI = frame.getVectorInME(oeil->getCleanDirection(I));
    PI = MatrixXd::Identity(3,3) - UI * UI.transpose();
    
    phi.block<3,3>(3*i,0) += PI;
    A = phi.block<3,3>(3*i,0);
    
    beta.block<3,1>(3*i,0) += PI * CI;
    Y = beta.block<3,1>(3*i,0);
    
    X = A.ldlt().solve(Y);
    estimation.block<3,1>(3*i,0) = X;
  }
}

