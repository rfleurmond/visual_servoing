#include "polynome.h"
#include <algorithm>

using namespace std;
using namespace Eigen;

void polynome::init(int n, double * p){

  if(n<0){
    degre = 0;
    coeff = VectorXd::Zero(1);
  }
  else{
    degre = n-1;
    coeff = VectorXd::Zero(n);
    for(int i=0;i<n;i++){
      coeff(i) = p[i];
      if(isEqual(p[i],0,1e-10)){
	coeff(i) = 0;
      }
    }

  }
  
  ajusteDegre();
}


void polynome::init(const VectorXd & c){

  degre = c.rows()-1;
  coeff = c;
  ajusteDegre();

}
  

polynome::polynome(){

  degre = 0;

  coeff = VectorXd::Zero(1);

}

polynome::polynome(int n, double * p){

    init(n,p);

}

polynome::polynome(const VectorXd & c){

    init(c);
    
}
  

  
/*définition des méthodes des polynomes ******************************************/ 
void polynome::raz(void){

  coeff = VectorXd::Zero(1);

  degre=0;

  ajusteDegre();
}

void polynome::ajusteDegre(void){

  degre = coeff.rows()-1;

  double cont = coeff(0);

  while(isEqual(coeff(degre),0,1e-10)){

    degre--;

    if(degre==0) break;

  }
  coeff = coeff.block(0,0,degre+1,1);

  coeff(0) = cont;


}

void polynome::saisie(void){
//ne fonctionne que sur cin/cout, contrairement au prochain

  int i;
  cout<<"Quel est degré du polynome?";
  cin>>degre;

  double value = 0;

  coeff = VectorXd::Zero(degre+1);
  for(i=degre;i>=0;i--){
    cout <<"coef d'ordre "<<i<<" ? ";
    cin >> value;
    coeff(i)= value;
  }
  ajusteDegre();
} 


void polynome::affiche(ostream &flux){
  //flux est en argument car je veux pouvoir utiliser cout mais aussi tout fichier texte
  if(degre==0){
    flux << coeff(0);
    return;
  }
  
  double val = 0;

  for(int i = degre; i>=0; i--){
    val = coeff(i);
    if(isEqual(val,0,1e-10)==false){

      if(i==degre && val<0)
	flux <<"-";

      if(i<degre){
	if(val<0)
	  flux <<" - ";
        else
	  flux <<" + ";
      }

      if(!isEqual(val,1,1e-6) && isEqual(val,-1,1e-6) && i!=0){
	if(val<0)
	  flux <<-val;
        else
	  flux <<val;
      }

      if(i>1)
	flux <<"x^"<<i;
      if(i==1)
	flux <<"x";
      if(i==0){
	if(val<0)
	  flux <<-val;
        else
	  flux <<val;
      }
	
    }
  }
}

void polynome::copie(polynome p){

  degre=p.getDegre();
  coeff = p.getAll();

}

bool polynome::egal(polynome p){

  int i;
  ajusteDegre();
  p.ajusteDegre();
  if(degre!=p.getDegre())
    return false;

  for(i=0; i<=degre;i++)
    if(!isEqual(coeff(i),p.getCoeff(i),1e-6))
      return false;
  return true; //si on est arrivé jusqu'ici c'est qu'ils sont égaux
}
 
double polynome::valeur(double x){

  int i;

  double r = coeff(0);
  
  double p = x;
  for(i=1;i<=degre;i++)
    {
      r += coeff(i)*p;
      p *= x;
    }
  return r;
} 

void polynome::additionner(double f){

  coeff(0) += f;

}

void polynome::additionner(polynome p){

  p.ajusteDegre();
  int maximum = max(degre,p.getDegre());
  VectorXd somme = VectorXd::Zero(maximum+1);

  somme.block(0,0,degre+1,1)+=coeff;
  somme.block(0,0,p.getDegre()+1,1)+=p.getAll();

  coeff = somme;
  degre = maximum;
 
  ajusteDegre();
}

void polynome::soustraire(double f){
  coeff(0)-=f;
}

void polynome::soustraire(polynome p){ //surtout il faut le passer par valeur !
  p.multiplier(-1);
  additionner(p);
}


void polynome::multiplier(double f){
  coeff = f * coeff;
  ajusteDegre();
}

void polynome::multiplier(polynome p){
  int i,j,nd;
  
  nd=degre+p.getDegre();
  VectorXd produit = VectorXd::Zero(degre + p.getDegre()+1);
  
  for(i=0;i<=degre;i++)
    for(j=0;j<=p.getDegre();j++)
      produit(i+j)+=coeff(i)*p.getCoeff(j);

  degre=nd;
  coeff = produit;
  ajusteDegre();
}

void polynome::deriver(void){
  int i;
  VectorXd result = VectorXd::Zero(degre);

  for(i=0;i<degre;i++)
    result(i)=(i+1)*coeff(i+1);

  degre--;
  coeff = result;
}

void polynome::integrer(double c=0){

  VectorXd result = VectorXd::Zero(degre+2);
  int i;
  for(i=degre;i>=0;i--)
    result(i+1)=coeff(i)/(i+1);
  
  result(0)=c;
  degre++;
  coeff = result;

}

double polynome::premiere_racine(double binf,double bsup,double precision) 
{
  double y0,x=binf;
  y0=valeur(x);
  do 
    x+=precision; 
  while ((y0*valeur(x)>0)&&(x<=bsup));
  
  return x;
}

double polynome::racine(double binf,double bsup,double precision) {
  double y0,x;
  y0=valeur(binf);
  do 
    {
      x=(binf+bsup)/2.0;
      if(y0*valeur(x)>0) binf=x; else bsup=x;
    }
  while ((bsup-binf)>precision);
  return x;
}
 
/*implantation des fonctions spécifiques aux polynomes **************************************/

ostream& operator << (ostream &flux, polynome &p)
//p par référence, mais ce n'est pas obligatoire, ça évite une copie locale
{ 
  p.affiche(flux);
  return flux;
}

istream& operator >> (istream &flux, polynome &p)
//p obligatoirement par référence car il sera modifié
{ 
  if(flux==cin) 
    p.saisie();
  else 
    cout<<"implantation de << uniquement pour cin\n";
  return flux;
}

int operator == (polynome &p1, polynome &p2)
{
  return p1.egal(p2);}

int operator != (polynome &p1, polynome &p2)
{
  return !(p1.egal(p2));}

polynome operator + (polynome &p,polynome &q)
{
  polynome r;
  r.copie(p);
  r.additionner(q);
  return r;
}
polynome operator + (polynome &p,double f)
{
  polynome r;
  r.copie(p);
  r.additionner(f);
  return r;
}
polynome operator + (double f,polynome &p)
{
  return(p+f);
}

polynome operator - (polynome &p,polynome &q)
{
  polynome r;
  r.copie(p);
  r.soustraire(q);
  return r;
}
polynome operator - (polynome &p,double f)
{
  polynome r;
  r.copie(p);
  r.soustraire(f);
  return r;
}
polynome operator - (double f,polynome &p)
{return(p-f);}
polynome operator * (const polynome &p,const polynome &q)
{
  polynome r;
  r.copie(p);
  r.multiplier(q);
  return r;
}
polynome operator * (const polynome &p,double f)
{
  polynome r;
  r.copie(p);
  r.multiplier(f);
  return r;
}

polynome operator * (double f,const polynome &p)
{
  return(p*f);

}

polynome operator / (const polynome &p,double f)
{
  return(p*(1/f));
}
