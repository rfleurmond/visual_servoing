#include "equation.h"

using namespace Eigen;
using namespace std;

void Equation::setValeurs(MatrixXd m,VectorXd v){
  assert(m.rows()==v.rows());
  mat = m;
  constante = v;
  solution = VectorXd::Zero(mat.cols());
}

VectorXd Equation::getSolution(){
  return solution;
}  

void Equation::solve(){
  LeastSquares::solve(constante,mat,solution);
}

void Equation::affiche(ostream & flux){
  double val;
  bool first;
  for(int i = 0;i < mat.rows(); i++){
    first = true;
    for(int j = 0; j< mat. cols(); j++){
      val = mat(i,j);
      if(isEqual(val,0,1e-10)==false){
	if(first){
	  first = false;
	  flux << val;
	}
	else{
	  if(val>0){
	    flux<<" + "<<val;
	  }
	  else{
	    flux<<" - "<<-val;
	  }
	}
	switch(j){
	case 0: flux <<"x";break;
	case 1: flux <<"y";break;
	case 2: flux <<"z";break;
	case 3: flux <<"t";break;
	default: flux <<" ";
	}
      }
    }
    if(first){
      flux <<"                 0 = 0"<<endl;
    }
    else{
      flux <<" =  "<<constante(i)<<endl;
    }
  }

}
  
ostream& operator << (ostream &flux, Equation &e)
{ 
  e.affiche(flux);
  return flux;
}

