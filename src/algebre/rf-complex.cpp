#include "rf-complex.h"
#include "../tools.h"

RFComplex::RFComplex(const RFComplex &c)
{
  x=c.x;
  y=c.y;
}

RFComplex::RFComplex(double a,double b)
{
  x=a;
  y=b;
}

double RFComplex::Re()
{
  return x;
}

double RFComplex::Im()
{
  return y;
}

bool RFComplex::isReal(){
  return isEqual(y,0,1e-10);
}

double RFComplex::Mod()
{
  return hypot(x,y);
}

double RFComplex::Arg()
{
  double arg;
  double mod;

  mod=hypot(x,y);
  if (mod==0.) return 0.;
  arg=acos(x/mod);
  if (y<0.) arg=2*M_PI-arg;
  return arg;
}

void RFComplex::Coord(double a,double b)
{
  x=a;
  y=b;
}

void RFComplex::Polar(double mod,double arg)
{
  x=mod*cos(arg);
  y=mod*sin(arg);
}

RFComplex RFComplex::Conj()
{
  RFComplex c(x,-y);
  return c;
}

RFComplex RFComplex::Root(unsigned n,unsigned k)
{
  RFComplex c;

  c.Polar(pow(Mod(),1./(double)(n)),(Arg()+2.*(double)(k)*M_PI)/(double)(n));
  return c;
}

bool RFComplex::operator==(const RFComplex &c)
{
  return isEqual(x,c.x,1e-10) &&  isEqual(y,c.y,1e-10);
}

bool RFComplex::operator!=(const RFComplex &c)
{
  return !isEqual(x,c.x,1e-10) || !isEqual(y,c.y,1e-10);;
}

bool RFComplex::operator<=(const RFComplex &c)
{
  return (x<=c.x);
}

bool RFComplex::operator>=(const RFComplex &c)
{
  return (x>=c.x);
}

bool RFComplex::operator<(const RFComplex &c)
{
  return (x<c.x);
}

bool RFComplex::operator>(const RFComplex &c)
{
  return (x>c.x);
}

RFComplex& RFComplex::operator=(const RFComplex &c)
{
  if (&c!=this)
    {
      x=c.x;
      y=c.y;
    }
  return *this;
}

RFComplex& RFComplex::operator+=(const RFComplex &c)
{
  x+=c.x;
  y+=c.y;
  return *this;
}

RFComplex& RFComplex::operator-=(const RFComplex &c)
{
  x-=c.x;
  y-=c.y;
  return *this;
}

RFComplex& RFComplex::operator*=(const RFComplex &c)
{
  double temp=x*c.y+c.x*y;

  x=x*c.x-y*c.y;
  y=temp;
  return *this;
}

RFComplex& RFComplex::operator/=(const RFComplex &c)
{
  double mod=c.x*c.x+c.y*c.y;
  double temp=(c.x*y-x*c.y)/mod;

  x=(x*c.x+y*c.y)/mod;
  y=temp;
  return *this;
}

RFComplex operator+(const RFComplex &c1,const RFComplex &c2)
{
  RFComplex ret=c1;
  return ret+=c2;
}

RFComplex operator-(const RFComplex &c1,const RFComplex &c2)
{
  RFComplex ret=c1;
  return ret-=c2;
}

RFComplex operator-(const RFComplex &c)
{
  return 0-c;
}


RFComplex operator+(const RFComplex &c)
{
  return 0+c;
}

RFComplex operator*(const RFComplex &c1,const RFComplex &c2)
{
  RFComplex ret=c1;
  return ret*=c2;
}

RFComplex operator/(const RFComplex &c1,const RFComplex &c2)
{
  RFComplex ret=c1;
  return ret/=c2;
}
