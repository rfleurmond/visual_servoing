#include "facteur.h"
#include <cmath>

using namespace std;
using namespace Eigen;

polyfacteur::polyfacteur():
  degre(0),
  multiple(1)
{

}

polyfacteur::polyfacteur(facteur f):
  degre(0),
  multiple(1)
{
  addFacteur(f);
}
  
polyfacteur::polyfacteur(polynome p):
  degre(0),
  multiple(1)
{
  addFacteur(1,p);
}

polyfacteur::polyfacteur(double a, list<double> racines):
  degre(0),
  multiple(1)
{
  
  list<double>::const_iterator it;
  for(it = racines.begin();it!=racines.end();it++){
    double d = (*it);
    polynome p(1,&d);
    addFacteur(1,p);
  };

  multiplie(a);
}

void polyfacteur::addFacteur(int expo, polynome p){
  if(expo>=1){
    facteur f;
    f.exposant = expo;
    f.terme = p;
    addFacteur(f);
  }
}

void polyfacteur::addFacteur(facteur f){
  if(f.terme.getDegre()==0){
    multiplie(f.terme.getCoeff(0));
    return;
  }
  termes.push_back(f);
  degre+=f.terme.getDegre()*f.exposant;
}

void polyfacteur::multiplie(double d){
  if(isEqual(d,0,1e-6)){
    raz();
  }
  else{
    multiple*=d;
  }
}

void polyfacteur::multiplie(facteur f){
  addFacteur(f);
}

void polyfacteur::multiplie(polynome p){
  facteur f;
  f.terme = p;
  f.exposant = 1;
  addFacteur(f);
}

void polyfacteur::multiplie(polyfacteur p){
  double m = p.getMultiple();
  if(isEqual(m,0,1e-6)){
    raz();
    return;
  }
  multiple*=m;

  list<facteur> import = p.getFacteurs();
  list<facteur>::const_iterator it;
  for(it = import.begin();it!=import.end();it++){
    facteur f = (*it);
    addFacteur(f);
  }
}

int polyfacteur::getNombreFacteurs(){
  return termes.size();
}

int polyfacteur::getDegre(){
  return degre;
}

list<facteur> polyfacteur::getFacteurs(){
  return termes;
}

void polyfacteur::setMultiple(double d){
   if(isEqual(d,0,1e-6)){
    raz();
  }
  else{
    multiple=d;
  }
}

double polyfacteur::getMultiple(){
  return multiple;
}

void polyfacteur::affiche(ostream &flux){
  if(isEqual(degre,0,1e-6)){
    flux<<multiple;
  }
  else{
    if(!isEqual(multiple,1,1e-6)){
      flux << multiple <<"*";
    }
    list<facteur>::const_iterator it;
    for(it = termes.begin();it!=termes.end();it++){
      facteur f = (*it);
      flux << f;
    }
  }
}

void polyfacteur::raz(){
  termes.clear();
  multiple = 0;
  degre = 0;
}

polynome polyfacteur::developpement(){

  double p = 1;
  polynome resultat(1,&p);

  list<facteur>::const_iterator it;
  for(it = termes.begin();it!=termes.end();it++){
    facteur f = (*it);
    for(int i = 0; i < f.exposant; i++){
      resultat.multiplier(f.terme);
    }
  }
  resultat.multiplier(multiple);
  return resultat;
}

ostream& operator << (ostream &flux, facteur &f){
  if(f.exposant>1){
    flux<<"("<<f.terme<<")^"<<f.exposant;
  }
  else{
    flux<<"("<<f.terme<<")";
  }
  return flux;
}

ostream& operator << (ostream& flux, polyfacteur& pf){
  pf.affiche(flux);
  return flux;
}

polyfacteur operator * (const polyfacteur &p,const polyfacteur &q){
  polyfacteur pp =p;
  pp.multiplie(q);
  return pp;
}
  
polyfacteur operator * (const polyfacteur &p,double q){
  polyfacteur pp =p;
  pp.multiplie(q);
  return pp;
}

polyfacteur operator * (double q, const polyfacteur &p){
  polyfacteur pp =p;
  pp.multiplie(q);
  return pp;
}

polyfacteur polyfacteur::factoriser(polynome p,double racine){

  polyfacteur pf;
  VectorXd diviseur, reste, quotient;
  int n;
  double mult;

  n = p.getDegre();

  if(n==0){
    pf.multiplie(p);
    return pf;
  }
  reste = p.getAll();
  quotient = VectorXd::Zero(n);
  diviseur = VectorXd::Zero(2);
  diviseur(1) = 1;
  diviseur(0) = -racine;

  bool erreur = false;
  
  for(int i = n; i > 0; i--){
    mult = reste(i);
    quotient(i-1)= mult;
    reste.block<2,1>(i-1,0)-= mult * diviseur;
    mult = reste(i);
    if(!isEqual(mult,0,1e-6)){
      quotient = quotient.block(i-1,0,n+1-i,1);
      erreur = true;
      break;
    }
  }

  mult = reste(0);
  if(!isEqual(mult,0,1e-6)){
    erreur = true;
  }
  
  polynome pDiviseur(diviseur);
  polynome pQuotient(quotient);
  polynome pReste(reste);

  cout <<"Le polynome:\n";
  cout <<p<<endl;
  cout <<"peut se decomposer ainsi:\n";
  cout <<"("<<pDiviseur<<")*("<<pQuotient<<") + "<<pReste<<endl;

  if(erreur){
    pf.multiplie(p);
  }
  else{
    pf.multiplie(diviseur);
    pf.multiplie(quotient);
  }

  return pf;

}

double polyfacteur::getFunctionValue(double x){
  double y = multiple;

  list<facteur>::const_iterator it;
  for(it = termes.begin();it!=termes.end();it++){
    facteur f = (*it);
    for(int i = 0; i < f.exposant; i++){
      y*=f.terme.valeur(x);
    }
  }
  return y;
  
}

double polyfacteur::extraire1racine(polynome p){
  RFComplex a = PolySolveur::resoudre1(p);
  return a.Re();
}

list<double> polyfacteur::selectRacines(list<RFComplex> roots){
  list<double> racines;
  list<RFComplex>::const_iterator it;
  for(it = roots.begin();it!=roots.end();it++){
    RFComplex a = (*it);
    //cout<<a.Re()<<" + "<<a.Im()<<" i"<<endl; 
    if(a.isReal()){
      racines.push_back(a.Re());
    }
  }
  return racines;
}
  
list<double> polyfacteur::extraire2racines(polynome p){
  return selectRacines(PolySolveur::resoudre2(p));
}
 
list<double> polyfacteur::extraire3racines(polynome p){
  return selectRacines(PolySolveur::resoudre3(p));
} 


