#include "poly-solveur.h"

using namespace std;

RFComplex PolySolveur::resoudre1(polynome p){
  p.ajusteDegre();
  assert(p.getDegre()==1);
  double racine = -p.getCoeff(0)/p.getCoeff(1);
  RFComplex val(racine);
  return val;
}

list<RFComplex> PolySolveur::resoudre2(polynome p){

  p.ajusteDegre();
  assert(p.getDegre()==2);
  list<RFComplex> racines;
  RFComplex delta,rDelta,x1,x2;
  RFComplex a(p.getCoeff(2),0);
  RFComplex b(p.getCoeff(1),0);
  RFComplex c(p.getCoeff(0),0);

  delta = b*b - 4*a*c;

  if(delta==0){
    x1 = -0.5*b/a;
    racines.push_back(x1);
    racines.push_back(x1);
  }
  else{
    rDelta = delta.Root(2,0);
    x1 = -0.5*(b+rDelta)/a;
    x2 = -0.5*(b-rDelta)/a;
    racines.push_back(x1);
    racines.push_back(x2);
  }

  return racines;
}
  

list<RFComplex> PolySolveur::resoudre3(polynome PP){

  PP.ajusteDegre();
  assert(PP.getDegre()==3);
  
  int n = 0;
  int i = 0;
  int j = 0;
  int p3;
  RFComplex p,u;
  RFComplex q;
  RFComplex z[2][3];
  RFComplex x[3];
  list<RFComplex> racines;

  RFComplex a(PP.getCoeff(3),0);
  RFComplex b(PP.getCoeff(2),0);
  RFComplex c(PP.getCoeff(1),0);
  RFComplex d(PP.getCoeff(0),0);


  p=(3*a*c-b*b)/(3*a*a);
  q=(2*b*b*b-9*a*b*c+27*a*a*d)/(27*a*a*a);

  double para[3];

  para[2] = 1;
  para[1] = q.Re();
  u = -(p*p*p)/27;
  para[0] = u.Re();

  polynome poly2(3,para);

  racines = resoudre2(poly2);

  list<RFComplex>::const_iterator it;

  for(it = racines.begin();it!=racines.end();it++){
    x[i] = (*it);
    i++;
  }

  for (i=0 ; i<3 ; i++) z[0][i]=x[0].Root(3,i);
  for (i=0 ; i<3 ; i++) z[1][i]=x[1].Root(3,i);

  n=0;
  p3=(int)((-p.Re()/3)*PRECISION);

  for (i=0 ; i<3 ; i++){
    for (j=0 ; j<3 ; j++){
      
      if ((int)((z[0][i]*z[1][j]).Re()*PRECISION)==p3)	    {
	x[n]=z[0][i]+z[1][j]-b/(3*a);
	x[n].Coord
	  (
	   x[n].Re(),
	   ((double)(int)(x[n].Im()*PRECISION))/PRECISION
	   );
	n++;
      }
    }
  }

  list<RFComplex> roots;
  for(i=0;i<3;i++){
    roots.push_back(x[i]);
  }
   
  return roots;
}

