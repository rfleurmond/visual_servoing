#include "my-kinematic-model.h"

using namespace Eigen;
using namespace std;


const double MyKineModel::CRAYON = 1e-10;


MyKineModel::MyKineModel(GeoRobot * robot):
  InverseKinematicModel(),
  filament(robot)
{
  assert(robot!=NULL);
}

int MyKineModel::getTotalDegres() const{
  if(filament!=NULL){
     return filament->getTotalDegres();
  }
  return 0;
}

void MyKineModel::addFrozenJoint(int i){
  if(i>=0 && i < filament->getTotalDegres()){
    frozen.push_back(i);
  }
}


void MyKineModel::setCoordonneArticulaires(const VectorXd & v){
  filament->setCoordonneArticulaires(v);
}


Repere MyKineModel::getSituation(const VectorXd & q, int i) const{
  Repere se3;
  if(filament!=NULL){
    se3 =filament->getSituation(q,i);
  }
  return se3;
}


Repere MyKineModel::getSituation(const VectorXd & q) const{
  Repere se3;
  if(filament!=NULL){
     se3 =filament->getSituation(q);
  }
  return se3;
}

Repere MyKineModel::getSituation() const{
  Repere se3;
  if(filament!=NULL){
     se3 =filament->getSituation();
  }
  return se3;
}

Vector3d MyKineModel::getLinearVelocity(const VectorXd & q, const VectorXd & qpoint) const{
  
  MatrixXd temp(getKinematicJacobian(q));
  VectorXd torseur(6);
  torseur.noalias() = temp * qpoint; 
  
  Vector3d vitesse = torseur.block<3,1>(0,0);
  
  return vitesse;
 
}

Vector3d MyKineModel::getAngularVelocity(const VectorXd & q, const VectorXd & qpoint) const{

  MatrixXd temp(getKinematicJacobian(q));
  VectorXd torseur(6);
  torseur.noalias() = temp * qpoint; 
  Vector3d vitesse = torseur.block<3,1>(3,0);
  
  return vitesse;
  
}

MatrixXd MyKineModel::getKinematicJacobian(const VectorXd & q) const{
  return getKinematicJacobian(q,getTotalDegres());
}



MatrixXd MyKineModel::getKinematicJacobian(const VectorXd & q, int max) const{

  assert(getTotalDegres()!=0);

  assert(q.rows()==getTotalDegres());

  assert(max>0 && max <=getTotalDegres());

  MatrixXd J = MatrixXd::Zero(6,getTotalDegres());

  mouvement muve;

  Vector3d omega = Vector3d::Zero();
  Vector3d vitesse = Vector3d::Zero();
  Vector3d levier = Vector3d::Zero();
  Vector3d levierTempo = Vector3d::Zero();
  Vector3d vecteur  = Vector3d::Zero();

  Repere courant, boutAbout, situation;

  Matrix3d rotation = Matrix3d::Identity();

  Matrix3d vissage  = Matrix3d::Identity();

  int n = getTotalDegres();
  int i = n;

  if(filament!=NULL){

    Liaison * link = filament->getLastLiaison();

    while(i>max){
      link = filament->previous(link);
      i--;
    }
    
    Corps * body = NULL;

    if(link!=NULL){
      body = link->getCorpsB();
    }

    if(body!=NULL){
      levier = body->getBoutAbout().getDeplacement();
    }

    while(link!=NULL){

      i--;
     
      assert(link->getDegres()==1);
      
      muve = link->getMouvement();

      courant = link->getFromMetoBodyB(q.block<1,1>(i,0));

      levierTempo = courant.getDeplacement();

      levier = levierTempo + courant.getVectorInParent(levier);
   
      vecteur = muve.direction;

      if(muve.isRotation){
	omega = vecteur;
	vitesse = vecteur.cross(levier-muve.origine);
      }
      else{
	vitesse = vecteur;
	omega = Vector3d::Zero();
      }

      rotation = courant.getRotation().getMatrix();
      
      body = link->getCorpsA();

      if(body!=NULL){

	boutAbout = body->getBoutAbout(); 
	
	vissage = boutAbout.getRotation().getMatrix();
	rotation = vissage * rotation;
		
	levier  = boutAbout.getPointInParent(levier);
	vitesse = boutAbout.getVectorInParent(vitesse);
	omega = boutAbout.getVectorInParent(omega);
      }

      if(i<n-1){
	J.block(0,i+1,3,n-i-1) = rotation * J.block(0,i+1,3,n-i-1);
	J.block(3,i+1,3,n-i-1) = rotation * J.block(3,i+1,3,n-i-1);
      }
	
      J.block<3,1>(0,i) = vitesse;
      J.block<3,1>(3,i) = omega;

      link = filament->previous(link);

     
    }
  }

  for(int i = 0; i < frozen.size(); i++){
    J.col(frozen[i]) = VectorXd::Zero(6);
  }
  return J;
}


bool MyKineModel::getCommandFromVelocity(const VectorXd & position, const VectorXd & torseur, VectorXd & commande) const{
  assert(getTotalDegres()!=0);
  
  MatrixXd jacobienne = getKinematicJacobian(position);
  
  double erreur = LeastSquares::solvePseudoInverse(torseur,jacobienne,commande);

  return (erreur<CRAYON); 

}

bool MyKineModel::getCommandFromLinearVelocity(const VectorXd & position, const Vector3d & linear, VectorXd & commande) const{
  
  assert(getTotalDegres()!=0);

  MatrixXd jacobienne = getKinematicJacobian(position);
  
  VectorXd ordre = VectorXd::Zero(getTotalDegres());
  MatrixXd JJ = jacobienne.topRows(3);
  double erreur = LeastSquares::solvePseudoInverse(linear,JJ,commande);
  return (erreur<CRAYON);
}

bool MyKineModel::getCommandFromAngularVelocity(const VectorXd & position, const Vector3d & angular, VectorXd & commande) const{
  assert(getTotalDegres()!=0);
  
  MatrixXd jacobienne = getKinematicJacobian(position);
  
  MatrixXd JJ = jacobienne.bottomRows(3);
  
  double erreur = LeastSquares::solvePseudoInverse(angular,JJ,commande);

  return (erreur<CRAYON);

}
