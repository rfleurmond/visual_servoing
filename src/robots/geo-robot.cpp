#include "geo-robot.h"
#include "rotoide.h"
#include "prismatique.h"

using namespace Eigen;
using namespace std;

GeoRobot::GeoRobot():
  Robot(),
  GeometricModel(),
  nom(),
  fichier(),
  base(NULL),
  main(NULL),
  link(NULL),
  poignet(NULL),
  bone(NULL),
  degres(0),
  nombre(0),
  etat(VectorXd::Zero(1)),
  qMin(VectorXd::Zero(1)),
  qMax(VectorXd::Zero(1))
{

}

GeoRobot::~GeoRobot(){
  Liaison * courant = getLastLiaison();
  Liaison * precedent = NULL;
  Corps * femur = NULL;
  while(courant!=NULL){
    femur = courant->getCorpsB();
    if(femur!=NULL){
      delete femur;
    }
    precedent = previous(courant);
    delete courant;
    courant = precedent;
  }

  if(base!=NULL){
    delete base;
  }
    
}

Liaison * GeoRobot::next(Liaison * l) const{
  if(l==NULL)
    return NULL;

  Corps * os = l->getCorpsB();

  if(os == NULL)
    return NULL;

  return os->getLiaisonB();
}

Liaison * GeoRobot::previous(Liaison * l) const{
  if(l==NULL)
    return NULL;

  Corps * os = l->getCorpsA();

  if(os == NULL)
    return NULL;

  return os->getLiaisonA();

}

Corps * GeoRobot::next(Corps * c) const{
  if(c==NULL)
    return NULL;

  Liaison * lien = c->getLiaisonB();

  if(lien == NULL)
    return NULL;

  return lien->getCorpsB();
}

Corps * GeoRobot::previous(Corps * c) const{
  if(c==NULL)
    return NULL;

  Liaison * lien = c->getLiaisonA();

  if(lien == NULL)
    return NULL;

  return lien->getCorpsA();
}

 
void GeoRobot::calculTotalDegres(){

  list<Liaison *>::const_iterator it;

  degres = 0;

  if(base!=NULL){

    link = base->getLiaisonB();

    while(link!=NULL){

      degres += link->getDegres();

      link = next(link);
   
    }
  }

  etat = VectorXd::Zero(degres);

  checkBounds();

}


int GeoRobot::getTotalDegres() const{ 
  return degres;
}

Repere GeoRobot::getSituation() const{ 
  return getSituation(etat);
}

Repere GeoRobot::getSituation(const VectorXd & q) const{
  return getSituation(q,getTotalDegres());
}


VectorXd GeoRobot::getCoordonneArticulaires() const{
  return etat;
}

VectorXd GeoRobot::getMinimalQ() const{
  return qMin;
}
VectorXd GeoRobot::getMaximalQ() const{
  return qMax;
}

void GeoRobot::checkBounds(){

  Liaison * lien = NULL;

  qMin = VectorXd::Zero(getTotalDegres());
  qMax = qMin;

  int index = 0;
  int dim = 0;

  if(base!=NULL){

    lien = base->getLiaisonB();
    
    while(lien!=NULL){
 
      dim = lien->getDegres();
      
      qMin.block(index,0,dim,1) = lien->getMinimum();

      qMax.block(index,0,dim,1) = lien->getMaximum();

      lien = next(lien);

      index+=dim;
   
    }
  }

}
  


Repere GeoRobot::getSituation(const VectorXd & q, int max) const{

  assert(q.rows()==degres);

  assert(max>0 && max<=degres);

  Repere courant;

  Repere situation;

  int i = 0, index = 0;

  int dimensions = 0;

  Liaison * lien = NULL;

  if(base!=NULL){

    situation = base->getBoutAbout();

    lien = base->getLiaisonB();
    
    while(lien!=NULL){
 
      Repere passage;

      if(lien->getCorpsB()!=NULL){
	passage = lien->getCorpsB()->getBoutAbout();
      }

      
      dimensions = lien->getDegres();
      Repere ephem = lien->getFromMetoBodyB(q.block(index,0,dimensions,1));
      courant = ephem * passage;
      index += dimensions;
      i++;
      //cout <<courant<<endl;
      situation = situation * courant;

      if(i>=max){
	break;
      }
      
      lien = next(lien);
   
    }
  }

  return situation;
  
}

void GeoRobot::setCoordonneArticulaires(const VectorXd & q){

  assert(degres == q.rows());

  etat = q;

  list<Liaison *>::const_iterator it;

  int index =0;

  int dimensions = 0;

  if(base!=NULL){

    link = base->getLiaisonB();

    while(link!=NULL){
 
     dimensions = link->getDegres();

      if(dimensions>0){
	link->setQ(q.block(index,0,dimensions,1));
      }

      index+=dimensions;
      link = next(link);

    }
  }

}

int GeoRobot::getNombreLiaisons() const{
  
  return nombre;

}

void GeoRobot::setBase(Corps * c){

  base = c;

  main = c;

}
void GeoRobot::setHand(Corps * c){

  main = c;

}

void GeoRobot::addCorps(Corps * c){
  
  if(c!=NULL){

    main = c;

    if(base==NULL){
      base = main;
    }

    if(poignet!=NULL)
      poignet->setCorpsB(c);

  }
}

void GeoRobot::addLiaison(Liaison * l){

  if(l!=NULL){

    l->setCorpsA(main);

    poignet = l;

    calculTotalDegres();

    nombre++;
  }
}


Liaison * GeoRobot::getFirstLiaison(){
  if(base!=NULL){
    return base->getLiaisonB();
  }
  return NULL;
}

Liaison * GeoRobot::getLastLiaison(){
  if(main!=NULL){
    if(main->getLiaisonB()!=NULL)
      return main->getLiaisonB();

    return main->getLiaisonA();
  }
  return NULL;
}

Corps * GeoRobot::getFirstCorps(){
  return base;
}
Corps * GeoRobot::getLastCorps(){
  return main;
}


bool saveDHTRobot(string path, GeoRobot & r2d2){
  /*
    sigma(i-1) = 1 si prismatique 0 si rotoide
    A(i-1) 
    alpha(i-1)
    r(i)
    teta(i)
    min(i)
    max(i)
   */

  VectorXd para;
  vector<VectorXd> liste;
  Liaison * link = r2d2.getFirstLiaison();

  int sigmaI = 0;
  double aI = 0;
  double alphaI = 0;
  double rI = 0;
  double tetaI = 0;
  double minI =0;
  double maxI =0;
  Repere se3;
  Vector3d dep;
  VectRotation muve;

  while(link!=NULL){

    para = VectorXd::Zero(7);
    
    if(link->getType()==Liaison::PRISMAT){
      sigmaI = 1;
    }
    if(link->getType()==Liaison::ROTOIDE){
      sigmaI =0;
    }

    Repere passage;
    if(link->getCorpsA()!=NULL){
      passage = link->getCorpsA()->getBoutAbout();
    }
    se3 = passage;
    
    dep = se3.getDeplacement();
    muve = se3.getRotation();

    aI = dep.transpose()*Vector3d::UnitX();
    alphaI = muve.getVector().transpose()*Vector3d::UnitX();

    if(link->getType()==Liaison::PRISMAT){
      rI = link->getQ()(0);
    }
    if(link->getType()==Liaison::ROTOIDE){
      rI = muve.deRotate(dep).transpose()*Vector3d::UnitZ();
    }

    muve = muve * VectRotation(-alphaI,0,0);

    if(link->getType()==Liaison::ROTOIDE){
      tetaI = link->getQ()(0);
    }
    if(link->getType()==Liaison::PRISMAT){
      tetaI = muve.getVector().transpose()*Vector3d::UnitZ();
    }
        
    minI = link->getMinimum()(0);
    maxI = link->getMaximum()(0);

    para <<sigmaI,aI,alphaI,rI,tetaI,minI,maxI;
    
    liste.push_back(para);
    
    link = r2d2.next(link);

  }

  link = r2d2.getLastLiaison();

  if(link!=NULL){

    Repere passage;
    if(link->getCorpsB()!=NULL){
      passage = link->getCorpsB()->getBoutAbout();
      para <<2,passage.getParameters();
      liste.push_back(para);
    }
 
  } 

  return writeVectors( path,liste);

}

bool loadDHTRobot(string path, GeoRobot & r2d2){

  int sigmaI = 0;
  double aI = 0;
  double alphaI = 0;
  double rI = 0;
  double tetaI = 0;
  double minI =0;
  double maxI =0;
  Repere se3;
  Repere rep;
  Vector3d dep;
  VectRotation muve;
  VectorXd qq = VectorXd::Zero(1);
  VectorXd qqMax = VectorXd::Zero(1);
  VectorXd qqMin = VectorXd::Zero(1);

  vector<double *> data = loadDAT(path,7);
  vector<double * >::iterator it;

  Corps * body = NULL;

  r2d2.setBase(body);

  for(it = data.begin();it!=data.end();it++){

    double * ligne = *it;

    if(isEqual(ligne[0],0,1e-10)){
      sigmaI = 0;
    }
    else{
      if(isEqual(ligne[0],1,1e-10)){
	sigmaI = 1;
      }
      else sigmaI = 2;
    }

    aI = ligne[1];
    alphaI = ligne[2];
    rI = ligne[3];
    tetaI = ligne[4];
    minI = ligne[5];
    maxI = ligne[6];

    if(sigmaI==0){
      muve = VectRotation(alphaI,0,0);
      dep = Vector3d(aI,0,rI);
      dep = muve.rotate(dep);
      se3 = Repere(muve,dep);
      qq(0) = tetaI;
      qqMin(0) = minI;
      qqMax(0) = maxI;
      body = new Corps(se3);
      r2d2.addCorps(body);

      Rotoide * rotoide = new Rotoide("rotoide ");
      rotoide->setInitialState(qq);
      rotoide->goToInitialState();
      rotoide->setMinimum(qqMin);
      rotoide->setMaximum(qqMax);
      r2d2.addLiaison(rotoide);
    }
    if(sigmaI==1){

      muve =  VectRotation(alphaI,0,0);
      dep = Vector3d(aI,0,0);
      muve = VectRotation(0,0,tetaI) * muve; 
      se3 = Repere(muve,dep);
      qq(0) = rI;
      qqMin(0) = minI;
      qqMax(0) = maxI;

      body = new Corps(se3);
      r2d2.addCorps(body);
      Prismatique * prismatique = new Prismatique("prismatique ");
      prismatique->setInitialState(qq);
      prismatique->goToInitialState();
      prismatique->setMinimum(qqMin);
      prismatique->setMaximum(qqMax);
      r2d2.addLiaison(prismatique);

    }
    if(sigmaI==2){
      Repere repere(VectRotation(ligne[1],ligne[2],ligne[3]),Vector3d(ligne[4],ligne[5],ligne[6]));
      body = new Corps(repere);
      r2d2.addCorps(body);
    }
    
    delete[] ligne;

  }

  return true;
   
}

bool saveSE3Robot(string path, GeoRobot & r2d2){
  
  VectorXd para;
  vector<VectorXd> liste;
  Liaison * link = r2d2.getFirstLiaison();

  int sigmaI = 0;
  double tetaI = 0;
  double minI =0;
  double maxI =0;
  Repere se3;
  
  while(link!=NULL){

    para = VectorXd::Zero(10);
    
    if(link->getType()==Liaison::PRISMAT){
      sigmaI = 1;
    }
    if(link->getType()==Liaison::ROTOIDE){
      sigmaI =0;
    }

    Repere passage;
    if(link->getCorpsA()!=NULL){
      passage = link->getCorpsA()->getBoutAbout();
    }
    se3 = passage;
    
    minI = link->getMinimum()(0);
    maxI = link->getMaximum()(0);

    para <<sigmaI,se3.getParameters(),tetaI,minI,maxI;
    
    liste.push_back(para);
    
    link = r2d2.next(link);

  }

  link = r2d2.getLastLiaison();

  if(link!=NULL){

    Repere passage;
    if(link->getCorpsB()!=NULL){
      passage = link->getCorpsB()->getBoutAbout();
      para <<2,passage.getParameters(),0,0,0;
      liste.push_back(para);
    }
 
  } 

  return writeVectors( path,liste);

}

bool loadSE3Robot(string path, GeoRobot & r2d2){

  int sigmaI = 0;
  double tetaI = 0;
  double minI =0;
  double maxI =0;
  VectorXd qq = VectorXd::Zero(1);
  VectorXd qqMax = VectorXd::Zero(1);
  VectorXd qqMin = VectorXd::Zero(1);

  vector<double *> data = loadDAT(path,10);
  vector<double * >::iterator it;

  Corps * body = NULL;

  r2d2.setBase(body);

  for(it = data.begin();it!=data.end();it++){
    double * ligne = *it;
    if(isEqual(ligne[0],0,1e-10)){
      sigmaI = 0;
    }
    else{
      if(isEqual(ligne[0],1,1e-10)){
	sigmaI = 1;
      }
      else sigmaI = 2;
    }
    Repere se3(VectRotation(ligne[1],ligne[2],ligne[3]),Vector3d(ligne[4],ligne[5],ligne[6]));
    tetaI = ligne[7];
    minI = ligne[8];
    maxI = ligne[9];
    qq(0) = tetaI;
    qqMin(0) = minI;
    qqMax(0) = maxI;
    body = new Corps(se3);
    r2d2.addCorps(body);
    
    if(sigmaI==0){
      Rotoide * rotoide = new Rotoide("rotoide");
      rotoide->setInitialState(qq);
      rotoide->goToInitialState();
      rotoide->setMinimum(qqMin);
      rotoide->setMaximum(qqMax);
      r2d2.addLiaison(rotoide);
    }
    if(sigmaI==1){
      Prismatique * prismatique = new Prismatique("prismatique");
      prismatique->setInitialState(qq);
      prismatique->goToInitialState();
      prismatique->setMinimum(qqMin);
      prismatique->setMaximum(qqMax);
      r2d2.addLiaison(prismatique);
    }
    delete[] ligne;
  }

  return true;
}
