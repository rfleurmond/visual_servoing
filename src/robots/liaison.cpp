#include "liaison.h"

using namespace std;
using namespace Eigen;


const double Liaison::CRAYON = 1e-10;

Repere Corps::getMeshToMe() const{
  return meshToMe;
}
  
void Corps::setMeshToMe(const Repere & r){
  meshToMe =r;
}
 

Repere Corps::getBoutAbout() const{
  return boutAbout;
}
  
void Corps::setBoutAbout(const Repere & r){
  boutAbout =r;
}
  
Liaison * Corps::getLiaisonA() const{
  return liaisonA;
}

Liaison * Corps::getLiaisonB() const{
  return liaisonB;
}

void Corps::addLiaisonA(Liaison * l){
  liaisonA = l;
}

void Corps::addLiaisonB(Liaison * l){
  liaisonB = l;
}

string Corps::getNom() const{
  return nom;
}

void Corps::setNom(string s){
  nom = s;
}

///////////////////////////////////////////


Liaison::Liaison():
  nom(),
  mesh(),
  type(Liaison::STATIC),
  degres(1),
  meToBodyB(),
  meshToMe(),
  corpsA(NULL),
  corpsB(NULL),
  etat(VectorXd::Zero(1)),
  initial(VectorXd::Zero(1)),
  minimum(VectorXd::Zero(1)),
  maximum(VectorXd::Zero(1))
{
  corpsA = NULL;
  corpsB = NULL;
}

int Liaison::getDegres() const{
  return degres;
}

void Liaison::setCorpsA(Corps *c){
  corpsA = c;
  corpsA->addLiaisonB(this);
}

void Liaison::setCorpsB(Corps *c){
  corpsB = c;
  corpsB->addLiaisonA(this);
}

Corps * Liaison::getCorpsA() const{
  return corpsA;
}

Corps * Liaison::getCorpsB() const{
  return corpsB;
}

VectorXd Liaison::getQ() const{
  return etat;
}
   
int Liaison::getType() const{
  return type;
}


Repere Liaison::getFromMetoBodyB() const{
  return meToBodyB;
}

void Liaison::goToInitialState(){
  setQ(initial);
}

VectorXd Liaison::getInitialState() const{
  return initial;
}


void  Liaison::setInitialState(const VectorXd & begin){

  assert(degres == begin.rows());

  initial = begin;

}

void Liaison::setMinimum(const VectorXd & m){

  assert(m.rows() == degres);

  minimum = m;

}

void Liaison::setMaximum(const VectorXd & m){

  assert(m.rows() == degres);

  maximum = m;

}

VectorXd Liaison::getMinimum(){

  return minimum;
 
}

VectorXd Liaison::getMaximum(){

  return maximum;

}


void Liaison::setMesh(string file){

  mesh = file;

}

string Liaison::getMesh(){

  return mesh;

}


