#include "prismatique.h"
using namespace Eigen;
using namespace std;

Prismatique::Prismatique(string n):
  Liaison()
{

    nom = n;

    type = Liaison::PRISMAT;

    degres = 1;

    etat = VectorXd::Zero(degres);

    initial = etat;

    minimum = initial;

    maximum = 1*VectorXd::Ones(degres);

}

void Prismatique::setQ(const VectorXd & q){

    assert(q.rows() == degres);

    etat = q;

    double longueur = q(0);

    Vector3d transla = longueur * Vector3d::UnitZ();

    meToBodyB.setDeplacement(transla);

}


Repere Prismatique::getFromMetoBodyB(const VectorXd & q) const{

  assert(q.rows() == degres);

  double longueur = q(0);

  Vector3d transla = longueur * Vector3d::UnitZ();

  Repere MyBaby(VectRotation(), transla);

  return MyBaby;

}


mouvement Prismatique::getMouvement() const{

  mouvement move;

  move.origine = Vector3d::Zero();

  move.direction = Vector3d::UnitZ();

  move.isRotation = false;

  return move;

}

MatrixXd Prismatique::getJacobian() const{

  MatrixXd  vecteur = MatrixXd::Zero(6,1);

  vecteur.block<3,1>(3,0) = Vector3d::UnitZ();

  return vecteur;

}

bool Prismatique::getQValueFor(const Repere & transfo, VectorXd & value) const{

  Vector3d dep = transfo.getDeplacement();

  Vector3d ori = transfo.getRotation().getVector();

  double val = dep.z();

  dep.z() = 0;

  if(isEqual(dep.norm(),0,CRAYON)==false){
    return false;
  }

  if(isEqual(ori.norm(),0,CRAYON)==false){
    return false;
  }

  value = VectorXd::Zero(1);

  value(0) = val;

  return true;
}
