#include "my-inverse-geo-model.h"

using namespace std;
using namespace Eigen;

MyInverseGeoModel::MyInverseGeoModel(GeoRobot * worker):
  InverseGeoModel(),
  n(0),
  creations(0),
  suppressions(0),
  complet(false),
  positionFound(false),
  situationFound(false),
  infinite(false),
  genese(0),
  apocal(0),
  robot(worker),
  arbre(NULL),
  squelette(NULL)
{
  assert(worker!=NULL);
  n = robot->getTotalDegres();
  reflexion();
}

MyInverseGeoModel::~MyInverseGeoModel(){

}

int MyInverseGeoModel::getTotalDegres() const{
  return robot->getTotalDegres();
}

Repere MyInverseGeoModel::getSituation(const VectorXd & q) const{
  return robot->getSituation(q);
}

Repere MyInverseGeoModel::getSituation(const VectorXd & q, int i) const{
  return robot->getSituation(q,i);
}

bool MyInverseGeoModel::isComplete(){
  return complet;
}

bool MyInverseGeoModel::hasFoundSolutionsForPosition(){
  return positionFound;
}

bool MyInverseGeoModel::hasFoundCompleteSolutions(){
  return situationFound;
}

bool MyInverseGeoModel::hasInfinityOfSolutions(){
  return infinite;
}

void MyInverseGeoModel::reflexion(){
  
}

list<VectorXd> MyInverseGeoModel::getCoordonnees(Repere situation) {
  list<VectorXd> solutions;
  Repere transfo;
  squelette = new Abstraction[n];

  Liaison * link = robot->getFirstLiaison();
  int a = 0;
  while(link!=NULL){
    squelette[a].isPositionFixed = false;
    squelette[a].isOrientationFixed = false;
    squelette[a].link = link;
    a++;
    link = robot->next(link);
  }

  affectation inutile;
  inutile.index = -1;
  arbre = new Node(inutile);
  Repere rien;
  
  Corps * body = robot->getFirstCorps();
  if(body!=NULL){
    transfo = body->getBoutAbout();
  }
  
  squelette[0].isPositionFixed = true;
  squelette[0].position = transfo.getDeplacement();
  squelette[0].isOrientationFixed = true;
  squelette[0].orientation = transfo.getRotation();

  cout<<"---------------------------------------------------"<<endl;
  //cout<<">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"<<endl;
  forwardAnalyse(squelette,n,0,transfo);
  //cout<<">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"<<endl;
  
  link = robot->getLastLiaison();
  body = link->getCorpsB();
  if(body!=NULL){
    situation = situation/body->getBoutAbout();
  }
  if(squelette[n-1].link!=NULL){
    squelette[n-1].orientation = situation.getRotation();
    if(squelette[n-1].link->getType()==Liaison::ROTOIDE){
      squelette[n-1].isPositionFixed = true;
      squelette[n-1].position = situation.getDeplacement();
      squelette[n-1].isOrientationFixed = false;
      squelette[n-1].droiteOrient.orient = new LPoint(situation.getZ());
      //cout<<"Creations STAR :   "<<++creations<<endl;
    }
    if(squelette[n-1].link->getType()==Liaison::PRISMAT){
      squelette[n-1].isOrientationFixed = true;
    }
  }
  //cout<<"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"<<endl;
  backwardAnalyse(squelette,n,n-1,situation);
  //cout<<"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"<<endl;
  printReflexion(squelette,n,0);
  exploitation(squelette,n,0,transfo,situation,arbre);
  arbre->clear();
  delete arbre;
  //cout<<"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"<<endl;
  clearMentalConstructs(squelette, n);
  //cout<<"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"<<endl;
  cout<<"---------------------------------------------------"<<endl;
  return solutions;
  
}




void MyInverseGeoModel::exploitation(Abstraction * symboles, int N,int index, Repere frame, Repere situation,Node * noeud){
  assert(noeud!=NULL);
  if(N==1){
    complet = true;
    Repere se3 = frame.inverse()*situation;
    VectorXd valX;
    Liaison * lien = symboles[0].link;
    if(lien!=NULL){
      if(lien->getQValueFor(se3, valX)){
	double val = valX(0);
	ajout(noeud,index,val);
	positionFound = true;
	situationFound = true;
      }
      positionFound = false;
      situationFound  = false;
    }
    return;
  }
  //cout<<"N = "<<N<<endl;
  //cout<<"I = "<<index<<endl;
  int i;
  Lieu * centre = symboles[0].centre;
  Liaison * link = symboles[0].link;
  if(link==NULL){
    return;
  }
  Vector3d base = symboles[0].position;
  Vector3d lune;
  int gauche = -1;
  int mediatrice = -1;
  Repere transfo;
  Corps * body = link->getCorpsB();
  if(body==NULL){
    transfo = Repere();
  }
  else{
    transfo  = body->getBoutAbout();
  }

  list<int> numOrbites;
  list<Vector3d> orbites; 
  list<int> numFoyers;
  list<Vector3d> foyers; 
  list<int> collisions;


  i = 0;
  while(i<N){
    if(symboles[i].lieuGauche !=NULL && symboles[i].lieuDroite !=NULL && symboles[i].isPositionFixed == false){
      complet = true;
      collisions.push_back(i);
    }
    if(symboles[i].lieuGauche != NULL && symboles[i].isPositionFixed){
      complet = true;
      numOrbites.push_back(i);
      orbites.push_back(symboles[i].position);
    }
    if(symboles[i].lieuDroite != NULL && symboles[i].isPositionFixed){
      complet = true;
      numFoyers.push_back(i);
      foyers.push_back(symboles[i].position);
    }
    i++;
  }

  list<double> valeurs;

  list<int>::const_iterator itI; 
  list<Vector3d>::const_iterator itV;
 

  Lieu * ciel = NULL;
  
  itI = collisions.begin();
  while(itI != collisions.end()){
    mediatrice = *itI;
    cout<<"Orbite de collision trouvee a gauche! #"<<mediatrice+1<<endl;
    ciel = symboles[mediatrice].lieuDroite;
    centre = symboles[mediatrice].lieuGauche;
    
    list<LPoint> points = searchPointsIntersectionGauche(centre, ciel);
    
    if(points.size()>0){
      cout<<"La collision est imminente(gauche)!"<<endl;
      list<LPoint>::const_iterator it;
      for(it = points.begin();it!=points.end();it++){
	LPoint lp = *it;
	Vector3d v = lp.getCenter();
	numOrbites.push_back(mediatrice);
	cout<<"Point d' intersection trouve!:"<<v.transpose()<<endl;
	orbites.push_back(v);
      }
    }
    itI++;
  }


  itI = numOrbites.begin();
  itV = orbites.begin();
  while(itI != numOrbites.end() && itV != orbites.end()){
    gauche = *itI;
    cout<<"Orbite de satellite trouvee! #"<<gauche+1<<" Type: "<<symboles[gauche].lieuGauche->afficheType()<<endl;
    lune = *itV;
    genese = 0;
    apocal = gauche;
    if(symboles[gauche].lieuGauche != NULL){
      if(symboles[gauche].lieuGauche->hasPoint(lune)){
	valeurs = findSatelliteInOrbit(symboles[0].link, symboles[gauche].lieuGauche, frame, base, lune,true);
	if(valeurs.size()>0){
	  cout<<"Le satellite est bien sur l'orbite!"<<endl;
	  ajout(noeud,index,valeurs);
	  break;
	}
	/*
	else{
	  Repere tempo = frame *transfo;
	  genese = 1;
	  valeurs = findSatelliteInOrbit(symboles[1].link, symboles[gauche].lieuGauche, tempo, base, lune,true);
	  if(valeurs.size()>0){
	    cout<<"Le satellite est bien sur l'orbite!"<<endl;
	    ajout(noeud,index+1,valeurs);
	    break;
	  }
	}
	//*/
      }
    }
    itI++;
    itV++;
  } 
  

  if(noeud->isLeaf()){
    
    itI = collisions.begin();
    while(itI != collisions.end()){
      mediatrice = *itI;
      //cout<<"Orbite de collision trouvee a droite! #"<<mediatrice<<endl;
      ciel = symboles[mediatrice].lieuDroite;
      centre = symboles[mediatrice].lieuGauche;
    
      list<LPoint> points = searchPointsIntersectionGauche(ciel, centre);
    
      if(points.size()>0){
	cout<<"La collision est imminente(droite)!"<<endl;
	list<LPoint>::const_iterator it;
	for(it = points.begin();it!=points.end();it++){
	  LPoint lp = *it;
	  Vector3d v = lp.getCenter();
	  numFoyers.push_back(mediatrice);
	  foyers.push_back(v);
	}
      }
      itI++;
    }

    itI = numFoyers.begin();
    itV = foyers.begin();
    while(itI != numFoyers.end() && itV != foyers.end()){
      gauche = *itI;
      cout<<"Orbite du soleil trouve! #"<<gauche+1<<" Type: "<<symboles[gauche].lieuDroite->afficheType()<<endl;
      lune = *itV;
      genese = N-1;
      apocal = gauche;
      valeurs = findSatelliteInOrbit(symboles[N-1].link, symboles[gauche].lieuDroite, situation, situation.getDeplacement(), frame.getDeplacement(),false);
      if(valeurs.size()>0){
	cout<<"Position du soleil trouvée!"<<endl;
	list<double>::const_iterator itd;
	for(itd = valeurs.begin();itd!=valeurs.end();itd++){
	  double val = *itd;
	  ajout(noeud,index +N -1, -val);
	}
      }
      itI++;
      itV++;
    } 
 
    
  }

  list<Node *> enfants = noeud->getAllChilds();
  list<Node *>::const_iterator iterateur;
  int ijk = 0;
  for(iterateur = enfants.begin();iterateur!= enfants.end();iterateur++){
    Node * feuille = *iterateur;
    int numero = feuille->getValue().index;
    cout<<"USE SOL#"<<++ijk<<" FOR Q_"<<numero+1<<" -------------------------------"<<endl;
    bourgeonnement(symboles,N,feuille,index,frame,situation);
  }

}

void MyInverseGeoModel::bourgeonnement(Abstraction * symboles, int N, Node * noeud, int index, Repere frame, Repere situation){
  double val = noeud->getValue().value;
  int numero = noeud->getValue().index;
  VectorXd q = VectorXd::Zero(1);
  Liaison * link = symboles[0].link;
  Repere transfo;
  Corps * body = link->getCorpsB();
  if(body!=NULL){
    transfo  = body->getBoutAbout();
  }
  Repere tempo;
  Repere final;
  Repere se3;
  Liaison * lien = NULL;
  q(0) = val;
  if(N==2){
    complet = true;
    VectorXd valX;
    tempo = link->getFromMetoBodyB(q);
    if(numero==index){
      tempo = tempo * transfo;
      final = frame * tempo;
      se3 = final.inverse()*situation;
      lien = symboles[1].link;
      if(lien!=NULL){
	if(lien->getQValueFor(se3, valX)){
	  val = valX(0);
	  ajout(noeud,index+1,val);
	  positionFound = true;
	  situationFound = true;
	}
      }
    }
    if(numero == index + N - 1){
      lien = symboles[N-1].link;
      body = lien->getCorpsA();
      tempo = body->getBoutAbout() * tempo;
      final = situation/tempo;
      se3 = final;
      Liaison * lien2 = symboles[0].link;
      if(lien2!=NULL){
	if(lien2->getQValueFor(se3, valX)){
	  val = valX(0);
	  ajout(noeud,index,val);
	  positionFound = true;
	  situationFound = true;
	}
      }
    }
  }

  if(N>2){
    int decalage = 0;
    int NNN = N-1;
    int newIndex = index;
    Repere debut = frame;
    Repere final = situation;
    Liaison * lien = NULL;
    Corps * body = NULL;
    if(numero==index){
      tempo = link->getFromMetoBodyB(q);
      tempo = tempo * transfo;
      debut = frame * tempo;
      decalage = 1;
      newIndex = index + 1; 
    }
    else if(numero == index + N - 1){
      lien = symboles[N-1].link;
      tempo = lien->getFromMetoBodyB(q);
      body = lien->getCorpsA();
      tempo = body->getBoutAbout() * tempo;
      final = situation/tempo;
    }
    else{
      NNN = N;
    }
    Abstraction * copie = new Abstraction[NNN];
    for(int k = 0; k < NNN; k++){

      copie[k].link = symboles[k+decalage].link;
      copie[k].isSetted = symboles[k+decalage].isSetted;
      copie[k].valeur = symboles[k+decalage].valeur;

      if(numero == k + index){
	copie[k].isSetted = true;
	copie[k].valeur = val;
      }
	  
      copie[k].isPositionFixed = symboles[k+decalage].isPositionFixed;
      copie[k].centre = NULL;
      copie[k].lieuGauche = NULL;
      copie[k].lieuDroite = NULL;
      copie[k].position = symboles[k+decalage].position;
	  
      copie[k].isOrientationFixed = symboles[k+decalage].isOrientationFixed;
      copie[k].gaucheOrient.orient = NULL;
      copie[k].droiteOrient.orient = NULL;
      copie[k].orientation = symboles[k+decalage].orientation;
	  
    }

    copie[0].isPositionFixed = true;
    copie[0].position = debut.getDeplacement();
    copie[0].isOrientationFixed = true;
    copie[0].orientation = debut.getRotation();
    //cout<<"---------------------------------------------------"<<endl;
    //cout<<">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"<<endl;
    forwardAnalyse(copie,NNN,0,debut);
    //cout<<">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"<<endl;
    if(copie[NNN-1].link->getType()==Liaison::ROTOIDE){
      copie[NNN-1].isOrientationFixed = false;
      copie[NNN-1].droiteOrient.orient = new LPoint(final.getZ());
      copie[NNN-1].isPositionFixed = true;
      copie[NNN-1].position = final.getDeplacement();
    }
    if(copie[NNN-1].link->getType()==Liaison::PRISMAT){
      copie[NNN-1].isOrientationFixed = true;
      copie[NNN-1].orientation = final.getRotation();
    }
    //cout<<"Creations STAR :   "<<++creations<<endl;
    //cout<<"ANCIEN TYPE NULL ET NOUVEAU TYPE LPOINT"<<endl;
    //cout<<"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"<<endl;
    backwardAnalyse(copie,NNN,NNN-1,final);
    //cout<<"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"<<endl;
    //printReflexion(copie,NNN,index);
    exploitation(copie, NNN, newIndex, debut, final, noeud);
    //cout<<"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"<<endl;
    clearMentalConstructs(copie, NNN);
    //cout<<"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"<<endl;
    //cout<<"---------------------------------------------------"<<endl;
  }

}


list<double> MyInverseGeoModel::findSatelliteInOrbit(Liaison * joint, Lieu * centre, Repere frame, Vector3d base, Vector3d lune, bool sens){

  list<double> valeurs;
  if(centre==NULL||joint == NULL){
    cerr<<"Mayday, Call to the Earth, please answer!"<<endl;
    return valeurs;
  }

  int typeG = centre->getType();
  int typeL = joint->getType();
  positionFound = centre->hasPoint(lune);
  if(positionFound){
    switch(typeG){
    case Lieu::DROITE:{
      if(typeL==Liaison::PRISMAT){
	valeurs = findInDroite(joint, (Droite *) centre, frame, base, lune,sens);
      }
    }; break;
    case Lieu::CIRCLE:{
      if(typeL==Liaison::ROTOIDE){
	valeurs = findInCircle(joint, (Cercle *) centre, frame, base, lune,sens);
      }
    }; break;
    case Lieu::SPHERE:{
      if(typeL==Liaison::ROTOIDE){
	valeurs = findInSphere(joint, (Sphere *) centre, frame, base, lune,sens);
      }
    }; break;
    case Lieu::CYLINDRE:{
      if(typeL==Liaison::PRISMAT){
	valeurs = findInCylinder(joint, (Cylindre *) centre, frame, base, lune,sens);
      }
    }; break;
    case Lieu::PLANTOURNANT:{
      if(typeL==Liaison::ROTOIDE){
	valeurs = findInTurningPlan(joint, (PlanTournant *) centre, frame, base, lune,sens);
      }
    }; break;
    case Lieu::TORE:{
      if(typeL==Liaison::ROTOIDE){
	valeurs = findInTore(joint, (Tore *) centre, frame, base, lune,sens);
      }
    }; break;
    case Lieu::DROITETOURNANTE:{
      if(typeL==Liaison::ROTOIDE){
	valeurs = findInTurningDroite(joint, (DroiteTournante *) centre, frame, base, lune,sens);
      }
    }; break;
    case Lieu::PLANGLISSANT:{
      if(typeL==Liaison::PRISMAT){
	valeurs = findInSlidingPlan(joint, (PlanGlissant *) centre, frame, base, lune,sens);
      }
    }; break;
    default: cout<<"Type d'orbite non pris en compte:"<<Lieu::getStringType(typeG)<<endl;
    }
  }
  if(valeurs.size()==0){
    cout<<"Aucune valeur trouvee pour ce "<<Lieu::getStringType(typeG)<<" contient-il le point? Reponse:"<<centre->hasPoint(lune)<<endl;     
  }
  return valeurs;
}


void MyInverseGeoModel::printReflexion(Abstraction * copie, int  N, int index){
  for(int k= N-1;k>=0;k--){
    cout <<"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;
    cout <<"Liaison #"<<k+1+index<<endl;
    if(copie[k].link!=NULL){
      if(copie[k].link->getType()==Liaison::PRISMAT){
	cout<<"       TYPE PRISMATIQUE"<<endl;
      }
      if(copie[k].link->getType()==Liaison::ROTOIDE){
	cout<<"       TYPE ROTOIDE"<<endl;
      }
    }
    if(copie[k].isPositionFixed){
      cout <<"la position est connue "<<endl;
      cout << copie[k].position.transpose()<<endl;
    }
    if(copie[k].isOrientationFixed){
      cout <<"L'orientation est connue "<<endl;
      cout << copie[k].orientation<<endl;
    }
    if(copie[k].lieuDroite!=NULL){
      cout <<"LA LIAISON APPARTIENT A DROITE AU LIEU GEOMETRIQUE:"<<endl;
      cout <<(*(copie[k].lieuDroite))<<endl;
    }
    if(copie[k].droiteOrient.orient!=NULL){
      cout <<"A DROITE LE VECTEUR LIE ["<<copie[k].droiteOrient.axe.transpose()<<"]^T APPARTIENT AU LIEU GEOMETRIQUE:"<<endl;
      cout <<(*(copie[k].droiteOrient.orient))<<endl;
    }
    else if(copie[k].droiteOrient.everything){
      cout<<"A DROITE LA LIAISON PEUT AVOIR TOUTES LES ORIENTATIONS"<<endl;
    }
    if(copie[k].lieuGauche!=NULL){
      cout <<"LA LIAISON APPARTIENT A GAUCHE AU LIEU GEOMETRIQUE:"<<endl;
      cout <<(*(copie[k].lieuGauche))<<endl;
    }
    if(copie[k].gaucheOrient.orient!=NULL){
      cout <<"A GAUCHE LE VECTEUR LIE ["<<copie[k].gaucheOrient.axe.transpose()<<"]^T APPARTIENT AU LIEU GEOMETRIQUE:"<<endl;
      cout <<(*(copie[k].gaucheOrient.orient))<<endl;
    }
    else if(copie[k].gaucheOrient.everything){
      cout<<"A GAUCHE LA LIAISON PEUT AVOIR TOUTES LES ORIENTATIONS"<<endl;
    }
    cout <<"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;
  }
      
}						    

list<LPoint> MyInverseGeoModel::searchPointsIntersectionGauche(Lieu * terre, Lieu * ciel){
  bool check =false;  
  bool special = false;
  bool creationG = false;
  bool creationD = false;
  list<LPoint> points;

  int typeG1 = terre->getType();
  int typeG2 = ciel->getType();

  if(typeG1==Lieu::DROITETOURNANTE){
    special = true;
    DroiteTournante * dt = (DroiteTournante *) terre;
    double scal = dt->getDirection().transpose()*dt->getRotation();
    if(isEqual(scal,1,CRAYON) || isEqual(scal,1,CRAYON)){
      terre = new Cylindre(dt->getCircleCenter(),dt->getDirection());
      creationG = true;
      typeG1 = terre->getType();
    }
  }

  if(typeG1==Lieu::CYLINDRE){
    special = true;
    Cylindre * cy = (Cylindre *) terre;
    if(isEqual(cy->getRayon(),0,CRAYON)){
      if(creationG){
	delete terre;
      }
      terre = new Droite(cy->getDroite());
      creationG = true;
      typeG1 = terre->getType();
    }
  }

  if(typeG2==Lieu::DROITETOURNANTE){
    special = true;
    DroiteTournante * dt = (DroiteTournante *) ciel;
    double scal = dt->getDirection().transpose()*dt->getRotation();
    if(isEqual(scal,1,CRAYON) || isEqual(scal,1,CRAYON)){
      ciel = new Cylindre(dt->getCircleCenter(),dt->getDirection());
      creationD = true;
      typeG2 = ciel->getType();
    }
  }

  if(typeG2==Lieu::CYLINDRE){
    special = true;
    Cylindre * cy = (Cylindre *) ciel;
    if(isEqual(cy->getRayon(),0,CRAYON)){
      if(creationD){
	delete ciel;
      }
      ciel = new Droite(cy->getDroite());
      creationD = true;
      typeG2 = ciel->getType();
    }
  }

  if(typeG1==Lieu::DROITE && typeG2==Lieu::DROITE){
    check = true;
    points = Architect::intersection(*((Droite *)terre),*((Droite *)ciel));
  }
  if(typeG1==Lieu::DROITE && typeG2==Lieu::CIRCLE){
    check = true;
    points = Architect::intersection(*((Droite *)terre),*((Cercle *)ciel));
  }
  if(typeG1==Lieu::CIRCLE && typeG2==Lieu::DROITE){
    check = true;
    points = Architect::intersection(*((Droite *)ciel),*((Cercle *)terre));
  }
  if(typeG1==Lieu::DROITE && typeG2==Lieu::PLAN){
    check = true;
    points = Architect::intersection(*((Droite *)terre),*((Plan *)ciel));
  }
  if(typeG1==Lieu::DROITE && typeG2==Lieu::SPHERE){
    check = true;
    points = Architect::intersection(*((Droite *)terre),*((Sphere *)ciel));
  }
  if(typeG1==Lieu::SPHERE && typeG2==Lieu::DROITE){
    check = true;
    points = Architect::intersection(*((Droite *)ciel),*((Sphere *)terre));
  }
  if(typeG1==Lieu::CIRCLE && typeG2==Lieu::PLAN){
    check = true;
    points = Architect::intersection(*((Plan *)ciel),*((Cercle *)terre));
  }
  if(typeG1==Lieu::CIRCLE && typeG2==Lieu::CIRCLE){
    check = true;
    points = Architect::intersection(*((Cercle *)terre),*((Cercle *)ciel));
  }
  if(typeG1==Lieu::CIRCLE && typeG2==Lieu::SPHERE){
    check = true;
    points = Architect::intersection(*((Cercle *)terre),*((Sphere *)ciel));
  }
  if(typeG1==Lieu::SPHERE && typeG2==Lieu::CIRCLE){
    check = true;
    points = Architect::intersection(*((Cercle *)ciel),*((Sphere *)terre));
  }
  if(points.size()<=0 && typeG2==Lieu::SPHERE){
    special = true;
    Sphere * ss = (Sphere *) ciel;
    if(isEqual(ss->getRayon(),0,CRAYON)){
      if(terre->hasPoint(ss->getCenter())){
	points.push_back(ss->getCenter());
      }
    }
  }
  if(points.size()<=0 && typeG2==Lieu::CIRCLE){
    special = true;
    Cercle * cc = (Cercle *) ciel;
    if(isEqual(cc->getRayon(),0,CRAYON)){
      if(terre->hasPoint(cc->getCenter())){
	points.push_back(cc->getCenter());
      }
    }
  }
  
  if(creationG){
    delete terre;
  }
  if(creationD){
    delete ciel;
  }
    
  if(points.size()>0){
    positionFound = true;
    //cout<<"Points d'intersection trouv'es!"<<endl;
  }
  else{
    if(check){
      cout<<"\nAucun point d'intersection trouve"<<endl;
      cout<<"INTERSECTION ENTRE 11111111111111111111111111111111111111111111111111111111111111"<<endl;
      cout<<*terre<<endl;
      cout<<"ET                 22222222222222222222222222222222222222222222222222222222222222"<<endl;
      cout<<*ciel<<endl;
      cout<<"FIN INTERSECTION  999999999999999999999999999999999999999999999999999999999999999\n"<<endl;
    }
    else{
      cout<<"L'intersection entre un(e) "<<Lieu::getStringType(typeG1)<<" a GAUCHE et un(e) "<<Lieu::getStringType(typeG2)<<" a DROITE dans le cas general n'est pas testee"<<endl;
      if(special){
	cout<<"Pourtant notre algorithme a teste si le probleme pouvait etre reduit a un cas qu'il peut traiter!"<<endl;
      }
    }
  }
  return points;
}


void MyInverseGeoModel::clearMentalConstructs(Abstraction * symboles, int nombre){

  for(int i = 0; i<nombre; i++){
    if(symboles[i].lieuGauche != NULL){
      delete symboles[i].lieuGauche;
      //cout<<"Supresssions      :"<<++suppressions<<endl;
    }
    if(symboles[i].lieuDroite != NULL){
      delete symboles[i].lieuDroite;
      //cout<<"Supresssions      :"<<++suppressions<<endl;
    }
    if(symboles[i].gaucheOrient.orient!=NULL){
      delete symboles[i].gaucheOrient.orient;
      //cout<<"Supresssions STAR :"<<++suppressions<<endl;
    }
    if(symboles[i].droiteOrient.orient!=NULL){
      delete symboles[i].droiteOrient.orient;
      //cout<<"Supresssions STAR    :"<<++suppressions<<endl;
    }
    symboles[i].centre = NULL;
    symboles[i].link = NULL;
  }

  delete[] symboles;

}

void MyInverseGeoModel::ajout(Node * noeud,int index, list<double> liste){
  list<double>::const_iterator it;
  for(it = liste.begin();it!=liste.end();it++){
    ajout(noeud,index,(*it));
  }
}

void MyInverseGeoModel::ajout(Node * noeud,int index, double val){
  affectation solution;
  solution.index = index;
  solution.value = val;
  Node * fils = new Node(solution);
  cout <<"q("<<index+1<<") = "<<val/M_PI<<" * PI = "<<val<<endl;
  noeud->addChild(fils);
}


void MyInverseGeoModel::forwardAnalyse(Abstraction * symboles, int N, int k, Repere base){
  if(symboles[k].link == NULL){
    cerr<<"Je ne trouve pas la liaison!"<<endl;
    return;
  }
  
  Liaison * link = symboles[k].link;
  Corps * body;
  int i = k;
  int j = k;
  int typeLiaison;
  Repere transfo;
  Repere debut = base;
  Repere terme;
  Lieu * geo = NULL;
  geo = symboles[k].lieuGauche;
  Vector3d levier;
  bool temoin = true;
  int stop = 0;
  Liaison * next = robot->next(link);
  Liaison * lien = NULL;

  while(next!=NULL && i<N-1){
    
    body = link->getCorpsB();
    if(body==NULL){
      transfo = Repere();
    }
    else{
      transfo  = body->getBoutAbout();
    }
    
    j = i+1;

    while(j<N){
      lien = symboles[j].link;
      if(lien!=NULL){
	if(symboles[j].isSetted){
	  VectorXd val = VectorXd::Zero(1);
	  val(0) = symboles[j].valeur;
	  transfo = transfo * link->getFromMetoBodyB(val);
	  body = lien->getCorpsB();
	  if(body!=NULL){
	    transfo = transfo * body->getBoutAbout();
	  }
	  j++;
	}
	else{
	  next = lien;
	  break;
	}
      }
      else{
	break;
      }
    }

    terme = debut*transfo;
    
    analyseOrientation(symboles,N, i, j, debut, terme,true);

    debut =  terme;
    i = j;
    link = next;
    next = robot->next(link);

  }

  i = k;
  link = symboles[k].link;
  next = robot->next(link);
  debut = base;

  if(symboles[k].isPositionFixed == false && symboles[k].lieuGauche == NULL){
    cerr<<"Le debut de ma chaine devrait etre fixee!"<<endl;
    return;
  }
  

  while(next != NULL && stop == 0 && i<N-1){
    
    body = link->getCorpsB();
    if(body==NULL){
      transfo = Repere();
    }
    else{
      transfo  = body->getBoutAbout();
    }
    
    if(symboles[i].isPositionFixed && temoin == false){
      break;
    }

    j = i+1;
    
    temoin = false;
    typeLiaison = link->getType();
    
    while(j<N){
      lien = symboles[j].link;
      if(lien!=NULL){
	if(symboles[j].isSetted){
	  VectorXd val = VectorXd::Zero(1);
	  val(0) = symboles[j].valeur;
	  transfo = transfo * link->getFromMetoBodyB(val);
	  body = lien->getCorpsB();
	  if(body!=NULL){
	    transfo = transfo * body->getBoutAbout();
	  }
	  j++;
	}
	else{
	  next = lien;
	  break;
	}
      }
      else{
	break;
      }
    }

    terme = debut*transfo;
    levier = terme.getDeplacement()- debut.getDeplacement();
    
    if(geo==NULL){
      if(typeLiaison==Liaison::PRISMAT){
	Droite * d = new Droite(terme.getDeplacement(),debut.getZ());
	//cout<<"Creations     :"<<++creations<<endl;
	geo  = d;
	if(j<N){
	  symboles[j].isPositionFixed = false;
	  symboles[j].lieuGauche = geo;
	}
      }
      if(typeLiaison==Liaison::ROTOIDE){
	Cercle * cc = new Cercle();
	//cout<<"Creations     :"<<++creations<<endl;
	Vector3d centre = Architect::projection(debut.getZ(),levier);
	cc->setCenter(debut.getDeplacement()+centre);
	Vector3d rayon = levier - centre;
	cc->setDirection(debut.getZ());
	cc->setRayon(rayon.norm());
	geo  = cc;
	if(j<N){
	  symboles[j].lieuGauche = geo;
	  if(isEqual(rayon.norm(),0,CRAYON)){
	    symboles[j].isPositionFixed = true;
	    temoin = true;
	    symboles[j].position = cc->getCenter(); 
	  }
	}
      }
    }
    else{
      temoin = universalAnalyse(symboles,N,&geo,i,j, debut,terme,true,&stop);
    }
    debut =  terme;
    i = j;
    link = next;
    next = robot->next(link);
  }
  symboles[k].centre = geo;
  
}

void MyInverseGeoModel::backwardAnalyse(Abstraction * symboles, int N,int k, Repere base){
  if(symboles[k].link == NULL){
    return;
  }
  Liaison * link = symboles[k].link;
  Corps * body;
  int i = k;
  int j = k;
  int typeLiaison = link->getType();
  Lieu * geo = NULL;
  geo  = symboles[k].lieuDroite;
  int previousType;
  Repere transfo;
  Repere debut = base;
  Repere terme;
  bool temoin = true;
  Vector3d levier;
  int stop = 0;
  Liaison * previous = robot->previous(link);
  Liaison * lien = NULL;

  while(previous!=NULL && i>0){
    body = link->getCorpsA();
    if(body==NULL){
      transfo = Repere();
    }
    else{
      transfo  = body->getBoutAbout();
    }
    
    j = i-1;
    terme = debut/transfo;

    while(j>0){
      lien = symboles[j].link;
      if(lien!=NULL){
	if(symboles[j].isSetted){
	  VectorXd val = VectorXd::Zero(1);
	  val(0) = symboles[j].valeur;
	  terme = terme/link->getFromMetoBodyB(val);
	  body = lien->getCorpsA();
	  if(body!=NULL){
	    terme = terme/body->getBoutAbout();
	  }
	  j--;
	}
	else{
	  previous = lien;
	  break;
	}
      }
      else{
	break;
      }
    }

    analyseOrientation(symboles,N, i, j, debut, terme,false);
    Lieu * premier = symboles[j].droiteOrient.orient;
    analyseOrientation(symboles,N, j, j, terme, terme,false);
    Lieu * second = symboles[j].droiteOrient.orient;

    if(premier!=NULL){
      if(premier!=second){
	delete premier;
      }
    }

    debut =  terme;
    i = j;
    link = previous;
    previous = robot->previous(link);
  }
 
  i = k;
  link = symboles[k].link;
  previous = robot->previous(link);
  debut = base;

  if(symboles[k].isPositionFixed == false && symboles[k].lieuDroite == NULL && typeLiaison == Liaison::ROTOIDE){
    return;
  }
  
  while(previous!=NULL && stop == 0 && i>0){
    body = link->getCorpsA();
    if(body==NULL){
      transfo = Repere();
    }
    else{
      transfo  = body->getBoutAbout();
    }
    if(symboles[i].isPositionFixed && temoin == false){
      break;
    }

    j = i-1;
    
    temoin = false;
    typeLiaison = link->getType();
    previousType  =  previous->getType();
    terme = debut/transfo;

    while(j>0){
      lien = symboles[j].link;
      if(lien!=NULL){
	if(symboles[j].isSetted){
	  VectorXd val = VectorXd::Zero(1);
	  val(0) = symboles[j].valeur;
	  terme = terme/link->getFromMetoBodyB(val);
	  body = lien->getCorpsA();
	  if(body!=NULL){
	    terme = terme/body->getBoutAbout();
	  }
	  j--;
	}
	else{
	  previous = lien;
	  previousType = previous->getType();
	  break;
	}
      }
      else{
	break;
      }
    }

    levier = terme.getDeplacement()- debut.getDeplacement();

    if(typeLiaison == Liaison::ROTOIDE && previousType == Liaison::ROTOIDE){
      if(geo==NULL){
	Cercle * cc = new Cercle();
	//cout<<"Creations     :"<<++creations<<endl;
	Vector3d centre = Architect::projection(debut.getZ(),levier);
	cc->setCenter(debut.getDeplacement()+centre);
	Vector3d rayon = levier - centre;
	cc->setDirection(debut.getZ());
	cc->setRayon(rayon.norm());
	geo  = cc;
	symboles[j].lieuDroite = geo;
	if(isEqual(rayon.norm(),0,CRAYON)){
	  symboles[j].isPositionFixed = true;
	  temoin = true;
	  symboles[j].position = cc->getCenter();
	}
      }
      else{
	temoin = universalAnalyse(symboles,N,&geo,i,j, debut,terme,false,&stop);
      }
    }

    if(typeLiaison == Liaison::PRISMAT && previousType == Liaison::ROTOIDE){
      if(geo==NULL){
	Droite * d = new Droite(debut.getDeplacement(),debut.getZ());
	//cout<<"Creations     :"<<++creations<<endl;
	geo  = d;
	symboles[i].isPositionFixed = false;
	symboles[i].lieuDroite = geo;
	Droite * dd = new Droite(terme.getDeplacement(),debut.getZ());
	//cout<<"Creations     :"<<++creations<<endl;
	geo  = dd;
	symboles[j].isPositionFixed = false;
	symboles[j].lieuDroite = geo;
      }
      else{
	temoin = universalAnalyse(symboles,N,&geo,i,j, debut,terme,false,&stop);
      }
    }

    if(typeLiaison == Liaison::ROTOIDE && previousType == Liaison::PRISMAT){
      if(geo==NULL){
	Cercle * cc = new Cercle();
	//cout<<"Creations     :"<<++creations<<endl;
	Vector3d centre = Architect::projection(debut.getZ(),levier);
	cc->setCenter(debut.getDeplacement()+centre);
	Vector3d rayon = levier - centre;
	cc->setDirection(debut.getZ());
	cc->setRayon(rayon.norm());
	geo  = cc;
	symboles[i].lieuDroite = geo;
	if(isEqual(rayon.norm(),0,CRAYON)){
	  symboles[i].isPositionFixed = true;
	  symboles[i].position = cc->getCenter();
	}
	Droite dd(terme.getDeplacement(),terme.getZ());
	DroiteTournante * dt  = new DroiteTournante();
	dt->setRotation(cc->getDirection());
	dt->setCenter(cc->getCenter());
	dt->setDroite(dd);
	//cout<<"Creations     :"<<++creations<<endl;
	geo  = dt;
	symboles[j].isPositionFixed = false;
	symboles[j].lieuDroite = geo;
      }
      else{
	temoin = universalAnalyse(symboles,N,&geo,i,j, debut,terme,false,&stop);
	Lieu * oldGeo = geo;
	symboles[j].lieuDroite = NULL;
	temoin = universalAnalyse(symboles,N,&geo,j,j, terme,terme,false,&stop);
	if(oldGeo!=geo){
	  delete oldGeo;
	  //cout<<"Supresssions    :"<<++suppressions<<endl;
	}
      }
    }

    if(typeLiaison == Liaison::PRISMAT && previousType == Liaison::PRISMAT){
      if(geo==NULL){
	Droite * d = new Droite(debut.getDeplacement(),debut.getZ());
	//cout<<"Creations     :"<<++creations<<endl;
	geo  = d;
	symboles[i].isPositionFixed = false;
	symboles[i].lieuDroite = geo;
	temoin = universalAnalyse(symboles,N,&geo,i,j, debut,terme,false,&stop);
      }
      else{
	temoin = universalAnalyse(symboles,N,&geo,i,j, debut,terme,false,&stop);
	Lieu * oldGeo = geo;
	symboles[j].lieuDroite = NULL;
	temoin = universalAnalyse(symboles,N,&geo,j,j, terme,terme,false,&stop);
	if(oldGeo!=geo){
	  delete oldGeo;
	  //cout<<"Supresssions    :"<<++suppressions<<endl;
	}	
      }
    }
    debut =  terme;
    i = j;
    link = previous;
    previous = robot->previous(link);
  }
  symboles[k].centre = geo;
   
}


void MyInverseGeoModel::analyseOrientation(Abstraction * symboles,int N, int i, int j, Repere debut, Repere terme, bool sens){

  if(i<0 || j<0 || i>N ||j>N){
    cerr<<"Debordement de tableau: i="<<i<<" , j="<<j<<" , N="<<N<<endl;
    return;
  }
  
  
  Lieu * leNord;
  Vector3d axe;
  Vector3d nouvelAxe;
  bool inutile = false;

  if(sens){
    leNord = symboles[i].gaucheOrient.orient;
    axe = symboles[i].gaucheOrient.axe;
    inutile = symboles[i].gaucheOrient.everything;
  }
  else{
    leNord = symboles[i].droiteOrient.orient;
    axe = symboles[i].droiteOrient.axe;
    inutile = symboles[i].droiteOrient.everything;
  }

  if(inutile){
    if(sens){
      symboles[j].gaucheOrient.everything = true;
    }
    else{
      symboles[j].droiteOrient.everything = true;
    }
    return;
  }

  if(symboles[i].isOrientationFixed == false && symboles[i].gaucheOrient.orient == NULL && sens){
    //cerr<<"La direction aurait du etre montree!"<<endl;
    return;
  }

  if(symboles[i].isOrientationFixed == false && symboles[i].droiteOrient.orient == NULL && sens==false){
    //cerr<<"La direction aurait du etre montree!"<<endl;
    return;
  }

  nouvelAxe = axe;

  int typeLiaison = symboles[i].link->getType();
  VectRotation orient = symboles[i].orientation;
  bool naissance = false;
  int typeInstrument;
  
  bool solved = false;

  Vector3d constante;
  constante = debut.getZ();
  constante = terme.getVectorInME(constante);

  if(leNord==NULL){
    if(typeLiaison==Liaison::PRISMAT){
      symboles[j].isOrientationFixed = true;
      if(sens)
	symboles[j].gaucheOrient.orient = NULL;
      else
	symboles[j].droiteOrient.orient = NULL;
    }
    if(typeLiaison==Liaison::ROTOIDE){
      LPoint * lp = new LPoint(debut.getZ());
      naissance = true;
      //cout<<"Creations STAR     :"<<++creations<<endl;
      leNord = lp;
      nouvelAxe = constante;
    }
    solved = true;
  }
  else{
    
    typeInstrument = leNord->getType();
    
    if(typeLiaison == Liaison::PRISMAT && typeInstrument == Lieu::POINT){
      LPoint * lp = (LPoint *)leNord;
      LPoint * llpp = new LPoint(*lp);
      naissance = true;
      //cout<<"Creations STAR     :"<<++creations<<endl;
      leNord = llpp;
      nouvelAxe = terme.getVectorInME(lp->getCenter());
      solved = true;
    }

    if(typeLiaison == Liaison::PRISMAT && typeInstrument == Lieu::CIRCLE){
      nouvelAxe = constante;
      Cercle * cc = (Cercle *)leNord;
      Cercle * cc22 = new Cercle(*cc);
      naissance = true;
      //cout<<"Creations STAR     :"<<++creations<<endl;
      leNord = cc22;
      solved = true;
    }
    
    if(typeLiaison == Liaison::ROTOIDE && typeInstrument == Lieu::POINT){
      LPoint * lp = (LPoint *)leNord;
      double scal  = axe.transpose()*Vector3d::UnitZ();
      nouvelAxe = constante;
      if(isEqual(scal,1,CRAYON) || isEqual(scal,-1,CRAYON)){
	LPoint * lplp = new LPoint(*lp);
	naissance = true;
	//cout<<"Creations STAR     :"<<++creations<<endl;
	leNord = lplp;
      }
      else{
	Vector3d centre = Architect::projection(lp->getCenter(),debut.getZ());
	Vector3d rayon = debut.getZ() - centre;
	Cercle * cc = new Cercle();
	naissance = true;
	//cout<<"Creations STAR     :"<<++creations<<endl;
	cc->setCenter(centre);
	cc->setRayon(rayon.norm());
	cc->setDirection(lp->getCenter());
	leNord = cc;
      }
      solved = true;
    }

    if(typeLiaison == Liaison::ROTOIDE && typeInstrument == Lieu::CIRCLE){
      Cercle * cc = (Cercle *)leNord;
      //double scal = cc->getDirection().transpose()*debut.getZ();
      //double scal2 = debut.getZ().transpose()*terme.getZ();
      double scal3 = Vector3d::UnitZ().transpose()*axe;
      if(isEqual(scal3,0,CRAYON) && isEqual(cc->getRayon(),1,CRAYON)){
	solved = true;
	inutile = true;
      }
      if(isEqual(scal3,1,CRAYON) || isEqual(scal3,-1,CRAYON)){
	Cercle * cc2 = new Cercle(*cc);
	naissance = true;
	//cout<<"Creations STAR     :"<<++creations<<endl;
	nouvelAxe = constante;
	leNord = cc2;
	solved = true;
      }
      if(solved==false){
	Tore * tr = new Tore();
	naissance = true;
	nouvelAxe = constante;
	//cout<<"Creations STAR     :"<<++creations<<endl;
	tr->setRotation(cc->getDirection());
	tr->setCenter(Vector3d(0,0,0));
	Vector3d centre = Architect::projection(debut.getZ(),terme.getZ());
	Vector3d rayon = terme.getZ() - centre;
	Cercle ccTempo;
	ccTempo.setCenter(centre);
	ccTempo.setRayon(rayon.norm());
	ccTempo.setDirection(debut.getZ());
	tr->setCercle(ccTempo);
	leNord = tr;
      }
    }
  }

  if(sens){
    symboles[j].gaucheOrient.axe = nouvelAxe;
  }
  else{
    symboles[j].droiteOrient.axe = nouvelAxe;
  }

  if(naissance){
    if(sens)
      symboles[j].gaucheOrient.orient = leNord;
    else
      symboles[j].droiteOrient.orient = leNord;
  }
  else{
    symboles[j].orientation = terme.getRotation();
    if(solved){
      if(inutile){
	if(sens){
	  symboles[j].gaucheOrient.everything = true;
	}
	else{
	  symboles[j].droiteOrient.everything = true;
	}
      }
      else{
	symboles[j].isOrientationFixed = true;
      }
    }
    if(sens)
      symboles[j].gaucheOrient.orient = NULL;
    else
      symboles[j].droiteOrient.orient = NULL;
  }

}

bool MyInverseGeoModel::universalAnalyse(Abstraction * symboles, int N, Lieu ** geo, int i, int j, Repere debut, Repere terme, bool sens, int * stop){
  bool temoin = false;

  bool inside = j>=0&&j<N;
  if(inside==false){
    return false;
  }

  bool naissance = false;
  *stop = 0;

  Liaison * link  = symboles[i].link;
  if(link==NULL){
    *stop = 1;
    return false;
  }
  int typeLiaison = link->getType();
  Vector3d levier = terme.getDeplacement()- debut.getDeplacement();

  int typeG = (*geo)->getType();

  if(typeLiaison==Liaison::PRISMAT){
    switch(typeG){
    case Lieu::DROITE:{
      Droite * dd = (Droite *)(*geo);
      double scal = dd->getDirection().transpose()*debut.getZ();
      if(isEqual(scal,1,CRAYON) || isEqual(scal,-1,CRAYON)){
	Droite * ddrr = new Droite();
	naissance = true;
	//cout<<"Creations     :"<<++creations<<endl;
	ddrr->setCenter(terme.getDeplacement());
	ddrr->setDirection(dd->getDirection());
	(*geo) = ddrr;
      }
      else{
	Vector3d normale = dd->getDirection().cross(debut.getZ());
	Plan * pp = new Plan();
	naissance = true;
	//cout<<"Creations     :"<<++creations<<endl;
	pp->setDirection(normale);
	pp->setCenter(terme.getDeplacement());
	(*geo) = pp;
      }
      
    } break;
    case Lieu::PLAN:{
      Plan * pp = (Plan *) (*geo);
      double scal = pp->getDirection().transpose()*debut.getZ();
      if(isEqual(scal,0,CRAYON)){
	Plan * PP = new Plan();
	naissance = true;
	//cout<<"Creations     :"<<++creations<<endl;
	PP->setCenter(terme.getDeplacement());
	PP->setDirection(pp->getDirection());
	(*geo) = PP;
      }
      else{
	PlanGlissant * pg = new PlanGlissant(*pp);
	naissance = true;
	//cout<<"Creations     :"<<++creations<<endl;
	(*geo) = pg;
      }
    } break;
    case Lieu::CIRCLE:{
      Cercle * cc = (Cercle *)(*geo);
      DroiteTournante * dt = new DroiteTournante();
      naissance = true;
      //cout<<"Creations     :"<<++creations<<endl;
      dt->setCenter(cc->getCenter());
      dt->setRotation(cc->getDirection());
      dt->setDroite(Droite(terme.getDeplacement(),debut.getZ()));
      (*geo) = dt;
    } break;
    case Lieu::CYLINDRE:{
      Cylindre * cc = (Cylindre *)(*geo);
      double scal = cc->getTranslation().transpose()*debut.getZ();
      if(isEqual(scal,1,CRAYON) || isEqual(scal,-1,CRAYON)){
	Cercle cc2;
	levier = terme.getDeplacement()- cc->getCenter();
	Vector3d fleche = Architect::projection(cc->getDirection(),levier);
	Vector3d rayons = levier - fleche;
	cc2.setCenter(cc->getCenter()+ fleche);
	cc2.setRayon(rayons.norm());
	cc2.setDirection(cc->getDirection());
	Cylindre * cy = new Cylindre(cc2,debut.getZ());
	naissance = true;
	//cout<<"Creations     :"<<++creations<<endl;
	(*geo) = cy;
      }
      else{
	*stop = 1; return temoin;
      }
    } break;
    case Lieu::SPHERE:{
      *stop = 1; return temoin;
    } break;
    case Lieu::PLANTOURNANT:{
      *stop = 1; return temoin;
    } break;
    case Lieu::CONE:{
      *stop = 1; return temoin;
    } break;
    case Lieu::TORE:{
      *stop = 1; return temoin;
    } break;
    case Lieu::DROITETOURNANTE:{
      DroiteTournante * dt = (DroiteTournante *)(*geo);
      list<Droite> liste = dt->getSuitableDroites(debut.getDeplacement());
      list<Droite>::const_iterator it;
      it = liste.begin();
      if(it!=liste.end()){
	Droite dd = (*it);
	double scal = dd.getDirection().transpose()*debut.getZ();
	if(isEqual(scal,1,CRAYON) || isEqual(scal,-1,CRAYON)){
	  DroiteTournante * dtt = new DroiteTournante();
	  naissance = true;
	  //cout<<"Creations     :"<<++creations<<endl;
	  dtt->setCenter(dt->getCenter());
	  dtt->setRotation(dt->getRotation());
	  dtt->setDroite(Droite(terme.getDeplacement(),debut.getZ()));
	  (*geo) = dtt;
	}
	else{
	  Vector3d normale = dd.getDirection().cross(debut.getZ());
	  normale.normalize();
	  Plan pp;
	  pp.setCenter(terme.getDeplacement());
	  pp.setDirection(normale);
	  PlanTournant * pt = new PlanTournant();
	  naissance = true;
	  //cout<<"Creations     :"<<++creations<<endl;
	  pt->setCenter(dt->getCenter());
	  pt->setRotation(dt->getRotation());
	  pt->setPlan(pp);
	  (*geo) = pt;
	}
      }
	  
    } break;
    case Lieu::PLANGLISSANT:{
      *stop = 1; return temoin;
    } break;
    default:*stop = 1; return temoin;
    }
  }
  if(typeLiaison==Liaison::ROTOIDE){
    switch(typeG){
    case Lieu::DROITE:{
      Droite * dd = (Droite *)(*geo);
      Cercle cc;
      cc.setDirection(debut.getZ());
      Vector3d fleche = Architect::projection(debut.getZ(),levier);
      cc.setCenter(debut.getDeplacement()+fleche);
      Vector3d ray = levier - fleche;
      cc.setRayon(ray.norm());
      Cylindre * cy = new Cylindre(cc,dd->getDirection());
      naissance = true;
      //cout<<"Creations     :"<<++creations<<endl;
      (*geo) = cy;
    } break;
    case Lieu::PLAN:{
      Plan * pp = (Plan *)(*geo);
      double scal = pp->getDirection().transpose()*debut.getZ();
      if(isEqual(scal,1,CRAYON) || isEqual(scal,-1,CRAYON)){
	Plan  * pppp = new Plan();
	naissance = true;
	//cout<<"Creations     :"<<++creations<<endl;
	pppp->setCenter(terme.getDeplacement());
	pppp->setDirection(pp->getDirection());
	(*geo) = pppp;	
      }
      else{
	*stop = 1; return temoin;
      }
    } break;
    case Lieu::CIRCLE:{
      Cercle * cc = (Cercle *)(*geo);
      double scal = cc->getDirection().transpose()*debut.getZ();
      Vector3d fleche = Architect::projection(debut.getZ(),levier);
      Vector3d arc = Architect::projection(cc->getDirection(),fleche);
      Vector3d rayon1 = debut.getDeplacement()-cc->getCenter() + fleche - arc;
      Vector3d rayon2 = levier - fleche;
      //Vector3d croix = cc->getDirection().cross(debut.getZ());

      if(isEqual(cc->getRayon(),0,CRAYON) && isEqual(scal,0,CRAYON) && (isEqual(fleche.norm(),0,CRAYON) || isEqual(rayon2.norm(),0,CRAYON))){
	double demiDiametre;
	if(isEqual(fleche.norm(),0,CRAYON)){
	  Sphere * sp = new Sphere();
	  naissance = true;
	  //cout<<"Creations     :"<<++creations<<endl;
	  sp->setCenter(cc->getCenter());
	  demiDiametre = rayon2.norm(); 
	  sp->setRayon(demiDiametre);
	  (*geo) = sp;
	}
	else{
	  Cercle *cc2 = new Cercle();
	  naissance = true;
	  //cout<<"JE SUIS DANS CE CAS LA Rayon ="<<cc->getRayon()<<"     -11111111111111111111111111111111111111"<<endl;
	  //cout<<"Creations     :"<<++creations<<endl;
	  cc2->setCenter(cc->getCenter());
	  cc2->setDirection(cc->getDirection());
	  demiDiametre = fleche.norm(); 
	  cc2->setRayon(demiDiametre);
	  (*geo) = cc2;
	}
	if(isEqual(demiDiametre,0,CRAYON)){
	  temoin = true;
	  symboles[j].position = debut.getDeplacement();
	}
      }
      else if(isEqual(cc->getRayon(),0,CRAYON)==false  && isEqual(scal,0,CRAYON) && isEqual(rayon2.norm(),0,CRAYON)){
	Cercle *cc2 = new Cercle();
	//cout<<"JE SUIS DANS CE CAS LA Rayon ="<<cc->getRayon()<<"   -22222222222222222222222222222222222222"<<endl;
	naissance = true;
	//cout<<"Creations     :"<<++creations<<endl;
	cc2->setCenter(cc->getCenter()+ arc);
	cc2->setDirection(cc->getDirection());
	double demiDiametre = rayon1.norm(); 
	cc2->setRayon(demiDiametre);
	(*geo) = cc2;
	if(isEqual(demiDiametre,0,CRAYON)){
	  temoin = true;
	  symboles[j].position = debut.getDeplacement();
	}
      }
      else if(isEqual(scal,1,CRAYON) || isEqual(scal,-1,CRAYON)){
	if(isEqual(rayon1.norm(),0,CRAYON)||isEqual(rayon2.norm(),0,CRAYON)){
	  double demiDiametre = max(rayon1.norm(),rayon2.norm());
	  Cercle * cc2 = new Cercle();
	  //cout<<"JE SUIS DANS CE CAS LA Rayon ="<<cc->getRayon()<<"  -33333333333333333333333333333333333333"<<endl;
	  naissance = true;
	  //cout<<"Creations     :"<<++creations<<endl;
	  cc2->setRayon(demiDiametre);
	  cc2->setDirection(cc->getDirection());
	  cc2->setCenter(cc->getCenter()+fleche);
	  (*geo) = cc2;
	  if(isEqual(demiDiametre,0,CRAYON)){
	    temoin = true;
	    symboles[j].position = debut.getDeplacement();
	  }
	}
	else{
	  Plan * pp = new Plan();
	  naissance = true;
	  //cout<<"Creations     :"<<++creations<<endl;
	  pp->setCenter(cc->getCenter()+fleche);
	  pp->setDirection(cc->getDirection());
	  (*geo) = pp;
	}
      }
      else {
	Cercle rond;
	rond.setDirection(debut.getZ());
	rond.setCenter(debut.getDeplacement()+fleche);
	rond.setRayon(rayon2.norm());
	Tore * thor = new Tore();
	naissance = true;
	//cout<<"Creations     :"<<++creations<<endl;
	thor->setRotation(cc->getDirection());
	thor->setDistance(rayon1.norm());
	thor->setCercle(rond);
	(*geo) = thor;
      }
    } break;
    case Lieu::CYLINDRE:{
      Cylindre * cc = (Cylindre *)(*geo);
      Vector3d moment = Architect::antiProjection(cc->getTranslation(),debut.getZ());
      if(isEqual(moment.norm(),0,CRAYON)){
	Cylindre * cc2 = new Cylindre(*cc);
	naissance = true;
	//cout<<"Creations     :"<<++creations<<endl;
	(*geo) = cc2;
      }
      else{
	*stop = 1; return temoin;
      }
    } break;
    case Lieu::SPHERE:{
      Sphere * sp = (Sphere *)(*geo);
      if(isEqual(sp->getRayon(),0,CRAYON)){
	Sphere * sspp = new Sphere();
	naissance = true;
	//cout<<"Creations     :"<<++creations<<endl;
	sspp->setCenter(sp->getCenter());
	sspp->setRayon(levier.norm());
	(*geo) = sspp;
	if(isEqual(levier.norm(),0,CRAYON)){
	  temoin = true;
	  symboles[j].position = debut.getDeplacement();
	}
      }
      else{
	*stop = 1; return temoin;
      }
    } break;
    case Lieu::PLANTOURNANT:{
      int count = 0;
      PlanTournant * pt = (PlanTournant *)(*geo);
      list<Plan> candidats =  pt->getSuitablePlans(debut.getDeplacement(),&count);
      list<Plan>::const_iterator it;
      Vector3d direct;
      bool found = false;
      it = candidats.begin();
      while(it!=candidats.end()){
	Plan pp = (Plan)(*it);
	double scal = pp.getDirection().transpose()*debut.getZ();
	if(isEqual(scal,1,CRAYON) || isEqual(scal,-1,CRAYON)){
	  found = true;
	  direct = pp.getDirection();
	  break;
	}
	it++;
      }
      if(found){
	Vector3d fleche = Architect::projection(debut.getZ(),levier);
	Plan pp;
	pp.setCenter(debut.getDeplacement()+fleche);
	pp.setDirection(direct);
	PlanTournant * pptt = new PlanTournant();
	naissance = true;
	//cout<<"Creations     :"<<++creations<<endl;
	pptt->setCenter(pt->getCenter());
	pptt->setRotation(pt->getRotation());
	pptt->setPlan(pp);
	(*geo) = pptt;
      }
      else{
	*stop = 1; return temoin;
      }
    } break;
    case Lieu::CONE:{
      *stop = 1; return temoin;
    } break;
    case Lieu::TORE:{
      int count = -1;
      Tore * thor = (Tore *)(*geo);
      PlanTournant pt = thor->getPlanTournant();
      list<Plan> candidats =  pt.getSuitablePlans(debut.getDeplacement(),&count);
      list<Plan>::const_iterator it;
      Vector3d direct;
      bool found = false;
      it = candidats.begin();
      while(it!=candidats.end()){
	Plan pp = (Plan)(*it);
	double scal = pp.getDirection().transpose()*debut.getZ();
	if(isEqual(scal,1,CRAYON) || isEqual(scal,-1,CRAYON)){
	  found = true;
	  direct = pp.getDirection();
	  break;
	}
	it++;
      }
      if(found){
	Vector3d fleche = Architect::projection(debut.getZ(),levier);
	Plan pp;
	pp.setCenter(debut.getDeplacement()+fleche);
	pp.setDirection(direct);
	PlanTournant * pptt = new PlanTournant();
	naissance = true;
	//cout<<"Creations     :"<<++creations<<endl;
	pptt->setCenter(pt.getCenter());
	pptt->setRotation(pt.getRotation());
	pptt->setPlan(pp);
	(*geo) = pptt;
      }
      else{
	*stop = 1; return temoin;
      }
    } break;
    case Lieu::DROITETOURNANTE:{
      *stop = 1; return temoin;
    } break;
    case Lieu::PLANGLISSANT:{
      *stop = 1; return temoin;
    } break;
    default:*stop = 1; return temoin;
    }
  }

  if(naissance){
    if(sens){
      symboles[j].lieuGauche = (*geo);
      if(temoin){
	symboles[j].isPositionFixed = true;
      }
    }
    else{
      symboles[j].lieuDroite = (*geo);
      Liaison * lien = symboles[j].link;
      if(lien!=NULL){
	if(lien->getType()==Liaison::ROTOIDE){
	  if(temoin){
	    symboles[j].isPositionFixed = true;
	  }
	}
      }
    }
  }
  return temoin;
}


list<double> MyInverseGeoModel::findInSphere(Liaison * fossile, Sphere * centre, Repere frame,Vector3d base,Vector3d lune, bool sens){
  list<double> valeurs;
  
  Repere transfo;
  int i = genese;
  
  if(sens && genese<apocal){
    Repere tempo;
    Liaison * lien = fossile;
    Corps * body = NULL;
    while(lien!=NULL){
      i++;
      body = lien->getCorpsB();
      if(body!=NULL){
	tempo = body->getBoutAbout();
	transfo = transfo * tempo;
      }
      if(i==apocal){
	break;
      }
      lien = robot->next(lien);
    }
  }
  
  if(sens == false && genese>apocal){
    Repere tempo;
    Liaison * lien = fossile;
    Corps * body = NULL;
    while(lien!=NULL){
      i--;
      body = lien->getCorpsA();
      if(body!=NULL){
	tempo = body->getBoutAbout();
	transfo = transfo * tempo.inverse();
      }
      if(i==apocal){
	break;
      }
      lien = robot->previous(lien);
    }
  }
  
  Vector3d  rayon2 = lune - base;
  rayon2 = frame.getVectorInME(rayon2);
  rayon2 = Architect::antiProjection(Vector3d::UnitZ(),rayon2);
  Vector3d rayon1 = Architect::antiProjection(Vector3d::UnitZ(),transfo.getDeplacement());
  
  if(isEqual(rayon1.norm(),0,CRAYON) || isEqual(rayon2.norm(),0,CRAYON)){
    return valeurs;
  }
  rayon1.normalize();
  rayon2.normalize();
  Repere se3;
  Vector3d ZZ(0,0,1);
  se3.setRotation(VectRotation::getRotationOnZ(ZZ,rayon1,rayon2));
  
  VectorXd valX;
  if(fossile->getQValueFor(se3, valX)){
    valeurs.push_back(valX(0));
  }
  return valeurs;
}


list<double> MyInverseGeoModel::findInSlidingPlan(Liaison * fossile,PlanGlissant * centre, Repere frame,Vector3d base,Vector3d lune, bool sens){
  list<double> valeurs;
  Repere transfo;
  Corps * body = NULL;
  if(sens)
    body = fossile->getCorpsB();
  else
    body = fossile->getCorpsA();

  if(body==NULL){
    transfo = Repere();
  }
  else{
    transfo  = body->getBoutAbout();
  }
  if(sens==false)
    transfo = transfo.inverse();
  
  Plan pp = centre->getPlan(lune);
  Droite dd(transfo.getDeplacement(),frame.getZ());
  list<LPoint> liste = Architect::intersection(dd,pp);
  list<LPoint>::const_iterator it;
  for(it = liste.begin();it!=liste.end();it++){
    LPoint lp = (*it);
    lune = lp.getCenter();
    Vector3d haut = lune - base;
    haut = frame.getVectorInME(haut);
    haut = haut - transfo.getDeplacement();
    haut = Architect::projection(Vector3d::UnitZ(),haut);
    Repere se3;
    se3.setDeplacement(haut);
    VectorXd valX;
    if(fossile->getQValueFor(se3, valX)){
      valeurs.push_back(valX(0));
    }
    
  }
  return valeurs;
}


list<double> MyInverseGeoModel::findInDroite(Liaison * fossile,Droite * centre, Repere frame,Vector3d base,Vector3d lune, bool sens){
  list<double> valeurs;
  
  Repere transfo;
  int i = genese;
  
  if(sens && genese<apocal){
    Repere tempo;
    Liaison * lien = fossile;
    Corps * body = NULL;
    while(lien!=NULL){
      i++;
      body = lien->getCorpsB();
      if(body!=NULL){
	tempo = body->getBoutAbout();
	transfo = transfo * tempo;
      }
      if(i==apocal){
	break;
      }
      lien = robot->next(lien);
    }
  }
  
  if(sens == false && genese>apocal){
    Repere tempo;
    Liaison * lien = fossile;
    Corps * body = NULL;
    while(lien!=NULL){
      i--;
      body = lien->getCorpsA();
      if(body!=NULL){
	tempo = body->getBoutAbout();
	transfo = transfo * tempo.inverse();
      }
      if(i==apocal){
	break;
      }
      lien = robot->previous(lien);
    }
  }
  
  Vector3d haut = lune - base;
  haut = frame.getVectorInME(haut)- transfo.getDeplacement();
  Repere se3;
  se3.setDeplacement(haut);
  VectorXd valX;
  if(fossile->getQValueFor(se3, valX)){
    valeurs.push_back(valX(0));
  }
  else{
    //cout <<"SE3 FAUTIF:\n"<<se3<<endl;
  }
  return valeurs;
}

list<double> MyInverseGeoModel::findInCircle(Liaison * fossile, Cercle * centre, Repere frame,Vector3d base,Vector3d lune, bool sens){
  list<double> valeurs;

  Repere transfo;
  int i = genese;
  if(sens && genese<apocal){
    Repere tempo;
    Liaison * lien = fossile;
    Corps * body = NULL;
    while(lien!=NULL){
      i++;
      body = lien->getCorpsB();
      if(body!=NULL){
	tempo = body->getBoutAbout();
	transfo = transfo * tempo;
      }
      if(i==apocal){
	break;
      }
      lien = robot->next(lien);
    }
  }
  
  if(sens == false && genese>apocal){
    Repere tempo;
    Liaison * lien = fossile;
    Corps * body = NULL;
    while(lien!=NULL){
      i--;
      body = lien->getCorpsA();
      if(body!=NULL){
	tempo = body->getBoutAbout();
	transfo = transfo * tempo.inverse();
      }
      if(i==apocal){
	break;
      }
      lien = robot->previous(lien);
    }
  }
  
  Vector3d  rayon2 = lune - base;
  rayon2 = frame.getVectorInME(rayon2);
  rayon2 = Architect::antiProjection(Vector3d::UnitZ(),rayon2);
  Vector3d rayon1 = Architect::antiProjection(Vector3d::UnitZ(),transfo.getDeplacement());
  if(isEqual(rayon1.norm(),0,CRAYON) || isEqual(rayon2.norm(),0,CRAYON)){
    return valeurs;
  }
  rayon1.normalize();
  rayon2.normalize();
  Repere se3;
  Vector3d ZZ(0,0,1);
  se3.setRotation(VectRotation::getRotationOnZ(ZZ,rayon1,rayon2));
  
  VectorXd valX;
  if(fossile->getQValueFor(se3, valX)){
    valeurs.push_back(valX(0));
  }
  return valeurs;
}


list<double> MyInverseGeoModel::findInCylinder(Liaison * fossile,Cylindre * cy, Repere frame,Vector3d base,Vector3d lune, bool sens){
  list<double> valeurs;
  Repere transfo;
  Corps * body = NULL;
  if(sens)
    body = fossile->getCorpsB();
  else
    body = fossile->getCorpsA();

  if(body==NULL){
    transfo = Repere();
  }
  else{
    transfo  = body->getBoutAbout();
  }
  if(sens==false)
    transfo = transfo.inverse();
  
  Cercle cc = cy->getCercle(lune);
  Vector3d  haut = cc.getCenter() - base;
  haut = frame.getVectorInME(haut);
  Repere se3;
  se3.setDeplacement(haut);
  VectorXd valX;
  if(fossile->getQValueFor(se3, valX)){
    valeurs.push_back(valX(0));
  }
  else{
    cout <<"SE3 FAUTIF:\n"<<se3<<endl;
  }
  return valeurs;
}

list<double> MyInverseGeoModel::findInTore(Liaison * fossile,Tore * tr, Repere frame,Vector3d base,Vector3d lune, bool sens){
  list<double> valeurs;
  Repere transfo;
  Corps * body = NULL;
  if(sens)
    body = fossile->getCorpsB();
  else
    body = fossile->getCorpsA();

  if(body==NULL){
    transfo = Repere();
  }
  else{
    transfo  = body->getBoutAbout();
  }
  if(sens==false)
    transfo = transfo.inverse();
  
  int m;
  list<Cercle> liste = tr->getSuitableCircles(lune,&m);
  list<Cercle>::const_iterator it;
  for(it = liste.begin();it!=liste.end();it++){
    Cercle cc = (*it);
    Vector3d d = cc.getDirection();
    Vector3d moyeu = tr->getMoyeu(d);
    Vector3d rayon2 = frame.getVectorInME(moyeu);
    Vector3d rayon1 = Architect::antiProjection(Vector3d::UnitZ(),transfo.getDeplacement());
    rayon2 = Architect::antiProjection(Vector3d::UnitZ(),rayon2);
    if(isEqual(rayon1.norm(),0,CRAYON) || isEqual(rayon2.norm(),0,CRAYON)){
      return valeurs;
    }
    rayon2.normalize();
    rayon1.normalize();
    Repere se3;
    Vector3d ZZ(0,0,1);
    se3.setRotation(VectRotation::getRotationOnZ(ZZ,rayon1,rayon2));
    VectorXd valX;
    if(fossile->getQValueFor(se3, valX)){
      valeurs.push_back(valX(0));
    }
  }
  return valeurs;
}

list<double> MyInverseGeoModel::findInTurningPlan(Liaison * fossile,PlanTournant * pt, Repere frame,Vector3d base,Vector3d lune, bool sens){
  list<double> valeurs;
  Repere transfo;
  Corps * body = NULL;
  if(sens)
    body = fossile->getCorpsB();
  else
    body = fossile->getCorpsA();

  if(body==NULL){
    transfo = Repere();
  }
  else{
    transfo  = body->getBoutAbout();
  }
  if(sens==false)
    transfo = transfo.inverse();
  
  int m;
  list<Plan> liste = pt->getSuitablePlans(lune,&m);
  Tore tr;
  tr.setRotation(pt->getRotation());
  Cercle cc;
  cc.setCenter(pt->getCenter() + 1*pt->getDirection());
  cc.setDirection(pt->getDirection());
  cc.setRayon(1);
  tr.setCenter(pt->getCenter());
  tr.setCercle(cc);
  list<Plan>::const_iterator it;
  for(it = liste.begin();it!=liste.end();it++){
    Plan pp = (*it);
    Vector3d d = pp.getDirection();
    Vector3d moyeu = tr.getMoyeu(d);
    Vector3d rayon2 = frame.getVectorInME(moyeu);
    rayon2 = Architect::antiProjection(Vector3d::UnitZ(),rayon2);
    Vector3d rayon1 = Architect::antiProjection(Vector3d::UnitZ(),transfo.getDeplacement());
    if(isEqual(rayon1.norm(),0,CRAYON) || isEqual(rayon2.norm(),0,CRAYON)){
      return valeurs;
    }
    rayon1.normalize();
    rayon2.normalize();
    Repere se3;
    Vector3d ZZ(0,0,1);
    se3.setRotation(VectRotation::getRotationOnZ(ZZ,rayon1,rayon2));
    VectorXd valX;
    if(fossile->getQValueFor(se3, valX)){
      valeurs.push_back(valX(0));
    }
  }
  return valeurs;
}
  
list<double> MyInverseGeoModel::findInTurningDroite(Liaison * fossile, DroiteTournante * dt, Repere frame,Vector3d base,Vector3d lune, bool sens){
  list<double> valeurs;
  Repere transfo;
  Corps * body = NULL;
  if(sens)
    body = fossile->getCorpsB();
  else
    body = fossile->getCorpsA();

  if(body==NULL){
    transfo = Repere();
  }
  else{
    transfo  = body->getBoutAbout();
  }
  
  if(sens==false)
    transfo = transfo.inverse();
  
  list<Droite> liste = dt->getSuitableDroites(lune);
  list<Droite>::const_iterator it;
  for(it = liste.begin();it!=liste.end();it++){
    Droite dd = (*it);
    Vector3d d = dd.getDirection();
    Vector3d moyeu = dt->getMoyeu(d);
    Vector3d rayon2 = frame.getVectorInME(moyeu);
    Vector3d rayon1 = Architect::antiProjection(Vector3d::UnitZ(),transfo.getDeplacement());
    rayon2 = Architect::antiProjection(Vector3d::UnitZ(),rayon2);
    if(isEqual(rayon1.norm(),0,CRAYON) || isEqual(rayon2.norm(),0,CRAYON)){
      return valeurs;
    }
    rayon2.normalize();
    rayon1.normalize();
    Repere se3;
    Vector3d ZZ(0,0,1);
    se3.setRotation(VectRotation::getRotationOnZ(ZZ,rayon1,rayon2));
    VectorXd valX;
    if(fossile->getQValueFor(se3, valX)){
      valeurs.push_back(valX(0));
    }
  }
  return valeurs;
}

