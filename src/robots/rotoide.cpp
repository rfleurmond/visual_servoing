#include "rotoide.h"
using namespace Eigen;
using namespace std;

Rotoide::Rotoide(string n):
  Liaison()
{

    nom = n;

    type = Liaison::ROTOIDE;

    degres = 1;

    etat = VectorXd::Zero(degres);

    initial = etat;

    minimum = -M_PI*VectorXd::Ones(degres);

    maximum = M_PI*VectorXd::Ones(degres);

  }

void Rotoide::setQ(const VectorXd & q){

    assert(q.rows() == degres);

    etat = q;

    double angle = q(0);

    Vector3d rota = angle * Vector3d::UnitZ();

    meToBodyB.setRotation(VectRotation(rota));

}

Repere Rotoide::getFromMetoBodyB(const VectorXd & q) const{

  assert(q.rows() == degres);

  double angle = q(0);

  Vector3d rota = angle * Vector3d::UnitZ();

  Repere myBaby;

  myBaby.setRotation(VectRotation(rota));

  return myBaby;

}


mouvement Rotoide::getMouvement() const{

  mouvement move;

  move.origine = Vector3d::Zero();

  move.isRotation =  true;

  move.direction = Vector3d::UnitZ();

  return move;

}

MatrixXd Rotoide::getJacobian() const{

  MatrixXd vecteur = MatrixXd::Zero(6,1);

  vecteur.block<3,1>(0,0) = Vector3d::UnitZ();

  return vecteur;

}

bool Rotoide::getQValueFor(const Repere & transfo, VectorXd & value) const{

  Vector3d dep = transfo.getDeplacement();

  Vector3d ori = transfo.getRotation().getVector();

  double val = ori.z();

  ori.z() = 0;

  if(isEqual(dep.norm(),0,CRAYON)==false){
    return false;
  }

  if(isEqual(ori.norm(),0,CRAYON)==false){
    return false;
  }

  value = VectorXd::Zero(1);

  value(0) = val;

  return true;
}


  
