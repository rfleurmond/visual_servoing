/*
 * LSystem.cpp
 *
 *  Created on: May 22, 2012
 *      @author Renliw Fleurmond
 */

#include "linear-system.h"

using namespace Eigen;

/**
 * constructeur par defaut
 * le systeme est considere comme discret
 *
 */
LSystem::LSystem() {
  // TODO Auto-generated constructor stub
  dt = 0.005;
}

LSystem::~LSystem() {
  // TODO Auto-generated destructor stub
}

/**
 * Constructeur prenant en parametre
 * l' etat initial du systeme
 *
 */
LSystem::LSystem(VectorXd XX){
  this->X = XX;
  dt = 0.005;
}

/**
 * Constructeur prenant en entree les matrice A et C du systemee
 */
LSystem::LSystem(MatrixXd a,MatrixXd c){
  assert(c.cols()==a.rows() && a.rows() == a.cols());
  A = a;
  C = c;
  dt = 0.005;
  N = A.rows();
  M = C.rows();
  U = VectorXd::Zero(1);
  B = MatrixXd::Zero(N,U.rows());
  D = MatrixXd::Zero(M,U.rows());
}

/**
 * Constructeur prenant en entree les matrice A et C du systeme
 * et l'etat initial
 */
LSystem::LSystem(MatrixXd a,MatrixXd c,VectorXd x){
  assert(x.rows()==c.cols() && a.rows() ==a.cols() && x.rows()==a.cols());
  this->A = a;
  this->C = c;
  this->X = x;
  dt = 0.005;
  N = A.rows();
  M = C.rows();
  U = VectorXd::Zero(1);
  B = MatrixXd::Zero(N,U.rows());
  D = MatrixXd::Zero(M,U.rows());
}



void LSystem::setX(VectorXd x){
  this->X = X;
}

void LSystem::setY(VectorXd YY){
  assert(YY.rows() == C.cols());
  this->Y = YY;
}


MatrixXd LSystem::getA() const{
  return A;
}

void LSystem::setA(MatrixXd AA){
  this->A = AA;
}


MatrixXd LSystem::getB() const{
  return B;
}

void LSystem::setB(MatrixXd BB){
  this->B = BB ;
}

MatrixXd LSystem::getC() const{
  return C;
}

void LSystem::setC(MatrixXd CC){
  this->C = CC;
}


MatrixXd LSystem::getD() const{
  return D;
}

void LSystem::setD(MatrixXd DD){
  this->D = DD;
}

VectorXd LSystem::getEvolution() const{
  return A * X + dt * B * U;
}


VectorXd LSystem::getObservation() const{
  return  C * X + D * U;
}

void LSystem::evoluate(){
  X = getEvolution() + X;
  Y = getObservation();
}
