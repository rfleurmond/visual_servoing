/*
 * DiscreteKalman.cpp
 *
 *  Created on: May 22, 2012
 *      @author Renliw Fleurmond
 */

#include "discrete-kalman.h"

using namespace Eigen;
using namespace std;

/**
 * Constructeur par defaut
 */

DiscreteKalman::DiscreteKalman() {
  discrimination = false;
  coefCalcul =false;
  methode = -1;	
  setNumberStates(1,1);
  methode =  DiscreteKalman::EKF;
  setUKFparameters(1,0,2);
}

DiscreteKalman::~DiscreteKalman() {
  if(coefCalcul){
    delete[] WC;
    delete[] WM;
    coefCalcul =false;
  }
}


/**
 * Permet de faire choix de la methode d'estimation
 */

void DiscreteKalman::setMethode(int met){

  switch(met){
  case DiscreteKalman::KALMAN  : methode = DiscreteKalman::KALMAN; break;
  case DiscreteKalman::EKF     : methode = DiscreteKalman::EKF; break;
  case DiscreteKalman::UKF_ADD : methode = DiscreteKalman::UKF_ADD; break;
  default                      : methode = DiscreteKalman::UKF_ADD; break;

  }
}

/**
 * Mise a jour des variances
 *
 * \a p  : variance de l'estimation de l'etat
 * \a BE : variance du bruit dans le modele d'evolution
 * \a BU : variance du bruit sur la commande
 * \a BO : variance du bruit qui s'ajoute aux mesures
 */
void DiscreteKalman::setStateVariance(MatrixXd cov){
  this->Pxx = cov;
}

void DiscreteKalman::setNoiseEvolVariance(MatrixXd cov){
  this->BE = cov;
}

void DiscreteKalman::setNoiseControlVariance(MatrixXd cov){
  this->BU = cov;
}

void DiscreteKalman::setNoiseMeasureVariance(MatrixXd cov){
  this->BO = cov;
}

/**
 * Retourne la variance d'estimation de l'etat
 */
MatrixXd DiscreteKalman::getPxx() const {
  return Pxx;
}

/**
 * Retourne la variance de l'estimation de la mesure par rapport a l'etat
 *
 */
MatrixXd DiscreteKalman::getPxy() const {
  return Pxy;
}

/**
 * Retourne la variance de l'estimation des mesures
 */
MatrixXd DiscreteKalman::getPyy() const {
  return Pyy;
}
/**
 * Met a jour le nombre de variables d'etat et la dimenstion des mesures;
 */
void DiscreteKalman::setNumberStates(int nn,int mm){

  this->n = nn;
  this->m = mm;

  X = VectorXd::Zero(n);

  /*YE = EY = VectorXd::Zero(m);
	
    Pxx = MatrixXd::Zero(n,n);
    Pxy = MatrixXd::Zero(n,m);
    Pyy = MatrixXd::Zero(m,m);
    K   = MatrixXd::Zero(n,m);
	
    selection = MatrixXd::Identity(n,n);;
	
    BE = MatrixXd::Zero(n,n);
    BO = MatrixXd::Zero(m,m);

    A = MatrixXd::Zero(n,n);
    C = MatrixXd::Zero(m,n);;
	
  */	

  if(this->methode == DiscreteKalman::UKF_ADD)
    calculUKFParametres();

}
/**
 * Met a jour les parametres empiriques de l' UKF
 * pour une distribution gaussienne
 *
 * alpha = 1
 * beta  = 0
 * kappa = 2
 */

void DiscreteKalman::calculUKFParametres(){

  cout <<" N      = "<<n<<endl;
  cout <<" M      = "<<m<<endl;
  cout <<" alpha  = "<<alpha<<endl;
  cout <<" beta   = "<<beta<<endl;
  cout <<" kappa  = "<<kappa<<endl;
	

  if(n>0){
    double lambda =0;
    lambda = pow(alpha,2)*(n+kappa)-n;

    gamma = sqrt(n+lambda);
		
    if(coefCalcul){

      delete[] WC;
      delete[] WM;
      coefCalcul =false;
    }

    WC =  new double[2*n+1];
    WM =  new double[2*n+1];

    coefCalcul = true;

    WC[0] = lambda/(n+lambda);
    WM[0] = lambda/(n+lambda) + 1- pow(alpha,2) + beta;

    for(int i=1;i<2*n+1;i++){
      WC[i] = 0.5/(n + lambda);
      WM[i] = 0.5/(n + lambda);
    }
  }




}
void DiscreteKalman::setUKFparameters(double a,double b,double k){
  this->alpha = a;
  this->beta  = b;
  this->kappa = k;
  this->gamma = 0;

  calculUKFParametres();

}
/**
 * Calcule l'estimation de l'etat du systeme
 */
void DiscreteKalman::computeEstimation(const LSystem & lineaireS,VectorXd Y){

  n = lineaireS.getNStates();
  this->computeKalmanEstimation(lineaireS,Y);

}

/**
 * Calcule l'estimation de l'etat du systeme
 */
void DiscreteKalman::computeEstimation(const NLSystem & nonLinear, VectorXd Y){

  switch(methode){
  case DiscreteKalman::KALMAN : this->computeEKF_Estimation(nonLinear, Y);		break;
  case DiscreteKalman::EKF    : this->computeEKF_Estimation(nonLinear, Y);        break;
  case DiscreteKalman::UKF_ADD: this->computeUKFAdditiveEstimation(nonLinear, Y);	break;
  default                     : this->computeUKFAdditiveEstimation(nonLinear, Y);	break;
  }

}

/**
 * Calcule l'estimation de l'etat du systeme filtre de Kalman pour systeme lineaire
 */
void DiscreteKalman::computeKalmanEstimation(const LSystem & lineaireS,VectorXd Y){

  X = lineaireS.getX();
  A = lineaireS.getA();
  C = lineaireS.getC();
  D = lineaireS.getD();

  X = lineaireS.getEvolution();

  YE = lineaireS.getObservation();

  //cout << "Pxx = A*Pxx*A.transpose() + BE"<< endl;
  Pxx = A*Pxx*A.transpose() + B * BU * B.transpose() + BE;
  //cout<<Pxx<<endl;

  //cout << "Pxy = C* Pxx*C.transpose() + BO;"<< endl;
  Pyy = C*Pxx*C.transpose() + D * BU * D.transpose() + BO;
  //cout << Pxy <<endl;

  //cout<<"MatrixXd tampon = Pxx* C.transpose();"<<endl;
  Pxy = Pxx* C.transpose();
  //cout << tampon <<endl;

  EY = Y - YE;

  correction(false,EY);

  //cout<<"Pxx = (MatrixXd::Identity(n,n)- K*C)*Pxx;"<<endl;
  Pxx = (MatrixXd::Identity(n,n)- K*C)*Pxx;
  //cout << Pxx << endl;

}

/**
 * Calcule l'estimation de l'etat du systeme EKF
 */
void DiscreteKalman::computeEKF_Estimation(const NLSystem & nonLinear, VectorXd Y){

  //cout <<"Je suis dans EKf Estimation"<< endl;

  X = nonLinear.getEvolution();

  U = nonLinear.getU();

  A = nonLinear.getJacobianEvolutionX(X,U);

  B = nonLinear.getJacobianEvolutionU(X,U);

  C = nonLinear.getJacobianObservationX(X,U);

  D = nonLinear.getJacobianObservationU(X,U);

  YE = nonLinear.getObservation(X,U);

  EY = Y - YE;

  

  //cout << "Pxx = A*Pxx*A.transpose() + BE"<< endl;
  Pxx = A*Pxx*A.transpose() + B * BU * B.transpose() + BE;
  //cout<<Pxx<<endl;
 
  //cout << "Pxy = C* Pxx*C.transpose() + BO;"<< endl;
  Pyy = C*Pxx*C.transpose() + D * BU * D.transpose() + BO;
  //cout << Pxy <<endl;

  //cout<<"MatrixXd tampon = Pxx* C.transpose();"<<endl;
  Pxy = Pxx* C.transpose();
  //cout << tampon <<endl;

  if(nonLinear.hasNonTrivialError()){

    EY = -nonLinear.getError(X,U,Y);

    correction(discrimination,EY,nonLinear.getJacobianError(X,U,Y));
    
  }

  else{ 

    correction(discrimination,EY);

  }

  //cout<<"Pxx = (MatrixXd::Identity(n,n)- K*C)*Pxx;"<<endl;
  Pxx = (MatrixXd::Identity(n,n)- K*C)*Pxx;
  //cout << Pxx << endl;

}


/**
 * Calcule l'estimation de l'etat du systeme UKF
 */
void DiscreteKalman::computeUKFAdditiveEstimation(const NLSystem & nonLinear,VectorXd Y){

  X = nonLinear.getX();

  U = nonLinear.getU();

  B = nonLinear.getJacobianEvolutionU(X,U);

  D = nonLinear.getJacobianObservationU(X,U);

  //cout<< "Covariance Matrix\n" << Pxx <<"\n\n"<<endl;

  //cout<<"MatrixXd racine = Pxx.llt().matrixL();"<<endl;
  MatrixXd racine = Pxx.llt().matrixL();

  //cout<< "Choleksy Root\n" << racine<<"\n\n"<<endl;

  MatrixXd chi(n,2*n+1);

  chi.col(0) = X;

  for(int i =1; i<=n; i++){
    //cout<<"chi.col(i) = X + gamma*racine.col(i);\nchi.col(i+n) = X - gamma*racine.col(i);"<<endl;
    chi.col(i) = X + gamma*racine.col(i-1);
    chi.col(i+n) = X - gamma*racine.col(i-1);
  }
  //cout<<"CHI = \n" << chi <<endl;

  MatrixXd XX(n,2*n+1);

  for(int i =0; i< 2*n+1;i++){
    XX.col(i) = nonLinear.getEvolution(chi.col(i),U);

  }
  //cout<<"XX=\n"<<XX<<endl;

  VectorXd Xmoins = VectorXd::Zero(n);

  VectorXd inutile;

  for(int i =0; i < 2*n+1;i++){
    inutile = XX.col(i);
    Xmoins = Xmoins + WM[i]*inutile;

  }
  //cout <<"Xmoins =\n"<<Xmoins<<endl;

  //cout << "MatrixXd Pmoins = MatrixXd::Zero(n,n);"<<endl;
  MatrixXd Pmoins = MatrixXd::Zero(n,n);
  //cout<<Pmoins<<endl;

  VectorXd colonne1;

  for(int i =0;i<2*n+1;i++){
    colonne1 = XX.col(i)-Xmoins;
    //cout<<"Pmoins = Pmoins + WC[i]*(colonne1*colonne1.transpose());"<<endl;
    Pmoins = Pmoins + WC[i]*(colonne1*colonne1.transpose());
  }
  //cout<<"Pmoins+=BE;"<<endl;
  Pmoins = Pmoins + B * BU * B.transpose() + BE;
  //cout<<Pmoins<<endl;

  //cout<<"racine = Pmoins.llt().matrixL();"<<endl;
  racine = Pmoins.llt().matrixL();
  //cout <<racine<<endl;

  MatrixXd chi2(n,2*n+1);

  chi2.col(0) = Xmoins;

  for(int i =1; i<=n; i++){
    //cout<<"chi2.col(i)   = Xmoins + gamma*racine.col(i);"<<endl;
    chi2.col(i)   = Xmoins + gamma*racine.col(i-1);
    chi2.col(i+n) = Xmoins - gamma*racine.col(i-1);
  }
  //cout <<chi2<<endl;

  MatrixXd YY(m,2*n+1);

  for(int i =0; i< 2*n+1;i++){
    //cout<<"YY.col(i) = nonLinear.getObservation(chi2.col(i));"<<endl;
    YY.col(i) = nonLinear.getObservation(chi2.col(i),U);
  }
  //cout <<YY<<endl;

  VectorXd Ymoins = VectorXd::Zero(m);

  for(int i =0; i < 2*n+1;i++){
    //cout<<"Ymoins += WM[i]*YY.col(i);"<<endl;
    Ymoins = Ymoins + WM[i]*YY.col(i);
  }
  //cout<<"Ymoins = \n"<<Ymoins<<endl;
  YE = Ymoins;

  Pyy = MatrixXd::Zero(m,m);

  VectorXd colonne2;

  for(int i =0;i<2*n+1;i++){
    //cout<<"colonne2 = YY.col(i)-Ymoins;"<<endl;
    colonne2 = YY.col(i)-Ymoins;
    //cout<<"Pyy += WC[i]*(colonne2*colonne2.transpose());"<<endl;
    Pyy = Pyy + WC[i]*(colonne2*colonne2.transpose());
  }
  //cout<<"Pyy+=BO;"<<endl;
  Pyy = Pyy + D * BU * D.transpose()  + BO;
  //cout<<Pyy<<endl;

  Pxy = MatrixXd::Zero(n,m);

  for(int i =0;i<2*n+1;i++){
    //<<"colonne1 = XX.col(i)-Xmoins;"<<endl;
    colonne1 = XX.col(i)-Xmoins;
    //cout<<"colonne2 = YY.col(i)-Ymoins;"<<endl;
    colonne2 = YY.col(i)-Ymoins;
    //cout<<"Pxy += WC[i]*colonne1*colonne2.transpose();"<<endl;
    Pxy =Pxy + WC[i]*(colonne1*colonne2.transpose());
  }

  //cout<<"Pxy = \n"<<Pxy<<endl;
  X = Xmoins;

  EY = Y - YE;

  if(nonLinear.hasNonTrivialError()){

    EY = -nonLinear.getError(X,U,Y);

    correction(discrimination,EY,nonLinear.getJacobianError(X,U,Y));
    
  }

  else{ 

    correction(discrimination,EY);

  }
  
  YE = nonLinear.getObservation(X,U);

  //cout<<Y<<endl;

  //cout<<"Pxx = Pmoins - K*Pyy*K.transpose();"<<endl;
  Pxx = Pmoins - K*Pyy*K.transpose();
  //cout<<Pxx<<endl;
}


void DiscreteKalman::correction(bool acception, VectorXd ey){
  //cout<<"K = tampon*Pxy.fullPivHouseholderQr().inverse();"<<endl;
  
  K = Pyy.transpose().fullPivLu().solve(Pxy.transpose()).transpose();
  
  //cout<<"X = X + K*(Y- YE);"<<endl;
  if(acception){
    X = X + selection * K * ey;

  }
  else{
    X = X + K * ey;
  }

  //cout << X << endl;
}

void DiscreteKalman::correction(bool acception, VectorXd ey, MatrixXd j){
  //cout<<"K = tampon*Pxy.fullPivHouseholderQr().inverse();"<<endl;
 
  K = Pyy.transpose().fullPivLu().solve(Pxy.transpose()).transpose();
  
  //K = Pyy.transpose().ldlt().solve(Pxy.transpose()).transpose();
  
  //cout<<"X = X + K*(Y- YE);"<<endl;
  if(acception){

    X = X + selection * K * j.transpose() * ey;

  }
  else{
    X = X + K * j.transpose() * ey;
  }

  //cout << X << endl;
}

void DiscreteKalman::setSelection(MatrixXd elus){
  assert(elus.cols() == elus.rows());
  assert(elus.cols() == n);

  this->selection = elus;

}

void DiscreteKalman::enableDiscrimination(bool choix){

  this->discrimination = choix;
}


/**
 * Retourne l' estimation de l'etat
 */
VectorXd DiscreteKalman::getStateEstimation() const{

  return X;

}

/**
 * Retourne l'estimation de la mesure
 */
VectorXd DiscreteKalman::getObservEstimation() const{

  return YE;

}

/**
 * Met a jour le systeme avec l'etat estime precedemment
 */
void DiscreteKalman::updateSystem(Systeme & sys){

  sys.setX(X);
}


VectorXd DiscreteKalman::getErrorMeasures() const{

  return EY;

}
