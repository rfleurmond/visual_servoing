
/*
 * Systeme.cpp
 *
 *  Created on: May 22, 2012
 *      @author Renliw Fleurmond
 */

#include "systeme.h"

using namespace Eigen;

Systeme::Systeme(){
  // TODO Auto-generated constructor stub
  N = 1;
  M = 1;

}

Systeme::Systeme(VectorXd XX){
  setX(XX);
  dt = 0.005;
}

Systeme::~Systeme(){
  // TODO Auto-generated destructor stub
}


VectorXd Systeme::getX() const{
  return X;
}


VectorXd Systeme::getY() const{
  return Y;
}

void Systeme::setX(VectorXd XX){
  this->X = XX;
  N = static_cast<int>(XX.rows()); 
}
void Systeme::setY(VectorXd Y){
  this->Y = Y;
  M = static_cast<int>(Y.rows());
}

double Systeme::getDt() const{
  return dt;
}

void Systeme::setDt (double t){
  this->dt = t;
}

int Systeme::getNStates() const{
  return N;
}

int Systeme::getMStates() const{
  return M;
}

VectorXd Systeme::getEvolution() const{
  return X;
}


VectorXd Systeme::getObservation() const{
  return Y;
}

void Systeme::evoluate(){

}
