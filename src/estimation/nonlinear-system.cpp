/*
 * NLSystem.cpp
 *
 *  Created on: May 22, 2012
 *      @author Renliw Fleurmond
 */

#include "nonlinear-system.h"

using namespace Eigen;

NLSystem::NLSystem() {
}

NLSystem::NLSystem(VectorXd XX) {
  this->X = XX;
}

NLSystem::~NLSystem() {
  // TODO Auto-generated destructor stub
}

/**
 * Modifier etat systeme
 */

void NLSystem::setU(VectorXd u){
  this->U = u;
  nO = u.rows();
}

VectorXd NLSystem::getU() const{
  return U;
}

/**
 * Cette methode retourne l' evolution du systeme et est destinee a etre surchargee
 */
VectorXd NLSystem::getEvolution(VectorXd XX, VectorXd uu) const{
  return X;
}

/**
 * Cette methode retourne l' observation du systeme et est destinee a etre surchargee
 */

VectorXd NLSystem::getObservation(VectorXd XX, VectorXd uu) const{

  return Y;
}

/**
 * Cette methode retourne la jacobienne de la fonction d'observation du systeme et est destinee a etre surchargee
 */
MatrixXd NLSystem::getJacobianObservationX(VectorXd XX, VectorXd uu) const{

  return MatrixXd::Identity(M,N);

}

MatrixXd NLSystem::getJacobianObservationU(VectorXd XX, VectorXd uu) const{

  return MatrixXd::Zero(M,uu.rows());

}

/**
 * Cette methode retourne la jacobienne de la fonction d'evolution du systeme et est destinee a etre surchargee
 */
MatrixXd NLSystem::getJacobianEvolutionX(VectorXd XX, VectorXd u) const{

  return MatrixXd::Identity(N,N);

}

MatrixXd NLSystem::getJacobianEvolutionU(VectorXd XX, VectorXd u) const{

  return MatrixXd::Zero(N,u.rows());

}


VectorXd NLSystem::getEvolution() const {

  return this->getEvolution(X,U);
}


VectorXd NLSystem::getObservation() const{

  return this->getObservation(X,U);
}

MatrixXd NLSystem::getJacobianEvolutionX() const{

  return this->getJacobianEvolutionX(X,U);

}

MatrixXd NLSystem::getJacobianEvolutionU() const{

  return this->getJacobianEvolutionU(X,U);

}

MatrixXd NLSystem::getJacobianObservationX() const{

  return this->getJacobianObservationX(X,U);

}

MatrixXd NLSystem::getJacobianObservationU() const{

  return this->getJacobianObservationU(X,U);

}

void NLSystem::evoluate(){
  
  this->setX(this->getEvolution());
 
}

bool NLSystem::hasNonTrivialError() const {
  return false;
}

VectorXd NLSystem::getError(VectorXd XX, VectorXd UU, VectorXd YY) const{
  return getObservation()-YY;
}

MatrixXd NLSystem::getJacobianError(VectorXd XX, VectorXd UU, VectorXd YY) const{
  int n = getMStates();
  return MatrixXd::Identity(n,n);
}

