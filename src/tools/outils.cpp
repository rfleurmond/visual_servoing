/*
 * outils.cpp
 *
 *  Created on: May 21, 2012
 *      @author Renliw Fleurmond
 */

#include "outils.h"
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <time.h>
#include <limits>


using namespace std;
using namespace Eigen;

typedef std::numeric_limits< double > dbl;

bool isEqual(double test, double val, double precision){
  assert(precision > 1e-30);
  return ((test<=val+precision) && (test>=val-precision));
}

double hasard(double a,double b){
  return ( rand()/static_cast<double>(RAND_MAX) ) * (b-a) + a;
}

vector<double *> loadDAT(string path,int cols){

  vector <double * > data;

  ifstream fichier(path.c_str(), ios::in);

  if(fichier){
    int i=0;
    double * instant = new double[cols];
    while(fichier.eof()==false){
      if(i==cols){
	data.push_back(instant);
	instant = new double[cols];
	i=0;
      }
      fichier >> instant[i];
      i++;
    }
    if(i!=cols){
      //cerr <<"\nAjustement de "<<i<<" double(s) donc une taille de "<<i*sizeof(double)<<" bytes"<<endl;
      //data.pop_back();
      delete[] instant;
    }
    fichier.close();
  }
  else
    cerr << "Impossible d'ouvrir le fichier \""<<path<<"\"!" << endl;
  return data;
}


void freeDATA(vector<double *> & data){
	
  vector<double * >::iterator iterateur;

  double * instant = NULL;

  for(iterateur = data.begin();iterateur!=data.end();iterateur++){

    instant = *iterateur;

    delete[] instant;

  }
}

bool writeDATA(string path, const vector<double *> & data, int cols){

  ofstream fichier(path.c_str());

  fichier.precision(dbl::digits10);

  if(fichier){

    vector<double * >::const_iterator iterateur = data.begin();

    double * instant = NULL;

    for(iterateur = data.begin();iterateur!=data.end();iterateur++){

      instant = *iterateur;

      for(int i=0;i<cols;i++){
	if(i==0){
	  fichier << fixed << instant[i];
	}
	else{
	  fichier <<"   "<< fixed << instant[i];
	}
      }

      fichier <<""<<endl;

    }
    

    return true;
  }

  return false;

}


bool writeVectors(string path, const vector<VectorXd> & data){

  vector<double *> produit;

  vector<VectorXd>::const_iterator iterateur= data.begin();

  VectorXd X = *iterateur;

  int cols = static_cast<int>(X.rows());

  double * instant = NULL;

  for(iterateur = data.begin();iterateur!=data.end();iterateur++){

    X = * iterateur;
    instant = new double[cols];

    for(int i =0;i< cols;i++){
      instant[i] = X(i);
    }
    produit.push_back(instant);
  }

  bool answer = writeDATA(path,produit,cols);

  freeDATA(produit);
	
  return answer;

}


bool writeVectors(string path, const vector<double> & temps, const vector<VectorXd> & data){

  vector<double *> produit;

  vector<VectorXd>::const_iterator iterateur= data.begin();

  vector<double>::const_iterator iterateurT= temps.begin();

  VectorXd X = *iterateur;

  int cols = static_cast<int>(X.rows());

  double * instant = NULL;

  for(iterateur = data.begin();iterateur!=data.end();iterateur++){

    X = * iterateur;
    //cout <<"X = \n"<< X <<endl;
    instant = new double[cols+1];
    instant[0] = *iterateurT;
    iterateurT++;
    for(int i =0;i<cols;i++){
      instant[i+1] = X(i);
    }
    produit.push_back(instant);
  }

  bool answer = writeDATA(path,produit,cols+1);

  freeDATA(produit);
	
  return answer;

}


VectorXd pinvSolve(const MatrixXd & A, const VectorXd & Y){

  VectorXd  X;
  
  X = A.jacobiSvd(ComputeThinU | ComputeThinV).solve(Y);

  return X;
  
}


///Took from this page:
///http://listengine.tuxfamily.org/lists.tuxfamily.org/eigen/2010/01/msg00173.html
bool pinv(const MatrixXd &a, MatrixXd &a_pinv)
{
  MatrixXd b = a;
  // see : http://en.wikipedia.org/wiki/Moore-Penrose_pseudoinverse#The_general_case_and_the_SVD_method

  /*
    if (a.rows()<a.cols()){
    // Here is  my modification
    MatrixXd aplus;
    //cout<<"Transpose de pinv"<<endl;
    pinv(a.transpose(),aplus);
    a_pinv = aplus.transpose();
    return true;
    }
    else*/
  {

    // SVD
    JacobiSVD<MatrixXd> svdA(b,ComputeThinU | ComputeThinV);

    MatrixXd vSingular = svdA.singularValues();

    // Build a diagonal matrix with the Inverted Singular values
    // The pseudo inverted singular matrix is easy to compute :
    // is formed by replacing every nonzero entry by its reciprocal (inversing).
    VectorXd vPseudoInvertedSingular(svdA.matrixV().cols(),1);
        
    for (int iRow =0; iRow<vSingular.rows(); iRow++)     {
      if ( fabs(vSingular(iRow))<=1e-6 ){ // Todo : Put epsilon in parameter
	vPseudoInvertedSingular(iRow,0)= 0.;
      }
      else{
	vPseudoInvertedSingular(iRow,0)=1./vSingular(iRow);
      }
    }

    // A little optimization here 
    MatrixXd mAdjointU = svdA.matrixU().adjoint().block(0,0,vSingular.rows(),svdA.matrixU().adjoint().cols());

    // Pseudo-Inversion : V * S * U'
    a_pinv.noalias() = (svdA.matrixV() *  vPseudoInvertedSingular.asDiagonal()) * mAdjointU  ;


    return true;
  }
}


