#include "sigmoid.h"


Sigmoid::Sigmoid(double b):
  beta(b)
{
  assert(b>0);
}

double Sigmoid::operator()(double x){
  return value(x);
}

double Sigmoid::value(double x) const{
  double f = 0;
  if( x <= 0){
    f = 0;
  }
  if( 0 < x && x < beta){
    f = 0.5 * ( 1 + tanh( 1/(1-x/beta) -beta/x)); 
  }
  if( beta <= x ){
    f = 1;
  }
  return f;
}
  
void Sigmoid::setMax( double b){
  assert(b>0);
  beta = b;
}
