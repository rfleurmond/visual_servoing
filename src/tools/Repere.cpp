/*
 * Repere.cpp
 *
 *  Created on: Jun 6, 2012
 *      @author Renliw Fleurmond
 */

#include "Repere.h"

using namespace Eigen;
using namespace std;

Repere::Repere():
  attitude(),
  position(Vector3d::Zero())
{

}


Repere::Repere(MatrixXd mat):
  attitude(),
  position(Vector3d::Zero())
{

  assert(mat.cols() == mat.rows());

  assert(mat.cols()==3 || mat.cols()==4);

  long int n = mat.cols();

  Matrix3d rot = mat.block<3,3>(0,0);

  if(n==3){
    position = Vector3d::Zero();
    attitude = VectRotation(rot);
  }
  else{
    position = mat.block<3,1>(0,3);
    attitude = VectRotation(rot);
  }

}

Repere::Repere(VectRotation att):
  attitude(att),
  position(Vector3d::Zero())
{

}


Repere::Repere(VectRotation att,Vector3d pos):
  attitude(att),
  position(pos)
{

}

Repere::Repere(VectorXd X):
  attitude(),
  position(Vector3d::Zero())
{

  setParameters(X);

}


Repere Repere::getFusionWithParent(const Repere & parent) const{

  Vector3d newV = parent.getVectorInParent(attitude.getVector());

  VectRotation rr = VectRotation(newV)*parent.attitude;

  Vector3d dep = parent.getVectorInParent(position)+parent.getDeplacement();

  Repere rep(rr,dep);

  return rep;

}




VectorXd Repere::getParameters() const{

  VectorXd X(6);
  X.block<3,1>(0,0) = attitude.getVector();
  X.block<3,1>(3,0) = position;

  return X;

}


void Repere::setParameters(const VectorXd & X){

  assert(X.rows()==6);

  attitude = VectRotation(static_cast<Vector3d>(X.block<3,1>(0,0)));
  position = X.block<3,1>(3,0);
}


MatrixXd Repere::getHomogenMatrix() const{

  MatrixXd matrice(4,4);

  matrice.block<3,3>(0,0) = attitude.getMatrix();

  matrice.block<3,1>(0,3) = position;

  matrice.block<1,3>(3,0) = RowVector3d::Zero();

  matrice(3,3) = 1;

  return matrice;

}


VectorXd Repere::getPointInME(const Vector3d & v) const{

  return attitude.deRotate(v-position);

}


VectorXd Repere::getVectorInME(const Vector3d & v) const{

  return attitude.deRotate(v);

}


VectorXd Repere::getPointInParent(const Vector3d & v) const{

  return attitude.rotate(v)+position;

}


VectorXd Repere::getVectorInParent(const Vector3d & v) const{

  return attitude.rotate(v);

}


MatrixXd Repere::getJacobianPointInME(const Vector3d & v) const{

  MatrixXd J(3,9);

  J.block<3,3>(0,0) = attitude.getJacobianRotation(v-position);
  J.block<3,3>(0,3) = -attitude.getMatrix();
  J.block<3,3>(0,6) = -J.block<3,3>(0,3);


  return J;

}



MatrixXd Repere::getJacobianVectorInME(const Vector3d & v) const{

  MatrixXd J(3,9);

  J.block<3,3>(0,0) = attitude.getJacobianRotation(v);
  J.block<3,3>(0,3) = MatrixXd::Zero(3,3);
  J.block<3,3>(0,6) = attitude.getMatrix();

  return J;

}



MatrixXd Repere::getJacobianPointInParent(const Vector3d & v) const{

  MatrixXd J(3,9);

  VectRotation edutitta = -1*attitude;

  J.block<3,3>(0,0) = -edutitta.getJacobianRotation(v);
  J.block<3,3>(0,3) = MatrixXd::Identity(3,3);
  J.block<3,3>(0,6) = edutitta.getMatrix();

  return J;

}



MatrixXd Repere::getJacobianVectorInParent(const Vector3d & v) const{

  MatrixXd J(3,9);

  VectRotation edutitta = -1*attitude;

  J.block<3,3>(0,0) = -edutitta.getJacobianRotation(v);
  J.block<3,3>(0,3) = MatrixXd::Zero(3,3);
  J.block<3,3>(0,6) = edutitta.getMatrix();

  return J;

}


VectRotation Repere::getRotation() const{

  return attitude;

}


void Repere::setRotation(const VectRotation & att){

  attitude = att;

}


Vector3d Repere::getDeplacement() const{

  return position;

}


void Repere::setDeplacement(const Vector3d & pos){

  position = pos;
}


Repere Repere::inverse() const {

  Vector3d retour = -1* position;

  Vector3d demiTour = -1 * attitude.getVector();

  retour = getVectorInME(retour);

  demiTour = getVectorInME(demiTour);

  Repere oppose(VectRotation(demiTour),retour);

  return oppose;

}

Vector3d Repere::getX() const{
  return static_cast<Vector3d>(attitude.getMatrix().col(0));
}

Vector3d Repere::getY() const{
  return static_cast<Vector3d>(attitude.getMatrix().col(1));
}

Vector3d Repere::getZ() const{
  return static_cast<Vector3d>(attitude.getMatrix().col(2));
}


MatrixXd Repere::getKineticMatrix() const{
  MatrixXd kine = MatrixXd::Zero(6,6);
  MatrixXd ROT = attitude.getMatrix();
  MatrixXd TT = attitude.getSkewMatrix(position);
  kine.block<3,3>(0,0) = ROT;
  kine.block<3,3>(0,3).noalias() = TT*ROT;
  kine.block<3,3>(3,3) = ROT;
  return kine;
}

MatrixXd Repere::getInversKineticMatrix() const{
  MatrixXd kine = MatrixXd::Zero(6,6);
  MatrixXd ROT = attitude.getMatrix().transpose();
  MatrixXd TT = -attitude.getSkewMatrix(position);
  kine.block<3,3>(0,0) = ROT;
  kine.block<3,3>(0,3).noalias() = ROT*TT;
  kine.block<3,3>(3,3) = ROT;
  return kine;
}


Repere operator*(const Repere &a, const Repere &b){

  Repere total = b.getFusionWithParent(a);

  return total;

}

Repere operator/(const Repere &a, const Repere &b){

  Repere total = a * b.inverse();

  return total;

}

ostream & operator<<(ostream &flux,Repere const  & se3){

  flux <<"SE3:\n"<<se3.getRotation()<<"\n("<<se3.getDeplacement().transpose()<<")\n"<< se3.getHomogenMatrix()<<endl;

  return flux;

}

