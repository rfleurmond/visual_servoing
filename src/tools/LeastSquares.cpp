/*
 * LeastSquares.cpp
 *
 *  Created on: Jul 17, 2012
 *      @author Renliw Fleurmond
 */
#include <iostream>
#include "LeastSquares.h"

using namespace std;
using namespace Eigen;

const double LeastSquares::defaultMinPas	        = 1e-30;
const double LeastSquares::defaultMinError		= 1e-60;
const int LeastSquares::defaultMaxIterations 	        = 100;
const int LeastSquares::maxDivergences		   	= 2;


LeastSquares::LeastSquares():
  activeMinPas(LeastSquares::defaultMinPas),
  activeMinError(LeastSquares::defaultMinError),
  maxIterations(LeastSquares::defaultMaxIterations)
{

}

void LeastSquares::setMinPas(double m){
  activeMinPas = m;
}

void LeastSquares::setMinError(double m){
  activeMinError = m;
}

void LeastSquares::setMaxIterations(int i){
  maxIterations = i;
}

double LeastSquares::getMinPas(){
  return activeMinPas;
}

double LeastSquares::getMinError(){
  return activeMinError;
}

int LeastSquares::getMaxIterations(){
  return maxIterations;
}


double LeastSquares::solve(const VectorXd & Y, const MatrixXd & A, VectorXd & solution){

  solution = A.fullPivLu().solve(Y);

  VectorXd YY(solution);
  YY.noalias() = A * solution;
  VectorXd E(YY-Y);
  
  double erreur = E.norm();

  //cout <<"Solution desiree  = "<<Y.block<4,1>(0,0).transpose()<<endl;
  //cout <<"Solution obtenue  = "<<YY.block<4,1>(0,0).transpose()<<"\n"<<endl;
  return erreur;

}

double LeastSquares::solvePseudoInverse(const VectorXd & Y, const MatrixXd & A, VectorXd & solution){

  solution = A.jacobiSvd(ComputeThinU | ComputeThinV).solve(Y);

  VectorXd YY(solution);
  YY.noalias() = A * solution;
  VectorXd E(YY-Y);
  
  double erreur = E.norm();

  //cout <<"solution desiree  = "<<Y.block<4,1>(0,0).transpose()<<endl;
  //cout <<"solution obtenue  = "<<YY.block<4,1>(0,0).transpose()<<"\n"<<endl;
    
  if(erreur>1e-2){
    return solve(Y,A,solution);
    
  }
  
  return erreur;

}

double LeastSquares::solveNewton(const VectorXd & X0,Fonction F,Jacobien J,VectorXd & solution) const{

  double erreur = 1e+9;

  double pas =1;

  VectorXd X = X0;

  VectorXd bestX = X;

  VectorXd DX = VectorXd::Zero(X0.rows());

  VectorXd E = F(X);

  erreur = E.norm();

  double leastSquare = erreur;

  double lastError =  erreur+1;

  MatrixXd jacobienne;

  int iterations =0;

  if(isEqual(erreur,0,1e-6)){

    solution = X;

    //cout <<"La valeur initiale est une solution"<<endl;

    return erreur;

  }

  while((lastError - erreur) >= this->activeMinError){

    jacobienne = J(X);

    DX = jacobienne.fullPivHouseholderQr().solve(E);


    pas = DX.norm();

    if(pas<this->activeMinPas){
      //cout <<"Variation de la solution inferieure au pas de discretisation defini"<<endl;
      break;
    }

    X = X - DX;

    E = F(X);

    lastError = erreur;

    erreur = E.norm();

    iterations++;

    if(iterations>=this->maxIterations){
      //cout <<"Depassement du nombre d'iterations maximal defini"<<endl;
      break;
    }

    if(erreur<leastSquare){
      leastSquare = erreur;
      bestX = X;
      if(isEqual(erreur,0,1e-6)){
	//cout<<" Solution trouvee!"<<endl;
	break;
      }
    }
    else if(erreur> leastSquare){
      //cerr<<"Divergence possible methode de newton!"<<endl;
      break;
    }
    else{
      //cerr<<"Avertissement stagnation!"<<endl;
    }

  }

  if((lastError - erreur)<this->activeMinError){
    //cout << "la variation de l'erreur de la fonction est inferieure au seuil fixe"<<endl;
  }

  solution = bestX;

  ////cout <<"nombre d'iterations:"<<iterations<<endl;

  return leastSquare;

}

double LeastSquares::solveGaussNewton(const VectorXd & X0,Fonction F,Jacobien J,VectorXd & solution) const{

  double erreur = 1e+9;

  double pas =1;

  VectorXd X = X0;

  VectorXd bestX =X;

  VectorXd DX = VectorXd::Zero(X0.rows());

  VectorXd E = F(X);

  erreur = E.norm();

  double leastSquare = erreur;

  double lastError = erreur+1;

  bool temoin =false;

  VectorXd B;

  MatrixXd hessienne;

  MatrixXd jacobienne;

  int iterations =0;

  if(isEqual(erreur,0,1e-6)){

    solution = X;

    //cout<<"La valeur initiale est une solution"<<endl;

    return erreur;

  }

  while((lastError - erreur) >= this->activeMinError){

    jacobienne = J(X);

    B = jacobienne.transpose()*E;

    hessienne = jacobienne.transpose()*jacobienne;

    DX = hessienne.fullPivHouseholderQr().solve(B);

    pas = DX.norm();

    if(pas<this->activeMinPas){
      //cout<<"Variation de la solution inferieure au pas de discretisation defini"<<endl;
      break;
    }

    X = X - DX;

    E = F(X);

    lastError = erreur;

    erreur = E.norm();

    iterations++;

    if(iterations>=this->maxIterations){
      //cout <<"Depassement du nombre d'iterations maximal defini"<<endl;
      break;
    }

    if(isEqual(erreur,0,1e-6)){
      ////cout<<" Solution trouvee!"<<endl;
      leastSquare = erreur;
      bestX = X;
      break;
    }


    if(erreur > leastSquare){

      if(temoin){
	//cerr<<"Divergence confirmee!"<<endl;
	break;
      }
      else{
	//cerr<<"Divergence possible!"<<endl;
	X = X+0.8*DX;
	E = F(X);
	erreur = E.norm();
	temoin=true;
	continue;
      }

    }

    if(isEqual(erreur,0,1e-6)){
      ////cout<<" Solution trouvee!"<<endl;
      leastSquare = erreur;
      bestX = X;
      break;
    }

    if(erreur<leastSquare){
      temoin = false;
      leastSquare = erreur;
      bestX = X;

    }

    if(isEqual(erreur,leastSquare,1e-6)){
      temoin = false;
      //cerr<<" stagnation!"<<endl;
    }

  }

  if((lastError - erreur)<this->activeMinError){
    //cout << "la variation de l'erreur de la fonction est inferieure au seuil fixe"<<endl;
  }

  solution = bestX;

  ////cout <<"nombre d'iterations:"<<iterations<<endl;

  return leastSquare;

}



double LeastSquares::solveNewton(const VectorXd & X0, Cout & cost,VectorXd & solution) const{

  double erreur = 1e+9;

  double pas = 1;

  VectorXd X = X0;

  VectorXd bestX = X;

  VectorXd DX = VectorXd::Zero(X0.rows());

  VectorXd E = cost.fonctionObjectif(X);

  erreur = E.norm();

  double leastSquare = erreur;

  double lastError =  erreur+1;

  MatrixXd jacobienne;

  int iterations =0;

  if(isEqual(erreur,0,1e-6)){

    solution = X;

    //cout<<"La valeur initiale est une solution"<<endl;

    return erreur;

  }

  while((lastError - erreur) >= this->activeMinError){

    jacobienne = cost.jacobienObjectif(X);

    DX = jacobienne.fullPivHouseholderQr().solve(E);

    pas = DX.norm();

    if(pas<this->activeMinPas){
      //cout<<"Variation de la solution inferieure au pas de discretisation defini"<<endl;
      break;
    }

    X = X - DX;

    E = cost.fonctionObjectif(X);
	  
    lastError = erreur;

    erreur = E.norm();

    iterations++;

    if(iterations>=this->maxIterations){
      //cout <<"Depassement du nombre d'iterations maximal defini"<<endl;
      break;
    }

    if(erreur<leastSquare){
      leastSquare = erreur;
      bestX = X;
      if(isEqual(erreur,0,1e-6)){
	////cout<<" Solution trouvee!"<<endl;
	break;
      }
    }
    else if(erreur> leastSquare){
      //cerr<<"Divergence possible methode de newton!"<<endl;
      break;
    }
    else{
      ////cout<<"stagnation!"<<endl;
    }
	  
  }

  if((lastError - erreur)<this->activeMinError){
    //cout << "la variation de l'erreur de la fonction est inferieure au seuil fixe"<<endl;
  }

  solution = bestX;

  ////cout <<"nombre d'iterations:"<<iterations<<endl;

  return leastSquare;

}

double LeastSquares::solveGaussNewton(const VectorXd & X0,Cout & cost,VectorXd & solution) const{

  double erreur = 1e+9;

  double pas =1;

  VectorXd X = X0;

  VectorXd bestX =X;

  VectorXd DX = VectorXd::Zero(X0.rows());

  VectorXd E = cost.fonctionObjectif(X);

  erreur = E.norm();

  double leastSquare = erreur;

  double lastError = erreur+1;

  bool temoin =false;

  VectorXd B;

  MatrixXd hessienne;

  MatrixXd jacobienne;

  int iterations =0;

  if(isEqual(erreur,0,1e-6)){

    solution = X;

    //cout<<"La valeur initiale est une solution"<<endl;

    return erreur;

  }

  while((lastError - erreur) >= this->activeMinError){

    jacobienne = cost.jacobienObjectif(X);

    B = jacobienne.transpose()*E;

    hessienne = jacobienne.transpose()*jacobienne;

    DX = hessienne.fullPivHouseholderQr().solve(B);

    pas = DX.norm();

    if(pas<this->activeMinPas){
      //cout<<"Variation de la solution inferieure au pas de discretisation defini"<<endl;
      break;
    }

    X = X - DX;

    E = cost.fonctionObjectif(X);

    lastError = erreur;

    erreur = E.norm();

    iterations++;

    if(iterations>=this->maxIterations){
      //cout<<"Depassement du nombre d'iterations maximal defini"<<endl;
      break;
    }

    if(isEqual(erreur,0,1e-6)){
      ////cout<<" Solution trouvee!"<<endl;
      leastSquare = erreur;
      bestX = X;
      break;
    }


    if(erreur > leastSquare){

      if(temoin){
	//cerr<<"Divergence confirmee!"<<endl;
	break;
      }
      else{
	//cerr<<"Divergence possible!"<<endl;
	X = X+0.8*DX;
	E = cost.fonctionObjectif(X);
	erreur = E.norm();
	temoin=true;
	continue;
      }

    }

    if(isEqual(erreur,0,1e-6)){
      //cout<<" Solution trouvee!"<<endl;
      leastSquare = erreur;
      bestX = X;
      break;
    }

    if(erreur<leastSquare){
      temoin = false;
      leastSquare = erreur;
      bestX = X;

    }

    if(isEqual(erreur,leastSquare,1e-6)){
      temoin = false;
      //cout<<"stagnation!"<<endl;
    }

  }

  if((lastError - erreur)<this->activeMinError){
    //cout << "la variation de l'erreur de la fonction est inferieure au seuil fixe"<<endl;
  }

  solution = bestX;

  //cout <<"nombre d'iterations:"<<iterations<<endl;

  return leastSquare;

}
