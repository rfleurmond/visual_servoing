#include "avoid.h"

Avoid::Avoid(double cMin, double cMax, double poids){
  assert(cMin<0.5);
  assert(cMax<0.5);
  assert(cMin>-0.5);
  assert(cMax>-0.5);
  assert(cMax<cMin);
  coeffMax = cMax;
  coeffMin = cMin;
  setWeight(poids);
  setCeil(10);
    
}

void Avoid::setWeight(double poids){
  pente = poids;
}

void Avoid::setCeil(double c){
  plafond = c;
}

void Avoid::setBounds(double mint, double maxt){
  assert(mint<maxt);
  min = mint;
  max = maxt;

  alpha = coeffMax * ( max - min ) + min;

  beta = coeffMin * ( max - min ) + min;

  milieu = 0.5 * ( max + min );

  psi = max - coeffMin * ( max - min );

  omega = max - coeffMax * ( max - min );

  assert(alpha<beta);
  assert(beta<milieu);
  assert(milieu<psi);
  assert(psi<omega);

}

double Avoid::operator()(double x){
  return coutHyp(x);
}

double Avoid::coutHyp(double x) const{
  double f = 0;
  if( x < beta){
    f = pente * (beta - x)/(x-alpha);
    if(min <x && x< alpha){
      f = pente * (beta - x)/(x-min);
    }
    if(x<=min && x < alpha) f = plafond; 
  }
  if( x > psi){
    f = pente * (x - psi)/(omega -x);
    if(omega <x && x< max){
      f = pente * (x - psi)/(max -x);
    }
    if(x>=max && x > omega) f = plafond;
  }
  if(f> plafond) f= plafond;
  return f;
}

double Avoid::coutTgt(double x) const{
  double f = 0;
  if( x < beta){
    f = 0.5* M_PI * pente * tan(0.5 * M_PI *(beta - x)/(beta - alpha));
    if(min <x && x< alpha){
      f = 0.5* M_PI * pente * tan(0.5 * M_PI *(beta - x)/(beta - min));
    }
    if(x<=min && x < alpha) f = plafond;
  }
  if( x > psi){
    f = 0.5* M_PI * pente * tan(0.5 * M_PI * (x - psi)/(omega -psi));
    if(omega <x && x< max){
      f = 0.5* M_PI * pente * tan(0.5 * M_PI * (x - psi)/(max -psi));
    }
    if(x>=max && x > omega) f = plafond;
  }
  if(f> plafond) f= plafond;
  return f;
}

double Avoid::jacobien( double x) const{
  double f = 0;
  if( x < beta){
    f = -1;
  }
  if( x > psi){
    f = 1;
  }
  return f;
}
