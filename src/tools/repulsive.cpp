#include "repulsive.h"

Repulsive::Repulsive(double cMin, double cMax, double poids){
  assert(cMin<0.5);
  assert(cMax<0.5);
  assert(cMin>-0.5);
  assert(cMax>-0.5);
  assert(cMax<cMin);
  coeffMax = cMax;
  coeffMin = cMin;
  setWeight(poids);
  setCeil(10);
    
}

void Repulsive::setWeight(double poids){
  pente = poids;
}

void Repulsive::setCeil(double c){
  plafond = c;
}

void Repulsive::setBounds(double mint, double maxt){
  assert(mint<maxt);
  min = mint;
  max = maxt;

  alpha = coeffMax * ( max - min ) + min;

  beta = coeffMin * ( max - min ) + min;

  milieu = 0.5 * ( max + min );

  psi = max - coeffMin * ( max - min );

  omega = max - coeffMax * ( max - min );

  assert(alpha<beta);
  assert(beta<milieu);
  assert(milieu<psi);
  assert(psi<omega);

}

double Repulsive::operator()(double x){
  return coutHyp(x);
}

double Repulsive::coutHyp(double x) const{
  double f = 0;
  if( x < beta){
    f = pente * (beta - x) * (beta - x) / (x-alpha);
    if(min <x && x< alpha){
      f = pente * (beta - x) * (beta - x) / (x-min);
    }
    if(x<=min && x < alpha) f = plafond; 
  }
  if( x > psi){
    f = pente * (x - psi) * (x - psi) / (omega -x);
    if(omega <x && x< max){
      f = pente * (x - psi) * (x - psi) / (max -x);
    }
    if(x>=max && x > omega) f = plafond;
  }
  if(f> plafond) f= plafond;
  return f;
}


double Repulsive::jacobien( double x) const{
  double j = 0;
  if( x < beta){
    j = pente * (beta - x) * (x - 2*alpha - beta) / ((x-alpha) * (x-alpha)) ;
    if(min <x && x< alpha){
      j = pente * (beta - x) * (x - 2*min - beta) / ((x-min) * (x-min));
    }
    if(x<=min && x < alpha) j = -plafond;
  }
  if( x > psi){
    j = pente * (x - psi) * (2*omega - psi - x) / ((omega -x) * (omega -x));
    if(omega <x && x< max){
      j = pente * (x - psi) * (2*max - psi - x) / ((max -x) * (max -x));
    }
    if(x>=max && x > omega) j = plafond;
  }
  if(j> plafond) j = plafond;
  if(j< -plafond) j = -plafond;
  return j;
}
