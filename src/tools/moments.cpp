#include "moments.h"

using namespace std;
using namespace Eigen;

VectorXd Moment::getMomentOrdre1(const list<VectorXd> & liste, int dim){

  VectorXd sol = VectorXd::Zero(dim);

  list<VectorXd>::const_iterator it = liste.begin();

  int n = 0;

  VectorXd X;

  while(it!=liste.end()){
    
    X = (VectorXd)(*it);
    
    assert(X.rows()==dim);

    sol = sol + X;

    n++;

    it++;
  }

  sol = (1.0/n)*sol;

  return sol;
  
}

MatrixXd Moment::getMomentOrdre2(const list<VectorXd> & liste, int dim){

  MatrixXd sol = MatrixXd::Zero(dim,dim);

  VectorXd moy = Moment::getMomentOrdre1(liste,dim);

  list<VectorXd>::const_iterator it = liste.begin();

  int n = 0;

  VectorXd X,Y;

  MatrixXd mat;

  while(it!=liste.end()){
    
    X = (VectorXd)(*it);
    
    assert(X.rows()==dim);

    Y = X - moy;

    mat = Y*Y.transpose();

    sol = sol + mat;

    n++;

    it++;
  }

  sol = (1.0/n)*sol;

  return sol;
  
}


