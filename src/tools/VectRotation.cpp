/*
 * VectRotation.cpp
 *
 *  Created on: RMay 24, 2012
 *      @author Renliw Fleurmond
 */

#include "VectRotation.h"
#include <cmath>
#include <iostream>

using namespace std;
using namespace Eigen;

const double VectRotation::EPSILON = 1e-10;

const double VectRotation::QUASIZERO = 1e-20;


void VectRotation::checkRotation(){
  angle = 0;
  direction = Vector3d::Zero();
  angle = rotation.angle();
  direction = rotation.axis();
  //cout << "\n\n\nangle = "<<angle<<endl;
  if(isEqual(angle,0,QUASIZERO)){
    //cout <<"\n\ngestion des valeurs bizzaroides"<<endl;
    //cout << "angle = "<<angle<<endl;
    direction = Vector3d::Zero();
    rotation = AngleAxisd(angle,direction);
    //cout <<"gestion des valeurs bizzaroides\n\n\n"<<endl;
  }
  if(angle<0){
    angle = -angle;
    direction = - direction;
    rotation = AngleAxisd(angle,direction);
  }
  matrice = rotation.matrix();
}



VectRotation::VectRotation():
  angle(0.0),
  direction(Vector3d::UnitZ()),
  rotation(AngleAxisd(0,Vector3d::UnitZ())),
  matrice(Matrix3d::Identity())
{
  // TODO Auto-generated constructor stub
  checkRotation();
}

VectRotation::VectRotation(double a,Vector3d axe):
  angle(a),
  direction(axe),
  rotation(AngleAxisd(a,axe)),
  matrice(Matrix3d::Identity())
{
  checkRotation();
}

VectRotation::VectRotation(AngleAxisd brut):
  angle(0),
  direction(Vector3d::UnitZ()),
  rotation(brut),
  matrice(Matrix3d::Identity())
{
  checkRotation();
}


VectRotation::VectRotation(Vector3d brut):
  angle(brut.norm()),
  direction(brut.normalized()),
  rotation(AngleAxisd(brut.norm(),brut.normalized())),
  matrice(Matrix3d::Identity())
{
  checkRotation();
}


VectRotation::VectRotation(Quaterniond brut):
  angle(0.0),
  direction(Vector3d::UnitZ()),
  rotation(brut),
  matrice(Matrix3d::Identity())
{
  checkRotation();
}

VectRotation::VectRotation(double x, double y, double z):
  angle(0.0),
  direction(Vector3d::UnitZ()),
  rotation(AngleAxisd(0.0,Vector3d::UnitZ())),
  matrice(Matrix3d::Identity())
{
  Vector3d brut(x,y,z);
  rotation = AngleAxisd(brut.norm(),brut.normalized());
  checkRotation();
}


VectRotation::VectRotation(Matrix3d rodrigues):
  angle(0.0),
  direction(Vector3d::UnitZ()),
  rotation(rodrigues),
  matrice(rodrigues)
{
  checkRotation();
}

Matrix3d VectRotation::getMatrix() const{
  return matrice;
}

double VectRotation::getAngle() const{
  return angle;
}

Vector3d VectRotation::getAxis() const{
  return direction;
}


Vector3d VectRotation::getVector() const{
  return angle*direction;
}


AngleAxisd VectRotation::getAngleAxisd() const{
  return rotation;
}


Vector3d VectRotation::rotate(const Vector3d & v) const{
  Vector3d r(Vector3d::Zero());
  r.noalias() += matrice * v;
  return r;
}


Vector3d VectRotation::deRotate(const Vector3d & v) const{
  Vector3d r(Vector3d::Zero());
  r.noalias() += matrice.transpose() * v;
  return r;
}




MatrixXd VectRotation::getJacobianCombinaison(const Vector3d & other) const{

  return this->getJacobianCombinaison(VectRotation(other));
}


MatrixXd VectRotation::getJacobianCombinaison(const AngleAxisd & other) const{

  return this->getJacobianCombinaison(VectRotation(other));

}


MatrixXd VectRotation::getJacobianCombinaison(const VectRotation & other) const{


  double angleO = other.getAngle();
  double angleT = angle;

  Matrix3d JT,JO,JR;

  Vector3d axeMoi = direction;
  Vector3d vecTgMoi   = tan(angleT/2)*axeMoi;

  Vector3d axeAutre = other.getAxis();
  Vector3d vecTgAutre   = tan(angleO/2)*axeAutre;


  if(angleT>5*1e+8*EPSILON){

    MatrixXd jUAlphaT(4,3);
    jUAlphaT.row(0) = axeMoi.transpose();
    jUAlphaT.block<3,3>(1,0).noalias() = 1.0/angleT * (MatrixXd::Identity(3,3)-axeMoi*axeMoi.transpose());

    MatrixXd jacRT(3,4);
    jacRT.col(0) = 0.5/pow(cos(angleT/2),2)*axeMoi;
    jacRT.block<3,3>(0,1) = tan(angleT/2)*Matrix3d::Identity();
    JT.noalias() = jacRT*jUAlphaT;
  }
  else{
    JT = 0.5*Matrix3d::Identity();
  }

  if(angleO>5*1e+8*EPSILON){

    MatrixXd jUAlphaO(4,3);
    jUAlphaO.row(0) = axeAutre.transpose();
    jUAlphaO.block<3,3>(1,0).noalias() = (1.0/angleO) * (MatrixXd::Identity(3,3)-axeAutre*axeAutre.transpose());

    MatrixXd jacRO(3,4);
    jacRO.col(0) = 0.5/pow(cos(angleO/2),2)*axeAutre;
    jacRO.block<3,3>(0,1) = tan(angleO/2)*Matrix3d::Identity(3,3);

    JO.noalias() = jacRO*jUAlphaO;
  }
  else{
    JO = 0.5*Matrix3d::Identity();
  }

  VectRotation fusion;
  fusion = (*this)*other;

  double angleR = fusion.getAngle();



  if(angleR>5*1e+8*EPSILON){

    Vector3d axeR = fusion.getAxis();
    MatrixXd jUAlphaR(4,3);
    jUAlphaR.row(0) = axeR.transpose();
    jUAlphaR.block<3,3>(1,0).noalias() = (1.0/angleR) * (MatrixXd::Identity(3,3)-axeR*axeR.transpose());

    MatrixXd jacR(3,4);
    jacR.col(0) = 0.5/pow(cos(angleO/2),2)*axeR;
    jacR.block<3,3>(0,1) = tan(angleO/2)*Matrix3d::Identity(3,3);

    JR.noalias() = jacR*jUAlphaR;

  }
  else{
    JR = 0.5*Matrix3d::Identity(3,3);
  }

  double denominateur = 1 -vecTgMoi.transpose()*vecTgAutre;

  MatrixXd bigJ(3,6);



  if(denominateur<EPSILON){

    bigJ.block<3,3>(0,0) = 0.05*Matrix3d::Identity(3,3);
    bigJ.block<3,3>(0,3) = 0.05*Matrix3d::Identity(3,3);

  }
  else{

    Vector3d ex,ey,ez;
    ex = Vector3d::UnitX();
    ey = Vector3d::UnitY();
    ez = Vector3d::UnitZ();

    Vector3d constante = vecTgMoi + vecTgAutre - vecTgAutre.cross(vecTgMoi);

    Vector3d colonne;
    colonne = denominateur*(ex + ex.cross(vecTgAutre)) + ex.dot(vecTgAutre) * constante;
    bigJ.col(0) = colonne;
    colonne = denominateur*(ey + ey.cross(vecTgAutre)) + ey.dot(vecTgAutre) * constante;
    bigJ.col(1) = colonne;
    colonne = denominateur*(ez + ez.cross(vecTgAutre)) + ez.dot(vecTgAutre) * constante;
    bigJ.col(2) = colonne;
    colonne = denominateur*(ex - ex.cross(vecTgMoi)) + ex.dot(vecTgMoi) * constante;
    bigJ.col(3) = colonne;
    colonne = denominateur*(ey - ey.cross(vecTgMoi)) + ey.dot(vecTgMoi) * constante;
    bigJ.col(4) = colonne;
    colonne = denominateur*(ez - ez.cross(vecTgMoi)) + ez.dot(vecTgMoi) * constante;
    bigJ.col(5) = colonne;

    bigJ = bigJ/pow(denominateur,2);

  }

  MatrixXd tampon(3,6);

  tampon.block<3,3>(0,0).noalias() = bigJ.block<3,3>(0,0)*JT;
  tampon.block<3,3>(0,3).noalias() = bigJ.block<3,3>(0,3)*JO;



  MatrixXd resultat(3,6);

  resultat= JR.fullPivLu().solve(tampon);
  /*cout << "bigJ = \n"<<bigJ<<endl;
    cout << "JT = \n"<<JT<<endl;
    cout << "JO = \n"<<JO<<endl;
    cout << "tampon = \n"<<tampon<<endl;
    cout << "JR = \n"<<JR<<endl;*/


  return resultat;

}



Matrix3d VectRotation::getJacobianRotation(const Vector3d & v) const{

  if(angle<(1e+4*EPSILON)){
    //cout <<"/matrice antisymetrique angle = "<<angle<<endl;
    return (-1*getSkewMatrix(v));
  }

  double cosA = cos(angle);
  double sinA = sin(angle);

  Vector3d ex,ey,ez;
  ex = Vector3d::UnitX();
  ey = Vector3d::UnitY();
  ez = Vector3d::UnitZ();

  MatrixXd jAlpha = MatrixXd(4,3);
  jAlpha.row(0) = direction.transpose();
  Matrix3d test = direction*direction.transpose();
  jAlpha.block<3,3>(1,0) = (1/angle)*(Matrix3d::Identity(3,3)-test);

  //cout <<"jacobienne OmegaTg2 =\n"<<jAlpha<<endl;
  MatrixXd jacob(3,4);

  Vector3d colonne = cosA * direction.cross(v) + sinA * direction.cross(direction.cross(v));
  jacob.col(0) =  colonne;
  colonne =(1-cosA) *(ex.cross(direction.cross(v)) + direction.cross(ex.cross(v))) + sinA * ex.cross(v);
  jacob.col(1) = colonne;
  colonne = (1-cosA) *(ey.cross(direction.cross(v)) + direction.cross(ey.cross(v))) + sinA * ey.cross(v);
  jacob.col(2) = colonne;
  colonne = (1-cosA) *(ez.cross(direction.cross(v)) + direction.cross(ez.cross(v))) + sinA * ez.cross(v);
  jacob.col(3) = colonne;

  Matrix3d resultat(jacob*jAlpha);

  return resultat;
}


Matrix3d VectRotation::getSkewMatrix() const{

  return this->getSkewMatrix(this->getVector());

}


Matrix3d VectRotation::getSkewMatrix(const Vector3d & w){

  Matrix3d chapeau;

  chapeau <<    0, -w(2),  w(1),
    w(2),    0, -w(0),
    -w(1), w(0),     0;
  
  return chapeau;

}

Matrix3d VectRotation::getTimeDerivativeJacobianOmega() const{

  if(angle<(1e+4*EPSILON)){

    return Matrix3d::Identity();

  }

  Matrix3d resultat = Matrix3d::Identity();

  Matrix3d U = this->getSkewMatrix(this->getAxis());

  double coeff = 1-0.25*angle*sin(angle)/pow(sin(angle/2),2);

  resultat.noalias() += -0.5 * angle * U + coeff * U * U;

  return resultat;
}

Matrix3d VectRotation::getInverseTimeDerivativeJacobianOmega() const{

  if(angle<(1e+4*EPSILON)){
    
    return Matrix3d::Identity();

  }

  Matrix3d resultat = Matrix3d::Identity();

  Matrix3d U = this->getSkewMatrix(this->getAxis());

  double alpha = 2*pow(sin(0.5*angle),2)/angle;

  double beta  = 1-sin(angle)/angle;

  resultat.noalias() += alpha*U + beta * U * U;

  return resultat;
}

VectRotation VectRotation::addTo(const VectRotation  & other) const{

  Matrix3d m(matrice);
  m *= other.matrice;
  VectRotation resultat(m);

  return resultat;

  /*

  double angleO = other.getAngle();
  //cout <<"moi"<<*this<<endl;
  //cout <<"oth"<<other<<endl;

  if(angle<angleO){
    if(angle<EPSILON){
      //cout <<"Il faut qu' il croisse et que je diminue!"<<endl;
      //cout <<"Mon angle ="<<angle<< " et l' angle de l' autre = "<<angleO<<endl;
      return other;
    }

  }

  VectRotation fusion;

  if(angleO<angle){

    if(angleO<EPSILON){
      //cout <<"Le moi est haissable!"<<endl;
      //cout <<"Mon angle ="<<angle<< " et l' angle de l' autre = "<<angleO<<endl;
      fusion = VectRotation(angle,direction);
      return fusion;
    }
  }


  Vector3d axeAutre = other.getAxis();

  Vector3d vecTgMoi   = tan(angle/2)*direction;

  Vector3d vecTgAutre   = tan(angleO/2)*axeAutre;

  //cout <<"tg moi = \n"<<vecTgMoi<<endl;

  //cout <<"tg autre = \n"<<vecTgAutre<<endl;

  double deno = 1 - vecTgMoi.transpose()*vecTgAutre;

  ///cout <<"denominateur = "<<deno<<endl;

  if(abs(deno)<=EPSILON){

    //cout<<" Youpi la quadrature du cercle!"<<endl;
    if(angle<angleO){
      fusion = VectRotation(M_PI,axeAutre);
    }
    else{
      if(angleO<angle){
	fusion = VectRotation(M_PI,direction);
      }
      else{
	fusion = VectRotation(M_PI,direction+axeAutre);
      }
    }

  }
  else{
    //cout << "Au moins ici la composition"<<endl;
    Vector3d allTG = (vecTgMoi + vecTgAutre + vecTgMoi.cross(vecTgAutre))/deno;

    //cout << "All tg= \n"<<allTG<<endl;

    double tangeante = allTG.norm();

    //cout <<"tangeante(alpha/2) = "<<tangeante<<endl;

    Vector3d axeR;

    double angleR;

    if(tangeante>EPSILON){

      //cout << "Nous avons pris la tangeante"<<endl;

      axeR = allTG/tangeante;

      //cout <<"Nouveau axe =\n"<<axeR<< endl;

      angleR = 2*atan(tangeante);

      //cout << "nouveau angle = "<<angleR<<endl;
    }
    else{

      //cout << "Nous restons au point zero"<<endl;

      angleR =0;

      axeR <<0,0,0;

    }

    fusion = VectRotation(angleR,axeR);
  }

  //VectRotation fusion = VectRotation(rotation*other.getAngleAxisd());

  return fusion;

  */
}

VectRotation VectRotation::inverse() const{
  Vector3d v = getVector();
  v = - v;
  v = deRotate(v);
  VectRotation other(v);
  return other;
}



VectRotation VectRotation::getMultiple(const double & scalaire) const{

  VectRotation multiple(scalaire *angle,direction);

  //cout <<"multiple=\n"<<multiple<<endl;

  return multiple;
}


VectRotation VectRotation::getRotationOnZ(const Vector3d & Z,
					  const Vector3d & original,
					  const Vector3d & copie){

  assert(isEqual(original.norm(),copie.norm(),1e-14));

  assert(isEqual(Z.norm(),1,1e-14));

  Vector3d V = copie.normalized();

  Vector3d XX = original.normalized();

  Vector3d YY = Z.cross(XX);

  Vector3d W = Z.cross(V);

  Matrix3d A = Matrix3d::Zero();

  A(0,0) = V.dot(XX);
  A(1,0) = V.dot(YY);
  A(2,0) = V.dot(Z);
  A(0,1) = W.dot(XX);
  A(1,1) = W.dot(YY);
  A(2,1) = W.dot(Z);
  A(0,2) = Z.dot(XX);
  A(1,2) = Z.dot(YY);
  A(2,2) = Z.dot(Z);
  
  VectRotation solution(A);

  return solution;
  
}


/**
 * Gestion de l'affichage dans un flux
 *
 */

ostream & VectRotation::afficher(ostream &flux) const{
  Vector3d v = this->getVector();
  flux << "[ "<<v(0)<<", "<<v(1)<<", "<<v(2)<<" ]^T";
  return flux;
}


VectRotation operator*(const VectRotation & rot1,const VectRotation &rot2){
  // operateur combinaison

  VectRotation rr =  rot1.addTo(rot2);
  return rr;

}



VectRotation operator/(const VectRotation &a, const VectRotation &b){
  
  VectRotation c = -1 * b; 
  VectRotation total = a * c;

  return total;

}



VectRotation operator*(const double &scalaire,const VectRotation & rot){

  VectRotation rr = rot.getMultiple(scalaire);
  //cout <<" multiplication scalaire" << *rr << endl;
  return rr;

}

VectRotation operator*(const VectRotation & rot,const double &scalaire){
  //cout<<"double appel"<<endl;
  return scalaire*rot;
}


ostream & operator<<(ostream & flux,const VectRotation& rot){
  // operateur affichage

  return rot.afficher(flux);

}

