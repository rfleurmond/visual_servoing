#include "../tools.h"
#include <fstream>

using namespace Eigen;
using namespace std;

int main (void){

  const string fichier_entree = "estim-nonlinear.dat";

  const string fichier_sortie = "3d-segment.dat";

  double * ligne = NULL;

  double x = 0, y = 0, z = 0 , a = 0, b = 0, c = 0;

  vector<double *> dataInput = loadDAT(fichier_entree,10);
  
  if(dataInput.size() == 0 ){
    cout << "Input file empty ! "<<endl;
    return 0;
  }

  std::ofstream printer; // output stream

  printer.open(fichier_sortie.c_str());
  
  if(!printer){
    cout << "Failed to create the output file  ! "<<endl;
    return 0;
  }

  unsigned int count = 0;

  while(count < dataInput.size()){

    ligne = dataInput[count];

    x = ligne[1];

    y = ligne[2];

    z = ligne[3];
      
    a = ligne[7];

    b = ligne[8];

    c = ligne[9];

    printer 
      << a << " "
      << b << " "
      << c << "\n"
      << x << " "
      << y << " "
      << z << "\n\n"<<endl;

    count ++;

  }

  printer.close();

  freeDATA(dataInput);

}
