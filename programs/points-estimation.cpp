#include "../vservoing.h"
#include <fstream>

using namespace Eigen;
using namespace std;

int main(void){

  Camera oeil;

  IntrinsicCam optique(240,240,320,240);
  
  oeil.setIntrinsicParameters(optique);

  double D = 4;
  double L = 2;
  
  MatrixXd points = MatrixXd::Zero(4,3);

  points << 
    D, L, L,
    D,-L, L,
    D,-L,-L,
    D, L,-L;

  VectorXd solution(12);
  
  solution << 
    D, L, L,
    D,-L, L,
    D,-L,-L,
    D, L,-L;

  MultiPointFeature cibleR(points);

  double cov = 1;

  MatrixXd covariance = cov * MatrixXd::Identity(8,8);
  
  NoisyFeature cible(&cibleR,covariance); 
  
  cible.attachToCamera(&oeil);

  cible.setStrategy(VisualFeature::PINV);

  MultiPointEstimator cibleE(&cible,&cible);

  cibleE.setLogFile("estim-nonlinear.dat");

  cibleE.attachToCamera(&oeil);

  cibleE.setThreshold(500);

  FlyingSystem peterpan;

  VisualTask tache(&cibleE, &peterpan);

  tache.setModeFixation(VisualTask::EMBARQUE);

  tache.setPeriod(1.0/20);

  tache.setLambda(0.2);

  //tache.setLambda(AdaptiveGain(0.5,5));

  tache.setMinimumErrorNorm(0.01*8*sqrt(cov));

  VectorXd etat = VectorXd::Zero(6);

  //etat << 0, 0, 0, -10, 0, 0;

  etat << 0, 0, 0, -2, 0, 0;

  tache.setState(etat);

  peterpan.setState(etat);

  MatrixXd lStar = cibleR.getInteractionMatrix();

  cibleR.setReferenceInteractionMatrix(lStar);

  VectorXd s_star = cibleR.getFunction();

  tache.setReference(s_star);

  etat << 0, 0, 0, 0, 0, 0;

  tache.setState(etat);

  peterpan.setState(etat);

  tache.keepVisualTrace(true);

  ////////////PREPARATION COMMANDE et ESTIMATION  ////////////////////////

  tache.start();

  TaskController controleur(&peterpan,&tache);

  controleur.setSmoothRatio(3);

  controleur.run();
  
}


