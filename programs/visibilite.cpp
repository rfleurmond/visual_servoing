#include "../vservoing.h"

using namespace Eigen;
using namespace std;

int main(void){

  Camera oeil;

  IntrinsicCam optique(320,320,320.5,240.5); 

  oeil.setIntrinsicParameters(optique);

  int D = 5;
  int L = 1;
  
  MatrixXd points = MatrixXd::Zero(4,3);

  points << 
    D, L, L,
    D,-L, L,
    D,-L,-L,
    D, L,-L;

  MultiPointFeature cible(points);

  cible.attachToCamera(&oeil);

  cible.setStrategy(VisualFeature::PINV);

  FlyingSystem peterpan;

  //Creation de la tache visuelle principale

  ManyArmHolderTask tache;

  // Ajout du bras robot

  tache.addArm(&peterpan,Repere());

  //Placement des caméras

  tache.addCamera(&oeil,&peterpan,Repere(),6);

  // Placement des indices visuels

  tache.addVisualTarget(&cible,NULL,Repere(),6);

  // Quels indices visuels percoivent les cameras?

  tache.makeTargetSeen(&cible,&oeil);

  // Etat souhaité

  VectorXd etat = VectorXd::Zero(6);

  etat << 
    0, 0, 0, 
    0, 0, 2;

  tache.setState(etat);

  peterpan.setState(etat);

  // Quelle est la reference des indices visuels par camera?
  
  cible.attachToCamera(&oeil);

  ConstReference ref1(cible.getFunction());

  tache.giveTargetReference(&oeil,&cible,&ref1,true);

  // Parametres interne de la tache
  
  tache.setPeriod(1.0/20);

  tache.setLambda(0.5);

  tache.setMinimumErrorNorm(1e-2);

  /// Ajout de contraintes sur les indices visuels

  ConstraintFeatures visible;

  VectorXd sMin = VectorXd::Zero(8);

  VectorXd sMax = VectorXd::Zero(8);

  sMax << 
    640, 480,
    640, 480,
    640, 480,
    640, 480;

  visible.setCoeffMax(0);
  visible.setCoeffMin(0.05);
  visible.setAlpha(5);
  visible.setMax(50);

  visible.addFeatures(&cible,sMin,sMax);
  
  tache.addOtherTask(&oeil,&visible);
 
  //Etat initial au début de l'asservissement
 
  //etat << 0, 0, 0, -5, 0, 0;

  etat << 
    0, 0, 0,
    0, 3, 0;

  tache.setState(etat);

  peterpan.setState(etat);

  
  /// Définition de la tache principale à exécuter

  MatrixXd selection1 = MatrixXd::Identity(8,16);

  PartTask principale(selection1,&tache);

  principale.setMinimumErrorNorm(1e-2);

  // Mise en oeuvre de la contrainte

  MatrixXd selection2 = MatrixXd::Zero(8,16);

  selection2.block<8,8>(0,8) = MatrixXd::Identity(8,8);

  PartTask contrainte(selection2,&tache);

  contrainte.setMinimumErrorNorm(1e-2);

  // Mise en oeuvre de la hiérarchie

  MyOptimaPriorityTask globale(&contrainte,&principale);

  globale.setState(etat);

  VectorXd val = tache.valeur();

  cout << "La fonction de tache \n"<<val<<endl;

  MatrixXd J = tache.jacobien(etat);

  cout <<"Jacobien de la tache\n"<< J <<endl;

  FullPivLU<MatrixXd> lu_decomp(J);

  cout << "The rank of J is " << lu_decomp.rank() << endl;

  cout <<"Etat du systeme\n"<<etat<<endl;

  val = tache.valeur();

  cout << "La fonction de tache \n"<<val<<endl;

  tache.keepVisualTrace(true);


  TaskController controleur(&peterpan,&globale);

  //controleur.enableSmoothing(false);

  //controleur.setSmoothRatio(5);

  controleur.setTimeLimit(30);

  //Lancement du contructeur

  controleur.run();

  //Fin du programme

  val = tache.valeur();

  cout << "La fonction de tache \n"<<val<<endl;

  
}


