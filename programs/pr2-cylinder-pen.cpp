#include "../vservoing.h"

using namespace Eigen;
using namespace std;

int main(void){

  //Definition de certains parametres

  double L = 0.07;
  double R = 0.011;

  //Creation des caméras

  Camera oeilFG,oeilHD;
  IntrinsicCam optiqueH(772.55,772.55,320.5,240.5);
  IntrinsicCam optiqueF(320,320,320.5,240.5);
  oeilFG.setIntrinsicParameters(optiqueF);
  oeilHD.setIntrinsicParameters(optiqueH);
  
  //Definition des indices visuels

  BoundedCylinder cibleGauche(L,R);

  BoundedCylinder cibleDroite(L,R);

  double cov = 25e-6;

  MatrixXd covariance = cov * MatrixXd::Identity(6,6);
    
  //NoisyFeature cibleGauche(&cibleG,covariance);

  //NoisyFeature cibleDroite(&cibleD,covariance);

  //Gestion des bras du robot

  //Creation des modeles vides et des bras

  GeoRobot brasGauche,brasDroite;

  //Chargement du modele geometrique des bras

  loadSE3Robot("../robot_models/pr2_gauche.ser",brasGauche);

  loadSE3Robot("../robot_models/pr2_droite.ser",brasDroite);

  //Creation des modeles cinematiques des bras

  MyKineModel modeleGauche(&brasGauche);

  MyKineModel modeleDroite(&brasDroite);

  //Creation de l'interface TaskSystem pour le controleur

  RobotSystem sysGauche(&brasGauche);

  RobotSystem sysDroite(&brasDroite);

  sysDroite.setPeriod(1.0/100);

  sysGauche.setPeriod(1.0/100);
 
  //Couplage des deux systemes

  DualArmSystem PR2(&sysGauche,&sysDroite);

  //Creation de la tache visuelle principale

  ManyArmHolderTask tache;

  // Defintion des reperes

  Repere tigeGauche(VectRotation(0,0,0),Vector3d(-3*L, 0, 0));
  
  Repere tigeDroite(VectRotation(0,0,0),Vector3d(-3*L, 0, 0));

  Repere epauleGauche(VectRotation(0,0,0),Vector3d(0,  0.188, 0));
  
  Repere epauleDroite(VectRotation(0,0,0),Vector3d(0, -0.188, 0));

  Repere priseGauche(VectRotation(0,0, 0),Vector3d(0.17, 0, -0.07));
  
  Repere priseDroite(VectRotation(0,0, 0),Vector3d(0.17, 0, 0));

  Repere camGauche(VectRotation(0,M_PI/4,0),Vector3d(0.067, 0.029, 0.4968));

  Repere camDroite(VectRotation(0,M_PI/4,0),Vector3d(0.067,-0.060, 0.4968));

  Repere fixDroite(VectRotation(3.018, -0.872524,0),
		   Vector3d(0.135-0.321, 0.044, 0));

  Repere simple;

  // Ajout des deux bras

  tache.addArm(&modeleGauche,epauleGauche);

  tache.addArm(&modeleDroite,epauleDroite);

  // Defintion des parametres de la tache

  tache.setPeriod(1.0/20);

  double lambda = 1;

  tache.setLambda(lambda);

  tache.setMinimumErrorNorm(1e-1);

  //Placement des caméras

  tache.addCamera(&oeilHD,NULL,camGauche,7);

  tache.addCamera(&oeilFG,&modeleDroite,fixDroite,5);

  // Placement des indices visuels

  tache.addVisualTarget(&cibleGauche,&modeleGauche,priseGauche,7);

  tache.addVisualTarget(&cibleDroite,&modeleDroite,priseDroite,7);

  // Quels indices visuels percoivent les cameras?

  tache.makeTargetSeen(&cibleGauche,&oeilFG);

  tache.makeTargetSeen(&cibleDroite,&oeilFG);

  tache.makeTargetSeen(&cibleGauche,&oeilHD);

  tache.makeTargetSeen(&cibleDroite,&oeilHD);

  // Quelle est la reference des indices visuels par camera?
  
  cibleDroite.attachToCamera(&oeilFG);

  OtherReference ref1(&cibleDroite);

  tache.giveTargetReference(&oeilHD,&cibleGauche,&ref1,true);

  // Constante arbitraire pour le moment
  
  CombinaisonFeature CF(&cibleGauche,&cibleDroite);

  MatrixXd C1 = MatrixXd::Zero(2,8);

  C1 << 
    0,0,1,0,0,0,0,0,
    0,0,0,0,0,0,1,0;
  
  MatrixXd C2 = MatrixXd::Zero(2,8);

  C2 << 
    0,0, 0,-1,  0,0,0, 0,
    0,0, 0, 0,  0,0,0,-1;
  
  VectorXd K(2);
  
  K << 30, 0;

  CF.setMatrices(C1,C2,K);

  tache.addOtherTask(&oeilFG,&CF);

  // Etat initial
  
  VectorXd etat = VectorXd::Zero(14);

  //*
  etat << 
    M_PI/4,  0.1,  M_PI/2,  -M_PI/2, -M_PI/2,  -0.05,  0,
   -M_PI/4, -0.1, -M_PI/2,  -M_PI/2,  M_PI/2,  -0.05,  0;
  //*/

  tache.setState(etat);

  PR2.setState(etat);

  cout << "Dimension de la tâche globale = "<< tache.getDimOutput()<<endl;
  
  //Defintion de la premiere tâche a enchainer

  // alignement des deux axes

  MatrixXd selection1 = MatrixXd::Zero(4,10);

  selection1.block<2,2>(0,0) = MatrixXd::Identity(2,2);
  
  selection1.block<2,2>(2,4) = MatrixXd::Identity(2,2);
  
  PartTask alignement(selection1,&tache);

  alignement.setMinimumErrorNorm(0.5);

  // Distance à conserver

  MatrixXd selection2 = MatrixXd::Zero(1,10);

  selection2(0,8) = 1;

  PartTask distance(selection2,&tache);

  distance.setMinimumErrorNorm(1e-2);

  PriorityTask approche(&distance, &alignement);

  approche.setMinimumErrorNorm(1e-2);

  //Defintion de la deuxieme tache a enchainer
  
  MatrixXd selection3 = MatrixXd::Zero(1,10);

  selection3(0,9) = 1;

  PartTask translation(selection3,&tache);

  translation.setMinimumErrorNorm(1e-2);

  PriorityTask assemblage(&alignement,&translation);

  assemblage.setMinimumErrorNorm(1e-3);

  // Ecriture de la priorite entre les deux taches


  //Definition de la sequence de tâches
  
  SequencyTask sequence(&distance,&approche);

  sequence.addTaskInStack(&assemblage);

  distance.setLambda(AdaptiveGain(1.5,5));

  approche.setLambda(AdaptiveGain(1.5,5));

  assemblage.setLambda(AdaptiveGain(1.5,5));
  
  //Gestion des butees

  VectorXd buteeMin(14);

  VectorXd buteeMax(14);

  buteeMin.topRows(7) = brasGauche.getMinimalQ();
  buteeMin.bottomRows(7) = brasDroite.getMinimalQ();

  buteeMax.topRows(7) = brasGauche.getMaximalQ();
  buteeMax.bottomRows(7) = brasDroite.getMaximalQ();

  MatrixXd selection4 = MatrixXd::Zero(10,14);

  selection4.block<4,4>(0,0) = MatrixXd::Identity(4,4);
  selection4.block<4,4>(5,7) = MatrixXd::Identity(4,4);
  selection4(4,5)  = 1;
  selection4(9,12) = 1;

  buteeMin = selection4*buteeMin;
  buteeMax = selection4*buteeMax;

  QBoundedTask gendarme(buteeMin,buteeMax,selection4);

  gendarme.setCoeffMax(-0.01);

  gendarme.setCoeffMin(0.05);

  gendarme.setAlpha(2);

  gendarme.setMax(10);
  
  gendarme.setState(etat);

  gendarme.setLambda(2);

  MyOptimaPriorityTask globale(&gendarme,&sequence);

  globale.setState(etat);

  //Creation du controleur
  
  cov = 6.4e-8;
  
  covariance = cov * MatrixXd::Identity(14,14);

  NoisySystem noisyPR2(&PR2,covariance);

  VectorXd val = tache.valeur();

  cout << "La fonction de tache \n"<<val<<endl;

  MatrixXd J = tache.jacobien(etat);

  cout <<"Jacobien de la tache\n"<< J <<endl;

  FullPivLU<MatrixXd> lu_decomp(J);

  cout << "The rank of J is " << lu_decomp.rank() << endl;

  cout <<"Etat du systeme\n"<<etat<<endl;

  TaskController controleur(&PR2,&globale);

  //controleur.enableSmoothing(false);

  //controleur.setSmoothRatio(5);

  controleur.setTimeLimit(30);

  //Lancement du contructeur

  controleur.run();

  //Fin du programme
  
}
