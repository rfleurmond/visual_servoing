#include "../vservoing.h"

using namespace Eigen;
using namespace std;

int main(void){

  Camera oeil;

  IntrinsicCam optique(240,240,320,240); 

  //oeil.setIntrinsicParameters(optique);

  Vector3d direction1(0,0,1);

  Vector3d centre1(10,2,-2);

  Droite ligne1(centre1,direction1);

  DroiteFeature indice1(ligne1);

  MultiFeature cible(&indice1);

  Vector3d direction2(0,1,0);

  Vector3d centre2(10,0,2);

  Droite ligne2(centre2,direction2);

  DroiteFeature indice2(ligne2);

  cible.addFeature(&indice2);
  
  Vector3d direction3(0,0,-1);

  Vector3d centre3(10,-2,-2);

  Droite ligne3(centre3,direction3);

  DroiteFeature indice3(ligne3);

  cible.addFeature(&indice3);
  
  cible.attachToCamera(&oeil);

  cout<<"Dimension de la cible:"<<cible.getDimensionOutput()<<endl;

  FlyingSystem peterpan;

  VisualTask tache(&cible, &peterpan);

  tache.setModeFixation(VisualTask::EMBARQUE);

  tache.setPeriod(1.0/20);

  tache.setLambda(0.8);

  tache.setMinimumErrorNorm(1e-4);

  VectorXd etat = VectorXd::Zero(6);

  etat << 0, 0, 0, 0, 0, 0;

  tache.setState(etat);

  peterpan.setState(etat);

  MatrixXd lStar = cible.getInteractionMatrix();

  cible.setReferenceInteractionMatrix(lStar);

  VectorXd s_star = cible.getFunction();

  //etat << 0.5, 0.2, 0.3, 7, -2, 0;

  etat << 0.5, 0, 0, -10, 0, 0;

  tache.setReference(s_star);

  tache.setState(etat);

  peterpan.setState(etat);

  cout<<"Valeur des indices visuels\n"<<cible.getFunction().transpose()<<endl;

  cout<<"Valeur souhaitee des indices visuels\n"<<s_star.transpose()<<endl;

  cout<<"Valeur de la tache\n"<<tache.valeur().transpose()<<endl;

  cout<<"Jacobien de la fonction de tache\n"<<tache.jacobien(etat)<<endl;

  TaskController controleur(&peterpan,&tache);

  controleur.setTimeLimit(100);

  cout << " Avant run !" << endl;

  //tache.keepVisualTrace(true);

  controleur.run();

  cout << " Apres run !" << endl;
  
}


