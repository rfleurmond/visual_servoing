#include "../vservoing.h"

using namespace Eigen;
using namespace std;

int main(void){

  //Definition de certains parametres

  double L = 0.07;
  
  //Creation des caméras

  Camera oeilG,oeilD;
  IntrinsicCam optique(418.6835, 419.16346, 309.05563, 224.1891); 
  oeilG.setIntrinsicParameters(optique);
  oeilD.setIntrinsicParameters(optique);
  

  //Definition des indices visuels

  Vector3d p0(0, 0, 0);

  Vector3d p1(0, 0, L);
  
  LineWithK cibleG(p0,p1,p0);

  LineWithK cibleD(p0,p1,p1);

  double cov = 25e-10;

  MatrixXd covariance = cov * MatrixXd::Identity(3,3);
    
  NoisyFeature cibleGauche(&cibleG,covariance);

  NoisyFeature cibleDroite(&cibleD,covariance);

  //Gestion des bras du robot

  //Creation des modeles vides et des bras

  GeoRobot brasGauche,brasDroite;

  //Chargement du modele geometrique des bras

  loadSE3Robot("../robot_models/pr2_gauche.ser",brasGauche);

  loadSE3Robot("../robot_models/pr2_droite.ser",brasDroite);

  //Creation des modeles cinematiques des bras

  MyKineModel modeleGauche(&brasGauche);

  MyKineModel modeleDroite(&brasDroite);

  //Creation de l'interface TaskSystem pour le controleur

  RobotSystem sysGauche(&brasGauche);

  RobotSystem sysDroite(&brasDroite);

  sysDroite.setPeriod(1.0/100);

  sysGauche.setPeriod(1.0/100);
 
  //Couplage des deux systemes

  DualArmSystem PR2(&sysGauche,&sysDroite);

  //Creation de la tache visuelle principale

  ManyArmHolderTask tache;

  // Defintion des reperes

  Repere tigeGauche(VectRotation(0,0,0),Vector3d(-3*L, 0, 0));
  
  Repere tigeDroite(VectRotation(0,0,0),Vector3d(-3*L, 0, 0));

  Repere epauleGauche(VectRotation(0,0,0),Vector3d(0,  0.188, 0));
  
  Repere epauleDroite(VectRotation(0,0,0),Vector3d(0, -0.188, 0));

  Repere priseGauche(VectRotation(0,0, 0),Vector3d(0.17, 0, -0.07));
  
  Repere priseDroite(VectRotation(0,0, 0),Vector3d(0.17, 0, 0));

  Repere camGauche(VectRotation(0,M_PI/4,0),Vector3d(0.067, 0.029, 0.4968));

  Repere camDroite(VectRotation(0,M_PI/4,0),Vector3d(0.067,-0.060, 0.4968));

  Repere simple;

  // Ajout des deux bras

  tache.addArm(&modeleGauche,epauleGauche);

  tache.addArm(&modeleDroite,epauleDroite);

  // Defintion des parametres de la tache

  tache.setPeriod(1.0/15);

  double lambda = 1;

  tache.setLambda(lambda);

  //tache.setLambda(AdaptiveGain(lambda,5*lambda));

  tache.setMinimumErrorNorm(1e-4);

  //Placement des caméras

  tache.addCamera(&oeilG,NULL,camGauche,7);

  tache.addCamera(&oeilD,NULL,camDroite,7);

  // Placement des indices visuels

  tache.addVisualTarget(&cibleGauche,&modeleGauche,priseGauche,7);

  tache.addVisualTarget(&cibleDroite,&modeleDroite,priseDroite,7);

  // Quels indices visuels percoivent les cameras?

  tache.makeTargetSeen(&cibleGauche,&oeilG);

  tache.makeTargetSeen(&cibleDroite,&oeilG);

  tache.makeTargetSeen(&cibleGauche,&oeilD);

  tache.makeTargetSeen(&cibleDroite,&oeilD);

  // Quelle est la reference des indices visuels par camera?
  
  cibleDroite.attachToCamera(&oeilG);

  OtherReference ref1(&cibleDroite);

  tache.giveTargetReference(&oeilG,&cibleGauche,&ref1,true);

  cibleDroite.attachToCamera(&oeilD);

  OtherReference ref2(&cibleDroite);

  tache.giveTargetReference(&oeilD,&cibleGauche,&ref2,true);

  // Constante arbitraire pour le moment
  
  VectorXd constante(3);
  
  constante << 0, 0, -50;

  tache.giveKandCmatrix(&oeilG,&cibleGauche,MatrixXd::Identity(3,3),constante);
  
  // Etat initial
  
  VectorXd etat = VectorXd::Zero(14);

  //*
  etat << 
    M_PI/4, -0.1,  M_PI/2,  -M_PI/2, -M_PI/2,  -0.05,  0,
   -M_PI/4,  0.1, -M_PI/2,  -M_PI/2,  M_PI/2,  -0.05,  0;
  //*/

  tache.setState(etat);

  PR2.setState(etat);

  //Defintion de la premiere tâche a enchainer

  // alignement des deux axes

  MatrixXd selection1(3,6);

  selection1<<
    1, 0, 0, 0, 0, 0,
    0, 1, 0, 0, 0, 0,
    0, 0, 1, 0, 0, 0;

  PartTask coplanaire(selection1,&tache);

  coplanaire.setMinimumErrorNorm(1);

  // Definition de la deuxieme tache a enchainer

  MatrixXd selection2(5,6);

  selection2<<
    1, 0, 0, 0, 0, 0,
    0, 1, 0, 0, 0, 0,
    0, 0, 1, 0, 0, 0,
    0, 0, 0, 1, 0, 0,
    0, 0, 0, 0, 1, 0;

  PartTask alignement(selection2,&tache);

  alignement.setMinimumErrorNorm(1);

  //Defintion de la troisieme tache a enchainer
  
  MatrixXd selection3(5,6);

  selection3<<
    1, 0, 0, 0, 0, 0,
    0, 1, 0, 0, 0, 0,
    0, 0, 0, 1, 0, 0,
    0, 0, 0, 0, 1, 0,
    0, 0, 0, 0, 0, 1;
  
  PartTask translation(selection3,&tache);

  translation.setMinimumErrorNorm(1);

  //Definition de la sequence de tâches
  
  SequencyTask sequence(&coplanaire,&alignement);

  sequence.addTaskInStack(&translation);

  //Gestion des butees

  VectorXd buteeMin(14);

  VectorXd buteeMax(14);

  buteeMin.topRows(7) = brasGauche.getMinimalQ();
  buteeMin.bottomRows(7) = brasDroite.getMinimalQ();

  buteeMax.topRows(7) = brasGauche.getMaximalQ();
  buteeMax.bottomRows(7) = brasDroite.getMaximalQ();

  MatrixXd selection4 = MatrixXd::Zero(10,14);

  selection4.block<4,4>(0,0) = MatrixXd::Identity(4,4);
  selection4.block<4,4>(5,7) = MatrixXd::Identity(4,4);
  selection4(4,5)  = 1;
  selection4(9,12) = 1;

  buteeMin = selection4*buteeMin;
  buteeMax = selection4*buteeMax;

  QBoundedTask gendarme(buteeMin,buteeMax,selection4);

  gendarme.setCoeffMax(-0.01);

  gendarme.setCoeffMin(0.03);

  gendarme.setAlpha(2);

  gendarme.setMax(10);
  
  gendarme.setState(etat);

  MyOptimaPriorityTask globale(&gendarme,&sequence);

  globale.hideTaskOnControl(&gendarme);

  globale.setState(etat);

  //Creation du controleur
  
  cov = 7.6e-8;

  covariance = cov * MatrixXd::Identity(14,14);

  NoisySystem noisyPR2(&PR2,covariance);

  VectorXd val = tache.valeur();

  cout << "La fonction de tache \n"<<val<<endl;

  MatrixXd J = tache.jacobien(etat);

  cout <<"Jacobien de la tache\n"<< J <<endl;

  FullPivLU<MatrixXd> lu_decomp(J);

  cout << "The rank of J is " << lu_decomp.rank() << endl;

  cout <<"Etat du systeme\n"<<etat<<endl;

  val = tache.valeur();

  cout << "La fonction de tache \n"<<val<<endl;

  TaskController controleur(&noisyPR2,&globale);

  //controleur.enableSmoothing(false);

  //controleur.setSmoothRatio(5);

  controleur.setTimeLimit(30);

  //Lancement du contructeur

  controleur.run();

  //Fin du programme

  val = tache.valeur();

  cout << "La fonction de tache \n"<<val<<endl;

}
