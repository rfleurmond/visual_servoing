#include "../taches.h"

using namespace Eigen;

using namespace std;

int main(void){

  double lambda = 0.5;

  VectorXd etat = VectorXd::Zero(2);

  etat << 0, 0;

  QSystem robot(etat,1.0/200);

  VectorXd cibleM = VectorXd::Zero(2);

  cibleM << 6,2;

  QTask1D master(cibleM);

  MatrixXd a = MatrixXd::Zero(1,2);
  
  a << 0, 1;

  //master.setActivationMatrix(a);

  master.setLambda(lambda);

  master.setState(etat);

  master.setPeriod(1.0/200);

  VectorXd cibleS = VectorXd::Zero(2);

  cibleS << 6,-2;

  QTask1D slave(cibleS);
  
  slave.setLambda(lambda);
  
  slave.setState(etat);

  master.setPeriod(1.0/200);
  
  CoTask dualite(&master,&slave);

  //EgalityTask dualite(&master,&slave);

  //PriorityTask dualite(&master,&slave);

  //OptimaPriorityTask dualite(&master,&slave);

  //MyOptimaPriorityTask dualite(&master,&slave);

  cout <<"Valeur de la tache"<<endl;

  cout <<dualite.valeur()<< endl;

  cout <<"Jacobien de la tache:"<< endl;

  cout <<dualite.jacobien(etat) << endl;

  TaskController controleur(&robot,&dualite);

  controleur.setSysLogFile("data_sys1.dat");

  controleur.setSmoothRatio(7);

  controleur.enableSmoothing(false);

  controleur.setTimeLimit(10);

  cout << "\n\n Avant run !" << endl;

  controleur.run();

  cout << " Apres run !" << endl;
 

}
