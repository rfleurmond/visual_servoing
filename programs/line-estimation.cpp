#include "../vservoing.h"

using namespace Eigen;
using namespace std;


int main(void){

  Camera oeil;

  IntrinsicCam optique(700,700,350,350);
  optique.largeur = 700;
  optique.hauteur = 700;

  double dim = 0.1;

  
  oeil.setIntrinsicParameters(optique);

  // Create the visual target
  
  Vector3d direction1(0,0,1);

  Vector3d centre1(0,dim,-dim);

  Droite ligne1(centre1,direction1);

  DroiteFeature indice1(ligne1);


  Vector3d direction2(0,1,0);

  Vector3d centre2(0,0,dim);

  Droite ligne2(centre2,direction2);

  DroiteFeature indice2(ligne2);


  Vector3d direction3(0, 0, -1);

  Vector3d centre3(0,-dim,-dim);

  Droite ligne3(centre3,direction3);

  DroiteFeature indice3(ligne3);

  // Add noise to the target

  double covP = 4;

  double d2r = M_PI/180;

  double covT =  d2r * d2r;

  cout << " Covoariance de l'angle = " << covT << endl;

  MatrixXd covariance = covP * MatrixXd::Identity(2,2);

  covariance(1,1) =  covT;
  
  NoisyFeature cible1(&indice1,covariance);

  NoisyFeature cible2(&indice2,covariance);

  NoisyFeature cible3(&indice3,covariance);

  // Put the estimator between 

  LineEstimator cibleE1(&cible1,&cible1);

  LineEstimator cibleE2(&cible2,&cible2);

  LineEstimator cibleE3(&cible3,&cible3);

  cibleE1.setLogFile("estim-nonlinear.dat");

  cibleE1.attachToCamera(&oeil);

  cibleE2.attachToCamera(&oeil);

  cibleE3.attachToCamera(&oeil);

  // Set different countdowns

  cibleE1.setDeadline(3);

  cibleE2.setDeadline(3);

  cibleE3.setDeadline(3);

 
  /// Put the features in one 

  //*

  MultiFeature bigTarget(&cibleE1);

  bigTarget.addFeature(&cibleE2);
  
  bigTarget.addFeature(&cibleE3);
  
  //*/

  /*
  
  MultiFeature bigTarget(&cible1);

  bigTarget.addFeature(&cible2);
  
  bigTarget.addFeature(&cible3);
  
  //*/


  bigTarget.attachToCamera(&oeil);


  FlyingSystem arm;

  Repere idi;

  Repere shoulder(VectRotation(),Vector3d(0,0,0));

  ManyArmHolderTask tache;

  tache.addArm(&arm,idi);
  
  tache.addCamera(&oeil,&arm,idi,6);

  tache.addVisualTarget(&bigTarget,NULL,shoulder,6);

  tache.makeTargetSeen(&bigTarget,&oeil);

  tache.setPeriod(1.0/20);

  tache.setLambda(0.5);

  //tache.setLambda(AdaptiveGain(0.8, 20));

  tache.setMinimumErrorNorm(0.001 * 6* sqrt(covP));

  VectorXd etat = VectorXd::Zero(6);

  etat << 
   0, 0, 0, // orientation
 -0.6, 0, 0; // position

  tache.setState(etat);

  arm.setState(etat);

  VectorXd s_star = VectorXd::Zero(6);

  s_star.block<2,1>(0,0) =  indice1.getFunction();

  s_star.block<2,1>(2,0) =  indice2.getFunction();

  s_star.block<2,1>(4,0) =  indice3.getFunction();

  ConstReference ref(s_star); 

  tache.giveTargetReference(&oeil,&bigTarget,&ref,true);

  etat << 
    0.5, 0.2, 0.3, 
  -3, 0.2, 0.4;

  tache.setState(etat);

  arm.setState(etat);

  tache.start();

  //tache.keepVisualTrace(true);

  ////////////PREPARATION COMMANDE et ESTIMATION  ////////////////////////

  MatrixXd traduction = MatrixXd::Identity(6,6);

  traduction(1,1) = 360/M_PI;

  traduction(3,3) = 360/M_PI;

  traduction(5,5) = 360/M_PI;

  PartTask conversion(traduction,&tache);

  cout << " Avant run !" << endl;

  /////////////////////////////////RUN/////////////////////////////////

  TaskController controleur(&arm,&conversion);

  controleur.setSmoothRatio(8);

  //controleur.enableSmoothing(false);

  controleur.setTimeLimit(10);

  controleur.run();
  
  //////////////////////////FIN RUN //////////////////////////////////

  cout << " Apres run !" << endl;
  
}


