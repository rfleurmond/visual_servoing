#include "../vservoing.h"

using namespace Eigen;
using namespace std;

int main(void){

  //Definition de certains parametres

  double L = 0.075;

  //Creation des caméras

  Camera oeilHG,oeilHD,oeilFD;
  IntrinsicCam optiqueH(772.55,772.55,320.5,240.5);
  //IntrinsicCam optiqueH(418.6835, 419.16346, 309.05563, 224.1891);
  IntrinsicCam optiqueF(320,320,320.5,240.5);
 
  oeilHG.setIntrinsicParameters(optiqueH);
  oeilHD.setIntrinsicParameters(optiqueH);
  oeilFD.setIntrinsicParameters(optiqueF);
  
  //Definition des indices visuels

  Vector3d p0(0, 0, 0);

  Vector3d p1(0, 0, L);
  
  LineWithK cibleG(p0,p1,p0);

  LineWithK cibleD(p0,p1,p1);

  double cov = 25e-8;

  double covP = 1;

  //double cov = 25e-30;

  //double covP = 1e-30;

  double borneInf = 1.5;

  MatrixXd covariance(3,3);

  covariance << 
    covP,   0,    0,
    0,    cov,    0,
    0,      0, covP;
    
  NoisyFeature cibleGauche(&cibleG,covariance);

  NoisyFeature cibleDroite(&cibleD,covariance);

  //Gestion des bras du robot

  //Creation des modeles vides et des bras

  GeoRobot brasGauche,brasDroite;

  //Chargement du modele geometrique des bras

  loadSE3Robot("../robot_models/pr2_gauche.ser",brasGauche);

  loadSE3Robot("../robot_models/pr2_droite.ser",brasDroite);

  //Creation des modeles cinematiques des bras

  MyKineModel modeleGauche(&brasGauche);

  MyKineModel modeleDroite(&brasDroite);

  //Creation de l'interface TaskSystem pour le controleur

  RobotSystem sysGauche(&brasGauche);

  RobotSystem sysDroite(&brasDroite);

  sysDroite.setPeriod(1.0/100);

  sysGauche.setPeriod(1.0/100);
 
  //Couplage des deux systemes

  DualArmSystem PR2(&sysGauche,&sysDroite);

  cov = 7.6e-9;//9
  //cov = 7.6e-29;//9


  covariance = cov * MatrixXd::Identity(14,14);

  NoisySystem noisyPR2(&PR2,covariance);

  //Creation de la tache visuelle principale

  ManyArmHolderTask tache;

  // Defintion des reperes

  Repere tigeGauche(VectRotation(0,0,0),Vector3d(-3*L, 0, 0));
  
  Repere tigeDroite(VectRotation(0,0,0),Vector3d(-3*L, 0, 0));

  Repere epauleGauche(VectRotation(0,0,0),Vector3d(0,  0.188, 0));
  
  Repere epauleDroite(VectRotation(0,0,0),Vector3d(0, -0.188, 0));

  Repere priseGauche(VectRotation(0, 0, 0),Vector3d(0.17, 0,-0.07));
  
  Repere priseDroite(VectRotation(0, 0, 0),Vector3d(0.17, 0, 0.00));

  Repere camGauche(VectRotation(0,M_PI/4,0),Vector3d(0.067, 0.029, 0.4968));

  Repere camDroite(VectRotation(0,M_PI/4,0),Vector3d(0.067,-0.060, 0.4968));

  Repere fixDroite(VectRotation(3.018, -0.872524,0),
		   Vector3d(0.135-0.321, 0.044, 0));

  Repere simple;

  // Ajout des deux bras a la tache

  tache.addArm(&modeleGauche,epauleGauche);

  tache.addArm(&modeleDroite,epauleDroite);

  // Defintion des parametres systemes de la tache

  tache.setPeriod(1.0/15);

  double lambda = 0.5;

  tache.setLambda(lambda);

  tache.setMinimumErrorNorm(1);

  //Placement des caméras

  tache.addCamera(&oeilFD,&modeleDroite,fixDroite,5,"Moving Right forearm Camera");

  //tache.addCamera(&oeilHD,NULL,camDroite,7);

  tache.addCamera(&oeilHG,NULL,camGauche,7,"Non-moving Head camera");

  // Placement des indices visuels

  tache.addVisualTarget(&cibleGauche,&modeleGauche,priseGauche,7);

  tache.addVisualTarget(&cibleDroite,&modeleDroite,priseDroite,7);

  // Quels indices visuels percoivent les cameras?

  tache.makeTargetSeen(&cibleGauche,&oeilHG);

  tache.makeTargetSeen(&cibleDroite,&oeilHG);

  tache.makeTargetSeen(&cibleGauche,&oeilFD);

  tache.makeTargetSeen(&cibleDroite,&oeilFD);

  //tache.makeTargetSeen(&cibleGauche,&oeilHD);

  //tache.makeTargetSeen(&cibleDroite,&oeilHD);

  // Quelle est la reference des indices visuels par camera?
  
  cibleDroite.attachToCamera(&oeilHG);

  OtherReference ref1(&cibleDroite);

  tache.giveTargetReference(&oeilHG,&cibleGauche,&ref1,true);

  cibleDroite.attachToCamera(&oeilFD);

  OtherReference ref2(&cibleDroite);

  tache.giveTargetReference(&oeilFD,&cibleGauche,&ref2,true);

  /// Ajout de contraintes sur les indices visuels

  ConstraintFeatures visible[2];

  VectorXd sMin = VectorXd::Ones(2);

  VectorXd sMax = VectorXd::Ones(2);

  MatrixXd acti = MatrixXd::Zero(2,3);
  
  sMin = -240.00/sqrt(2) * sMin;

  sMax << 240.00/sqrt(2) * sMax;

  acti << 
    1, 0, 0,
    0, 0, 1;

  double coefMax = -0.4;
  double coefMin = 0.1;
  double alpha = 0.5;
  double max = 50;

  for(int i = 0; i < 2; i++){

    visible[i].setCoeffMax(coefMax);
    visible[i].setCoeffMin(coefMin);
    visible[i].setAlpha(alpha);
    visible[i].setMax(max);
  }
  
  visible[0].addFeatures(&cibleGauche,sMin,sMax,acti);
  visible[1].addFeatures(&cibleDroite,sMin,sMax,acti);

  tache.addOtherTask(&oeilHG,&visible[0]);
  tache.addOtherTask(&oeilHG,&visible[1]);
  
  tache.addOtherTask(&oeilFD,&visible[0]);
  tache.addOtherTask(&oeilFD,&visible[1]);
  
  
  // Etat initial
  
  VectorXd etat = VectorXd::Zero(14);

  //*
  etat << 
    M_PI/4,  0.1,  M_PI/2,  -M_PI/2, -M_PI/2,  -0.05,  0,
   -M_PI/4, -0.1, -M_PI/2,  -M_PI/2,  M_PI/2,  -0.05,  0;
  //*/

  tache.setState(etat);

  PR2.setState(etat);


  //Defintion de la premiere tâche a enchainer

  // alignement des deux axes

  MatrixXd selection1 = MatrixXd::Identity(3,14);

  selection1.row(1) = 360/M_PI * selection1.row(1);

  PartTask coplanaire(selection1,&tache);

  // Constante arbitraire pour l'écart entre le stylo et le capuchon

  double offset = -50;
  
  VectorXd constante(3);
  
  constante << 0, 0, offset;

  coplanaire.setOffset(constante);

  coplanaire.setMinimumErrorNorm(borneInf);

  //coplanaire.setLambda(AdaptiveGain(lambda,5*lambda));

  
// Definition de la deuxieme tache a enchainer

  MatrixXd selection2 = MatrixXd::Identity(5,14);

  selection2.row(1) = 360/M_PI * selection2.row(1);

  selection2.row(4) = 360/M_PI * selection2.row(4);

  PartTask alignement(selection2,&tache);

  constante = VectorXd::Zero(5);

  constante(2) = offset;

  alignement.setOffset(constante);

  alignement.setMinimumErrorNorm(borneInf);

  //alignement.setLambda(AdaptiveGain(lambda,6*lambda));

  //Defintion de la troisieme tache a enchainer
  
  MatrixXd selection3 = MatrixXd::Identity(6,14);

  selection3(1,1) =  360/M_PI;
  selection3(4,4) =  360/M_PI;

  PartTask translation(selection3,&tache);

  translation.setLambda(0.2);

  translation.setMinimumErrorNorm(borneInf);

  //translation.setLambda(AdaptiveGain(lambda,5*lambda));


  //Definition de la sequence de tâches
  
  SequencyTask sequence(&coplanaire,&alignement);

  sequence.addTaskInStack(&translation);

  // Création de la tache spécifique à la contrainte de visibilité

  MatrixXd selection4 = MatrixXd::Zero(8,14);

  selection4.block<8,8>(0,6)  = MatrixXd::Identity(8,8);
  
  PartTask resteVisible(selection4,&tache);

  resteVisible.setMinimumErrorNorm(1);

  resteVisible.setLambda(2);




  //Gestion des butees

  VectorXd buteeMin(14);

  VectorXd buteeMax(14);

  buteeMin.topRows(7) = brasGauche.getMinimalQ();
  buteeMin.bottomRows(7) = brasDroite.getMinimalQ();

  buteeMax.topRows(7) = brasGauche.getMaximalQ();
  buteeMax.bottomRows(7) = brasDroite.getMaximalQ();

  MatrixXd selection5 = MatrixXd::Zero(10,14);

  selection5.block<4,4>(0,0) = MatrixXd::Identity(4,4);
  selection5.block<4,4>(5,7) = MatrixXd::Identity(4,4);
  selection5(4,5)  = 1;
  selection5(9,12) = 1;

  buteeMin = selection5 * buteeMin;
  buteeMax = selection5 * buteeMax;

  QBoundedTask gendarme(buteeMin,buteeMax,selection5);

  gendarme.setCoeffMax(-0.01);

  gendarme.setCoeffMin(0.04);

  gendarme.setAlpha(20);

  gendarme.setMax(10);
  
  gendarme.setState(etat);

  gendarme.setLambda(2);

  MyOptimaPriorityTask etage1(&gendarme,&sequence);

  //etage1.hideTaskOnControl(&gendarme);

  MyOptimaPriorityTask globale(&etage1,&resteVisible);

  //globale.hideTaskOnControl(&resteVisible);

  globale.setState(etat);

  //Creation du controleur
  
  VectorXd val = tache.valeur();

  cout << "La fonction de tache \n"<<val<<endl;

  MatrixXd J = tache.jacobien(etat);

  cout <<"Jacobien de la tache\n"<< J <<endl;

  FullPivLU<MatrixXd> lu_decomp(J);

  cout << "The rank of J is " << lu_decomp.rank() << endl;

  cout <<"Etat du systeme\n"<<etat<<endl;

  val = tache.valeur();

  cout << "La fonction de tache \n"<<val<<endl;

  TaskController controleur(&noisyPR2,&globale);

  //controleur.enableSmoothing(false);

  //controleur.setSmoothRatio(2);

  controleur.setTimeLimit(25);

  //Lancement du contructeur

  controleur.run();

  //Fin du programme

  val = tache.valeur();

  cout << "La fonction de tache \n"<<val<<endl;

}
