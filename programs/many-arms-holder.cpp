#include "../vservoing.h"

using namespace Eigen;
using namespace std;

int main(void){

  Camera oeilG;
  IntrinsicCam optique(320,320,320,240); 
  oeilG.setIntrinsicParameters(optique);
  

  double D = 0.3;
  double L = 0.1;
  
  MatrixXd points = MatrixXd::Zero(4,3);

  points << 
    0, L, L,
    0,-L, L,
    0,-L,-L,
    0, L,-L;

  MultiPointFeature cible1(points);

  MultiPointFeature cible2(points);

  cible1.setStrategy(VisualFeature::PINV);

  cible2.setStrategy(VisualFeature::PINV);

  FlyingSystem brasGauche;

  FlyingSystem brasDroite;

  DualArmSystem PR2(&brasGauche,&brasDroite);

  Repere epauleGauche(VectRotation(0,0,0),Vector3d(0, 3*L,0));

  Repere epauleDroite(VectRotation(0,0,0),Vector3d(0,-3*L,0));

  Repere oeilGauche(VectRotation(0,0,0),Vector3d(-D,0,0));

  Repere simple;
  
  double lambda = 0.5;

  ManyArmHolderTask tache;

  tache.addArm(&brasGauche,epauleGauche);

  tache.addArm(&brasDroite,epauleDroite);
  
  tache.addCamera(&oeilG,NULL,oeilGauche,6);

  tache.addVisualTarget(&cible1,&brasGauche,simple,6);

  tache.addVisualTarget(&cible2,&brasDroite,simple,6);

  tache.makeTargetSeen(&cible1,&oeilG);

  tache.makeTargetSeen(&cible2,&oeilG);

  tache.setPeriod(1.0/20);

  tache.setLambda(lambda);

  tache.setMinimumErrorNorm(1e-4);

  VectorXd etat = VectorXd::Zero(12);

  etat << 
    0, 0, 0, 
    D, 0, 0, 
    0, 0, 0, 
    D, 0, 0;

  tache.setState(etat);

  PR2.setState(etat);

  cible1.attachToCamera(&oeilG);

  ConstReference ref1(cible1.getFunction()); 

  cible2.attachToCamera(&oeilG);

  ConstReference ref2(cible2.getFunction()); 

  etat << 
    0, 0, 0, 
    D, 0, 0, 
    0, 0, 0, 
    2*D, 0, 0;

  tache.setState(etat);

  PR2.setState(etat);

  //tache.giveTargetReference(&oeilG,&cible1,&ref1,true);

  tache.giveTargetReference(&oeilG,&cible2,&ref2,true);

  //tache.keepVisualTrace(true);

  VectorXd val = tache.valeur();

  cout << "La fonction de tache \n"<<val<<endl;

  MatrixXd J = tache.jacobien(etat);

  cout <<"Jacobien de la tache\n"<< J <<endl;

  FullPivLU<MatrixXd> lu_decomp(J);

  cout << "The rank of J is " << lu_decomp.rank() << endl;

  cout <<"Etat du systeme\n"<<etat<<endl;

  TaskController controleur(&PR2,&tache);

  //controleur.enableSmoothing(false);

  controleur.setTimeLimit(200);

  controleur.run();

}


