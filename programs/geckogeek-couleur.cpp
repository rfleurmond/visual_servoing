/*
 * Code written by Lya (GeckoGeek.fr)
   and adapted to my case Renliw Fleurmond
 */


#include <vservoing.h>

using namespace Eigen;
using namespace std;

// Step mooving for object min & max
#define STEP_MIN 5
#define STEP_MAX 100 

#define sign(x) ((x) > 0 ? 1 : -1)

 
cv::Mat image;
 
// Position of the object we overlay
cv::Point objectPos(-1, -1);
// Color tracked and our tolerance towards it
int h = 0, s = 0, v = 0, tolerance = 20;//10

/*
 * Transform the image into a two colored image, one color for the color we want to track, another color for the others colors
 * From this image, we get two datas : the number of pixel detected, and the center of gravity of these pixel
 */
cv::Point binarisation(cv::Mat & image, int & nbPixels) {
 
  int x, y;
  
  cv::Scalar couleur;
  
  int sommeX = 0, sommeY = 0;

  // petit ajout personnel
  double mux = 0, muy = 0, ixx = 0, iyy = 0, ixy = 0;
  double /*rho = 0,*/ theta = 0;
  double px = 0, py = 0;
  
  nbPixels = 0;

  int marge = 2;

  int taille = 2*marge +1;
 
  // Create the mask &initialize it to white (no color detected)
  cv::Mat mask(image.size(), image.depth(), 1);
 
  // Create the hsv image
  cv::Mat hsv = image.clone();
  
  cvtColor(image, hsv, CV_BGR2HSV);
 
  // We create the mask
  inRange(hsv, cv::Scalar(h - tolerance -1, s - tolerance, 0),
	  cv::Scalar(h + tolerance -1, s + tolerance, 255), mask);
 
  // Create kernels for the morphological operation
  cv::Mat kernel =  getStructuringElement(CV_SHAPE_ELLIPSE, cv::Size(taille,taille), cv::Point(marge,marge));
  
  // Morphological opening (inverse because we have white pixels on black background)
  dilate(mask, mask, kernel);
  erode(mask, mask, kernel);  
 
  // We go through the mask to look for the tracked object and get its gravity center
  for(x = 0; x < mask.cols; x++) {
    for(y = 0; y < mask.rows; y++) { 

      
      // If its a tracked pixel, count it to the center of gravity's calcul
      if(mask.at<uchar>(y,x) == 255) {
	sommeX += x;
	sommeY += y;
	nbPixels++;
      }
    }
  }

  mux = sommeX * 1.0/nbPixels;
  muy = sommeY * 1.0/nbPixels;

  if(nbPixels>5){
  
    // We go through the mask to look for the tracked object and get its gravity center
    for(x = 0; x < mask.cols; x++) {
      for(y = 0; y < mask.rows; y++) { 
	
	// If its a tracked pixel, count it to the center of gravity's calcul
	if(mask.at<uchar>(y,x) == 255) {
	  px = x-mux;
	  py = y-muy;
	  ixx += px*px;
	  ixy += px*py;
	  iyy += py*py;
	}
      }
    }

    ixx/=nbPixels;
    ixy/=nbPixels;
    iyy/=nbPixels;

    cv::Point I;
    I.x = (int)(mux);
    I.y = (int)(muy);
      
    if(fabs(ixy) > 1.5 && fabs(ixx - iyy) > 1.5) {
      theta = 0.5 * atan(2*ixy/(ixx-iyy));
      //rho = sqrt(mux*mux + muy*muy);
      //rho = mux * cos(theta) + muy * sin(theta);
      cv::Point A,B;
      int l = 800;
      A.x = (int)(I.x - l * sin(theta));
      A.y = (int)(I.y + l * cos(theta));
      B.x = (int)(I.x + l * sin(theta));
      B.y = (int)(I.y - l * cos(theta));
      cv::Scalar blanc = cv::Scalar(255,255,255);
      //cv::Scalar vert = cv::Scalar(0,255,0);
      cv::line(mask,A,I,blanc,1,8);
      cv::line(mask,B,I,blanc ,1,8);
      A.x = A.x + marge;
      A.y = A.y + marge;
      B.x = B.x + marge;
      B.y = B.y + marge;
      cv::line(image,A,I,blanc,1,8);
      cv::line(image,B,I,blanc,1,8);
    
    }
    
    else{
      cv::circle(image, I, 45, CV_RGB(0, 0, 255), 2);
    }

  }

  // Show the result of the mask image
  cv::imshow("GeckoGeek Mask", mask);
 
  // We release the memory of kernels
  kernel.release();
 
  // We release the memory of the mask
  mask.release();
  // We release the memory of the hsv image
  hsv.release();
 
  // If there is no pixel, we return a center outside the image, else we return the center of gravity
  if(nbPixels > 0)
    return cv::Point((int)(mux), (int)(muy));
  else
    return cv::Point(-1, -1);
}
 
/*
 * Add a circle on the video that fellow your colored object
 */
void addObjectToVideo(cv::Mat & image, cv::Point objectNextPos, int nbPixels) {
 
  int objectNextStepX, objectNextStepY;
 
  // Calculate circle next position (if there is enough pixels)
  if (nbPixels > 10) {
 
    // Reset position if no pixel were found
    if (objectPos.x == -1 || objectPos.y == -1) {
      objectPos.x = objectNextPos.x;
      objectPos.y = objectNextPos.y;
    }
 
    // Move step by step the object position to the desired position
    if (abs(objectPos.x - objectNextPos.x) > STEP_MIN) {
      objectNextStepX = max(STEP_MIN, min(STEP_MAX, abs(objectPos.x - objectNextPos.x) / 2));
      objectPos.x += (-1) * sign(objectPos.x - objectNextPos.x) * objectNextStepX;
    }
    if (abs(objectPos.y - objectNextPos.y) > STEP_MIN) {
      objectNextStepY = max(STEP_MIN, min(STEP_MAX, abs(objectPos.y - objectNextPos.y) / 2));
      objectPos.y += (-1) * sign(objectPos.y - objectNextPos.y) * objectNextStepY;
    }
 
    // -1 = object isn't within the camera range
  } else {
 
    objectPos.x = -1;
    objectPos.y = -1;
 
  }
 
  // Draw an object (circle) centered on the calculated center of gravity
  if (nbPixels > 10)             //15 
    cv::circle(image, objectPos, 5, CV_RGB(255, 0, 0), -1);
 
  // We show the image on the window
  cv::imshow("GeckoGeek Color Tracking", image);
 
}
 
/*
 * Get the color of the pixel where the mouse has clicked
 * We put this color as model color (the color we want to tracked)
 */
void getObjectColor(int event, int x, int y, int flags, void *param = NULL) {
 
  // Vars
  cv::Mat hsv;
 
  if(event == CV_EVENT_LBUTTONUP) {
 
    // Get the hsv image
    hsv = image.clone();

    cvtColor(image, hsv, CV_BGR2HSV);
 
    // Get the selected pixel
    cv::Vec3b pixel = hsv.at<cv::Vec3b>(y,x);
 
    // Change the value of the tracked color with the color of the selected pixel
    h = (int)pixel.val[0];
    s = (int)pixel.val[1];
    v = (int)pixel.val[2];
 
    // Release the memory of the hsv image
    hsv.release();
 
  }
 
}

int main(void){

  // Image & hsvImage
  cv::Mat hsv;
  // Video Capture
  CvCapture *capture;
  // Key for keyboard event
  char key;
 
  // Number of tracked pixels
  int nbPixels;
  // Next position of the object we overlay
  CvPoint objectNextPos;
 
  // Initialize the video Capture (200 => CV_CAP_V4L2)
  capture = cvCreateCameraCapture(200);
 
  // Check if the capture is ok
  if (!capture) {
    printf("Can't initialize the video capture.\n");
    return -1;
  }
 
  // Create the windows
  cvNamedWindow("GeckoGeek Color Tracking", CV_WINDOW_AUTOSIZE);
  cvNamedWindow("GeckoGeek Mask", CV_WINDOW_AUTOSIZE);
  cvMoveWindow("GeckoGeek Color Tracking", 0, 100);
  cvMoveWindow("GeckoGeek Mask", 650, 100);
 
  // Mouse event to select the tracked color on the original image
  cvSetMouseCallback("GeckoGeek Color Tracking", getObjectColor);
 
  // While we don't want to quit
  while(key != 'Q' && key != 'q') {
 
    // We get the current image
    image = cvQueryFrame(capture);
 
    // If there is no image, we exit the loop
    if(image.data)
      {

	objectNextPos = binarisation(image, nbPixels);
	addObjectToVideo(image, objectNextPos, nbPixels);
 
      }
    else
      {
	continue;
      }
    // We wait 10 ms
    key = cvWaitKey(10);
 
  }
 
  // Destroy the windows we have created
  cvDestroyWindow("GeckoGeek Color Tracking");
  cvDestroyWindow("GeckoGeek Mask");
 
  // Destroy the capture
  cvReleaseCapture(&capture);
 
  return 0;
 


}
