#include "../algebre.h"

using namespace std;
using namespace Eigen;

//main
int main(void)
{
  polynome p1,p2,p3,p4,p5;
  p1.saisie();
  cout <<"Apres saisie\n";
  //p2.raz();
  cout <<"Apres mise a zero\n";
  p1.affiche(cout);
  cout<<"\n Apres affichage\n";
  cout<<"entrez un second polynôme :\n";
  cin>>p2;
  cout<<"le polynome vaut "<<p2<<"\n";
  if(p1==p2)
    cout<<"ils sont égaux\n";

  cout<<p1<<" plus deux fois "<<p2;
  p5 = 2* p2;
  p3 = p1 + p5;
  cout<<" fait "<<p3<<"\n";
  p3.deriver();
  cout<<"dérivée : "<<p3<<"\n";
  p3.integrer(0);
  cout<<"intégrale : "<<p3<<"\n";
  p4 = p1*p2;
  polyfacteur pf;
  pf.multiplie(p1);
  pf.multiplie(p2);
  cout << "produit: "<<p4<<endl;
  cout << "\nLe produit de facteurs \n";
  cout << pf<<endl;
  cout << "se developpe ainsi:"<<endl;
  polynome p7 = pf.developpement();
  cout << p7 << endl;
  polynome p8;
  cout <<"\n";
  cin>>p8;
  cout<<p8<<endl;
  double racine =0;
  cout <<"Donnez moi une racine? ";
  cin>>racine;
  polyfacteur pf2 = polyfacteur::factoriser(p8,racine);
  cout <<pf2<<endl;

  Matrix3d m;

  m <<1,2,3,4,5,6,7,8,9;

  Vector3d v;

  v<<1,3,5;

  Equation eq;

  eq.setValeurs(m,v);

  cout <<"\nL'equation:\n"<<eq<<endl;

 

}

