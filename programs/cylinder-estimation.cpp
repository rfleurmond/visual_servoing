#include "../vservoing.h"

using namespace Eigen;
using namespace std;


int main(void){

  Camera oeil;

  IntrinsicCam optique(240,240,320,240);

  oeil.setIntrinsicParameters(optique);

  // Create the visual target
  
  double R = 0.8;
  
  Cylinder cible(R);

  // Add noise to the target

  double covP = 4;

  double covT = 1e-3;

  MatrixXd covariance = covP * MatrixXd::Identity(4,4);

  covariance(1,1) =  covT;
  
  covariance(3,3) =  covT;
  
  NoisyFeature cibleN(&cible,covariance);

  // Put the estimator between 

  cout << "!!!!!!!!!!!!!!!!!!   ICI !!!!!!!!!!!!!!!!!!!!!! " <<endl;

  ICylinderEstimator cibleE(&cibleN,&cibleN);

  //cibleE.setRadius(R);

  cibleE.setLogFile("estim-nonlinear.dat");

  cibleE.attachToCamera(&oeil);
  
  // Set different countdowns

  cibleE.setThreshold(400);

  /// Put the features in one 

  FlyingSystem arm;

  Repere idi;

  Repere shoulder(VectRotation(),Vector3d(2,0,0));

  ManyArmHolderTask tache(true);

  tache.addArm(&arm,idi);
  
  tache.addCamera(&oeil,&arm,idi,6);

  tache.addVisualTarget(&cibleE,NULL,shoulder,6);

  tache.makeTargetSeen(&cibleE,&oeil);

  tache.setPeriod(1.0/20);

  tache.setLambda(0.3);

  tache.setLambda(AdaptiveGain(0.3, 0.8));

  tache.setMinimumErrorNorm(0.001 * 2 * sqrt(covP));

  VectorXd etat = VectorXd::Zero(6);

  etat << 
     0, 0, 0, // orientation
       0, 0, 0; // position

  tache.setState(etat);

  arm.setState(etat);

  ConstReference ref(cible.getFunction()); 

  etat << 
    0,  0, 0, 
    -10, 0, 0;

  tache.setState(etat);

  arm.setState(etat);

  tache.giveTargetReference(&oeil,&cibleE,&ref,true);

  //tache.start();

  //tache.keepVisualTrace(true);

  ////////////PREPARATION COMMANDE et ESTIMATION  ////////////////////////

  MatrixXd a(2,6);

  a << 
    0, 0, 0, 0, 1, 0,
    0, 0, 0, 0, 0, 1;

  VectorXd qq = VectorXd::Zero(6);

  QPointTask constraint(qq,a);

  PriorityTask globale(&constraint,&tache);

  cout << " Avant run !" << endl;

  /////////////////////////////////RUN/////////////////////////////////

  TaskController controleur(&arm,&globale);

  controleur.setSmoothRatio(8);

  controleur.setTimeLimit(30);

  controleur.run();
  
  //////////////////////////FIN RUN //////////////////////////////////

  cout << " Apres run !" << endl;
  
}


