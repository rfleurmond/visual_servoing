#include "../vservoing.h"

using namespace Eigen;
using namespace std;


int main(void){

  Camera oeil;

  IntrinsicCam optique(240,240,320,240);
  oeil.setIntrinsicParameters(optique);

  double D = 2;
  double L = 0.8;

  Vector3d p1(0, 0.5*L,0);

  Vector3d p2(0, 0.5*L,L);

  Vector3d p3(0,-0.5*L,0);

  Vector3d p4(0,    -L,L);

  Segment cibleR1(p1,p2);

  Segment cibleR2(p3,p4);

  double covP = 2;

  double covT = 1e-3;

  MatrixXd covariance = covP *MatrixXd::Identity(4,4);

  covariance(1,1) =  covT;
  
  NoisyFeature cible1(&cibleR1,covariance);

  NoisyFeature cible2(&cibleR2,covariance);

  SegmentEstimator cibleE1(&cible1,&cible1);

  SegmentEstimator cibleE2(&cible2,&cible2);

  cibleE1.attachToCamera(&oeil);

  cibleE2.attachToCamera(&oeil);

  cibleE1.setThreshold(300);

  cibleE2.setThreshold(200);

  MultiFeature bigTarget(&cibleE1);

  bigTarget.addFeature(&cibleE2);

  FlyingSystem arm;

  Repere idi;

  Repere shoulder(VectRotation(),Vector3d(D,0,0));

  ManyArmHolderTask tache;

  tache.addArm(&arm,idi);
  
  tache.addCamera(&oeil,&arm,idi,6);

  tache.addVisualTarget(&bigTarget,NULL,shoulder,6);

  tache.makeTargetSeen(&bigTarget,&oeil);

  tache.setPeriod(1.0/20);

  tache.setLambda(0.1);

  tache.setLambda(AdaptiveGain(0.1, 1.5));

  tache.setMinimumErrorNorm(0.002*3*sqrt(covP));

  VectorXd etat = VectorXd::Zero(6);

  etat << 
    0,   0, 0, // orientation
   -2,   0, 0; // position

  tache.setState(etat);

  arm.setState(etat);

  VectorXd s_star = VectorXd::Zero(8);

  s_star.block<4,1>(0,0) =  cibleR1.getFunction();

  s_star.block<4,1>(4,0) =  cibleR2.getFunction();

  ConstReference ref(s_star); 

  tache.giveTargetReference(&oeil,&bigTarget,&ref,true);

  etat << 
    0,   0, 0, 
    1,   0, 0;

  tache.setState(etat);

  arm.setState(etat);

  tache.start();

  //tache.keepVisualTrace(true);

  ////////////PREPARATION COMMANDE et ESTIMATION  ////////////////////////


  cout << " Avant run !" << endl;

  /////////////////////////////////RUN/////////////////////////////////

  TaskController controleur(&arm,&tache);

  controleur.setSmoothRatio(3);

  controleur.run();
  
  //////////////////////////FIN RUN //////////////////////////////////

  cout << " Apres run !" << endl;
  
}


