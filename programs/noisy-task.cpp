#include "../vservoing.h"

using namespace Eigen;
using namespace std;

int main(void){

  Camera oeil;

  int D = 20;
  int L = 5;
  
  MatrixXd points = MatrixXd::Zero(4,3);

  points << 
    D, L, L,
    D,-L, L,
    D,-L,-L,
    D, L,-L;

  MultiPointFeature cible(points);

  cible.attachToCamera(&oeil);

  cible.setStrategy(VisualFeature::PINV);

  double cov = 1e-6;

  MatrixXd covariance = cov *MatrixXd::Identity(8,8);
  
  NoisyFeature cibleB(&cible,covariance);

  FlyingSystem peterpan;

  VisualTask tache(&cibleB, &peterpan);

  tache.setModeFixation(VisualTask::EMBARQUE);

  tache.setPeriod(1.0/20);

  tache.setLambda(0.1);

  tache.setMinimumErrorNorm(1e-4);

  VectorXd etat = VectorXd::Zero(6);

  etat << 0, 0, 0, 0, 0, 0;

  tache.setState(etat);

  peterpan.setState(etat);

  MatrixXd lStar = cible.getInteractionMatrix();

  cible.setReferenceInteractionMatrix(lStar);

  VectorXd s_star = cible.getFunction();

  tache.setReference(s_star);

  etat << 0.1, 0.2, 0.3, 0, 0, 0;

  tache.setState(etat);

  peterpan.setState(etat);

  TaskController controleur(&peterpan,&tache);

  controleur.setTimeLimit(180);

  cout << " Avant run !" << endl;

  controleur.run();

  cout << " Apres run !" << endl;
  
}


