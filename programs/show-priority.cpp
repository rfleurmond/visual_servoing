#include "../taches.h"

using namespace Eigen;

using namespace std;

int main(void){

  double v2 = sqrt(2.0);

  double lambda = 1;

  VectorXd etat = VectorXd::Zero(2);

  QSystem robot(etat,1.0/200);

  VectorXd cibleM = VectorXd::Zero(2);

  cibleM << 0, 5;

  MatrixXd filtre = MatrixXd::Zero(1,2);

  filtre << 0, 1;

  QTask master(cibleM,filtre);

  master.setLambda(lambda);

  master.setState(etat);

  master.setPeriod(1.0/200);


  VectorXd cibleS = VectorXd::Zero(2);

  cibleS << 4,4;

  filtre << v2, v2;

  QTask slave(cibleS, filtre);
  
  slave.setLambda(0.52*lambda);
  
  slave.setState(etat);

  master.setPeriod(1.0/200);

  //EgalityTask dualite(&master,&slave);
  
  //PriorityTask dualite(&master,&slave);

  //OptimaPriorityTask dualite(&master,&slave);

  MyOptimaPriorityTask dualite(&master,&slave);

  cout <<"Valeur de la tache"<<endl;

  cout <<dualite.valeur()<< endl;

  cout <<"Jacobien de la tache:"<< endl;

  cout <<dualite.jacobien(etat) << endl;

  TaskController controleur(&robot,&dualite);

  //controleur.setSysLogFile("priority4.task");

  //controleur.setSmoothRatio(10);

  controleur.enableSmoothing(false);

  controleur.setTimeLimit(30);

  cout << "\n\n Avant run !" << endl;

  controleur.run();

  cout << " Apres run !" << endl;
 

}
