#include "../vservoing.h"

using namespace Eigen;
using namespace std;

int main(void){

  Camera oeil;

  IntrinsicCam optique(240,240,320,240); 

  //oeil.setIntrinsicParameters(optique);
  
  Vector3d p1(10, 2,-2);

  Vector3d p2(10,-2,-2);

  Vector3d p3(10,-2, 2);

  Vector3d p4(10, 2, 2);

  LineFeature lf1(p4,p1);

  LineFeature lf2(p2,p3);

  LineFeature lf3(p3,p4);

  MultiFeature cible(&lf1);

  cible.addFeature(&lf2);

  cible.addFeature(&lf3);

  cible.attachToCamera(&oeil);

  cout<<"Dimension de la cible:"<<cible.getDimensionOutput()<<endl;

  FlyingSystem peterpan;

  VisualTask tache(&cible, &peterpan);

  tache.setModeFixation(VisualTask::EMBARQUE);

  tache.setPeriod(1.0/20);

  tache.setLambda(0.2);

  tache.setMinimumErrorNorm(1e-6);

  VectorXd etat = VectorXd::Zero(6);

  etat << 3*M_PI/4-1, 0, 0, 0, 0, 0;

  tache.setState(etat);

  peterpan.setState(etat);

  MatrixXd lStar = cible.getInteractionMatrix();

  cible.setReferenceInteractionMatrix(lStar);

  VectorXd s_star = cible.getFunction();

  //etat << 0.5, 0.2, 0.3, 7, -2, 0;

  //etat << 1, 0, 0, 0, 0, 0;

  etat << 3*M_PI/4, 0, 0, 0, 0, 0;

  tache.setReference(s_star);

  peterpan.setState(etat);

  tache.setState(etat);

  cout<<"Valeur des indices visuels\n"<<cible.getFunction().transpose()<<endl;

  cout<<"Valeur souhaitee des indices visuels\n"<<s_star.transpose()<<endl;

  cout<<"Valeur de la tache\n"<<tache.valeur().transpose()<<endl;

  MatrixXd L,J, jacobien;

  L =  cible.getInteractionMatrix();

  cout<<"Matrice d'interaction\n"<<L<<endl;

  J = peterpan.getKinematicJacobian(etat);

  cout<<"Jacobien du robot \n"<<J<<endl;

  jacobien = tache.jacobien(etat);

  cout<<"Jacobien de la fonction de tache\n"<<jacobien<<endl;

  TaskController controleur(&peterpan,&tache);

  controleur.setTimeLimit(100);

  cout << " Avant run !" << endl;

  //tache.keepVisualTrace(true);

  controleur.run();

  cout << " Apres run !" << endl;
  
}


