#include "../vservoing.h"
#include <fstream>

using namespace Eigen;
using namespace std;

int main(void){

  Camera oeil;

  IntrinsicCam optique(240,240,320,240);

  oeil.setIntrinsicParameters(optique);

  double D = 6;
  double L = 3;
  
  MatrixXd points = MatrixXd::Zero(4,3);

  points << 
    0, L, L,
    0,-L, L,
    0,-L,-L,
    0, L,-L;

  VectorXd solution(12);
  
  solution << 
    0, L, L,
    0,-L, L,
    0,-L,-L,
    0, L,-L;

  MultiPointFeature cibleR(points);

  double cov = 1e-2;

  MatrixXd covariance = cov *MatrixXd::Identity(8,8);
  
  NoisyFeature cible(&cibleR,covariance);

  cible.attachToCamera(&oeil);

  cible.setStrategy(VisualFeature::PINV);

  MultiPointEstimator cibleE(&cible,&cible);

  cibleE.setLogFile("estim-nonlinear.dat");

  cibleE.attachToCamera(&oeil);

  cibleE.setThreshold(100);

  FlyingSystem neck;

  FlyingSystem arm;

  DualArmSystem PR2(&neck,&arm);

  Repere atlas;

  Repere idi;

  Repere shoulder(VectRotation(),Vector3d(D,0,0));

  ManyArmHolderTask tache;

  tache.addArm(&neck,atlas);

  tache.addArm(&arm,shoulder);
  
  tache.addCamera(&oeil,&neck,idi,6);

  tache.addVisualTarget(&cibleE,&arm,idi,6);

  tache.makeTargetSeen(&cibleE,&oeil);

  tache.setPeriod(1.0/20);

  tache.setLambda(0.5);

  tache.setLambda(AdaptiveGain(0.5,5));

  tache.setMinimumErrorNorm(0.0002*8*sqrt(cov));

  VectorXd etat = VectorXd::Zero(12);

  etat << 
    0.4, 0.6, 0,
    -5,   0, 10,
    0,    0,  0,
    0,    0,  0;

  tache.setState(etat);

  PR2.setState(etat);

  cible.attachToCamera(&oeil);

  VectorXd s_star = cibleR.getFunction();

  ConstReference ref(s_star); 

  tache.giveTargetReference(&oeil,&cibleE,&ref,true);

  etat << 
    0, 0, 0, 
    0, 0, 0, 
    0, 0, 0, 
    0, 0, 0;

  tache.setState(etat);

  PR2.setState(etat);

  //tache.keepVisualTrace(true);

  ////////////PREPARATION COMMANDE et ESTIMATION  ////////////////////////

  cout << " Avant run !" << endl;

  /////////////////////////////////RUN/////////////////////////////////

  tache.start();

  TaskController controleur(&PR2,&tache);

  Matrix3d phi;

  controleur.setSmoothRatio(3);

  controleur.run();
}


