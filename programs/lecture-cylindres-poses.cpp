#include "../vservoing.h"
#include <iostream>
#include <string>
#include <fstream>
#include <limits>
#include <sstream> 

using namespace Eigen;
using namespace std;

int main (int argc, char * argv[]){

  int numberCyl = 1;

  int debut = 0;
  
  if(argc<2){

    std::cout << "Give as argument the absolute or the relative"<<
      "path of the directory which contains the data" <<std::endl;

    return 0;
  }
  
  const string dossier(argv[1]);

  if(argc>=3){

    int val = 0;
    string num(argv[2]);
    std::istringstream istr(num);
    istr >> val;
    std::cout <<" J'ai bien recu comme numero de cylindre # " << val <<endl;
    if(val >0 && val <10){
      numberCyl = val;
    }
    
  }

  if(argc>=4){
    
    int val = 0;
    string num(argv[3]);
    std::istringstream istr(num);
    istr >> val;
    if(val > 0){
      debut = val;
    }
    
  }

  int taille = 8;

  //Definition de certains parametres

  Camera camera;

  //camera.setRegard(2);
 
  IntrinsicCam optique(344.00748,379.09781,327.56131, 235.62077); // Max right forearm
  
  //IntrinsicCam optique(430.67783,430.19755,324.72594, 236.54259); // Max right forearm

  camera.setIntrinsicParameters(optique);

  const int CVperiode = 50;

  CVEcran windows;

  windows.setOptique(optique);

  string titre = "Visualisation et estimation";

  windows.setNom(titre);

  windows.ouvrirFenetre();

  cv::Scalar couleur = Palette::getLastColor();

  cv::Scalar contraste = Palette::getLastColor();

  cv::Scalar tranchant = Palette::getLastColor();

  cv::Scalar voyant = Palette::getLastColor();

  Quaterniond q;

  VectRotation so3;

  Matrix3d rotation;

  rotation <<
    0, 0,-1,
    0, 1, 0,
    1, 0, 0;

  VectRotation changement(rotation);
    
  Vector3d position;

  Repere se3;

  //Repere simple;

  Repere simple(VectRotation(),Vector3d(0.9,-0.1,0.4));

  double x,y,z,w;

  BoundedCylinder cible(0.15,0.05);

  cible.setSituation(simple);

  cible.attachToCamera(&camera);

  StupidTracker monTraceur(taille,&cible);

  monTraceur.attachToCamera(&camera);

  cible.setSituation(simple);

  //MultiPointEstimator estimateur(&cible,&cible);

  BCylinderEstimator estimateur(&cible,&monTraceur);

  estimateur.attachToCamera(&camera);

  estimateur.setSituation(simple);

  estimateur.setLogFile("estim-nonlinear.dat");

  estimateur.setThreshold(2);
  
  bool notEmpty = true;

  double * ligneImage = NULL;

  double * lignePoses = NULL;

  double * ligneClick = NULL;

  const string fichier_repere = dossier + "pose_camera.dat";

  const string fichier_images = dossier + "heure_image.dat";

  ostringstream oss1;
      
  oss1 <<"annotations"<< numberCyl <<"cylindre.dat";

  const string subtitles = dossier + oss1.str();

  string picture_path = "";

  cv::Mat photo;

  vector<double *> dataCamera = loadDAT(fichier_images,2);
  
  vector<double *> dataPose = loadDAT(fichier_repere,8);

  vector<double *> dataClicks = loadDAT(subtitles, taille + 1);

  VectorXd F(taille);

  F = VectorXd::Zero(taille);

  if(dataCamera.size() == 0){
    cerr << "Le fichier contenant la liste d'images n'a pas pu être utilisé" <<endl;
    notEmpty = false;
  }
    

  if(dataClicks.size() == 0){
    cerr << "Le fichier contenant l'évolution des indices visuels n'a pas pu être utilisé" <<endl;
    notEmpty = false;
  }

  if(dataPose.size() == 0){
    cerr << "Le fichier contenant l'évolution des situations de la camera "
	 <<  "n'a pas pu être ouvert !" <<endl;
    notEmpty = false;
  }

  unsigned int countI = 0;

  unsigned int countP = 0;

  int numImage = 0;

  double oldTime = 0.0;

  double tfTime = 0.0;

  double imgTime = 0.0;

  ligneImage = dataCamera[countI];

  imgTime = ligneImage[0];

  oldTime = imgTime;

  MyClock::start();

  while(notEmpty){

    ligneImage = dataCamera[countI];

    ligneClick = dataClicks[countI];

    countI++;

    if(countI >= dataClicks.size()){
      break;
    }

    // Extraire les informations concernant l'image a un instant t

    imgTime = ligneImage[0];

    MyClock::setTime(imgTime-oldTime);

    numImage = static_cast<int>(ligneImage[1]);

    int numero = static_cast<int>(ligneClick[0]);

    for(int i = 0; i < taille; i++){
      F(i) = ligneClick[i+1];
    }

    assert(numero == numImage);

    ostringstream oss;
      
    oss <<"photo"<< numImage <<".png";

    picture_path = dossier + oss.str();

    photo = cv::imread(picture_path, CV_LOAD_IMAGE_COLOR);

    if(!photo.data){
      cout << "impossible de lire l'image : "<< picture_path <<endl;
      break;
    }

    windows.setImage(photo);
    
    if(numero-10000 < debut){
      continue;
    }

    lignePoses = dataPose[countP];

    tfTime = lignePoses[0];

    while(tfTime <= imgTime)
      {
	
	countP++;
	
	if(countP>dataPose.size()){
	  break;
	}
	
	lignePoses = dataPose[countP];

	tfTime = lignePoses[0];

      }

    //cout << tfTime - imgTime << endl;

    if(countP>dataPose.size()){
      break;
    }

    // Extraire les informations concernant la pose relative de la camera

    x = lignePoses[1];

    y = lignePoses[2];

    z = lignePoses[3];
      
    w = lignePoses[4];

    q = Quaterniond(w,x,y,z);

    x = lignePoses[5];

    y = lignePoses[6];

    z = lignePoses[7];

    so3 = VectRotation(q);

    //so3 = so3 * changement;
      
    position = Vector3d(x,y,z);

    se3 = Repere(so3,position);

    camera.setRepere(se3);

    monTraceur.findFeatures(F);

    //monTraceur.track(photo);

    estimateur.useTracker();
    
    monTraceur.draw(&windows,couleur);

    cible.draw(&windows,contraste);

    //cible.drawFeat(F,&windows,voyant);

    estimateur.draw(&windows,tranchant);

    windows.affichage();

    cv::waitKey(CVperiode);
    
  }

  freeDATA(dataCamera);

  freeDATA(dataPose);

  return 0;

}
