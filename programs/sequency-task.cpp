#include "../vservoing.h"

using namespace Eigen;

using namespace std;

int main(void){

  // Definition de l'etat initial

  VectorXd etat1 = VectorXd::Ones(6);

  etat1 <<
    0.2,   0,    0,
     -3,   0,    0;

  // Definition de l'etat de transition

  VectorXd etat2 = VectorXd::Ones(6);

  etat2 <<
   -0.1,   0.2,   0,
     -1,   0.1, 0.1;

  //Definition de l'etat final

  VectorXd etat3 = VectorXd::Zero(6);
  
  //Construction de la scene

  Camera oeil;
  
  int D = 8;
  int L = 5;

  double lambda = 0.5;
  
  MatrixXd points = MatrixXd::Zero(4,3);

  points << 
    D, L, L,
    D,-L, L,
    D,-L,-L,
    D, L,-L;

  MultiPointFeature cible(points);

  cible.attachToCamera(&oeil);

  cible.setStrategy(VisualFeature::PINV);

  // Free flying camera

  FlyingSystem peterpan;

  //Definition de la premiere tache
  
  VisualTask tache1(&cible, &peterpan);

  tache1.setModeFixation(VisualTask::EMBARQUE);

  tache1.setPeriod(1.0/20);

  tache1.setLambda(lambda);

  tache1.setMinimumErrorNorm(1e-3);

  tache1.setState(etat2);

  peterpan.setState(etat2);

  VectorXd s_star = cible.getFunction();

  tache1.setReference(s_star);
  
  // Definition de la seconde tache

  VisualTask tache2(&cible, &peterpan);

  tache2.setModeFixation(VisualTask::EMBARQUE);

  tache2.setPeriod(1.0/20);

  tache2.setLambda(lambda);

  tache2.setMinimumErrorNorm(1e-3);

  tache2.setState(etat3);

  peterpan.setState(etat3);

  s_star = cible.getFunction();

  tache2.setReference(s_star);

  //Defintion de la sequence des taches
  
  SequencyTask realTask(&tache1,&tache2);

  // Retour a l'instant intitial

  realTask.setState(etat1);

  tache1.setState(etat1);

  tache2.setState(etat1);

  peterpan.setState(etat1);

  // Creation du controleur

  TaskController controleur(&peterpan,&realTask);

  controleur.setTimeLimit(300);

  // Lancement du controleur

  controleur.run();

}
