#include "../vservoing.h"
#include <iostream>
#include <string>
#include <fstream>
#include <limits>
#include <sstream> 

using namespace Eigen;
using namespace std;

int main (void){

  Camera camera;

  //IntrinsicCam optique(430.67783,430.19755,324.72594, 236.54259); // Max right forearm
  
  IntrinsicCam optique(418.6835, 419.16346, 309.05563, 224.1891); // Bob  wide left stereo
  
  //IntrinsicCam optique(383.47353, 383.47353, 315.04432, 228.73354); // Bob  wide left stereo

  //IntrinsicCam optique(428.79954, 426.47188, 314.3521, 257.69028); // Bob  right forearm
  
  //IntrinsicCam optique(344.49994, 374.8096, 312.63787, 262.71118); // Bob  right forearm


  //IntrinsicCam optique(422,422,318,224); // Max wide left stereo
  
  //IntrinsicCam optique(772.55,772.55,320.5,240.5); // Gazebo narrow
  
  //IntrinsicCam optique(320,320,320.5,240.5); // Gazebo // wide and forearm
 
  camera.setIntrinsicParameters(optique);

  const int CVperiode = 200;

  CVEcran windows;

  windows.setOptique(optique);

  string titre = "Visualisation et estimation";

  windows.setNom(titre);

  windows.ouvrirFenetre();

  cvNamedWindow("GeckoGeek Mask", CV_WINDOW_AUTOSIZE);
  
  cvMoveWindow("GeckoGeek Mask", 650, 100);
 
  SpecialTracker monTraceur(2,8);

  monTraceur.initTracking(&windows);
  
  monTraceur.attachToCamera(&camera);

  cv::Scalar couleur = Palette::getLastColor();

  /* Modif pour ECC 2016*/

  double r2d = 180/M_PI;

  MatrixXd traduction = MatrixXd::Identity(3,3);
  
  traduction(1,1) = r2d;

  string fichier_feat = "visible_features.dat";

  std::ofstream printer;
  
  printer.open(fichier_feat.c_str());

  VectorXd tempoF;

  /* Modif pour ECC 2016  */

  double L = 0.1;
  
  Vector3d p0(0, 0, L);

  Vector3d p1(0, 0, 0);
  
  Quaterniond q;

  VectRotation so3;

  Vector3d position;

  Repere se3;

  double x,y,z,w;

  LineWithK cible(p0,p1,p0);

  cible.setSituation(se3);

  cible.attachToCamera(&camera);

  LineKEstimator estimateur(&cible,&monTraceur);

  //LineKEstimator estimateur(&cible,&cible);

  estimateur.attachToCamera(&camera);

  estimateur.setSituation(se3);

  estimateur.setLogFile("estim-nonlinear.dat");

  estimateur.setThreshold(1);
  
  const string dossier = "../../Data/exp8/";

  bool notEmpty = true;

  double * ligneImage = NULL;

  double * lignePoses = NULL;

  const string fichier_repere = dossier + "pose_camera.dat";

  const string fichier_images = dossier + "heure_image.dat";

  string picture_path = "";

  cv::Mat photo;

  vector<double *> dataCamera = loadDAT(fichier_images,2);
  
  vector<double *> dataPose = loadDAT(fichier_repere,8);

  //vector<double * >::iterator itP,itI;
  
  if(dataCamera.size() == 0 || dataPose.size() == 0){
    notEmpty = false;
  }

  unsigned int countI = 0;

  unsigned int countP = 0;

  int numImage = 0;

  double oldTime = 0.0;

  double tfTime = 0.0;

  double imgTime = 0.0;

  MyClock::start();

  ligneImage = dataCamera[countI];

  imgTime = ligneImage[0];

  oldTime = imgTime;


  while(notEmpty){

    ligneImage = dataCamera[countI];

    countI++;

    if(countI > dataCamera.size()){
      break;
    }

    // Extraire les informations concernant l'image a un instant t

    imgTime = ligneImage[0];

    MyClock::setTime(imgTime-oldTime);

    numImage = static_cast<int>(ligneImage[1]);

    ostringstream oss;
      
    oss <<"photo"<< numImage <<".png";

    picture_path = dossier + oss.str();

    photo = cv::imread(picture_path, CV_LOAD_IMAGE_COLOR);

    if(!photo.data){
      cout << "impossible de lire l'image : "<< picture_path <<endl;
      break;
    }

    windows.setImage(photo);

    monTraceur.photo = photo;

    lignePoses = dataPose[countP];

    tfTime = lignePoses[0];

    while(tfTime < imgTime)
      {
	
	countP++;
	
	if(countP>dataPose.size()){
	  break;
	}
	
	lignePoses = dataPose[countP];

	tfTime = lignePoses[0];

      }

    //cout << tfTime - imgTime << endl;

    if(countP>dataPose.size()){
      break;
    }

    // Extraire les informations concernant la pose relative de la camera

    x = lignePoses[1];

    y = lignePoses[2];

    z = lignePoses[3];
      
    w = lignePoses[4];

    q = Quaterniond(w,x,y,z);

    x = lignePoses[5];

    y = lignePoses[6];

    z = lignePoses[7];

    so3 = VectRotation(q);
      
    position = Vector3d(x,y,z);

    se3 = Repere(so3,position);

    camera.setRepere(se3);

    while(monTraceur.isRunning()==false){

      windows.affichage();

      cv::waitKey(CVperiode);
    }
    
    monTraceur.track(photo);

    //if(MyClock::getTime()>5)

    estimateur.useTracker();
    
    monTraceur.draw(&windows,couleur);

    estimateur.draw(&windows,couleur);

    /* Modif pour ECC 2016*/

    if(printer){
      
      printer << MyClock::getTime();

      tempoF = traduction * monTraceur.getFunction();

      printer << " " << tempoF.transpose();

      tempoF = traduction * estimateur.getFunction();

      printer << " " << tempoF.transpose();

      if(monTraceur.isLost())
	
	printer << " 0";

      else

	printer << " 100";


      tempoF = traduction * cible.getFunction();

      printer << " " << tempoF.transpose();

      printer << endl;

    }

    /* Modif pour ECC 2016*/

    windows.affichage();

    cv::waitKey(CVperiode);
    
  }

  cvDestroyWindow("GeckoGeek Mask");
 
  freeDATA(dataCamera);

  freeDATA(dataPose);

  /* Modif pour ECC 2016*/

  printer.close();

}
