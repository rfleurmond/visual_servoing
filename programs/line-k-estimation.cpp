#include "../vservoing.h"

using namespace Eigen;
using namespace std;


int main(void){

  Camera oeil;

  IntrinsicCam optique(240,240,320,240);
  
  oeil.setIntrinsicParameters(optique);

  // Create the visual target
  
  Vector3d p1(10, 2,-2);

  Vector3d p2(10,-2,-2);

  Vector3d p3(10,-2, 2);

  Vector3d p4(10, 2, 2);

  LineWithK indice1(p1,p4,p4);

  LineWithK indice2(p2,p1,p2);

  // Add noise to the target

  double covP = 5;

  double covT = 1e-3;

  MatrixXd covariance = covP * MatrixXd::Identity(3,3);

  covariance(1,1) =  covT;
  
  NoisyFeature cible1(&indice1,covariance);

  NoisyFeature cible2(&indice2,covariance);

  // Put the estimator between 

  LineKEstimator cibleE1(&cible1,&cible1);

  LineKEstimator cibleE2(&cible2,&cible2);

  cibleE2.setLogFile("estim-nonlinear.dat");

  cibleE1.attachToCamera(&oeil);

  cibleE2.attachToCamera(&oeil);

  
  // Set different countdowns

  cibleE1.setThreshold(300);

  cibleE2.setThreshold(300);

  /// Put the features in one 

  MultiFeature bigTarget(&cibleE1);

  bigTarget.addFeature(&cibleE2);
  
  bigTarget.attachToCamera(&oeil);


  FlyingSystem arm;

  Repere idi;

  Repere shoulder(VectRotation(),Vector3d(2,0,0));

  ManyArmHolderTask tache;

  tache.addArm(&arm,idi);
  
  tache.addCamera(&oeil,&arm,idi,6);

  tache.addVisualTarget(&bigTarget,NULL,shoulder,6);

  tache.makeTargetSeen(&bigTarget,&oeil);

  tache.setPeriod(1.0/20);

  tache.setLambda(0.8);

  //tache.setLambda(AdaptiveGain(0.8, 20));

  tache.setMinimumErrorNorm(0.001 * 6* sqrt(covP));

  VectorXd etat = VectorXd::Zero(6);

  etat << 
     0.1, 0, 0, // orientation
       0, 0, 0; // position

  tache.setState(etat);

  arm.setState(etat);

  VectorXd s_star = VectorXd::Zero(6);

  s_star.block<3,1>(0,0) =  indice1.getFunction();

  s_star.block<3,1>(3,0) =  indice2.getFunction();

  ConstReference ref(s_star); 

  tache.giveTargetReference(&oeil,&bigTarget,&ref,true);

  etat << 
      0.6,  0, 0, 
      -10, -5, 5;

  tache.setState(etat);

  arm.setState(etat);

  tache.start();

  //tache.keepVisualTrace(true);

  ////////////PREPARATION COMMANDE et ESTIMATION  ////////////////////////


  cout << " Avant run !" << endl;

  /////////////////////////////////RUN/////////////////////////////////

  TaskController controleur(&arm,&tache);

  controleur.setSmoothRatio(3);

  controleur.setTimeLimit(50);

  controleur.run();
  
  //////////////////////////FIN RUN //////////////////////////////////

  cout << " Apres run !" << endl;
  
}


