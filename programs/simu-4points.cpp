#include "../vservoing.h"

using namespace Eigen;
using namespace std;

int main(void){

  Camera oeil;

  IntrinsicCam optique(543.547,544.972,304,238); 

  //IntrinsicCam optique(240,240,320,240); 

  oeil.setIntrinsicParameters(optique);

  int D = 20;
  int L = 5;
  
  MatrixXd points = MatrixXd::Zero(4,3);

  points << 
    D, L, L,
    D,-L, L,
    D,-L,-L,
    D, L,-L;

  MultiPointFeature cible(points);

  cible.attachToCamera(&oeil);

  cible.setStrategy(VisualFeature::PINV);

  FlyingSystem peterpan;

  VisualTask tache(&cible, &peterpan);

  tache.setModeFixation(VisualTask::EMBARQUE);

  tache.setPeriod(1.0/20);

  tache.setLambda(0.5);

  //tache.setLambda(AdaptiveGain(0.5,10));

  tache.setMinimumErrorNorm(1e-8);

  VectorXd etat = VectorXd::Zero(6);

  etat << 0, 0, 0, 0, 0, 0;

  tache.setState(etat);

  peterpan.setState(etat);

  MatrixXd lStar = cible.getInteractionMatrix();

  cible.setReferenceInteractionMatrix(lStar);

  VectorXd s_star = cible.getFunction();

  tache.setReference(s_star);

  etat << 0.5, 0.1, 0.1, -10, 0, 0;

  //etat << 0, 0, 0, -20, 0, 0;

  //etat << 1, 0, 0, -3, 0, 0;

  tache.setState(etat);

  peterpan.setState(etat);

  tache.keepVisualTrace(true);

  tache.start();

  TaskController controleur(&peterpan,&tache);

  controleur.setTimeLimit(15);

  controleur.enableSmoothing(false);

  cout << " Avant run !" << endl;

  controleur.run();

  cout << " Apres run !" << endl;
  
}


