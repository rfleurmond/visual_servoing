#include "../taches.h"

using namespace Eigen;

using namespace std;

int main(void){

  VectorXd etat = VectorXd::Zero(2);

  etat << 0,0;
  
  QSystem robot(etat,1.0/100);

  VectorXd buteeMax = VectorXd::Ones(2);

  buteeMax << 10,10;

  VectorXd buteeMin = -10*VectorXd::Ones(2);
  
  QBoundedTask gendarme(buteeMin,buteeMax);

  gendarme.setPeriod(0.01);

  gendarme.setState(etat);

  gendarme.setLambda(2);

  gendarme.setAlpha(5);

  gendarme.setMax(10);

  gendarme.setCoeffMax(0);

  gendarme.setCoeffMin(0.05);

  MatrixXd selection(1,2);
  
  selection << 1, 0;

  VectorXd vitesse(1);

  vitesse << 0.2;

  //QPointTask derive(vitesse,selection);

  VectorXd cible = VectorXd::Zero(2);
  
  cible << 8,7.9;

  QTask1D derive(cible);

  derive.setLambda(0.2);

  derive.setPeriod(0.01);

  derive.setState(etat);

  derive.setLambda(AdaptiveGain(0.2,10));

  derive.start();

  VectorXd coude = 6 *VectorXd::Ones(2);
  
  AvoidTask singularite(coude,MatrixXd::Identity(2,2),20, 0.93198120356);
 
  singularite.setPeriod(0.01);

  singularite.setState(etat);

  singularite.setLambda(1);

  MyOptimaPriorityTask premier(&singularite,&derive);

  MyOptimaPriorityTask deuxieme(&gendarme,&premier);

  cout <<"Valeur de la tache"<<endl;

  cout <<deuxieme.valeur()<< endl;

  cout <<"Jacobien de la tache:"<< endl;

  cout <<deuxieme.jacobien(etat) << endl;

  TaskController controleur(&robot,&deuxieme);

  controleur.setSmoothRatio(3);

  //controleur.enableSmoothing(false);

  controleur.setTimeLimit(50);

  cout << "\n\n Avant run !" << endl;

  controleur.run();

  cout << " Apres run !" << endl;
 
}
