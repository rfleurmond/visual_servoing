#include "../vservoing.h"

using namespace Eigen;
using namespace std;

int main(void){

  //Definition de certains parametres

  
  double lengthCap = 0.075;
  double lengthPen = 0.1;
  double radius = 0.01;
  

  //Creation des caméras

  //IntrinsicCam narrowStereo(772.55,772.55,320.5,240.5);
  IntrinsicCam narrowStereo(418.6835, 419.16346, 309.05563, 224.1891); 
  IntrinsicCam forearmCam(320,320,320.5,240.5);

  Camera camHead, camArm;
  

  camHead.setIntrinsicParameters(narrowStereo);
  camArm.setIntrinsicParameters(forearmCam);
  
  //Definition des indices visuels

  BoundedCylinder cap(lengthCap, radius);

  BoundedCylinder pen(lengthPen, radius);

  double ratio = 1;

  double covRho = ratio * 4;

  double d2r = M_PI/180;

  double r2d = 360/M_PI;

  double covTheta = ratio * d2r * d2r;

  double minimum = 1e-2;

  MatrixXd covariance = covRho * MatrixXd::Identity(8,8);

  covariance(1,1) = covariance(5,5) = covTheta;
    
  NoisyFeature noisyCap(&cap,covariance);

  NoisyFeature noisyPen(&pen,covariance);

  BCylinderEstimator capEstimator(&cap,&noisyCap);

  BCylinderEstimator penEstimator(&pen,&noisyPen);

  //capEstimator.setRadius(radius);

  //penEstimator.setRadius(radius);

  capEstimator.setLogFile("estimation-cap.dat");//estimation-cap.dat

  capEstimator.setDeadline(0.5);

  penEstimator.setLogFile("estimation-pen.dat");

  penEstimator.setDeadline(0.5);


  //Gestion des bras du robot

  //Creation des modeles vides et des bras

  GeoRobot brasGauche,brasDroite, couPR2;

  //Chargement du modele geometrique des bras

  loadSE3Robot("../robot_models/pr2_gauche.ser",brasGauche);

  loadSE3Robot("../robot_models/pr2_droite.ser",brasDroite);

  loadSE3Robot("../robot_models/pr2_head.ser",couPR2);

  //Creation des modeles cinematiques des bras

  MyKineModel modeleGauche(&brasGauche);

  MyKineModel modeleDroite(&brasDroite);

  MyKineModel modeleTete(&couPR2);

  //modeleTete.addFrozenJoint(1);
 
  //Creation de l'interface TaskSystem pour le controleur

  RobotSystem sysGauche(&brasGauche);

  RobotSystem sysDroite(&brasDroite);

  RobotSystem sysTete(&couPR2);

  sysDroite.setPeriod(1.0/100);

  sysGauche.setPeriod(1.0/100);
 
  sysTete.setPeriod(1.0/100);

  //Couplage des trois systemes

  DualArmSystem arms(&sysGauche,&sysDroite);

  DualArmSystem PR2(&arms,&sysTete);

  //Creation de la tache visuelle principale

  ManyArmHolderTask tache;

  // Defintion des reperes

  Repere epauleGauche(VectRotation(0,0,0),Vector3d(0,  0.188, 0));
  
  Repere epauleDroite(VectRotation(0,0,0),Vector3d(0, -0.188, 0));

  Repere priseGauche(VectRotation(0,0, 0),Vector3d(0.17, 0, -lengthCap/2));
  
  Repere priseDroite(VectRotation(0,0, 0),Vector3d(0.17, 0, 0));

  Repere narrowLStereo(VectRotation(0,0,0),Vector3d(0.068, 0.06, 0.115));

  Repere narrowRStereo(VectRotation(0,0,0),Vector3d(0.068, -0.03, 0.115));

  Repere wideLStereo(VectRotation(0,0,0),Vector3d(0.068, 0.03, 0.115));

  Repere wideRStereo(VectRotation(0,0,0),Vector3d(0.068, -0.06, 0.115));

  Repere forearmRCam(VectRotation(3.018, -0.872524,0), Vector3d(0.135-0.321, 0.044, 0));

  Repere identite;

  // Ajout des deux bras et de la tete

  tache.addArm(&modeleGauche,epauleGauche);

  tache.addArm(&modeleDroite,epauleDroite);

  tache.addArm(&modeleTete, identite);

  // Defintion des parametres de la tache

  tache.setPeriod(1.0/20);

  double lambda = 0.2;

  tache.setLambda(lambda);

  tache.setMinimumErrorNorm(1e-5*minimum);

  //Placement des caméras

  tache.addCamera(&camHead,&modeleTete,narrowLStereo,2);

  tache.addCamera(&camArm,&modeleDroite,forearmRCam,5);

  // Placement des indices visuels

  tache.addVisualTarget(&capEstimator,&modeleGauche,priseGauche,7);

  tache.addVisualTarget(&penEstimator,&modeleDroite,priseDroite,7);

  // Quels indices visuels percoivent les cameras?

  tache.makeTargetSeen(&capEstimator,&camArm);

  tache.makeTargetSeen(&penEstimator,&camArm);

  tache.makeTargetSeen(&capEstimator,&camHead);

  tache.makeTargetSeen(&penEstimator,&camHead);

  // Quelle est la reference des indices visuels par camera?

  /*
  
  penEstimator.attachToCamera(&camHead);

  OtherReference ref1(&penEstimator);

  tache.giveTargetReference(&camHead,&capEstimator,&ref1,true);

  */
  
  penEstimator.attachToCamera(&camArm);

  OtherReference ref2(&penEstimator);

  tache.giveTargetReference(&camArm,&capEstimator,&ref2,true);

  // Constante arbitraire pour le moment
  
  CombinaisonFeature CF(&capEstimator,&penEstimator);

  MatrixXd C1 = MatrixXd::Zero(2,8);

  C1 << 
    0,0,1,0,0,0,0,0,
    0,0,0,0,0,0,1,0;
  
  MatrixXd C2 = MatrixXd::Zero(2,8);

  C2 << 
    0,0, 0,-1,  0,0,0, 0,
    0,0, 0, 0,  0,0,0,-1;
  
  VectorXd K(2);
  
  K << 30, 0;

  CF.setMatrices(C1,C2,K);

  tache.addOtherTask(&camArm,&CF);

  /// Ajout de contraintes sur les indices visuels

  ConstraintFeatures capConstraint;

  ConstraintFeatures penConstraint;

  VectorXd sMin = VectorXd::Ones(6);

  VectorXd sMax = VectorXd::Ones(6);

  MatrixXd acti = MatrixXd::Zero(6,8);
  
  sMin = -240.00/sqrt(2) * sMin;

  sMax << 240.00/sqrt(2) * sMax;

  acti.block<3,4>(0,0) << 
    1, 0, 0, 0,
    0, 0, 1, 0,
    0, 0, 0, 1;

  acti.block<3,4>(3,4) = acti.block<3,4>(0,0);

  double coefMax = -0.4;
  double coefMin = 0.05;
  double alpha = 1e-3;
  double max = 50;


  capConstraint.setCoeffMax(coefMax);
  capConstraint.setCoeffMin(coefMin);
  capConstraint.setAlpha(alpha);
  capConstraint.setMax(max);
  capConstraint.addFeatures(&capEstimator,sMin,sMax,acti);
  
  penConstraint.setCoeffMax(coefMax);
  penConstraint.setCoeffMin(coefMin);
  penConstraint.setAlpha(alpha);
  penConstraint.setMax(max);
  penConstraint.addFeatures(&penEstimator,sMin,sMax,acti);
  

  tache.addOtherTask(&camHead,&capConstraint);
  tache.addOtherTask(&camHead,&penConstraint);
  
  tache.addOtherTask(&camArm,&capConstraint);
  tache.addOtherTask(&camArm,&penConstraint);
  
  // Etat initial
  
  VectorXd etat = VectorXd::Zero(16);

  //*
  etat << 
    M_PI/4,  0.1,  M_PI/2,  -M_PI/2, -M_PI/2,  -0.05,  0,
    -M_PI/4, -0.1, -M_PI/2,  -M_PI/2,  M_PI/2,  -0.05,  0,
    0,   0.7;
  //*/

  tache.setState(etat);

  PR2.setState(etat);

  cout << "Dimension de la tâche globale = "<< tache.getDimOutput()<<endl;
  
  //Defintion de la premiere tâche a enchainer

  // alignement des deux axes

  MatrixXd selection1 = MatrixXd::Zero(4,34);

  selection1.block<2,2>(0,0) = MatrixXd::Identity(2,2);

  selection1(1,1) = r2d;
  
  selection1.block<2,2>(2,4) = MatrixXd::Identity(2,2);

  selection1(3,5) = r2d;
    
  PartTask alignement(selection1,&tache);

  alignement.setMinimumErrorNorm(minimum);

  // Distance à conserver

  MatrixXd selection2 = MatrixXd::Zero(1,34);

  selection2(0,8) = 1;

  PartTask distance(selection2,&tache);

  distance.setMinimumErrorNorm(1);

  EgalityTask approche(&alignement, &distance);

  approche.setMinimumErrorNorm(minimum);

  //Defintion de la deuxieme tache a enchainer
  
  MatrixXd selection3 = MatrixXd::Zero(1,34);

  selection3(0,9) = 1;

  PartTask translation(selection3,&tache);

  translation.setMinimumErrorNorm(1);

  EgalityTask assemblage(&alignement,&translation);

  assemblage.setMinimumErrorNorm(minimum);

  // Création de la tache spécifique à la contrainte de visibilité

  MatrixXd selection4 = MatrixXd::Zero(24,34);

  selection4.block<24,24>(0,10)  = MatrixXd::Identity(24,24);
  
  PartTask resteVisible(selection4,&tache);

  resteVisible.setMinimumErrorNorm(5);

  resteVisible.setLambda(lambda);


  // Ecriture de la priorite entre les deux taches


  //Definition de la sequence de tâches
  
  SequencyTask sequence(&distance,&approche);

  sequence.addTaskInStack(&assemblage);

  //*

  distance.setLambda(AdaptiveGain(lambda,3*lambda));

  alignement.setLambda(AdaptiveGain(lambda,3*lambda));

  translation.setLambda(AdaptiveGain(lambda,3*lambda));
  
  //*/

  //Gestion des butees

  VectorXd buteeMin(16);

  VectorXd buteeMax(16);

  buteeMin.topRows(7) = brasGauche.getMinimalQ();
  buteeMax.topRows(7) = brasGauche.getMaximalQ();
  
  buteeMin.block<7,1>(7,0) = brasDroite.getMinimalQ();
  buteeMax.block<7,1>(7,0) = brasDroite.getMaximalQ();

  buteeMin.block<2,1>(14,0) = couPR2.getMinimalQ();
  buteeMax.block<2,1>(14,0) = couPR2.getMaximalQ();

  MatrixXd selection5 = MatrixXd::Zero(12,16);

  selection5.block<4,4>(0,0) = MatrixXd::Identity(4,4);
  selection5.block<4,4>(5,7) = MatrixXd::Identity(4,4);
  selection5(4,5)  = 1;
  selection5(9,12) = 1;
  selection5.block<2,2>(10,14) = MatrixXd::Identity(2,2);
  

  buteeMin = selection5*buteeMin;
  buteeMax = selection5*buteeMax;

  QBoundedTask gendarme(buteeMin,buteeMax,selection5);

  gendarme.setCoeffMax(-0.01);

  gendarme.setCoeffMin(0.05);

  gendarme.setAlpha(2);

  gendarme.setMax(10);
  
  gendarme.setState(etat);

  gendarme.setLambda(2*lambda);

  MyOptimaPriorityTask etage1(&gendarme,&sequence);

  etage1.setState(etat);

  etage1.hideTaskOnControl(&gendarme);

  MyOptimaPriorityTask globale(&etage1,&resteVisible);

  globale.setState(etat);

  globale.hideTaskOnControl(&resteVisible);

  //Creation du controleur
  
  double cov = 6.4e-8;
  
  covariance = cov * MatrixXd::Identity(16,16);

  NoisySystem noisyPR2(&PR2,covariance);

  VectorXd val = tache.valeur();

  cout << "La fonction de tache \n"<<val<<endl;

  MatrixXd J = tache.jacobien(etat);

  cout <<"Jacobien de la tache\n"<< J <<endl;

  FullPivLU<MatrixXd> lu_decomp(J);

  cout << "The rank of J is " << lu_decomp.rank() << endl;

  cout <<"Etat du systeme\n"<<etat<<endl;

  TaskController controleur(&PR2,&globale);

  //controleur.enableSmoothing(false);

  //controleur.setSmoothRatio(3);

  controleur.setTimeLimit(50.0);

  //Lancement du contructeur

  controleur.run();

  //Fin du programme
  
}
