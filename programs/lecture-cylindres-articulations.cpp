#include "../vservoing.h"
#include <iostream>
#include <string>
#include <fstream>
#include <limits>
#include <sstream> 

using namespace Eigen;
using namespace std;


void readJointValues(ifstream & reader, double & temps, Eigen::VectorXd & q){

  int i = 0;

  double poubelle;

  while(!reader.eof()){
      
    if(i==0){
      reader >> temps;
    }
    else if(i <= 7){
      reader >> poubelle;
    }
    else if(i < 15){
      reader >> q(i-8);
    }
    else{
      reader >> poubelle;
    }

    i++;
    if(i==17){
      break;
    }
  }
 
}

int main (int argc, char * argv[]){

  int numberCyl = 1;

  int debut = 0;

  if(argc<2){

    std::cout << "Give as argument the absolute or the relative"<<
      "path of the directory which contains the data" <<std::endl;

    return 0;
  }

  const string dossier(argv[1]);

  if(argc>=3){

    int val = 0;
    string num(argv[2]);
    std::istringstream istr(num);
    istr >> val;
    std::cout <<" J'ai bien recu comme numero de droite " << val <<endl;
    if(val >0 && val <10){
      numberCyl = val;
    }
    
  }

  if(argc>=4){
    
    int val = 0;
    string num(argv[3]);
    std::istringstream istr(num);
    istr >> val;
    if(val > 0){
      debut = val;
    }
    
  }


  //Definition de certains parametres

  int taille = 8;

  Camera rightCam;

  IntrinsicCam optique(344.00748,379.09781,327.56131, 235.62077); // Max right forearm
  
  rightCam.setIntrinsicParameters(optique);

  // BRAS DROIT

  GeoRobot brasDroit;

  MyKineModel modeleDroit(&brasDroit);

  loadSE3Robot("../robot_models/pr2_droite.ser",brasDroit);

  RobotSystem sysDroit(&brasDroit);

  sysDroit.setPeriod(1.0/100);

  ManyArmHolderTask tache;

  Repere epauleDroite(VectRotation(0,0,0),Vector3d(0, -0.188, 0));

  Repere camArmDroite(VectRotation(3.018, -0.872524,0),
		   Vector3d(0.135-0.321, 0.044, 0));
  
  tache.addArm(&modeleDroit,epauleDroite);
  
  tache.addCamera(&rightCam,&modeleDroit,camArmDroite,5);

  // FIN BRAS DROIT
  
  const int CVperiode = 50;

  CVEcran windows;

  windows.setOptique(optique);

  string titre = "Visualisation et estimation";

  windows.setNom(titre);

  windows.ouvrirFenetre();

  cv::Scalar couleur = Palette::getLastColor();

  cv::Scalar contraste = Palette::getLastColor();

  cv::Scalar tranchant = Palette::getLastColor();

  cv::Scalar voyant = Palette::getLastColor();

  //Repere simple;

  Repere simple(VectRotation(),Vector3d(0.9,-0.1,0.4));

  BoundedCylinder cible(0.15,0.05);

  cible.setSituation(simple);

  cible.setSituation(simple);

  cible.attachToCamera(&rightCam);

  StupidTracker monTraceur(taille, &cible);

  monTraceur.attachToCamera(&rightCam);

  cible.setSituation(simple);

  //MultiPointEstimator estimateur(&cible,&cible);

  BCylinderEstimator estimateur(&cible,&monTraceur);

  estimateur.attachToCamera(&rightCam);

  estimateur.setSituation(simple);

  tache.addVisualTarget(&estimateur,NULL,simple,6);

  ConstReference reference(VectorXd::Zero(8));

  tache.giveTargetReference(&rightCam,&estimateur,&reference,true);

  estimateur.setLogFile("estim-nonlinear.dat");

  estimateur.setThreshold(2);
  
  bool notEmpty = true;

  double * ligneImage = NULL;

  double * lignePoses = NULL;

  double * ligneClick = NULL;

  const string fichier_joints = dossier + "pr2-joints.dat";

  const string fichier_images = dossier + "heure_image.dat";

  ostringstream oss1;
      
  oss1 <<"annotations"<< numberCyl <<"cylindre.dat";

  const string subtitles = dossier + oss1.str();

  string picture_path = "";

  cv::Mat photo;

  vector<double *> dataCamera = loadDAT(fichier_images,2);
  
  vector<double *> dataClicks = loadDAT(subtitles, taille + 1);

  ifstream fileJoints(fichier_joints.c_str(), ios::in);

  VectorXd F(taille);

  VectorXd Q(7);

  F = VectorXd::Zero(taille);

  if(dataCamera.size() == 0){
    cerr << "Le fichier contenant la liste d'images n'a pas pu être utilisé" <<endl;
    notEmpty = false;
  }
    

  if(dataClicks.size() == 0){
    cerr << "Le fichier contenant l'évolution des indices visuels n'a pas pu être utilisé" <<endl;
    notEmpty = false;
  }

  if(!fileJoints){
    cerr << "Le fichier contenant l'évolution de coordonnées articulaires "
	 <<  "n'a pas pu être ouvert !" <<endl;
    notEmpty = false;
  }

  unsigned int countI = 0;

  int numImage = 0;

  double oldTime = 0.0;

  double tfTime = 0.0;

  double imgTime = 0.0;

  ligneImage = dataCamera[countI];

  imgTime = ligneImage[0];

  oldTime = imgTime;

  MyClock::start();

  while(notEmpty){

    ligneImage = dataCamera[countI];

    ligneClick = dataClicks[countI];

    countI++;

    if(countI >= dataClicks.size()){
      cout << "Plus d'image que de clics ?" <<endl;
      break;
    }

    // Extraire les informations concernant l'image a un instant t

    imgTime = ligneImage[0];

    MyClock::setTime(imgTime-oldTime);

    numImage = static_cast<int>(ligneImage[1]);

    int numero = static_cast<int>(ligneClick[0]);

    for(int i = 0; i < taille; i++){
      F(i) = ligneClick[i+1];
    }

    assert(numero == numImage);

    if(numero-10000 < debut){
      continue;
    }


    ostringstream oss;
      
    oss <<"photo"<< numImage <<".png";

    picture_path = dossier + oss.str();

    photo = cv::imread(picture_path, CV_LOAD_IMAGE_COLOR);

    if(!photo.data){
      cout << "impossible de lire l'image : "<< picture_path <<endl;
      break;
    }

    windows.setImage(photo);

    
    readJointValues(fileJoints,tfTime,Q);
  
    while(tfTime <= imgTime){
	
      readJointValues(fileJoints,tfTime,Q);
	
    }

    if(fileJoints.eof()){
      cout << " Fin du fichier" <<endl;
      break;
    }

    // Extraire les informations concernant la pose relative de la camera

    tache.setState(Q);

    sysDroit.setState(Q);

    monTraceur.findFeatures(F);

    //if(numero >= 10180)
    {
      estimateur.useTracker();
    }
   
    monTraceur.draw(&windows,couleur);

    cible.draw(&windows,contraste);

    //cible.drawFeat(F,&windows,voyant);

    estimateur.draw(&windows,tranchant);

    windows.affichage();

    cv::waitKey(CVperiode);
    
  }

  freeDATA(dataCamera);

  freeDATA(dataClicks);

  if(fileJoints){
    fileJoints.close();
  }

  return 0;

}
