#include "../vservoing.h"

using namespace Eigen;
using namespace std;

int main(void){

  Camera oeil;

  //oeil.setConstantes(240,240,0,0);

  //oeil.setFenetre(640,480);
  
  int D = 4;
  int L = 1;
  
  MatrixXd points = MatrixXd::Zero(4,3);

  points << 
    0, L, L,
    0, L,-L,
    0,-L,-L,
    0,-L, L;

  MultiPointFeature cible(points);

  cible.attachToCamera(&oeil);

  cible.setStrategy(VisualFeature::MIXE);

  FlyingSystem peterpan;

  VisualTask tache(&cible, &peterpan);

  tache.setModeFixation(VisualTask::DEPORTEE);

  Repere trepied(VectRotation(),Vector3d(-D,0,0));

  cout<<trepied<<endl;

  tache.setBaseCamera(trepied);

  tache.setPeriod(1.0/20);

  tache.setLambda(3);

  tache.setMinimumErrorNorm(1e-10);

  VectorXd etat = VectorXd::Zero(6);

  etat << 0, 0, 0, 0, 0, 0;

  tache.setState(etat);

  peterpan.setState(etat);

  MatrixXd lStar = cible.getInteractionMatrix();

  cible.setReferenceInteractionMatrix(lStar);

  VectorXd s_star = cible.getFunction();

  tache.setReference(s_star);

  etat << 0.1, 0, 0.1, 0, 0, 0;

  tache.setState(etat);

  peterpan.setState(etat);

  TaskController controleur(&peterpan,&tache);

  controleur.setTimeLimit(50);

  cout << " Avant run !" << endl;

  controleur.run();

  cout << " Apres run !" << endl;
  
}


