/*
 * test_LeastSquares.cpp
 *
 *  Created on: Jul 17, 2012
 *      @author Renliw Fleurmond
 */

#include <iostream>
#include "../tools.h"


VectorXd  objectif(VectorXd X){

	assert(X.rows() == 2);

	VectorXd resultat(2);

	double x = X(0);
	double y = X(1);

	resultat(0) = (x-3)*(x-3)+(y-4)*(y-4)+1e-15;
	resultat(1) = 0;

	return resultat;

}

MatrixXd derivee(VectorXd X){

	assert(X.rows() == 2);

	MatrixXd JJ = MatrixXd::Zero(2,2);

	double x = X(0);
	double y = X(1);


	JJ.row(0) << 2*(x-3),2*(y-4);

	return JJ;
}

int main(void){

	VectorXd solution(2);
	VectorXd initiale(2);

	initiale << 4,4;
	solution << 0,0;

	LeastSquares solveur;

	//solveur.setMinPas(1e-15);

	cout <<"solution initiale\n"<<initiale<<endl;

	cout <<"Cout de la fonction a la position initiale:\n"<< objectif(initiale)<<endl;

	cout << "Jacobien de la fonction objectif a la position initiale\n"<<derivee(initiale)<<endl;

	double erreur = solveur.solveNewton(initiale,objectif,derivee,solution);

	cout <<"Solution trouvee methode de Newton:\n"<<solution<<endl;

	cout <<"Valeur de la fonction pour la solution trouvee:\n"<<objectif(solution)<<endl;

	cout << "Erreur residuelle = "<<erreur<<endl;

	erreur = solveur.solveGaussNewton(initiale,objectif,derivee,solution);

	cout <<"Solution trouvee methode de Gauss-Newton:\n"<<solution<<endl;

	cout <<"Valeur de la fonction pour la solution trouvee:\n"<<objectif(solution)<<endl;

	cout << "Erreur residuelle = "<<erreur<<endl;


}
