#include "../vservoing.h"
#include <iostream>
#include <string>
#include <fstream>
#include <limits>
#include <sstream> 

using namespace Eigen;
using namespace std;

int main (int argc, char * argv[]){
  
  if(argc<2){

    std::cout << "Give as argument the absolute or the relative"<<
      "path of the directory which contains the data" <<std::endl;

    return 0;
  }

    //Definition de certains parametres

  double L = 0.12;
  double D = 1.6;
  double H = -0.1;


  Camera camera;

  IntrinsicCam optique(344.00748,379.09781,327.56131, 235.62077); // Max right forearm
  
  camera.setIntrinsicParameters(optique);

  const int CVperiode = 15;

  CVEcran windows;

  windows.setOptique(optique);

  string titre = "Visualisation et estimation";

  windows.setNom(titre);

  windows.ouvrirFenetre();

  //cvNamedWindow("GeckoGeek Mask", CV_WINDOW_AUTOSIZE);
  
  //cvMoveWindow("GeckoGeek Mask", 650, 100);
 
  TrackPoly monTraceur(4,true);

  monTraceur.initTracking(&windows);
  
  monTraceur.attachToCamera(&camera);

  cv::Scalar couleur = Palette::getLastColor();

  cv::Scalar contraste = Palette::getLastColor();

  Quaterniond q;

  VectRotation so3;

  Vector3d position;

  Repere se3;

  double x,y,z,w;

  MatrixXd points = MatrixXd::Zero(4,3);

  points << 
    D, L, L + H,
    D,-L, L + H,
    D,-L,-L + H,
    D, L,-L + H;

  MultiPointFeature cible(points);

  cible.setSituation(se3);

  cible.attachToCamera(&camera);

  MultiPointEstimator estimateur(&cible,&monTraceur);

  estimateur.attachToCamera(&camera);

  estimateur.setSituation(se3);

  estimateur.setLogFile("estim-nonlinear.dat");

  estimateur.setThreshold(2);
  
  const string dossier(argv[1]);

  string subtitle = dossier + "annotations.dat";

  std::ofstream printer;
  
  printer.open(subtitle.c_str());

  bool notEmpty = true;

  double * ligneImage = NULL;

  double * lignePoses = NULL;

  const string fichier_repere = dossier + "pose_camera.dat";

  const string fichier_images = dossier + "heure_image.dat";

  string picture_path = "";

  cv::Mat photo;

  vector<double *> dataCamera = loadDAT(fichier_images,2);
  
  vector<double *> dataPose = loadDAT(fichier_repere,8);

  vector<VectorXd> dataClicks;

  VectorXd F;

  if(dataCamera.size() == 0 || dataPose.size() == 0){
    notEmpty = false;
  }

  unsigned int countI = 0;

  unsigned int selectedImg = 0;

  unsigned int countP = 0;

  int numImage = 0;

  double oldTime = 0.0;

  double tfTime = 0.0;

  double imgTime = 0.0;

  ligneImage = dataCamera[countI];

  imgTime = ligneImage[0];

  oldTime = imgTime;


  while(notEmpty){

    ligneImage = dataCamera[countI];

    countI++;

    selectedImg = countI;

    if(countI > dataCamera.size()){
      break;
    }

    // Extraire les informations concernant l'image a un instant t

    numImage = static_cast<int>(ligneImage[1]);

    cout << "--------------------Image # " << numImage -10000 << " / " <<dataCamera.size()<<endl;

    ostringstream oss;
      
    oss <<"photo"<< numImage <<".png";

    picture_path = dossier + oss.str();

    photo = cv::imread(picture_path, CV_LOAD_IMAGE_COLOR);

    if(!photo.data){
      cout << "impossible de lire l'image : "<< picture_path <<endl;
      break;
    }

    windows.setImage(photo);

    monTraceur.resetClicks();

    while(monTraceur.isRunning()==false){

      windows.affichage();

      cv::waitKey(CVperiode);
    }
    
    if(printer){

      printer << numImage <<" " << monTraceur.getFunction().transpose() << std::endl;
    }

    dataClicks.push_back(monTraceur.getFunction());

    monTraceur.track(photo);
    
  }

  if(printer){
    printer.close();
  }

  countI = 0;

  countP = 0;

  MyClock::start();

  while(notEmpty){

    ligneImage = dataCamera[countI];

    
    // Extraire les informations concernant l'image a un instant t

    imgTime = ligneImage[0];

    MyClock::setTime(imgTime-oldTime);

    numImage = static_cast<int>(ligneImage[1]);

    ostringstream oss;
      
    oss <<"photo"<< numImage <<".png";

    picture_path = dossier + oss.str();

    photo = cv::imread(picture_path, CV_LOAD_IMAGE_COLOR);

    if(!photo.data){
      cout << "impossible de lire l'image : "<< picture_path <<endl;
      break;
    }

    windows.setImage(photo);

    lignePoses = dataPose[countP];

    tfTime = lignePoses[0];

    while(tfTime < imgTime)
      {
	
	countP++;
	
	if(countP>dataPose.size()){
	  break;
	}
	
	lignePoses = dataPose[countP];

	tfTime = lignePoses[0];

      }

    //cout << tfTime - imgTime << endl;

    if(countP>dataPose.size()){
      break;
    }

    // Extraire les informations concernant la pose relative de la camera

    x = lignePoses[1];

    y = lignePoses[2];

    z = lignePoses[3];
      
    w = lignePoses[4];

    q = Quaterniond(w,x,y,z);

    x = lignePoses[5];

    y = lignePoses[6];

    z = lignePoses[7];

    so3 = VectRotation(q);
      
    position = Vector3d(x,y,z);

    se3 = Repere(so3,position);

    camera.setRepere(se3);

    F = dataClicks[countI];

    cout << F.transpose() <<endl;

    monTraceur.findFeatures(F);

    estimateur.useTracker();

    monTraceur.draw(&windows,couleur);

    //cible.drawFeat(F,&windows,couleur);

    estimateur.draw(&windows,contraste);

    windows.affichage();

    cv::waitKey(30*CVperiode);

    countI++;

    if(countI >= selectedImg-1){
      break;
    }

    
  }

  freeDATA(dataCamera);

  freeDATA(dataPose);




  return 0;

}
