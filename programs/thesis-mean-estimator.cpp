#include "../vservoing.h"
#include <fstream>

using namespace Eigen;
using namespace std;

int main(void){

  int maximumImages = 50;

  int maximumEssais = 2000;

  int nombreBruit = 12;

  double niveauxBruit[12] = {0, 0.001, 0.002, 0.004, 0.006, 0.008, 0.01, 0.012, 0.014, 0.016, 0.018, 0.02};

  double cov = 0;

  MatrixXd covariance = cov * MatrixXd::Identity(3,3);
  
  const string fichier_sortie = "noise-performance.dat";

  const string fichier_sortie2D = "noise-performance2D.dat";

  const string fichier_2vsortie = "v2-noise-performance.dat";

  const string fichier_2vsortie2D = "v2-noise-performance2D.dat";


  std::ofstream printer; // output stream

  std::ofstream printer2D; // output stream

  std::ofstream printer2v; // output stream

  std::ofstream printer2v2D; // output stream

  printer.open(fichier_sortie.c_str());

  if(!printer){
    cout << "Failed to create the output file  ! 1"<<endl;
    return 0;
  }

  printer2D.open(fichier_sortie2D.c_str());

  if(!printer2D){
    cout << "Failed to create the output file  ! 2"<<endl;
    return 0;
  }

  printer2v.open(fichier_2vsortie.c_str());

  if(!printer2v){
    cout << "Failed to create the output file  ! 3"<<endl;
    return 0;
  }

  printer2v2D.open(fichier_2vsortie2D.c_str());

  if(!printer2v2D){
    cout << "Failed to create the output file  ! 4"<<endl;
    return 0;
  }

  IntrinsicCam optique(700,700,350,350);
  optique.largeur = 700;
  optique.hauteur = 700;

  Camera oeil;

  oeil.setIntrinsicParameters(optique);

  Repere se3;

  double D = 0;
  double L = 0.05;
  
  MatrixXd points = MatrixXd::Zero(4,3);

  points << 
    D, L, L,
    D,-L, L,
    D,-L,-L,
    D, L,-L;

  VectorXd solution(12);
  
  solution << 
    D, L, L,
    D,-L, L,
    D,-L,-L,
    D, L,-L;

  MultiPointFeature cible(points);

  
  cible.attachToCamera(&oeil);

  cible.setStrategy(VisualFeature::PINV);

  MultiPointEstimator estimateur(&cible,&cible);

  estimateur.setThreshold(2);

  Vector3d depart(-0.15,  0, 0); 

  Vector3d arrive(-1.15,  0, 0);

  Vector3d position(0,0,0);

  VectorXd biais = VectorXd::Zero(3);

  VectorXd bruit = VectorXd::Zero(3);

  double erreur3D = 0, erreur2D, eee;

  VectorXd erreurXd, F;

  for(int N = 2; N <= maximumImages; N++){

    Vector3d pas = (arrive - depart)/(N-1);

    if(printer){
      printer << N ;
    }

    if(printer2D){
      printer2D << N ;
    }

    
    for(int b = 0; b < nombreBruit ; b++){

      if(N==2){

	if(printer2v){
	  printer2v << niveauxBruit[b];
	}

	if(printer2v2D){
	  printer2v2D << niveauxBruit[b] ;
	}
      }

      cov = niveauxBruit[b] * niveauxBruit[b];

      covariance = cov * MatrixXd::Identity(3,3);

      MultivariateGaussian  * bruiteur = new MultivariateGaussian(biais, covariance);

      erreur3D = 0;

      erreur2D = 0;

      for(int j = 0; j < maximumEssais; j++){

	estimateur.eraseMemory();

	for(int i = 0; i < N; i++){

	  //bruit

	  bruiteur->sample(bruit);

	  position = depart + i * pas;

	  se3.setDeplacement(position);

	  oeil.setRepere(se3);

	  F = cible.getFunction();

	  position = depart + i * pas + bruit;

	  se3.setDeplacement(position);

	  oeil.setRepere(se3);

	  estimateur.computeSpecialEstimation(F);

	  position = depart + i * pas;

	  se3.setDeplacement(position);

	  oeil.setRepere(se3);

	}

	erreurXd = estimateur.getEstimation() - solution;

	eee = 0.25 *( 
		     erreurXd.block<3,1>(0,0).norm() +
		     erreurXd.block<3,1>(3,0).norm() +
		     erreurXd.block<3,1>(6,0).norm() +
		     erreurXd.block<3,1>(9,0).norm()
		      );

	erreur3D += eee;

	for(int i = 0; i < N; i++){

	  position = depart + i * pas;

	  se3.setDeplacement(position);

	  oeil.setRepere(se3);

	  //estimateur.useTracker();

	  erreurXd = estimateur.getFunction() - cible.getFunction();

	  eee = erreurXd.norm()/4;

	  erreur2D += eee/N;

	}
	
      }

      erreur3D /= maximumEssais;

      erreur2D /= maximumEssais;

      if(printer){
	printer << " " <<erreur3D;
      }

      if(printer2D){
	printer2D << " " <<erreur2D;
      }

      if(N==2){
      
	if(printer2v){
	  printer2v << " " <<erreur3D  <<endl;
	}

	if(printer2v2D){
	  printer2v2D << " " <<erreur2D << endl;
	}

      }

      delete bruiteur;

    }

    if(printer){
      printer << endl;
    }

    if(printer2D){
      printer2D << endl;
    }

  }

  printer.close();
  printer2D.close();
  printer2v.close();
  printer2v2D.close();

  
}


