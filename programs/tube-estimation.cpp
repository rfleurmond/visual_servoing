#include "../vservoing.h"

using namespace Eigen;
using namespace std;


int main(void){

  Camera oeil;

  IntrinsicCam optique(700,700,350,350);
  optique.largeur = 700;
  optique.hauteur = 700;

  oeil.setIntrinsicParameters(optique);

  // Create the visual target
  
  double R = 0.025;
  double L = 0.1;
  Vector3d B(0,0,0);
  //Vector3d T(0,0.8*L,0.6*L);
  Vector3d T(0,0,L);
  
  BoundedCylinder cible(R, B, T);

  // Add noise to the target

  double covP = 4;

  double d2r = M_PI/180;

  double covT = d2r * d2r; //1e-3 ?

  cout << " Covoariance de l'angle = " << covT << endl;

  MatrixXd covariance = covP * MatrixXd::Identity(8,8);

  covariance(1,1) =  covT;
  
  covariance(5,5) =  covT;

  //covariance = 1e-15 * covariance;
  
  NoisyFeature cibleN(&cible,covariance);

  // Put the estimator between 

  cout << "!!!!!!!!!!!!!!!!!!   ICI !!!!!!!!!!!!!!!!!!!!!! " <<endl;

  BCylinderEstimator cibleE(&cibleN,&cibleN);

  //cibleE.setRadius(R);

  cibleE.setLogFile("estim-nonlinear.dat");

  cibleE.attachToCamera(&oeil);
  
  // Set different countdowns

  cibleE.setDeadline(1.5);

  /// Put the features in one 

  FlyingSystem arm;

  Repere idi;

  Repere shoulder(VectRotation(),Vector3d(0.5,0,0));

  ManyArmHolderTask tache;

  tache.addArm(&arm,idi);
  
  tache.addCamera(&oeil,&arm,idi,6);

  tache.addVisualTarget(&cibleE,NULL,shoulder,6);

  tache.makeTargetSeen(&cibleE,&oeil);

  tache.setPeriod(1.0/20);

  tache.setLambda(0.5);

  //2tache.setLambda(AdaptiveGain(0.5, 1));

  tache.setMinimumErrorNorm(0.001 * 2 * sqrt(covP));

  VectorXd etat = VectorXd::Zero(6);

  etat << 
     0, 0, 0, // orientation
       0, 0, 0; // position

  tache.setState(etat);

  arm.setState(etat);

  ConstReference ref(cible.getFunction()); 

  etat << 
    0.2, 0.3, 0, 
   -1, 0.3, 0.2;
  
  tache.setState(etat);

  arm.setState(etat);

  tache.giveTargetReference(&oeil,&cibleE,&ref,true);

  //tache.start();

  //tache.keepVisualTrace(true);

  ////////////PREPARATION COMMANDE et ESTIMATION  ////////////////////////

  MatrixXd a(2,6);

  a << 
    0, 0, 0, 0, 1, 0,
    0, 0, 0, 0, 0, 1;

  VectorXd qq = VectorXd::Zero(6);

  QPointTask constraint(qq,a);

  MatrixXd conversion = MatrixXd::Identity(8,8);

  conversion(1,1) = 360/M_PI;

  conversion(5,5) = 360/M_PI;

  PartTask expression(conversion,&tache);

  PriorityTask globale(&constraint,&expression);

  globale.hideTaskOnControl(&constraint);

  cout << " Avant run !" << endl;

  /////////////////////////////////RUN/////////////////////////////////

  TaskController controleur(&arm,&globale);

  controleur.setSmoothRatio(10);

  controleur.setTimeLimit(10);

  controleur.run();
  
  //////////////////////////FIN RUN //////////////////////////////////

  cout << " Apres run !" << endl;
  
}


