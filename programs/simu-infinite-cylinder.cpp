#include "../vservoing.h"

using namespace Eigen;
using namespace std;

int main(void){

  Camera cam;

  //IntrinsicCam optique(240,240,320,240); 

  //cam.setIntrinsicParameters(optique);

  double R = 0.2;
  
  Cylinder cible(R);

  FlyingSystem bras;

  Repere epaule(VectRotation(0,0,0),Vector3d(0, 0,0));

  Repere orbite(VectRotation(0,0,0),Vector3d(-2,0,0));

  Repere simple;
  
  double lambda = 0.5;

  ManyArmHolderTask tache;

  tache.addArm(&bras,epaule);

  tache.addCamera(&cam,NULL,orbite,0);

  tache.addVisualTarget(&cible,&bras,simple,6);

  tache.makeTargetSeen(&cible,&cam);

  tache.setPeriod(1.0/20);

  tache.setLambda(lambda);

  tache.setMinimumErrorNorm(1e-5);

  VectorXd etat = VectorXd::Zero(6);

  etat << -M_PI/4, 0, 0, 0, 0, 0;

  tache.setState(etat);

  bras.setState(etat);

  cible.attachToCamera(&cam);

  ConstReference ref(cible.getFunction()); 

  //etat << 0.3, 0, 0, 5, -2, 1;

  etat << 2.5*M_PI/4, 0, 0, 0, 0, 0;

  tache.setState(etat);

  bras.setState(etat);

  tache.giveTargetReference(&cam,&cible,&ref,true);

  VectorXd val = tache.valeur();

  cout << "La fonction de tache \n"<<val<<endl;

  MatrixXd J = tache.jacobien(etat);

  cout <<"Jacobien de la tache\n"<< J <<endl;

  FullPivLU<MatrixXd> lu_decomp(J);

  cout << "The rank of J is " << lu_decomp.rank() << endl;

  cout <<"Etat du systeme\n"<<etat<<endl;

  TaskController controleur(&bras,&tache);

  //controleur.setSmoothRatio(4);

  //controleur.enableSmoothing(false);

  controleur.setTimeLimit(200);

  controleur.run();

}


