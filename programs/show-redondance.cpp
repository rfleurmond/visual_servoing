#include "../taches.h"

using namespace Eigen;

using namespace std;

int main(void){

  double lambda = 1;

  VectorXd etat = VectorXd::Zero(2);

  etat << 2,8;//2.5,7.5;

  QSystem robot(etat,1.0/200);

  VectorXd cibleM = VectorXd::Zero(2);

  cibleM << 0, 0;

  MatrixXd filtre = MatrixXd::Zero(1,2);

  filtre << 1, -1;

  QTask master(cibleM,filtre);

  master.setLambda(lambda);

  master.setState(etat);

  master.setPeriod(1.0/200);

  VectorXd qMin = VectorXd::Zero(2);

  VectorXd qMax = VectorXd::Zero(2);

  qMax << 5, 10;

  qMin << 0, 0;

  QBoundedTask gendarme(qMin,qMax);

  gendarme.setCoeffMax(0.0);

  gendarme.setCoeffMin(0.2);

  gendarme.setAlpha(10);

  gendarme.setMax(10);
  
  gendarme.setState(etat);

  gendarme.setLambda(lambda);
  
  gendarme.setPeriod(1.0/200);

  EgalityTask dualite(&gendarme,&master);

  cout <<"Valeur de la tache"<<endl;

  cout <<dualite.valeur()<< endl;

  cout <<"Jacobien de la tache:"<< endl;

  cout <<dualite.jacobien(etat) << endl;

  TaskController controleur(&robot,&dualite);

  //controleur.setSysLogFile("priority1.task");

  controleur.setSmoothRatio(3);

  controleur.enableSmoothing(false);

  controleur.setTimeLimit(10);

  cout << "\n\n Avant run !" << endl;

  controleur.run();

  cout << " Apres run !" << endl;
 

}
