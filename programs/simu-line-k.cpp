#include "../vservoing.h"

using namespace Eigen;
using namespace std;

int main(void){

  Camera oeil;

  Vector3d p1(10, 2,-2);

  Vector3d p2(10,-2,-2);

  Vector3d p3(10,-2, 2);

  Vector3d p4(10, 2, 2);

  LineWithK lf1(p1,p4,p4);

  LineWithK lf2(p2,p1,p2);

  MultiFeature cible(&lf1);

  cible.addFeature(&lf2);

  cible.attachToCamera(&oeil);

  cout<<"Dimension de la cible:"<<cible.getDimensionOutput()<<endl;

  FlyingSystem peterpan;

  VisualTask tache(&cible, &peterpan);

  tache.setModeFixation(VisualTask::EMBARQUE);

  tache.setPeriod(1.0/20);

  tache.setLambda(0.8);

  tache.setMinimumErrorNorm(1e-10);

  VectorXd etat = VectorXd::Zero(6);

  etat << 0, 0, 0, 0, 0, 0;

  tache.setState(etat);

  peterpan.setState(etat);

  MatrixXd lStar = cible.getInteractionMatrix();

  cible.setReferenceInteractionMatrix(lStar);

  VectorXd s_star = cible.getFunction();

  //etat << 0.5, 0, 0, 0, 0, 0;

  etat << M_PI, 0, 0, 5, 1, -1;

  //etat << -0.6, 0, 0, 5, 1, -1;

  tache.setReference(s_star);

  tache.setState(etat);

  peterpan.setState(etat);

  cout<<"Valeur des indices visuels\n"<<cible.getFunction().transpose()<<endl;

  cout<<"Valeur souhaitee des indices visuels\n"<<s_star.transpose()<<endl;

  cout<<"Valeur de la tache\n"<<tache.valeur().transpose()<<endl;

  MatrixXd A = tache.jacobien(etat);

  cout<<"Jacobien de la fonction de tache\n"<<A<<endl;

  FullPivLU<MatrixXd> lu_decomp(A);
  
  cout << "The rank of A is " << lu_decomp.rank() << endl;

  TaskController controleur(&peterpan,&tache);

  controleur.setTimeLimit(100);

  cout << " Avant run !" << endl;

  //tache.keepVisualTrace(true);

  controleur.run();

  cout << " Apres run !" << endl;
  
}


