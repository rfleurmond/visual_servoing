#include "../vservoing.h"
#include <iostream>
#include <string>
#include <fstream>
#include <limits>
#include <sstream> 

using namespace std;

int main (int argc, char * argv[]){

  int numPoints = 4;

  int debut = 0;
  
  if(argc<2){

    std::cout << "Give as argument the absolute or the relative"<<
      "path of the directory which contains the data" <<std::endl;

    return 0;
  }

  const string dossier(argv[1]);

  if(argc>=3){

    int val = 0;
    string num(argv[2]);
    std::istringstream istr(num);
    istr >> val;
    std::cout <<" J'ai bien recu comme nombre de points " << val <<endl;
    if(val >0 && val <10){
      numPoints = val;
    }
    
  }

  if(argc>=4){
    
    int val = 0;
    string num(argv[3]);
    std::istringstream istr(num);
    istr >> val;
    if(val > 0){
      debut = val;
    }
    
  }


  const string fichier_images = dossier + "heure_image.dat";

  vector<double *> dataCamera = loadDAT(fichier_images,2);
  
  std::ofstream printer;

  ostringstream oss1;
      
  oss1 <<"annotations"<< numPoints <<"points.dat";

  const string subtitle = dossier + oss1.str();

  printer.open(subtitle.c_str(), std::ofstream::out | std::ofstream::app);

  if(dataCamera.size() ==0 ){
    std::cout << "Not enough data" << endl;
    return 0;
  }

  Camera camera;

  IntrinsicCam optique(344.00748,379.09781,327.56131, 235.62077); // Max right forearm
  
  camera.setIntrinsicParameters(optique);

  const int CVperiode = 5;

  CVEcran windows;

  windows.setOptique(optique);

  string titre = "Enregistrement";

  windows.setNom(titre);

  windows.ouvrirFenetre();

  TrackPoints monTraceur(numPoints);

  std::cout << "Nombre de points suivis "<< numPoints << std::endl;

  monTraceur.attachToCamera(&camera);

  cv::Scalar couleur = Palette::getLastColor();
 
  cv::Mat photo;
 
  int numImage = 0;

  double * ligneImage = NULL;

  string picture_path = "";

  //  double scoreMax = 0;

  // double score = 0;

  for(int i = debut; i < dataCamera.size(); i++){

    ligneImage = dataCamera[i];

    numImage = static_cast<int>(ligneImage[1]);

    ostringstream oss;
      
    cout << "--------------------Image # " << numImage -10000 << " / " <<dataCamera.size()<<endl;

    oss <<"photo"<< numImage <<".png";

    picture_path = dossier + oss.str();

    photo = cv::imread(picture_path, CV_LOAD_IMAGE_COLOR);

    if(!photo.data){
      cout << "impossible de lire l'image : "<< picture_path <<endl;
      break;
    }

    windows.setImage(photo);

    if(i == debut){
      monTraceur.initTracking(&windows);
    }

    if(monTraceur.isLost()){
      monTraceur.resetClicks();
    }

    while(monTraceur.isRunning()==false){

      windows.affichage();

      cv::waitKey(CVperiode);

      if(monTraceur.isRunning()){
	//monTraceur.track(photo);
	//scoreMax = monTraceur.getScore();
      }
    }

    //score = monTraceur.getScore();
    
    
    if(printer){

      printer << numImage <<" " << monTraceur.getFunction().transpose() << std::endl;
    }

    monTraceur.track(photo);

    monTraceur.draw(&windows, couleur);

    windows.affichage();

    cv::waitKey(numPoints * CVperiode);
    
  }

  if(printer){
    printer.close();
  }

  return 0;

}
