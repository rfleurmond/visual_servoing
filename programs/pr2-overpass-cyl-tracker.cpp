#include "../vservoing.h"
#include <iostream>
#include <string>
#include <fstream>
#include <limits>
#include <sstream> 

using namespace Eigen;
using namespace std;


int main (void){

  Camera camera;

  IntrinsicCam optique(418.6835, 419.16346, 309.05563, 224.1891); // Bob  wide left stereo
  
  //IntrinsicCam optique(383.47353, 383.47353, 315.04432, 228.73354); // Bob  wide left stereo
  
  //IntrinsicCam optique(422,422,318,224); // Max wide left stereo
  
  camera.setIntrinsicParameters(optique);

  const int CVperiode = 200;

  CVEcran windows;

  windows.setOptique(optique);

  string titre = "Visualisation et estimation";

  windows.setNom(titre);

  windows.ouvrirFenetre();

  
  cv::Scalar couleur = Palette::getLastColor();

  Quaterniond q;

  VectRotation so3;

  Vector3d position;

  Repere se3;

  double w;

  double R = 0.0075;//0.0075

  double L = 0.15;

  double x = 0.02;

  double y = -0.025;

  double z = 0.08;

  BoundedCylinder cibleV(R, Vector3d(x,y,z-L),Vector3d(x,y,z));

  cibleV.setSituation(se3);

  cibleV.attachToCamera(&camera);

  double cov = 8e-5;

  double covP = 4;

  MatrixXd covariance = covP * MatrixXd::Identity(8,8);

  covariance(1,1) =  cov;
  
  covariance(5,5) =  cov;
  
  NoisyFeature cibleN(&cibleV,covariance);

  VSnuffer cible(&cibleN,15,30); 

  BCylinderEstimator estimateur(&cibleV,&cible);

  //estimateur.setRadius(R);

  estimateur.setLength(L);

  estimateur.selectInformation(0,0,1,1);

  estimateur.attachToCamera(&camera);

  estimateur.setSituation(se3);

  estimateur.setLogFile("estim-nonlinear.dat");

  estimateur.setThreshold(30);
  
  const string dossier = "../../Data/exp7/";

  bool notEmpty = true;

  double * ligneImage = NULL;

  double * lignePoses = NULL;

  const string fichier_repere = dossier + "pose_camera.dat";

  const string fichier_images = dossier + "heure_image.dat";

  string picture_path = "";

  cv::Mat photo;

  vector<double *> dataCamera = loadDAT(fichier_images,2);
  
  vector<double *> dataPose = loadDAT(fichier_repere,8);

  //vector<double * >::iterator itP,itI;
  
  if(dataCamera.size() == 0 || dataPose.size() == 0){
    notEmpty = false;
  }

  unsigned int countI = 0;

  unsigned int countP = 0;

  int numImage = 0;

  double oldTime = 0.0;

  double tfTime = 0.0;

  double imgTime = 0.0;

  MyClock::start();

  ligneImage = dataCamera[countI];

  imgTime = ligneImage[0];

  oldTime = imgTime;


  while(notEmpty){

    ligneImage = dataCamera[countI];

    countI++;

    if(countI > dataCamera.size()){
      break;
    }

    // Extraire les informations concernant l'image a un instant t

    imgTime = ligneImage[0];

    MyClock::setTime(imgTime-oldTime);

    numImage = static_cast<int>(ligneImage[1]);

    ostringstream oss;
      
    oss <<"photo"<< numImage <<".png";

    picture_path = dossier + oss.str();

    photo = cv::imread(picture_path, CV_LOAD_IMAGE_COLOR);

    if(!photo.data){
      cout << "impossible de lire l'image : "<< picture_path <<endl;
      break;
    }

    windows.setImage(photo);

    lignePoses = dataPose[countP];

    tfTime = lignePoses[0];

    while(tfTime < imgTime)
      {
	
	countP++;
	
	if(countP>dataPose.size()){
	  break;
	}
	
	lignePoses = dataPose[countP];

	tfTime = lignePoses[0];

      }

    //cout << tfTime - imgTime << endl;

    if(countP>dataPose.size()){
      break;
    }

    // Extraire les informations concernant la pose relative de la camera

    x = lignePoses[1];

    y = lignePoses[2];

    z = lignePoses[3];
      
    w = lignePoses[4];

    q = Quaterniond(w,x,y,z);

    x = lignePoses[5];

    y = lignePoses[6];

    z = lignePoses[7];

    so3 = VectRotation(q);
      
    position = Vector3d(x,y,z);

    se3 = Repere(so3,position);

    camera.setRepere(se3);
    
    estimateur.useTracker();

    //cible.draw(&windows,couleur);

    estimateur.draw(&windows,couleur);

    windows.affichage();

    cv::waitKey(CVperiode);
    
  }

  freeDATA(dataCamera);

  freeDATA(dataPose);

}
