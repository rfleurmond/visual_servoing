#include "../vservoing.h"

using namespace Eigen;
using namespace std;

int main(void){

  //Definition de certains parametres

  double L = 0.07;
  
  //Creation des caméras

  Camera oeilHG,oeilHD,oeilFD;
  IntrinsicCam optiqueH(418.6835, 419.16346, 309.05563, 224.1891);
  //IntrinsicCam optiqueH(772.55,772.55,320.5,240.5);
  IntrinsicCam optiqueF(320,320,320.5,240.5);
 
  oeilHG.setIntrinsicParameters(optiqueH);
  oeilHD.setIntrinsicParameters(optiqueH);
  oeilFD.setIntrinsicParameters(optiqueF);
  
  //Definition des indices visuels

  Vector3d p0(0, 0, 0);

  Vector3d p1(0, 0, L);
  
  LineWithK cibleG(p0,p1,p0);

  LineWithK cibleD(p0,p1,p1);

  double covP = 4;

  double d2r = M_PI/180;

  double r2d = 360/M_PI;

  double cov = d2r * d2r;

  MatrixXd covariance(3,3);

  covariance << 
    covP,   0,    0,
    0,    cov,    0,
    0,      0, covP;
    
  NoisyFeature cibleGauche(&cibleG,covariance);

  NoisyFeature cibleDroite(&cibleD,covariance);

  // Hide the visual features during one given period 

  VSnuffer hideLeft(&cibleGauche,3,8); 

  VSnuffer hideRight(&cibleDroite,3,8); 

  LineKEstimator cibleEG(&cibleGauche,&hideLeft);

  LineKEstimator cibleED(&cibleDroite,&hideRight);

  cibleED.setLogFile("estim-nonlinear.dat");

  cibleEG.setThreshold(200000);

  cibleED.setThreshold(200000);

  //cibleEG.setDeadline(7);

  //cibleED.setDeadline(7);

  

  //Gestion des bras du robot

  //Creation des modeles vides et des bras

  GeoRobot brasGauche,brasDroite;

  //Chargement du modele geometrique des bras

  loadSE3Robot("../robot_models/pr2_gauche.ser",brasGauche);

  loadSE3Robot("../robot_models/pr2_droite.ser",brasDroite);

  //Creation des modeles cinematiques des bras

  MyKineModel modeleGauche(&brasGauche);

  MyKineModel modeleDroite(&brasDroite);

  //Creation de l'interface TaskSystem pour le controleur

  RobotSystem sysGauche(&brasGauche);

  RobotSystem sysDroite(&brasDroite);

  sysDroite.setPeriod(1.0/100);

  sysGauche.setPeriod(1.0/100);
 
  //Couplage des deux systemes

  DualArmSystem PR2(&sysGauche,&sysDroite);

  cov = 7.6e-9;//9

  covariance = cov * MatrixXd::Identity(14,14);

  NoisySystem noisyPR2(&PR2,covariance);

  //Creation de la tache visuelle principale

  ManyArmHolderTask tache;

  // Defintion des reperes

  Repere tigeGauche(VectRotation(0,0,0),Vector3d(-3*L, 0, 0));
  
  Repere tigeDroite(VectRotation(0,0,0),Vector3d(-3*L, 0, 0));

  Repere epauleGauche(VectRotation(0,0,0),Vector3d(0,  0.188, 0));
  
  Repere epauleDroite(VectRotation(0,0,0),Vector3d(0, -0.188, 0));

  Repere priseGauche(VectRotation(0, 0, 0),Vector3d(0.17, 0,-0.07));
  
  Repere priseDroite(VectRotation(0, 0, 0),Vector3d(0.17, 0, 0.00));

  Repere camGauche(VectRotation(0,M_PI/4,0),Vector3d(0.067, 0.029, 0.4968));

  Repere camDroite(VectRotation(0,M_PI/4,0),Vector3d(0.067,-0.060, 0.4968));

  Repere fixDroite(VectRotation(3.018, -0.872524,0),
		   Vector3d(0.135-0.321, 0.044, 0));

  Repere simple;

  // Ajout des deux bras a la tache

  tache.addArm(&modeleGauche,epauleGauche);

  tache.addArm(&modeleDroite,epauleDroite);

  // Defintion des parametres systemes de la tache

  tache.setPeriod(1.0/15);

  double lambda = 1;

  tache.setLambda(lambda);

  tache.setMinimumErrorNorm(1e-1);

  //Placement des caméras

  tache.addCamera(&oeilFD,&modeleDroite,fixDroite,5,"Moving Right forearm Camera");

  //tache.addCamera(&oeilHD,NULL,camDroite,7);

  tache.addCamera(&oeilHG,NULL,camGauche,7,"Non-moving Head camera");

  // Placement des indices visuels

  tache.addVisualTarget(&cibleEG,&modeleGauche,priseGauche,7);

  tache.addVisualTarget(&cibleED,&modeleDroite,priseDroite,7);

  // Quels indices visuels percoivent les cameras?

  tache.makeTargetSeen(&cibleEG,&oeilHG);

  tache.makeTargetSeen(&cibleED,&oeilHG);

  tache.makeTargetSeen(&cibleEG,&oeilFD);

  tache.makeTargetSeen(&cibleED,&oeilFD);

  //tache.makeTargetSeen(&cibleGauche,&oeilHD);

  //tache.makeTargetSeen(&cibleDroite,&oeilHD);

  // Quelle est la reference des indices visuels par camera?
  
  cibleED.attachToCamera(&oeilHG);

  OtherReference ref1(&cibleED);

  tache.giveTargetReference(&oeilHG,&cibleEG,&ref1,true);

  cibleED.attachToCamera(&oeilFD);

  OtherReference ref2(&cibleED);

  tache.giveTargetReference(&oeilFD,&cibleEG,&ref2,true);

  // Constante arbitraire pour l'écart entre le stylo et le capuchon
  
  VectorXd constante(3);
  
  constante << 0, 0, 50;

  tache.giveKandCmatrix(&oeilFD,&cibleEG,MatrixXd::Identity(3,3),constante);

  // Etat initial
  
  VectorXd etat = VectorXd::Zero(14);

  //*
  etat << 
    M_PI/4,  0.1,  M_PI/2,  -M_PI/2, -M_PI/2,  -0.05,  0,
    -M_PI/4, -0.1, -M_PI/2,  -M_PI/2,  M_PI/2,  -0.05,  0;
  //*/

  tache.setState(etat);

  PR2.setState(etat);


  //tache.keepVisualTrace(true);

  //Defintion de la premiere tâche a enchainer

  // alignement des deux axes

  MatrixXd selection1 = MatrixXd::Identity(3,6);

  selection1(1,1) = r2d;

  PartTask coplanaire(selection1,&tache);

  coplanaire.setMinimumErrorNorm(2);

  //coplanaire.setLambda(AdaptiveGain(lambda,5*lambda));

  
  // Definition de la deuxieme tache a enchainer

  MatrixXd selection2 = MatrixXd::Identity(5,6);

  selection2(1,1) = r2d;

  selection2(4,4) = r2d;

  PartTask alignement(selection2,&tache);

  alignement.setMinimumErrorNorm(2);

  //alignement.setLambda(AdaptiveGain(lambda,5*lambda));

  //Defintion de la troisieme tache a enchainer
  
  MatrixXd selection3 = MatrixXd::Zero(5,6);

  selection3.block<5,6>(0,0) <<
    1,   0, 0, 0,   0, 0,
    0, r2d, 0, 0,   0, 0,
    0,   0, 0, 1,   0, 0,
    0,   0, 0, 0, r2d, 0,
    0,   0, 0, 0,   0, 1;
  
  PartTask translation(selection3,&tache);

  translation.setMinimumErrorNorm(2);

  //translation.setLambda(AdaptiveGain(lambda,5*lambda));


  //Definition de la sequence de tâches
  
  SequencyTask sequence(&coplanaire,&alignement);

  sequence.addTaskInStack(&translation);

  //Gestion des butees

  VectorXd buteeMin(14);

  VectorXd buteeMax(14);

  buteeMin.topRows(7) = brasGauche.getMinimalQ();
  buteeMin.bottomRows(7) = brasDroite.getMinimalQ();

  buteeMax.topRows(7) = brasGauche.getMaximalQ();
  buteeMax.bottomRows(7) = brasDroite.getMaximalQ();

  MatrixXd selection5 = MatrixXd::Zero(10,14);

  selection5.block<4,4>(0,0) = MatrixXd::Identity(4,4);
  selection5.block<4,4>(5,7) = MatrixXd::Identity(4,4);
  selection5(4,5)  = 1;
  selection5(9,12) = 1;

  buteeMin = selection5 * buteeMin;
  buteeMax = selection5 * buteeMax;

  QBoundedTask gendarme(buteeMin,buteeMax,selection5);

  gendarme.setCoeffMax(-0.01);

  gendarme.setCoeffMin(0.03);

  gendarme.setAlpha(2);

  gendarme.setMax(10);
  
  gendarme.setState(etat);

  gendarme.setLambda(2);

  MyOptimaPriorityTask globale(&gendarme,&sequence);

  globale.setState(etat);

  globale.hideTaskOnControl(&gendarme);


  //Creation du controleur
  
  cout <<"Etat du systeme\n"<<etat<<endl;

  VectorXd val = tache.valeur();

  cout << "La fonction de tache \n"<<val<<endl;

  MatrixXd J = tache.jacobien(etat);

  cout <<"Jacobien de la tache\n"<< J <<endl;

  FullPivLU<MatrixXd> lu_decomp(J);

  cout << "The rank of J is " << lu_decomp.rank() << endl;

  val = tache.valeur();

  cout << "La fonction de tache \n"<<val<<endl;

  TaskController controleur(&noisyPR2,&globale);

  //controleur.enableSmoothing(false);

  //controleur.setSmoothRatio(5);

  controleur.setTimeLimit(30);

  //Lancement du contructeur

  controleur.run();

  //Fin du programme

  val = tache.valeur();

  cout << "La fonction de tache \n"<<val<<endl;

}
