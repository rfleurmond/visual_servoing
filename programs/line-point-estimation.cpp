#include "../vservoing.h"

using namespace Eigen;
using namespace std;


int main(void){

  Camera oeil;

  IntrinsicCam optique(240,240,320,240);
  
  oeil.setIntrinsicParameters(optique);

  // Create the visual target
  
  Vector3d direction1(0,0,1);

  Vector3d centre1(10,2,-2);

  LineFeature indice1(centre1,centre1+direction1);


  Vector3d direction2(0,1,0);

  Vector3d centre2(10,0,2);

  LineFeature indice2(centre2,centre2+direction2);


  Vector3d direction3(0, 0, -1);

  Vector3d centre3(10,-2,-2);

  LineFeature indice3(centre3,centre3+direction3);

  // Add noise to the target

  double covP = 4;

  double covT = 1e-4;

  MatrixXd covariance = covP * MatrixXd::Identity(2,2);

  covariance(1,1) =  covT;
  
  NoisyFeature cible1(&indice1,covariance);

  NoisyFeature cible2(&indice2,covariance);

  NoisyFeature cible3(&indice3,covariance);

  // Put the estimator between 

  LinePointEstimator cibleE1(&cible1,&cible1);

  LinePointEstimator cibleE2(&cible2,&cible2);

  LinePointEstimator cibleE3(&cible3,&cible3);

  cibleE3.setLogFile("estim-nonlinear.dat");

  cibleE1.attachToCamera(&oeil);

  cibleE2.attachToCamera(&oeil);

  cibleE3.attachToCamera(&oeil);

  // Set different countdowns

  cibleE1.setThreshold(50);

  cibleE2.setThreshold(50);

  cibleE3.setThreshold(50);

 
  /// Put the features in one 

  //*

  MultiFeature bigTarget(&cibleE1);

  bigTarget.addFeature(&cibleE2);
  
  bigTarget.addFeature(&cibleE3);
  
  //*/

  /*
  
  MultiFeature bigTarget(&cible1);

  bigTarget.addFeature(&cible2);
  
  bigTarget.addFeature(&cible3);
  
  //*/


  bigTarget.attachToCamera(&oeil);


  FlyingSystem arm;

  Repere idi;

  Repere shoulder(VectRotation(),Vector3d(2,0,0));

  ManyArmHolderTask tache;

  tache.addArm(&arm,idi);
  
  tache.addCamera(&oeil,&arm,idi,6);

  tache.addVisualTarget(&bigTarget,NULL,shoulder,6);

  tache.makeTargetSeen(&bigTarget,&oeil);

  tache.setPeriod(1.0/20);

  tache.setLambda(0.8);

  //tache.setLambda(AdaptiveGain(0.8, 20));

  tache.setMinimumErrorNorm(0.001 * 6* sqrt(covP));

  VectorXd etat = VectorXd::Zero(6);

  etat << 
   0.5, 0, 0, // orientation
     2, 0, 0; // position

  tache.setState(etat);

  arm.setState(etat);

  VectorXd s_star = VectorXd::Zero(6);

  s_star.block<2,1>(0,0) =  indice1.getFunction();

  s_star.block<2,1>(2,0) =  indice2.getFunction();

  s_star.block<2,1>(4,0) =  indice3.getFunction();

  ConstReference ref(s_star); 

  tache.giveTargetReference(&oeil,&bigTarget,&ref,true);

  etat << 
    0.8, 0, 0, 
      8, 0, 0;

  tache.setState(etat);

  arm.setState(etat);

  tache.start();

  //tache.keepVisualTrace(true);

  ////////////PREPARATION COMMANDE et ESTIMATION  ////////////////////////


  cout << " Avant run !" << endl;

  /////////////////////////////////RUN/////////////////////////////////

  TaskController controleur(&arm,&tache);

  controleur.setSmoothRatio(3);

  controleur.setTimeLimit(50);

  controleur.run();
  
  //////////////////////////FIN RUN //////////////////////////////////

  cout << " Apres run !" << endl;
  
}


