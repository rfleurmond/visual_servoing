#include "../vservoing.h"
#include <fstream>
#include <vector>
#include <algorithm>

using namespace Eigen;
using namespace std;

int main(void){

  int maximumImages = 50;

  int maximumEssais = 2000;

  vector<double> stat2D(maximumImages * maximumEssais * 4);

  vector<double> stat3D(maximumEssais * 4);

  int num2D = 0;

  int num3D = 0;

  int nombreBruit = 12;

  double dt = 0.05;

  MyClock::start();

  double niveauxBruit[12] = {0.1, 0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

  const string fichier_sortie = "noise-performance.dat";

  const string fichier_sortie2D = "noise-performance2D.dat";

  const string fichier_2vsortie = "v2-noise-performance.dat";

  const string fichier_2vsortie2D = "v2-noise-performance2D.dat";


  std::ofstream printer; // output stream

  std::ofstream printer2D; // output stream

  std::ofstream printer2v; // output stream

  std::ofstream printer2v2D; // output stream

  printer.open(fichier_sortie.c_str());

  if(!printer){
    cout << "Failed to create the output file  ! 1"<<endl;
    return 0;
  }

  printer2D.open(fichier_sortie2D.c_str());

  if(!printer2D){
    cout << "Failed to create the output file  ! 2"<<endl;
    return 0;
  }

  printer2v.open(fichier_2vsortie.c_str());

  if(!printer2v){
    cout << "Failed to create the output file  ! 3"<<endl;
    return 0;
  }

  printer2v2D.open(fichier_2vsortie2D.c_str());

  if(!printer2v2D){
    cout << "Failed to create the output file  ! 4"<<endl;
    return 0;
  }

  

  IntrinsicCam optique(700,700,350,350);
  optique.largeur = 700;
  optique.hauteur = 700;

  Camera oeil;

  oeil.setIntrinsicParameters(optique);

  Repere se3;

  double D = 0;
  double L = 0.05;
  
  MatrixXd points = MatrixXd::Zero(4,3);

  points << 
    D, L, L,
    D,-L, L,
    D,-L,-L,
    D, L,-L;

  VectorXd solution(12);
  
  solution << 
    D, L, L,
    D,-L, L,
    D,-L,-L,
    D, L,-L;

  MultiPointFeature cible(points);

  double cov = 0;

  MatrixXd covariance = cov * MatrixXd::Identity(8,8);
  
  NoisyFeature cibleNoisy(&cible,covariance); 
  
  cibleNoisy.attachToCamera(&oeil);

  cibleNoisy.setStrategy(VisualFeature::PINV);

  MultiPointEstimator estimateur(&cible,&cibleNoisy);

  estimateur.setThreshold(2);

  Vector3d depart(-1.15,  0.2, 0.3); 

  Vector3d arrive(-0.15,  0, 0);

  Vector3d position(0,0,0);

  double erreur3D = 0, erreur2D;

  VectorXd erreurXd;

  for(int N = 2; N <= maximumImages; N++){

    Vector3d pas = (arrive - depart)/(N-1);

    if(printer){
      printer << N ;
    }

    if(printer2D){
      printer2D << N ;
    }

    int mesures2d = maximumEssais * 4 * N;
    int mesures3d = maximumEssais * 4;
    
    
    
    for(int b = 0; b < nombreBruit ; b++){

      if(N==2){

	if(printer2v){
	  printer2v << niveauxBruit[b];
	}

	if(printer2v2D){
	  printer2v2D << niveauxBruit[b] ;
	}
      }

      cov = niveauxBruit[b] * niveauxBruit[b];

      covariance = cov * MatrixXd::Identity(8,8);

      cibleNoisy.setCovariance(covariance);

      erreur3D = 0;

      erreur2D = 0;

      num2D = 0;

      num3D = 0;


      for(int j = 0; j < maximumEssais; j++){

	estimateur.eraseMemory();

	for(int i = 0; i < N; i++){

	  MyClock::addToTime(dt);

	  position = depart + i * pas;

	  se3.setDeplacement(position);

	  oeil.setRepere(se3);

	  estimateur.useTracker();

	}

	if(num3D>mesures3d){
	  cout << "Houston we have a 3D problem ! Num:" << num3D << " Mes:" <<mesures3d <<endl;
	}

	if(num2D>mesures2d){
	  cout << "Houston we have a 2D problem ! Num:" << num2D << " Mes:" <<mesures2d  << endl;
	}

	erreurXd = estimateur.getEstimation() - solution;

	stat3D[num3D++] = erreurXd.block<3,1>(0,0).norm();
	stat3D[num3D++] = erreurXd.block<3,1>(3,0).norm();
	stat3D[num3D++] = erreurXd.block<3,1>(6,0).norm();
	stat3D[num3D++] = erreurXd.block<3,1>(9,0).norm();

	for(int i = 0; i < N; i++){

	  position = depart + i * pas;

	  se3.setDeplacement(position);

	  oeil.setRepere(se3);

	  //estimateur.useTracker();

	  erreurXd = estimateur.getFunction() - cible.getFunction();

	  stat2D[num2D++] = erreurXd.block<2,1>(0,0).norm();
	  stat2D[num2D++] = erreurXd.block<2,1>(2,0).norm();
	  stat2D[num2D++] = erreurXd.block<2,1>(4,0).norm();
	  stat2D[num2D++] = erreurXd.block<2,1>(6,0).norm();


	}
	
      }

      sort(stat2D.begin(),stat2D.begin()+mesures2d);
      sort(stat3D.begin(),stat3D.begin()+mesures3d);

      if(mesures3d%2==0)
	erreur3D  = 0.5 *(stat3D[mesures3d/2-1] + stat3D[mesures3d/2-1]);
      else
	erreur3D  = stat3D[mesures3d/2];

      if(mesures2d%2==0)
	erreur2D  = 0.5 *(stat2D[mesures2d/2-1] + stat2D[mesures2d/2-1]);
      else
	erreur2D  = stat2D[mesures2d/2];

      if(printer){
	printer << " " <<erreur3D;
      }

      if(printer2D){
	printer2D << " " <<erreur2D;
      }

      if(N==2){

	if(printer2v){
	  printer2v << " " <<erreur3D  <<endl;
	}

	if(printer2v2D){
	  printer2v2D << " " <<erreur2D << endl;
	}

      }

    }

    if(printer){
      printer << endl;
    }

    if(printer2D){
      printer2D << endl;
    }

  }

  printer.close();
  printer2D.close();
  printer2v.close();
  printer2v2D.close();

  
}


