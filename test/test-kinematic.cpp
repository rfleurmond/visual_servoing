#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK           // A ajouter dans le cas d'une liaison dynamique à Boost Test
#endif

#define BOOST_TEST_MODULE MyTest      // Nom du module de test

#include <boost/test/unit_test.hpp>
#include "../robots.h"

using namespace std;
using namespace Eigen;

BOOST_AUTO_TEST_CASE(MDD) { 

  //int main(int argc, char * argv[]){


  int max = 3;

  cout << "Test du modele cinematique du robot " << endl;

  GeoRobot filament;

  Corps * c0 = new Corps();

  filament.setBase(c0);
  
  Rotoide * l1 = new Rotoide("ceinture");

  filament.addLiaison(l1);

  
  Repere r1(VectRotation(-M_PI/2,0,0),Vector3d(0,0.049,0));

  Corps * c1 = new Corps(r1);

  filament.addCorps(c1);

  Rotoide * l2 = new Rotoide("epaule");

  l2->setInitialState(-M_PI/2*VectorXd::Ones(1));

  l2->goToInitialState();

  filament.addLiaison(l2);


  Repere r2(VectRotation(0,0,0),Vector3d(0.400,0,0));

  Corps * c2 = new Corps(r2);
 
  filament.addCorps(c2);

  
  Rotoide *l3 = new Rotoide("coude");

  l3->setInitialState(M_PI/2*VectorXd::Ones(1));

  l3->goToInitialState();

  filament.addLiaison(l3);

  Repere r3(VectRotation(M_PI/2,0,0),Vector3d(0,-0.400,0));

  Corps * c3 = new Corps(r3);

  filament.addCorps(c3);


  Rotoide * l4 = new Rotoide("poignet 1");

  filament.addLiaison(l4);

  
  Repere r4(VectRotation(-M_PI/2,0,0),Vector3d(0,0,0));

  Corps * c4 = new Corps(r4);

  filament.addCorps(c4);

  
  Rotoide * l5 = new Rotoide("poignet 2");

  filament.addLiaison(l5);

  
  Repere r5(VectRotation(M_PI/2,0,0),Vector3d(0,0,0));

  Corps * c5 = new Corps(r5);

  filament.addCorps(c5);


  Rotoide * l6 = new Rotoide("poignet 3");

  filament.addLiaison(l6);

  
  Repere r6(VectRotation(0,0,0),Vector3d(0,0,0.065));

  Corps * c6 = new Corps(r6);

  filament.addCorps(c6);

  MatrixXd J1 = MatrixXd::Zero(6,6);

  MatrixXd J2 = MatrixXd::Zero(6,6);

  MatrixXd EJ = MatrixXd::Zero(6,6);


  
  int degres = filament.getTotalDegres();

  cout << "Somme des degres:" << degres << endl;

  VectorXd q = VectorXd::Zero(degres);

  cout<<"Modele cinematique"<<endl;

  MyKineModel impulsion(&filament);

  J1 = impulsion.getKinematicJacobian(q);

  cout<<J1<<endl;

  J2<<
    -0.049,  0.465,  0.465,  0    ,  0.065,  0    ,
    0.4   ,  0    ,  0    ,  0    ,  0    ,  0    ,
    0     , -0.4  ,  0    ,  0    ,  0    ,  0    ,
    0     ,  0    ,  0    ,  0    ,  0    ,  0    ,
    0     ,  1    ,  1    ,  0    ,  1    ,  0    ,
    1     ,  0    ,  0    ,  1    ,  0    ,  1    ;
     
  EJ = J1 -J2;

  BOOST_CHECK(isEqual( EJ.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout << "Pour les coordonness articulaires:\n" <<q<<"\n\n"<<endl;

    
  cout<<"Modele cinematique pour "<<max<<" degres"<<endl;


  J1 = impulsion.getKinematicJacobian(q,max);

  cout<<J1<<endl;

  J2<<
    -0.049,  0.4  ,  0.4  ,  0    ,  0    ,  0    ,
    0.4   ,  0    ,  0    ,  0    ,  0    ,  0    ,
    0     , -0.4  ,  0    ,  0    ,  0    ,  0    ,
    0     ,  0    ,  0    ,  0    ,  0    ,  0    ,
    0     ,  1    ,  1    ,  0    ,  0    ,  0    ,
    1     ,  0    ,  0    ,  0    ,  0    ,  0    ;
     
  EJ = J1 -J2;

  BOOST_CHECK(isEqual( EJ.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout << "Pour les coordonness articulaires:\n" <<q<<"\n\n"<<endl;



  q<< 0, -M_PI/2, M_PI/2, 0, 0, 0;

  cout<<"Modele cinematique"<<endl;

  J1 = impulsion.getKinematicJacobian(q);

  cout<<J1<<endl;

  J2<<
    -0.049,  0.865,  0.465,  0    ,  0.065,  0    ,
    0     ,  0    ,  0    ,  0    ,  0    ,  0    ,
    0     ,  0    ,  0    ,  0    ,  0    ,  0    ,
    0     ,  0    ,  0    ,  0    ,  0    ,  0    ,
    0     ,  1    ,  1    ,  0    ,  1    ,  0    ,
    1     ,  0    ,  0    ,  1    ,  0    ,  1    ;
     
  EJ = J1 -J2;

  BOOST_CHECK(isEqual( EJ.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout << "Pour les coordonness articulaires:\n" <<q<<"\n\n"<<endl;

    
  cout<<"Modele cinematique pour "<<max<<" degres"<<endl;


  J1 = impulsion.getKinematicJacobian(q,max);

  cout<<J1<<endl;

  J2<<
    -0.049,  0.8  ,  0.4  ,  0    ,  0    ,  0    ,
    0     ,  0    ,  0    ,  0    ,  0    ,  0    ,
    0     ,  0    ,  0    ,  0    ,  0    ,  0    ,
    0     ,  0    ,  0    ,  0    ,  0    ,  0    ,
    0     ,  1    ,  1    ,  0    ,  0    ,  0    ,
    1     ,  0    ,  0    ,  0    ,  0    ,  0    ;
     
  EJ = J1 -J2;

  BOOST_CHECK(isEqual( EJ.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout << "Pour les coordonness articulaires:\n" <<q<<"\n\n"<<endl;

}
