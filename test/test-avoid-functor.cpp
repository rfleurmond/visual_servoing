#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK           // A ajouter dans le cas d'une liaison dynamique à Boost Test
#endif

#define BOOST_TEST_MODULE MyTest      // Nom du module de test

#include <boost/test/unit_test.hpp>
#include "../tools.h"

using namespace std;

BOOST_AUTO_TEST_CASE(AvoidFunctor) { 

  //int main(){

  int borne = 100;

  double minC = 0.15;

  double maxC = 0.35;

  Avoid bounds(maxC,minC);

  bounds.setBounds(0,borne);

  double x = 0;

  for(int i = 0; i < 11 ; i++){

    x = 10*i;
    
    cout << "F( " << x << " ) = "<< bounds(x) <<" and J(x) = " << bounds.jacobien(x) << endl;

    if( x < maxC * borne){

      BOOST_CHECK( bounds(x) > 0);
      BOOST_CHECK( bounds.jacobien(x) < 0);
    
    }

    else if( x > (1 - maxC) * borne){

      BOOST_CHECK( bounds(x) > 0);
      BOOST_CHECK( bounds.jacobien(x)  > 0);
    
    }

    else{

      BOOST_CHECK( isEqual(bounds(x),0,1e-6) );
      BOOST_CHECK( isEqual(bounds.jacobien(x),0,1e-6));
    
    }

  }
  
  
}
