#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK           // A ajouter dans le cas d'une liaison dynamique à Boost Test
#endif

#define BOOST_TEST_MODULE MyTest      // Nom du module de test

#include <boost/test/unit_test.hpp>
#include "../vservoing.h"

using namespace Eigen;
using namespace std;

BOOST_AUTO_TEST_CASE(VisualOrientedDroite) { 

  //int main(void){

  Camera oeil;

  Vector3d premier(10,-4,0);

  Vector3d deuxieme(10,0,-3);

  double angle = M_PI/2 -atan(0.75);

  LineWithK indice(premier,deuxieme,deuxieme);

  indice.attachToCamera(&oeil);

  cout<<"La droite peut elle etre vue par la camera? Reponse: "<<indice.itCanBeSeen()<<endl;

  VectorXd F = indice.getFunction();

  MatrixXd L = indice.getInteractionMatrix();

  VectorXd V = VectorXd::Zero(3);

  cout<<"Valeur des indices visuels \n"<<F<<"\n"<<endl;

  V << 0.24, angle, 0.18;

  V = V - F;

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout<<"Matrice d'interaction des indices visuels\n"<<L<<"\n"<<endl;

  MatrixXd M = MatrixXd::Zero(3,6);

  M << 
   0.024, 0.06, 0.08,  0, -0.84608, 0.63456,
       0,    0,    0, -1,    0.144,   0.192,
   0.018,-0.08, 0.06,  0, -0.68856,-0.84608;
  // Deux dernieres colonnes a verifier !!

  M = M - L;

  BOOST_CHECK(isEqual( M.array().abs().maxCoeff() , 0 , 1e-6));


  Vector3d first(10,4,0);

  Vector3d second(10,0,3);

  double angle2 = -M_PI/2 -atan(0.75);

  LineWithK indice2(first,second,second);

  indice2.attachToCamera(&oeil);

  cout<<"La droite peut elle etre vue par la camera? Reponse: "<<indice2.itCanBeSeen()<<endl;

  VectorXd F2 = indice2.getFunction();

  MatrixXd L2 = indice2.getInteractionMatrix();

  VectorXd V2 = VectorXd::Zero(3);

  cout<<"Valeur des indices visuels \n"<<F2<<"\n"<<endl;

  V2 << 0.24, angle2, 0.18;

  V2 = V2 - F2;

  BOOST_CHECK(isEqual( V2.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout<<"Matrice d'interaction des indices visuels\n"<<L2<<"\n"<<endl;

  MatrixXd M2 = MatrixXd::Zero(3,6);

  M2 << 
   0.024,-0.06,-0.08,  0,  0.84608,-0.63456,
       0,    0,    0, -1,   -0.144,  -0.192,
   0.018, 0.08,-0.06,  0,  0.68856, 0.84608;
  // Deux dernieres colonnes a verifier !!

  M2 = M2 - L2;

  BOOST_CHECK(isEqual( M2.array().abs().maxCoeff() , 0 , 1e-6));
  
}
