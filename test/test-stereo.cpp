/*
 * test_stereo_camera.cpp
 *
 *  Created on: Jun 13, 2012
 *      @author Renliw Fleurmond
 */

#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK           // A ajouter dans le cas d'une liaison dynamique à Boost Test
#endif

#define BOOST_TEST_MODULE MyTest      // Nom du module de test

#include <boost/test/unit_test.hpp>
#include "../vision.h"

using namespace std;
using namespace Eigen;


BOOST_AUTO_TEST_CASE(StereoVision) { 

  //int main(void){

  StereoCamera binocle;

  Vector3d point, EP;

  point << 5, -0.05, -1.6;

  cout<<"Les coordonnees d'un point:\n"<<point<<endl;

  Vector2d imgG,imgD, II, EI;

  imgG = binocle.getCleanLeftImage(point);

  imgD = binocle.getCleanRightImage(point);

  cout<<"Image de gauche = \n"<<imgG<<endl;

  II << 0.01 , 0.32 ;

  EI = imgG - II;

  BOOST_CHECK(isEqual( EI(0) , 0 , 1e-6));
  
  BOOST_CHECK(isEqual( EI(1) , 0 , 1e-6));
  

  cout<<"Image de droite = \n"<<imgD<<endl;

  II << -0.01 , 0.32 ;

  EI = imgD - II;

  BOOST_CHECK(isEqual( EI(0) , 0 , 1e-6));
   
  BOOST_CHECK(isEqual( EI(1) , 0 , 1e-6));

  cout<<"confiance sur la satisfaction de la contrainte epipolaire="<<binocle.beliefEpipolar(imgG,imgD)<<endl;

  cout<<"Est ce oui ou non la contrainte epipolaire est respectee? "<<binocle.areEpipolar(imgG,imgD)<<endl;

  cout<<"Les coordonnees du point correspondant aux deux images:\n"<< binocle.getPoint(imgG,imgD)<<endl;

  BOOST_CHECK(isEqual( binocle.beliefEpipolar(imgG,imgD) , 1 , 1e-6));
  
  EP = point - binocle.getPoint(imgG,imgD);

  BOOST_CHECK(isEqual( EP(0) , 0 , 1e-6));

  BOOST_CHECK(isEqual( EP(1) , 0 , 1e-6));

  BOOST_CHECK(isEqual( EP(2) , 0 , 1e-6));


  RadialDistorsion illusion(-0.05);

  imgG = binocle.getDirtyLeftImage(illusion,point);

  imgD = binocle.getDirtyRightImage(illusion,point);

  cout<<"Image de gauche deformee= \n"<<imgG<<endl;

  cout<<"Image de droite deformee= \n"<<imgD<<endl;

  cout<<"confiance sur la satisfaction de la contrainte epipolaire="<<binocle.beliefEpipolar(illusion,imgG,illusion,imgD)<<endl;

  cout<<"Est ce oui ou non la contrainte epipolaire est respectee? "<<binocle.areEpipolar(illusion,imgG,illusion,imgD)<<endl;

  cout<<"Les coordonnees du point correspondant aux deux images:\n"<<binocle.getPoint(illusion,imgG,illusion,imgD)<<endl;

}



