#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK           // A ajouter dans le cas d'une liaison dynamique à Boost Test
#endif

#define BOOST_TEST_MODULE MyTest      // Nom du module de test

#include <boost/test/unit_test.hpp>
#include "../tools.h"

using namespace Eigen;

BOOST_AUTO_TEST_CASE(PointCoordinates) {  // Ce test s'appelle PointCoordinates

  Vector3d point(1,2,3);

  std::cout<<point<<std::endl;

  BOOST_CHECK_CLOSE(point(0), 1.0, 1e-8);       

  BOOST_CHECK_CLOSE(point(1), 2.0, 1e-8);      

  BOOST_CHECK_CLOSE(point(2), 3.0, 1e-8);      

}
