#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK           // A ajouter dans le cas d'une liaison dynamique à Boost Test
#endif

#define BOOST_TEST_MODULE MyTest      // Nom du module de test

#include <boost/test/unit_test.hpp>
#include "../vservoing.h"

using namespace Eigen;
using namespace std;

BOOST_AUTO_TEST_CASE(VisualOrientedDroite) { 

  //int main(void){

  Camera oeil;

  Vector3d premier(10,-4,0);

  Vector3d deuxieme(10,0,-3);

  double angle = M_PI/2 -atan(0.75);

  LineFeature cible(premier,deuxieme);

  cible.attachToCamera(&oeil);

  VectorXd feat;

  MatrixXd L;

  VectorXd V = VectorXd::Zero(2);

  MatrixXd M = MatrixXd::Zero(2,6);

  MatrixXd artisan = MatrixXd::Zero(2,6);

  
  
  
  cout<<"La droite peut elle etre vue par la camera? Reponse: "<<cible.itCanBeSeen()<<endl;

  
  FlyingSystem peterpan;

  VisualTask tache(&cible, &peterpan);

  tache.setModeFixation(VisualTask::EMBARQUE);

  tache.setPeriod(1.0/20);

  tache.setLambda(0.2);

  tache.setMinimumErrorNorm(1e-6);

  /// INSTANT INITIAL

  cout << "!!!!!!!!!!! INSTANT INITIAL !!!!!!!!!!!!!"<<endl;

  VectorXd etat = VectorXd::Zero(6);

  tache.setState(etat);

  peterpan.setState(etat);

  feat = cible.getFunction();

  L = cible.getInteractionMatrix();

  cout<<"Valeurs des cibles visuels \n"<<feat<<"\n"<<endl;

  V << 0.24, angle;

  V = V - feat;

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout<<"Matrice d'interaction des cibles visuels\n"<<L<<"\n"<<endl;

  artisan << 
   0.024,    0.06, 0.08,      0, -0.84608, 0.63456,
       0,    0,       0,     -1,    0.144,   0.192;
  // Deux dernieres colonnes a verifier !!

  M = artisan - L;

  BOOST_CHECK(isEqual( M.array().abs().maxCoeff() , 0 , 1e-6));


  /////// PREMIER QUART DE TOUR

  etat << -M_PI/2, 0, 0, 0, 0, 0;

  tache.setState(etat);

  peterpan.setState(etat);

  cout << "!!!!!!!!!!! UN QUART DE TOUR !!!!!!!!!!!!!"<<endl;

  feat = cible.getFunction();

  L = cible.getInteractionMatrix();

  cout<<"Valeurs des cibles visuels \n"<<feat<<"\n"<<endl;

  V << 0.24, angle + M_PI/2;

  V = V - feat;

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout<<"Matrice d'interaction des cibles visuels\n"<<L<<"\n"<<endl;

  artisan << 
   0.024,   -0.08, 0.06,      0, -0.63456,-0.84608,
       0,    0,       0,     -1,   -0.192,   0.144;
  // Deux dernieres colonnes a verifier !!

  M = artisan - L;

  BOOST_CHECK(isEqual( M.array().abs().maxCoeff() , 0 , 1e-6));

  
  /////// DEUXIEME QUART DE TOUR

  etat << -M_PI, 0, 0, 0, 0, 0;

  tache.setState(etat);

  peterpan.setState(etat);

  cout << "!!!!!!!!!!! UN DEMI TOUR !!!!!!!!!!!!!"<<endl;

  feat = cible.getFunction();

  L = cible.getInteractionMatrix();

  cout<<"Valeurs des cibles visuels \n"<<feat<<"\n"<<endl;

  V << 0.24, angle - M_PI;

  V = V - feat;

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout<<"Matrice d'interaction des cibles visuels\n"<<L<<"\n"<<endl;

  artisan << 
   0.024,   -0.06,-0.08,      0,  0.84608,-0.63456,
       0,    0,       0,     -1,   -0.144,  -0.192;
  // Deux dernieres colonnes a verifier !!

  M = artisan - L;

  BOOST_CHECK(isEqual( M.array().abs().maxCoeff() , 0 , 1e-6));

  
  /////// TROISIEME QUART DE TOUR

  etat << -3*M_PI/2, 0, 0, 0, 0, 0;

  tache.setState(etat);

  peterpan.setState(etat);

  cout << "!!!!!!!!!!! TROIS QUARTS DE TOUR !!!!!!!!!!!!!"<<endl;

  feat = cible.getFunction();

  L = cible.getInteractionMatrix();

  cout<<"Valeurs des cibles visuels \n"<<feat<<"\n"<<endl;

  V << 0.24, angle - M_PI/2;

  V = V - feat;

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout<<"Matrice d'interaction des cibles visuels\n"<<L<<"\n"<<endl;

  artisan << 
   0.024,    0.08,-0.06,      0,  0.63456, 0.84608,
       0,    0,       0,     -1,    0.192,  -0.144;
  // Deux dernieres colonnes a verifier !!

  M = artisan - L;

  BOOST_CHECK(isEqual( M.array().abs().maxCoeff() , 0 , 1e-6));

  
  /////// TOUR COMPLET

  etat << -2*M_PI, 0, 0, 0, 0, 0;

  tache.setState(etat);

  peterpan.setState(etat);

  cout << "!!!!!!!!!!! UN TOUR COMPLET !!!!!!!!!!!!!"<<endl;

  feat = cible.getFunction();

  L = cible.getInteractionMatrix();

  cout<<"Valeurs des cibles visuels \n"<<feat<<"\n"<<endl;

  V << 0.24, angle;

  V = V - feat;

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout<<"Matrice d'interaction des cibles visuels\n"<<L<<"\n"<<endl;

  artisan << 
   0.024,    0.06, 0.08,      0, -0.84608, 0.63456,
       0,    0,       0,     -1,    0.144,   0.192;
  // Deux dernieres colonnes a verifier !!

  M = artisan - L;

  BOOST_CHECK(isEqual( M.array().abs().maxCoeff() , 0 , 1e-6));

}


