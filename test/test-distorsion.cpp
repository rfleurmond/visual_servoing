
/*
 * test_distorsion.cpp
 *
 *  Created on: Jun 13, 2012
 *      @author Renliw Fleurmond
 */


#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK           // A ajouter dans le cas d'une liaison dynamique à Boost Test
#endif

#define BOOST_TEST_MODULE MyTest      // Nom du module de test

#include <boost/test/unit_test.hpp>
#include "../vision.h"

using namespace std;
using namespace Eigen;


BOOST_AUTO_TEST_CASE(DistorsionCheck) { 

  //int main(){

  Vector3d p1(10,8,0);

  Vector3d p2(10,0,8);

  Vector3d p3(10,12,9);

  Vector2d IP,II,EI;

  Camera oeil;

  double k = -0.05;

  BOOST_CHECK(oeil.canSee(p1));

  BOOST_CHECK(oeil.canSee(p2));

  BOOST_CHECK(oeil.canSee(p3) == false);

  RadialDistorsion illusion(k);
  
  IP = oeil.getDirtyImage(illusion,p1);

  II << -0.8 , 0 ;

  II *= 1 + k * 0.64;

  EI  = IP - II ;

  cout << EI.transpose() << endl;

  BOOST_CHECK(isEqual( EI(0) , 0 , 1e-6));
  
  BOOST_CHECK(isEqual( EI(1) , 0 , 1e-6));
  
  
  IP = oeil.getDirtyImage(illusion,p2);

  II << 0 , -0.8 ;

  II *= 1 + k * 0.64;

  EI  = IP - II ;

  cout << EI.transpose() << endl;

  BOOST_CHECK(isEqual( EI(0) , 0 , 1e-6));
  
  BOOST_CHECK(isEqual( EI(1) , 0 , 1e-6));
  


  IP = oeil.getDirtyImage(illusion,p3);

  II << -1.2 ,-0.9 ;

  II *= 1 + k * 9 * 0.25;

  EI  = IP - II ;

  cout << EI.transpose() << endl;

  BOOST_CHECK(isEqual( EI(0) , 0 , 1e-6));
  
  BOOST_CHECK(isEqual( EI(1) , 0 , 1e-6));
  
  
  IntrinsicCam optique(240,240,324,240); 

  oeil.setIntrinsicParameters(optique);

  VectorXd param(1);

  param << 1e-7;

  illusion.setParameters(param);


  BOOST_CHECK(oeil.canSee(p1));

  BOOST_CHECK(oeil.canSee(p2));

  BOOST_CHECK(oeil.canSee(p3));

  
  IP = oeil.getDirtyImage(illusion,p1);

  II << 324 - 192 , 240 - 0 ;

  EI  = IP - II ;

  cout << EI.transpose() << endl;

  BOOST_CHECK(isEqual( EI(0) , 0 , 1));
  
  BOOST_CHECK(isEqual( EI(1) , 0 , 1));
  
  
  IP = oeil.getDirtyImage(illusion,p2);

  II << 324 - 0 , 240 - 192 ;

  EI  = IP - II ;

  cout << EI.transpose() << endl;

  BOOST_CHECK(isEqual( EI(0) , 0 , 1));
  
  BOOST_CHECK(isEqual( EI(1) , 0 , 1));
  


  IP = oeil.getDirtyImage(illusion,p3);

  II << 324 - 288 , 240 - 216 ;

  EI  = IP - II ;

  cout << EI.transpose() << endl;

  BOOST_CHECK(isEqual( EI(0) , 0 , 5));
  
  BOOST_CHECK(isEqual( EI(1) , 0 , 5));
  
  
}
