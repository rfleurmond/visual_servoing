#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK           // A ajouter dans le cas d'une liaison dynamique à Boost Test
#endif

#define BOOST_TEST_MODULE MyTest      // Nom du module de test

#include <iostream>
#include <boost/test/unit_test.hpp>
#include "../tools.h"

using namespace std;
using namespace Eigen;

BOOST_AUTO_TEST_CASE(ThetaU) { 

  //int main(){

  Matrix3d mat = Matrix3d::Zero();

  mat <<
    1,  0,  0,
    0,  0,  1,
    0, -1,  0;

  VectRotation vec(mat);

  Matrix3d mat2 = vec.getMatrix();

  Vector3d axe = vec.getAxis();

  Vector3d vect = vec.getVector();

  double angle = vec.getAngle();
  
  cout << "La matrice \n" <<  mat2 << endl;

  cout <<" L'axe est "<< axe.transpose() << "," <<endl;

  cout <<" le vecteur est "<< vect.transpose() << endl;

  cout <<" Et l'angle est " << angle << endl;

  BOOST_CHECK(isEqual( angle , M_PI/2 , 1e-6));

  mat2 -= mat;

  BOOST_CHECK(isEqual( mat2.minCoeff() , 0       , 1e-6));

  BOOST_CHECK(isEqual( mat2.maxCoeff() , 0       , 1e-6));

  BOOST_CHECK(isEqual( vect(0)         , -M_PI/2 , 1e-6));

  BOOST_CHECK(isEqual( vect(1)         ,  0      , 1e-6));

  BOOST_CHECK(isEqual( vect(2)         , 0       , 1e-6));



  Quaterniond q2(0.679288,0.679288,-0.196387,0.196387);

  VectRotation vec2(q2);

  mat2 = vec2.getMatrix();

  axe = vec2.getAxis();

  vect = vec2.getVector();

  angle = vec2.getAngle();
  

  cout << "La matrice \n" <<  mat2 << endl;

  cout <<" L'axe est "<< axe.transpose() << "," <<endl;

  cout <<" le vecteur est "<< vect.transpose() << endl;

  cout <<" Et l'angle est " << angle << endl;

  BOOST_CHECK(isEqual( angle   , 1.64801   , 1e-5));

  BOOST_CHECK(isEqual( vect(0) , 1.52543   , 1e-5));

  BOOST_CHECK(isEqual( vect(1) , -0.441014 , 1e-6));

  BOOST_CHECK(isEqual( vect(2) , 0.441014  , 1e-6));



  VectRotation ab = vec.inverse() * vec2;

  mat2 = ab.getMatrix();

  axe = ab.getAxis();

  vect = ab.getVector();

  angle = ab.getAngle();
  
  cout << "\n" << ab << endl;

  cout << "La matrice \n" <<  mat2 << endl;

  cout <<" L'axe est "<< axe.transpose() << "," <<endl;

  cout <<" le vecteur est "<< vect.transpose() << endl;

  cout <<" Et l'angle est " << angle << endl;

  BOOST_CHECK(isEqual( angle   , M_PI      , 1e-6));

  BOOST_CHECK(isEqual( vect(0) , 3.018     , 1e-3));

  BOOST_CHECK(isEqual( vect(1) , -0.872524 , 1e-6));

  BOOST_CHECK(isEqual( vect(2) , 0         , 1e-6));


  Quaterniond q3(ab.getAngleAxisd());

  cout<< "Quaternion tot x="<< q3.x() <<" y " <<q3.y()<<" z " <<q3.z()<<" w " <<q3.w()<< endl;

}


