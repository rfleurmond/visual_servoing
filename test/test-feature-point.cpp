#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK           // A ajouter dans le cas d'une liaison dynamique à Boost Test
#endif

#define BOOST_TEST_MODULE MyTest      // Nom du module de test

#include <boost/test/unit_test.hpp>
#include "../vservoing.h"

using namespace Eigen;
using namespace std;

BOOST_AUTO_TEST_CASE(VisualPoint) { 

  //int main(void){

  Camera oeil;

  MatrixXd points(4,3);

  points <<
    10, 0, 0,
    10, 0, 6,
    10, 8, 0,
    10, 8, 6;

  VectorXd liste(12,1);

  liste <<
    10, 0, 0,
    10, 0, 6,
    10, 8, 0,
    10, 8, 6;


  MultiPointFeature p14(points);

  p14.attachToCamera(&oeil);
  
  PointFeature p1(Vector3d(10,0,0));

  p1.attachToCamera(&oeil);
  
  PointFeature p2(Vector3d(10,0,6));
  
  p2.attachToCamera(&oeil);
  
  PointFeature p3(Vector3d(10,8,0));
  
  p3.attachToCamera(&oeil);
  
  PointFeature p4(Vector3d(10,8,6));
  
  p4.attachToCamera(&oeil);
  
  
  VectorXd F1 = VectorXd::Zero(2);

  VectorXd F2 = F1;

  VectorXd F3 = F2;

  VectorXd F4 = F3;

  VectorXd EF = F4;

  VectorXd FF = VectorXd::Zero(8);

  MatrixXd L1  = MatrixXd::Zero(2,6);

  MatrixXd L2 = L1;

  MatrixXd L3 = L2;

  MatrixXd L4 = L3;

  MatrixXd EL, LL;

  F1 <<    0,    0;

  F2 <<    0, -0.6;

  F3 << -0.8,    0;

  F4 << -0.8, -0.6;

  FF.block<2,1>(0,0) = F1;
  FF.block<2,1>(2,0) = F2;
  FF.block<2,1>(4,0) = F3;
  FF.block<2,1>(6,0) = F4;

  
  L1 <<
    0, 0.1,   0, 0, 0, 1,
    0,   0, 0.1, 0,-1, 0;


  L2 <<
        0,  0.1,   0, -0.6,    0, 1,
    -0.06,    0, 0.1,    0,-1.36, 0;


  L3 <<
    -0.08, 0.1,   0,   0,  0, 1.64,
        0,   0, 0.1, 0.8, -1,    0;


  L4 <<
    -0.08, 0.1,   0, -0.6, -0.48, 1.64,
    -0.06,   0, 0.1,  0.8, -1.36, 0.48;

  
  LL = MatrixXd::Zero(8,6);

  LL.block<2,6>(0,0) = L1;
  LL.block<2,6>(2,0) = L2;
  LL.block<2,6>(4,0) = L3;
  LL.block<2,6>(6,0) = L4;


  cout << "Le point de coordonnées : "<< points.row(0) << endl;
  cout << "a pour image : " << p1.getFunction().transpose() << endl;
  cout << "et pour matrice d'interaction:\n"<<p1.getInteractionMatrix()<<"\n\n"<<endl;
  
  EF = F1 - p1.getFunction();

  BOOST_CHECK(isEqual( EF.array().abs().maxCoeff() , 0 , 1e-6));
  
  EL = L1 - p1.getInteractionMatrix();

  BOOST_CHECK(isEqual( EL.array().abs().maxCoeff() , 0 , 1e-6));
  

  cout << "Le point de coordonnées : "<< points.row(1) << endl;
  cout << "a pour image : " << p2.getFunction().transpose() << endl;
  cout << "et pour matrice d'interaction:\n"<<p2.getInteractionMatrix()<<"\n\n"<<endl;
  
  EF = F2 - p2.getFunction();

  BOOST_CHECK(isEqual( EF.array().abs().maxCoeff() , 0 , 1e-6));
  
  EL = L2 - p2.getInteractionMatrix();

  BOOST_CHECK(isEqual( EL.array().abs().maxCoeff() , 0 , 1e-6));
  

  cout << "Le point de coordonnées : "<< points.row(2) << endl;
  cout << "a pour image : " << p3.getFunction().transpose() << endl;
  cout << "et pour matrice d'interaction:\n"<<p3.getInteractionMatrix()<<"\n\n"<<endl;
  
  EF = F3 - p3.getFunction();

  BOOST_CHECK(isEqual( EF.array().abs().maxCoeff() , 0 , 1e-6));
  
  EL = L3 - p3.getInteractionMatrix();

  BOOST_CHECK(isEqual( EL.array().abs().maxCoeff() , 0 , 1e-6));
  


  cout << "Le point de coordonnées : "<< points.row(3) << endl;
  cout << "a pour image : " << p4.getFunction().transpose() << endl;
  cout << "et pour matrice d'interaction:\n"<<p4.getInteractionMatrix()<<"\n\n"<<endl;
  
  EF = F4 - p4.getFunction();

  BOOST_CHECK(isEqual( EF.array().abs().maxCoeff() , 0 , 1e-6));
  
  EL = L4 - p4.getInteractionMatrix();

  BOOST_CHECK(isEqual( EL.array().abs().maxCoeff() , 0 , 1e-6));
  

  cout << "L'ensemble des points sus-cités (dans l'ordre) : "<< endl;
  cout << "a pour image : \n" << p14.getFunction().transpose() << endl;
  cout << "et pour matrice d'interaction:\n"<<p14.getInteractionMatrix()<<"\n\n"<<endl;
  
  EF = FF - p14.getFunction();

  BOOST_CHECK(isEqual( EF.array().abs().maxCoeff() , 0 , 1e-6));

  EF = p14.getFunction() - p14.getFunction(liste);

  BOOST_CHECK(isEqual( EF.array().abs().maxCoeff() , 0 , 1e-6));
  
  EL = LL - p14.getInteractionMatrix();

  BOOST_CHECK(isEqual( EL.array().abs().maxCoeff() , 0 , 1e-6));

  /////// PARAMETRES INTRINSEQUES /////////////////////////////////////////////  

  IntrinsicCam optique(240,240,320,240); 

  cout << "!!!!!!!!!!!!!!!!!!!Changement de focale et de retine !!!!!!!!!!!!!!!!!!!!!!!"<<endl;

  oeil.setIntrinsicParameters(optique);
  
  /////// PARAMETRES INTRINSEQUES /////////////////////////////////////////////  

  F1 <<  320,  240;

  F2 <<  320,   96; //240 - 0.6 * 240

  F3 <<  128,  240; //320 - 0.8 * 240

  F4 <<  128,   96;

  FF.block<2,1>(0,0) = F1;
  FF.block<2,1>(2,0) = F2;
  FF.block<2,1>(4,0) = F3;
  FF.block<2,1>(6,0) = F4;

  // Multiplication par 240
  
  L1 <<
    0, 24,  0,  0,    0, 240,
    0,  0, 24,  0, -240,   0;

  L2 <<
        0,  24,   0, -144,      0, 240,
    -14.4,   0,  24,    0, -326.4,   0;

  L3 <<
    -19.2,  24,   0,   0,    0, 393.6,
        0,   0,  24, 192, -240,     0;

  L4 <<
    -19.2,  24,   0, -144, -115.2, 393.6,
    -14.4,   0,  24,  192, -326.4, 115.2;

  
  LL = MatrixXd::Zero(8,6);

  LL.block<2,6>(0,0) = L1;
  LL.block<2,6>(2,0) = L2;
  LL.block<2,6>(4,0) = L3;
  LL.block<2,6>(6,0) = L4;


  cout << "Le point de coordonnées : "<< points.row(0) << endl;
  cout << "a pour image : " << p1.getFunction().transpose() << endl;
  cout << "et pour matrice d'interaction:\n"<<p1.getInteractionMatrix()<<"\n\n"<<endl;
  
  EF = F1 - p1.getFunction();

  BOOST_CHECK(isEqual( EF.array().abs().maxCoeff() , 0 , 1e-6));
  
  EL = L1 - p1.getInteractionMatrix();

  BOOST_CHECK(isEqual( EL.array().abs().maxCoeff() , 0 , 1e-6));
  

  cout << "Le point de coordonnées : "<< points.row(1) << endl;
  cout << "a pour image : " << p2.getFunction().transpose() << endl;
  cout << "et pour matrice d'interaction:\n"<<p2.getInteractionMatrix()<<"\n\n"<<endl;
  
  EF = F2 - p2.getFunction();

  BOOST_CHECK(isEqual( EF.array().abs().maxCoeff() , 0 , 1e-6));
  
  EL = L2 - p2.getInteractionMatrix();

  BOOST_CHECK(isEqual( EL.array().abs().maxCoeff() , 0 , 1e-6));
  

  cout << "Le point de coordonnées : "<< points.row(2) << endl;
  cout << "a pour image : " << p3.getFunction().transpose() << endl;
  cout << "et pour matrice d'interaction:\n"<<p3.getInteractionMatrix()<<"\n\n"<<endl;
  
  EF = F3 - p3.getFunction();

  BOOST_CHECK(isEqual( EF.array().abs().maxCoeff() , 0 , 1e-6));
  
  EL = L3 - p3.getInteractionMatrix();

  BOOST_CHECK(isEqual( EL.array().abs().maxCoeff() , 0 , 1e-6));
  


  cout << "Le point de coordonnées : "<< points.row(3) << endl;
  cout << "a pour image : " << p4.getFunction().transpose() << endl;
  cout << "et pour matrice d'interaction:\n"<<p4.getInteractionMatrix()<<"\n\n"<<endl;
  
  EF = F4 - p4.getFunction();

  BOOST_CHECK(isEqual( EF.array().abs().maxCoeff() , 0 , 1e-6));
  
  EL = L4 - p4.getInteractionMatrix();

  BOOST_CHECK(isEqual( EL.array().abs().maxCoeff() , 0 , 1e-6));
  

  cout << "L'ensemble des points sus-cités (dans l'ordre) : "<< endl;
  cout << "a pour image : \n" << p14.getFunction().transpose() << endl;
  cout << "et pour matrice d'interaction:\n"<<p14.getInteractionMatrix()<<"\n\n"<<endl;
  
  EF = FF - p14.getFunction();

  BOOST_CHECK(isEqual( EF.array().abs().maxCoeff() , 0 , 1e-6));
  
  EL = LL - p14.getInteractionMatrix();

  BOOST_CHECK(isEqual( EL.array().abs().maxCoeff() , 0 , 1e-6));
  
}


