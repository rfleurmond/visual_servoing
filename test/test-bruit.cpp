#include "../tools/chompMultivariateGaussian.hpp"
#include "../tools/outils.h"
#include "../vision/graphiques-opencv.h"

using namespace std;
using namespace Eigen;

int main(){

  //Gestion de l'affichage des points en OpenCV

  CVEcran windows;

  windows.setNom("Tir aleatoire");

  windows.ouvrirFenetre();

  windows.nettoyer();

  cv::Scalar rouge = cv::Scalar(0,0,255);
  cv::Scalar bleue = cv::Scalar(255,0,0);

  int n = 3000; 

  double ratio = 0.1;

  // Gestion du bruit gaussien avec une classe
  //  qui utilise Boost Random

  double cov = 5;

  Matrix2d covariance = cov * Matrix2d::Identity(2,2);

  covariance(0,0) = 0.1 * cov;
  covariance(1,1) = 1.9 * cov;


  Vector2d mean = Vector2d::Zero(2);

  mean(0) = -5;

  MultivariateGaussian roulette(mean,covariance);

  Vector2d tir,I;
  
  int x,y;
  
  cout<<"Tir aleatoire Gaussien"<<endl;

  for(int i = 0; i<n ;i++){

    roulette.sample(tir);

    I = windows.getPoint(ratio * tir);
    
    x = (int)I(0);
    y = (int)I(1);
  
    cv::circle(windows.getImage(),cv::Point(x,y),1,rouge,-1);

    windows.affichage();

    cv::waitKey(15);

  }

  cout<<"Tir aleatoire  equiprobable sur un intervalle"<<endl;

  //cov = sqrt(cov);

  for(int i = 0; i<n ;i++){

    tir(0) = 5 + hasard(-cov, cov);
    tir(1) = hasard(-cov, cov);

    I = windows.getPoint(ratio * tir);
    
    x = (int)I(0);
    y = (int)I(1);
  
    cv::circle(windows.getImage(),cv::Point(x,y),1,bleue,-1);

    windows.affichage();

    cv::waitKey(3);

  }

  windows.fermerFenetre();

}
