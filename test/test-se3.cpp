/*
 * 
 *
 *  Created on: September 5, 2013
 *      @author Renliw Fleurmond
 */

#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK           // A ajouter dans le cas d'une liaison dynamique à Boost Test
#endif

#define BOOST_TEST_MODULE MyTest      // Nom du module de test

#include <boost/test/unit_test.hpp>
#include <iostream>
#include "../tools.h"

using namespace std;
using namespace Eigen;

BOOST_AUTO_TEST_CASE(SE3) { 

  //int main(){

  cout << "Test de la classe dediee au changement de repere " <<endl;

  Vector3d P(1,2,3);

  //Vector3d U(0.8,0.6,0);

  Repere Fa(VectRotation(0,0,atan(0.75)),Vector3d(4,3,8));

  Repere Fb(VectRotation(0,M_PI,0),Vector3d(0,2,0));

  Vector3d R3,V3,E3;

  cout << "Transformation de repere Fa:\n"<<Fa<<"\n"<<endl;

  R3 = Fa.getPointInParent(P);
  V3 << 3.6 , 5.2, 11 ;
  E3 = R3 - V3;

  
  cout << "Le point P de coordonnees [1 2 3] dans le repere Fa a pour coordonnes dans le repere monde"<<endl;
  cout << " ["<<R3.transpose()<<"]"<<endl;

  
  BOOST_CHECK(isEqual( E3(0) , 0 , 1e-6));
  BOOST_CHECK(isEqual( E3(1) , 0 , 1e-6));
  BOOST_CHECK(isEqual( E3(2) , 0 , 1e-6));
  

  R3 = Fa.getPointInME(P);
  V3 << -3, 1, -5;
  E3 = R3 - V3;

  cout << "Le point P de coordonnees [1 2 3] dans le repere monde a pour coordonnes dans le repere Fa"<<endl;
  cout << "["<<R3.transpose()<<"]\n"<<endl;

  BOOST_CHECK(isEqual( E3(0) , 0 , 1e-6));
  BOOST_CHECK(isEqual( E3(1) , 0 , 1e-6));
  BOOST_CHECK(isEqual( E3(2) , 0 , 1e-6));

  
  cout << "Transformation de repere Fb:\n"<<Fb<<endl;
  
  Repere Fc= Fa*Fb;

  cout <<"Le produit de Fa * Fb donne:\n"<<Fc<<endl;

  MatrixXd AB = Fa.getHomogenMatrix()*Fb.getHomogenMatrix();

  MatrixXd E16 = Fc.getHomogenMatrix() - AB;
  
  cout <<" tandis que le produit des matrices homogenes correspondantes donne:\n"<<AB<<"\n\n"<<endl;

  BOOST_CHECK(isEqual( E16.array().abs().maxCoeff() , 0 , 1e-6));
  
  

  Repere Fd= Fb*Fa;

  cout <<"Le produit de Fb * Fa donne:\n"<<Fd<<endl;

  MatrixXd BA = Fb.getHomogenMatrix()*Fa.getHomogenMatrix();

  E16 = AB - BA;

  cout <<" tandis que le produit des matrices homogenes correspondantes donne:\n"<<BA<<endl;

  cout << "Nous rappellons que la multiplication de matrices n'est pas commutative\n\n"<<endl;

  BOOST_CHECK( E16.array().abs().maxCoeff() > 0 );


  MatrixXd MI = Fb.inverse().getHomogenMatrix();

  MatrixXd IM = Fb.getHomogenMatrix().inverse();

  cout << "Transformation de repere inverse(Fb):\n"<<Fb.inverse()<<endl;

  cout <<" tandis que le l' inverse de la matrice B donne:\n"<<Fb.getHomogenMatrix().inverse()<<"\n\n"<<endl;

  E16 = MI - IM;

  BOOST_CHECK(isEqual( E16.array().abs().maxCoeff() , 0 , 1e-6));
  
  

  Repere Fe= Fa/Fb;

  cout <<"La division de Fa par Fb (Fa/FB) donne:\n"<<Fe<<endl;

  MatrixXd AFE = Fe.getHomogenMatrix();
  
  MatrixXd AdB = Fa.getHomogenMatrix()*Fb.getHomogenMatrix().inverse();

  cout <<" tandis que le produit des matrices A * inverse(B) donne:\n"<<AdB<<"\n\n"<<endl;

  E16 = AFE - AdB;

  BOOST_CHECK(isEqual( E16.array().abs().maxCoeff() , 0 , 1e-6));
  
  
  cout << "Si le torseur est exprime sous forme d' un vecteur de dimension 6 [vx vy vz wx wy wz]"<<endl;

  cout << "dans les exemples qui suivent l'on supposera que ce vecteur est la valeur du torseur (vitesse et omega)"<<endl;

  cout << "en l'origine d'un repere et que les differents reperes sont lies rigidement \n"<<endl;

  cout << "La matrice de transformation de torseur du repere Fa au repere monde (getKineticMatrix)"<<endl;

  cout << Fa.getKineticMatrix()<<endl;

  cout << "La matrice de transformation de torseur du repere monde au repere Fa (getInversKineticMatrix)"<<endl;

  cout << Fa.getInversKineticMatrix()<<endl;

  cout << "Le produit des deux precedentes matrices doit donner la matrice identite 6 x 6" <<endl;

  MatrixXd I6 = Fa.getKineticMatrix() * Fa.getInversKineticMatrix();

  cout << I6<<endl;

  cout << "\n" << endl;

  E16 = I6 - MatrixXd::Identity(6,6);

  BOOST_CHECK(isEqual( E16.array().abs().maxCoeff() , 0 , 1e-6));
  


  cout << "La matrice de transformation de torseur du repere Fb au repere monde (getKineticMatrix)"<<endl;

  cout << Fb.getKineticMatrix()<<endl;

  cout << "La matrice de transformation de torseur du repere monde au repere Fb (getInversKineticMatrix)"<<endl;

  cout << Fb.getInversKineticMatrix()<<endl;

  cout << "Le produit des deux precedentes matrices doit donner la matrice identite 6 x 6" <<endl;

  I6 = Fb.getKineticMatrix() * Fb.getInversKineticMatrix();

  cout << I6<<endl;

  E16 = I6 - MatrixXd::Identity(6,6);

  BOOST_CHECK(isEqual( E16.array().abs().maxCoeff() , 0 , 1e-6));
  
}
