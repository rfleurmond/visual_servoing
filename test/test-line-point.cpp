#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK           // A ajouter dans le cas d'une liaison dynamique à Boost Test
#endif

#define BOOST_TEST_MODULE MyTest      // Nom du module de test

#include <boost/test/unit_test.hpp>
#include "../vservoing.h"

using namespace Eigen;
using namespace std;

BOOST_AUTO_TEST_CASE(VisualOrientedDroite) { 

  //int main(void){

  Camera oeil;

  Vector3d premier(10,-4,0);

  Vector3d deuxieme(10,0,-3);

  double angle = M_PI/2 -atan(0.75);

  LineFeature indice(premier,deuxieme);

  indice.attachToCamera(&oeil);

  cout<<"La droite peut elle etre vue par la camera? Reponse: "<<indice.itCanBeSeen()<<endl;

  VectorXd feat = indice.getFunction();

  MatrixXd L = indice.getInteractionMatrix();

  VectorXd V = VectorXd::Zero(2);

  MatrixXd M = MatrixXd::Zero(2,6);

  cout<<"Valeurs des indices visuels \n"<<feat<<"\n"<<endl;

  V << 0.24, angle;

  V = V - feat;

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout<<"Matrice d'interaction des indices visuels\n"<<L<<"\n"<<endl;

  MatrixXd artisan = MatrixXd::Zero(2,6);

  artisan << 
   0.024,    0.06, 0.08,      0, -0.84608, 0.63456,
       0,    0,       0,     -1,    0.144,   0.192;
  // Deux dernieres colonnes a verifier !!

  M = artisan - L;

  BOOST_CHECK(isEqual( M.array().abs().maxCoeff() , 0 , 1e-6));
  

  VectorXd ref1 = VectorXd::Zero(2);

  ref1 << 1, M_PI/2;

  cout <<"Pour une valeur de reference:\n"<<ref1<<"\n"
       <<"L'erreur calculee est:\n"<<indice.getErrorBetweenMeAnd(ref1)
       <<"\n"<<endl;
  
  V << -0.76, angle - M_PI/2;

  V = V - indice.getErrorBetweenMeAnd(ref1);

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  VectorXd ref2 = VectorXd::Zero(2);

  ref2 << 1, 0;

  cout <<"Pour une valeur de reference:\n"<<ref2<<"\n"
       <<"L'erreur calculee est:\n"<< indice.getErrorBetweenMeAnd(ref2)
       <<"\n"<<endl;
  
  V << -0.76, angle;

  V = V - indice.getErrorBetweenMeAnd(ref2);

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  VectorXd ref3 = VectorXd::Zero(2);

  ref3 << 1, -0.4*M_PI;

  cout <<"Pour une valeur de reference:\n"<<ref3<<"\n"
       <<"L'erreur calculee est:\n"<< indice.getErrorBetweenMeAnd(ref3)
       <<"\n"<<endl;
  
  V << -0.76, angle + 0.4*M_PI;

  V = V - indice.getErrorBetweenMeAnd(ref3);

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));


///////////////////////////////////////////////////////////////////////////////////

  cout << "!!!!!!!!!!!!!!!!!!!!!!!!! TROISIEME QUADRANT ????????????????????????????" <<endl;

  
  Vector3d point1(10,4,0);

  Vector3d point2(10,0,3);

  LineFeature indice2(point1,point2);

  indice2.attachToCamera(&oeil);

  feat = indice2.getFunction();
    
  L = indice2.getInteractionMatrix();


  cout<<"Valeurs des indices visuels \n"<<feat<<"\n"<<endl;

  V << 0.24, angle - M_PI;

  V = V - feat;

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout<<"Matrice d'interaction des indices visuels\n"<<L<<"\n"<<endl;

  artisan << 
   0.024,-0.06,   -0.08,      0,  0.84608, -0.63456,
       0,    0,       0,     -1, -0.144  , -0.192  ;
  // Le dernier 3.0/4 est a verifier !!

  M = artisan - L;

  BOOST_CHECK(isEqual( M.array().abs().maxCoeff() , 0 , 1e-6));
  
  ref1 << 0.24, angle ;;

  cout <<"Pour une valeur de reference:\n"<<ref1<<"\n"
       <<"L'erreur calculee est:\n"<<indice2.getErrorBetweenMeAnd(ref1)
       <<"\n"<<endl;
  
  V << 0, - M_PI;

  V = V - indice2.getErrorBetweenMeAnd(ref1);

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));

  MatrixXd jmoins1;

  pinv(L,jmoins1);
  
  VectorXd commande = jmoins1 * indice2.getErrorBetweenMeAnd(ref1);

  cout << "la commande a appliquer alors est : "<<commande.transpose() <<endl;

  ref1 << 1, M_PI/2;

  cout <<"Pour une valeur de reference:\n"<<ref1<<"\n"
       <<"L'erreur calculee est:\n"<<indice2.getErrorBetweenMeAnd(ref1)
       <<"\n"<<endl;
  
  V << -0.76, angle + M_PI/2;

  V = V - indice2.getErrorBetweenMeAnd(ref1);

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  ref2 << 1, 0;

  cout <<"Pour une valeur de reference:\n"<<ref2<<"\n"
       <<"L'erreur calculee est:\n"<< indice2.getErrorBetweenMeAnd(ref2)
       <<"\n"<<endl;
  
  V << -0.76, angle - M_PI;

  V = V - indice2.getErrorBetweenMeAnd(ref2);

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  ref3 << 1, -0.4*M_PI;

  cout <<"Pour une valeur de reference:\n"<<ref3<<"\n"
       <<"L'erreur calculee est:\n"<< indice2.getErrorBetweenMeAnd(ref3)
       <<"\n"<<endl;
  
  V << -0.76, angle - 0.6* M_PI;

  V = V - indice2.getErrorBetweenMeAnd(ref3);

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));

  ////////////////////////////////////////////////////////////////////////////////////

  cout <<"!!!!!!!!!!!!!!!!!!!!! Changement de focale et de retine !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
  
  IntrinsicCam optique(240,240,320,240); 

  oeil.setIntrinsicParameters(optique);


  feat = indice.getFunction();
    
  L = indice.getInteractionMatrix();

  cout<<"Valeurs des indices visuels \n"<<feat<<"\n"<<endl;

  V << 0.24*240, angle;

  V = V - feat;

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout<<"Matrice d'interaction des indices visuels\n"<<L<<"\n"<<endl;

  artisan << 
   0.024,    0.06, 0.08,      0, -0.84608, 0.63456,
       0,    0,       0,     -1,    0.144,   0.192;
  artisan.row(0) *= 240;
  // Deux dernieres colonnes a verifier !!

 
  M = artisan - L;

  BOOST_CHECK(isEqual( M.array().abs().maxCoeff() , 0 , 1e-6));
  

  ref1 << 240, M_PI/2;

  cout <<"Pour une valeur de reference:\n"<<ref1<<"\n"
       <<"L'erreur calculee est:\n"<<indice.getErrorBetweenMeAnd(ref1)
       <<"\n"<<endl;
  
  V << 57.6-240, angle - M_PI/2;

  V = V - indice.getErrorBetweenMeAnd(ref1);

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  ref2 << 240, 0;

  cout <<"Pour une valeur de reference:\n"<<ref2<<"\n"
       <<"L'erreur calculee est:\n"<< indice.getErrorBetweenMeAnd(ref2)
       <<"\n"<<endl;
  
  V << 57.6-240, angle;

  V = V - indice.getErrorBetweenMeAnd(ref2);

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  ref3 << 240, -0.4*M_PI;

  cout <<"Pour une valeur de reference:\n"<<ref3<<"\n"
       <<"L'erreur calculee est:\n"<< indice.getErrorBetweenMeAnd(ref3)
       <<"\n"<<endl;
  
  V << 57.6-240, angle +0.4*M_PI;

  V = V - indice.getErrorBetweenMeAnd(ref3);

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));

}


