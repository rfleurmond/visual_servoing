#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK           // A ajouter dans le cas d'une liaison dynamique à Boost Test
#endif

#define BOOST_TEST_MODULE MyTest      // Nom du module de test

#include <boost/test/unit_test.hpp>
#include "../vservoing.h"

using namespace Eigen;
using namespace std;

BOOST_AUTO_TEST_CASE(VisualDroite) { 

  //int main(void){

  Camera oeil;

  Vector3d direction(0,1,0);

  Vector3d centre(8,0,6);

  Droite ligne(centre,direction);

  DroiteFeature indice(ligne);

  indice.attachToCamera(&oeil);

  cout<<"La droite peut elle etre vue par la camera? Reponse: "<<indice.itCanBeSeen()<<endl;

  cout<<"Affichage de la droite\n"<<ligne<<endl;

  VectorXd feat = indice.getFunction();

  MatrixXd L = indice.getInteractionMatrix();

  VectorXd V = VectorXd::Zero(2);

  MatrixXd M = MatrixXd::Zero(2,6);

  cout<<"Valeurs des indices visuels \n"<<feat<<"\n"<<endl;

  V << -0.75, M_PI/2;

  V = V - feat;

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout<<"Matrice d'interaction des indices visuels\n"<<L<<"\n"<<endl;

  MatrixXd artisan = MatrixXd::Zero(2,6);

  artisan << 
   -3.0/32,    0,  1.0/8,      0,-25.0/16,      0,
         0,    0,      0,     -1,        0,-3.0/4;
  // Le dernier 3.0/4 est a verifier !!

  M = artisan - L;

  BOOST_CHECK(isEqual( M.array().abs().maxCoeff() , 0 , 1e-6));
  

  VectorXd ref1 = VectorXd::Zero(2);

  ref1 << 1, M_PI/2;

  cout <<"Pour une valeur de reference:\n"<<ref1<<"\n"
       <<"L'erreur calculee est:\n"<<indice.getErrorBetweenMeAnd(ref1)
       <<"\n"<<endl;
  
  V << -1.75, 0;

  V = V - indice.getErrorBetweenMeAnd(ref1);

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  VectorXd ref2 = VectorXd::Zero(2);

  ref2 << 1, 0;

  cout <<"Pour une valeur de reference:\n"<<ref2<<"\n"
       <<"L'erreur calculee est:\n"<< indice.getErrorBetweenMeAnd(ref2)
       <<"\n"<<endl;
  
  V << -1.75, M_PI/2;

  V = V - indice.getErrorBetweenMeAnd(ref2);

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  VectorXd ref3 = VectorXd::Zero(2);

  ref3 << 1, -0.4*M_PI;

  cout <<"Pour une valeur de reference:\n"<<ref3<<"\n"
       <<"L'erreur calculee est:\n"<< indice.getErrorBetweenMeAnd(ref3)
       <<"\n"<<endl;
  
  V << -0.25, 0.1*M_PI;

  V = V - indice.getErrorBetweenMeAnd(ref3);

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));


  ///////////////////////////////////////////////////////////////////////////////////

  cout << "!!!!!!!!!!!!!!!!!!!!!!!!! TROISIEME QUADRANT ????????????????????????????" <<endl;

  
  Vector3d direction2(0,-3,4);

  Vector3d centre2(10,4,3);

  Droite ligne2(centre2,direction2);

  DroiteFeature indice2(ligne2);

  indice2.attachToCamera(&oeil);


  double angle = atan(0.75);

  feat = indice2.getFunction();
    
  L = indice2.getInteractionMatrix();


  cout<<"Valeurs des indices visuels \n"<<feat<<"\n"<<endl;

  V << -0.5, angle;

  V = V - feat;

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout<<"Matrice d'interaction des indices visuels\n"<<L<<"\n"<<endl;

  artisan << 
   -0.05,  0.08,  0.06,      0, -3.0/4,     1,
       0,     0,     0,     -1,   -0.4,  -0.3;
  // Le dernier 3.0/4 est a verifier !!

  M = artisan - L;

  BOOST_CHECK(isEqual( M.array().abs().maxCoeff() , 0 , 1e-6));
  

  ref1 << 1, M_PI/2;

  cout <<"Pour une valeur de reference:\n"<<ref1<<"\n"
       <<"L'erreur calculee est:\n"<<indice2.getErrorBetweenMeAnd(ref1)
       <<"\n"<<endl;
  
  V << -1.5, angle - M_PI/2;

  V = V - indice2.getErrorBetweenMeAnd(ref1);

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  ref2 << 1, 0;

  cout <<"Pour une valeur de reference:\n"<<ref2<<"\n"
       <<"L'erreur calculee est:\n"<< indice2.getErrorBetweenMeAnd(ref2)
       <<"\n"<<endl;
  
  V << -1.5, angle;

  V = V - indice2.getErrorBetweenMeAnd(ref2);

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  ref3 << 1, -0.4*M_PI;

  cout <<"Pour une valeur de reference:\n"<<ref3<<"\n"
       <<"L'erreur calculee est:\n"<< indice2.getErrorBetweenMeAnd(ref3)
       <<"\n"<<endl;
  
  V << -0.5, 0.6*M_PI - angle;

  V = V - indice2.getErrorBetweenMeAnd(ref3);

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));

  ////////////////////////////////////////////////////////////////////////////////////

  IntrinsicCam optique(240,240,320,240); 

  oeil.setIntrinsicParameters(optique);


  feat = indice.getFunction();
    
  L = indice.getInteractionMatrix();

  cout<<"Valeurs des indices visuels \n"<<feat<<"\n"<<endl;

  V << -180, M_PI/2;

  V = V - feat;

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout<<"Matrice d'interaction des indices visuels\n"<<L<<"\n"<<endl;

  artisan << 
   -3.0/32*240,    0,  1.0/8*240,      0,-25.0/16*240,      0,
             0,    0,          0,     -1,           0,-3.0/4;
  // Le dernier 3.0/4 est a verifier !!

  M = artisan - L;

  BOOST_CHECK(isEqual( M.array().abs().maxCoeff() , 0 , 1e-6));
  

  ref1 = VectorXd::Zero(2);

  ref1 << 240, M_PI/2;

  cout <<"Pour une valeur de reference:\n"<<ref1<<"\n"
       <<"L'erreur calculee est:\n"<<indice.getErrorBetweenMeAnd(ref1)
       <<"\n"<<endl;
  
  V << -420, 0;

  V = V - indice.getErrorBetweenMeAnd(ref1);

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  ref2 = VectorXd::Zero(2);

  ref2 << 240, 0;

  cout <<"Pour une valeur de reference:\n"<<ref2<<"\n"
       <<"L'erreur calculee est:\n"<< indice.getErrorBetweenMeAnd(ref2)
       <<"\n"<<endl;
  
  V << -420, M_PI/2;

  V = V - indice.getErrorBetweenMeAnd(ref2);

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  ref3 = VectorXd::Zero(2);

  ref3 << 240, -0.4*M_PI;

  cout <<"Pour une valeur de reference:\n"<<ref3<<"\n"
       <<"L'erreur calculee est:\n"<< indice.getErrorBetweenMeAnd(ref3)
       <<"\n"<<endl;
  
  V << -60, 0.1*M_PI;

  V = V - indice.getErrorBetweenMeAnd(ref3);

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));

}


