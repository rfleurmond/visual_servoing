#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK           // A ajouter dans le cas d'une liaison dynamique à Boost Test
#endif

#define BOOST_TEST_MODULE MyTest      // Nom du module de test

#include <boost/test/unit_test.hpp>
#include "../vservoing.h"

using namespace std;
using namespace Eigen;

BOOST_AUTO_TEST_CASE(ICanFly) { 

  //int main(int argc, char * argv[]){

  cout << "Test du modele geometrique du robot " << endl;

  FlyingSystem freeHand;

  int degres = freeHand.getTotalDegres();

  cout << "Somme des degres:" << degres << endl;

  BOOST_CHECK(degres == 6);

  if(degres != 6){

    return;

  }

  VectorXd q = VectorXd::Zero(degres);

  MatrixXd H1 = MatrixXd::Zero(4,4);

  MatrixXd EH = MatrixXd::Zero(4,4);

  MatrixXd J1 = MatrixXd::Zero(6,6);

  MatrixXd J2 = MatrixXd::Zero(6,6);

  MatrixXd EJ = MatrixXd::Zero(6,6);

  cout << "Situation :\n"<< freeHand.getSituation(q).getHomogenMatrix()<< endl;

  H1 <<
    1, 0, 0, 0,
    0, 1, 0, 0,
    0, 0, 1, 0,
    0, 0, 0, 1;

  EH = H1 - freeHand.getSituation(q).getHomogenMatrix();

  BOOST_CHECK(isEqual(EH.array().abs().maxCoeff() , 0 , 1e-6));
  

  cout<<"Jacobien du robot"<<endl;

  J1 = freeHand.getKinematicJacobian(q);

  cout<<J1<<endl;

  J2<<
    0, 0, 0, 1, 0, 0,
    0, 0, 0, 0, 1, 0,
    0, 0, 0, 0, 0, 1,
    1, 0, 0, 0, 0, 0,
    0, 1, 0, 0, 0, 0,
    0, 0, 1, 0, 0, 0;
     
  EJ = J1 -J2;

  BOOST_CHECK(isEqual( EJ.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout << "Pour les coordonness articulaires:\n" <<q <<endl;

  double angle = atan(0.75);

  q << 0, 0, angle, 4, 3, 2;

  cout << "Situation :\n"<< freeHand.getSituation(q).getHomogenMatrix()<< endl;

  H1 <<
    0.8, -0.6, 0, 4,
    0.6,  0.8, 0, 3,
    0  ,    0, 1, 2,
    0  ,    0, 0, 1;

  EH = H1 - freeHand.getSituation(q).getHomogenMatrix();

  BOOST_CHECK(isEqual(EH.array().abs().maxCoeff() , 0 , 1e-6));
  

  cout<<"Jacobien du robot"<<endl;

  J1 = freeHand.getKinematicJacobian(q);

  cout<<J1<<endl;

  J2<<
    0       ,  0       , 0, 1, 0, 0,
    0       ,  0       , 0, 0, 1, 0,
    0       ,  0       , 0, 0, 0, 1,
    0.932399, -0.3108  , 0, 0, 0, 0,
    0.3108  ,  0.932399, 0, 0, 0, 0,
    0       ,         0, 1, 0, 0, 0;
  
  EJ = J1 -J2;

  BOOST_CHECK(isEqual( EJ.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout << "Pour les coordonness articulaires:\n" <<q <<endl;

  

  q << angle + M_PI/2, 0, 0, 4, 3, 2;

  cout << "Situation :\n"<< freeHand.getSituation(q).getHomogenMatrix()<< endl;

  H1 <<
    1,    0,    0, 4,
    0, -0.6, -0.8, 3,
    0,  0.8, -0.6, 2,
    0,    0,    0, 1;

  EH = H1 - freeHand.getSituation(q).getHomogenMatrix();

  BOOST_CHECK(isEqual(EH.array().abs().maxCoeff() , 0 , 1e-6));
  

  cout<<"Jacobien du robot"<<endl;

  J1 = freeHand.getKinematicJacobian(q);

  cout<<J1<<endl;

  J2<<
    0,         0,          0, 1, 0, 0,
    0,         0,          0, 0, 1, 0,
    0,         0,          0, 0, 0, 1,
    1,         0,          0, 0, 0, 0,
    0,  0.361288,  -0.722577, 0, 0, 0,
    0,  0.722577,   0.361288, 0, 0, 0;
  
  EJ = J1 -J2;

  BOOST_CHECK(isEqual( EJ.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout << "Pour les coordonness articulaires:\n" <<q <<endl;

}

