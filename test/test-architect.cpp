#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK           // A ajouter dans le cas d'une liaison dynamique à Boost Test
#endif

#define BOOST_TEST_MODULE MyTest      // Nom du module de test

#include <boost/test/unit_test.hpp>
#include "../geometrie.h"

using namespace Eigen;
using namespace std;

BOOST_AUTO_TEST_CASE(DessinTechnique) { 

  //int main(){

  //cin.ignore( numeric_limits<streamsize>::max(), '\n' );

  Vector3d P = Vector3d::Zero();
  
  Droite d1;
  d1.setCenter(Vector3d(0,0,0));
  d1.setDirection(Vector3d(1,1,0));

  Droite d2;
  d2.setCenter(Vector3d(1,0,0));
  d2.setDirection(Vector3d(1,1,1));

  Droite d3;
  d3.setCenter(Vector3d(2,1,1));
  d3.setDirection(Vector3d(1,1,0));

  list<LPoint> liste = Architect::intersection(d1,d2);

  cout <<"La droite d1\n"<<d1<<" et la droite d2\n"<<d2<<" sont senses ne pas avoir de points en commun"<<endl;

  list<LPoint>::const_iterator it;

  BOOST_CHECK(liste.size()==0);

  if(liste.size()==0){
    cout <<"Comme de raison, nous n'avons pas trouve de points communs a ces deux droites"<<endl;
  }
  else{
    cout<<"Pourquoi alors notre programme trouve-t-il ces points communs?"<<endl;

    for(it = liste.begin();it!=liste.end();it++){
      LPoint lp = *it;
      cout<<lp.getCenter().transpose()<<endl;
    }
  }

  //cin.ignore( numeric_limits<streamsize>::max(), '\n' );

  cout <<"HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHhHHH"<<endl;

  cout <<"La droite d2\n"<<d2<<" et la droite d3\n"<<d3<<" sont senses avoir un point en commun"<<endl;

  liste = Architect::intersection(d2,d3);

  BOOST_CHECK(liste.size()==1);

  if(liste.size()==0){
    cout <<"C'est bizarre, nous n'avons pas trouve de points communs a ces deux droites"<<endl;
  }
  else{
    cout<<"Le(s) point(s) commun trouve par notre programme( il ne devrait avoir qu'un seul)!"<<endl;

    for(it = liste.begin();it!=liste.end();it++){
      LPoint lp = *it;
      P = lp.getCenter();
      cout<<P.transpose()<<endl;
    }
  }

  BOOST_CHECK(isEqual( P(0) , 2 , 1e-6));
  BOOST_CHECK(isEqual( P(1) , 1 , 1e-6));
  BOOST_CHECK(isEqual( P(2) , 1 , 1e-6));


  //cin.ignore( numeric_limits<streamsize>::max(), '\n' );

  cout <<"HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHhHHH"<<endl;

  Plan p1;
  p1.setCenter(Vector3d(0,0,0));
  p1.setDirection(Vector3d(1,1,1));

  Plan p2;
  p2.setCenter(Vector3d(2,0,0));
  p2.setDirection(Vector3d(-1,-1,1));

  Plan p3;
  p3.setCenter(Vector3d(2,1,1));
  p3.setDirection(Vector3d(1,1,1));

  liste = Architect::intersection(d1,p2);

  cout <<"La droite d1\n"<<d1<<" et le plan p2\n"<<p2<<" sont senses  avoir 1 point en commun"<<endl;

  BOOST_CHECK(liste.size()==1);

  if(liste.size()==0){
    cout <<"Pourquoi nous n'avons pas trouve de points communs?"<<endl;
  }
  else{
    cout<<"notre programme trouve ce(s) points commun(s)"<<endl;

    for(it = liste.begin();it!=liste.end();it++){
      LPoint lp = *it;
      P = lp.getCenter();
      cout<<P.transpose()<<endl;
    }
  }

  BOOST_CHECK(isEqual( P(0) , 1 , 1e-6));
  BOOST_CHECK(isEqual( P(1) , 1 , 1e-6));
  BOOST_CHECK(isEqual( P(2) , 0 , 1e-6));


  //cin.ignore( numeric_limits<streamsize>::max(), '\n' );

  cout <<"HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHhHHH"<<endl;

  liste = Architect::intersection(d2,p2);

  cout <<"La droite d2\n"<<d2<<" et le plan p2\n"<<p2<<" sont senses  ne pas avoir de point en commun"<<endl;

  BOOST_CHECK(liste.size()==1);

  if(liste.size()==0){
    cout <<"nous n'avons pas trouve de points communs a ces deux droites"<<endl;
  }
  else{
    cout<<"Pourquoi notre programme trouve ce(s) points commun(s)"<<endl;

    for(it = liste.begin();it!=liste.end();it++){
      LPoint lp = *it;
      P = lp.getCenter();
      cout<<P.transpose()<<endl;
    }
  }

  BOOST_CHECK(isEqual( P(0) , 2 , 1e-6));
  BOOST_CHECK(isEqual( P(1) , 1 , 1e-6));
  BOOST_CHECK(isEqual( P(2) , 1 , 1e-6));


  //cin.ignore( numeric_limits<streamsize>::max(), '\n' );

  cout <<"HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHhHHH"<<endl;

  list<Droite> liste2 = Architect::intersection(p1,p2);
  list<Droite>::const_iterator it2;

  cout <<"Le plan p1\n"<<p1<<" et le plan p2\n"<<p2<<" sont senses avoir 1 droite en commun"<<endl;

  if(liste2.size()==0){
    cout <<"Pourquoi nous n'avons pas trouve de droite commune"<<endl;
  }
  else{
    cout<<"notre programme trouve-t-il c-(e|ttes) droite(s) commune(s)"<<endl;

    for(it2 = liste2.begin();it2!=liste2.end();it2++){
      Droite dd = *it2;
      cout<<dd<<endl;
    }
  }

  //cin.ignore( numeric_limits<streamsize>::max(), '\n' );

  cout <<"HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHhHHH"<<endl;

  liste2 = Architect::intersection(p1,p3);
  
  cout <<"Le plan p1\n"<<p1<<" et le plan p3\n"<<p3<<" ne sont pas senses avoir 1 droite en commun"<<endl;

  if(liste2.size()==0){
    cout <<"nous n'avons pas trouve de droite commune"<<endl;
  }
  else{
    cout<<"Pourquoi notre programme trouve-t-il c-(e|ttes) droite(s) commune(s)"<<endl;

    for(it2 = liste2.begin();it2!=liste2.end();it2++){
      Droite dd = *it2;
      cout<<dd<<endl;
    }
  }

  //cin.ignore( numeric_limits<streamsize>::max(), '\n' );

  cout <<"HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHhHHH"<<endl;

  Cercle c1;

  liste = Architect::intersection(p1,c1);

  cout <<"Le cercle c1\n"<<c1<<" et le plan p1\n"<<p1<<" sont senses  avoir 2 points en commun"<<endl;

  if(liste.size()==0){
    cout <<"Pourquoi nous n'avons pas trouve de points communs?"<<endl;
  }
  else{
    cout<<"notre programme trouve ce(s) points commun(s)"<<endl;

    for(it = liste.begin();it!=liste.end();it++){
      LPoint lp = *it;
      cout<<lp.getCenter().transpose()<<endl;
    }
  }

  //cin.ignore( numeric_limits<streamsize>::max(), '\n' );

  cout <<"HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHhHHH"<<endl;

  liste = Architect::intersection(d1,c1);

  cout <<"Le cercle c1\n"<<c1<<" et la droite d1\n"<<d1<<" sont senses  avoir 2 points en commun"<<endl;

  if(liste.size()==0){
    cout <<"Pourquoi nous n'avons pas trouve de points communs?"<<endl;
  }
  else{
    cout<<"notre programme trouve ce(s) points commun(s)"<<endl;

    for(it = liste.begin();it!=liste.end();it++){
      LPoint lp = *it;
      cout<<lp.getCenter().transpose()<<endl;
    }
  }

  //cin.ignore( numeric_limits<streamsize>::max(), '\n' );

  cout <<"HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHhHHH"<<endl;

  Cercle c2;

  c2.setDirection(Vector3d(0.8,0.6,0));

  liste = Architect::intersection(c1,c2);

  cout <<"Le cercle c1\n"<<c1<<" et le cercle c2\n"<<c2<<" sont senses  avoir 2 points en commun"<<endl;

  if(liste.size()==0){
    cout <<"Pourquoi nous n'avons pas trouve de points communs?"<<endl;
  }
  else{
    cout<<"notre programme trouve ce(s) points commun(s)"<<endl;

    for(it = liste.begin();it!=liste.end();it++){
      LPoint lp = *it;
      cout<<lp.getCenter().transpose()<<endl;
    }
  }

  //cin.ignore( numeric_limits<streamsize>::max(), '\n' );

  cout <<"HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHhHHH"<<endl;

  c2.setDirection(Vector3d(0,0,1));
  c2.setCenter(Vector3d(0.1,0.2,0));


  liste = Architect::intersection(c1,c2);

  cout <<"Le cercle c1\n"<<c1<<" et le cercle c2\n"<<c2<<" sont senses  avoir 2 points en commun"<<endl;

  if(liste.size()==0){
    cout <<"Pourquoi nous n'avons pas trouve de points communs?"<<endl;
  }
  else{
    cout<<"notre programme trouve ce(s) points commun(s)"<<endl;

    for(it = liste.begin();it!=liste.end();it++){
      LPoint lp = *it;
      cout<<lp.getCenter().transpose()<<endl;
    }
  }

  //cin.ignore( numeric_limits<streamsize>::max(), '\n' );

  cout <<"HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHhHHH"<<endl;

  Sphere s1;
  s1.setCenter(Vector3d(1,1,1));
  s1.setRayon(5);

  list<Cercle> liste3 = Architect::intersection(p1,s1);
  list<Cercle>::const_iterator it3;

  cout <<"La sphere s1\n"<<s1<<" et le plan p1\n"<<p1<<" sont senses  avoir 1 cercle en commun"<<endl;

  if(liste3.size()==0){
    cout <<"Pourquoi nous n'avons pas trouve de cercle en  commun?"<<endl;
  }
  else{
    cout<<"notre programme trouve ce(s) cercle(s)"<<endl;

    for(it3 = liste3.begin();it3!=liste3.end();it3++){
      Cercle cc = *it3;
      cout<<cc<<endl;
    }
  }

  //cin.ignore( numeric_limits<streamsize>::max(), '\n' );

  cout <<"HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHhHHH"<<endl;

  Sphere s2;
  s2.setCenter(Vector3d(8,0,5));
  s2.setRayon(5);

  Cercle c3;
  c3.setCenter(Vector3d(0,0,5));
  c3.setRayon(5);
  c3.setDirection(Vector3d(0,1,0));

  liste = Architect::intersection(c3,s2);
  
  cout <<"La sphere s2\n"<<s2<<" et le cercle c3\n"<<c3<<" sont senses  avoir 2 points en commun"<<endl;

  if(liste.size()==0){
    cout <<"Pourquoi nous n'avons pas trouve de points en commun?"<<endl;
  }
  else{
    cout<<"notre programme trouve ce(s) point(s)"<<endl;

    for(it = liste.begin();it!=liste.end();it++){
      LPoint lp = *it;
      cout<<lp.getCenter().transpose()<<endl;
    }
  }

  //cin.ignore( numeric_limits<streamsize>::max(), '\n' );

  cout <<"HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHhHHH"<<endl;


}
