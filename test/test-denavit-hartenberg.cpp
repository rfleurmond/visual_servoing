#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK           // A ajouter dans le cas d'une liaison dynamique à Boost Test
#endif

#define BOOST_TEST_MODULE MyTest      // Nom du module de test

#include <boost/test/unit_test.hpp>
#include "../robots.h"

using namespace std;
using namespace Eigen;

BOOST_AUTO_TEST_CASE(DHT) { 

  //int main(int argc, char * argv[]){

  cout << "Test du modele geometrique du robot " << endl;

  GeoRobot filament;

  loadDHTRobot("../robot_models/staubli.dht",filament);

  int degres = filament.getTotalDegres();

  cout << "Somme des degres:" << degres << endl;

  BOOST_CHECK(degres == 6);

  if(degres != 6){

    return;

  }

  VectorXd q = VectorXd::Zero(degres);

  MatrixXd H1 = MatrixXd::Zero(4,4);

  MatrixXd EH = MatrixXd::Zero(4,4);

  cout << "Situation :\n"<< filament.getSituation(q).getHomogenMatrix()<< endl;

  cout << "Pour les coordonness articulaires:\n" <<q <<endl;

  H1 <<
    1, 0, 0, 0.400,
    0, 1, 0, 0.049,
    0, 0, 1, 0.465,
    0, 0, 0, 1.000;

  EH = H1 - filament.getSituation(q).getHomogenMatrix();

  BOOST_CHECK(isEqual(EH.array().abs().maxCoeff() , 0 , 1e-6));
  

  q<< 0, -M_PI/2, M_PI/2, 0, 0, 0;
  
  cout << "Situation :\n"<< filament.getSituation(q).getHomogenMatrix()<< endl;

  cout << "Pour les coordonness articulaires:\n" <<q <<endl;

  H1 <<
    1, 0, 0, 0.000,
    0, 1, 0, 0.049,
    0, 0, 1, 0.865,
    0, 0, 0, 1.000;

  EH = H1 - filament.getSituation(q).getHomogenMatrix();

  BOOST_CHECK(isEqual( EH.array().abs().maxCoeff() , 0 , 1e-6));

  


  cout<<"Modele cinematique"<<endl;

  MatrixXd J1 = MatrixXd::Zero(6,6);

  MatrixXd J2 = MatrixXd::Zero(6,6);

  MatrixXd EJ = MatrixXd::Zero(6,6);

  q = VectorXd::Zero(degres);

  MyKineModel impulsion(&filament);

  J1 = impulsion.getKinematicJacobian(q);

  cout<<J1<<endl;

  J2<<
    -0.049,  0.465,  0.465,  0    ,  0.065,  0    ,
    0.4   ,  0    ,  0    ,  0    ,  0    ,  0    ,
    0     , -0.4  ,  0    ,  0    ,  0    ,  0    ,
    0     ,  0    ,  0    ,  0    ,  0    ,  0    ,
    0     ,  1    ,  1    ,  0    ,  1    ,  0    ,
    1     ,  0    ,  0    ,  1    ,  0    ,  1    ;
     
  EJ = J1 -J2;

  BOOST_CHECK(isEqual( EJ.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout << "Pour les coordonness articulaires:\n" <<q<<"\n\n"<<endl;
 
  int partie = 3;
    
  cout<<"Modele cinematique pour "<<partie<<" degres"<<endl;


  J1 = impulsion.getKinematicJacobian(q,partie);

  cout<<J1<<endl;

  J2<<
    -0.049,  0.4  ,  0.4  ,  0    ,  0    ,  0    ,
    0.4   ,  0    ,  0    ,  0    ,  0    ,  0    ,
    0     , -0.4  ,  0    ,  0    ,  0    ,  0    ,
    0     ,  0    ,  0    ,  0    ,  0    ,  0    ,
    0     ,  1    ,  1    ,  0    ,  0    ,  0    ,
    1     ,  0    ,  0    ,  0    ,  0    ,  0    ;
     
  EJ = J1 -J2;

  BOOST_CHECK(isEqual( EJ.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout << "Pour les coordonness articulaires:\n" <<q<<"\n\n"<<endl;



  q<< 0, -M_PI/2, M_PI/2, 0, 0, 0;

  cout<<"Modele cinematique"<<endl;

  J1 = impulsion.getKinematicJacobian(q);

  cout<<J1<<endl;

  J2<<
    -0.049,  0.865,  0.465,  0    ,  0.065,  0    ,
    0     ,  0    ,  0    ,  0    ,  0    ,  0    ,
    0     ,  0    ,  0    ,  0    ,  0    ,  0    ,
    0     ,  0    ,  0    ,  0    ,  0    ,  0    ,
    0     ,  1    ,  1    ,  0    ,  1    ,  0    ,
    1     ,  0    ,  0    ,  1    ,  0    ,  1    ;
     
  EJ = J1 -J2;

  BOOST_CHECK(isEqual( EJ.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout << "Pour les coordonness articulaires:\n" <<q<<"\n\n"<<endl;

    
  cout<<"Modele cinematique pour "<<partie<<" degres"<<endl;


  J1 = impulsion.getKinematicJacobian(q,partie);

  cout<<J1<<endl;

  J2<<
    -0.049,  0.8  ,  0.4  ,  0    ,  0    ,  0    ,
    0     ,  0    ,  0    ,  0    ,  0    ,  0    ,
    0     ,  0    ,  0    ,  0    ,  0    ,  0    ,
    0     ,  0    ,  0    ,  0    ,  0    ,  0    ,
    0     ,  1    ,  1    ,  0    ,  0    ,  0    ,
    1     ,  0    ,  0    ,  0    ,  0    ,  0    ;
     
  EJ = J1 -J2;

  BOOST_CHECK(isEqual( EJ.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout << "Pour les coordonness articulaires:\n" <<q<<"\n\n"<<endl;

}

