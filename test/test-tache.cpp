#include "../taches.h"

using namespace Eigen;
using namespace std;

class MaTache: public TaskFunction{

private:

  MatrixXd A;

public:

  MaTache(){

    dimOutput = 3;
    dimInput = 3;

    A = MatrixXd::Identity(3,3);
    A <<  1, 3 ,5, 7 ,11, 13, 17, 19, 23;

  }  

  VectorXd fonction(const VectorXd & q) const{
    assert(q.rows()==dimInput);
    return A*q;
  }


};

int main(){

  MaTache encre;

  ApproximateTask buvard(&encre,1e-6);

  VectorXd q = VectorXd::Zero(3);

  cout <<"Q ="<<q.transpose()<<endl;

  cout <<"FQ) ="<<buvard.fonction(q).transpose()<<endl;

  cout <<"J(Q)=\n"<<buvard.jacobien(q)<<endl;

  MatrixXd J(2,2);

  J <<
    1.0/10 , 1.0/5 ,
    2.0/10 , 2.0/5;
  MatrixXd jplus;

  pinv(J,jplus);

  cout <<"J12=\n"<<J<<endl;

  cout <<"J12+=\n"<<jplus<<endl;

  MatrixXd project = PriorityTask::projecteur(J.row(0));

  cout <<"P1=\n"<<project<<endl;

  VectorXd e = VectorXd::Ones(2);

  VectorXd dotQ;

  dotQ = jplus*e;

  cout <<"dot q12=\n"<<dotQ<<endl;

  pinv(J.row(0),jplus);

  cout <<"J1=\n"<<J.row(0)<<endl;

  cout <<"J1+=\n"<<jplus<<endl;

  dotQ = jplus*e.row(0);

  cout <<"dot q=\n"<<dotQ<<endl;

  MatrixXd J2P = J.row(1)*project;

  cout <<"J2P=\n"<<J2P<<endl;

  MatrixXd J2Plus;

  pinv(J2P,J2Plus);

  cout <<"J2Plus=\n"<<J2Plus<<endl;

  cout <<"J2Plus+=\n"<<project*J2Plus<<endl;

  dotQ = dotQ + J2Plus *(e.row(1) - J.row(1)*jplus*e.row(0));

  cout <<"dot q=\n"<<dotQ<<endl;

  
}
