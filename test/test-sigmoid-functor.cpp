#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK           // A ajouter dans le cas d'une liaison dynamique à Boost Test
#endif

#define BOOST_TEST_MODULE MyTest      // Nom du module de test

#include <boost/test/unit_test.hpp>
#include "../tools.h"

using namespace std;

BOOST_AUTO_TEST_CASE(AvoidFunctor) { 

  //int main(){

  int borne = 100;

  Sigmoid transition(borne);

  double x = 0;

  for(int i = 0; i < 21 ; i++){

    x = 10*i-50;
    
    cout << "F( " << x << " ) = "<< transition(x) << endl;

    if( x <= 0 ){

      BOOST_CHECK( isEqual(transition(x),0,1e-6) );
    }

    else if( x < borne){

      BOOST_CHECK( transition(x) > 0);
      BOOST_CHECK( transition(x) < 1);
    
    }

    else{

      BOOST_CHECK( isEqual(transition(x),1,1e-6));
    
    }

  }
  
  
}
