/*
 * Camera_test.cpp
 *
 *  Created on: Jun 7, 2012
 *      @author Renliw Fleurmond
 */


#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK           // A ajouter dans le cas d'une liaison dynamique à Boost Test
#endif

#define BOOST_TEST_MODULE MyTest      // Nom du module de test

#include <boost/test/unit_test.hpp>
#include "../vision.h"

using namespace std;
using namespace Eigen;


BOOST_AUTO_TEST_CASE(VisionCamera) { 

  //int main(){

  Vector3d p1(10,8,0);

  Vector3d p2(10,0,8);

  Vector3d p3(10,12,8);

  Vector2d IP,II,EI;

  Camera oeil;


  BOOST_CHECK(oeil.canSee(p1));

  BOOST_CHECK(oeil.canSee(p2));

  BOOST_CHECK(oeil.canSee(p3) == false);

  
  IP = oeil.getCleanImage(p1);

  II << -0.8 , 0 ;

  EI  = IP - II ;

  cout << EI.transpose() << endl;

  BOOST_CHECK(isEqual( EI(0) , 0 , 1e-6));
  
  BOOST_CHECK(isEqual( EI(1) , 0 , 1e-6));
  
  
  IP = oeil.getCleanImage(p2);

  II << 0 , -0.8 ;

  EI  = IP - II ;

  cout << EI.transpose() << endl;

  BOOST_CHECK(isEqual( EI(0) , 0 , 1e-6));
  
  BOOST_CHECK(isEqual( EI(1) , 0 , 1e-6));
  


  IP = oeil.getCleanImage(p3);

  II << -1.2 ,-0.8 ;

  EI  = IP - II ;

  cout << EI.transpose() << endl;

  BOOST_CHECK(isEqual( EI(0) , 0 , 1e-6));
  
  BOOST_CHECK(isEqual( EI(1) , 0 , 1e-6));
  
  
  IntrinsicCam optique(240,240,324,240); 

  oeil.setIntrinsicParameters(optique);


  BOOST_CHECK(oeil.canSee(p1));

  BOOST_CHECK(oeil.canSee(p2));

  BOOST_CHECK(oeil.canSee(p3));

  
  IP = oeil.getCleanImage(p1);

  II << 324 - 192 , 240 - 0 ;

  EI  = IP - II ;

  cout << EI.transpose() << endl;

  BOOST_CHECK(isEqual( EI(0) , 0 , 1e-6));
  
  BOOST_CHECK(isEqual( EI(1) , 0 , 1e-6));
  
  
  IP = oeil.getCleanImage(p2);

  II << 324 - 0 , 240 - 192 ;

  EI  = IP - II ;

  cout << EI.transpose() << endl;

  BOOST_CHECK(isEqual( EI(0) , 0 , 1e-6));
  
  BOOST_CHECK(isEqual( EI(1) , 0 , 1e-6));
  


  IP = oeil.getCleanImage(p3);

  II << 324 - 288 , 240 - 192 ;

  EI  = IP - II ;

  cout << EI.transpose() << endl;

  BOOST_CHECK(isEqual( EI(0) , 0 , 1e-6));
  
  BOOST_CHECK(isEqual( EI(1) , 0 , 1e-6));
  
  
}

