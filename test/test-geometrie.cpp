#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK           // A ajouter dans le cas d'une liaison dynamique à Boost Test
#endif

#define BOOST_TEST_MODULE MyTest      // Nom du module de test

#include <boost/test/unit_test.hpp>
#include <iostream>
#include "../geometrie.h"

using namespace Eigen;
using namespace std;

BOOST_AUTO_TEST_CASE(Geometrie) { 

  //int main(){

  Vector3d P(7,6.5,2);

  Vector3d S;

  Droite d1;
  d1.setCenter(Vector3d(1,1,4));
  d1.setDirection(Vector3d(-1,1,0));
  cout << "Le point appartenant a la droite d1\n";
  cout << d1;
  cout <<"le plus proche du point:\n"<<P<<endl;
  S = d1.getNearestPoint(P);
  cout <<"est le point:\n"<<S<<endl;
  cout <<"la distance est egale a "<<d1.getLowestDistance(P)<<endl;

  BOOST_CHECK(isEqual( S(0) , 1.25 , 1e-6));
  BOOST_CHECK(isEqual( S(1) , 0.75 , 1e-6));
  BOOST_CHECK(isEqual( S(2) , 4 , 1e-6));
  
  Plan p1;
  p1.setCenter(Vector3d(1,1,3));
  p1.setDirection(Vector3d(1,1,0));
  cout << "\nLe point appartenant au plan\n";
  cout << p1;
  cout <<"le plus proche du point:\n"<<P<<endl;
  S = p1.getNearestPoint(P);
  cout <<"est le point:\n"<<S<<endl;
  cout <<"la distance est egale a "<<p1.getLowestDistance(P)<<endl;


  BOOST_CHECK(isEqual( S(0) , 1.25 , 1e-6));
  BOOST_CHECK(isEqual( S(1) , 0.75 , 1e-6));
  BOOST_CHECK(isEqual( S(2) , 2 , 1e-6));
  
  Cercle c1;
  c1.setCenter(Vector3d(5,5,5));
  c1.setDirection(Vector3d(0,0,1));
  c1.setRayon(5);
  cout << "\nLe point appartenant au cercle\n";
  cout << c1;
  cout <<"le plus proche du point:\n"<<P<<endl;
  S = c1.getNearestPoint(P);
  cout <<"est le point:\n"<<S<<endl;
  cout <<"la distance est egale a "<<c1.getLowestDistance(P)<<endl;

  BOOST_CHECK(isEqual( S(0) , 9 , 1e-6));
  BOOST_CHECK(isEqual( S(1) , 8 , 1e-6));
  BOOST_CHECK(isEqual( S(2) , 5 , 1e-6));
  

  Disque dd(c1);
  cout << "\nLe point appartenant au disque\n";
  cout << dd;
  cout <<"le plus proche du point:\n"<<P<<endl;
  S = dd.getNearestPoint(P);
  cout <<"est le point:\n"<<S<<endl;
  cout <<"la distance est egale a "<<dd.getLowestDistance(P)<<endl;

  BOOST_CHECK(isEqual( S(0) , 7 , 1e-6));
  BOOST_CHECK(isEqual( S(1) , 6.5 , 1e-6));
  BOOST_CHECK(isEqual( S(2) , 5 , 1e-6));


  Cylindre cc(c1,Vector3d(0,0,1));
  cout << "\nLe point appartenant au cylindre\n";
  cout << cc;
  cout <<"le plus proche du point:\n"<<P<<endl;
  S = cc.getNearestPoint(P);
  cout <<"est le point:\n"<<S<<endl;
  cout <<"la distance est egale a "<<cc.getLowestDistance(P)<<endl;

  BOOST_CHECK(isEqual( S(0) , 7 , 1e-6)); // 9 A vérifier à la main
  BOOST_CHECK(isEqual( S(1) , 6.5 , 1e-6)); // 8 A vérifier à la main
  BOOST_CHECK(isEqual( S(2) , 2 , 1e-6));

  P << 2,0,1.5;
  PlanTournant pt;
  Vector3d V;
  V <<4,0,3;
  pt.setDirection(V);
  V<<0,0,1;
  pt.setRotation(V);
  double dddd = 5;
  pt.setDistance(dddd);
  V<<0,0,0;
  pt.setCenter(V);
  cout << "\nLe point appartenant au plan tournant\n";
  cout << pt;
  cout <<"le plus proche du point:\n"<<P<<endl;
  S = pt.getNearestPoint(P);
  cout <<"est le point:\n"<<S<<endl;
  cout <<"la distance est egale a "<<pt.getLowestDistance(P)<<endl;
  
  BOOST_CHECK(isEqual( S(0) , 4 , 1e-6));
  BOOST_CHECK(isEqual( S(1) , 0 , 1e-6));
  BOOST_CHECK(isEqual( S(2) , 3 , 1e-6));


  P <<4,4,3;
  int n = -1;
  list<Plan> coupes = pt.getSuitablePlans(P,&n);

  BOOST_CHECK(n == 2);
  
  if(n==0){
    cout<<"Il n'y a pas de plans(tournants) passant par le point:\n"<<P<<endl;
  }
  else{
    if(n<1000){
      cout<<"Il y a "<<n<<" plan(s) convenable(s) pour le point:\n"<<P<<endl;
    }
    else{
      cout<<"Il y a une infinite de plans convenables pour le point:\n"<<P<<endl;
    }
    list<Plan>::const_iterator it;
    int i =1;
    for(it=coupes.begin();it!=coupes.end();it++){
      cout <<"Plan #"<<i<<endl;
      i++;
      Plan table = (Plan) (*it);
      cout <<table;
    }
    

  }

  Cone con;
  V << 0,0,0;
  con.setCenter(V);
  V<<0,0,1;
  con.setRotation(V);
  con.setAngle(M_PI/4);

  P <<0.5,0,1.5;
  cout << "\nLe point appartenant au cone\n";
  cout << con;
  cout <<"le plus proche du point:\n"<<P<<endl;
  S = con.getNearestPoint(P);
  cout <<"est le point:\n"<<S<<endl;
  cout <<"la distance est egale a "<<con.getLowestDistance(P)<<endl;

  BOOST_CHECK(isEqual( S(0) , 1 , 1e-6));
  BOOST_CHECK(isEqual( S(1) , 0 , 1e-6));
  BOOST_CHECK(isEqual( S(2) , 1 , 1e-6));


  Tore tt;
  P <<0,1,1;
  cout << "\nLe point appartenant au Tore\n";
  cout << tt;
  cout <<"le plus proche du point:\n"<<P<<endl;
  S = tt.getNearestPoint(P); 
  cout <<"est le point:\n"<<S<<endl;
  cout <<"la distance est egale a "<<tt.getLowestDistance(P)<<endl;

  BOOST_CHECK(isEqual( S(0) , 0 , 1e-6));
  BOOST_CHECK(isEqual( S(1) , 1 , 1e-6));
  BOOST_CHECK(isEqual( S(2) , 0.1 , 1e-6));


  DroiteTournante dt;
  P <<0.9,0,4;
  cout << "\nLe point appartenant a la droite tournante\n";
  cout << dt;
  cout <<"le plus proche du point:\n"<<P<<endl;
  S = dt.getNearestPoint(P);
  cout <<"est le point:\n"<<S<<endl;
  cout <<"la distance est egale a "<<dt.getLowestDistance(P)<<endl;

  // Par la foi, rires !
  BOOST_CHECK(isEqual( S(0) , 5.42627 , 1e-5));
  BOOST_CHECK(isEqual( S(1) , 1.92 , 1e-6));
  BOOST_CHECK(isEqual( S(2) , 1.44 , 1e-6));

}
