#include "../robots.h"

using namespace std;
using namespace Eigen;

int main(int argc, char * argv[]){

  if(argc>1){

    string fichier(argv[1]);

    cout << "Test du modele géometrique inverse du robot " << endl;

    GeoRobot filament;

    loadDHTRobot(fichier,filament);

    int degres = filament.getTotalDegres();

    cout << "Le robot a " << degres <<" degré(s) de liberte"<< endl;

    VectorXd q = M_PI/2 * VectorXd::Ones(degres);

    cout << "Pour les coordonnées articulaires:\n" <<q <<endl;

    Repere situation = filament.getSituation(q);

    cout << "La situation de l'organe terminal est :\n"<< situation<< endl;

    MyInverseGeoModel retro(&filament);

    list<VectorXd> solutions = retro.getCoordonnees(situation);

    if(solutions.size()>0){
    
      cout << "Pour cette situation "<< endl;
    
      cout << "Les solutions trouvées par ma retro ingenierie sont:"<<endl;

      list<VectorXd>::const_iterator it;

      int i = 0;
      for(it = solutions.begin();it!=solutions.end();it++){
	q = (*it);
	i++;
	cout <<"Solution #"<<i<<" = "<<q.transpose()<<endl;
      }

    }

    if(retro.isComplete()){
      
      cout<<"Mon algorithme peut trouver toutes les solutions de ce modèle si elles existent" <<endl;

    }
    else{
      cout<<"Mon algorithme ne garantit pas de trouver de solutions pour ce modèle " <<endl;

    }

    if(retro.hasFoundSolutionsForPosition()){
      
      cout<<"Mon algorithme a trouvé des solutions pour la position" <<endl;

    }
    else{
      cout<<"Mon algorithme n'a pas trouvé des solutions pour la position" <<endl;

    }


    if(retro.hasFoundCompleteSolutions()){
      
      cout<<"Mon algorithme a trouvé des solutions pour la situation" <<endl;

    }
    else{
      cout<<"Mon algorithme n'a pas trouvé des solutions pour la situation" <<endl;

    }

    if(retro.hasInfinityOfSolutions()){
      
      cout<<"Mon algorithme indique qu'il existe une infinité de solutions" <<endl;

    }

    cout <<"Fin du test"<<endl;

  }
  else{
    cout<<"Aucun fichier passe en argument!"<<endl;
  }

}

