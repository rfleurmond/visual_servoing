#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK           // A ajouter dans le cas d'une liaison dynamique à Boost Test
#endif

#define BOOST_TEST_MODULE MyTest      // Nom du module de test

#include <boost/test/unit_test.hpp>
#include "../vservoing.h"

using namespace Eigen;
using namespace std;

BOOST_AUTO_TEST_CASE(VisualInfiniteCylinder) { 

  //int main(void){

  Camera oeil;

  double r = 0.3;

  double l = 16.0/15 *r;

  double d = 0.5*r/0.3;

  Cylinder indice(r);

  Repere frame(VectRotation(),Vector3d(d,0,-0.5*l));

  indice.setSituation(frame);

  indice.attachToCamera(&oeil);

  cout<<"Le cylindre peut-il etre vu par la camera? Reponse: "<<indice.itCanBeSeen()<<endl;

  VectorXd F = indice.getFunction();

  MatrixXd L = indice.getInteractionMatrix();

  VectorXd V = VectorXd::Zero(4);

  cout<<"Valeur des indices visuels \n"<<F<<"\n\n"<<endl;

  V << -0.75, -M_PI, 0.75, -M_PI;

  V = V - F;

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  cout<<"Matrice d'interaction des indices visuels\n"<<L<<"\n"<<endl;

  MatrixXd M = MatrixXd::Zero(4,6);

  M << 
   -2.34375, -3.125,      0,   0,       0, -1.5625,
          0,      0,      0,  -1,    0.75,       0,
    2.34375, -3.125,      0,   0,       0, -1.5625,
          0,      0,      0,  -1,   -0.75,       0;
  // Deux dernieres colonnes a verifier !!

  V = M.col(1) - 2 * M.col(5); // Rotation around the axis

  cout <<" La deuxieme colonne et la dernière colonne sont liées: COL(2) = 2 * COL(6)" << endl;

  BOOST_CHECK(isEqual( V.array().abs().maxCoeff() , 0 , 1e-6));
  
  M = M - L;

  BOOST_CHECK(isEqual( M.array().abs().maxCoeff() , 0 , 1e-6));
 
  
  
}
