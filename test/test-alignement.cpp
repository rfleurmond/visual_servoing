#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK           // A ajouter dans le cas d'une liaison dynamique à Boost Test
#endif

#define BOOST_TEST_MODULE MyTest      // Nom du module de test

#include <boost/test/unit_test.hpp>

#include "../vservoing/alignement.h"
#include <iostream>

using namespace std;
using namespace Eigen;

BOOST_AUTO_TEST_CASE(MakeALine) { 
  
  //int main(){

  cout <<"Test de la classe Alignement:" <<endl;
  
  Vector2d A,B,C,D;

  A << 0, 3;

  B << 0, 7;

  C << 2, 4;

  D << 2, 8;

  VectorXd P = VectorXd::Zero(2);

  cout << "Alignement du point C["<<C.transpose()
       <<"] sur les points A["<<A.transpose()
       <<"] et B["<<B.transpose()<<"]"<<endl;

  cout <<"Le resultat devrait donner "<<endl;
  
  P = Alignement::getReference(A, B, C);
  cout <<"C*["<<P.transpose()<<"]\n"<<endl;

  BOOST_CHECK(isEqual( P(0) , 0 , 1e-6));
  BOOST_CHECK(isEqual( P(1) , 4 , 1e-6));
  

  
  cout << "Alignement des point C["<<C.transpose()
       <<"] et D["<<D.transpose()<<"] sur les points A["
       <<A.transpose()<<"] et B["<<B.transpose()<<"]"<<endl;

  cout <<"Le resultat devrait donner CD*[0 4 0 8]"<<endl;

  P = Alignement::getReference(A, B, C, D);

  cout <<"Resultat: CD*["<<P.transpose()<<"]\n"<<endl;

  BOOST_CHECK(isEqual( P(0) , 0 , 1e-6));
  BOOST_CHECK(isEqual( P(1) , 4 , 1e-6));
  BOOST_CHECK(isEqual( P(2) , 0 , 1e-6));
  BOOST_CHECK(isEqual( P(3) , 8 , 1e-6));
  


  
  C << 2, 4;

  D << 6, 4;

  cout << "Alignement des point C["<<C.transpose()
       <<"] et D["<<D.transpose()<<"] sur les points A["
       <<A.transpose()<<"] et B["<<B.transpose()<<"]"<<endl;

  cout <<"Le resultat devrait donner CD*[0 6 0 10]"<<endl;

  P = Alignement::getReference(A, B, C, D);
  cout <<"Resultat: CD*["<<P.transpose()<<"]\n"<<endl;

  BOOST_CHECK(isEqual( P(0) , 0 , 1e-6));
  BOOST_CHECK(isEqual( P(1) , 6 , 1e-6));
  BOOST_CHECK(isEqual( P(2) , 0 , 1e-6));
  BOOST_CHECK(isEqual( P(3) , 10 , 1e-6));
  

  C << 4, 3;

  B << 0,17;

  D << 12, 9;

  cout << "Alignement des point C["<<C.transpose()
       <<"] et D["<<D.transpose()<<"] sur les points A["
       <<A.transpose()<<"] et B["<<B.transpose()<<"]"<<endl;

  cout <<"Le resultat devrait donner CD*[0 5 0 15]"<<endl;

  P = Alignement::getReference(A, B, C, D);

  cout <<"Resultat: CD*["<<P.transpose()<<"]\n"<<endl;

  BOOST_CHECK(isEqual( P(0) , 0 , 1e-6));
  BOOST_CHECK(isEqual( P(1) , 5 , 1e-6));
  BOOST_CHECK(isEqual( P(2) , 0 , 1e-6));
  BOOST_CHECK(isEqual( P(3) , 15 , 1e-6));
  
  
}
