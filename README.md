# README #


### What is this repository for? ###

* This repository is used to store the produced code during my PhD thesis.

       My work is on coordination of arms on dual arm robot with visual servoing. 

       This code has started from a previous work made on my internship when I had to

       estimate the pose of a humanoid robot using the vision and the inertial unit.


* In this code are implemented some **unpublished works**, 
 
     and instead stated in some specific files

     this code is my intellectual property, and owns by default to my laboratory: [LAAS](https://www.laas.fr/public/fr)

*  I don't know yet what software license will be applied to this code. 

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

  This code has ben produced on a Linux machine, running Ubuntu 12.04,
  but normally it should works in any Unix environnment and maybe in Windows systems.

* Configuration

  **CMake** and **Gcc** are needed to be able to compile this code.
  After cloning this repository please create a build directory with these commands

```
#!sh

mkdir build & cd build
cmake ..
make
```


* Dependencies

  This code has some dependencies ** Eigen **, ** OpenCv ** and ** Boost **

* How to run tests

  Before got the build directory , then  enter
 
```
#!sh

make test
```

or 

```
#!sh

ctest
```

  The verbose option will display all printouts of this executable tests

```
#!sh

ctest --verbose
```


* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* [Renliw Fleurmond](mailto: renliw_fds@yahoo.fr)