set terminal postscript eps enhanced size 10,6 color font 'Helvetica,30'

set style arrow 1 head size screen 0.025,15 lw 5 lt 2 lc rgb "red"
set style arrow 2 head size screen 0.025,15 lw 5 lt 2 lc rgb "blue"
set style arrow 3 nohead lw 3 lc rgb "black"

set output "bounds-task.eps"
col = 1
set title "Tache pour l'evitement des butees"
set grid
set yrange [-0.1: 0.5]
set xlabel "Temps (secondes)"
set ylabel "Valeur des composantes de la fonction de tache" 

plot "./data_task.dat" using 1:col+1 title "F_{b1}" with lines lw 4 lt 1 lc rgb "blue", \
     "./data_task.dat" using 1:col+2 title "F_{b2}" with lines lw 5, \
     "./data_task.dat" using 1:col+3 title "F_{b3}" with lines lw 5, \
     "./data_task.dat" using 1:col+4 title "F_{b4}" with lines lw 5, \
     "./data_task.dat" using 1:col+5 title "F_{b6}" with lines lw 4 lt 1 lc rgb "red", \
     "./data_task.dat" using 1:col+6 title "F_{b8}" with lines lw 5, \
     "./data_task.dat" using 1:col+7 title "F_{b9}" with lines lw 5, \
     "./data_task.dat" using 1:col+8 title "F_{b10}" with lines lw 5, \
     "./data_task.dat" using 1:col+9 title "F_{b11}" with lines lw 5, \
     "./data_task.dat" using 1:col+10 title "F_{b13}" with points lw 2 lc rgb "blue"

pause -1
col = col + 10


set output "real-task.eps"
t1 = 8.4
t2 = 17.1
bot = -200
top = 200
set yrange [bot: top]
set title "Taches referencee vision pour la fermeture d'un stylo"
set grid
set xlabel "Temps (secondes)"
set ylabel "Composante de la tache (pixels et/ou 1/2 de degres)" 
set arrow from t1,bot to t1,top as 3
set arrow from t2,bot to t2,top as 3
plot "./data_task.dat" using 1:col+1 title "v_1" with lines lw 5, \
 "./data_task.dat" using 1:col+2 title "v_2" with lines lw 5, \
 "./data_task.dat" using 1:col+3 title "v_3" with lines lw 5, \
 "./data_task.dat" using 1:col+4 title "v_4" with lines lw 5, \
 "./data_task.dat" using 1:col+5 title "v_5" with lines lw 5, \
 "./data_task.dat" using 1:col+6 title "v_6" with lines lw 5
pause -1
col = col + 6

set output "visibility-task.eps"
set title "Tache pour l'evitement des occultations"
set grid
set yrange [-5: 55]
set xlabel "Temps (secondes)"
set ylabel "Valeur des composantes de la tache (pixels)" 
set noarrow 1
set noarrow 2
plot "./data_task.dat" using 1:col+1 title "F_{v1}" with lines lw 5, \
 "./data_task.dat" using 1:col+2 title "F_{v2}" with lines lw 5, \
 "./data_task.dat" using 1:col+3 title "F_{v3}" with lines lw 5, \
 "./data_task.dat" using 1:col+4 title "F_{v4}" with lines lw 5, \
 "./data_task.dat" using 1:col+5 title "F_{v5}" with lines lw 5, \
 "./data_task.dat" using 1:col+6 title "F_{v6}" with lines lw 5, \
 "./data_task.dat" using 1:col+7 title "F_{v7}" with lines lw 5, \
 "./data_task.dat" using 1:col+8 title "F_{v8}" with lines lw 5
pause -1


set output "left-control.eps"
col = 1
set title " Commande en vitesse sur le bras gauche "
set grid
set mxtics
top = 0.06
bot = -0.1
set yrange [bot:top]
set xlabel "Temps (secondes)"
set ylabel "Vitesse articulaire (rad/s)" 
#set noarrow 1
#set noarrow 2
set arrow from t1,bot to t1,top as 3
set arrow from t2,bot to t2,top as 3
plot "./data_sys.dat" using 1:col+1 title "q_1" with lines lw 5, \
 "./data_sys.dat" using 1:col+2 title "q_2" with lines lw 5, \
 "./data_sys.dat" using 1:col+3 title "q_3" with lines lw 5, \
 "./data_sys.dat" using 1:col+4 title "q_4" with lines lw 5, \
 "./data_sys.dat" using 1:col+5 title "q_5" with lines lw 5, \
 "./data_sys.dat" using 1:col+6 title "q_6" with lines lw 5, \
 "./data_sys.dat" using 1:col+7 title "q_7" with lines lw 5

pause -1
col = col+7       

set output "right-control.eps"
set title "Commande en vitesse sur le bras droit "
set grid
set mxtics
bot = -0.1
top = 0.1
set yrange [-0.1:0.1]
set xlabel "Temps (secondes)"
set ylabel "Vitese articulaire (rad/s)" 
set noarrow 1
set noarrow 2
set arrow from t1,bot to t1, top as 3
set arrow from t2,bot to t2, top as 3
plot "./data_sys.dat" using 1:col+1 title "q_1" with lines lw 5, \
 "./data_sys.dat" using 1:col+2 title "q_2" with lines lw 5, \
 "./data_sys.dat" using 1:col+3 title "q_3" with lines lw 5, \
 "./data_sys.dat" using 1:col+4 title "q_4" with lines lw 5, \
 "./data_sys.dat" using 1:col+5 title "q_5" with lines lw 5, \
 "./data_sys.dat" using 1:col+6 title "q_6" with lines lw 5, \
 "./data_sys.dat" using 1:col+7 title "q_7" with lines lw 5
pause -1
col = col+7       

#set terminal x11
gauche = col
set title "Left joint coordinates values"
set grid
set noarrow 1
set noarrow 2
set xlabel "Temps (secondes)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:col+1 title "q_1" with lines lw 5
replot "./data_sys.dat" using 1:col+2 title "q_2" with lines lw 5
replot "./data_sys.dat" using 1:col+3 title "q_3" with lines lw 5
replot "./data_sys.dat" using 1:col+4 title "q_4" with lines lw 5
replot "./data_sys.dat" using 1:col+5 title "q_5" with lines lw 5
replot "./data_sys.dat" using 1:col+6 title "q_6" with lines lw 5
replot "./data_sys.dat" using 1:col+7 title "q_7" with lines lw 5
pause -1
col = col+7       

droite = col
set title "Right joint coordinate values"
set grid
set xlabel "Temps (secondes)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:col+1 title "q_1" with lines lw 5
replot "./data_sys.dat" using 1:col+2 title "q_2" with lines lw 5
replot "./data_sys.dat" using 1:col+3 title "q_3" with lines lw 5
replot "./data_sys.dat" using 1:col+4 title "q_4" with lines lw 5
replot "./data_sys.dat" using 1:col+5 title "q_5" with lines lw 5
replot "./data_sys.dat" using 1:col+6 title "q_6" with lines lw 5
replot "./data_sys.dat" using 1:col+7 title "q_7" with lines lw 5
pause -1
col = col+7       

set title "Left joint coordinates values respect to bounds"
set grid
set xlabel "Temps (secondes)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:gauche+1 title "q_1" with lines lw 5
replot -0.69813170079 title "Min" with lines lw 5
replot 2.26892802759 title "Max" with lines lw 5
pause -1

set title "Left joint coordinates values respect to bounds"
set grid
set xlabel "Temps (secondes)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:gauche+2 title "q_2" with lines lw 5
replot -0.52359877559 title "Min" with lines lw 5
replot 1.3962634016 title "Max" with lines lw 5
pause -1


set title "Left joint coordinates values respect to bounds"
set grid
set xlabel "Temps (secondes)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:gauche+3 title "q_3" with lines lw 5
replot -0.76794487087  title "Min" with lines lw 5
replot 3.90953752447  title "Max" with lines lw 5
pause -1

set title "Left joint coordinates values respect to bounds"
set grid
set xlabel "Temps (secondes)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:gauche+4 title "q_4" with lines lw 5
replot 0 title "Max" with lines lw 5
replot -2.26892802759  title "Min" with lines lw 5
pause -1

set title "Left joint coordinates values respect to bounds"
set grid
set xlabel "Temps (secondes)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:gauche+6 title "q_6" with lines lw 5
replot 0 title "Max" with lines lw 5
replot -2.26892802759  title "Min" with lines lw 5
pause -1

set title "Right joint coordinates values respect to bounds"
set grid
set xlabel "Temps (secondes)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:droite+1 title "q_1" with lines lw 5
replot  0.69813170079 title "Max" with lines lw 5
replot -2.26892802759 title "Min" with lines lw 5
pause -1

set title "Right joint coordinates values respect to bounds"
set grid
set xlabel "Temps (secondes)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:droite+2 title "q_2" with lines lw 5
replot -0.52359877559 title "Min" with lines lw 5
replot  1.3962634016 title "Max" with lines lw 5
pause -1


set title "Right joint coordinates values respect to bounds"
set grid
set xlabel "Temps (secondes)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:droite+3 title "q_3" with lines lw 5
replot  0.76794487087  title "Max" with lines lw 5
replot -3.90953752447  title "Min" with lines lw 5
pause -1

set title "Right joint coordinates values respect to bounds"
set grid
set xlabel "Temps (secondes)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:droite+4 title "q_4" with lines lw 5
replot 0 title "Max" with lines lw 5
replot -2.26892802759  title "Min" with lines lw 5
pause -1

set terminal postscript eps enhanced size 10,4 color font 'Helvetica,30'
set output "right-q6-joint.eps"
set title "Coordonnee articulaire de la sixieme articulation du bras droit et ses limites"
set xrange [0:25]
set yrange [-0.1:0.025]
set grid
set xlabel "Temps (secondes)"
set ylabel "Coordonnee articulaire (radians)" 
plot "./data_sys.dat" using 1:droite+6 title "q_6" with lines lw 5,\
     0 title "Max" with lines lw 11 lc rgb "blue"
#, \ -2.26892802759  title "Min" with lines lw 5

pause -1

quit