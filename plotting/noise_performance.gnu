set terminal postscript eps enhanced size 10,6 color solid font 'Helvetica,30'


set output "two-views-3D.eps"
set title "Erreur de reconstruction en fonction du bruit"
set grid
set xlabel "Ecart type du bruit injecte dans l'image"
set ylabel "Erreur 3D " 
set style data linespoints
plot "./v2-noise-performance.dat" using 1:2 title "Mediane de l'erreur avec 2 vues" with linespoints lw 5
pause -1


set output "two-views-2D.eps"
set title "Erreur de reprojection en fonction du bruit"
set grid
set xlabel "Ecart type du bruit injecte dans l'image"
set ylabel "Erreur 2D (pixels)" 
plot sqrt(2)*x  title "Modele de l'erreur" with points lw 5, \
     "./v2-noise-performance2D.dat" using 1:2 title "Mediane de l'erreur avec 2 vues" with steps lw 5 lc "blue"
      

pause -1

col = 1
set output "n-views-3D.eps"
set title "Erreur de reconstruction en fonction du nombre de vues"
set grid
set xrange [2:50]
#set yrange [0:0.04]
set xlabel "Nombre de vues"
set ylabel "Erreur 3D " 
plot "./noise-performance.dat" using 1:col+1 title "Ecart type 0.1 pixel(s)" with lines lw 5, \
      "./noise-performance.dat" using 1:col+2 title "Ecart type 0.5 pixel(s)" with lines lw 5, \
      "./noise-performance.dat" using 1:col+3 title "Ecart type 1 pixel(s)" with lines lw 5, \
      "./noise-performance.dat" using 1:col+4 title "Ecart type 2 pixel(s)" with lines lw 5, \
      "./noise-performance.dat" using 1:col+5 title "Ecart type 3 pixel(s)" with lines lw 5, \
      "./noise-performance.dat" using 1:col+6 title "Ecart type 4 pixel(s)" with lines lw 5, \
      "./noise-performance.dat" using 1:col+7 title "Ecart type 5 pixel(s)" with lines lw 5, \
      "./noise-performance.dat" using 1:col+8 title "Ecart type 6 pixel(s)" with lines lw 5, \
      "./noise-performance.dat" using 1:col+9 title "Ecart type 7 pixel(s)" with lines lw 5, \
      "./noise-performance.dat" using 1:col+10 title "Ecart type 8 pixel(s)" with lines lw 5, \
      "./noise-performance.dat" using 1:col+11 title "Ecart type 9 pixel(s)" with lines lw 5, \
      "./noise-performance.dat" using 1:col+12 title "Ecart type 10 pixel(s)" with lines lw 5

pause -1

col = 1
set output "n-views-3D-zoom.eps"
set title "Erreur de reconstruction en fonction du nombre de vues"
set grid
set xrange [2:50]
#set yrange [0:0.02]
set xlabel "Nombre de vues"
set ylabel "Erreur 3D " 
plot "./noise-performance.dat" using 1:col+1 title "Ecart type 0.1 pixel(s)" with lines lw 5, \
      "./noise-performance.dat" using 1:col+2 title "Ecart type 0.5 pixel(s)" with lines lw 5, \
      "./noise-performance.dat" using 1:col+3 title "Ecart type 1 pixel(s)" with lines lw 5, \
      "./noise-performance.dat" using 1:col+4 title "Ecart type 2 pixel(s)" with lines lw 5, \
      "./noise-performance.dat" using 1:col+5 title "Ecart type 3 pixel(s)" with lines lw 5, \
      "./noise-performance.dat" using 1:col+6 title "Ecart type 4 pixel(s)" with lines lw 5, \
      "./noise-performance.dat" using 1:col+7 title "Ecart type 5 pixel(s)" with lines lw 5

pause -1



set output "n-views-2D.eps"
set title "Erreur de reprojection en fonction du nombre de vues"
set grid
set xrange [2:50]
#set yrange [0:15]
set xlabel "Nombre de vues"
set ylabel "Erreur 2D  (pixels)" 

plot "./noise-performance2D.dat" using 1:col+1 title "Ecart type 0.1 pixel(s)" with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+2 title "Ecart type 0.5 pixel(s)" with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+3 title "Ecart type 1 pixel(s)" with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+4 title "Ecart type 2 pixel(s)" with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+5 title "Ecart type 3 pixel(s)" with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+6 title "Ecart type 4 pixel(s)" with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+7 title "Ecart type 5 pixel(s)" with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+8 title "Ecart type 6 pixel(s)" with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+9 title "Ecart type 7 pixel(s)" with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+10 title "Ecart type 8 pixel(s)" with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+11 title "Ecart type 9 pixel(s)" with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+12 title "Ecart type 10 pixel(s)" with lines lw 5, \
      10*sqrt(2)/sqrt(x-1)  title "Modele" with points

pause -1

set output "n-views-2D-zoom.eps"
set title "Erreur de reprojection en fonction du nombre de vues"
set grid
set xrange [2:50]
set yrange [0:7]
set xlabel "Nombre de vues"
set ylabel "Erreur 2D  (pixels)" 
plot "./noise-performance2D.dat" using 1:col+1 title "Ecart type 0.1 pixel(s)" with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+2 title "Ecart type 0.5 pixel(s)" with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+3 title "Ecart type 1 pixel(s)" with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+4 title "Ecart type 2 pixel(s)" with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+5 title "Ecart type 3 pixel(s)" with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+6 title "Ecart type 4 pixel(s)" with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+7 title "Ecart type 5 pixel(s)" with lines lw 5, \
      5*sqrt(2)/sqrt(x-1)  title "Modele ecart type 5 pixel(s)" with points, \
      4*sqrt(2)/sqrt(x-1)  title "Modele ecart type 4 pixel(s)" with points, \
      3*sqrt(2)/sqrt(x-1)  title "Modele ecart type 3 pixel(s)" with points, \
      2*sqrt(2)/sqrt(x-1)  title "Modele ecart type 2 pixel(s)2" with points, \
      1*sqrt(2)/sqrt(x-1)  title "Modele ecart type 1 pixel(s)1" with points 
     

pause -1


quit
