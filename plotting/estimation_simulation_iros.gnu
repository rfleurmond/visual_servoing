set terminal postscript eps enhanced size 10,6 color solid font 'Helvetica,30' 

h1 = 3
h2 = 8

set style arrow 1 head size screen 0.025,15 lw 5 lt 2 lc rgb "red"
set style arrow 2 head size screen 0.025,15 lw 5 lt 2 lc rgb "blue"


set output "estimation-p.eps"
col = 2
set title "Estimation du parametre  P"
set grid

set xlabel "Temps (secondes)"
set ylabel "Coordonnees (metres)"

top = 0.0015
bot = -0.001

set yrange[bot:top]

set arrow from h1,top to h1,bot as 1
set arrow from h2,bot to h2,top as 2


plot "./estim-nonlinear.dat" using 1:col   title "x" with lines lw 8, \
     "./estim-nonlinear.dat" using 1:col+1 title "y" with lines lw 8, \
     "./estim-nonlinear.dat" using 1:col+2 title "z" with lines lw 8
pause -1
col = col +3





t1 = 0
t2 = 10

set output "estimation-u.eps"
set title "Estimation du parametre U"

set object 1 rect from t1,-0.04 to t2, 0.06 fs empty

set grid
set xlabel "Temps (secondes)"
set ylabel "Coordonnees (metres)"

top = 1.2
bot = -0.2

set yrange[bot:top]

set noarrow 1
set noarrow 2
set arrow from h1,top to h1,bot as 1
set arrow from h2,bot to h2,top as 2


set arrow from 4.2,0.10 to 7.5,0.4 head size screen 0.02,15 lw 2

set multiplot

plot "./estim-nonlinear.dat" using 1:col   title "x" with lines lw 8, \
     "./estim-nonlinear.dat" using 1:col+1 title "y" with lines lw 8, \
     "./estim-nonlinear.dat" using 1:col+2 title "z" with lines lw 8

set size 0.3,0.3
set origin 0.6,0.35
set bmargin 0; set tmargin 0; set lmargin 0; set rmargin 0
clear
set title "Details"
set xrange [t1:t2]
set yrange [-0.004:0.006]
unset xlabel 
unset ylabel 
set grid
set ytics 0.002 offset -0.004,0

set noarrow 1
set noarrow 2

plot "./estim-nonlinear.dat" using 1:col   title "x" with lines lw 8, \
     "./estim-nonlinear.dat" using 1:col+1 title "y" with lines lw 8
unset multiplot
pause -1
col = col + 3



reset
set terminal postscript eps enhanced size 10,6 color solid font 'Helvetica,30' 

h1 = 3
h2 = 8

set style arrow 1 head size screen 0.025,15 lw 5 lt 2 lc rgb "red"
set style arrow 2 head size screen 0.025,15 lw 5 lt 2 lc rgb "blue"


set output "estimation-k.eps"

top = 0.08
bot = -0.01

set yrange[bot:top]
set xrange[0:16]

set title "Estimation du parametre K"

set object 1 rect from 0,0.065 to 4, 0.075 fs empty

set grid
set xlabel "Temps (secondes)"
set ylabel "Coordonnees (metres)"


#set noarrow 1
#set noarrow 2
set arrow from h1,top to h1,bot as 1
set arrow from h2,bot to h2,top as 2

set arrow from 3.5,0.064 to 7.5,0.035 head size screen 0.02,15 lw 2


set multiplot

plot "./estim-nonlinear.dat" using 1:col   title "x" with lines lw 8, \
     "./estim-nonlinear.dat" using 1:col+1 title "y" with lines lw 8, \
     "./estim-nonlinear.dat" using 1:col+2 title "z" with lines lw 8



set size 0.3,0.3
set origin 0.6,0.3
set bmargin 0; set tmargin 0; set lmargin 0; set rmargin 0
clear
set title "Details"
set xrange [0:4]
set yrange [0.068:0.072]
unset xlabel 
unset ylabel 
set grid
set ytics 0.001 offset 0.068,0

set noarrow 1
set noarrow 2

plot "./estim-nonlinear.dat" using 1:col+2 title "z" with lines lw 8

unset multiplot
pause -1
quit
