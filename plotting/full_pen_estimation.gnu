set terminal postscript eps enhanced size 10,6 color solid font 'Helvetica,30' 

set output "r-estimation.eps"
col = 2
set title "Estimation du rayon du stylo"
set yrange[0.008:0.012]
set grid

set xlabel "Temps (secondes)"
set ylabel "Valeur (metres)"


plot "./estimation-pen.dat" using 1:col   title "rayon" with lines lw 9 
pause -1
col = col + 1


set output "b-axis-estimation.eps"
set title "Estimation du parametre B du stylo"
set yrange[-1:1]
set grid
set xlabel "Temps (secondes)"
set ylabel "Coordonnees (metres)"
plot "./estimation-pen.dat" using 1:col   title "x" with lines lw 5 , \
     "./estimation-pen.dat" using 1:col+1 title "y" with lines lw 5 , \
     "./estimation-pen.dat" using 1:col+2 title "z" with lines lw 5 

pause -1
col = col + 3


set output "t-axis-estimation.eps"
set title "Estimation du parametre T du stylo"
set yrange[-1:1]
set grid
set xlabel "Temps (secondes)"
set ylabel "Coordonnees (metres)"

plot "./estimation-pen.dat" using 1:col   title "x" with lines lw 5 , \
     "./estimation-pen.dat" using 1:col+1 title "y" with lines lw 5 , \
     "./estimation-pen.dat" using 1:col+2 title "z" with lines lw 5 
pause -1



#set terminal postscript eps enhanced size 10,6 color solid font 'Helvetica,30' 

set output "r2-estimation.eps"
col = 2
set title "Estimation du rayon du capuchon"
set yrange[0.008:0.012]
set grid

set xlabel "Temps (secondes)"
set ylabel "Valeur (metres)"


plot "./estimation-cap.dat" using 1:col   title "rayon" with lines lw 9 
pause -1
col = col + 1


set output "b2-axis-estimation.eps"
set title "Estimation du parametre B du capuchon"
set yrange[-1:1]
set grid
set xlabel "Temps (secondes)"
set ylabel "Coordonnees (metres)"
plot "./estimation-cap.dat" using 1:col   title "x" with lines lw 5 , \
     "./estimation-cap.dat" using 1:col+1 title "y" with lines lw 5 , \
     "./estimation-cap.dat" using 1:col+2 title "z" with lines lw 5 

pause -1
col = col + 3


set output "t2-axis-estimation.eps"
set title "Estimation du parametre T du capuchon"
set yrange[-1:1]
set grid
set xlabel "Temps (secondes)"
set ylabel "Coordonnees (metres)"

plot "./estimation-cap.dat" using 1:col   title "x" with lines lw 5 , \
     "./estimation-cap.dat" using 1:col+1 title "y" with lines lw 5 , \
     "./estimation-cap.dat" using 1:col+2 title "z" with lines lw 5 
pause -1

quit

