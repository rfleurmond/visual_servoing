set terminal postscript eps enhanced size 10,6 color solid font 'Helvetica,30' 

set output "r-estimation.eps"
col = 2
set title "Estimation du rayon"
set yrange[0:0.03]
set grid
set xlabel "Temps (secondes)"
set ylabel "Valeur (metres)"
plot "./estim-nonlinear.dat" using 1:col   title "rayon" with lines lw 5 
pause -1
col = col + 1


set output "b-axis-estimation.eps"
set title "Estimation de B dans F_o"
set yrange[-1:1]
set grid
set xlabel "Temps (secondes)"
set ylabel "Coordonees (metres)"
plot "./estim-nonlinear.dat" using 1:col   title "x" with lines lw 5 , \
     "./estim-nonlinear.dat" using 1:col+1 title "y" with lines lw 5 , \
     "./estim-nonlinear.dat" using 1:col+2 title "z" with lines lw 5 

pause -1
col = col + 3


set output "t-axis-estimation.eps"
set title "Estimation de T dans F_o"
set yrange[-0.1:0.2]
set grid
set xlabel "Temps (secondes)"
set ylabel "Coordonees (metres)"

plot "./estim-nonlinear.dat" using 1:col   title "x" with lines lw 5 , \
     "./estim-nonlinear.dat" using 1:col+1 title "y" with lines lw 5 , \
     "./estim-nonlinear.dat" using 1:col+2 title "z" with lines lw 5 
pause -1


set output "length-axis-estimation.eps"
set title "Estimation de la longueur"
set yrange[0:0.5]
set grid
set xlabel "Temps (secondes)"
set ylabel "Coordonees (metres)"

plot "./estim-nonlinear.dat" using 1:col+3  title "Longueur" with lines lw 5

pause -1
quit
