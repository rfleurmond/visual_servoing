set terminal postscript eps enhanced size 10,6 color solid 2font 'Helvetica,30' 

set output "bounds-task.eps"
col = 1
set title "Tache pour l'evitement des butees"
set grid
set xlabel "Temps (secondes)"
set ylabel "Valeur des composantes de la tache"
plot "./data_task.dat" using 1:col+1 title "F_{b1}" with lines lw 4 lt 1 lc rgb "blue", \
     "./data_task.dat" using 1:col+2 title "F_{b2}" with lines lw 5, \
     "./data_task.dat" using 1:col+3 title "F_{b3}" with lines lw 5, \
     "./data_task.dat" using 1:col+4 title "F_{b4}" with lines lw 5, \
     "./data_task.dat" using 1:col+5 title "F_{b6}" with lines lw 4 lt 1 lc rgb "red", \
     "./data_task.dat" using 1:col+6 title "F_{b8}" with lines lw 5, \
     "./data_task.dat" using 1:col+7 title "F_{b9}" with lines lw 5, \
     "./data_task.dat" using 1:col+8 title "F_{b10}" with lines lw 5, \
     "./data_task.dat" using 1:col+9 title "F_{b11}" with lines lw 5, \
     "./data_task.dat" using 1:col+10 title "F_{b13}" with points lw 2 lc rgb "blue", \
     "./data_task.dat" using 1:col+11 title "F_{b15}" with lines lw 5, \
     "./data_task.dat" using 1:col+12 title "F_{b16}" with lines lw 5
pause -1
col = col + 12


set output "real-task.eps"
set title "Tache referencee vision"
set mxtics
set grid
set xlabel "Temps (secondes)"
set ylabel "Erreur (pixels ou 1/2 degres)" 
plot "./data_task.dat" using 1:col+1 title "v_1" with lines lw 5, \
 "./data_task.dat" using 1:col+2 title "v_2" with lines lw 5, \
 "./data_task.dat" using 1:col+3 title "v_3" with lines lw 5, \
 "./data_task.dat" using 1:col+4 title "v_4" with lines lw 5, \
 "./data_task.dat" using 1:col+5 title "v_5" with lines lw 5
pause -1

#col = col + 5
#set output "visibility-task.eps"
#set title "Tache pour l'evitement des occultations"
#set grid
#set xlabel "Temps (secondes)"
#set ylabel "Valeur des composantes de la tache (pixels)" 
#plot "./data_task.dat" using 1:col+1 title "F_{v1}" with lines lw 5, \
# "./data_task.dat" using 1:col+2 title "F_{v2}" with lines lw 5, \
# "./data_task.dat" using 1:col+3 title "F_{v3}" with lines lw 5, \
# "./data_task.dat" using 1:col+4 title "F_{v4}" with lines lw 5, \
# "./data_task.dat" using 1:col+5 title "F_{v5}" with lines lw 5, \
# "./data_task.dat" using 1:col+6 title "F_{v6}" with lines lw 5, \
# "./data_task.dat" using 1:col+7 title "F_{v7}" with lines lw 5, \
# "./data_task.dat" using 1:col+8 title "F_{v8}" with lines lw 5, \
# "./data_task.dat" using 1:col+9 title "F_{v9}" with lines lw 5, \
# "./data_task.dat" using 1:col+10 title "F_{v10}" with lines lw 5, \
# "./data_task.dat" using 1:col+11 title "F_{v11}" with lines lw 5, \
# "./data_task.dat" using 1:col+12 title "F_{v12}" with lines lw 5, \
# "./data_task.dat" using 1:col+13 title "F_{v13}" with lines lw 5, \
# "./data_task.dat" using 1:col+14 title "F_{v14}" with lines lw 5, \
# "./data_task.dat" using 1:col+15 title "F_{v15}" with lines lw 5, \
# "./data_task.dat" using 1:col+16 title "F_{v16}" with lines lw 5, \
# "./data_task.dat" using 1:col+17 title "F_{v17}" with lines lw 5, \
# "./data_task.dat" using 1:col+18 title "F_{v18}" with lines lw 5, \
# "./data_task.dat" using 1:col+19 title "F_{v19}" with lines lw 5, \
# "./data_task.dat" using 1:col+20 title "F_{v20}" with lines lw 5, \
# "./data_task.dat" using 1:col+21 title "F_{v21}" with lines lw 5, \
# "./data_task.dat" using 1:col+22 title "F_{v22}" with lines lw 5, \
# "./data_task.dat" using 1:col+23 title "F_{v23}" with lines lw 5, \
# "./data_task.dat" using 1:col+24 title "F_{v24}" with lines lw 5
#pause -1


col = 1
set output "left-control.eps"
set title " Commande en vitesse sur le bras gauche "
set grid
set xlabel "Temps (secondes)"
set ylabel "Vitesse articulaire (rad/s)" 
plot "./data_sys.dat" using 1:col+1 title "q_1" with lines lw 5, \
 "./data_sys.dat" using 1:col+2 title "q_2" with lines lw 5, \
 "./data_sys.dat" using 1:col+3 title "q_3" with lines lw 5, \
 "./data_sys.dat" using 1:col+4 title "q_4" with lines lw 5, \
 "./data_sys.dat" using 1:col+5 title "q_5" with lines lw 5, \
 "./data_sys.dat" using 1:col+6 title "q_6" with lines lw 5, \
 "./data_sys.dat" using 1:col+7 title "q_7" with lines lw 5
pause -1
col = col+7       


set output "right-control.eps"
set title " Commande en vitesse sur le bras droit"
set grid
set xlabel "Temps (secondes)"
set ylabel "Vitesse articulaire (rad/s)" 
plot "./data_sys.dat" using 1:col+1 title "q_1" with lines lw 5, \
 "./data_sys.dat" using 1:col+2 title "q_2" with lines lw 5, \
 "./data_sys.dat" using 1:col+3 title "q_3" with lines lw 5, \
 "./data_sys.dat" using 1:col+4 title "q_4" with lines lw 5, \
 "./data_sys.dat" using 1:col+5 title "q_5" with lines lw 5, \
 "./data_sys.dat" using 1:col+6 title "q_6" with lines lw 5, \
 "./data_sys.dat" using 1:col+7 title "q_7" with lines lw 5
pause -1
col = col+7       

set output "head-control.eps"
set title "Commande en vitesse sur la tete"
set grid
set xlabel "Temps (secondes)"
set ylabel "Vitesse articulaire (rad/s)" 
plot "./data_sys.dat" using 1:col+1 title "q_1" with lines lw 5, \
 "./data_sys.dat" using 1:col+2 title "q_2" with lines lw 5
pause -1
col = col+2       

set output "poubelle.eps"
gauche = col
set title "Left joint coordinates values"
set grid
set xlabel "Temps (secondes)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:col+1 title "Q1" with lines lw 5, \
 "./data_sys.dat" using 1:col+2 title "Q2" with lines lw 5, \
 "./data_sys.dat" using 1:col+3 title "Q3" with lines lw 5, \
 "./data_sys.dat" using 1:col+4 title "Q4" with lines lw 5, \
 "./data_sys.dat" using 1:col+5 title "Q5" with lines lw 5, \
 "./data_sys.dat" using 1:col+6 title "Q6" with lines lw 5, \
 "./data_sys.dat" using 1:col+7 title "Q7" with lines lw 5
pause -1
col = col+7       

set output "poubelle.eps"
droite = col
set title "Right joint coordinate values"
set grid
set xlabel "Temps (secondes)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:col+1 title "Q1" with lines lw 5, \
 "./data_sys.dat" using 1:col+2 title "Q2" with lines lw 5, \
 "./data_sys.dat" using 1:col+3 title "Q3" with lines lw 5, \
 "./data_sys.dat" using 1:col+4 title "Q4" with lines lw 5, \
 "./data_sys.dat" using 1:col+5 title "Q5" with lines lw 5, \
 "./data_sys.dat" using 1:col+6 title "Q6" with lines lw 5, \
 "./data_sys.dat" using 1:col+7 title "Q7" with lines lw 5
pause -1
col = col+7       

set output "poubelle.eps"
set title "Head joint coordinate values"
set grid
set xlabel "Temps (secondes)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:col+1 title "Q1" with lines lw 5, \
 "./data_sys.dat" using 1:col+2 title "Q2" with lines lw 5
pause -1

quit