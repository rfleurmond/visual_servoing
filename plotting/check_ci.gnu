col = 6
p1 = col
set title "Position de la camera"
set grid
set xlabel "X (metres)"
set ylabel "Y (metres)"
set zlabel "Z (metres)"
splot "../../Data/exp1/pose_camera.dat" using col:col+1:col+2   title "C_i" with lines
pause -1
quit