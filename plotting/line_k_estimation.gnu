col = 2
set title "Estimation of parameter P"
set grid

set xlabel "Time (seconds)"
set ylabel "Coordinates (meters)"


plot "./estim-nonlinear.dat" using 1:col   title "x" with lines, \
     "./estim-nonlinear.dat" using 1:col+1 title "y" with lines , \
     "./estim-nonlinear.dat" using 1:col+2 title "z" with lines 
pause -1
col = col +3


p = col
set title "Estimation of parameter U"
set grid
set xlabel "Time (seconds)"
set ylabel "Coordinates (meters)"
plot "./estim-nonlinear.dat" using 1:col   title "x" with lines , \
     "./estim-nonlinear.dat" using 1:col+1 title "y" with lines , \
     "./estim-nonlinear.dat" using 1:col+2 title "z" with lines 

pause -1
col = col + 3


set title "Estimation of parameter R"
set grid
set xlabel "Time (seconds)"
set ylabel "Coordinates (meters)"

plot "./estim-nonlinear.dat" using 1:col   title "x" with lines , \
     "./estim-nonlinear.dat" using 1:col+1 title "y" with lines , \
     "./estim-nonlinear.dat" using 1:col+2 title "z" with lines 
pause -1

quit
