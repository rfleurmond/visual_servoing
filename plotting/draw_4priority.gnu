col = 1
set title "Evolution de la commande dans le temps"
set grid
set xlabel "Temps(secondes)"
set ylabel "Commande en vitesse m/s" 
plot "./priority1.task" using 1:col+1 title "cmd1" with lines
pause -1
replot "./priority2.task" using 1:col+1 title "cmd3" with lines
pause-1
replot "./priority3.task" using 1:col+1 title "cmd5" with lines
pause -1
replot "./priority4.task" using 1:col+1 title "cmd7" with lines
pause -1

set title "Evolution de la commande dans le temps"
set grid
set xlabel "Temps(secondes)"
set ylabel "Commande en vitesse m/s" 
plot "./priority1.task" using 1:col+2 title "cmd2" with lines
replot "./priority2.task" using 1:col+2 title "cmd4" with lines
replot "./priority3.task" using 1:col+2 title "cmd6" with lines
replot "./priority4.task" using 1:col+2 title "cmd8" with lines
pause -1

pos = 4
set title "Trajectoire du robot dans l'espace"
set size ratio -1
set xrange [-1:6]
set yrange [-1:6]
set grid
set xlabel "X (metre)"
set ylabel "Y (metre)" 
set parametric
plot [0:10] t,5 title "Master goal", t, 8-t title "Slave goal", 0.3*t, 0.5*t title "Straight trajectory"
pause -1
replot "./priority1.task" using pos:pos+1 title "Egality trajectory" with lines
pause -1
replot "./priority2.task" using pos:pos+1 title "Priority trajectory" with lines
pause -1
replot "./priority3.task" using pos:pos+1 title "Optimal trajectory" with lines
pause -1
replot "./priority4.task" using pos:pos+1 title "Our trajectory" with lines
pause -1

quit