set terminal postscript eps enhanced size 10,6 color solid 2font 'Helvetica,30' 

set style arrow 1 head size screen 0.025,15 lw 5 lt 2 lc rgb "red"
set style arrow 2 head size screen 0.025,15 lw 5 lt 2 lc rgb "blue"
set style arrow 3 nohead lw 3 lc rgb "black"

set output "bounds-task.eps"
col = 1
set title "Tache pour l'evitement des butees"
set grid
set xlabel "Temps (secondes)"
set ylabel "Valeur des composantes de la tache"
plot "./data_task.dat" using 1:col+1 title "F_{b1}" with lines lw 4 lt 1 lc rgb "blue", \
     "./data_task.dat" using 1:col+2 title "F_{b2}" with lines lw 5, \
     "./data_task.dat" using 1:col+3 title "F_{b3}" with lines lw 5, \
     "./data_task.dat" using 1:col+4 title "F_{b4}" with lines lw 5, \
     "./data_task.dat" using 1:col+5 title "F_{b6}" with lines lw 4 lt 1 lc rgb "red", \
     "./data_task.dat" using 1:col+6 title "F_{b8}" with lines lw 5, \
     "./data_task.dat" using 1:col+7 title "F_{b9}" with lines lw 5, \
     "./data_task.dat" using 1:col+8 title "F_{b10}" with lines lw 5, \
     "./data_task.dat" using 1:col+9 title "F_{b11}" with lines lw 5, \
     "./data_task.dat" using 1:col+10 title "F_{b13}" with points lw 2 lc rgb "blue"
pause -1
col = col + 10


set output "real-task.eps"
t1 = 4.9
t2 = 12.3
h1 = 3
h2 = 8

top = 150

set title "Taches referencee vision pour la fermeture d'un stylo"
set grid
set xlabel "Temps (secondes)"
set ylabel "Erreur (pixels et/ou 1/2 de degres)" 

set arrow from t1,-300 to t1,top as 3
set arrow from t2,-300 to t2,top as 3

set arrow from h1,top to h1,-300 as 1
set arrow from h2,-300 to h2,top as 2

plot "./data_task.dat" using 1:col+1 title "" with lines lw 5, \
 "./data_task.dat" using 1:col+2 title "" with lines lw 5, \
 "./data_task.dat" using 1:col+3 title "" with lines lw 5, \
 "./data_task.dat" using 1:col+4 title "" with lines lw 5, \
 "./data_task.dat" using 1:col+5 title "" with lines lw 5
pause -1

set output "left-control.eps"
col = 1
set title "Commande en vitesse sur le bras gauche "
set grid
set mxtics
set xlabel "Temps (secondes)"
set ylabel "Vitesse articulaire (rad/s)" 

set noarrow 1
set noarrow 2
set noarrow 3
set noarrow 4

set arrow from t1,-0.14 to t1,0.06 as 3
set arrow from t2,-0.14 to t2,0.06 as 3

set arrow from h1,0.06 to h1,-0.14 as 1
set arrow from h2,-0.14 to h2,0.06 as 2

plot "./data_sys.dat" using 1:col+1 title "q_1" with lines lw 5, \
 "./data_sys.dat" using 1:col+2 title "q_2" with lines lw 5, \
 "./data_sys.dat" using 1:col+3 title "q_3" with lines lw 5, \
 "./data_sys.dat" using 1:col+4 title "q_4" with lines lw 5, \
 "./data_sys.dat" using 1:col+5 title "q_5" with lines lw 5, \
 "./data_sys.dat" using 1:col+6 title "q_6" with lines lw 5, \
 "./data_sys.dat" using 1:col+7 title "q_7" with lines lw 5

pause -1
col = col+7       

set output "right-control.eps"
set title "Commande en vitesse sur le bras droit "
set grid
set mxtics
set xlabel "Temps (secondes)"
set ylabel "Vitesse articulaire (rad/s)" 

set noarrow 1
set noarrow 2
set noarrow 3
set noarrow 4

set arrow from t1,-0.2 to t1,0.15 as 3
set arrow from t2,-0.2 to t2,0.15 as 3

set arrow from h1,0.15 to h1,-0.2 as 1
set arrow from h2,-0.2 to h2,0.15 as 2

plot "./data_sys.dat" using 1:col+1 title "q_1" with lines lw 5, \
 "./data_sys.dat" using 1:col+2 title "q_2" with lines lw 5, \
 "./data_sys.dat" using 1:col+3 title "q_3" with lines lw 5, \
 "./data_sys.dat" using 1:col+4 title "q_4" with lines lw 5, \
 "./data_sys.dat" using 1:col+5 title "q_5" with lines lw 5, \
 "./data_sys.dat" using 1:col+6 title "q_6" with lines lw 5, \
 "./data_sys.dat" using 1:col+7 title "q_7" with lines lw 5
pause -1
quit