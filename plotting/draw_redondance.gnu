col = 1
set title "Evolution de la tache  dans le temps"
set grid
set xlabel "Temps(secondes)"
set ylabel "Erreur en pixel" 
plot "./data_task.dat" using 1:col+1 title "T1" with lines
replot "./data_task.dat" using 1:col+2 title "T2" with lines
replot "./data_task.dat" using 1:col+3 title "T3" with lines
pause -1

set title "Evolution de la commande dans le temps"
set grid
set xlabel "Temps(secondes)"
set ylabel "Commande en vitesse m/s" 
plot "./data_sys.dat" using 1:col+1 title "cmd1" with lines
replot "./data_sys.dat" using 1:col+2 title "cmd2" with lines
pause -1
cmd = col+1
col = col+2       

pos = col+1
set title "Evolution de la position dans le temps"
set grid
set xlabel "Temps(secondes)"
set ylabel "Position (m)" 
plot "./data_sys.dat" using 1:col+1 title "q1" with lines
replot "./data_sys.dat" using 1:col+2 title "q2" with lines
pause -1


set title "Variation de la vitesse dans l'espace"
set grid
set xlabel "X (metre/seconde)"
set ylabel "Y (metre/seconde)" 
plot "./data_sys.dat" using cmd:cmd+1 title "V" with lines
pause -1

set title "Variation de la vitesse en fonction de la position"
set grid
set ylabel "dot X (metre/seconde)"
set xlabel " X (metre)" 
plot "./data_sys.dat" using pos:cmd title "V" with lines
pause -1

set title "Variation de la vitesse en fonction de la position"
set grid
set ylabel "dot Y (metre/seconde)"
set xlabel " Y (metre)" 
plot "./data_sys.dat" using pos+1:cmd+1 title "V" with lines
pause -1


set title "Trajectoire du robot dans l'espace"
set size ratio -1
set xrange [0:8]
set yrange [0:8]
set grid
set xlabel "X (metre)"
set ylabel "Y (metre)" 
set parametric
plot [0:10] t,t title "Main task Goal" with points,\
 5, t title "Constraint x < 5",\
 4, t title "Activation Constraint x < 5"
replot "./data_sys.dat" using pos:pos+1 title "Real trajectory" with lines
pause -1

quit