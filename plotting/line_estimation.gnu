set terminal postscript eps enhanced size 10,6 color font 'Helvetica,30'

col = 2
set output "p-line-estimation.eps"
set title "Estimation de P"
set grid
#set yrange [-0.05:0.15]
set xlabel "Temps (secondes)"
set ylabel "Position (metres)"
plot "./estim-nonlinear.dat" using 1:col   title "x" with lines lw 5, \
     "./estim-nonlinear.dat" using 1:col+1 title "y" with lines lw 5, \
     "./estim-nonlinear.dat" using 1:col+2 title "z" with lines lw 5
pause -1
col = col +3

p = col
set output "u-line-estimation.eps"
set title "Estimation de U"
set grid
#set yrange [-0.5:1.5]
set xlabel "Temps (secondes)"
set ylabel "Position (metres)"
plot "./estim-nonlinear.dat" using 1:col   title "x" with lines lw 5, \
     "./estim-nonlinear.dat" using 1:col+1 title "y" with lines lw 5, \
     "./estim-nonlinear.dat" using 1:col+2 title "z" with lines lw 5
pause -1

set output "u-3d-line-estimation.eps"
set title "Estimation de U"
set grid
set size ratio -1
set xrange [-1:1]
set yrange [-1:1]
set zrange [-1:1]
set xlabel "X (metres)"
set ylabel "Y (metres)"
set zlabel "Z (metres) 
splot "./estim-nonlinear.dat" using p:p+1:p+2 title "U" with points
pause -1
quit
