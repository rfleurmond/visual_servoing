set terminal postscript eps enhanced size 10,6 color solid lw 5 font 'Helvetica,25' 

cmd = 2
pos = 4

set output "vitesses.eps"
set title "Variation de la vitesse dans l'espace"
set grid
set xrange [-1:4]
set yrange [-1:1]
set xlabel "dx/dt (metre/seconde)"
set ylabel "dy/dt (metre/seconde)" 
plot "./data_sys1.dat" using cmd:cmd+1 title "Ponderation #1" with lines lt rgb "#FF00FF",\
 "./data_sys2.dat" using cmd:cmd+1 title "Redondance classique #2" with lines lt rgb "green", \
 "./data_sys3.dat" using cmd:cmd+1 title "Redondance optimale #3" with lines lt rgb "blue", \
 "./data_sys4.dat" using cmd:cmd+1 title "Notre approche #4" with lines lw 2 lt rgb "red"
pause -1

set output "fail-optimale.eps"
set title "Coordonnees du robot en fonction du temps"
set xrange [0:30]
set yrange [-15:20]
set grid
set ylabel "Coordonnee (metres)"
set xlabel "Temps (secondes)" 
set parametric
plot "./data_sys3.dat" using 1:pos title "X" with lines lt rgb "blue", \
     "./data_sys3.dat" using 1:pos+1 title "Y" with lines lt rgb "red"
pause -1

set output "success-approach.eps"
set title "Coordonnees du robot en fonction du temps"
set xrange [0:14]
set yrange [-1:7]
set grid
set ylabel "Coordonnee (metres)"
set xlabel "Temps (secondes)" 
set parametric
plot "./data_sys4.dat" using 1:pos title "X" with lines lt rgb "blue", \
     "./data_sys4.dat" using 1:pos+1 title "Y" with lines lt rgb "red"
pause -1

set output "security-classic.eps"
set title "Coordonnees du robot en fonction du temps"
set xrange [0:14]
set yrange [-1:7]
set grid
set ylabel "Coordonnee (metres)"
set xlabel "Temps (secondes)" 
set parametric
plot "./data_sys2.dat" using 1:pos title "X" with lines lt rgb "blue", \
     "./data_sys2.dat" using 1:pos+1 title "Y" with lines lt rgb "red"
pause -1



set output "trajectoires.eps"
set title "Trajectoires du robot dans le plan"
set size ratio -1
set xrange [-1:11]
set yrange [-3:4]
set grid
set xlabel "X (metres)"
set ylabel "Y (metres)" 
set parametric
plot [0:6] 6+2*sin(1.1*t), 2*cos(1.1*t) title "Zone critique" with dots, \
 t, 0.3333333*t title "Trajectoire pour la cible prioritaire #5" with lines  lt rgb "purple", \
 t,-0.3333333*t title "Trajectoire pour la cible secondaire #6" with lines  lt rgb "orange", \
 "./data_sys1.dat" using pos:pos+1 title "Ponderation  #1" with lines lt rgb "#FF00FF", \
 "./data_sys2.dat" using pos:pos+1 title "Redondance classique  #2" with lines lt rgb "green", \
 "./data_sys3.dat" using pos:pos+1 title "Redondance optimale  #3" with lines lt rgb "blue", \
 "./data_sys4.dat" using pos:pos+1 title "Notre approche  #4" with lines lw 2 lt rgb "red"
pause -1

quit