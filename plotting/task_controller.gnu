set terminal postscript eps enhanced size 10,6 color solid font 'Helvetica,30' 

col = 1
set output "visual-task.eps"
set title "Evolution de la tache  dans le temps"
set grid
set xlabel "Temps(secondes)"
set ylabel "Erreur en pixel ou 1/2 de degres" 
plot "./data_task.dat" using 1:col+1 title "T1" with lines lw 5, \
       "./data_task.dat" using 1:col+2 title "T2" with lines lw 5, \
       "./data_task.dat" using 1:col+3 title "T3" with lines lw 5, \
       "./data_task.dat" using 1:col+4 title "T4" with lines lw 5, \
       "./data_task.dat" using 1:col+5 title "T5" with lines lw 5, \
       "./data_task.dat" using 1:col+6 title "T6" with lines lw 5, \
       "./data_task.dat" using 1:col+7 title "T7" with lines lw 5, \
       "./data_task.dat" using 1:col+8 title "T8" with lines lw 5
pause -1

set output "angular-velocity.eps"
set title "Evolution de la commande (vitesse angulaire) dans le temps"
set grid
set xlabel "Temps(secondes)"
set ylabel "Commande en vitesse(rad/s)" 
plot "./data_sys.dat" using 1:col+1 title "Wx" with lines lw 5, \
       "./data_sys.dat" using 1:col+2 title "Wy" with lines lw 5, \
       "./data_sys.dat" using 1:col+3 title "Wz" with lines lw 5
pause -1
col = col+3       

set output "linear-velocity.eps"
set title "Evolution de la commande (vitesse lineraire) dans le temps"
set grid
set xlabel "Temps(secondes)"
set ylabel "Commande en vitesse(m/s)" 
plot "./data_sys.dat" using 1:col+1 title "Vx" with lines lw 5, \
       "./data_sys.dat" using 1:col+2 title "Vy" with lines lw 5, \
       "./data_sys.dat" using 1:col+3 title "Vz" with lines lw 5
pause -1
col = col+3

set output "angular-position.eps"
set title "Evolution de l'orientation de la camera dans le temps"
set grid
set xlabel "Temps(secondes)"
set ylabel "orientation (radians)" 
plot "./data_sys.dat" using 1:col+1 title "Ux" with lines lw 5, \
       "./data_sys.dat" using 1:col+2 title "Uy" with lines lw 5, \
       "./data_sys.dat" using 1:col+3 title "Uz" with lines lw 5
pause -1
col = col+3

pos = col+1
set output "linear-position.eps"
set title "Evolution de la position de la camera dans le temps"
set grid
set xlabel "Temps(secondes)"
set ylabel "Position (metres)" 
plot "./data_sys.dat" using 1:col+1 title "Px" with lines lw 5, \
       "./data_sys.dat" using 1:col+2 title "Py" with lines lw 5, \
       "./data_sys.dat" using 1:col+3 title "Pz" with lines lw 5
pause -1
col = col+3

set output "3d-camera-trajectory.eps"
set title "Trajectoire de la camera dans l'espace"
set grid
set xlabel "X (metres)"
set ylabel "Y (metres)"
set zlabel "Z (metres) 
splot "./data_sys.dat" using pos:pos+1:pos+2 title "Trajectoire de la camera" with lines lw 5
pause -1

quit