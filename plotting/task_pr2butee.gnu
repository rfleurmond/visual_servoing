col = 1
set title "Bounds Task values"
set grid
set xlabel "Time (seconds)"
set ylabel "Metric errors in image" 
plot "./data_task.dat" using 1:col+1 title "T1" with lines
replot "./data_task.dat" using 1:col+2 title "T2" with lines
replot "./data_task.dat" using 1:col+3 title "T3" with lines
replot "./data_task.dat" using 1:col+4 title "T4" with lines
replot "./data_task.dat" using 1:col+5 title "T5" with lines
replot "./data_task.dat" using 1:col+6 title "T6" with lines
replot "./data_task.dat" using 1:col+7 title "T7" with lines
replot "./data_task.dat" using 1:col+8 title "T8" with lines
replot "./data_task.dat" using 1:col+9 title "T9" with lines
replot "./data_task.dat" using 1:col+10 title "T10" with lines
pause -1

t1 = 5.107
t2 = 9.709

set title "Real Task values"
set mxtics
set grid
set xlabel "Time (seconds)"
set ylabel "Metric errors in image" 
set arrow from t1,-200 to t1,200 nohead
set arrow from t2,-200 to t2,200 nohead
plot "./data_task.dat" using 1:col+11 title "T1" with lines
replot "./data_task.dat" using 1:col+12 title "T2" with lines
replot "./data_task.dat" using 1:col+13 title "T3" with lines
replot "./data_task.dat" using 1:col+14 title "T4" with lines
replot "./data_task.dat" using 1:col+15 title "T5" with lines
pause -1

col = 1
set title " Command on left arm"
set grid
set xlabel "Time (seconds)"
set ylabel "Command (rad/s)" 
set noarrow 1
set noarrow 2
set arrow from t1,-0.3 to t1,0.2 nohead
set arrow from t2,-0.3 to t2,0.2 nohead
plot "./data_sys.dat" using 1:col+1 title "Q1" with lines
replot "./data_sys.dat" using 1:col+2 title "Q2" with lines
replot "./data_sys.dat" using 1:col+3 title "Q3" with lines
replot "./data_sys.dat" using 1:col+4 title "Q4" with lines
replot "./data_sys.dat" using 1:col+5 title "Q5" with lines
replot "./data_sys.dat" using 1:col+6 title "Q6" with lines
replot "./data_sys.dat" using 1:col+7 title "Q7" with lines
pause -1
col = col+7       


set title "Command on right arm "
set grid
set xlabel "Time (seconds)"
set ylabel "Command (rad/s)" 
set noarrow 1
set noarrow 2
set arrow from t1,-0.6 to t1,0.5 nohead
set arrow from t2,-0.6 to t2,0.5 nohead
plot "./data_sys.dat" using 1:col+1 title "Q1" with lines
replot "./data_sys.dat" using 1:col+2 title "Q2" with lines
replot "./data_sys.dat" using 1:col+3 title "Q3" with lines
replot "./data_sys.dat" using 1:col+4 title "Q4" with lines
replot "./data_sys.dat" using 1:col+5 title "Q5" with lines
replot "./data_sys.dat" using 1:col+6 title "Q6" with lines
replot "./data_sys.dat" using 1:col+7 title "Q7" with lines
pause -1
col = col+7       

gauche = col
set title "Left joint coordinates values"
set grid
set noarrow 1
set noarrow 2
set xlabel "Time (seconds)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:col+1 title "Q1" with lines
replot "./data_sys.dat" using 1:col+2 title "Q2" with lines
replot "./data_sys.dat" using 1:col+3 title "Q3" with lines
replot "./data_sys.dat" using 1:col+4 title "Q4" with lines
replot "./data_sys.dat" using 1:col+5 title "Q5" with lines
replot "./data_sys.dat" using 1:col+6 title "Q6" with lines
replot "./data_sys.dat" using 1:col+7 title "Q7" with lines
pause -1
col = col+7       

droite = col
set title "Right joint coordinate values"
set grid
set xlabel "Time (seconds)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:col+1 title "Q1" with lines
replot "./data_sys.dat" using 1:col+2 title "Q2" with lines
replot "./data_sys.dat" using 1:col+3 title "Q3" with lines
replot "./data_sys.dat" using 1:col+4 title "Q4" with lines
replot "./data_sys.dat" using 1:col+5 title "Q5" with lines
replot "./data_sys.dat" using 1:col+6 title "Q6" with lines
replot "./data_sys.dat" using 1:col+7 title "Q7" with lines
pause -1
col = col+7       

set title "Left joint coordinates values respect to bounds"
set grid
set xlabel "Time (seconds)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:gauche+1 title "Q1" with lines
replot -0.69813170079 title "Min" with lines
replot 2.26892802759 title "Max" with lines
pause -1

set title "Left joint coordinates values respect to bounds"
set grid
set xlabel "Time (seconds)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:gauche+2 title "Q2" with lines
replot -0.52359877559 title "Min" with lines
replot 1.3962634016 title "Max" with lines
pause -1


set title "Left joint coordinates values respect to bounds"
set grid
set xlabel "Time (seconds)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:gauche+3 title "Q3" with lines
replot -0.76794487087  title "Min" with lines
replot 3.90953752447  title "Max" with lines
pause -1

set title "Left joint coordinates values respect to bounds"
set grid
set xlabel "Time (seconds)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:gauche+4 title "Q4" with lines
replot 0 title "Max" with lines
replot -2.26892802759  title "Min" with lines
pause -1

set title "Left joint coordinates values respect to bounds"
set grid
set xlabel "Time (seconds)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:gauche+6 title "Q6" with lines
replot 0 title "Max" with lines
replot -2.26892802759  title "Min" with lines
pause -1


set title "Right joint coordinates values respect to bounds"
set grid
set xlabel "Time (seconds)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:droite+1 title "Q1" with lines
replot  0.69813170079 title "Max" with lines
replot -2.26892802759 title "Min" with lines
pause -1

set title "Right joint coordinates values respect to bounds"
set grid
set xlabel "Time (seconds)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:droite+2 title "Q2" with lines
replot -0.52359877559 title "Min" with lines
replot  1.3962634016 title "Max" with lines
pause -1


set title "Right joint coordinates values respect to bounds"
set grid
set xlabel "Time (seconds)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:droite+3 title "Q3" with lines
replot  0.76794487087  title "Max" with lines
replot -3.90953752447  title "Min" with lines
pause -1

set title "Right joint coordinates values respect to bounds"
set grid
set xlabel "Time (seconds)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:droite+4 title "Q4" with lines
replot 0 title "Max" with lines
replot -2.26892802759  title "Min" with lines
pause -1

set title "Right joint coordinates values respect to bounds"
set grid
set xlabel "Time (seconds)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:droite+6 title "Q6" with lines
replot 0 title "Max" with lines
replot -2.26892802759  title "Min" with lines
pause -1


quit