col = 1
set title "Task values"
set grid
set xlabel "Time (seconds)"
set ylabel "Metric errors in image" 
plot "./data_task.dat" using 1:col+1 title "T1" with lines
replot "./data_task.dat" using 1:col+2 title "T2" with lines
replot "./data_task.dat" using 1:col+3 title "T3" with lines
replot "./data_task.dat" using 1:col+4 title "T4" with lines
replot "./data_task.dat" using 1:col+5 title "T5" with lines
pause -1


set title " Command on left arm"
set grid
set xlabel "Time (seconds)"
set ylabel "Command (rad/s)" 
plot "./data_sys.dat" using 1:col+1 title "Q1" with lines
replot "./data_sys.dat" using 1:col+2 title "Q2" with lines
replot "./data_sys.dat" using 1:col+3 title "Q3" with lines
replot "./data_sys.dat" using 1:col+4 title "Q4" with lines
replot "./data_sys.dat" using 1:col+5 title "Q5" with lines
replot "./data_sys.dat" using 1:col+6 title "Q6" with lines
replot "./data_sys.dat" using 1:col+7 title "Q7" with lines
pause -1
col = col+7       


set title "Command on right arm "
set grid
set xlabel "Time (seconds)"
set ylabel "Command (rad/s)" 
plot "./data_sys.dat" using 1:col+1 title "Q1" with lines
replot "./data_sys.dat" using 1:col+2 title "Q2" with lines
replot "./data_sys.dat" using 1:col+3 title "Q3" with lines
replot "./data_sys.dat" using 1:col+4 title "Q4" with lines
replot "./data_sys.dat" using 1:col+5 title "Q5" with lines
replot "./data_sys.dat" using 1:col+6 title "Q6" with lines
replot "./data_sys.dat" using 1:col+7 title "Q7" with lines
pause -1
col = col+7       


set title "Left joint coordinates values"
set grid
set xlabel "Time (seconds)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:col+1 title "Q1" with lines
replot "./data_sys.dat" using 1:col+2 title "Q2" with lines
replot "./data_sys.dat" using 1:col+3 title "Q3" with lines
replot "./data_sys.dat" using 1:col+4 title "Q4" with lines
replot "./data_sys.dat" using 1:col+5 title "Q5" with lines
replot "./data_sys.dat" using 1:col+6 title "Q6" with lines
replot "./data_sys.dat" using 1:col+7 title "Q7" with lines
pause -1
col = col+7       


set title "Right joint coordinate values"
set grid
set xlabel "Time (seconds)"
set ylabel "Value (rad)" 
plot "./data_sys.dat" using 1:col+1 title "Q1" with lines
replot "./data_sys.dat" using 1:col+2 title "Q2" with lines
replot "./data_sys.dat" using 1:col+3 title "Q3" with lines
replot "./data_sys.dat" using 1:col+4 title "Q4" with lines
replot "./data_sys.dat" using 1:col+5 title "Q5" with lines
replot "./data_sys.dat" using 1:col+6 title "Q6" with lines
replot "./data_sys.dat" using 1:col+7 title "Q7" with lines
pause -1
col = col+7       

quit