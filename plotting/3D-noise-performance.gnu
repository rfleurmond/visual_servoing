#set terminal postscript eps enhanced size 10,6 color solid font 'Helvetica,30'


set output "two-views-3D.eps"
set title "Erreur de reconstruction en fonction du bruit"
set grid
set xlabel "Ecart type du bruit injecte dans l'image"
set ylabel "Erreur 3D " 
set style data linespoints
plot "./v2-noise-performance.dat" using 1:2 title "Erreur moyenne avec 2 vues" with linespoints lw 5
pause -1


set output "two-views-2D.eps"
set title "Erreur de reprojection en fonction du bruit"
set grid
set xlabel "Ecart type du bruit injecte dans l'image"
set ylabel "Erreur 2D (pixels)" 
plot "./v2-noise-performance2D.dat" using 1:2 title "Erreur moyenne avec 2 vues" with linespoints lw 5
pause -1

col = 1
set output "n-views-3D.eps"
set title "Erreur de reconstruction en fonction du nombre de vues"
set grid
#set xrange [2:50]
#set yrange [0:0.06]
set xlabel "Nombre de vues"
set ylabel "Erreur 3D " 
plot "./noise-performance.dat" using 1:col+1 title "Ecart type 0 " with lines lw 5, \
      "./noise-performance.dat" using 1:col+2 title "Ecart type 1e-3 " with lines lw 5, \
      "./noise-performance.dat" using 1:col+3 title "Ecart type 2e-3 " with lines lw 5, \
      "./noise-performance.dat" using 1:col+4 title "Ecart type 4e-3 " with lines lw 5, \
      "./noise-performance.dat" using 1:col+5 title "Ecart type 6e-3 " with lines lw 5, \
      "./noise-performance.dat" using 1:col+6 title "Ecart type 8e-3 " with lines lw 5, \
      "./noise-performance.dat" using 1:col+7 title "Ecart type 1e-2 " with lines lw 5, \
      "./noise-performance.dat" using 1:col+8 title "Ecart type 1.2e-2 " with lines lw 5, \
      "./noise-performance.dat" using 1:col+9 title "Ecart type 1.4e-2 " with lines lw 5, \
      "./noise-performance.dat" using 1:col+10 title "Ecart type 1.6e-2 " with lines lw 5, \
      "./noise-performance.dat" using 1:col+11 title "Ecart type 1.8e-2 " with lines lw 5, \
      "./noise-performance.dat" using 1:col+12 title "Ecart type 2e-2" with lines lw 5

pause -1

col = 1
set output "n-views-3D-zoom.eps"
set title "Erreur de reconstruction en fonction du nombre de vues"
set grid
#set xrange [2:50]
#set yrange [0:0.02]
set xlabel "Nombre de vues"
set ylabel "Erreur 3D " 
plot  1e-2*sqrt(pi/2)/(x-1) title "Comportement souhaite" with points, \
      "./noise-performance.dat" using 1:col+1 title "Ecart type 0 " with lines lw 5, \
      "./noise-performance.dat" using 1:col+2 title "Ecart type 1e-3 " with lines lw 5, \
      "./noise-performance.dat" using 1:col+3 title "Ecart type 2e-3 " with lines lw 5, \
      "./noise-performance.dat" using 1:col+4 title "Ecart type 4e-3 " with lines lw 5, \
      "./noise-performance.dat" using 1:col+5 title "Ecart type 6e-3 " with lines lw 5, \
      "./noise-performance.dat" using 1:col+6 title "Ecart type 8e-3 " with lines lw 5, \
      "./noise-performance.dat" using 1:col+7 title "Ecart type 1e-2 " with lines lw 5 

pause -1



set output "n-views-2D.eps"
set title "Erreur de reprojection en fonction du nombre de vues"
set grid
#set xrange [2:50]
#set yrange [0:15]
set xlabel "Nombre de vues"
set ylabel "Erreur 2D  (pixels)" 

plot "./noise-performance2D.dat" using 1:col+1 title "Ecart type 0 " with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+2 title "Ecart type 1e-3 " with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+3 title "Ecart type 2e-3 " with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+4 title "Ecart type 4e-3 " with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+5 title "Ecart type 6e-3 " with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+6 title "Ecart type 8e-3 " with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+7 title "Ecart type 1e-2 " with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+8 title "Ecart type 1.2e-2 " with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+9 title "Ecart type 1.4e-2 " with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+10 title "Ecart type 1.6e-2 " with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+11 title "Ecart type 1.8e-2 " with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+12 title "Ecart type 2e-2" with lines lw 5


pause -1

set output "n-views-2D-zoom.eps"
set title "Erreur de reprojection en fonction du nombre de vues"
set grid
#set xrange [2:50]
#set yrange [0:7]
set xlabel "Nombre de vues"
set ylabel "Erreur 2D  (pixels)" 

plot "./noise-performance2D.dat" using 1:col+1 title "Ecart type 0 " with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+2 title "Ecart type 1e-3 " with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+3 title "Ecart type 2e-3 " with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+4 title "Ecart type 4e-3 " with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+5 title "Ecart type 6e-3 " with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+6 title "Ecart type 8e-3 " with lines lw 5, \
      "./noise-performance2D.dat" using 1:col+7 title "Ecart type 1e-2 " with lines lw 5

pause -1


quit
