col = 1
set title "Evolution de la tache  Gauche dans le temps"
set grid
set xlabel "Temps(secondes)"
set ylabel "Erreur en pixel" 
plot "./data_task.dat" using 1:col+1 title "T1" with lines
replot "./data_task.dat" using 1:col+2 title "T2" with lines
replot "./data_task.dat" using 1:col+3 title "T3" with lines
replot "./data_task.dat" using 1:col+4 title "T4" with lines
replot "./data_task.dat" using 1:col+5 title "T5" with lines
replot "./data_task.dat" using 1:col+6 title "T6" with lines
replot "./data_task.dat" using 1:col+7 title "T7" with lines
replot "./data_task.dat" using 1:col+8 title "T8" with lines
pause -1


#col = col +8
#set title "Evolution de la tache Droite dans le temps"
#set grid
#set xlabel "Temps(secondes)"
#set ylabel "Erreur en pixel" 
#plot "./data_task.dat" using 1:col+1 title "T1" with lines
#replot "./data_task.dat" using 1:col+2 title "T2" with lines
#replot "./data_task.dat" using 1:col+3 title "T3" with lines
#replot "./data_task.dat" using 1:col+4 title "T4" with lines
#replot "./data_task.dat" using 1:col+5 title "T5" with lines
#replot "./data_task.dat" using 1:col+6 title "T6" with lines
#replot "./data_task.dat" using 1:col+7 title "T7" with lines
#replot "./data_task.dat" using 1:col+8 title "T8" with lines
#pause -1

col = 1
set title "Evolution de la commande Gauche (vitesse angulaire) dans le temps"
set grid
set xlabel "Temps(secondes)"
set ylabel "Commande en vitesse(rad/s)" 
plot "./data_sys.dat" using 1:col+1 title "Wx" with lines
replot "./data_sys.dat" using 1:col+2 title "Wy" with lines
replot "./data_sys.dat" using 1:col+3 title "Wz" with lines
pause -1
col = col+3       

set title "Evolution de la commande Gauche (vitesse lineraire) dans le temps"
set grid
set xlabel "Temps(secondes)"
set ylabel "Commande en vitesse(m/s)" 
plot "./data_sys.dat" using 1:col+1 title "Vx" with lines
replot "./data_sys.dat" using 1:col+2 title "Vy" with lines
replot "./data_sys.dat" using 1:col+3 title "Vz" with lines
pause -1
col = col+3

set title "Evolution de la commande Droite (vitesse angulaire) dans le temps"
set grid
set xlabel "Temps(secondes)"
set ylabel "Commande en vitesse(rad/s)" 
plot "./data_sys.dat" using 1:col+1 title "Wx" with lines
replot "./data_sys.dat" using 1:col+2 title "Wy" with lines
replot "./data_sys.dat" using 1:col+3 title "Wz" with lines
pause -1
col = col+3

set title "Evolution de la commande Droite (vitesse lineraire) dans le temps"
set grid
set xlabel "Temps(secondes)"
set ylabel "Commande en vitesse(m/s)" 
plot "./data_sys.dat" using 1:col+1 title "Vx" with lines
replot "./data_sys.dat" using 1:col+2 title "Vy" with lines
replot "./data_sys.dat" using 1:col+3 title "Vz" with lines
pause -1
col = col+3

set title "Evolution de l'orientation de la camera Gauche dans le temps"
set grid
set xlabel "Temps(secondes)"
set ylabel "orientation(radians)" 
plot "./data_sys.dat" using 1:col+1 title "Ux" with lines
replot "./data_sys.dat" using 1:col+2 title "Uy" with lines
replot "./data_sys.dat" using 1:col+3 title "Uz" with lines
pause -1
col = col+3

pg = col+1
set title "Evolution de la position de la camera Gauche dans le temps"
set grid
set xlabel "Temps(secondes)"
set ylabel "Position (metres)" 
plot "./data_sys.dat" using 1:col+1 title "Px" with lines
replot "./data_sys.dat" using 1:col+2 title "Py" with lines
replot "./data_sys.dat" using 1:col+3 title "Pz" with lines
pause -1
col = col+3

set title "Evolution de l'orientation de la camera Droite dans le temps"
set grid
set xlabel "Temps(secondes)"
set ylabel "orientation(radians)" 
plot "./data_sys.dat" using 1:col+1 title "Ux" with lines
replot "./data_sys.dat" using 1:col+2 title "Uy" with lines
replot "./data_sys.dat" using 1:col+3 title "Uz" with lines
pause -1
col = col+3

pd = col+1
set title "Evolution de la position de la camera Droite dans le temps"
set grid
set xlabel "Temps(secondes)"
set ylabel "Position (metres)" 
plot "./data_sys.dat" using 1:col+1 title "Px" with lines
replot "./data_sys.dat" using 1:col+2 title "Py" with lines
replot "./data_sys.dat" using 1:col+3 title "Pz" with lines
pause -1
col = col+3

set title "Trajectoire de la camera Gauche dans l'espace"
set grid
set xlabel "X (metres)"
set ylabel "Y (metres)"
set zlabel "Z (metres) 
splot "./data_sys.dat" using pg:pg+1:pg+2 title "Trajectoire" with lines
pause -1

set title "Trajectoire de la camera Droite dans l'espace"
set grid
set xlabel "X (metres)"
set ylabel "Y (metres)"
set zlabel "Z (metres) 
splot "./data_sys.dat" using pd:pd+1:pd+2 title "Trajectoire" with lines
pause -1

set title "Trajectoire des deux cameras dans l'espace"
set grid
set xlabel "X (metres)"
set ylabel "Y (metres)"
set zlabel "Z (metres) 
splot "./data_sys.dat" using pg:pg+1:pg+2 title "Trajectoire 1" with lines, "./data_sys.dat" using pd:pd+1:pd+2 title "Trajectoire 2" with lines
pause -1



quit