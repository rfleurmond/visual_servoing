col = 2
p1 = col
set title "Estimation de P1"
set grid
set xlabel "iterations"
set ylabel "Position (metres)"
plot "./estim-nonlinear.dat" using 1:col   title "x" with lines
replot "./estim-nonlinear.dat" using 1:col+1 title "y" with lines
replot "./estim-nonlinear.dat" using 1:col+2 title "z" with lines
pause -1
col = col +3

p2 = col
set title "Estimation de P2"
set grid
set xlabel "iterations"
set ylabel "Position (metres)"
plot "./estim-nonlinear.dat" using 1:col   title "x" with lines
replot "./estim-nonlinear.dat" using 1:col+1 title "y" with lines
replot "./estim-nonlinear.dat" using 1:col+2 title "z" with lines
pause -1
col = col +3

p3 = col
set title "Estimation de P3"
set grid
set xlabel "iterations"
set ylabel "Position (metres)"
plot "./estim-nonlinear.dat" using 1:col   title "x" with lines
replot "./estim-nonlinear.dat" using 1:col+1 title "y" with lines
replot "./estim-nonlinear.dat" using 1:col+2 title "z" with lines
pause -1
col = col +3

p4 = col
set title "Estimation de P4"
set grid
set xlabel "iterations"
set ylabel "Position (metres)"
plot "./estim-nonlinear.dat" using 1:col   title "x" with lines
replot "./estim-nonlinear.dat" using 1:col+1 title "y" with lines
replot "./estim-nonlinear.dat" using 1:col+2 title "z" with lines
pause -1


set title "Trajectoire des points dans l'espace"
set grid
set xlabel "X (metres)"
set ylabel "Y (metres)"
set zlabel "Z (metres) 
splot "./estim-nonlinear.dat" using p1:p1+1:p1+2 title "PT1" with points,\
 "./estim-nonlinear.dat" using p2:p2+1:p2+2 title "PT2" with points,\
 "./estim-nonlinear.dat" using p3:p3+1:p3+2 title "PT3" with points,\
 "./estim-nonlinear.dat" using p4:p4+1:p4+2 title "PT4" with points
# "./vue.dat" using 1:2:3 title "V1" with lines,\
# "./vue.dat" using 4:5:6 title "V2" with lines,\
# "./vue.dat" using 7:8:9 title "V3" with lines,\
# "./vue.dat" using 10:11:12 title "V4" with lines
pause -1

quit
